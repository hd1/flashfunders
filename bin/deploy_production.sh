#!/bin/sh

set -e

git remote add migration git@heroku.com:flashfunders-migration.git || echo ''
git push --force migration $BRANCH_NAME:master
heroku run bundle exec rake db:migrate --app flashfunders-migration --exit-code
git remote add production git@heroku.com:flashfunders-production.git || echo ''
git push --force production $BRANCH_NAME:master
gem install flash_flow --no-ri --no-rdoc
flash_flow --prod-deploy

