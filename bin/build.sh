#!/bin/bash

EXIT_CODE=0

bundle exec rspec spec
EXIT_CODE=$(($EXIT_CODE+$?))

bundle exec rake jasmine:ci
EXIT_CODE=$(($EXIT_CODE+$?))

if [ $EXIT_CODE -eq 0 ]
then
    echo "Build successful"
else
    echo "Build failed"
fi

exit $EXIT_CODE
