#!/bin/bash

export RAILS_ENV=test

set -e
set -x

EXIT_CODE=0

if [ "$SEMAPHORE_TRIGGER_SOURCE" = "scheduler" ]
then
  gem install flash_flow --no-ri --no-rdoc
  # TODO - This could be moved into flash flow itself
  git config --global user.email "engineering@flashfunders.com"
  git config --global user.name "Flash Flow"

  flash_flow --merge-release-email
  flash_flow --make-release ready
  exit
fi

bundle install --deployment --path vendor/bundle
bundle exec rake db:drop db:create
bundle exec rake db:migrate

PERCY_ENABLE=0 bundle exec rspec spec
EXIT_CODE=$(($EXIT_CODE+$?))

if [ $EXIT_CODE = 0 ] && [ "$BRANCH_NAME" != "acceptance" ]
then
  PERCY_ENABLE=1 bundle exec rspec --tag percy spec
  EXIT_CODE=$(($EXIT_CODE+$?))
fi

bundle exec rake jasmine:ci
EXIT_CODE=$(($EXIT_CODE+$?))

bundle exec brakeman -q -o brakeman-output.tabs
EXIT_CODE=$(($EXIT_CODE+$?))

if [ "$BRANCH_NAME" = "compliance" ]
then
  gem install flash_flow --no-ri --no-rdoc

  flash_flow --release-email
fi

exit $EXIT_CODE
