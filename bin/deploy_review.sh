#!/bin/bash

function do_deploy {
    do_with_timing export HEROKU_APP=flashfunders-review
    do_with_timing export RAILS_ENV=review

    do_with_timing git remote add review git@heroku.com:flashfunders-review.git

    set -e

    do_with_timing git push --force review $BRANCH_NAME:master
    do_with_timing bundle exec rake environment_variables:check['review']
    do_with_timing heroku run bundle exec rake db:migrate --app flashfunders-review --exit-code
    do_with_timing heroku restart -a $HEROKU_APP

    set +e

    do_with_timing gem install flash_flow --no-ri --no-rdoc
    do_with_timing flash_flow --review-deploy
}

function do_with_timing {
    echo \$ "$@"
    time "$@"
    echo
}

do_with_timing do_deploy
