#!/bin/bash

export RAILS_ENV=test

set -e

EXIT_CODE=0

bundle
bundle exec rake environment_variables:check['review']
bundle exec rake db:migrate

bundle exec rspec spec
EXIT_CODE=$(($EXIT_CODE+$?))

bundle exec rake jasmine:ci
EXIT_CODE=$(($EXIT_CODE+$?))

bundle exec brakeman -q -o brakeman-output.tabs
EXIT_CODE=$(($EXIT_CODE+$?))

exit $EXIT_CODE
