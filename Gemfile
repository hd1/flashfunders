def private_repo(name)
  "https://5cc7e45ef7340707511c332ff147d9fd47bcb27d:x-oauth-basic@github.com/FlashFunders/#{name}"
end

source 'https://rubygems.org'

ruby '2.3.1'

gem 'rails', '4.2.5.1'

gem 'sidekiq'

gem 'ckeditor'

gem 'haml', '~> 4.0.3'

gem 'truncate_html'

gem 'simple_form', :git => 'git://github.com/plataformatec/simple_form.git'

gem 'pg', '~> 0.17.0'

gem 'sequel', '~> 4.7.0'

gem 'sassc-rails', '~> 1.3.0'

gem 'uglifier', '>= 1.3.0'

gem 'coffee-rails', '~> 4.0.0'

gem 'jquery-rails'

gem 'tilt-jade', '~> 1.2.0'

gem 'devise', '~> 3.4.1'

gem 'devise-async'

gem 'carrierwave_direct', git: private_repo('carrierwave_direct')

gem 'fog', '~> 1.23.0'

gem 'docusign_rest', '~> 0.1.0', git: private_repo('docusign_rest')

gem 'idology', '~> 2.0.0', git: private_repo('idology')

gem 'honeybadger', '~> 2.0.1'

gem 'unicorn', '~> 4.7.0'

gem 'unicorn-worker-killer'

gem 'redis-rails'

gem 'accreditor', git: private_repo("accreditor")

gem 'fund_america', git: private_repo("fund_america")

gem 'newrelic_rpm', '~> 3.14'

gem 'state_machines'

gem 'state_machines-activerecord'

gem 'email_validator', '~> 1.4.0', :require => 'email_validator/strict'

gem 'carmen-rails'

gem 'attr_encrypted'

gem 'flashfunders_utilities', git: private_repo('FlashUtilities')

gem 'momentjs-rails'

gem 'american_date'

gem 'gon'

gem 'execjs'

gem 'hubspot-ruby', git: private_repo('hubspot-ruby')

gem 'kaminari', '~> 0.16.3'

gem 'google-api-client', '0.9.1'

gem 'staccato', require: false

gem 'omniauth-facebook'

gem 'column_hider'

gem 'percy-capybara'

gem 'combine_pdf', '~> 0.2.29'

group :test, :development do
  gem 'dotenv-rails', '~> 0.9.0'
  gem 'rspec-rails', '~> 3.1.0'
  gem 'jasmine', '~> 2.2'
  gem 'brakeman', '~> 2.1.2', require: false
  gem 'awesome_print'
  gem 'quiet_assets', '~> 1.0.2'
  gem 'capybara-webkit', '~> 1.4.1'
  gem 'nokogiri', '~> 1.6.0'
  gem 'byebug'
  gem "codeclimate-test-reporter", require: nil
  gem 'railroady'
end

group :development do
  gem 'web-console', '~> 2.0'
  gem 'octokit'
  gem 'state_machines-graphviz'
  gem 'ruby-graphviz', '>=0.9.17'
end

group :test do
  gem 'selenium-webdriver'
  gem 'capybara', '~> 2.4.4'
  gem 'database_cleaner', '~> 1.1.1'
  gem 'artifice', '~> 0.6.0'
  gem 'timecop', '~> 0.6.3'
  gem 'sqlite3'
  gem 'guard-rspec'
  gem 'vcr'
  gem 'webmock'
  gem 'rspec-activemodel-mocks'
  gem 'factory_girl_rails'
  gem 'fakeredis', :require => 'fakeredis/rspec'
end

group :review, :production do
  gem 'rails_12factor', '~> 0.0.2'
  gem 'heroku_rails_deflate'
end

group :development, :test, :review do
  gem 'mail_safe', '~> 0.3.4'
end
