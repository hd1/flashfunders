class ChangeCampaignsVideoToVideoUrl < ActiveRecord::Migration
  def change
    rename_column :campaigns, :video, :video_url
  end
end
