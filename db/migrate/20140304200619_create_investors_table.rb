class CreateInvestorsTable < ActiveRecord::Migration
  def change
    create_table :investors do |t|
      t.integer :user_id
      t.string :state

      t.timestamps
    end

    add_index :investors, :user_id, unique: true
  end
end
