class AlterOfferingsChangeFieldsToDecimal < ActiveRecord::Migration
  def up
    change_column :offerings, :amount_previously_raised, :decimal
    change_column :offerings, :minimum_total_investment, :decimal
    change_column :offerings, :maximum_total_investment, :decimal
    change_column :offerings, :minimum_investment_amount, :decimal
    change_column :offerings, :pre_money_valuation, :decimal
    change_column :offerings, :share_price, :decimal
  end

  def down
    change_column :offerings, :amount_previously_raised, :decimal, precision: 64, scale: 2
    change_column :offerings, :minimum_total_investment, :decimal, precision: 64, scale: 2
    change_column :offerings, :maximum_total_investment, :decimal, precision: 64, scale: 2
    change_column :offerings, :minimum_investment_amount, :decimal, precision: 64, scale: 2
    change_column :offerings, :pre_money_valuation, :decimal, precision: 64, scale: 2
    change_column :offerings, :share_price, :decimal, precision: 64, scale: 3
  end
end
