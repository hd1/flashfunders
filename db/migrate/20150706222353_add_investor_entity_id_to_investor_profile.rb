class AddInvestorEntityIdToInvestorProfile < ActiveRecord::Migration
  def change
    add_column :investor_profiles, :investor_entity_id, :integer
  end
end
