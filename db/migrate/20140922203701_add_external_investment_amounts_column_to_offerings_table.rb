class AddExternalInvestmentAmountsColumnToOfferingsTable < ActiveRecord::Migration
  def change
    add_column :offerings, :external_investment_amount, :decimal, precision: 8, scale: 2, default: 0
    add_column :offerings, :external_investor_count, :integer, default: 0
  end
end
