class AddPreMoneyValuationToOfferings < ActiveRecord::Migration
  def change
    add_column :offerings, :pre_money_valuation, :string
  end
end
