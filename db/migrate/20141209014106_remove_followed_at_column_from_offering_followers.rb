class RemoveFollowedAtColumnFromOfferingFollowers < ActiveRecord::Migration
  def change
    remove_column :offering_followers, :followed_at, :datetime
  end
end
