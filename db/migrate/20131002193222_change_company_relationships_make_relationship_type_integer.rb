class ChangeCompanyRelationshipsMakeRelationshipTypeInteger < ActiveRecord::Migration
  def change
    remove_column :company_relationships, :relationship_type, :string
    add_column :company_relationships, :company_relationship_type_id, :integer
  end
end
