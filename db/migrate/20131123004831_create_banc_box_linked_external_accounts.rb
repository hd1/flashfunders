class CreateBancBoxLinkedExternalAccounts < ActiveRecord::Migration
  def change
    create_table :banc_box_linked_external_accounts do |t|
      t.references :banc_box_investor_account
      t.integer :banc_box_id
      t.timestamps
    end
  end
end