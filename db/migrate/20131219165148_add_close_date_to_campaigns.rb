class AddCloseDateToCampaigns < ActiveRecord::Migration
  def change
    add_column :campaigns, :close_date, :date
  end
end
