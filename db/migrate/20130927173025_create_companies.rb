class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.references :campaign
      t.string :name
      t.string :website_url
      t.string :corporate_name
      t.string :ein
      t.string :state_of_incorporation

      t.timestamps
    end
  end
end
