class BackfillAchDebitUuids < ActiveRecord::Migration
  def up
    AchDebit.find_each do |ach_debit|
      ach_debit.update_attributes!(uuid: SecureRandom.uuid)
    end
  end

  def down

  end
end
