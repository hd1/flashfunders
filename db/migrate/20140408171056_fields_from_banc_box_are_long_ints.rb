class FieldsFromBancBoxAreLongInts < ActiveRecord::Migration
  def up
    change_column :banc_box_fund_account_transactions, :transaction_id, :integer, limit: 8
    change_column :banc_box_linked_external_accounts, :banc_box_id, :integer, limit: 8
  end

  def down
    change_column :banc_box_linked_external_accounts, :banc_box_id, :integer, limit: 4
    change_column :banc_box_fund_account_transactions, :transaction_id, :integer, limit: 4
  end
end
