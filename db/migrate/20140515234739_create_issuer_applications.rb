class CreateIssuerApplications < ActiveRecord::Migration
  def change
    create_table :issuer_applications do |t|
      t.integer :user_id

      t.timestamps
    end
  end
end
