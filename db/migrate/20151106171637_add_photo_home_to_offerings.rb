class AddPhotoHomeToOfferings < ActiveRecord::Migration
  def change
    add_column :offerings, :photo_home, :string
    add_column :offerings, :tagline_browse, :string
  end
end
