class RemoveColumnsFromOffering < ActiveRecord::Migration
  def up
    change_table(:offerings) do |t|
      t.remove :visible
      t.remove :growth_stage_id
      t.remove :category_sector_id
      t.remove :pitch
      t.remove :swatch_photo
      t.remove :logo_photo
      t.remove :key_metrics
    end
  end
  def down
    # we can't recover the data, but we can recreate the columns, which may be enough to stop exceptions.
    change_table(:offerings) do |t|
      t.column :visible, :boolean
      t.column :growth_stage_id, :integer
      t.column :category_sector_id, :integer
      t.column :pitch, :text
      t.column :swatch_photo, :string
      t.column :logo_photo, :string
      t.column :key_metrics, :string
    end
  end
end
