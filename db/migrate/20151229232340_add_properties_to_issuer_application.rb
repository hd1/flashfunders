class AddPropertiesToIssuerApplication < ActiveRecord::Migration
  def change
    add_column :issuer_applications, :properties, :hstore
  end
end
