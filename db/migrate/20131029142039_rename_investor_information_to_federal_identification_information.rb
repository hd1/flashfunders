class RenameInvestorInformationToFederalIdentificationInformation < ActiveRecord::Migration
  def change
    rename_table :investor_informations, :federal_identification_informations
  end
end
