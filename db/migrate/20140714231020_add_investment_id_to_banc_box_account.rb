class AddInvestmentIdToBancBoxAccount < ActiveRecord::Migration
  def up
    add_column :banc_box_investor_accounts, :investment_id, :integer
    add_index :banc_box_investor_accounts, :investment_id, unique: true

    port_investment_ids

    remove_column :banc_box_investor_accounts, :user_id
  end

  def down
    add_column :banc_box_investor_accounts, :user_id, :integer
    add_index :banc_box_investor_accounts, :user_id, unique: true

    port_user_ids

    remove_column :banc_box_investor_accounts, :investment_id
  end

  private

  class BancBoxInvestorAccount < ActiveRecord::Base
    belongs_to :user
    belongs_to :investment
  end

  def port_user_ids
    BancBoxInvestorAccount.includes(:investment).all.each do |acct|
      acct.update_attributes user_id: acct.investment.user_id if acct.investment
    end
  end

  def port_investment_ids
    BancBoxInvestorAccount.includes(user: :investments).all.each do |acct|
      investment = acct.user.investments.detect { |i| i.state.in?(%w{linked_refund_account escrowed initiated_ach_debit failed_ach_debit wire_transfer_selected refunded transferred_investment}) } if acct.user

      if investment
        puts "Updating account: #{acct.id} with investment id: #{investment.id}"
        acct.update_attributes investment_id: investment.id
      else
        puts "No investment found for BancBoxInvestorAccount: #{acct.id}, which is weird but not impossible given how much messing around we do."
      end
    end
  end
end
