class RemoveUnusedColumnsFromOfferings < ActiveRecord::Migration
  def change
    remove_column :offerings, :amount_previously_raised, :string
    remove_column :offerings, :end_date, :date
  end
end
