class AddPassportImageToPersonalInfo < ActiveRecord::Migration
  def change
    add_column :personal_federal_identification_informations, :passport_image, :string
  end
end
