class RenameAccountNumberToEncryptedAccountNumber < ActiveRecord::Migration
  class InvestorBankAccount < ActiveRecord::Base; end

  def up
    rename_column :investor_bank_accounts, :account_number, :tmp_account_number
    add_column :investor_bank_accounts, :encrypted_account_number, :string

    InvestorBankAccount.all.each do |account|
      account.account_number = account.tmp_account_number
      account.save!
    end

    remove_column :investor_bank_accounts, :tmp_account_number
  end

  def down
    add_column :investor_bank_accounts, :tmp_account_number, :string

    InvestorBankAccount.all.each do |account|
      account.tmp_account_number = account.account_number
      account.save!
    end

    remove_column :investor_bank_accounts, :encrypted_account_number
    rename_column :investor_bank_accounts, :tmp_account_number, :account_number
  end
end
