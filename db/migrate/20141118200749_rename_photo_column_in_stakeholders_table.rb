class RenamePhotoColumnInStakeholdersTable < ActiveRecord::Migration
  def change
    add_column :stakeholders, :avatar_photo, :string
    remove_column :stakeholders, :photo, :text
  end
end
