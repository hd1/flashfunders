class AddFieldsToOffering < ActiveRecord::Migration
  def change
    add_column :offerings, :minimum_total_investment, :bigint
    add_column :offerings, :maximum_total_investment, :bigint
    add_column :offerings, :minimum_investment_amount, :bigint
  end
end
