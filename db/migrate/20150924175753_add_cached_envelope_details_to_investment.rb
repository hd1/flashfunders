class AddCachedEnvelopeDetailsToInvestment < ActiveRecord::Migration
  def change
    add_column :investments, :cached_envelope_details, :text
  end
end
