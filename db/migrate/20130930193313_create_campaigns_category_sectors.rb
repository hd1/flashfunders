class CreateCampaignsCategorySectors < ActiveRecord::Migration
  def change
    create_table :campaign_category_sectors do |t|
      t.references :category_sector
      t.references :campaign
    end
  end
end
