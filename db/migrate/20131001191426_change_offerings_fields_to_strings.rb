class ChangeOfferingsFieldsToStrings < ActiveRecord::Migration
  def change
    change_column :offerings, :minimum_total_investment, :string
    change_column :offerings, :maximum_total_investment, :string
    change_column :offerings, :minimum_investment_amount, :string
  end
end
