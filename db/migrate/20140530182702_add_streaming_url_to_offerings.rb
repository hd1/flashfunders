class AddStreamingUrlToOfferings < ActiveRecord::Migration
  def change
    add_column :offerings, :streaming_url, :string
  end
end
