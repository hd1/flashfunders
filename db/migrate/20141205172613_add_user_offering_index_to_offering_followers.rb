class AddUserOfferingIndexToOfferingFollowers < ActiveRecord::Migration
  def change
    add_index :offering_followers, [:user_id, :offering_id], unique: true
  end
end
