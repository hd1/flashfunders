class AddBankAccountIdToInvestment < ActiveRecord::Migration
  def up
    add_column :investments, :investor_account_id, :integer
    add_column :investments, :internal_account_id, :integer

    execute <<-SQL
      UPDATE investments SET investor_account_id = bank_accounts.id
      FROM bank_accounts
      WHERE bank_accounts.user_id = investments.user_id AND bank_accounts.type = 'Bank::InvestorAccount'
    SQL

    execute <<-SQL
      UPDATE investments SET internal_account_id = bank_accounts.id
      FROM bank_accounts
      WHERE bank_accounts.user_id = investments.user_id AND bank_accounts.type = 'Bank::InternalAccount'
    SQL
  end

  def down
    remove_column :investments, :investor_account_id
    remove_column :investments, :internal_account_id
  end
end
