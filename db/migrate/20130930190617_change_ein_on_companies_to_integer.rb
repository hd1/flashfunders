class ChangeEinOnCompaniesToInteger < ActiveRecord::Migration
  def change
    remove_column :companies, :ein
    add_column :companies, :ein, :integer
  end
end
