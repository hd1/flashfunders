class AddUuidToPeople < ActiveRecord::Migration
  def change
    add_column :people, :uuid, :string

    add_index 'people', ['uuid'], name: 'unique_people_uuid_index', unique: true, using: :btree
  end
end
