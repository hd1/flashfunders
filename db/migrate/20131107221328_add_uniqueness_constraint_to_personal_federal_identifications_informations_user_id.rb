class AddUniquenessConstraintToPersonalFederalIdentificationsInformationsUserId < ActiveRecord::Migration
  def up
    execute <<-SQL
      ALTER TABLE personal_federal_identification_informations
        ADD CONSTRAINT unique_personal_federal_identification_information_user_id
        UNIQUE (user_id)
    SQL
  end

  def down
    execute <<-SQL
      ALTER TABLE personal_federal_identification_informations
        DROP CONSTRAINT unique_personal_federal_identification_information_user_id
    SQL
  end
end
