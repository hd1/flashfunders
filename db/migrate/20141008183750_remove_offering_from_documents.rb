class RemoveOfferingFromDocuments < ActiveRecord::Migration
  def change
    remove_column :documents, :offering_id, :integer
  end
end
