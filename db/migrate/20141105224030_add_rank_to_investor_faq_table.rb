class AddRankToInvestorFaqTable < ActiveRecord::Migration
  def change
    add_column :investor_faqs, :rank, :integer
  end
end
