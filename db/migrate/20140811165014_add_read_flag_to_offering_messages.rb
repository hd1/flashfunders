class AddReadFlagToOfferingMessages < ActiveRecord::Migration
  def change
    add_column :offering_messages, :unread, :boolean, default: true
  end
end
