class RenameAgreeToPrerequisitesEvent < ActiveRecord::Migration
  class InvestorOnboardingEvent < ActiveRecord::Base

  end
  def up
    InvestorOnboardingEvent.where(type: 'InvestorOnboardingEvent::AccreditationSelfCertificationEvent').update_all "type='InvestorOnboardingEvent::AgreeToPrerequisitesEvent'"
  end

  def down
    InvestorOnboardingEvent.where(type: 'InvestorOnboardingEvent::AgreeToPrerequisitesEvent').update_all "type='InvestorOnboardingEvent::AccreditationSelfCertificationEvent'"
  end
end
