class CreateCampaignDocuments < ActiveRecord::Migration
  def change
    create_table :campaign_documents do |t|
      t.string :name
      t.string :document

      t.references :campaign

      t.timestamps
    end
  end
end
