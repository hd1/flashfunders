class AddClosedStatusToOfferingTable < ActiveRecord::Migration
  def change
    add_column :offerings, :closed_status, :integer, null: false, default: 1
  end
end

