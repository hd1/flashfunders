class AddPitchVideosTable < ActiveRecord::Migration
  def up
    create_table :pitch_videos do |t|
      t.string :url
      t.timestamps
    end
  end

  def down
    drop_table :pitch_videos
  end
end
