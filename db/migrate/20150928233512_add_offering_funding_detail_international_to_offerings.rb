class AddOfferingFundingDetailInternationalToOfferings < ActiveRecord::Migration
  def change
    add_column :offerings, :offering_funding_detail_direct_international_id, :integer
    add_column :offerings, :offering_funding_detail_spv_international_id, :integer
  end
end
