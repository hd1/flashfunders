class UpdateUuidColumnsToNativeType < ActiveRecord::Migration
  def up
    execute "ALTER TABLE users ALTER COLUMN uuid type uuid using uuid::uuid"
    execute "ALTER TABLE ach_debits ALTER COLUMN uuid type uuid using uuid::uuid"
    execute "ALTER TABLE investments ALTER COLUMN uuid type uuid using uuid::uuid"
    execute "ALTER TABLE campaigns ALTER COLUMN uuid type uuid using uuid::uuid"
    execute "ALTER TABLE deal_documents ALTER COLUMN uuid type uuid using uuid::uuid"
    execute "ALTER TABLE campaign_documents ALTER COLUMN uuid type uuid using uuid::uuid"
    execute "ALTER TABLE people ALTER COLUMN uuid type uuid using uuid::uuid"
    execute "ALTER TABLE profiles ALTER COLUMN uuid type uuid using uuid::uuid"
  end

  def down
    execute "ALTER TABLE users ALTER COLUMN uuid type string"
    execute "ALTER TABLE ach_debits ALTER COLUMN uuid type uuid"
    execute "ALTER TABLE investments ALTER COLUMN uuid type uuid"
    execute "ALTER TABLE campaigns ALTER COLUMN uuid type uuid"
    execute "ALTER TABLE deal_documents ALTER COLUMN uuid type uuid"
    execute "ALTER TABLE campaign_documents ALTER COLUMN uuid type uuid"
    execute "ALTER TABLE people ALTER COLUMN uuid type uuid"
    execute "ALTER TABLE profiles ALTER COLUMN uuid type uuid"
  end
end
