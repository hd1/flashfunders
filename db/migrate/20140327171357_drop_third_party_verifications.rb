class DropThirdPartyVerifications < ActiveRecord::Migration
  def change
    drop_table :third_party_verifications
  end
end
