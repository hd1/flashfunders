class AddStateIdToCompanies < ActiveRecord::Migration
  def change
    remove_column :companies, :state_of_incorporation
    add_column :companies, :state_id, :integer
  end
end
