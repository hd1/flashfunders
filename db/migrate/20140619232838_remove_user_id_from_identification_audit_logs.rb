class RemoveUserIdFromIdentificationAuditLogs < ActiveRecord::Migration
  def up
    execute <<-SQL
      UPDATE identification_audit_logs
        SET investor_entity_id = investor_entities.id
      FROM investor_entities
      WHERE investor_entities.user_id = identification_audit_logs.user_id
      AND identification_audit_logs.investor_entity_id IS NULL
    SQL

    remove_column :identification_audit_logs, :user_id
  end

  def down
    add_column :identification_audit_logs, :user_id, :integer

    execute <<-SQL
      UPDATE identification_audit_logs
        SET user_id = investor_entities.user_id
      FROM investor_entities
      WHERE investor_entities.id = identification_audit_logs.investor_entity_id
    SQL
  end
end
