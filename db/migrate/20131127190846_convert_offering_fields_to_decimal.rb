class ConvertOfferingFieldsToDecimal < ActiveRecord::Migration
  def up
    add_column :offerings, :wat_amount_previously_raised, :decimal, precision: 64, scale: 2
    add_column :offerings, :wat_minimum_total_investment, :decimal, precision: 64, scale: 2
    add_column :offerings, :wat_maximum_total_investment, :decimal, precision: 64, scale: 2
    add_column :offerings, :wat_minimum_investment_amount, :decimal, precision: 64, scale: 2
    add_column :offerings, :wat_pre_money_valuation, :decimal, precision: 64, scale: 2
    add_column :offerings, :wat_share_price, :decimal, precision: 64, scale: 3
    add_column :offerings, :wat_number_of_days, :integer


    Offering.connection.select_all('SELECT * FROM offerings;').each do |table_row|
      execute <<-SQL
UPDATE offerings SET wat_amount_previously_raised = #{table_row['amount_previously_raised'].to_f} WHERE offerings.id = #{table_row['id']};
UPDATE offerings SET wat_minimum_total_investment = #{table_row['minimum_total_investment'].to_f} WHERE offerings.id = #{table_row['id']};
UPDATE offerings SET wat_maximum_total_investment = #{table_row['maximum_total_investment'].to_f} WHERE offerings.id = #{table_row['id']};
UPDATE offerings SET wat_minimum_investment_amount = #{table_row['minimum_investment_amount'].to_f} WHERE offerings.id = #{table_row['id']};
UPDATE offerings SET wat_pre_money_valuation = #{table_row['pre_money_valuation'].to_f} WHERE offerings.id = #{table_row['id']};
UPDATE offerings SET wat_share_price = #{table_row['share_price'].to_f} WHERE offerings.id = #{table_row['id']};
UPDATE offerings SET wat_number_of_days = #{table_row['number_of_days'].to_i} WHERE offerings.id = #{table_row['id']};
      SQL
    end


    remove_column :offerings, :amount_previously_raised
    remove_column :offerings, :minimum_total_investment
    remove_column :offerings, :maximum_total_investment
    remove_column :offerings, :minimum_investment_amount
    remove_column :offerings, :pre_money_valuation
    remove_column :offerings, :share_price
    remove_column :offerings, :number_of_days

    rename_column :offerings, :wat_amount_previously_raised, :amount_previously_raised
    rename_column :offerings, :wat_minimum_total_investment, :minimum_total_investment
    rename_column :offerings, :wat_maximum_total_investment, :maximum_total_investment
    rename_column :offerings, :wat_minimum_investment_amount, :minimum_investment_amount
    rename_column :offerings, :wat_pre_money_valuation, :pre_money_valuation
    rename_column :offerings, :wat_share_price, :share_price
    rename_column :offerings, :wat_number_of_days, :number_of_days
  end

  def down
    change_column :offerings, :amount_previously_raised, :string
    change_column :offerings, :minimum_total_investment, :string
    change_column :offerings, :maximum_total_investment, :string
    change_column :offerings, :minimum_investment_amount, :string
    change_column :offerings, :pre_money_valuation, :string
    change_column :offerings, :share_price, :string
    change_column :offerings, :number_of_days, :string
  end
end
