class AddFundraisingFieldsToOffering < ActiveRecord::Migration
  def change
    add_column :offerings, :carried_interest_display,     :text, limit: 255
    add_column :offerings, :setup_fee_percentage_display, :text, limit: 255
    add_column :offerings, :management_fee_display,       :text, limit: 255
    add_column :offerings, :commission_fee_display,       :text, limit: 255
    add_column :offerings, :setup_fee_dollar,             :decimal, precision: 64, scale: 2
    add_column :offerings, :spv_minimum_goal,             :decimal, precision: 64, scale: 2
  end
end
