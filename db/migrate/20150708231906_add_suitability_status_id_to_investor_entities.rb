class AddSuitabilityStatusIdToInvestorEntities < ActiveRecord::Migration
  def change
    add_column :investor_entities, :suitability_status, :integer, null: false, default: 1
  end
end
