class AddCoverPhotoToCampaigns < ActiveRecord::Migration
  def change
    add_column :campaigns, :cover_photo, :string
  end
end
