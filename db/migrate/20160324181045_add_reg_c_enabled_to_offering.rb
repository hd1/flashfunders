class AddRegCEnabledToOffering < ActiveRecord::Migration
  def change
    add_column :offerings, :reg_c_enabled, :boolean
  end
end
