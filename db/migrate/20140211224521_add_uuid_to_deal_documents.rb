class AddUuidToDealDocuments < ActiveRecord::Migration
  def change
    add_column :deal_documents, :uuid, :string

    add_index 'deal_documents', ['uuid'], name: 'unique_deal_documents_uuid_index', unique: true, using: :btree
  end
end
