class CreateInvestments < ActiveRecord::Migration
  def change
    create_table :investments do |t|
      t.references :user
      t.references :campaign
      t.integer :shares
      t.float :amount
    end
  end
end
