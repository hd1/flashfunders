class AddFlagsToInvestment < ActiveRecord::Migration
  def change
    add_column :investments, :flags, :hstore
  end
end
