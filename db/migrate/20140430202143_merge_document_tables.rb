class MergeDocumentTables < ActiveRecord::Migration
  def up
    create_table(:documents) do |t|
      t.string :type
      t.string :name
      t.string :file
      t.integer :campaign_id
      t.uuid   :uuid
    end

    connection.execute("INSERT INTO documents (type, name, file, uuid, campaign_id) \
                        SELECT 'CampaignDocument', name, file, uuid, campaign_id FROM campaign_documents")
    connection.execute("INSERT INTO documents (type, name, file, uuid, campaign_id) \
                        SELECT 'DealDocument', name, file, uuid, campaign_id FROM deal_documents")
  end

  def down
    drop_table :documents
  end
end
