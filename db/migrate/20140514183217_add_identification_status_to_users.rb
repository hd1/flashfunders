class AddIdentificationStatusToUsers < ActiveRecord::Migration
  def up
    add_column :users, :identification_status_id, :integer, null: false, default: 1
    add_index :users, :identification_status_id

    execute <<-SQL
      UPDATE users SET identification_status_id = 1 WHERE cip_status = 'Not Reviewed';
      UPDATE users SET identification_status_id = 2 WHERE cip_status = 'Review in Progress';
      UPDATE users SET identification_status_id = 3 WHERE cip_status = 'Verified';
      UPDATE users SET identification_status_id = 4 WHERE cip_status = 'Rejected';
    SQL
  end

  def down
    remove_column :users, :identification_status_id
  end
end

