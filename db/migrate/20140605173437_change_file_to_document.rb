class ChangeFileToDocument < ActiveRecord::Migration
  def up
    add_column :documents, :document, :string

    execute <<-SQL
      UPDATE documents SET document = concat_ws('/', uuid, file)
    SQL
  end

  def down
    remove_column :documents, :document
  end
end
