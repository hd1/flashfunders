class AddAdminEmailSentToInvestments < ActiveRecord::Migration
  def change
    add_column :investments, :admin_email_sent, :boolean, null: false, default: false
  end
end
