class AddSuitabilityStatusColumnToInvestorProfiles < ActiveRecord::Migration
  def up
    add_column :investor_profiles, :suitability_status_id, :integer, null: false, default: 1
    add_index :investor_profiles, :suitability_status_id
  end

  def down
    remove_column :investor_profiles, :suitability_status_id
  end
end
