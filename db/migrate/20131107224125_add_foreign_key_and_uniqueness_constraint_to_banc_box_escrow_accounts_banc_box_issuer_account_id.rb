class AddForeignKeyAndUniquenessConstraintToBancBoxEscrowAccountsBancBoxIssuerAccountId < ActiveRecord::Migration
  def up
    execute <<-SQL
      ALTER TABLE banc_box_escrow_accounts
        ADD CONSTRAINT fk_banc_box_escrow_account_banc_box_issuer_account
        FOREIGN KEY (banc_box_issuer_account_id)
        REFERENCES banc_box_issuer_accounts(id)
    SQL

    execute <<-SQL
      ALTER TABLE banc_box_escrow_accounts
        ADD CONSTRAINT unique_banc_box_escrow_account_banc_box_issuer_account_id
        UNIQUE (banc_box_issuer_account_id)
    SQL
  end

  def down
    execute <<-SQL
      ALTER TABLE banc_box_escrow_accounts
        DROP CONSTRAINT fk_banc_box_escrow_account_banc_box_issuer_account
    SQL
    execute <<-SQL
      ALTER TABLE banc_box_escrow_accounts
        DROP CONSTRAINT unique_banc_box_escrow_account_banc_box_issuer_account_id
    SQL
  end
end