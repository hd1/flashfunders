class AddColumnsToIssuerApplication < ActiveRecord::Migration
  def change
    add_column :issuer_applications, :first_name, :string
    add_column :issuer_applications, :middle_initial, :string, limit: 1
    add_column :issuer_applications, :last_name, :string
    add_column :issuer_applications, :phone_number, :string
    add_column :issuer_applications, :company_name, :string
    add_column :issuer_applications, :website, :string
    add_column :issuer_applications, :raise_amount, :integer
    add_column :issuer_applications, :pre_money_valuation, :integer
    add_column :issuer_applications, :previously_raised, :integer
    add_column :issuer_applications, :pledged, :integer
    add_column :issuer_applications, :entity_type, :string
    add_column :issuer_applications, :state_id, :integer
    add_column :issuer_applications, :message, :string
  end
end
