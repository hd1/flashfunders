class RenameImpersonatedByColumnInAuditLogEntries < ActiveRecord::Migration
  def up
    rename_column :audit_log_entries, :impersonated_by, :impersonating_user_id
  end

  def down
    rename_column :audit_log_entries, :impersonating_user_id, :impersonated_by
  end
end
