class AddProviderIndexToOfferingsTable < ActiveRecord::Migration
  def up
    add_index :offerings, :provider
  end

  def down
    remove_index :offerings, :provider
  end
end
