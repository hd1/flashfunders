class ChangeInvestorSuitabilityQuestionnaireCategorySectorToReference < ActiveRecord::Migration
  def change
    rename_column :investor_suitability_questionnaire_category_sectors, :category_sector, :category_sector_id
  end
end
