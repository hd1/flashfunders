class AddIndicesToUuidFields < ActiveRecord::Migration
  def change
    add_index 'users', ['uuid'], name: 'unique_users_uuid_index', unique: true, using: :btree
    add_index 'ach_debits', ['uuid'], name: 'unique_ach_debits_uuid_index', unique: true, using: :btree
    add_index 'investments', ['uuid'], name: 'unique_investments_uuid_index', unique: true, using: :btree
    add_index 'campaigns', ['uuid'], name: 'unique_campaigns_uuid_index', unique: true, using: :btree
  end
end
