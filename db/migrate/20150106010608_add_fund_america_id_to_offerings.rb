class AddFundAmericaIdToOfferings < ActiveRecord::Migration
  def change
    add_column :offerings, :fund_america_id, :string
  end

end
