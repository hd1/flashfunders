class AddUuidToCampaignDocuments < ActiveRecord::Migration
  def change
    add_column :campaign_documents, :uuid, :string

    add_index 'campaign_documents', ['uuid'], name: 'unique_campaign_documents_uuid_index', unique: true, using: :btree
  end
end
