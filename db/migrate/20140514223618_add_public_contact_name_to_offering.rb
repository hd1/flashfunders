class AddPublicContactNameToOffering < ActiveRecord::Migration
  def change
    add_column :offerings, :public_contact_name, :string
  end
end
