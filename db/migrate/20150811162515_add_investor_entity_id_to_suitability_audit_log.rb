class AddInvestorEntityIdToSuitabilityAuditLog < ActiveRecord::Migration
  def change
    add_column :suitability_audit_logs, :investor_entity_id, :integer
  end
end
