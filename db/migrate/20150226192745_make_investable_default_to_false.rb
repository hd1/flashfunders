class MakeInvestableDefaultToFalse < ActiveRecord::Migration
  def up
    change_column_default :offerings, :investable, false
  end

  def down
    change_column_default :offerings, :investable, true
  end
end
