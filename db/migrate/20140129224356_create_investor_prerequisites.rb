class CreateInvestorPrerequisites < ActiveRecord::Migration
  def change
    create_table :investor_prerequisites do |t|
      t.integer :user_id
      t.boolean :self_certified
      t.boolean :capable_of_evaluating_illiquid_investments
      t.boolean :understand_risk
    end
  end
end
