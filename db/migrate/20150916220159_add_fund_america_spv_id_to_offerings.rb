class AddFundAmericaSpvIdToOfferings < ActiveRecord::Migration
  def change
    add_column :offerings, :fund_america_spv_id, :string
  end
end
