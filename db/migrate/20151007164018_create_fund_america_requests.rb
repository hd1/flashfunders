class CreateFundAmericaRequests < ActiveRecord::Migration
  def change
    create_table :fund_america_requests do |t|
      t.integer :offering_id
      t.json :request
      t.json :response
    end
  end
end
