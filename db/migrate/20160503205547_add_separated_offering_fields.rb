class AddSeparatedOfferingFields < ActiveRecord::Migration

  COLUMNS = {
      minimum_total_investment: { type: :decimal },
      minimum_total_investment_display: { type: :string },
      maximum_total_investment: { type: :decimal },
      maximum_total_investment_display: { type: :string },
      minimum_investment_amount: { type: :decimal },
      minimum_investment_amount_display: { type: :string },
      shares_offered: { type: :integer },
      fully_diluted_shares: { type: :integer },
      external_investment_amount: { type: :decimal, options: { default: 0 } },
      external_investor_count: { type: :integer, options: { default: 0 } },
      security_type_id: { type: :integer }
  }

  def up
    COLUMNS.each do |col, data|
      add(:cf, col, data[:type], data[:options])
      add(:seed, col, data[:type], data[:options])
    end

    copy_existing_columns(:seed)
  end

  def down
    COLUMNS.keys.each do |col|
      remove_column :offerings, col_name(:cf, col)
      remove_column :offerings, col_name(:seed, col)
    end
  end

  private

  def col_name(prefix, col)
    "#{prefix}_#{col}"
  end

  def add(prefix, col, type, options)
    options ||= {}
    add_column :offerings, col_name(prefix, col), type, options
  end

  def copy_existing_columns(prefix)
    assignments = COLUMNS.keys.map do |col|
      "#{prefix}_#{col} = #{col}"
    end

    Offering.update_all(assignments.join(', '))
  end
end
