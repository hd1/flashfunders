class CreateBancBoxFundAccountTransactions < ActiveRecord::Migration
  def change
    create_table :banc_box_fund_account_transactions do |t|
      t.integer :transaction_id
      t.integer :ach_debit_id
    end
  end
end
