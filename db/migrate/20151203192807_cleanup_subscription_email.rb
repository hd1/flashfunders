class CleanupSubscriptionEmail < ActiveRecord::Migration
  def up
    Subscription.connection.execute('UPDATE subscriptions SET email = LOWER(LTRIM(RTRIM(email)))')
    Subscription.dedupe

    add_index :subscriptions, :email, unique: true
    add_index :subscriptions, :created_at
  end

  def down
    remove_index :subscriptions, :email
    remove_index :subscriptions, :created_at
  end
end
