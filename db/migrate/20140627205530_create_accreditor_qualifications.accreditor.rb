# This migration comes from accreditor (originally 20140627181135)
class CreateAccreditorQualifications < ActiveRecord::Migration
  def change
    rename_table :accreditation_qualifications, :accreditor_qualifications
  end
end
