class RemoveProviderColumnFromOfferings < ActiveRecord::Migration
  def change
    remove_column :offerings, :provider, :integer
  end
end
