class ChangeBancBoxIssuerAccountsToReferenceCampaignInsteadOfUser < ActiveRecord::Migration
  def change
    rename_column :banc_box_issuer_accounts, :user_id, :campaign_id
  end
end
