class CreatePitchSubsections < ActiveRecord::Migration
  def change
    create_table :pitch_subsections do |t|
      t.integer :pitch_section_id
      t.string :type
      t.integer :rank
      t.json :options

      t.timestamps
    end
  end
end
