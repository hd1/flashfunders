class RenameAccreditationDocumentFileUrl < ActiveRecord::Migration
  def change
    rename_column :accreditation_documents, :file_url, :file
  end
end
