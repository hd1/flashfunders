class CreateInvestorSuitabilityQuestionnaireInvestmentRiskLevels < ActiveRecord::Migration
  def change
    create_table :investor_suitability_questionnaire_investment_risk_levels do |t|
      t.references :investor_suitability_questionnaire
      t.references :investment_risk_level
    end
  end
end
