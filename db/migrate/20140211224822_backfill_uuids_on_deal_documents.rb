class BackfillUuidsOnDealDocuments < ActiveRecord::Migration
  class FakeClass < ActiveRecord::Base; self.table_name = :deal_documents; end

  def up
    FakeClass.find_each do |deal_document|
      deal_document.update_attributes!(uuid: SecureRandom.uuid)
    end
  end

  def down
  end

end
