class AddEmailConsentAtToUser < ActiveRecord::Migration
  def change
    add_column :users, :email_consent_at, :timestamp
  end
end
