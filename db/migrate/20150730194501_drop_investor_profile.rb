class DropInvestorProfile < ActiveRecord::Migration
  def up
    drop_table :investor_profiles
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
