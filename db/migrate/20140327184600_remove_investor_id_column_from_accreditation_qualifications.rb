class RemoveInvestorIdColumnFromAccreditationQualifications < ActiveRecord::Migration
  def up
    remove_column :accreditation_qualifications, :investor_id
  end

  def down
    add_column :accreditation_qualifications, :investor_id, :integer

    execute <<-SQL
      UPDATE accreditation_qualifications
        SET investor_id = investors.id
      FROM investors
      WHERE accreditation_qualifications.user_id = investors.user_id
    SQL

    change_column :accreditation_qualifications, :investor_id, :integer, null: false
  end
end
