class InvestorFaqs < ActiveRecord::Migration
  def change
    create_table :investor_faqs do |t|
      t.text :question
      t.text :answer

      t.references :campaign
      t.timestamps
    end
  end
end
