class RevokePermissionsOnAuditLog < ActiveRecord::Migration
  def up
    connection.execute("REVOKE DELETE ON audit_log_entries FROM #{current_user}")
    connection.execute("REVOKE UPDATE ON audit_log_entries FROM #{current_user}")
  end

  def down
    connection.execute("GRANT DELETE ON audit_log_entries TO #{current_user}")
    connection.execute("GRANT UPDATE ON audit_log_entries TO #{current_user}")
  end

  private

  def current_user
    @current_user ||= connection.execute("select current_user").first['current_user']
  end
end
