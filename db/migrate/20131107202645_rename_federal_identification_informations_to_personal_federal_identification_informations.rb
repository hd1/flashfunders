class RenameFederalIdentificationInformationsToPersonalFederalIdentificationInformations < ActiveRecord::Migration
  def change
    rename_table :federal_identification_informations, :personal_federal_identification_informations
  end
end
