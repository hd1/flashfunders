class AddDisplayColumnForOfferingNumerics < ActiveRecord::Migration
  def up
    add_column :offerings, :maximum_total_investment_display, :string
    add_column :offerings, :minimum_total_investment_display, :string
    add_column :offerings, :minimum_investment_amount_display, :string
    add_column :offerings, :pre_money_valuation_display, :string

    execute <<-SQL
      UPDATE offerings
        SET maximum_total_investment_display = maximum_total_investment
    SQL

    execute <<-SQL
      UPDATE offerings
        SET minimum_total_investment_display = minimum_total_investment
    SQL

    execute <<-SQL
      UPDATE offerings
        SET minimum_investment_amount_display = minimum_investment_amount
    SQL

    execute <<-SQL
      UPDATE offerings SET pre_money_valuation_display = pre_money_valuation
    SQL
  end

  def down
    remove_column :offerings, :maximum_total_investment_display
    remove_column :offerings, :minimum_total_investment_display
    remove_column :offerings, :minimum_investment_amount_display
    remove_column :offerings, :pre_money_valuation_display
  end
end
