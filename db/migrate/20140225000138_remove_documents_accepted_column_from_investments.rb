class RemoveDocumentsAcceptedColumnFromInvestments < ActiveRecord::Migration
  def change
    remove_column :investments, :documents_accepted
  end
end
