class RemoveIdentificationStatusFromUsers < ActiveRecord::Migration
  def change
    remove_column :users, :identification_status_id, :integer, null: false, default: 1
  end
end
