class AddVisibilityToOffering < ActiveRecord::Migration
  class Offering < ActiveRecord::Base
    enum visibility: { visibility_restricted: 1, visibility_preview: 2, visibility_public: 3 }
  end

  def up
    add_column :offerings, :visibility, :integer, null: false, default: Offering.visibilities[:visibility_restricted]
    Offering.all.each do |offering|
      if offering.visible?
        offering.update_attribute(:visibility, Offering.visibilities[:visibility_public])
      else
        offering.update_attribute(:visibility, Offering.visibilities[:visibility_preview])
      end
      puts "Updated offering #{offering.id} with #{offering.visibility}"
    end
  end

  def down
    remove_column :offerings, :visibility
  end
end
