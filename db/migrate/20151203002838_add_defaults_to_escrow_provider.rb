class AddDefaultsToEscrowProvider < ActiveRecord::Migration
  def change
    add_column :escrow_providers, :default_instructions, :json
    add_column :escrow_providers, :provider_key, :string
  end
end
