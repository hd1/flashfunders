class AddEncryptedAccountHolderToBankAccounts < ActiveRecord::Migration
  def change
    add_column :bank_accounts, :encrypted_account_holder, :string
  end
end
