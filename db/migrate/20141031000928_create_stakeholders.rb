class CreateStakeholders < ActiveRecord::Migration
  def change
    create_table :stakeholders do |t|
      t.integer :offering_id
      t.integer :stakeholder_type_id
      t.integer :rank
      t.string :full_name
      t.string :position
      t.string :website
      t.string :twitter_url
      t.string :facebook_url
      t.string :linkedin_url
      t.text :photo

      t.text :bio

      t.references :offering

      t.timestamps
    end

  end
end
