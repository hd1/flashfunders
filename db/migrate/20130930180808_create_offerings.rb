class CreateOfferings < ActiveRecord::Migration
  def change
    create_table :offerings do |t|
      t.integer :amount_previously_raised
      t.references :campaign
      t.timestamps
    end
  end
end
