class BackfillUuidsOnProfiles < ActiveRecord::Migration
  class Profile < ActiveRecord::Base; end

  def up
    Profile.find_each do |profile|
      profile.update_attributes!(uuid: SecureRandom.uuid)
    end
  end

  def down
  end

end
