class CreateEscrowProvider < ActiveRecord::Migration
  def change
    create_table :escrow_providers do |t|
      t.string :name
      t.hstore :properties

      t.timestamps null: false
    end

    add_column :offerings, :escrow_provider_id, :integer

  end
end
