class AddUuidToCampaigns < ActiveRecord::Migration
  def change
    add_column :campaigns, :uuid, :string
  end
end
