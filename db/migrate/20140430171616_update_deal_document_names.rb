class UpdateDealDocumentNames < ActiveRecord::Migration
  class FakeClass < ActiveRecord::Base; self.table_name = :deal_documents; end

  def up
    FakeClass.where(name: "Subscription Rights Agreement").update_all(name: "Subscription Agreement")
    FakeClass.where(name: "Investor Rights Agreement").update_all(name: "Investors' Rights Agreement")
    FakeClass.where(name: "Restated Certification of Incorporation").update_all(name: "Restated Certificate of Incorporation")
  end

  def down
    FakeClass.where(name: "Subscription Agreement").update_all(name: "Subscription Rights Agreement")
    FakeClass.where(name: "Investors' Rights Agreement").update_all(name: "Investor Rights Agreement")
    FakeClass.where(name: "Restated Certificate of Incorporation").update_all(name: "Restated Certification of Incorporation")
  end
end

