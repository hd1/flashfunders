class AddMobilePhoneToUserInfo < ActiveRecord::Migration
  def change
    add_column :personal_federal_identification_informations, :mobile_phone, :string

    rename_column :personal_federal_identification_informations, :phone_number, :daytime_phone
  end
end
