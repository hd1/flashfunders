class CreateEscrowCaches < ActiveRecord::Migration
  def change
    create_table :escrow_caches do |t|
      t.json :escrow_data
      t.string :banc_box_escrow_id

      t.timestamps
    end

    add_index :escrow_caches, :banc_box_escrow_id, unique: true
  end
end
