class AddCustomDataToOfferings < ActiveRecord::Migration
  def change
    add_column :offerings, :custom_data, :hstore
  end
end
