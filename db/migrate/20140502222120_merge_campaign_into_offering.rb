class MergeCampaignIntoOffering < ActiveRecord::Migration

  def up
    update_offerings
    switch_campaign_id_to_offering_id(:banc_box_issuer_accounts)
    switch_campaign_id_to_offering_id(:campaign_category_sectors)
    switch_campaign_id_to_offering_id(:campaign_questions)
    switch_campaign_id_to_offering_id(:companies)
    switch_campaign_id_to_offering_id(:documents)
    switch_campaign_id_to_offering_id(:investments)
    switch_campaign_id_to_offering_id(:investor_faqs)
  end

  def down
    remove_column :offerings, :user_id, :integer
    remove_column :offerings, :growth_stage_id, :integer
    remove_column :offerings, :category_sector_id, :integer
    remove_column :offerings, :video_url, :string
    remove_column :offerings, :elevator_pitch, :string
    remove_column :offerings, :pitch, :text
    remove_column :offerings, :tagline, :string
    remove_column :offerings, :cover_photo, :string
    remove_column :offerings, :visible, :boolean
    remove_column :offerings, :swatch_photo, :string
    remove_column :offerings, :close_date, :date
    remove_column :offerings, :logo_photo, :string
    remove_column :offerings, :public_email, :string
    remove_column :offerings, :key_metrics, :text
    remove_column :offerings, :uuid, :string
    remove_offering_id(:banc_box_issuer_accounts)
    remove_offering_id(:campaign_category_sectors)
    remove_offering_id(:campaign_questions)
    remove_offering_id(:companies)
    remove_offering_id(:documents)
    remove_offering_id(:investments)
    remove_offering_id(:investor_faqs)
  end

  private

  def update_offerings
    add_column :offerings, :user_id, :integer
    add_column :offerings, :growth_stage_id, :integer
    add_column :offerings, :category_sector_id, :integer
    add_column :offerings, :video_url, :string
    add_column :offerings, :elevator_pitch, :text
    add_column :offerings, :pitch, :text
    add_column :offerings, :tagline, :string
    add_column :offerings, :cover_photo, :string
    add_column :offerings, :visible, :boolean
    add_column :offerings, :swatch_photo, :string
    add_column :offerings, :close_date, :date
    add_column :offerings, :logo_photo, :string
    add_column :offerings, :public_email, :string
    add_column :offerings, :key_metrics, :text
    add_column :offerings, :uuid, :string

    sql =<<-EOS
      UPDATE offerings
      SET user_id = campaigns.user_id,
          growth_stage_id = campaigns.growth_stage_id,
          category_sector_id = campaigns.category_sector_id,
          video_url = campaigns.video_url,
          elevator_pitch = campaigns.elevator_pitch,
          pitch = campaigns.pitch,
          tagline = campaigns.tagline,
          cover_photo = campaigns.cover_photo,
          visible = campaigns.visible,
          swatch_photo = campaigns.swatch_photo,
          close_date = campaigns.close_date,
          logo_photo = campaigns.logo_photo,
          public_email = campaigns.public_email,
          key_metrics = campaigns.key_metrics,
          uuid = campaigns.uuid
      FROM campaigns
      WHERE campaigns.id = offerings.campaign_id;
EOS

    connection.execute sql
  end

  def switch_campaign_id_to_offering_id(table)
    add_column table, :offering_id, :integer
    sql =<<-EOS
      UPDATE #{table}
      SET offering_id = offerings.id
      FROM offerings
      WHERE #{table}.campaign_id = offerings.campaign_id
EOS
    connection.execute sql
  end

  def remove_offering_id(table)
    remove_column table, :offering_id
  end
end
