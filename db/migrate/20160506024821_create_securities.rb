require 'offering'
class Offering < ActiveRecord::Base
  has_many :old_deal_documents, as: :documentable, class_name: 'DealDocument'
  has_many :old_spv_deal_documents, as: :documentable, class_name: 'SpvDealDocument'

  enum security_type_id: { security_custom_equity: 1, security_fsp: 2, security_fsp_convertible: 3, security_custom_convertible: 4, security_fsp_llc: 5, security_custom_llc: 6 }

  store_accessor :custom_convertible_details,
                 :other_security_type_title,
                 :convertible_offering_valuation_cap,
                 :convertible_offering_valuation_cap_display,
                 :other_security_type_title_plural,
                 :convertible_offering_discount_rate,
                 :convertible_offering_interest_rate
end

class FundAmericaRequest < ActiveRecord::Base
end

class CreateSecurities < ActiveRecord::Migration
  def up
    ActiveRecord::Base.record_timestamps = false

    create_table :securities do |t|
      t.string :type
      t.boolean :is_spv
      t.references :offering
      t.references :escrow_provider
      t.references :offering_funding_detail_domestic
      t.references :offering_funding_detail_international
      t.integer :shares_offered
      t.decimal :minimum_total_investment
      t.string :minimum_total_investment_display
      t.decimal :maximum_total_investment
      t.string :maximum_total_investment_display
      t.decimal :minimum_investment_amount
      t.string :minimum_investment_amount_display
      t.decimal :external_investment_amount, default: 0
      t.integer :external_investor_count, default: 0
      t.string :security_title
      t.string :security_title_plural
      t.string :docusign_template_id
      t.string :docusign_international_template_id
      t.string :fund_america_id
      t.integer :regulation, default: 1
      t.hstore :custom_data
    end

    add_index :securities, :offering_id

    add_column :investments, :security_id, :integer
    add_index :investments, :security_id

    add_column :fund_america_requests, :security_id, :integer

    move_security_data
    move_fa_requests
    move_documents
    reference_security_from_investment

    ActiveRecord::Base.record_timestamps = true
  end

  def down
    unmove_documents

    drop_table :securities

    remove_column :investments, :security_id
    remove_column :fund_america_requests, :security_id
  end

  private

  def reference_security_from_investment
    Offering.includes(:investments, :securities).all.each do |offering|
      other_security = offering.securities.detect { |s| s.is_spv? || s.is_a?(Securities::CfStock) }
      reg_d_security = offering.securities.detect { |s| s != other_security }

      offering.investments.each do |investment|
        if offering.reg_c_enabled?
          if investment.reg_selector == 'reg-c'
            investment.security_id = other_security.id
          else
            investment.security_id = reg_d_security.id
          end

        elsif offering.offering_model == 2

          if investment.amount < offering.direct_investment_threshold.to_i
            investment.security_id = other_security.id
          else
            investment.security_id = reg_d_security.id
          end

        else
          investment.security_id = reg_d_security.id
        end

        investment.save!
      end
    end
  end

  def move_security_data
    Offering.where('security_type_id IS NOT NULL').all.each do |offering|
      primary_security = securitize_primary(offering)
      other_security = if offering.reg_c_enabled?
                         securitize_cf(offering)
                       elsif offering.offering_model == 2
                         securitize_spv(offering)
                       end

      other_security.save! if other_security
      primary_security.save!
    end
  end

  def move_fa_requests
    FundAmericaRequest.all.each do |fa|
      offering = Offering.find(fa.offering_id)
      fa_offering_id = fa.response['offering_create']['id']
      next unless fa_offering_id.present?

      if (offering.reg_c_enabled? || offering.offering_model == 2) && fa_offering_id == offering.attributes['fund_america_spv_id']
        fa.security_id = offering.other_security.id
      elsif fa_offering_id == offering.attributes['fund_america_id']
        fa.security_id = offering.reg_d_direct_security.id
      end

      fa.save!
    end
  end

  def unmove_documents
    DealDocument.
        joins("INNER JOIN securities ON securities.id = documentable_id").
        where("documentable_type like 'Securities::%'").
        where("securities.is_spv = ? OR securities.regulation = ?", true, Securities::Security.regulations[:reg_cf]).
        update_all("type = 'SpvDealDocument'")

    sql =<<-EOS
    UPDATE documents SET documentable_type = 'Offering', documentable_id = securities.offering_id
    FROM securities
    WHERE documents.documentable_id = securities.id
      AND documents.documentable_type like 'Securities::%'
      AND documents.type IN ('DealDocument', 'SpvDealDocument')
EOS

    DealDocument.connection.execute(sql)
  end

  def move_documents
    Offering.includes(:securities, :old_deal_documents, :old_spv_deal_documents).all.each do |offering|
      other_security = offering.securities.detect { |s| s.is_spv? || s.reg_cf? }
      reg_d_security = offering.securities.detect { |s| s != other_security }

      offering.old_deal_documents.each do |doc|
        Document.where(id: doc.id).update_all("documentable_type = 'Securities::Security', documentable_id = #{reg_d_security.id}")
      end

      if other_security # Need this because there is some bad data where offerings have spv deal docs even though they aren't spv offerings
        offering.old_spv_deal_documents.each do |doc|
          Document.where(id: doc.id).update_all("documentable_type = 'Securities::Security', documentable_id = #{other_security.id}")
        end
      end
    end

    Document.where("type = 'SpvDealDocument'").where("documentable_type = 'Securities::Security'").update_all("type = 'DealDocument'")
  end

  def securitize_primary(offering)
    klass = security_to_class(offering)

    security = klass.new(
        offering_id: offering.id,
        escrow_provider_id: offering.attributes['escrow_provider_id'],
        offering_funding_detail_domestic_id: offering.attributes['offering_funding_detail_direct_id'],
        offering_funding_detail_international_id: offering.attributes['offering_funding_detail_direct_international_id'],
        shares_offered: offering.attributes['seed_shares_offered'],
        minimum_total_investment: offering.attributes['seed_minimum_total_investment'],
        minimum_total_investment_display: offering.attributes['seed_minimum_total_investment_display'],
        maximum_total_investment: offering.attributes['seed_maximum_total_investment'],
        maximum_total_investment_display: offering.attributes['seed_maximum_total_investment_display'],
        # This should be the spv direct investment threshold for model_direct_spv offerings
        minimum_investment_amount: offering.attributes['seed_minimum_investment_amount'],
        minimum_investment_amount_display: offering.attributes['seed_minimum_investment_amount_display'],
        external_investment_amount: offering.attributes['seed_external_investment_amount'],
        external_investor_count: offering.attributes['seed_external_investor_count'],
        security_title: offering.attributes['security_type_custom'],
        docusign_template_id: offering.attributes['docusign_template_id'],
        docusign_international_template_id: offering.attributes['docusign_international_template_id'],
        fund_america_id: offering.attributes['fund_america_id']
    )

    security.regulation = Securities::Security.regulations[:reg_d]

    if security.is_a?(Securities::Convertible)
      security.security_title = offering.custom_convertible_details['other_security_type_title']
      security.security_title_plural = offering.custom_convertible_details['other_security_type_title_plural']
      security.valuation_cap = offering.custom_convertible_details['convertible_offering_valuation_cap']
      security.valuation_cap_display = offering.custom_convertible_details['convertible_offering_valuation_cap_display']
      security.discount_rate = offering.custom_convertible_details['convertible_offering_discount_rate']
      security.interest_rate = offering.custom_convertible_details['convertible_offering_interest_rate']
    end

    security
  end

  def securitize_spv(offering)
    klass = security_to_class(offering)

    security = klass.new(
        offering_id: offering.id,
        escrow_provider_id: offering.attributes['escrow_provider_id'],
        offering_funding_detail_domestic_id: offering.attributes['offering_funding_detail_spv_id'],
        offering_funding_detail_international_id: offering.attributes['offering_funding_detail_spv_international_id'],
        shares_offered: offering.attributes['seed_shares_offered'],
        minimum_total_investment: offering.attributes['spv_minimum_goal'],
        minimum_total_investment_display: "$#{offering.spv_minimum_goal.to_i}",
        maximum_total_investment: offering.attributes['seed_maximum_total_investment'],
        maximum_total_investment_display: offering.attributes['seed_maximum_total_investment_display'],
        minimum_investment_amount: offering.attributes['seed_minimum_investment_amount'],
        minimum_investment_amount_display: offering.attributes['seed_minimum_investment_amount_display'],
        security_title: offering.attributes['security_type_custom'],
        external_investment_amount: 0,
        external_investor_count: 0,
        docusign_template_id: offering.attributes['spv_docusign_template_id'],
        docusign_international_template_id: offering.attributes['spv_docusign_international_template_id'],
        fund_america_id: offering.attributes['fund_america_spv_id']
    )

    security.regulation = Securities::Security.regulations[:reg_d]
    security.is_spv = true

    if security.is_a?(Securities::Convertible)
      security.security_title = offering.custom_convertible_details['other_security_type_title']
      security.security_title_plural = offering.custom_convertible_details['other_security_type_title_plural']
      security.valuation_cap = offering.custom_convertible_details['convertible_offering_valuation_cap']
      security.valuation_cap_display = offering.custom_convertible_details['convertible_offering_valuation_cap_display']
      security.discount_rate = offering.custom_convertible_details['convertible_offering_discount_rate']
      security.interest_rate = offering.custom_convertible_details['convertible_offering_interest_rate']
    end

    security
  end

  def securitize_cf(offering)
    klass = Securities::CfStock

    security = klass.new(
        offering_id: offering.id,
        escrow_provider_id: offering.attributes['escrow_provider_id'],
        offering_funding_detail_domestic_id: offering.attributes['offering_funding_detail_spv_id'],
        offering_funding_detail_international_id: offering.attributes['offering_funding_detail_spv_international_id'],
        shares_offered: offering.attributes['cf_shares_offered'],
        minimum_total_investment: offering.attributes['cf_minimum_total_investment'],
        minimum_total_investment_display: offering.attributes['cf_minimum_total_investment_display'],
        maximum_total_investment: offering.attributes['cf_maximum_total_investment'],
        maximum_total_investment_display: offering.attributes['cf_maximum_total_investment_display'],
        minimum_investment_amount: offering.attributes['cf_minimum_investment_amount'],
        minimum_investment_amount_display: offering.attributes['cf_minimum_investment_amount_display'],
        security_title: offering.attributes['security_type_custom'],
        external_investment_amount: offering.attributes['cf_external_investment_amount'],
        external_investor_count: offering.attributes['cf_external_investor_count'],
        docusign_template_id: offering.attributes['spv_docusign_template_id'],
        docusign_international_template_id: offering.attributes['spv_docusign_international_template_id'],
        fund_america_id: offering.attributes['fund_america_spv_id']
    )

    security.regulation = Securities::Security.regulations[:reg_cf]
    security
  end

  def security_to_class(offering)
    case
      when offering.security_custom_equity?
        Securities::CustomStock
      when offering.security_fsp?
        Securities::FspStock
      when offering.security_fsp_convertible?
        Securities::FspConvertible
      when offering.security_custom_convertible?
        Securities::CustomConvertible
      when offering.security_fsp_llc?
        Securities::FspLlc
      when offering.security_custom_llc?
        Securities::CustomLlc
    end
  end
end

