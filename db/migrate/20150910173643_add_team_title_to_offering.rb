class AddTeamTitleToOffering < ActiveRecord::Migration
  def change
    add_column :offerings, :first_team_title, :string, default: nil
    add_column :offerings, :second_team_title, :string, default: nil
  end
end
