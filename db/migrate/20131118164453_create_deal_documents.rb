class CreateDealDocuments < ActiveRecord::Migration
  def change
    create_table :deal_documents do |t|
      t.string :name
      t.string :file
      t.references :campaign

      t.timestamps
    end
  end
end
