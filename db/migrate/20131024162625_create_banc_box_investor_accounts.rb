class CreateBancBoxInvestorAccounts < ActiveRecord::Migration
  def change
    create_table :banc_box_investor_accounts do |t|
      t.integer :banc_box_id
      t.references :user
    end
  end
end
