class CreatePitchContentItemsTable < ActiveRecord::Migration
  def change
    create_table :pitch_content_items do |t|
      t.references :section
      t.references :content, polymorphic: true
      t.integer :rank

      t.timestamps
    end

    add_index :pitch_content_items, :section_id
  end
end

