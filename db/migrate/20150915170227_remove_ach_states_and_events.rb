class RemoveAchStatesAndEvents < ActiveRecord::Migration
  def up
    Investment.transaction do
      Investment.where(state: [ :failed_ach_debit, :initiated_ach_debit ]).each do |i|
        i.update_attributes! state: :funds_sent, funding_method: :ach
      end

      Investment.where(state: :fund_escrow_failed).find_each do |i|
        i.update_attributes! state: :signed_documents
      end
    end
  end
end
