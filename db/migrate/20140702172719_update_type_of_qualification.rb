class UpdateTypeOfQualification < ActiveRecord::Migration
  def up
    Accreditor::Qualification.where("type like 'Accreditation::%'").update_all("type = overlay(type placing 'Accreditor' from 1 for 13)")
  end
  def down
    Accreditor::Qualification.where("type like 'Accreditor::%'").update_all("type = overlay(type placing 'Accreditation' from 1 for 10)")
  end
end
