class ChangeTeamUrlsToTypeText < ActiveRecord::Migration
  def up
    change_column :stakeholders, :facebook_url, :text
    change_column :stakeholders, :website, :text
    change_column :stakeholders, :linkedin_url, :text
    change_column :stakeholders, :twitter_url, :text
  end

  def down
    change_column :stakeholders, :facebook_url, :string
    change_column :stakeholders, :website, :string
    change_column :stakeholders, :linkedin_url, :string
    change_column :stakeholders, :twitter_url, :string
  end
end
