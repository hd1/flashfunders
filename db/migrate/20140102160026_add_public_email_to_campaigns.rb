class AddPublicEmailToCampaigns < ActiveRecord::Migration
  def change
    add_column :campaigns, :public_email, :string
  end
end
