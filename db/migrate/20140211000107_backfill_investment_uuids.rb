class BackfillInvestmentUuids < ActiveRecord::Migration
  def up
    Investment.find_each do |investment|
      investment.update_attributes!(uuid: SecureRandom.uuid)
    end
  end

  def down

  end
end
