class AddInvestmentColumnToBankActivity < ActiveRecord::Migration
  def change
    add_column :bank_activities, :investment_id, :integer
    remove_column :bank_activities, :user_id, :integer
  end
end
