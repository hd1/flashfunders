class AddHeaderPhotoToOfferings < ActiveRecord::Migration
  def change
    add_column :offerings, :photo_header, :string
  end
end
