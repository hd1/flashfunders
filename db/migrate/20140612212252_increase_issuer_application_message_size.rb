class IncreaseIssuerApplicationMessageSize < ActiveRecord::Migration
  def change
    change_column :issuer_applications, :message, :text
  end
end
