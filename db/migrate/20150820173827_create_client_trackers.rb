class CreateClientTrackers < ActiveRecord::Migration
  def change
    create_table :client_trackers do |t|
      t.references :trackable, polymorphic: true, index: true
      t.string :client_id
    end
  end
end
