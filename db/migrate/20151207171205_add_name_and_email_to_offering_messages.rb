class AddNameAndEmailToOfferingMessages < ActiveRecord::Migration
  def change
    add_column :offering_messages, :name, :string
    add_column :offering_messages, :email, :string
  end
end
