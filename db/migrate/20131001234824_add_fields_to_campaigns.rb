class AddFieldsToCampaigns < ActiveRecord::Migration
  def change
    add_column :campaigns, :video, :string
    add_column :campaigns, :elevator_pitch, :string
    add_column :campaigns, :pitch, :text
  end
end
