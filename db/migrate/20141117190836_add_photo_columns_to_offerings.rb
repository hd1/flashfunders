class AddPhotoColumnsToOfferings < ActiveRecord::Migration
  def change
    add_column :offerings, :photo_cover, :string
    add_column :offerings, :photo_logo, :string
    add_column :offerings, :photo_swatch, :string
  end
end
