class AddPitchImagesTable < ActiveRecord::Migration
  def change
    create_table :pitch_images do |t|
      t.string :photo
      t.boolean :expandable

      t.timestamps
    end
  end
end
