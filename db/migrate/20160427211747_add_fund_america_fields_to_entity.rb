class AddFundAmericaFieldsToEntity < ActiveRecord::Migration
  def change
    add_column :investor_entities, :fund_america_id, :string
    add_column :investor_entities, :fund_america_ach_authorization_id, :string
  end
end
