class AddUuidToInvestments < ActiveRecord::Migration
  def change
    add_column :investments, :uuid, :string
  end
end
