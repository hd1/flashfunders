class UpdateUserTypeToUsers < ActiveRecord::Migration
  def change
    User.transaction do
      User.where(user_type: nil).each do |u|
        if Offering.where(issuer_user_id: u.id).present? || IssuerApplication.where(user_id: u.id).present?
          u.update_attributes(user_type: 2)
        else
          u.update_attributes(user_type: 1)
        end
      end
    end
  end
end
