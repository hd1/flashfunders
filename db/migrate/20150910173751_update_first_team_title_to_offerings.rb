class UpdateFirstTeamTitleToOfferings < ActiveRecord::Migration
  def up
    sql = <<-EOS
    UPDATE offerings
    SET first_team_title = 'The Management Team'
    WHERE id = 61
    EOS
    Offering.connection.execute(sql)
  end

  def down
    sql = <<-EOS
    UPDATE offerings
    SET first_team_title = nil
    WHERE id = 61
    EOS
    Offering.connection.execute(sql)
  end
end
