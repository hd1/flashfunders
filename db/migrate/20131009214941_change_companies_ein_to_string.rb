class ChangeCompaniesEinToString < ActiveRecord::Migration
  def change
    change_column :companies, :ein, :string
  end
end
