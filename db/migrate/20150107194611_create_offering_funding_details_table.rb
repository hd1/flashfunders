class CreateOfferingFundingDetailsTable < ActiveRecord::Migration
  def change
    create_table :offering_funding_details do |t|
      t.references :offering
      t.string :check_payable_to
      t.string :check_memo
      t.references :check_address
      t.references :wire_bank_account
      t.references :wire_bank_address
      t.references :wire_beneficiary_address
      t.string :wire_instructions

      t.timestamp
    end
  end
end
