class BackfillCampaignUuids < ActiveRecord::Migration
  def up
    Offering.find_each do |offering|
      offering.update_attributes!(uuid: SecureRandom.uuid)
    end
  end

  def down

  end

end
