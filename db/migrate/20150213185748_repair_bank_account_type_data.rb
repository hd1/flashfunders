class RepairBankAccountTypeData < ActiveRecord::Migration
  def up
    execute <<-SQL
      UPDATE bank_accounts
        SET type = 'Bank::WireAccount'
        WHERE user_id ISNULL and type ISNULL
    SQL
  end

  def down
  end
end
