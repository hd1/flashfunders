class RemoveCampaignsTable < ActiveRecord::Migration
  def up
    remove_column :banc_box_issuer_accounts, :campaign_id
    remove_column :campaign_category_sectors, :campaign_id
    remove_column :campaign_questions, :campaign_id
    remove_column :companies, :campaign_id
    remove_column :documents, :campaign_id
    remove_column :investments, :campaign_id
    remove_column :investor_faqs, :campaign_id
    remove_column :offerings, :campaign_id

    drop_table :campaign_documents
    drop_table :deal_documents
    drop_table :campaigns
  end
end
