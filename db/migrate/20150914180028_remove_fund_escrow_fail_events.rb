class RemoveFundEscrowFailEvents < ActiveRecord::Migration
  def up
    InvestmentEvent.where(type: ['InvestmentEvent::FailFundEscrowEvent', 'InvestmentEvent::FixFundEscrowEvent']).delete_all
  end
end
