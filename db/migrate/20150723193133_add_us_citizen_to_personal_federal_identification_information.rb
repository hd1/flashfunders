class AddUsCitizenToPersonalFederalIdentificationInformation < ActiveRecord::Migration
  class PersonalFederalIdentificationInformation < ActiveRecord::Base
  end

  def up
    add_column :personal_federal_identification_informations, :us_citizen, :boolean, null: false, default: true

    PersonalFederalIdentificationInformation.all.each do |pfii|
      pfii.update_attributes us_citizen: pfii.country == 'US'
    end
  end

  def down
    remove_column :personal_federal_identification_informations, :us_citizen
  end
end