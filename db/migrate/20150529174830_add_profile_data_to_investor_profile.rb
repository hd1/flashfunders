class AddProfileDataToInvestorProfile < ActiveRecord::Migration
  def change
    add_column :investor_profiles, :profile_data, :hstore
  end
end
