class AddPercentOwnershipToInvestments < ActiveRecord::Migration
  def change
    add_column :investments, :percent_ownership, :decimal, precision: 64, scale: 3
  end
end
