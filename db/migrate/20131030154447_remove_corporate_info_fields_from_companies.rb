class RemoveCorporateInfoFieldsFromCompanies < ActiveRecord::Migration
  def up
    remove_column :companies, :ein
    remove_column :companies, :state_id
    remove_column :companies, :corporate_name
  end

  def down
    add_column :companies, :ein, :string
    add_column :companies, :state_id, :integer
    add_column :companies, :corporate_name, :string
  end
end
