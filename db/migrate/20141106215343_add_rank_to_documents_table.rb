class AddRankToDocumentsTable < ActiveRecord::Migration
  def change
    add_column :documents, :rank, :integer
  end
end
