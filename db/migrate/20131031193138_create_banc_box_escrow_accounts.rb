class CreateBancBoxEscrowAccounts < ActiveRecord::Migration
  def change
    create_table :banc_box_escrow_accounts do |t|
      t.references :banc_box_issuer_account
      t.integer :banc_box_id
    end
  end
end
