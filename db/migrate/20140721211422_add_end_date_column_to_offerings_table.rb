class AddEndDateColumnToOfferingsTable < ActiveRecord::Migration
  def up
    add_column :offerings, :end_date, :date

    execute <<-SQL
      UPDATE offerings SET end_date = close_date
    SQL
  end

  def down
    remove_column :offerings, :end_date
  end
end
