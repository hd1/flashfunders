class RemoveFundingFromInvestorProfile < ActiveRecord::Migration
  def change
    remove_column :investor_profiles, :funding_income, :boolean
    remove_column :investor_profiles, :funding_pension, :boolean
    remove_column :investor_profiles, :funding_sales, :boolean
    remove_column :investor_profiles, :funding_inheritance, :boolean
    remove_column :investor_profiles, :funding_home_equity, :boolean
    remove_column :investor_profiles, :funding_other, :boolean
    remove_column :investor_profiles, :funding_other_text, :string
  end
end
