class SetSecurityTypeIdOnOfferings < ActiveRecord::Migration
  def up
    Offering.where(security_type_id: nil).update_all(security_type_id: 2)
  end

  def down
  end
end
