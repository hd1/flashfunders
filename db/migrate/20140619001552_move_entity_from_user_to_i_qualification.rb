class MoveEntityFromUserToIQualification < ActiveRecord::Migration
  def change
    sql = <<-EOS
    UPDATE accreditation_qualifications set investor_entity_id = investor_entities.id
    FROM users INNER JOIN investor_entities ON users.id = investor_entities.user_id
    WHERE accreditation_qualifications.user_id = users.id
    EOS
    Accreditor::Qualification.connection.execute(sql)
  end
end

