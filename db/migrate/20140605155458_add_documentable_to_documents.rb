class AddDocumentableToDocuments < ActiveRecord::Migration
  class Document < ActiveRecord::Base
    default_scope { order(:id) }
  end

  def change
    add_column :documents, :documentable_id, :integer
    add_column :documents, :documentable_type, :string

    Document.update_all("documentable_id = offering_id, documentable_type = 'Offering'")
  end
end
