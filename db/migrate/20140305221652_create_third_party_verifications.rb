class CreateThirdPartyVerifications < ActiveRecord::Migration
  def change
    create_table :third_party_verifications do |t|
      t.uuid :uuid
      t.string :name
      t.integer :investor_id
      t.string :accreditation_type
      t.string :role
      t.string :company_name
      t.string :license_number
      t.string :license_state_id
      t.string :address_1
      t.string :address_2
      t.string :city
      t.integer :state_id
      t.string :zip
      t.string :phone_number
      t.string :email
      t.string :signature
      t.string :status

      t.timestamps
    end
  end
end
