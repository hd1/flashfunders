class AddRankToOffering < ActiveRecord::Migration
  def change
    add_column :offerings, :rank, :integer
  end
end
