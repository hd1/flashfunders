class CreateInvestmentStates < ActiveRecord::Migration
  def change
    create_table :investment_states do |t|
      t.references :investment
      t.string :type
      t.timestamps
    end
  end
end
