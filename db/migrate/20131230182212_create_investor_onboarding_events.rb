class CreateInvestorOnboardingEvents < ActiveRecord::Migration
  def change
    create_table :investor_onboarding_events do |t|
      t.references :user
      t.string :type
      t.timestamps
    end
  end
end
