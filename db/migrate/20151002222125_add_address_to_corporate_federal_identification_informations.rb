class AddAddressToCorporateFederalIdentificationInformations < ActiveRecord::Migration
  def change
    add_column :corporate_federal_identification_informations, :address_id, :integer
    add_column :corporate_federal_identification_informations, :executive_name, :string
    add_column :corporate_federal_identification_informations, :state_formed_in, :string
  end
end
