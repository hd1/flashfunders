class AddSsnToPersonalFederalIdentificationInformations < ActiveRecord::Migration
  def change
    add_column :personal_federal_identification_informations, :encrypted_ssn, :text
  end
end
