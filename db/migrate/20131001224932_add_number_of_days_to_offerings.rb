class AddNumberOfDaysToOfferings < ActiveRecord::Migration
  def change
    add_column :offerings, :number_of_days, :string
  end
end
