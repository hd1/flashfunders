class AlterInvestmentsChangeAmountAndPercentOwnershipToDecimal < ActiveRecord::Migration
  def up
    change_column :investments, :amount, :decimal
    change_column :investments, :percent_ownership, :decimal
  end

  def down
    change_column :investments, :percent_ownership, :decimal, precision: 64, scale: 3
    change_column :investments, :amount, :float
  end
end
