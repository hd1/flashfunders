class CreatePitchTextAreasTable < ActiveRecord::Migration
  def change
    create_table :pitch_text_areas do |t|
      t.text :body
      t.boolean :html, default: false

      t.timestamps
    end
  end
end
