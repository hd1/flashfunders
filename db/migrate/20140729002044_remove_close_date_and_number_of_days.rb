class RemoveCloseDateAndNumberOfDays < ActiveRecord::Migration
  def change
    remove_column :offerings, :close_date
    remove_column :offerings, :number_of_days
  end
end
