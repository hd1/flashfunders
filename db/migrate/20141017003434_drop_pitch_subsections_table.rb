require_relative '20140925002712_create_pitch_subsections'

class DropPitchSubsectionsTable < ActiveRecord::Migration
  def change
    revert CreatePitchSubsections
  end
end
