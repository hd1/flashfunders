class AddFacebookAndTwitterUrlToCompanies < ActiveRecord::Migration
  def change
    add_column :companies, :twitter_url, :string
    add_column :companies, :facebook_url, :string
  end
end
