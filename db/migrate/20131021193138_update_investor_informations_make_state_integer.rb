class UpdateInvestorInformationsMakeStateInteger < ActiveRecord::Migration
  def up
    remove_column :investor_informations, :state
    add_column :investor_informations, :state_id, :integer
  end

  def down
    remove_column :investor_informations, :state_id
    add_column :investor_informations, :state, :string
  end
end
