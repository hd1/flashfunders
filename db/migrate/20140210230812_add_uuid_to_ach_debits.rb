class AddUuidToAchDebits < ActiveRecord::Migration
  def change
    add_column :ach_debits, :uuid, :string
  end
end
