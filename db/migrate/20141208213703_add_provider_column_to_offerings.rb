class AddProviderColumnToOfferings < ActiveRecord::Migration
  def up
    add_column :offerings, :provider, :integer, default: 1
  end
  def down
    remove_column :offerings, :provider 
  end
end
