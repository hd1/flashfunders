class AddIndexToInvestments < ActiveRecord::Migration
  def change
    add_index(:investments, :offering_id)
  end
end
