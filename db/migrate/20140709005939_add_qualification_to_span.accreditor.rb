# This migration comes from accreditor (originally 20140708175925)
class AddQualificationToSpan < ActiveRecord::Migration
  def change
    add_column :accreditor_spans, :accreditor_qualification_id, :integer
  end
end
