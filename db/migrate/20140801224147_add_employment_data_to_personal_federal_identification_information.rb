class AddEmploymentDataToPersonalFederalIdentificationInformation < ActiveRecord::Migration
  def change
    add_column :personal_federal_identification_informations, :employment_data, :hstore
  end
end
