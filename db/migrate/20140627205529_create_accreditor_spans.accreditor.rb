# This migration comes from accreditor (originally 20140627180121)
class CreateAccreditorSpans < ActiveRecord::Migration
  def change
    rename_table :accreditation_spans, :accreditor_spans
    rename_column :accreditor_spans, :status_id, :status
  end
end
