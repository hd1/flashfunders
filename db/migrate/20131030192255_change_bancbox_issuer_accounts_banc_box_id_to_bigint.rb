class ChangeBancboxIssuerAccountsBancBoxIdToBigint < ActiveRecord::Migration
  def up
    change_column :banc_box_issuer_accounts, :banc_box_id, :bigint
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
