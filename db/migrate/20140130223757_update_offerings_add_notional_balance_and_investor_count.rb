class UpdateOfferingsAddNotionalBalanceAndInvestorCount < ActiveRecord::Migration
  def change
    add_column :offerings, :notional_balance, :string
    add_column :offerings, :investor_count, :integer
  end
end
