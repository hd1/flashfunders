class ChangeOfferingsAmountPreviouslyRaisedToString < ActiveRecord::Migration
  def change
    change_column :offerings, :amount_previously_raised, :string
  end
end
