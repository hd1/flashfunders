class AddTimestampsToInvestment < ActiveRecord::Migration
  def up
    add_column :investments, :created_at, :datetime
    add_column :investments, :updated_at, :datetime

    now = Time.now.utc.strftime('%Y-%m-%d %l:%M:%S')

    execute <<-SQL
UPDATE investments SET created_at = '#{now}';
UPDATE investments SET updated_at = '#{now}';
    SQL
  end

  def down
    remove_column :investments, :created_at
    remove_column :investments, :updated_at
  end
end
