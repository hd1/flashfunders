class AddForeignKeyAndUniquenessConstraintToInvestorSuitabilityQuestionnairesUserId < ActiveRecord::Migration
    def up
      execute <<-SQL
        ALTER TABLE investor_suitability_questionnaires
          ADD CONSTRAINT fk_investor_suitability_questionnaire_users
          FOREIGN KEY (user_id)
          REFERENCES users(id)
      SQL

      execute <<-SQL
        ALTER TABLE investor_suitability_questionnaires
          ADD CONSTRAINT unique_investor_suitability_questionnaire_user_id
          UNIQUE (user_id)
      SQL
    end

    def down
      execute <<-SQL
        ALTER TABLE investor_suitability_questionnaires
          DROP CONSTRAINT fk_investor_suitability_questionnaire_users
      SQL
      execute <<-SQL
        ALTER TABLE investor_suitability_questionnaires
          DROP CONSTRAINT unique_investor_suitability_questionnaire_user_id
      SQL
    end
end
