class AddDocusignEmailToInvestment < ActiveRecord::Migration
  def up
    add_column :investments, :envelope_email, :string

    execute <<-EOS
      UPDATE investments
      SET envelope_email = users.email
      FROM users
      WHERE investments.user_id = users.id
    EOS
  end

  def down
    remove_column :investments, :envelope_email, :string
  end
end
