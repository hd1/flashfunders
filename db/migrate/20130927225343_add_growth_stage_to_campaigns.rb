class AddGrowthStageToCampaigns < ActiveRecord::Migration
  def change
    add_column :campaigns, :growth_stage_id, :integer
  end
end
