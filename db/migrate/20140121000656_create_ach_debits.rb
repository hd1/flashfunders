class CreateAchDebits < ActiveRecord::Migration
  def change
    create_table :ach_debits do |t|
      t.references :user
      t.decimal :amount
      t.timestamps
    end
  end
end
