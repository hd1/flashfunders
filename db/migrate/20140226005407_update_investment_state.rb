class UpdateInvestmentState < ActiveRecord::Migration
  def up
    InvestmentEvent.where(id: InvestmentEvent.select('max(id) as id').group(:investment_id).map(&:id)).all.each do |event|
      investment = Investment.find(event.investment_id) rescue nil
      if investment
        state = case event.class.to_s
                  when 'InvestmentEvent::BeginEvent'
                    'intended'
                  when 'InvestmentEvent::AcceptInvestmentEvent'
                    'agreed_to_investment'
                  when 'InvestmentEvent::SignDocumentsEvent'
                    'signed_documents'
                  when 'InvestmentEvent::LinkRefundAccountEvent'
                    'linked_refund_account'
                  when 'InvestmentEvent::InitiateAchDebitEvent'
                    'initiated_ach_debit'
                  when 'InvestmentEvent::AchDebitFailureEvent'
                    'failed_ach_debit'
                  when 'InvestmentEvent::TransferFundsEvent'
                    'escrowed'
                  when 'InvestmentEvent::AllowFundingEvent'
                    'linked_refund_account'
                  else
                    'started'
                end

        investment.update_attribute :state, state
      end
    end
  end
end
