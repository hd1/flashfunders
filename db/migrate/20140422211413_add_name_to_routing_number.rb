class AddNameToRoutingNumber < ActiveRecord::Migration
  def change
    add_column :routing_numbers, :name, :string
  end
end
