class AddAccreditationStatusToInvestorEntity < ActiveRecord::Migration
  def change
    add_column :investor_entities, :accreditation_status, :integer, default: 0
  end
end
