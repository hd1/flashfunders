class AddEntityIdToQualifications < ActiveRecord::Migration
  def change
    add_column :accreditation_qualifications, :investor_entity_id, :integer
  end
end
