class AddForeignKeyAndUniquenessConstraintToBancBoxInvestorAccountsUserId < ActiveRecord::Migration
  def up
    execute <<-SQL
      ALTER TABLE banc_box_investor_accounts
        ADD CONSTRAINT fk_banc_box_investor_account_user
        FOREIGN KEY (user_id)
        REFERENCES users(id)
    SQL

    execute <<-SQL
      ALTER TABLE banc_box_investor_accounts
        ADD CONSTRAINT unique_banc_box_investor_account_user_id
        UNIQUE (user_id)
    SQL
  end

  def down
    execute <<-SQL
      ALTER TABLE banc_box_investor_accounts
        DROP CONSTRAINT fk_banc_box_investor_account_user
    SQL
    execute <<-SQL
      ALTER TABLE banc_box_investor_accounts
        DROP CONSTRAINT unique_banc_box_investor_account_user_id
    SQL
  end
end
