class RemoveIncorrectFksOnInvestorSuitabilityQuestionnaires < ActiveRecord::Migration
  def change
    remove_column :investor_suitability_questionnaires, :category_sector_id, :integer
    remove_column :investor_suitability_questionnaires, :investment_risk_level_id, :integer
  end
end
