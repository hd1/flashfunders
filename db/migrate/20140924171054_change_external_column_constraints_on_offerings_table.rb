class ChangeExternalColumnConstraintsOnOfferingsTable < ActiveRecord::Migration
  def up
    change_column :offerings, :external_investment_amount, :decimal, precision: 14, scale: 2, default: 0
  end

  def down
    change_column :offerings, :external_investment_amount, :decimal, precision: 8, scale: 2, default: 0
  end
end
