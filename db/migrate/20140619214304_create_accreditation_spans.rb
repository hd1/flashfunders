class CreateAccreditationSpans < ActiveRecord::Migration
  def change
    create_table :accreditation_spans do |t|
      t.references :investor_entity, index: true
      t.integer :status_id
      t.date :start_date
      t.date :end_date

      t.timestamps
    end
  end
end
