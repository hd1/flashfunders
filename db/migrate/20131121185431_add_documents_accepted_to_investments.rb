class AddDocumentsAcceptedToInvestments < ActiveRecord::Migration
  def change
    add_column :investments, :documents_accepted, :boolean, default: false
  end
end
