class AddIntlTemplatesToOfferings < ActiveRecord::Migration
  def change
    add_column :offerings, :docusign_international_template_id, :string, limit: 255
    add_column :offerings, :spv_docusign_international_template_id, :string, limit: 255
  end
end
