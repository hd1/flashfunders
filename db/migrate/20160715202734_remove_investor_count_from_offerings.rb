class RemoveInvestorCountFromOfferings < ActiveRecord::Migration
  def change
    remove_column :offerings, :investor_count, :integer
  end
end
