class AddTypeToBankAccounts < ActiveRecord::Migration
  def up
    add_column :bank_accounts, :type, :string

    Bank::Account.update_all(type: 'Bank::InternalAccount')
  end

  def down
    remove_column :bank_accounts, :type
  end
end
