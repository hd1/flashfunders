class AddAuthorizedToInvestStatusToUsers < ActiveRecord::Migration
  def change
    add_column :users, :authorized_to_invest_status, :integer, null: false, default: 1
  end
end
