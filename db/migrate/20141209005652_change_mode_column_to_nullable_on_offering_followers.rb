class ChangeModeColumnToNullableOnOfferingFollowers < ActiveRecord::Migration
  def up
    change_column :offering_followers, :mode, :integer, null: true, default: nil
  end

  def down
    change_column :offering_followers, :mode, :integer, null: false, default: 1
  end
end
