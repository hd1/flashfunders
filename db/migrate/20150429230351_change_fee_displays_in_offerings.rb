class ChangeFeeDisplaysInOfferings < ActiveRecord::Migration
  def change
    rename_column :offerings, :setup_fee_percentage_display, :setup_fee_display
    remove_columns :offerings, :management_fee_display, :commission_fee_display
  end
end
