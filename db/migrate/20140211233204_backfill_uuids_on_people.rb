class BackfillUuidsOnPeople < ActiveRecord::Migration
  class Person < ActiveRecord::Base; end

  def up
    Person.find_each do |person|
      person.update_attributes!(uuid: SecureRandom.uuid)
    end
  end

  def down
  end

end
