class AddEntityIdToInvestments < ActiveRecord::Migration
  def change
    add_column :investments, :investor_entity_id, :integer
  end
end
