class AddPersonalPortfolioPercentageFieldsToInvestorSuitabilityQuestionnaires < ActiveRecord::Migration
  def change
    add_column :investor_suitability_questionnaires, :personal_portfolio_traditional_investment_percentage_id, :integer
    add_column :investor_suitability_questionnaires, :personal_portfolio_sophisticated_investment_percentage_id, :integer
  end
end
