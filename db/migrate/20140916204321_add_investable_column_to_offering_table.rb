class AddInvestableColumnToOfferingTable < ActiveRecord::Migration
  def change
    add_column :offerings, :investable, :boolean, default: true
  end
end
