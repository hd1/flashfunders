class AddInvestmentEmailsTable < ActiveRecord::Migration
  def change
    create_table :investment_emails do |t|
      t.integer :offering_id
      t.string :type
      t.timestamp :queued_at
      t.timestamp :sent_at
      t.json :data
    end
  end
end
