class AddDashboardLastViewedAtToOfferings < ActiveRecord::Migration
  def change
    add_column :offerings, :dashboard_last_viewed_at, :timestamp
  end
end
