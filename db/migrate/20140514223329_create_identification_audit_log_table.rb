class CreateIdentificationAuditLogTable < ActiveRecord::Migration
  def change
    create_table :identification_audit_logs do |t|
      t.integer :user_id
      t.integer :identification_status_id
      t.string :created_by

      t.timestamps
    end
  end
end
