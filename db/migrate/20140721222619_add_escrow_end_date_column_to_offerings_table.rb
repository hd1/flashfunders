class AddEscrowEndDateColumnToOfferingsTable < ActiveRecord::Migration
  def change
    add_column :offerings, :escrow_end_date, :date
  end
end
