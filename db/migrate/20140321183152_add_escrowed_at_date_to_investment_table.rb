class AddEscrowedAtDateToInvestmentTable < ActiveRecord::Migration
  def change
    add_column :investments, :escrowed_at, :datetime
  end
end
