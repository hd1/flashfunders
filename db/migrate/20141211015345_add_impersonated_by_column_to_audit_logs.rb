class AddImpersonatedByColumnToAuditLogs < ActiveRecord::Migration
  def change
    add_column :audit_log_entries, :impersonated_by, :integer
  end
end
