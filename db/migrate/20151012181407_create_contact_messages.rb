class CreateContactMessages < ActiveRecord::Migration
  def change
    create_table :contact_messages do |t|
      t.integer :user_id, null: true
      t.string :email
      t.string :name
      t.string :kind
      t.text :message

      t.timestamps null: false
    end
  end
end
