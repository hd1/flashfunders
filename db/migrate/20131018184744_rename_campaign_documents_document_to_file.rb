class RenameCampaignDocumentsDocumentToFile < ActiveRecord::Migration
  def change
    rename_column :campaign_documents, :document, :file
  end
end
