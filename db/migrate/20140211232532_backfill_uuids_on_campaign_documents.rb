class BackfillUuidsOnCampaignDocuments < ActiveRecord::Migration
  class FakeClass < ActiveRecord::Base; self.table_name = :campaign_documents; end

  def up
    FakeClass.find_each do |campaign_document|
      campaign_document.update_attributes!(uuid: SecureRandom.uuid)
    end
  end

  def down
  end

end
