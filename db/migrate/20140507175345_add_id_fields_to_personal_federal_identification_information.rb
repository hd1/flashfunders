class AddIdFieldsToPersonalFederalIdentificationInformation < ActiveRecord::Migration
  def change
    add_column :personal_federal_identification_informations, :id_type, :integer
    add_column :personal_federal_identification_informations, :id_number, :string
    add_column :personal_federal_identification_informations, :location, :string
    add_column :personal_federal_identification_informations, :issued_date, :date
    add_column :personal_federal_identification_informations, :expiration_date, :date
  end
end
