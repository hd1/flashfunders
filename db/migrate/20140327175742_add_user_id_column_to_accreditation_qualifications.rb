class AddUserIdColumnToAccreditationQualifications < ActiveRecord::Migration
  def up
    add_column :accreditation_qualifications, :user_id, :integer

    execute <<-SQL
      UPDATE accreditation_qualifications
        SET user_id = investors.user_id
      FROM investors
      WHERE accreditation_qualifications.investor_id = investors.id
    SQL

    change_column :accreditation_qualifications, :user_id, :integer, null: false
  end

  def down
    remove_column :accreditation_qualifications, :user_id
  end
end
