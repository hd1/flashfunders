class ChangeInvestorSuitabilityQuestionnaires < ActiveRecord::Migration
  def change
    rename_column :investor_suitability_questionnaires, :portfolio_percentage_alternative_assets, :portfolio_alternative_assets_percentage_id
  end
end
