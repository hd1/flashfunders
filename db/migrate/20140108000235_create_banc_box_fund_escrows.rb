class CreateBancBoxFundEscrows < ActiveRecord::Migration
  def change
    create_table :banc_box_fund_escrows do |t|
      t.string :banc_box_funding_schedule_id
      t.references :investment
      t.timestamps
    end
  end
end
