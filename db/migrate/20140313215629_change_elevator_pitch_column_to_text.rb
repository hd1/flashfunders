class ChangeElevatorPitchColumnToText < ActiveRecord::Migration
  def up
    change_column :campaigns, :elevator_pitch, :text
  end

  def down
    change_column :campaigns, :elevator_pitch, :string
  end
end
