class CreateCompanyRelationships < ActiveRecord::Migration
  def change
    create_table :company_relationships do |t|
      t.references :company
      t.references :person
      t.string :relationship_type
      t.string :position
      t.string :ownership_percentage
      t.string :date_joined

      t.timestamps
    end
  end
end
