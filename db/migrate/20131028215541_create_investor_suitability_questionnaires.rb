class CreateInvestorSuitabilityQuestionnaires < ActiveRecord::Migration
  def change
    create_table :investor_suitability_questionnaires do |t|
      t.references :user

      t.integer :category_sector_id
      t.integer :investment_amount_range_id
      t.integer :investment_risk_level_id
      t.boolean :comfortable_with_illiquid_investments
      t.boolean :interested_in_managed_fund_investments
      t.boolean :capable_of_evaluating_illiquid_investments

      t.integer :time_in_traditional_investment_id
      t.integer :time_in_sophisticated_investment_id
      t.integer :portfolio_percentage_alternative_assets
    end
  end
end