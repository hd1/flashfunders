class CreateUtms < ActiveRecord::Migration
  def change
    create_table :utms do |t|
      t.string :client_id
      t.string :source
      t.string :medium
      t.string :campaign
      t.string :term
      t.string :content
      t.string :other

      t.timestamps null: false
    end
  end
end
