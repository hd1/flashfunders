class AddTaglineToCampaigns < ActiveRecord::Migration
  def change
    add_column :campaigns, :tagline, :string
  end
end
