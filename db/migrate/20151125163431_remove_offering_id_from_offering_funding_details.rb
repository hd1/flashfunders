class RemoveOfferingIdFromOfferingFundingDetails < ActiveRecord::Migration
  def change
    remove_column :offering_funding_details, :offering_id, :integer
  end
end
