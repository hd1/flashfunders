class CreateAccreditationDocuments < ActiveRecord::Migration
  def change
    create_table :accreditation_documents do |t|
      t.references :accreditation_qualification, index: true
      t.string :file_url
      t.uuid :uuid

      t.timestamps
    end
  end
end
