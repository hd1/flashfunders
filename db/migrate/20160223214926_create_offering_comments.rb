class CreateOfferingComments < ActiveRecord::Migration
  def change
    create_table :offering_comments do |t|
      t.references :offering, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true
      t.text :body
      t.integer :display_state, default: 1
      t.integer :updated_by_user
      t.references :offering_comments, :parent, index: true

      t.timestamps null: false
    end
  end
end
