class DropSsnLastFourFromPersonalFederalIdentificationInformations < ActiveRecord::Migration
  def change
    remove_column :personal_federal_identification_informations, :ssn_last_four
  end
end
