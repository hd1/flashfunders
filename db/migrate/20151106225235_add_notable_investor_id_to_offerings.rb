class AddNotableInvestorIdToOfferings < ActiveRecord::Migration
  def change
    add_column :offerings, :notable_investor_id, :integer
  end
end
