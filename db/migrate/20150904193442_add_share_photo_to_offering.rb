class AddSharePhotoToOffering < ActiveRecord::Migration
  def change
    add_column :offerings, :share_photo, :string
  end
end
