class CreateOfferingMessages < ActiveRecord::Migration
  def change
    create_table :offering_messages do |t|
      t.integer :offering_id
      t.integer :user_id
      t.text :body
      t.boolean :sent_by_user

      t.timestamps
    end
  end
end
