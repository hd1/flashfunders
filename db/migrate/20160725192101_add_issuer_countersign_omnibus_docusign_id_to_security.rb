class AddIssuerCountersignOmnibusDocusignIdToSecurity < ActiveRecord::Migration
  def change
    add_column :securities, :docusign_issuer_countersign_omnibus_id, :string
  end
end
