class AddVanityPathToOfferings < ActiveRecord::Migration
  def change
    add_column :offerings, :vanity_path, :string
    add_index :offerings, :vanity_path
  end
end
