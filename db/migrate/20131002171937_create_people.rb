class CreatePeople < ActiveRecord::Migration
  def change
    create_table :people do |t|
      t.string :full_name
      t.string :location
      t.text :bio
      t.string :avatar
      t.string :twitter_url
      t.string :facebook_url
      t.string :linkedin_url

      t.timestamps
    end
  end
end
