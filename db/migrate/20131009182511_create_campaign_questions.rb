class CreateCampaignQuestions < ActiveRecord::Migration
  def change
    create_table :campaign_questions do |t|
      t.references :user
      t.references :campaign
      t.text :question_text

      t.timestamps
    end
  end
end
