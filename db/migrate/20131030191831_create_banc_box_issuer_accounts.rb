class CreateBancBoxIssuerAccounts < ActiveRecord::Migration
  def change
    create_table :banc_box_issuer_accounts do |t|
      t.references :user
      t.integer :banc_box_id
    end
  end
end
