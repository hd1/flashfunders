class CreateBankActivitiesTable < ActiveRecord::Migration
  def change
    create_table :bank_activities do |t|
      t.integer :user_id, null: false
      t.string :action
      t.string :direction
      t.decimal :amount, precision: 12, scale: 2
      t.string :method
      t.string :status
      t.uuid :offering_uuid
      t.uuid :activity_uuid
      t.datetime :started_at
      t.datetime :scheduled_at
      t.datetime :completed_at

      t.timestamps
    end
  end
end

