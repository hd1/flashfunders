class RenameInvestorBankAccountToBankAccount < ActiveRecord::Migration
  def change
    rename_table :investor_bank_accounts, :bank_accounts
  end
end
