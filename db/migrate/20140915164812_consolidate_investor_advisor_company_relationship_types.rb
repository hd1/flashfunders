class ConsolidateInvestorAdvisorCompanyRelationshipTypes < ActiveRecord::Migration
  def up
    execute <<-SQL
      UPDATE company_relationships
        SET company_relationship_type_id = 2
      WHERE company_relationship_type_id in (2,3)
    SQL
  end

  def down
    # no going back
  end
end
