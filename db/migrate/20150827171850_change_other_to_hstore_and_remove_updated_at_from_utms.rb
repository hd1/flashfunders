class ChangeOtherToHstoreAndRemoveUpdatedAtFromUtms < ActiveRecord::Migration
  def change
    remove_column :utms, :updated_at, :datetime, null: false
    remove_column :utms, :other, :string
    add_column :utms, :other, :hstore
  end
end
