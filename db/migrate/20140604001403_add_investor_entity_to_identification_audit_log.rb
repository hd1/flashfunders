class AddInvestorEntityToIdentificationAuditLog < ActiveRecord::Migration
  def up
    add_column :identification_audit_logs, :investor_entity_id, :integer

    execute <<-SQL
      UPDATE identification_audit_logs a
        SET investor_entity_id = e.id
      FROM investor_entities as e
      WHERE e.type = 'InvestorEntity::Individual'
      AND e.user_id = a.user_id;
    SQL
  end

  def down
    remove_column :identification_audit_logs, :investor_entity_id
  end
end

