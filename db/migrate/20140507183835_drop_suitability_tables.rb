class DropSuitabilityTables < ActiveRecord::Migration
  def change
    drop_table :investor_suitability_questionnaire_investment_risk_levels
    drop_table :investor_suitability_questionnaire_category_sectors
    drop_table :investor_suitability_questionnaires
  end
end
