class AddSecurityTypeColumnToOffersTable < ActiveRecord::Migration
  def change
    add_column :offerings, :security_type_id, :integer
    add_column :offerings, :security_type_custom, :string
  end
end
