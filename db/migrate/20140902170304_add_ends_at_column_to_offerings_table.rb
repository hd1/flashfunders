class AddEndsAtColumnToOfferingsTable < ActiveRecord::Migration
  def up
    add_column :offerings, :ends_at, :datetime
    execute 'UPDATE offerings SET ends_at = end_date'
  end

  def down
    remove_column :offerings, :ends_at, :datetime
  end
end

