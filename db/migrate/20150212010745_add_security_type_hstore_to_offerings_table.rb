class AddSecurityTypeHstoreToOfferingsTable < ActiveRecord::Migration
  def up
    add_column :offerings, :custom_convertible_details, :hstore
  end

  def down
    remove_column :offerings, :custom_convertible_details
  end
end
