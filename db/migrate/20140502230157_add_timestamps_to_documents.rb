class AddTimestampsToDocuments < ActiveRecord::Migration
  def up
    add_column :documents, :updated_at, :timestamp
    add_column :documents, :created_at, :timestamp

    connection.execute("UPDATE documents SET updated_at = cd.updated_at, created_at = cd.created_at \
                        FROM campaign_documents cd
                        WHERE cd.uuid = documents.uuid")
    connection.execute("UPDATE documents SET updated_at = dd.updated_at, created_at = dd.created_at \
                        FROM deal_documents dd
                        WHERE dd.uuid = documents.uuid")
  end

  def down
    remove_column :documents, :updated_at
    remove_column :documents, :created_at
  end
end
