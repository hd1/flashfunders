class AddUuidToProfiles < ActiveRecord::Migration
  def change
    add_column :profiles, :uuid, :string

    add_index 'profiles', ['uuid'], name: 'unique_profiles_uuid_index', unique: true, using: :btree
  end
end
