class CreateInvestorAccreditationVerification < ActiveRecord::Migration
  def change
    create_table :investor_accreditation_verifications do |t|
      t.references :user
      t.datetime :submitted_accreditation_verification_for_approval

      t.timestamps
    end
  end
end
