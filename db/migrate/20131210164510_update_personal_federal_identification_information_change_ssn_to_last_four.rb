class UpdatePersonalFederalIdentificationInformationChangeSsnToLastFour < ActiveRecord::Migration
  def change
    rename_column :personal_federal_identification_informations, :ssn, :ssn_last_four
  end
end
