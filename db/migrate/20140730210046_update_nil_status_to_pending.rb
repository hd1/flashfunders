class UpdateNilStatusToPending < ActiveRecord::Migration
  def up
    AchDebit.where(status: nil).update_all(status: AchDebit::PENDING)
  end
end
