class AddDocusignTemplateIdToOfferings < ActiveRecord::Migration
  def change
    add_column :offerings, :docusign_template_id, :string
  end
end
