class AddImpersonatorColumnToUsers < ActiveRecord::Migration
  def change
    add_column :users, :impersonator, :boolean, default: false
  end
end
