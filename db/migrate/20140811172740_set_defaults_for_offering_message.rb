class SetDefaultsForOfferingMessage < ActiveRecord::Migration
  def change
    change_column :offering_messages, :sent_by_user, :boolean, default: true
    OfferingMessage.where("sent_by_user IS NULL").each { |m| m.update_attribute :sent_by_user, false }
  end
end
