class AddFollowerModeToOfferingFollowers < ActiveRecord::Migration
  def change
    add_column :offering_followers, :mode, :integer, null: false, default: 1
  end
end
