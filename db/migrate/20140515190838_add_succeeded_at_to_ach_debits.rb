class AddSucceededAtToAchDebits < ActiveRecord::Migration
  def change
    add_column :ach_debits, :succeeded_at, :datetime
  end
end
