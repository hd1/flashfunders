class RemoveOfferingCommentsIdFromOfferingComments < ActiveRecord::Migration
  def change
    remove_column :offering_comments, :offering_comments_id, :integer
  end
end
