class DropTeamMembers < ActiveRecord::Migration
  def up
    drop_table :team_members
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
