class MoveEntityFromUserToInvestment < ActiveRecord::Migration
  def up
    sql = <<-EOS
    UPDATE investments set investor_entity_id = investor_entities.id
    FROM users INNER JOIN investor_entities ON users.id = investor_entities.user_id
    WHERE investments.user_id = users.id
    EOS
    Investment.connection.execute(sql)
  end
end
