class AddIdentificationStatusToInvestmentEntities < ActiveRecord::Migration
  def up
    add_column :investor_entities, :identification_status_id, :integer, null: false, default: 1

    execute <<-SQL
      UPDATE investor_entities
        SET identification_status_id = u.identification_status_id
      FROM users AS u
      WHERE type = 'InvestorEntity::Individual'
      AND u.id = user_id;
    SQL
  end

  def down
    remove_column :investor_entities, :identification_status_id
  end
end
