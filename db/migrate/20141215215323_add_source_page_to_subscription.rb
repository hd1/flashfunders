class AddSourcePageToSubscription < ActiveRecord::Migration
  def change
    add_column :subscriptions, :source_page, :string
  end
end
