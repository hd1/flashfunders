class RemoveManagedFundsQuestionFromSuitability < ActiveRecord::Migration
  def up
    remove_column :investor_suitability_questionnaires, :interested_in_managed_fund_investments
  end

  def down
    add_column :investor_suitability_questionnaires, :interested_in_managed_fund_investments, :boolean
  end
end
