class AddDocumentColumnToAccreditationDocuments < ActiveRecord::Migration
  def up
    add_column :accreditation_documents, :document, :string

    execute <<-SQL
      UPDATE accreditation_documents SET document = concat_ws('/', uuid, file)
    SQL
  end

  def down
    remove_column :accreditation_documents, :document
  end
end

