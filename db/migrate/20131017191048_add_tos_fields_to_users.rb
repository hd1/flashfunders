class AddTosFieldsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :agreed_to_tos, :boolean
    add_column :users, :agreed_to_tos_datetime, :datetime
    add_column :users, :agreed_to_tos_ip, :string
  end
end
