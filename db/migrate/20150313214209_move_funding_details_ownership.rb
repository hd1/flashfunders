class MoveFundingDetailsOwnership < ActiveRecord::Migration
  def up
    add_column :offerings, :offering_funding_detail_direct_id, :integer
    add_column :offerings, :offering_funding_detail_spv_id, :integer

    execute <<-SQL
      UPDATE offerings AS offering
        SET offering_funding_detail_direct_id = ofd.id
        FROM offering_funding_details AS ofd
        WHERE offering.id = ofd.offering_id
    SQL

  end

  def down
    remove_column :offerings, :offering_funding_detail_direct_id
    remove_column :offerings, :offering_funding_detail_spv_id
  end
end
