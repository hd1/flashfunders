class CreateRoutingNumbers < ActiveRecord::Migration
  def change
    create_table :routing_numbers do |t|
      t.string :routing_number
    end

    add_index :routing_numbers, :routing_number
  end
end
