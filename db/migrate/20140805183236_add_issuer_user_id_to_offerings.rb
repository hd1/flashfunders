class AddIssuerUserIdToOfferings < ActiveRecord::Migration
  def change
    add_column :offerings, :issuer_user_id, :integer
  end
end
