class BackfillUserUuids < ActiveRecord::Migration
  def up
    User.find_each do |user|
      user.update_attributes!(uuid: SecureRandom.uuid)
    end
  end

  def down

  end
end
