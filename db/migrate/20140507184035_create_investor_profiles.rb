class CreateInvestorProfiles < ActiveRecord::Migration
  def change
    create_table :investor_profiles do |t|
      t.integer :user_id
      t.boolean :risk_conservative
      t.boolean :risk_moderate
      t.boolean :risk_aggressive
      t.boolean :i_have_knowledge
      t.boolean :i_understand
      t.string :how_much
      t.string :experience
      t.string :percentage
      t.string :net_worth
      t.string :income
      t.string :liquid_net_worth
      t.boolean :funding_income
      t.boolean :funding_pension
      t.boolean :funding_sales
      t.boolean :funding_inheritance
      t.boolean :funding_home_equity
      t.boolean :funding_other
      t.string :funding_other_text

      t.timestamps
    end
  end
end
