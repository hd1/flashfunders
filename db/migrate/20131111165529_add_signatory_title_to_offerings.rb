class AddSignatoryTitleToOfferings < ActiveRecord::Migration
  def change
    add_column :offerings, :signatory_title, :string
  end
end
