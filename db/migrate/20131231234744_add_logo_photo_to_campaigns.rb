class AddLogoPhotoToCampaigns < ActiveRecord::Migration
  def change
    add_column :campaigns, :logo_photo, :string
  end
end
