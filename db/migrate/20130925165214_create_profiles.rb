class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.string :name
      t.text :bio
      t.string :location

      t.string :website_url
      t.string :facebook_url
      t.string :twitter_url
      t.string :linkedin_url

      t.references :user
    end
  end
end
