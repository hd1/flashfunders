class CreateCorporateFederalIdentificationInformations < ActiveRecord::Migration
  def change
    create_table :corporate_federal_identification_informations do |t|
      t.references :company
      t.integer :state_id
      t.string :ein
      t.string :corporate_name

      t.timestamps
    end
  end
end
