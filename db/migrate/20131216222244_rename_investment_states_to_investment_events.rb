class RenameInvestmentStatesToInvestmentEvents < ActiveRecord::Migration
  def change
    rename_table :investment_states, :investment_events
  end
end
