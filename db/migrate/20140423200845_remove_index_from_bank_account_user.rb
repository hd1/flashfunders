class RemoveIndexFromBankAccountUser < ActiveRecord::Migration
  def change
    remove_index :bank_accounts, name: 'unique_investor_bank_accounts_user_id_index'
  end
end
