class CreateSuitabilityAuditLogTable < ActiveRecord::Migration
  def change
    create_table :suitability_audit_logs do |t|
      t.integer :user_id
      t.integer :suitability_status_id
      t.string :created_by

      t.timestamps
    end
  end
end
