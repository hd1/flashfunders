class CreateStaticPages < ActiveRecord::Migration
  def change
    create_table :static_pages do |t|
      t.string :name
      t.integer :total_raised
      t.text :startups_image
      t.text :investors_image
    end

    sql = <<-EOS
    INSERT INTO static_pages (name, total_raised)
    VALUES ('what_is_flashfunders', 8608018)
    EOS
    connection.execute(sql)
  end
end
