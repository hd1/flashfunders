class AddSwatchPhotoToCampaign < ActiveRecord::Migration
  def change
    add_column :campaigns, :swatch_photo, :string
  end
end
