class AddForeignKeyConstraintToPersonalFederalIdentificationInformationsUserId < ActiveRecord::Migration
  def up
    execute <<-SQL
      ALTER TABLE personal_federal_identification_informations
        ADD CONSTRAINT fk_personal_federal_identification_information_users
        FOREIGN KEY (user_id)
        REFERENCES users(id)
    SQL
  end

  def down
    execute <<-SQL
      ALTER TABLE personal_federal_identification_informations
        DROP CONSTRAINT fk_personal_federal_identification_information_users
    SQL
  end
end
