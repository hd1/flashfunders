class RemovePublicEmailFromOfferings < ActiveRecord::Migration
  def change
    remove_column :offerings, :public_email, :string
  end
end
