class AddSpvFieldsToOffering < ActiveRecord::Migration
  class Offering < ActiveRecord::Base
    enum offering_model: { model_direct: 1, model_direct_spv: 2 }
  end

  def up
    add_column :offerings, :offering_model, :integer, null: false, default: Offering.offering_models[:model_direct]
    add_column :offerings, :direct_investment_threshold, :decimal, precision: 64, scale: 2
    add_column :offerings, :spv_docusign_template_id, :string, limit: 255

    Offering.all.each do |offering|
      offering.update_attribute(:offering_model, Offering.offering_models[:model_direct])
    end
  end

  def down
    remove_column :offerings, :offering_model
    remove_column :offerings, :direct_investment_threshold
    remove_column :offerings, :spv_docusign_template_id
  end
end
