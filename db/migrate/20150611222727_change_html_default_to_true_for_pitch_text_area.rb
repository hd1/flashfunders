include ActionView::Helpers::TextHelper

class ChangeHtmlDefaultToTrueForPitchTextArea < ActiveRecord::Migration
  def up
    change_column :pitch_text_areas, :html, :boolean, :default => true

    Pitch::TextArea.find_each do |pitch_text|
      pitch_text.update!(body: simple_format(pitch_text.body), html: true) unless pitch_text.html?
    end
  end

  def down
    change_column :pitch_text_areas, :html, :boolean, :default => false
  end
end
