class CreateOfferingFollowersTable < ActiveRecord::Migration
  def change
    create_table :offering_followers do |t|
      t.references :offering
      t.references :user
      t.datetime :followed_at

      t.timestamps
    end
  end
end
