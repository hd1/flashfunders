class RemoveOfferingColumns < ActiveRecord::Migration
  def up
    remove_column :offerings, :escrow_provider_id
    remove_column :offerings, :offering_funding_detail_direct_id
    remove_column :offerings, :offering_funding_detail_direct_international_id
    remove_column :offerings, :shares_offered
    remove_column :offerings, :minimum_total_investment
    remove_column :offerings, :minimum_total_investment_display
    remove_column :offerings, :maximum_total_investment
    remove_column :offerings, :maximum_total_investment_display
    remove_column :offerings, :minimum_investment_amount
    remove_column :offerings, :minimum_investment_amount_display
    remove_column :offerings, :external_investment_amount
    remove_column :offerings, :external_investor_count
    remove_column :offerings, :docusign_template_id
    remove_column :offerings, :docusign_international_template_id
    remove_column :offerings, :spv_docusign_template_id
    remove_column :offerings, :spv_docusign_international_template_id
    remove_column :offerings, :fund_america_id
    remove_column :offerings, :fund_america_spv_id
    remove_column :offerings, :cf_minimum_total_investment
    remove_column :offerings, :seed_minimum_total_investment
    remove_column :offerings, :cf_minimum_total_investment_display
    remove_column :offerings, :seed_minimum_total_investment_display
    remove_column :offerings, :cf_maximum_total_investment
    remove_column :offerings, :seed_maximum_total_investment
    remove_column :offerings, :cf_maximum_total_investment_display
    remove_column :offerings, :seed_maximum_total_investment_display
    remove_column :offerings, :cf_minimum_investment_amount
    remove_column :offerings, :seed_minimum_investment_amount
    remove_column :offerings, :cf_minimum_investment_amount_display
    remove_column :offerings, :seed_minimum_investment_amount_display
    remove_column :offerings, :cf_shares_offered
    remove_column :offerings, :seed_shares_offered
    remove_column :offerings, :cf_fully_diluted_shares
    remove_column :offerings, :seed_fully_diluted_shares
    remove_column :offerings, :cf_external_investment_amount
    remove_column :offerings, :seed_external_investment_amount
    remove_column :offerings, :cf_external_investor_count
    remove_column :offerings, :seed_external_investor_count
    remove_column :offerings, :cf_security_type_id
    remove_column :offerings, :seed_security_type_id
  end
end
