class DropInvestorAccreditationVerifications < ActiveRecord::Migration
  def up
    drop_table :investor_accreditation_verifications
  end

  def down
    create_table :investor_accreditation_verifications do |t|
      t.references :user
      t.datetime :submitted_accreditation_verification_for_approval
      t.boolean :self_certified

      t.timestamps
    end

  end
end
