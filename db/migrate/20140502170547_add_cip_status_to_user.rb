class AddCipStatusToUser < ActiveRecord::Migration
  def change
    add_column :users, :cip_status, :string, default: 'Not Reviewed'

    add_index :users, :cip_status
  end
end
