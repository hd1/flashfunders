class AddDocsSignedDateToInvestments < ActiveRecord::Migration
  def up
    add_column :investments, :docs_signed_at, :datetime

    execute <<-SQL
      UPDATE investments SET docs_signed_at = investment_events.created_at
      FROM investment_events
      WHERE investments.id = investment_events.investment_id
        AND investment_events.type = 'InvestmentEvent::SignDocumentsEvent'
    SQL
  end

  def down
    remove_column :investments, :docs_signed_at
  end
end
