class CreateInvestorSuitabilityQuestionnaireCategorySectors < ActiveRecord::Migration
  def change
    create_table :investor_suitability_questionnaire_category_sectors do |t|
      t.references :investor_suitability_questionnaire
      t.integer :category_sector
    end
  end
end