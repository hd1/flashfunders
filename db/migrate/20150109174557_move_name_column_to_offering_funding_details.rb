class MoveNameColumnToOfferingFundingDetails < ActiveRecord::Migration
  def up
    add_column :offering_funding_details, :check_bank_name, :string
    add_column :offering_funding_details, :wire_beneficiary_name, :string

    execute <<-SQL
      UPDATE offering_funding_details
        SET check_bank_name = addresses.name
      FROM addresses
      WHERE addresses.id = check_address_id
    SQL

    execute <<-SQL
      UPDATE offering_funding_details
        SET wire_beneficiary_name = addresses.name
      FROM addresses
      WHERE addresses.id = wire_beneficiary_address_id
    SQL

    remove_column :addresses, :name
  end

  def down
    add_column :addresses, :name, :string

    execute <<-SQL
      UPDATE addresses
        SET name = offering_funding_details.check_bank_name
      FROM offering_funding_details
      WHERE offering_funding_details.check_address_id = addresses.id
    SQL

    execute <<-SQL
      UPDATE addresses
        SET name = offering_funding_details.wire_beneficiary_name
      FROM offering_funding_details
      WHERE offering_funding_details.wire_beneficiary_address_id = addresses.id
    SQL

    remove_column :offering_funding_details, :check_bank_name
    remove_column :offering_funding_details, :wire_beneficiary_name
  end
end

