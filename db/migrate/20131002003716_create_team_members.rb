class CreateTeamMembers < ActiveRecord::Migration
  def change
    create_table :team_members do |t|
      t.string :full_name
      t.string :position
      t.string :location
      t.string :percent_ownership
      t.string :date_joined
      t.string :twitter_url
      t.string :facebook_url
      t.string :linkedin_url
      t.string :avatar

      t.text :bio

      t.references :campaign

      t.timestamps
    end
  end
end
