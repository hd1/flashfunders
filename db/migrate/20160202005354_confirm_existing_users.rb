class ConfirmExistingUsers < ActiveRecord::Migration
  def up
    User.update_all({ confirmed_at: DateTime.now, confirmation_token: 'Grandfathered Account' })
  end
end
