class CreatePitchSections < ActiveRecord::Migration
  def change
    create_table :pitch_sections do |t|
      t.integer :offering_id, index: true
      t.string :title
      t.integer :rank

      t.timestamps
    end
  end
end
