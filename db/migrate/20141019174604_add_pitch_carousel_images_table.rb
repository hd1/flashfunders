class AddPitchCarouselImagesTable < ActiveRecord::Migration
  def change
    create_table :pitch_carousel_images do |t|
      t.references :carousel
      t.string :carousel_photo
      t.integer :rank

      t.timestamps
    end
  end
end
