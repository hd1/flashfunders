class AddForeignKeyAndUniquenessConstraintToBancBoxIssuerAccountsCampaignId < ActiveRecord::Migration
  def up
    execute <<-SQL
      ALTER TABLE banc_box_issuer_accounts
        ADD CONSTRAINT fk_banc_box_issuer_account_campaign
        FOREIGN KEY (campaign_id)
        REFERENCES campaigns(id)
    SQL

    execute <<-SQL
      ALTER TABLE banc_box_issuer_accounts
        ADD CONSTRAINT unique_banc_box_issuer_account_campaign_id
        UNIQUE (campaign_id)
    SQL
  end

  def down
    execute <<-SQL
      ALTER TABLE banc_box_issuer_accounts
        DROP CONSTRAINT fk_banc_box_issuer_account_campaign
    SQL
    execute <<-SQL
      ALTER TABLE banc_box_issuer_accounts
        DROP CONSTRAINT unique_banc_box_issuer_account_campaign_id
    SQL
  end
end
