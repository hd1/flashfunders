class CreateWaitingListRegistration < ActiveRecord::Migration
  def change
    create_table :waiting_list_registrations do |t|
      t.string :full_name
      t.string :email
      t.string :category
      t.timestamps
    end
  end
end
