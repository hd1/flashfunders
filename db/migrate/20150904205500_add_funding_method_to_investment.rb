class AddFundingMethodToInvestment < ActiveRecord::Migration
  def change
    add_column :investments, :funding_method, :integer
  end
end
