class UpdatePaymentMethodAndInvestmentState < ActiveRecord::Migration
  def up
    Investment.transaction do
      populate_funding_method
      migrate_payment_method_selected
      migrate_payment_sent
      migrate_agreed_to_investment
      migrate_agreed_to_prerequisites
      migrate_linked_refund_account
    end
  end

  def down
    # There's no going back...
  end

  def migrate_agreed_to_investment
    Investment.where(state: :agreed_to_investment).update_all(state: :accreditation_verified)
    InvestmentEvent.delete_all(type: 'InvestmentEvent::AcceptInvestmentEvent')
  end

  def migrate_agreed_to_prerequisites
    Investment.where(state: :agreed_to_prerequisites).update_all(state: :intended)
    InvestmentEvent.delete_all(type: 'InvestmentEvent::AgreeToPrerequisitesEvent')
  end

  def migrate_linked_refund_account
    Investment.where(state: :linked_refund_account).find_each do |investment|
      investment.update_attributes(state: :signed_documents, funding_method: :wire)
      investment.select_funding_method!
    end
    InvestmentEvent.delete_all(type: 'InvestmentEvent::LinkRefundAccountEvent')
  end

  def migrate_payment_method_selected
    Investment.where(state: %w(wire_transfer_selected paper_check_selected)).update_all(state: :funding_method_selected)
    InvestmentEvent.where(type: %w(InvestmentEvent::SelectWireTransferEvent InvestmentEvent::SelectPaperCheckEvent))
        .update_all(type: 'InvestmentEvent::SelectFundingMethodEvent')
  end

  def migrate_payment_sent
    Investment.where(state: %w(wire_transfer_sent paper_check_sent)).update_all(state: :funds_sent)
    InvestmentEvent.where(type: %w(InvestmentEvent::WirePaymentEvent InvestmentEvent::MailPaymentEvent))
        .update_all(type: 'InvestmentEvent::FundsSentEvent')
  end

  def populate_funding_method
    Investment.where('funding_method is null').find_each do |investment|
      method = lookup_payment_method investment
      Investment.where(id: investment.id).update_all(funding_method: Investment.funding_methods[method]) unless method.nil?
    end
  end

  def lookup_payment_method(investment_id)
    event = InvestmentEvent.select(:type)
                .where(investment_id: investment_id,
                       type:          %w(InvestmentEvent::SelectWireTransferEvent
                                         InvestmentEvent::SelectPaperCheckEvent
                                         InvestmentEvent::WirePaymentEvent
                                         InvestmentEvent::MailPaymentEvent))
                .order(updated_at: :desc)
                .first
    case event
      when InvestmentEvent::SelectWireTransferEvent, InvestmentEvent::WirePaymentEvent
        :wire
      when InvestmentEvent::SelectPaperCheckEvent, InvestmentEvent::MailPaymentEvent
        :paper_check
      else
        nil
    end
  end
end
