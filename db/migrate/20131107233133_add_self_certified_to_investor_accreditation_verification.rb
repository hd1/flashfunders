class AddSelfCertifiedToInvestorAccreditationVerification < ActiveRecord::Migration
  def change
    add_column :investor_accreditation_verifications, :self_certified, :boolean
  end
end
