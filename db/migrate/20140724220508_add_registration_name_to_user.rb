class AddRegistrationNameToUser < ActiveRecord::Migration
  def change
    add_column :users, :registration_name, :string
  end
end
