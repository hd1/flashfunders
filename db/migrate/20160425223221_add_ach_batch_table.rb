class AddAchBatchTable < ActiveRecord::Migration
  def change
    create_table :ach_batches do |t|
      t.integer :ref_code
      t.string :destination_bank
      t.string :routing_number
      t.string :company_name
      t.string :company_ein
      t.integer :kind
      t.date :effective_entry_date
      t.references :escrow_provider

      t.timestamps null: false
    end

    create_table :ach_activities do |t|
      t.references :ach_batch, index: true
      t.references :investment, index: true
      t.integer :xact_code
      t.integer :batch_number
      t.integer :trace_number
      t.float :amount
      t.string :routing_number
      t.string :encrypted_account_number
      t.string :encrypted_account_holder
    end

  end
end
