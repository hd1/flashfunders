class AddCountryToAddresses < ActiveRecord::Migration
  def change
    add_column :personal_federal_identification_informations, :country, :string
    add_column :investor_entities, :country, :string
    add_column :investor_entities, :formation_country, :string

    PersonalFederalIdentificationInformation.update_all("country = 'US'")
    InvestorEntity.update_all("country = 'US'")
    InvestorEntity.update_all("formation_country = 'US'")
  end
end
