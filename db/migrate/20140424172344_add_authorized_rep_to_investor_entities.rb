class AddAuthorizedRepToInvestorEntities < ActiveRecord::Migration
  def change
    add_column :investor_entities, :confirmed_authorization, :boolean
  end
end
