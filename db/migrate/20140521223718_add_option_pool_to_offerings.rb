class AddOptionPoolToOfferings < ActiveRecord::Migration
  def change
    add_column :offerings, :option_pool, :string
  end
end
