class CreateAccreditationQualificationsTable < ActiveRecord::Migration
  def up
    execute 'CREATE EXTENSION IF NOT EXISTS hstore;'

    create_table :accreditation_qualifications do |t|
      t.uuid :uuid, null: false
      t.string :type, null: false
      t.integer :investor_id, null: false
      t.hstore :data, null: false
      t.datetime :verified_at
      t.string :verified_by
      t.datetime :declined_at
      t.string :declined_by
      t.datetime :expires_at

      t.timestamps
    end

    add_index :accreditation_qualifications, :uuid, unique: true
  end

  def down
    drop_table :accreditation_qualifications
  end
end
