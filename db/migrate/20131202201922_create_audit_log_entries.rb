class CreateAuditLogEntries < ActiveRecord::Migration
  def change
    create_table :audit_log_entries do |t|
      t.references :acting_user
      t.string :acting_user_ip_address

      t.string :form_path
      t.json :persisted_state
      t.datetime :persisted_time

      t.timestamps
    end
  end
end
