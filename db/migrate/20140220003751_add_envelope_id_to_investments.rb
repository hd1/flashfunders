class AddEnvelopeIdToInvestments < ActiveRecord::Migration
  def change
    add_column :investments, :envelope_id, :string
  end
end
