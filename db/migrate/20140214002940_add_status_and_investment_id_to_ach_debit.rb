class AddStatusAndInvestmentIdToAchDebit < ActiveRecord::Migration
  def change
    add_column :ach_debits, :status, :string
    add_column :ach_debits, :investment_id, :integer
  end
end
