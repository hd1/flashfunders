class AddFieldsToOfferings < ActiveRecord::Migration
  def change
    add_column :offerings, :share_price, :float
    add_column :offerings, :shares_offered, :integer
    add_column :offerings, :fully_diluted_shares, :integer
  end
end
