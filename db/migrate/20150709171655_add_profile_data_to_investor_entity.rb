class AddProfileDataToInvestorEntity < ActiveRecord::Migration
  def change
    add_column :investor_entities, :profile_data, :hstore
  end
end
