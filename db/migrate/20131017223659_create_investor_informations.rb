class CreateInvestorInformations < ActiveRecord::Migration
  def change
    create_table :investor_informations do |t|
      t.references :user

      t.string :first_name
      t.string :middle_initial
      t.string :last_name

      t.date :date_of_birth

      t.string :ssn

      t.string :address1
      t.string :address2
      t.string :city
      t.string :state
      t.string :zip_code
      t.string :phone_number

      t.timestamps
    end
  end
end
