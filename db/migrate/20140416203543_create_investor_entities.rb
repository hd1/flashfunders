class CreateInvestorEntities < ActiveRecord::Migration
  def change
    create_table :investor_entities do |t|
      t.string :type
      t.references :user, index: true
      t.string :name
      t.string :tax_id_number
      t.date :formation_date
      t.string :address_1
      t.string :address_2
      t.string :city
      t.string :state_id
      t.string :zip_code
      t.string :phone_number
      t.string :position
      t.string :formation_state_id

      t.timestamps
    end
  end
end
