class CreateInvestorBankAccounts < ActiveRecord::Migration
  def change
    create_table :investor_bank_accounts do |t|
      t.integer :user_id
      t.string :bank_name
      t.string :account_number
      t.string :routing_number
      t.string :account_type

      t.timestamps
    end

    add_index 'investor_bank_accounts', ['user_id'], name: 'unique_investor_bank_accounts_user_id_index', unique: true, using: :btree
  end
end
