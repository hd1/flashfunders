class AddFundingTypeToBankAccounts < ActiveRecord::Migration
  def change
    add_column :bank_accounts, :funding_type, :integer
  end
end
