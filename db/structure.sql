--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: hstore; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS hstore WITH SCHEMA public;


--
-- Name: EXTENSION hstore; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION hstore IS 'data type for storing sets of (key, value) pairs';


--
-- Name: pg_stat_statements; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS pg_stat_statements WITH SCHEMA public;


--
-- Name: EXTENSION pg_stat_statements; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION pg_stat_statements IS 'track execution statistics of all SQL statements executed';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: accreditation_documents; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE accreditation_documents (
    id integer NOT NULL,
    accreditation_qualification_id integer,
    uuid uuid,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    document character varying(255)
);


--
-- Name: accreditation_documents_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE accreditation_documents_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: accreditation_documents_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE accreditation_documents_id_seq OWNED BY accreditation_documents.id;


--
-- Name: accreditor_qualifications; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE accreditor_qualifications (
    id integer NOT NULL,
    uuid uuid NOT NULL,
    type character varying(255) NOT NULL,
    data hstore NOT NULL,
    verified_at timestamp without time zone,
    verified_by character varying(255),
    declined_at timestamp without time zone,
    declined_by character varying(255),
    expires_at timestamp without time zone,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    user_id integer NOT NULL,
    investor_entity_id integer
);


--
-- Name: accreditor_qualifications_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE accreditor_qualifications_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: accreditor_qualifications_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE accreditor_qualifications_id_seq OWNED BY accreditor_qualifications.id;


--
-- Name: accreditor_spans; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE accreditor_spans (
    id integer NOT NULL,
    investor_entity_id integer,
    status integer,
    start_date date,
    end_date date,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    accreditor_qualification_id integer
);


--
-- Name: accreditor_spans_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE accreditor_spans_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: accreditor_spans_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE accreditor_spans_id_seq OWNED BY accreditor_spans.id;


--
-- Name: ach_activities; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE ach_activities (
    id integer NOT NULL,
    ach_batch_id integer,
    investment_id integer,
    xact_code integer,
    batch_number integer,
    trace_number integer,
    amount double precision,
    routing_number character varying,
    encrypted_account_number character varying,
    encrypted_account_holder character varying
);


--
-- Name: ach_activities_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE ach_activities_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: ach_activities_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE ach_activities_id_seq OWNED BY ach_activities.id;


--
-- Name: ach_batches; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE ach_batches (
    id integer NOT NULL,
    ref_code integer,
    destination_bank character varying,
    routing_number character varying,
    company_name character varying,
    company_ein character varying,
    kind integer,
    effective_entry_date date,
    escrow_provider_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: ach_batches_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE ach_batches_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: ach_batches_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE ach_batches_id_seq OWNED BY ach_batches.id;


--
-- Name: ach_debits; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE ach_debits (
    id integer NOT NULL,
    user_id integer,
    amount numeric,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    uuid uuid,
    status character varying(255),
    investment_id integer,
    succeeded_at timestamp without time zone
);


--
-- Name: ach_debits_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE ach_debits_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: ach_debits_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE ach_debits_id_seq OWNED BY ach_debits.id;


--
-- Name: active_admin_attributes; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE active_admin_attributes (
    id integer NOT NULL,
    record_type character varying(255),
    record_id integer,
    attr_type character varying(255),
    attr_value character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: active_admin_attributes_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE active_admin_attributes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: active_admin_attributes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE active_admin_attributes_id_seq OWNED BY active_admin_attributes.id;


--
-- Name: active_admin_comments; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE active_admin_comments (
    id integer NOT NULL,
    namespace character varying(255),
    body text,
    resource_id character varying(255) NOT NULL,
    resource_type character varying(255) NOT NULL,
    author_id integer,
    author_type character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: active_admin_comments_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE active_admin_comments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: active_admin_comments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE active_admin_comments_id_seq OWNED BY active_admin_comments.id;


--
-- Name: addresses; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE addresses (
    id integer NOT NULL,
    address1 character varying(255),
    address2 character varying(255),
    city character varying(255),
    state character varying(255),
    zip_code character varying(255),
    phone_number character varying(255)
);


--
-- Name: addresses_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE addresses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: addresses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE addresses_id_seq OWNED BY addresses.id;


--
-- Name: admin_users; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE admin_users (
    id integer NOT NULL,
    email character varying(255) DEFAULT ''::character varying NOT NULL,
    encrypted_password character varying(255) DEFAULT ''::character varying NOT NULL,
    reset_password_token character varying(255),
    reset_password_sent_at timestamp without time zone,
    remember_created_at timestamp without time zone,
    sign_in_count integer DEFAULT 0 NOT NULL,
    current_sign_in_at timestamp without time zone,
    last_sign_in_at timestamp without time zone,
    current_sign_in_ip character varying(255),
    last_sign_in_ip character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    roles_mask integer
);


--
-- Name: admin_users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE admin_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: admin_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE admin_users_id_seq OWNED BY admin_users.id;


--
-- Name: audit_log_entries; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE audit_log_entries (
    id integer NOT NULL,
    acting_user_id integer,
    acting_user_ip_address character varying(255),
    form_path character varying(255),
    persisted_state json,
    persisted_time timestamp without time zone,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    impersonating_user_id integer
);


--
-- Name: audit_log_entries_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE audit_log_entries_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: audit_log_entries_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE audit_log_entries_id_seq OWNED BY audit_log_entries.id;


--
-- Name: banc_box_escrow_accounts; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE banc_box_escrow_accounts (
    id integer NOT NULL,
    banc_box_issuer_account_id integer,
    banc_box_id bigint
);


--
-- Name: banc_box_escrow_accounts_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE banc_box_escrow_accounts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: banc_box_escrow_accounts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE banc_box_escrow_accounts_id_seq OWNED BY banc_box_escrow_accounts.id;


--
-- Name: banc_box_fund_account_transactions; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE banc_box_fund_account_transactions (
    id integer NOT NULL,
    transaction_id bigint,
    ach_debit_id integer
);


--
-- Name: banc_box_fund_account_transactions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE banc_box_fund_account_transactions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: banc_box_fund_account_transactions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE banc_box_fund_account_transactions_id_seq OWNED BY banc_box_fund_account_transactions.id;


--
-- Name: banc_box_fund_escrows; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE banc_box_fund_escrows (
    id integer NOT NULL,
    banc_box_funding_schedule_id character varying(255),
    investment_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: banc_box_fund_escrows_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE banc_box_fund_escrows_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: banc_box_fund_escrows_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE banc_box_fund_escrows_id_seq OWNED BY banc_box_fund_escrows.id;


--
-- Name: banc_box_investor_accounts; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE banc_box_investor_accounts (
    id integer NOT NULL,
    banc_box_id bigint,
    investment_id integer
);


--
-- Name: banc_box_investor_accounts_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE banc_box_investor_accounts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: banc_box_investor_accounts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE banc_box_investor_accounts_id_seq OWNED BY banc_box_investor_accounts.id;


--
-- Name: banc_box_issuer_accounts; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE banc_box_issuer_accounts (
    id integer NOT NULL,
    banc_box_id bigint,
    offering_id integer
);


--
-- Name: banc_box_issuer_accounts_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE banc_box_issuer_accounts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: banc_box_issuer_accounts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE banc_box_issuer_accounts_id_seq OWNED BY banc_box_issuer_accounts.id;


--
-- Name: banc_box_linked_external_accounts; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE banc_box_linked_external_accounts (
    id integer NOT NULL,
    banc_box_investor_account_id integer,
    banc_box_id bigint,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: banc_box_linked_external_accounts_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE banc_box_linked_external_accounts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: banc_box_linked_external_accounts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE banc_box_linked_external_accounts_id_seq OWNED BY banc_box_linked_external_accounts.id;


--
-- Name: bank_accounts; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE bank_accounts (
    id integer NOT NULL,
    user_id integer,
    bank_name character varying(255),
    routing_number character varying(255),
    account_type character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    encrypted_account_number character varying(255),
    type character varying(255),
    encrypted_account_holder character varying(255),
    funding_type integer
);


--
-- Name: bank_accounts_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE bank_accounts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bank_accounts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE bank_accounts_id_seq OWNED BY bank_accounts.id;


--
-- Name: bank_activities; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE bank_activities (
    id integer NOT NULL,
    action character varying(255),
    direction character varying(255),
    amount numeric(12,2),
    method character varying(255),
    status character varying(255),
    offering_uuid uuid,
    activity_uuid uuid,
    started_at timestamp without time zone,
    scheduled_at timestamp without time zone,
    completed_at timestamp without time zone,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    investment_id integer,
    user_id integer
);


--
-- Name: bank_activities_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE bank_activities_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bank_activities_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE bank_activities_id_seq OWNED BY bank_activities.id;


--
-- Name: bank_activity_details; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE bank_activity_details (
    id integer NOT NULL,
    banc_box_escrow_id bigint,
    activity_date date,
    description character varying(255),
    debit_amount numeric(12,2),
    credit_amount numeric(12,2),
    balance numeric(12,2),
    investor_id bigint,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: bank_activity_details_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE bank_activity_details_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bank_activity_details_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE bank_activity_details_id_seq OWNED BY bank_activity_details.id;


--
-- Name: campaign_category_sectors; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE campaign_category_sectors (
    id integer NOT NULL,
    category_sector_id integer,
    offering_id integer
);


--
-- Name: campaign_category_sectors_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE campaign_category_sectors_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: campaign_category_sectors_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE campaign_category_sectors_id_seq OWNED BY campaign_category_sectors.id;


--
-- Name: campaign_questions; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE campaign_questions (
    id integer NOT NULL,
    user_id integer,
    question_text text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    offering_id integer
);


--
-- Name: campaign_questions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE campaign_questions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: campaign_questions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE campaign_questions_id_seq OWNED BY campaign_questions.id;


--
-- Name: client_trackers; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE client_trackers (
    id integer NOT NULL,
    trackable_id integer,
    trackable_type character varying,
    client_id character varying
);


--
-- Name: client_trackers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE client_trackers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: client_trackers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE client_trackers_id_seq OWNED BY client_trackers.id;


--
-- Name: companies; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE companies (
    id integer NOT NULL,
    name character varying(255),
    website_url character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    twitter_url character varying(255),
    facebook_url character varying(255),
    location character varying(255),
    offering_id integer
);


--
-- Name: companies_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE companies_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: companies_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE companies_id_seq OWNED BY companies.id;


--
-- Name: company_relationships; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE company_relationships (
    id integer NOT NULL,
    company_id integer,
    person_id integer,
    "position" character varying(255),
    ownership_percentage character varying(255),
    date_joined character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    company_relationship_type_id integer
);


--
-- Name: company_relationships_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE company_relationships_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: company_relationships_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE company_relationships_id_seq OWNED BY company_relationships.id;


--
-- Name: contact_messages; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE contact_messages (
    id integer NOT NULL,
    user_id integer,
    email character varying,
    name character varying,
    kind character varying,
    message text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: contact_messages_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE contact_messages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: contact_messages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE contact_messages_id_seq OWNED BY contact_messages.id;


--
-- Name: corporate_federal_identification_informations; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE corporate_federal_identification_informations (
    id integer NOT NULL,
    company_id integer,
    state_id character varying(255),
    ein character varying(255),
    corporate_name character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    address_id integer,
    executive_name character varying,
    state_formed_in character varying
);


--
-- Name: corporate_federal_identification_informations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE corporate_federal_identification_informations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: corporate_federal_identification_informations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE corporate_federal_identification_informations_id_seq OWNED BY corporate_federal_identification_informations.id;


--
-- Name: documents; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE documents (
    id integer NOT NULL,
    type character varying(255),
    name character varying(255),
    file character varying(255),
    uuid uuid,
    updated_at timestamp without time zone,
    created_at timestamp without time zone,
    documentable_id integer,
    documentable_type character varying(255),
    document character varying(255),
    rank integer,
    description text,
    document_new text
);


--
-- Name: documents_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE documents_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: documents_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE documents_id_seq OWNED BY documents.id;


--
-- Name: escrow_caches; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE escrow_caches (
    id integer NOT NULL,
    escrow_data json,
    banc_box_escrow_id character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: escrow_caches_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE escrow_caches_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: escrow_caches_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE escrow_caches_id_seq OWNED BY escrow_caches.id;


--
-- Name: escrow_providers; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE escrow_providers (
    id integer NOT NULL,
    name character varying,
    properties hstore,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    default_instructions json,
    provider_key character varying
);


--
-- Name: escrow_providers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE escrow_providers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: escrow_providers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE escrow_providers_id_seq OWNED BY escrow_providers.id;


--
-- Name: fund_america_requests; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE fund_america_requests (
    id integer NOT NULL,
    offering_id integer,
    request json,
    response json,
    security_id integer
);


--
-- Name: fund_america_requests_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE fund_america_requests_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: fund_america_requests_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE fund_america_requests_id_seq OWNED BY fund_america_requests.id;


--
-- Name: identification_audit_logs; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE identification_audit_logs (
    id integer NOT NULL,
    identification_status_id integer,
    created_by character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    investor_entity_id integer
);


--
-- Name: identification_audit_logs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE identification_audit_logs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: identification_audit_logs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE identification_audit_logs_id_seq OWNED BY identification_audit_logs.id;


--
-- Name: investment_emails; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE investment_emails (
    id integer NOT NULL,
    offering_id integer,
    type character varying,
    queued_at timestamp without time zone,
    sent_at timestamp without time zone,
    data json
);


--
-- Name: investment_emails_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE investment_emails_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: investment_emails_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE investment_emails_id_seq OWNED BY investment_emails.id;


--
-- Name: investment_events; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE investment_events (
    id integer NOT NULL,
    investment_id integer,
    type character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: investment_events_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE investment_events_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: investment_events_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE investment_events_id_seq OWNED BY investment_events.id;


--
-- Name: investments; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE investments (
    id integer NOT NULL,
    user_id integer,
    shares integer,
    amount numeric,
    percent_ownership numeric,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    uuid uuid,
    envelope_id character varying(255),
    state character varying(255),
    escrowed_at timestamp without time zone,
    offering_id integer,
    investor_entity_id integer,
    accreditation_status_id integer DEFAULT 1 NOT NULL,
    docs_signed_at timestamp without time zone,
    investor_account_id integer,
    internal_account_id integer,
    envelope_email character varying(255),
    fund_america_id character varying(255),
    admin_email_sent boolean DEFAULT false NOT NULL,
    funding_method integer,
    cached_envelope_details text,
    calc_data hstore,
    security_id integer,
    flags hstore
);


--
-- Name: investments_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE investments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: investments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE investments_id_seq OWNED BY investments.id;


--
-- Name: investor_entities; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE investor_entities (
    id integer NOT NULL,
    type character varying(255),
    user_id integer,
    name character varying(255),
    tax_id_number character varying(255),
    formation_date date,
    address_1 character varying(255),
    address_2 character varying(255),
    city character varying(255),
    state_id character varying(255),
    zip_code character varying(255),
    phone_number character varying(255),
    "position" character varying(255),
    formation_state_id character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    confirmed_authorization boolean,
    identification_status_id integer DEFAULT 1 NOT NULL,
    accreditation_status integer DEFAULT 0,
    country character varying(255),
    formation_country character varying(255),
    suitability_status integer DEFAULT 1 NOT NULL,
    profile_data hstore,
    fund_america_id character varying,
    fund_america_ach_authorization_id character varying
);


--
-- Name: investor_entities_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE investor_entities_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: investor_entities_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE investor_entities_id_seq OWNED BY investor_entities.id;


--
-- Name: investor_faqs; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE investor_faqs (
    id integer NOT NULL,
    question text,
    answer text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    offering_id integer,
    rank integer
);


--
-- Name: investor_faqs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE investor_faqs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: investor_faqs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE investor_faqs_id_seq OWNED BY investor_faqs.id;


--
-- Name: investor_onboarding_events; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE investor_onboarding_events (
    id integer NOT NULL,
    user_id integer,
    type character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: investor_onboarding_events_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE investor_onboarding_events_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: investor_onboarding_events_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE investor_onboarding_events_id_seq OWNED BY investor_onboarding_events.id;


--
-- Name: investor_prerequisites; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE investor_prerequisites (
    id integer NOT NULL,
    user_id integer,
    self_certified boolean,
    capable_of_evaluating_illiquid_investments boolean,
    understand_risk boolean
);


--
-- Name: investor_prerequisites_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE investor_prerequisites_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: investor_prerequisites_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE investor_prerequisites_id_seq OWNED BY investor_prerequisites.id;


--
-- Name: issuer_applications; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE issuer_applications (
    id integer NOT NULL,
    user_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    first_name character varying(255),
    middle_initial character varying(1),
    last_name character varying(255),
    phone_number character varying(255),
    company_name character varying(255),
    website character varying(255),
    raise_amount integer,
    pre_money_valuation integer,
    previously_raised integer,
    pledged integer,
    entity_type character varying(255),
    state_id character varying(255),
    message text,
    properties hstore
);


--
-- Name: issuer_applications_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE issuer_applications_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: issuer_applications_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE issuer_applications_id_seq OWNED BY issuer_applications.id;


--
-- Name: offering_comments; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE offering_comments (
    id integer NOT NULL,
    offering_id integer,
    user_id integer,
    body text,
    display_state integer DEFAULT 1,
    updated_by_user integer,
    parent_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: offering_comments_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE offering_comments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: offering_comments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE offering_comments_id_seq OWNED BY offering_comments.id;


--
-- Name: offering_followers; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE offering_followers (
    id integer NOT NULL,
    offering_id integer,
    user_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    mode integer
);


--
-- Name: offering_followers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE offering_followers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: offering_followers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE offering_followers_id_seq OWNED BY offering_followers.id;


--
-- Name: offering_funding_details; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE offering_funding_details (
    id integer NOT NULL,
    check_payable_to character varying(255),
    check_memo character varying(255),
    check_address_id integer,
    wire_bank_account_id integer,
    wire_bank_address_id integer,
    wire_beneficiary_address_id integer,
    wire_instructions character varying(255),
    check_bank_name character varying(255),
    wire_beneficiary_name character varying(255)
);


--
-- Name: offering_funding_details_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE offering_funding_details_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: offering_funding_details_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE offering_funding_details_id_seq OWNED BY offering_funding_details.id;


--
-- Name: offering_messages; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE offering_messages (
    id integer NOT NULL,
    offering_id integer,
    user_id integer,
    body text,
    sent_by_user boolean DEFAULT true,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    unread boolean DEFAULT true,
    name character varying,
    email character varying
);


--
-- Name: offering_messages_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE offering_messages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: offering_messages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE offering_messages_id_seq OWNED BY offering_messages.id;


--
-- Name: offerings; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE offerings (
    id integer NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    fully_diluted_shares integer,
    signatory_title character varying(255),
    pre_money_valuation numeric,
    share_price numeric,
    notional_balance character varying(255),
    investor_count integer,
    security_type_id integer,
    security_type_custom character varying(255),
    user_id integer,
    video_url character varying(255),
    elevator_pitch text,
    tagline character varying(255),
    cover_photo character varying(255),
    uuid character varying(255),
    public_contact_name character varying(255),
    option_pool character varying(255),
    streaming_url character varying(255),
    rank integer,
    closed_status integer DEFAULT 1 NOT NULL,
    escrow_end_date date,
    issuer_user_id integer,
    ends_at timestamp without time zone,
    investable boolean DEFAULT false,
    pre_money_valuation_display character varying(255),
    photo_cover character varying(255),
    photo_logo character varying(255),
    photo_swatch character varying(255),
    vanity_path character varying(255),
    visibility integer DEFAULT 1 NOT NULL,
    custom_convertible_details hstore,
    offering_model integer DEFAULT 1 NOT NULL,
    direct_investment_threshold numeric(64,2),
    offering_funding_detail_spv_id integer,
    carried_interest_display text,
    setup_fee_display text,
    setup_fee_dollar numeric(64,2),
    spv_minimum_goal numeric(64,2),
    share_photo character varying,
    first_team_title character varying,
    second_team_title character varying,
    offering_funding_detail_spv_international_id integer,
    photo_home character varying,
    tagline_browse character varying,
    notable_investor_id integer,
    photo_header character varying,
    dashboard_last_viewed_at timestamp without time zone,
    reg_c_enabled boolean,
    custom_data hstore
);


--
-- Name: offerings_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE offerings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: offerings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE offerings_id_seq OWNED BY offerings.id;


--
-- Name: personal_federal_identification_informations; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE personal_federal_identification_informations (
    id integer NOT NULL,
    user_id integer,
    first_name character varying(255),
    middle_initial character varying(255),
    last_name character varying(255),
    date_of_birth date,
    address1 character varying(255),
    address2 character varying(255),
    city character varying(255),
    zip_code character varying(255),
    daytime_phone character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    state_id character varying(255),
    encrypted_ssn text,
    id_type integer,
    id_number character varying(255),
    location character varying(255),
    issued_date date,
    expiration_date date,
    mobile_phone character varying(255),
    employment_data hstore,
    country character varying(255),
    passport_image character varying(255),
    investor_entity_id integer,
    us_citizen boolean DEFAULT true NOT NULL
);


--
-- Name: personal_federal_identification_informations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE personal_federal_identification_informations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: personal_federal_identification_informations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE personal_federal_identification_informations_id_seq OWNED BY personal_federal_identification_informations.id;


--
-- Name: pitch_carousel_images; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE pitch_carousel_images (
    id integer NOT NULL,
    carousel_id integer,
    carousel_photo character varying(255),
    rank integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: pitch_carousel_images_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE pitch_carousel_images_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pitch_carousel_images_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE pitch_carousel_images_id_seq OWNED BY pitch_carousel_images.id;


--
-- Name: pitch_carousels; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE pitch_carousels (
    id integer NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: pitch_carousels_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE pitch_carousels_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pitch_carousels_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE pitch_carousels_id_seq OWNED BY pitch_carousels.id;


--
-- Name: pitch_content_items; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE pitch_content_items (
    id integer NOT NULL,
    section_id integer,
    content_id integer,
    content_type character varying(255),
    rank integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: pitch_content_items_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE pitch_content_items_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pitch_content_items_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE pitch_content_items_id_seq OWNED BY pitch_content_items.id;


--
-- Name: pitch_images; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE pitch_images (
    id integer NOT NULL,
    photo character varying(255),
    expandable boolean,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: pitch_images_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE pitch_images_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pitch_images_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE pitch_images_id_seq OWNED BY pitch_images.id;


--
-- Name: pitch_sections; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE pitch_sections (
    id integer NOT NULL,
    offering_id integer,
    title character varying(255),
    rank integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: pitch_sections_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE pitch_sections_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pitch_sections_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE pitch_sections_id_seq OWNED BY pitch_sections.id;


--
-- Name: pitch_text_areas; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE pitch_text_areas (
    id integer NOT NULL,
    body text,
    html boolean DEFAULT true,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: pitch_text_areas_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE pitch_text_areas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pitch_text_areas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE pitch_text_areas_id_seq OWNED BY pitch_text_areas.id;


--
-- Name: pitch_videos; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE pitch_videos (
    id integer NOT NULL,
    url character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: pitch_videos_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE pitch_videos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pitch_videos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE pitch_videos_id_seq OWNED BY pitch_videos.id;


--
-- Name: routing_numbers; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE routing_numbers (
    id integer NOT NULL,
    routing_number character varying(255),
    name character varying(255)
);


--
-- Name: routing_numbers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE routing_numbers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: routing_numbers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE routing_numbers_id_seq OWNED BY routing_numbers.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE schema_migrations (
    version character varying(255) NOT NULL
);


--
-- Name: securities; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE securities (
    id integer NOT NULL,
    type character varying,
    is_spv boolean,
    offering_id integer,
    escrow_provider_id integer,
    offering_funding_detail_domestic_id integer,
    offering_funding_detail_international_id integer,
    shares_offered integer,
    minimum_total_investment numeric,
    minimum_total_investment_display character varying,
    maximum_total_investment numeric,
    maximum_total_investment_display character varying,
    minimum_investment_amount numeric,
    minimum_investment_amount_display character varying,
    external_investment_amount numeric DEFAULT 0,
    external_investor_count integer DEFAULT 0,
    security_title character varying,
    security_title_plural character varying,
    docusign_template_id character varying,
    docusign_international_template_id character varying,
    fund_america_id character varying,
    regulation integer DEFAULT 1,
    custom_data hstore,
    docusign_issuer_countersign_omnibus_id character varying
);


--
-- Name: securities_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE securities_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: securities_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE securities_id_seq OWNED BY securities.id;


--
-- Name: stakeholders; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE stakeholders (
    id integer NOT NULL,
    offering_id integer,
    stakeholder_type_id integer,
    rank integer,
    full_name character varying(255),
    "position" character varying(255),
    website text,
    twitter_url text,
    facebook_url text,
    linkedin_url text,
    bio text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    avatar_photo character varying(255)
);


--
-- Name: stakeholders_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE stakeholders_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: stakeholders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE stakeholders_id_seq OWNED BY stakeholders.id;

--
-- Name: subscriptions; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE subscriptions (
    id integer NOT NULL,
    email character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    source_page character varying(255)
);


--
-- Name: subscriptions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE subscriptions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: subscriptions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE subscriptions_id_seq OWNED BY subscriptions.id;


--
-- Name: suitability_audit_logs; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE suitability_audit_logs (
    id integer NOT NULL,
    user_id integer,
    suitability_status_id integer,
    created_by character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    investor_entity_id integer
);


--
-- Name: suitability_audit_logs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE suitability_audit_logs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: suitability_audit_logs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE suitability_audit_logs_id_seq OWNED BY suitability_audit_logs.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE users (
    id integer NOT NULL,
    email character varying(255) DEFAULT ''::character varying NOT NULL,
    encrypted_password character varying(255) DEFAULT ''::character varying NOT NULL,
    reset_password_token character varying(255),
    reset_password_sent_at timestamp without time zone,
    remember_created_at timestamp without time zone,
    sign_in_count integer DEFAULT 0 NOT NULL,
    current_sign_in_at timestamp without time zone,
    last_sign_in_at timestamp without time zone,
    current_sign_in_ip character varying(255),
    last_sign_in_ip character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    confirmation_token character varying(255),
    confirmed_at timestamp without time zone,
    confirmation_sent_at timestamp without time zone,
    agreed_to_tos boolean,
    agreed_to_tos_datetime timestamp without time zone,
    agreed_to_tos_ip character varying(255),
    uuid uuid,
    registration_name character varying(255),
    failed_attempts integer DEFAULT 0 NOT NULL,
    unlock_token character varying(255),
    locked_at timestamp without time zone,
    impersonator boolean DEFAULT false,
    authorized_to_invest_status integer DEFAULT 1 NOT NULL,
    user_type integer,
    needs_password_set boolean DEFAULT false,
    context json,
    email_consent_at timestamp without time zone,
    provider character varying,
    uid character varying
);


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: utms; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE utms (
    id integer NOT NULL,
    client_id character varying,
    source character varying,
    medium character varying,
    campaign character varying,
    term character varying,
    content character varying,
    created_at timestamp without time zone NOT NULL,
    other hstore
);


--
-- Name: utms_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE utms_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: utms_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE utms_id_seq OWNED BY utms.id;


--
-- Name: waiting_list_registrations; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE waiting_list_registrations (
    id integer NOT NULL,
    full_name character varying(255),
    email character varying(255),
    category character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: waiting_list_registrations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE waiting_list_registrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: waiting_list_registrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE waiting_list_registrations_id_seq OWNED BY waiting_list_registrations.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY accreditation_documents ALTER COLUMN id SET DEFAULT nextval('accreditation_documents_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY accreditor_qualifications ALTER COLUMN id SET DEFAULT nextval('accreditor_qualifications_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY accreditor_spans ALTER COLUMN id SET DEFAULT nextval('accreditor_spans_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY ach_activities ALTER COLUMN id SET DEFAULT nextval('ach_activities_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY ach_batches ALTER COLUMN id SET DEFAULT nextval('ach_batches_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY ach_debits ALTER COLUMN id SET DEFAULT nextval('ach_debits_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY active_admin_attributes ALTER COLUMN id SET DEFAULT nextval('active_admin_attributes_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY active_admin_comments ALTER COLUMN id SET DEFAULT nextval('active_admin_comments_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY addresses ALTER COLUMN id SET DEFAULT nextval('addresses_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY admin_users ALTER COLUMN id SET DEFAULT nextval('admin_users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY audit_log_entries ALTER COLUMN id SET DEFAULT nextval('audit_log_entries_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY banc_box_escrow_accounts ALTER COLUMN id SET DEFAULT nextval('banc_box_escrow_accounts_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY banc_box_fund_account_transactions ALTER COLUMN id SET DEFAULT nextval('banc_box_fund_account_transactions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY banc_box_fund_escrows ALTER COLUMN id SET DEFAULT nextval('banc_box_fund_escrows_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY banc_box_investor_accounts ALTER COLUMN id SET DEFAULT nextval('banc_box_investor_accounts_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY banc_box_issuer_accounts ALTER COLUMN id SET DEFAULT nextval('banc_box_issuer_accounts_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY banc_box_linked_external_accounts ALTER COLUMN id SET DEFAULT nextval('banc_box_linked_external_accounts_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY bank_accounts ALTER COLUMN id SET DEFAULT nextval('bank_accounts_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY bank_activities ALTER COLUMN id SET DEFAULT nextval('bank_activities_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY bank_activity_details ALTER COLUMN id SET DEFAULT nextval('bank_activity_details_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY campaign_category_sectors ALTER COLUMN id SET DEFAULT nextval('campaign_category_sectors_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY campaign_questions ALTER COLUMN id SET DEFAULT nextval('campaign_questions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY client_trackers ALTER COLUMN id SET DEFAULT nextval('client_trackers_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY companies ALTER COLUMN id SET DEFAULT nextval('companies_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY company_relationships ALTER COLUMN id SET DEFAULT nextval('company_relationships_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY contact_messages ALTER COLUMN id SET DEFAULT nextval('contact_messages_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY corporate_federal_identification_informations ALTER COLUMN id SET DEFAULT nextval('corporate_federal_identification_informations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY documents ALTER COLUMN id SET DEFAULT nextval('documents_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY escrow_caches ALTER COLUMN id SET DEFAULT nextval('escrow_caches_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY escrow_providers ALTER COLUMN id SET DEFAULT nextval('escrow_providers_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY fund_america_requests ALTER COLUMN id SET DEFAULT nextval('fund_america_requests_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY identification_audit_logs ALTER COLUMN id SET DEFAULT nextval('identification_audit_logs_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY investment_emails ALTER COLUMN id SET DEFAULT nextval('investment_emails_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY investment_events ALTER COLUMN id SET DEFAULT nextval('investment_events_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY investments ALTER COLUMN id SET DEFAULT nextval('investments_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY investor_entities ALTER COLUMN id SET DEFAULT nextval('investor_entities_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY investor_faqs ALTER COLUMN id SET DEFAULT nextval('investor_faqs_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY investor_onboarding_events ALTER COLUMN id SET DEFAULT nextval('investor_onboarding_events_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY investor_prerequisites ALTER COLUMN id SET DEFAULT nextval('investor_prerequisites_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY issuer_applications ALTER COLUMN id SET DEFAULT nextval('issuer_applications_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY offering_comments ALTER COLUMN id SET DEFAULT nextval('offering_comments_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY offering_followers ALTER COLUMN id SET DEFAULT nextval('offering_followers_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY offering_funding_details ALTER COLUMN id SET DEFAULT nextval('offering_funding_details_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY offering_messages ALTER COLUMN id SET DEFAULT nextval('offering_messages_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY offerings ALTER COLUMN id SET DEFAULT nextval('offerings_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY personal_federal_identification_informations ALTER COLUMN id SET DEFAULT nextval('personal_federal_identification_informations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY pitch_carousel_images ALTER COLUMN id SET DEFAULT nextval('pitch_carousel_images_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY pitch_carousels ALTER COLUMN id SET DEFAULT nextval('pitch_carousels_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY pitch_content_items ALTER COLUMN id SET DEFAULT nextval('pitch_content_items_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY pitch_images ALTER COLUMN id SET DEFAULT nextval('pitch_images_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY pitch_sections ALTER COLUMN id SET DEFAULT nextval('pitch_sections_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY pitch_text_areas ALTER COLUMN id SET DEFAULT nextval('pitch_text_areas_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY pitch_videos ALTER COLUMN id SET DEFAULT nextval('pitch_videos_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY routing_numbers ALTER COLUMN id SET DEFAULT nextval('routing_numbers_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY securities ALTER COLUMN id SET DEFAULT nextval('securities_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY stakeholders ALTER COLUMN id SET DEFAULT nextval('stakeholders_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY subscriptions ALTER COLUMN id SET DEFAULT nextval('subscriptions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY suitability_audit_logs ALTER COLUMN id SET DEFAULT nextval('suitability_audit_logs_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY utms ALTER COLUMN id SET DEFAULT nextval('utms_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY waiting_list_registrations ALTER COLUMN id SET DEFAULT nextval('waiting_list_registrations_id_seq'::regclass);


--
-- Name: accreditation_documents_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY accreditation_documents
    ADD CONSTRAINT accreditation_documents_pkey PRIMARY KEY (id);


--
-- Name: accreditation_qualifications_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY accreditor_qualifications
    ADD CONSTRAINT accreditation_qualifications_pkey PRIMARY KEY (id);


--
-- Name: accreditation_spans_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY accreditor_spans
    ADD CONSTRAINT accreditation_spans_pkey PRIMARY KEY (id);


--
-- Name: ach_activities_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY ach_activities
    ADD CONSTRAINT ach_activities_pkey PRIMARY KEY (id);


--
-- Name: ach_batches_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY ach_batches
    ADD CONSTRAINT ach_batches_pkey PRIMARY KEY (id);


--
-- Name: ach_debits_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY ach_debits
    ADD CONSTRAINT ach_debits_pkey PRIMARY KEY (id);


--
-- Name: active_admin_attributes_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY active_admin_attributes
    ADD CONSTRAINT active_admin_attributes_pkey PRIMARY KEY (id);


--
-- Name: active_admin_comments_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY active_admin_comments
    ADD CONSTRAINT active_admin_comments_pkey PRIMARY KEY (id);


--
-- Name: addresses_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY addresses
    ADD CONSTRAINT addresses_pkey PRIMARY KEY (id);


--
-- Name: admin_users_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY admin_users
    ADD CONSTRAINT admin_users_pkey PRIMARY KEY (id);


--
-- Name: audit_log_entries_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY audit_log_entries
    ADD CONSTRAINT audit_log_entries_pkey PRIMARY KEY (id);


--
-- Name: banc_box_escrow_accounts_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY banc_box_escrow_accounts
    ADD CONSTRAINT banc_box_escrow_accounts_pkey PRIMARY KEY (id);


--
-- Name: banc_box_fund_account_transactions_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY banc_box_fund_account_transactions
    ADD CONSTRAINT banc_box_fund_account_transactions_pkey PRIMARY KEY (id);


--
-- Name: banc_box_fund_escrows_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY banc_box_fund_escrows
    ADD CONSTRAINT banc_box_fund_escrows_pkey PRIMARY KEY (id);


--
-- Name: banc_box_investor_accounts_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY banc_box_investor_accounts
    ADD CONSTRAINT banc_box_investor_accounts_pkey PRIMARY KEY (id);


--
-- Name: banc_box_issuer_accounts_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY banc_box_issuer_accounts
    ADD CONSTRAINT banc_box_issuer_accounts_pkey PRIMARY KEY (id);


--
-- Name: banc_box_linked_external_accounts_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY banc_box_linked_external_accounts
    ADD CONSTRAINT banc_box_linked_external_accounts_pkey PRIMARY KEY (id);


--
-- Name: bank_activities_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY bank_activities
    ADD CONSTRAINT bank_activities_pkey PRIMARY KEY (id);


--
-- Name: bank_activity_details_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY bank_activity_details
    ADD CONSTRAINT bank_activity_details_pkey PRIMARY KEY (id);


--
-- Name: campaign_category_sectors_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY campaign_category_sectors
    ADD CONSTRAINT campaign_category_sectors_pkey PRIMARY KEY (id);


--
-- Name: campaign_questions_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY campaign_questions
    ADD CONSTRAINT campaign_questions_pkey PRIMARY KEY (id);


--
-- Name: client_trackers_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY client_trackers
    ADD CONSTRAINT client_trackers_pkey PRIMARY KEY (id);


--
-- Name: companies_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY companies
    ADD CONSTRAINT companies_pkey PRIMARY KEY (id);


--
-- Name: company_relationships_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY company_relationships
    ADD CONSTRAINT company_relationships_pkey PRIMARY KEY (id);


--
-- Name: contact_messages_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY contact_messages
    ADD CONSTRAINT contact_messages_pkey PRIMARY KEY (id);


--
-- Name: corporate_federal_identification_informations_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY corporate_federal_identification_informations
    ADD CONSTRAINT corporate_federal_identification_informations_pkey PRIMARY KEY (id);


--
-- Name: documents_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY documents
    ADD CONSTRAINT documents_pkey PRIMARY KEY (id);


--
-- Name: escrow_caches_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY escrow_caches
    ADD CONSTRAINT escrow_caches_pkey PRIMARY KEY (id);


--
-- Name: escrow_providers_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY escrow_providers
    ADD CONSTRAINT escrow_providers_pkey PRIMARY KEY (id);


--
-- Name: fund_america_requests_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY fund_america_requests
    ADD CONSTRAINT fund_america_requests_pkey PRIMARY KEY (id);


--
-- Name: identification_audit_logs_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY identification_audit_logs
    ADD CONSTRAINT identification_audit_logs_pkey PRIMARY KEY (id);


--
-- Name: investment_emails_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY investment_emails
    ADD CONSTRAINT investment_emails_pkey PRIMARY KEY (id);


--
-- Name: investment_states_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY investment_events
    ADD CONSTRAINT investment_states_pkey PRIMARY KEY (id);


--
-- Name: investments_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY investments
    ADD CONSTRAINT investments_pkey PRIMARY KEY (id);


--
-- Name: investor_bank_accounts_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY bank_accounts
    ADD CONSTRAINT investor_bank_accounts_pkey PRIMARY KEY (id);


--
-- Name: investor_entities_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY investor_entities
    ADD CONSTRAINT investor_entities_pkey PRIMARY KEY (id);


--
-- Name: investor_faqs_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY investor_faqs
    ADD CONSTRAINT investor_faqs_pkey PRIMARY KEY (id);


--
-- Name: investor_informations_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY personal_federal_identification_informations
    ADD CONSTRAINT investor_informations_pkey PRIMARY KEY (id);


--
-- Name: investor_onboarding_events_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY investor_onboarding_events
    ADD CONSTRAINT investor_onboarding_events_pkey PRIMARY KEY (id);


--
-- Name: investor_prerequisites_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY investor_prerequisites
    ADD CONSTRAINT investor_prerequisites_pkey PRIMARY KEY (id);


--
-- Name: issuer_applications_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY issuer_applications
    ADD CONSTRAINT issuer_applications_pkey PRIMARY KEY (id);


--
-- Name: offering_comments_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY offering_comments
    ADD CONSTRAINT offering_comments_pkey PRIMARY KEY (id);


--
-- Name: offering_followers_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY offering_followers
    ADD CONSTRAINT offering_followers_pkey PRIMARY KEY (id);


--
-- Name: offering_funding_details_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY offering_funding_details
    ADD CONSTRAINT offering_funding_details_pkey PRIMARY KEY (id);


--
-- Name: offering_messages_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY offering_messages
    ADD CONSTRAINT offering_messages_pkey PRIMARY KEY (id);


--
-- Name: offerings_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY offerings
    ADD CONSTRAINT offerings_pkey PRIMARY KEY (id);


--
-- Name: pitch_carousel_images_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY pitch_carousel_images
    ADD CONSTRAINT pitch_carousel_images_pkey PRIMARY KEY (id);


--
-- Name: pitch_carousels_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY pitch_carousels
    ADD CONSTRAINT pitch_carousels_pkey PRIMARY KEY (id);


--
-- Name: pitch_content_items_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY pitch_content_items
    ADD CONSTRAINT pitch_content_items_pkey PRIMARY KEY (id);


--
-- Name: pitch_images_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY pitch_images
    ADD CONSTRAINT pitch_images_pkey PRIMARY KEY (id);


--
-- Name: pitch_sections_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY pitch_sections
    ADD CONSTRAINT pitch_sections_pkey PRIMARY KEY (id);


--
-- Name: pitch_text_areas_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY pitch_text_areas
    ADD CONSTRAINT pitch_text_areas_pkey PRIMARY KEY (id);


--
-- Name: pitch_videos_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY pitch_videos
    ADD CONSTRAINT pitch_videos_pkey PRIMARY KEY (id);


--
-- Name: routing_numbers_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY routing_numbers
    ADD CONSTRAINT routing_numbers_pkey PRIMARY KEY (id);


--
-- Name: securities_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY securities
    ADD CONSTRAINT securities_pkey PRIMARY KEY (id);


--
-- Name: stakeholders_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY stakeholders
    ADD CONSTRAINT stakeholders_pkey PRIMARY KEY (id);


--
-- Name: subscriptions_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY subscriptions
    ADD CONSTRAINT subscriptions_pkey PRIMARY KEY (id);


--
-- Name: suitability_audit_logs_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY suitability_audit_logs
    ADD CONSTRAINT suitability_audit_logs_pkey PRIMARY KEY (id);


--
-- Name: unique_banc_box_escrow_account_banc_box_issuer_account_id; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY banc_box_escrow_accounts
    ADD CONSTRAINT unique_banc_box_escrow_account_banc_box_issuer_account_id UNIQUE (banc_box_issuer_account_id);


--
-- Name: unique_personal_federal_identification_information_user_id; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY personal_federal_identification_informations
    ADD CONSTRAINT unique_personal_federal_identification_information_user_id UNIQUE (user_id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: utms_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY utms
    ADD CONSTRAINT utms_pkey PRIMARY KEY (id);


--
-- Name: waiting_list_registrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY waiting_list_registrations
    ADD CONSTRAINT waiting_list_registrations_pkey PRIMARY KEY (id);


--
-- Name: index_accreditation_documents_on_accreditation_qualification_id; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE INDEX index_accreditation_documents_on_accreditation_qualification_id ON accreditation_documents USING btree (accreditation_qualification_id);


--
-- Name: index_accreditor_qualifications_on_uuid; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE UNIQUE INDEX index_accreditor_qualifications_on_uuid ON accreditor_qualifications USING btree (uuid);


--
-- Name: index_accreditor_spans_on_investor_entity_id; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE INDEX index_accreditor_spans_on_investor_entity_id ON accreditor_spans USING btree (investor_entity_id);


--
-- Name: index_ach_activities_on_ach_batch_id; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE INDEX index_ach_activities_on_ach_batch_id ON ach_activities USING btree (ach_batch_id);


--
-- Name: index_ach_activities_on_investment_id; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE INDEX index_ach_activities_on_investment_id ON ach_activities USING btree (investment_id);


--
-- Name: index_active_admin_comments_on_author_type_and_author_id; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE INDEX index_active_admin_comments_on_author_type_and_author_id ON active_admin_comments USING btree (author_type, author_id);


--
-- Name: index_active_admin_comments_on_namespace; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE INDEX index_active_admin_comments_on_namespace ON active_admin_comments USING btree (namespace);


--
-- Name: index_active_admin_comments_on_resource_type_and_resource_id; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE INDEX index_active_admin_comments_on_resource_type_and_resource_id ON active_admin_comments USING btree (resource_type, resource_id);


--
-- Name: index_admin_users_on_email; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE UNIQUE INDEX index_admin_users_on_email ON admin_users USING btree (email);


--
-- Name: index_admin_users_on_reset_password_token; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE UNIQUE INDEX index_admin_users_on_reset_password_token ON admin_users USING btree (reset_password_token);


--
-- Name: index_banc_box_investor_accounts_on_investment_id; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE UNIQUE INDEX index_banc_box_investor_accounts_on_investment_id ON banc_box_investor_accounts USING btree (investment_id);


--
-- Name: index_client_trackers_on_trackable_type_and_trackable_id; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE INDEX index_client_trackers_on_trackable_type_and_trackable_id ON client_trackers USING btree (trackable_type, trackable_id);


--
-- Name: index_escrow_caches_on_banc_box_escrow_id; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE UNIQUE INDEX index_escrow_caches_on_banc_box_escrow_id ON escrow_caches USING btree (banc_box_escrow_id);


--
-- Name: index_investments_on_offering_id; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE INDEX index_investments_on_offering_id ON investments USING btree (offering_id);


--
-- Name: index_investments_on_security_id; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE INDEX index_investments_on_security_id ON investments USING btree (security_id);


--
-- Name: index_investor_entities_on_user_id; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE INDEX index_investor_entities_on_user_id ON investor_entities USING btree (user_id);


--
-- Name: index_investors_on_user_id; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE UNIQUE INDEX index_investors_on_user_id ON investors USING btree (user_id);


--
-- Name: index_offering_comments_on_offering_id; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE INDEX index_offering_comments_on_offering_id ON offering_comments USING btree (offering_id);


--
-- Name: index_offering_comments_on_parent_id; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE INDEX index_offering_comments_on_parent_id ON offering_comments USING btree (parent_id);


--
-- Name: index_offering_comments_on_user_id; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE INDEX index_offering_comments_on_user_id ON offering_comments USING btree (user_id);


--
-- Name: index_offering_followers_on_user_id_and_offering_id; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE UNIQUE INDEX index_offering_followers_on_user_id_and_offering_id ON offering_followers USING btree (user_id, offering_id);


--
-- Name: index_offerings_on_vanity_path; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE INDEX index_offerings_on_vanity_path ON offerings USING btree (vanity_path);


--
-- Name: index_pitch_content_items_on_section_id; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE INDEX index_pitch_content_items_on_section_id ON pitch_content_items USING btree (section_id);


--
-- Name: index_routing_numbers_on_routing_number; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE INDEX index_routing_numbers_on_routing_number ON routing_numbers USING btree (routing_number);


--
-- Name: index_securities_on_offering_id; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE INDEX index_securities_on_offering_id ON securities USING btree (offering_id);


--
-- Name: index_subscriptions_on_created_at; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE INDEX index_subscriptions_on_created_at ON subscriptions USING btree (created_at);


--
-- Name: index_subscriptions_on_email; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE UNIQUE INDEX index_subscriptions_on_email ON subscriptions USING btree (email);


--
-- Name: index_users_on_email; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE UNIQUE INDEX index_users_on_email ON users USING btree (email);


--
-- Name: index_users_on_reset_password_token; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE UNIQUE INDEX index_users_on_reset_password_token ON users USING btree (reset_password_token);


--
-- Name: index_users_on_unlock_token; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE UNIQUE INDEX index_users_on_unlock_token ON users USING btree (unlock_token);


--
-- Name: unique_ach_debits_uuid_index; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE UNIQUE INDEX unique_ach_debits_uuid_index ON ach_debits USING btree (uuid);


--
-- Name: unique_investments_uuid_index; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE UNIQUE INDEX unique_investments_uuid_index ON investments USING btree (uuid);


--
-- Name: unique_schema_migrations; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE UNIQUE INDEX unique_schema_migrations ON schema_migrations USING btree (version);


--
-- Name: unique_users_uuid_index; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE UNIQUE INDEX unique_users_uuid_index ON users USING btree (uuid);


--
-- Name: fk_banc_box_escrow_account_banc_box_issuer_account; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY banc_box_escrow_accounts
    ADD CONSTRAINT fk_banc_box_escrow_account_banc_box_issuer_account FOREIGN KEY (banc_box_issuer_account_id) REFERENCES banc_box_issuer_accounts(id);


--
-- Name: fk_personal_federal_identification_information_users; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY personal_federal_identification_informations
    ADD CONSTRAINT fk_personal_federal_identification_information_users FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: fk_rails_565249ecca; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY offering_comments
    ADD CONSTRAINT fk_rails_565249ecca FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: fk_rails_5b7587efe6; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY offering_comments
    ADD CONSTRAINT fk_rails_5b7587efe6 FOREIGN KEY (offering_id) REFERENCES offerings(id);


--
-- PostgreSQL database dump complete
--

SET search_path TO "$user",public;

INSERT INTO schema_migrations (version) VALUES ('20130924192854');

INSERT INTO schema_migrations (version) VALUES ('20130924223449');

INSERT INTO schema_migrations (version) VALUES ('20130925165214');

INSERT INTO schema_migrations (version) VALUES ('20130925174531');

INSERT INTO schema_migrations (version) VALUES ('20130927171216');

INSERT INTO schema_migrations (version) VALUES ('20130927173025');

INSERT INTO schema_migrations (version) VALUES ('20130927190215');

INSERT INTO schema_migrations (version) VALUES ('20130927225343');

INSERT INTO schema_migrations (version) VALUES ('20130930180548');

INSERT INTO schema_migrations (version) VALUES ('20130930180808');

INSERT INTO schema_migrations (version) VALUES ('20130930190617');

INSERT INTO schema_migrations (version) VALUES ('20130930193313');

INSERT INTO schema_migrations (version) VALUES ('20131001005914');

INSERT INTO schema_migrations (version) VALUES ('20131001191426');

INSERT INTO schema_migrations (version) VALUES ('20131001192257');

INSERT INTO schema_migrations (version) VALUES ('20131001224932');

INSERT INTO schema_migrations (version) VALUES ('20131001234824');

INSERT INTO schema_migrations (version) VALUES ('20131002003716');

INSERT INTO schema_migrations (version) VALUES ('20131002171640');

INSERT INTO schema_migrations (version) VALUES ('20131002171937');

INSERT INTO schema_migrations (version) VALUES ('20131002172118');

INSERT INTO schema_migrations (version) VALUES ('20131002193222');

INSERT INTO schema_migrations (version) VALUES ('20131002213824');

INSERT INTO schema_migrations (version) VALUES ('20131004192641');

INSERT INTO schema_migrations (version) VALUES ('20131009005036');

INSERT INTO schema_migrations (version) VALUES ('20131009182511');

INSERT INTO schema_migrations (version) VALUES ('20131009214941');

INSERT INTO schema_migrations (version) VALUES ('20131009215102');

INSERT INTO schema_migrations (version) VALUES ('20131009220519');

INSERT INTO schema_migrations (version) VALUES ('20131009223426');

INSERT INTO schema_migrations (version) VALUES ('20131014234008');

INSERT INTO schema_migrations (version) VALUES ('20131017191048');

INSERT INTO schema_migrations (version) VALUES ('20131017223659');

INSERT INTO schema_migrations (version) VALUES ('20131018184744');

INSERT INTO schema_migrations (version) VALUES ('20131021193138');

INSERT INTO schema_migrations (version) VALUES ('20131024162625');

INSERT INTO schema_migrations (version) VALUES ('20131028203823');

INSERT INTO schema_migrations (version) VALUES ('20131028215541');

INSERT INTO schema_migrations (version) VALUES ('20131028215613');

INSERT INTO schema_migrations (version) VALUES ('20131029142039');

INSERT INTO schema_migrations (version) VALUES ('20131029164915');

INSERT INTO schema_migrations (version) VALUES ('20131029165156');

INSERT INTO schema_migrations (version) VALUES ('20131029190933');

INSERT INTO schema_migrations (version) VALUES ('20131029191619');

INSERT INTO schema_migrations (version) VALUES ('20131030154447');

INSERT INTO schema_migrations (version) VALUES ('20131030191831');

INSERT INTO schema_migrations (version) VALUES ('20131030192255');

INSERT INTO schema_migrations (version) VALUES ('20131031193138');

INSERT INTO schema_migrations (version) VALUES ('20131031193438');

INSERT INTO schema_migrations (version) VALUES ('20131101160520');

INSERT INTO schema_migrations (version) VALUES ('20131105020051');

INSERT INTO schema_migrations (version) VALUES ('20131105215732');

INSERT INTO schema_migrations (version) VALUES ('20131107202645');

INSERT INTO schema_migrations (version) VALUES ('20131107213545');

INSERT INTO schema_migrations (version) VALUES ('20131107221328');

INSERT INTO schema_migrations (version) VALUES ('20131107223351');

INSERT INTO schema_migrations (version) VALUES ('20131107224125');

INSERT INTO schema_migrations (version) VALUES ('20131107224855');

INSERT INTO schema_migrations (version) VALUES ('20131107233133');

INSERT INTO schema_migrations (version) VALUES ('20131111165529');

INSERT INTO schema_migrations (version) VALUES ('20131111212906');

INSERT INTO schema_migrations (version) VALUES ('20131118164453');

INSERT INTO schema_migrations (version) VALUES ('20131119224120');

INSERT INTO schema_migrations (version) VALUES ('20131119234316');

INSERT INTO schema_migrations (version) VALUES ('20131121185431');

INSERT INTO schema_migrations (version) VALUES ('20131123004831');

INSERT INTO schema_migrations (version) VALUES ('20131127190846');

INSERT INTO schema_migrations (version) VALUES ('20131202201922');

INSERT INTO schema_migrations (version) VALUES ('20131203162314');

INSERT INTO schema_migrations (version) VALUES ('20131205172433');

INSERT INTO schema_migrations (version) VALUES ('20131205180459');

INSERT INTO schema_migrations (version) VALUES ('20131210164510');

INSERT INTO schema_migrations (version) VALUES ('20131213165539');

INSERT INTO schema_migrations (version) VALUES ('20131213232916');

INSERT INTO schema_migrations (version) VALUES ('20131216185313');

INSERT INTO schema_migrations (version) VALUES ('20131216222244');

INSERT INTO schema_migrations (version) VALUES ('20131217234651');

INSERT INTO schema_migrations (version) VALUES ('20131218195746');

INSERT INTO schema_migrations (version) VALUES ('20131219165148');

INSERT INTO schema_migrations (version) VALUES ('20131220184313');

INSERT INTO schema_migrations (version) VALUES ('20131230182212');

INSERT INTO schema_migrations (version) VALUES ('20131231234744');

INSERT INTO schema_migrations (version) VALUES ('20140102160026');

INSERT INTO schema_migrations (version) VALUES ('20140108000235');

INSERT INTO schema_migrations (version) VALUES ('20140108184000');

INSERT INTO schema_migrations (version) VALUES ('20140108222637');

INSERT INTO schema_migrations (version) VALUES ('20140121000656');

INSERT INTO schema_migrations (version) VALUES ('20140122191151');

INSERT INTO schema_migrations (version) VALUES ('20140123200719');

INSERT INTO schema_migrations (version) VALUES ('20140124200733');

INSERT INTO schema_migrations (version) VALUES ('20140129200914');

INSERT INTO schema_migrations (version) VALUES ('20140129224356');

INSERT INTO schema_migrations (version) VALUES ('20140130223757');

INSERT INTO schema_migrations (version) VALUES ('20140206000328');

INSERT INTO schema_migrations (version) VALUES ('20140207002109');

INSERT INTO schema_migrations (version) VALUES ('20140210193010');

INSERT INTO schema_migrations (version) VALUES ('20140210193115');

INSERT INTO schema_migrations (version) VALUES ('20140210230812');

INSERT INTO schema_migrations (version) VALUES ('20140210230958');

INSERT INTO schema_migrations (version) VALUES ('20140210235926');

INSERT INTO schema_migrations (version) VALUES ('20140211000107');

INSERT INTO schema_migrations (version) VALUES ('20140211001911');

INSERT INTO schema_migrations (version) VALUES ('20140211001943');

INSERT INTO schema_migrations (version) VALUES ('20140211011830');

INSERT INTO schema_migrations (version) VALUES ('20140211224521');

INSERT INTO schema_migrations (version) VALUES ('20140211224822');

INSERT INTO schema_migrations (version) VALUES ('20140211231111');

INSERT INTO schema_migrations (version) VALUES ('20140211232532');

INSERT INTO schema_migrations (version) VALUES ('20140211233103');

INSERT INTO schema_migrations (version) VALUES ('20140211233204');

INSERT INTO schema_migrations (version) VALUES ('20140211234101');

INSERT INTO schema_migrations (version) VALUES ('20140211234111');

INSERT INTO schema_migrations (version) VALUES ('20140214002940');

INSERT INTO schema_migrations (version) VALUES ('20140220000830');

INSERT INTO schema_migrations (version) VALUES ('20140220003751');

INSERT INTO schema_migrations (version) VALUES ('20140221004714');

INSERT INTO schema_migrations (version) VALUES ('20140225000138');

INSERT INTO schema_migrations (version) VALUES ('20140225191416');

INSERT INTO schema_migrations (version) VALUES ('20140226003831');

INSERT INTO schema_migrations (version) VALUES ('20140226005407');

INSERT INTO schema_migrations (version) VALUES ('20140302195439');

INSERT INTO schema_migrations (version) VALUES ('20140304200619');

INSERT INTO schema_migrations (version) VALUES ('20140305221652');

INSERT INTO schema_migrations (version) VALUES ('20140307202149');

INSERT INTO schema_migrations (version) VALUES ('20140307204930');

INSERT INTO schema_migrations (version) VALUES ('20140313215629');

INSERT INTO schema_migrations (version) VALUES ('20140320010022');

INSERT INTO schema_migrations (version) VALUES ('20140321172948');

INSERT INTO schema_migrations (version) VALUES ('20140321183152');

INSERT INTO schema_migrations (version) VALUES ('20140326171356');

INSERT INTO schema_migrations (version) VALUES ('20140327171357');

INSERT INTO schema_migrations (version) VALUES ('20140327175742');

INSERT INTO schema_migrations (version) VALUES ('20140327184600');

INSERT INTO schema_migrations (version) VALUES ('20140401163108');

INSERT INTO schema_migrations (version) VALUES ('20140401163110');

INSERT INTO schema_migrations (version) VALUES ('20140401230523');

INSERT INTO schema_migrations (version) VALUES ('20140401235200');

INSERT INTO schema_migrations (version) VALUES ('20140402174402');

INSERT INTO schema_migrations (version) VALUES ('20140408171056');

INSERT INTO schema_migrations (version) VALUES ('20140410163621');

INSERT INTO schema_migrations (version) VALUES ('20140410190022');

INSERT INTO schema_migrations (version) VALUES ('20140414191816');

INSERT INTO schema_migrations (version) VALUES ('20140416203543');

INSERT INTO schema_migrations (version) VALUES ('20140422211413');

INSERT INTO schema_migrations (version) VALUES ('20140423181234');

INSERT INTO schema_migrations (version) VALUES ('20140423181414');

INSERT INTO schema_migrations (version) VALUES ('20140423182954');

INSERT INTO schema_migrations (version) VALUES ('20140423200845');

INSERT INTO schema_migrations (version) VALUES ('20140424172344');

INSERT INTO schema_migrations (version) VALUES ('20140430171616');

INSERT INTO schema_migrations (version) VALUES ('20140430202143');

INSERT INTO schema_migrations (version) VALUES ('20140502170547');

INSERT INTO schema_migrations (version) VALUES ('20140502222120');

INSERT INTO schema_migrations (version) VALUES ('20140502230157');

INSERT INTO schema_migrations (version) VALUES ('20140507175345');

INSERT INTO schema_migrations (version) VALUES ('20140507183835');

INSERT INTO schema_migrations (version) VALUES ('20140507184035');

INSERT INTO schema_migrations (version) VALUES ('20140513215517');

INSERT INTO schema_migrations (version) VALUES ('20140514183217');

INSERT INTO schema_migrations (version) VALUES ('20140514223329');

INSERT INTO schema_migrations (version) VALUES ('20140514223618');

INSERT INTO schema_migrations (version) VALUES ('20140515190838');

INSERT INTO schema_migrations (version) VALUES ('20140515234739');

INSERT INTO schema_migrations (version) VALUES ('20140516001548');

INSERT INTO schema_migrations (version) VALUES ('20140516002202');

INSERT INTO schema_migrations (version) VALUES ('20140521223718');

INSERT INTO schema_migrations (version) VALUES ('20140527174115');

INSERT INTO schema_migrations (version) VALUES ('20140530182702');

INSERT INTO schema_migrations (version) VALUES ('20140603005828');

INSERT INTO schema_migrations (version) VALUES ('20140603185310');

INSERT INTO schema_migrations (version) VALUES ('20140603190652');

INSERT INTO schema_migrations (version) VALUES ('20140604001403');

INSERT INTO schema_migrations (version) VALUES ('20140605155458');

INSERT INTO schema_migrations (version) VALUES ('20140605173437');

INSERT INTO schema_migrations (version) VALUES ('20140605184322');

INSERT INTO schema_migrations (version) VALUES ('20140612212252');

INSERT INTO schema_migrations (version) VALUES ('20140616211137');

INSERT INTO schema_migrations (version) VALUES ('20140616211611');

INSERT INTO schema_migrations (version) VALUES ('20140616212358');

INSERT INTO schema_migrations (version) VALUES ('20140618180625');

INSERT INTO schema_migrations (version) VALUES ('20140618225852');

INSERT INTO schema_migrations (version) VALUES ('20140619001552');

INSERT INTO schema_migrations (version) VALUES ('20140619214304');

INSERT INTO schema_migrations (version) VALUES ('20140619232331');

INSERT INTO schema_migrations (version) VALUES ('20140619232838');

INSERT INTO schema_migrations (version) VALUES ('20140627205529');

INSERT INTO schema_migrations (version) VALUES ('20140627205530');

INSERT INTO schema_migrations (version) VALUES ('20140702172719');

INSERT INTO schema_migrations (version) VALUES ('20140702175132');

INSERT INTO schema_migrations (version) VALUES ('20140707194355');

INSERT INTO schema_migrations (version) VALUES ('20140707220149');

INSERT INTO schema_migrations (version) VALUES ('20140709005939');

INSERT INTO schema_migrations (version) VALUES ('20140709185902');

INSERT INTO schema_migrations (version) VALUES ('20140711184505');

INSERT INTO schema_migrations (version) VALUES ('20140714231020');

INSERT INTO schema_migrations (version) VALUES ('20140714233020');

INSERT INTO schema_migrations (version) VALUES ('20140716004741');

INSERT INTO schema_migrations (version) VALUES ('20140718221649');

INSERT INTO schema_migrations (version) VALUES ('20140721211422');

INSERT INTO schema_migrations (version) VALUES ('20140721222619');

INSERT INTO schema_migrations (version) VALUES ('20140724220508');

INSERT INTO schema_migrations (version) VALUES ('20140729002044');

INSERT INTO schema_migrations (version) VALUES ('20140729204639');

INSERT INTO schema_migrations (version) VALUES ('20140730210046');

INSERT INTO schema_migrations (version) VALUES ('20140801224147');

INSERT INTO schema_migrations (version) VALUES ('20140805183236');

INSERT INTO schema_migrations (version) VALUES ('20140805183726');

INSERT INTO schema_migrations (version) VALUES ('20140806004049');

INSERT INTO schema_migrations (version) VALUES ('20140811165014');

INSERT INTO schema_migrations (version) VALUES ('20140811172740');

INSERT INTO schema_migrations (version) VALUES ('20140902170304');

INSERT INTO schema_migrations (version) VALUES ('20140915164812');

INSERT INTO schema_migrations (version) VALUES ('20140915210908');

INSERT INTO schema_migrations (version) VALUES ('20140915210950');

INSERT INTO schema_migrations (version) VALUES ('20140915215300');

INSERT INTO schema_migrations (version) VALUES ('20140916204321');

INSERT INTO schema_migrations (version) VALUES ('20140917214920');

INSERT INTO schema_migrations (version) VALUES ('20140918221412');

INSERT INTO schema_migrations (version) VALUES ('20140922203701');

INSERT INTO schema_migrations (version) VALUES ('20140924171054');

INSERT INTO schema_migrations (version) VALUES ('20140925002631');

INSERT INTO schema_migrations (version) VALUES ('20140925002712');

INSERT INTO schema_migrations (version) VALUES ('20141002174840');

INSERT INTO schema_migrations (version) VALUES ('20141003194212');

INSERT INTO schema_migrations (version) VALUES ('20141008183750');

INSERT INTO schema_migrations (version) VALUES ('20141009160209');

INSERT INTO schema_migrations (version) VALUES ('20141009224535');

INSERT INTO schema_migrations (version) VALUES ('20141014170423');

INSERT INTO schema_migrations (version) VALUES ('20141015201528');

INSERT INTO schema_migrations (version) VALUES ('20141015203505');

INSERT INTO schema_migrations (version) VALUES ('20141017003434');

INSERT INTO schema_migrations (version) VALUES ('20141017174544');

INSERT INTO schema_migrations (version) VALUES ('20141019174347');

INSERT INTO schema_migrations (version) VALUES ('20141019174604');

INSERT INTO schema_migrations (version) VALUES ('20141031000928');

INSERT INTO schema_migrations (version) VALUES ('20141105224030');

INSERT INTO schema_migrations (version) VALUES ('20141106215343');

INSERT INTO schema_migrations (version) VALUES ('20141117190836');

INSERT INTO schema_migrations (version) VALUES ('20141118200749');

INSERT INTO schema_migrations (version) VALUES ('20141118232833');

INSERT INTO schema_migrations (version) VALUES ('20141120221025');

INSERT INTO schema_migrations (version) VALUES ('20141121213652');

INSERT INTO schema_migrations (version) VALUES ('20141126181113');

INSERT INTO schema_migrations (version) VALUES ('20141126213945');

INSERT INTO schema_migrations (version) VALUES ('20141205171241');

INSERT INTO schema_migrations (version) VALUES ('20141205172613');

INSERT INTO schema_migrations (version) VALUES ('20141208213703');

INSERT INTO schema_migrations (version) VALUES ('20141209005652');

INSERT INTO schema_migrations (version) VALUES ('20141209014106');

INSERT INTO schema_migrations (version) VALUES ('20141210001635');

INSERT INTO schema_migrations (version) VALUES ('20141211015345');

INSERT INTO schema_migrations (version) VALUES ('20141212174935');

INSERT INTO schema_migrations (version) VALUES ('20141213044058');

INSERT INTO schema_migrations (version) VALUES ('20141215215323');

INSERT INTO schema_migrations (version) VALUES ('20150106010608');

INSERT INTO schema_migrations (version) VALUES ('20150107194611');

INSERT INTO schema_migrations (version) VALUES ('20150107194627');

INSERT INTO schema_migrations (version) VALUES ('20150109174557');

INSERT INTO schema_migrations (version) VALUES ('20150109234806');

INSERT INTO schema_migrations (version) VALUES ('20150113000142');

INSERT INTO schema_migrations (version) VALUES ('20150129222552');

INSERT INTO schema_migrations (version) VALUES ('20150212010745');

INSERT INTO schema_migrations (version) VALUES ('20150213185748');

INSERT INTO schema_migrations (version) VALUES ('20150213233944');

INSERT INTO schema_migrations (version) VALUES ('20150219003017');

INSERT INTO schema_migrations (version) VALUES ('20150226192745');

INSERT INTO schema_migrations (version) VALUES ('20150305013805');

INSERT INTO schema_migrations (version) VALUES ('20150313214209');

INSERT INTO schema_migrations (version) VALUES ('20150317205331');

INSERT INTO schema_migrations (version) VALUES ('20150429230351');

INSERT INTO schema_migrations (version) VALUES ('20150529174830');

INSERT INTO schema_migrations (version) VALUES ('20150611222727');

INSERT INTO schema_migrations (version) VALUES ('20150616154147');

INSERT INTO schema_migrations (version) VALUES ('20150624003831');

INSERT INTO schema_migrations (version) VALUES ('20150629200953');

INSERT INTO schema_migrations (version) VALUES ('20150630002243');

INSERT INTO schema_migrations (version) VALUES ('20150630132900');

INSERT INTO schema_migrations (version) VALUES ('20150630174425');

INSERT INTO schema_migrations (version) VALUES ('20150706222353');

INSERT INTO schema_migrations (version) VALUES ('20150708231906');

INSERT INTO schema_migrations (version) VALUES ('20150709171655');

INSERT INTO schema_migrations (version) VALUES ('20150710002250');

INSERT INTO schema_migrations (version) VALUES ('20150723193133');

INSERT INTO schema_migrations (version) VALUES ('20150730194501');

INSERT INTO schema_migrations (version) VALUES ('20150811162515');

INSERT INTO schema_migrations (version) VALUES ('20150817174424');

INSERT INTO schema_migrations (version) VALUES ('20150820173827');

INSERT INTO schema_migrations (version) VALUES ('20150826210826');

INSERT INTO schema_migrations (version) VALUES ('20150827171850');

INSERT INTO schema_migrations (version) VALUES ('20150904193442');

INSERT INTO schema_migrations (version) VALUES ('20150904205500');

INSERT INTO schema_migrations (version) VALUES ('20150910173643');

INSERT INTO schema_migrations (version) VALUES ('20150910173751');

INSERT INTO schema_migrations (version) VALUES ('20150911210035');

INSERT INTO schema_migrations (version) VALUES ('20150914180028');

INSERT INTO schema_migrations (version) VALUES ('20150915170227');

INSERT INTO schema_migrations (version) VALUES ('20150916220159');

INSERT INTO schema_migrations (version) VALUES ('20150924175753');

INSERT INTO schema_migrations (version) VALUES ('20150928233512');

INSERT INTO schema_migrations (version) VALUES ('20151002222125');

INSERT INTO schema_migrations (version) VALUES ('20151007164018');

INSERT INTO schema_migrations (version) VALUES ('20151012181407');

INSERT INTO schema_migrations (version) VALUES ('20151014230421');

INSERT INTO schema_migrations (version) VALUES ('20151019193314');

INSERT INTO schema_migrations (version) VALUES ('20151106171637');

INSERT INTO schema_migrations (version) VALUES ('20151106225235');

INSERT INTO schema_migrations (version) VALUES ('20151124151329');

INSERT INTO schema_migrations (version) VALUES ('20151125163431');

INSERT INTO schema_migrations (version) VALUES ('20151203002838');

INSERT INTO schema_migrations (version) VALUES ('20151203192807');

INSERT INTO schema_migrations (version) VALUES ('20151207171205');

INSERT INTO schema_migrations (version) VALUES ('20151214194258');

INSERT INTO schema_migrations (version) VALUES ('20151229232340');

INSERT INTO schema_migrations (version) VALUES ('20160104235136');

INSERT INTO schema_migrations (version) VALUES ('20160106233946');

INSERT INTO schema_migrations (version) VALUES ('20160111131025');

INSERT INTO schema_migrations (version) VALUES ('20160111192906');

INSERT INTO schema_migrations (version) VALUES ('20160120184136');

INSERT INTO schema_migrations (version) VALUES ('20160202005354');

INSERT INTO schema_migrations (version) VALUES ('20160223214926');

INSERT INTO schema_migrations (version) VALUES ('20160224192545');

INSERT INTO schema_migrations (version) VALUES ('20160229143042');

INSERT INTO schema_migrations (version) VALUES ('20160303221847');

INSERT INTO schema_migrations (version) VALUES ('20160310155404');

INSERT INTO schema_migrations (version) VALUES ('20160310160420');

INSERT INTO schema_migrations (version) VALUES ('20160324181045');

INSERT INTO schema_migrations (version) VALUES ('20160401212713');

INSERT INTO schema_migrations (version) VALUES ('20160425223221');

INSERT INTO schema_migrations (version) VALUES ('20160427211747');

INSERT INTO schema_migrations (version) VALUES ('20160503205547');

INSERT INTO schema_migrations (version) VALUES ('20160506001508');

INSERT INTO schema_migrations (version) VALUES ('20160506024821');

INSERT INTO schema_migrations (version) VALUES ('20160512210108');

INSERT INTO schema_migrations (version) VALUES ('20160523211640');

INSERT INTO schema_migrations (version) VALUES ('20160606180346');

INSERT INTO schema_migrations (version) VALUES ('20160603193959');

INSERT INTO schema_migrations (version) VALUES ('20160705215559');

INSERT INTO schema_migrations (version) VALUES ('20160712203137');

INSERT INTO schema_migrations (version) VALUES ('20160715201744');

INSERT INTO schema_migrations (version) VALUES ('20160715202734');

INSERT INTO schema_migrations (version) VALUES ('20160725192101');
