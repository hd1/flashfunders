def escrow_provider_seed_data
  arr = []
  arr.push bancbox_data
  arr.push manual_data
  arr.push fund_america_data
  arr.push first_century_data
  arr.push fintech_clearing_data
  arr
end

def bancbox_data
  {
    id: 1,
    name: 'BancBox',
    provider_key: 'bancbox',
    properties: {
      requires_instructions_international_direct_check: false,
      requires_instructions_international_direct_wire: false,
      requires_instructions_international_spv_check: false,
      requires_instructions_international_spv_wire: false,
      requires_instructions_domestic_direct_check: false,
      requires_instructions_domestic_direct_wire: false,
      requires_instructions_domestic_spv_check: false,
      requires_instructions_domestic_spv_wire: false,
      requires_offering_funding_details: false,
      allows_refund_account: true,
      show_payment_details: false,
      active: false
    },
    default_instructions: {}
  }
end

def manual_data
  {
    id: 2,
    name: 'Manual',
    provider_key: 'manual',
    properties: {
      requires_instructions_international_direct_check: false,
      requires_instructions_international_direct_wire: false,
      requires_instructions_international_spv_check: false,
      requires_instructions_international_spv_wire: false,
      requires_instructions_domestic_direct_check: false,
      requires_instructions_domestic_direct_wire: false,
      requires_instructions_domestic_spv_check: false,
      requires_instructions_domestic_spv_wire: false,
      requires_offering_funding_details: false,
      allows_refund_account: false,
      show_payment_details: false,
      active: true
    },
    default_instructions: {}
  }
end

def fund_america_data
  {
    id: 3,
    name: 'FundAmerica',
    provider_key: 'fund_america',
    properties: {
      requires_instructions_international_direct_check: false,
      requires_instructions_international_direct_wire: true,
      requires_instructions_international_spv_check: false,
      requires_instructions_international_spv_wire: true,
      requires_instructions_domestic_direct_check: true,
      requires_instructions_domestic_direct_wire: true,
      requires_instructions_domestic_spv_check: true,
      requires_instructions_domestic_spv_wire: true,
      requires_offering_funding_details: true,
      provides_realtime_investment_api: true,
      allows_refund_account: false,
      wire_code: 'PMERUS66',
      show_payment_details: true,
      active: true
    },
    default_instructions: {
      international_direct_check: {
        payable_to: "",
        bank_name: "",
        address_1: "",
        address_2: "",
        city: "",
        state: "",
        zipcode: "",
        phone: ""
      },
      international_direct_wire: {
        bank_name: "Pacific Mercantile Bank",
        routing_number: "122242869",
        account_number: "0001476100",
        account_type: "Checking",
        address_1: "949 South Coast Dr.",
        address_2: "",
        city: "Costa Mesa",
        state: "CA",
        zipcode: "92626",
        phone: "",
        beneficiary_name: "Fund America Securities, LLC",
        beneficiary_address_1: "3455 Peachtree Rd, NE",
        beneficiary_address_2: "5th Floor",
        beneficiary_city: "Atlanta",
        beneficiary_state: "GA",
        beneficiary_zipcode: "30326",
        beneficiary_phone: "4043889324"
      },
      international_spv_check: {
        payable_to: "",
        bank_name: "",
        address_1: "",
        address_2: "",
        city: "",
        state: "",
        zipcode: "",
        phone: ""
      },
      international_spv_wire: {
        bank_name: "Pacific Mercantile Bank",
        routing_number: "122242869",
        account_number: "0001476100",
        account_type: "Checking",
        address_1: "949 South Coast Dr.",
        address_2: "",
        city: "Costa Mesa",
        state: "CA",
        zipcode: "92626",
        phone: "",
        beneficiary_name: "Fund America Securities, LLC",
        beneficiary_address_1: "3455 Peachtree Rd, NE",
        beneficiary_address_2: "5th Floor",
        beneficiary_city: "Atlanta",
        beneficiary_state: "GA",
        beneficiary_zipcode: "30326",
        beneficiary_phone: "4043889324"
      },
      domestic_direct_check: {
        payable_to: "Fund America Securities",
        bank_name: "Fund America Securities",
        address_1: "3455 Peachtree Rd, NE",
        address_2: "5th Floor",
        city: "Atlanta",
        state: "GA",
        zipcode: "30326",
        phone: "4043889324"
      },
      domestic_direct_wire: {
        bank_name: "BofI Federal Bank",
        routing_number: "122287251",
        account_number: "200000147849",
        account_type: "Checking",
        address_1: "4350 La Jolla Village Drive",
        address_2: "Suite 140",
        city: "San Diego",
        state: "CA",
        zipcode: "92122",
        phone: "8772478002",
        beneficiary_name: "Fund America Securities, LLC",
        beneficiary_address_1: "3455 Peachtree Rd, NE",
        beneficiary_address_2: "5th Floor",
        beneficiary_city: "Atlanta",
        beneficiary_state: "GA",
        beneficiary_zipcode: "30326",
        beneficiary_phone: "4043889324"
      },
      domestic_spv_check: {
        payable_to: "Fund America Securities",
        bank_name: "Fund America Securities",
        address_1: "3455 Peachtree Road, NE",
        address_2: "5th Floor",
        city: "Atlanta",
        state: "GA",
        zipcode: "30326",
        phone: "4043889324"
      },
      domestic_spv_wire: {
        bank_name: "BofI Federal Bank",
        routing_number: "122287251",
        account_number: "200000147849",
        account_type: "Checking",
        address_1: "4350 La Jolla Village Drive",
        address_2: "Suite 140",
        city: "San Diego",
        state: "CA",
        zipcode: "92122",
        phone: "8772478002",
        beneficiary_name: "Fund America Securities, LLC",
        beneficiary_address_1: "3455 Peachtree Rd, NE",
        beneficiary_address_2: "5th Floor",
        beneficiary_city: "Atlanta",
        beneficiary_state: "GA",
        beneficiary_zipcode: "30326",
        beneficiary_phone: "4043889324"
      }
    }
  }
end

def first_century_data
  {
    id: 4,
    name: 'First Century',
    provider_key: 'first_century',
    properties: {
      requires_instructions_international_direct_check: false,
      requires_instructions_international_direct_wire: false,
      requires_instructions_international_spv_check: false,
      requires_instructions_international_spv_wire: false,
      requires_instructions_domestic_direct_check: true,
      requires_instructions_domestic_direct_wire: true,
      requires_instructions_domestic_spv_check: true,
      requires_instructions_domestic_spv_wire: true,
      requires_offering_funding_details: true,
      allows_refund_account: true,
      wire_code: 'CETYUS66',
      show_payment_details: true,
      active: true
    },
    default_instructions: {
      international_direct_check: {
        payable_to: "",
        bank_name: "",
        address_1: "",
        address_2: "",
        city: "",
        state: "",
        zipcode: "",
        phone: ""
      },
      international_direct_wire: {
        bank_name: "",
        routing_number: "",
        account_number: "",
        account_type: "",
        address_1: "",
        address_2: "",
        city: "",
        state: "",
        zipcode: "",
        phone: "",
        beneficiary_name: "",
        beneficiary_address_1: "",
        beneficiary_address_2: "",
        beneficiary_city: "",
        beneficiary_state: "",
        beneficiary_zipcode: "",
        beneficiary_phone: ""
      },
      international_spv_check: {
        payable_to: "",
        bank_name: "",
        address_1: "",
        address_2: "",
        city: "",
        state: "",
        zipcode: "",
        phone: ""
      },
      international_spv_wire: {
        bank_name: "",
        routing_number: "",
        account_number: "",
        account_type: "",
        address_1: "",
        address_2: "",
        city: "",
        state: "",
        zipcode: "",
        phone: "",
        beneficiary_name: "",
        beneficiary_address_1: "",
        beneficiary_address_2: "",
        beneficiary_city: "",
        beneficiary_state: "",
        beneficiary_zipcode: "",
        beneficiary_phone: ""
      },
      domestic_direct_check: {
        payable_to: "",
        bank_name: "",
        address_1: "",
        address_2: "",
        city: "",
        state: "",
        zipcode: "",
        phone: ""
      },
      domestic_direct_wire: {
        bank_name: "",
        routing_number: "",
        account_number: "",
        account_type: "",
        address_1: "",
        address_2: "",
        city: "",
        state: "",
        zipcode: "",
        phone: "",
        beneficiary_name: "",
        beneficiary_address_1: "",
        beneficiary_address_2: "",
        beneficiary_city: "",
        beneficiary_state: "",
        beneficiary_zipcode: "",
        beneficiary_phone: ""
      },
      domestic_spv_check: {
        payable_to: "",
        bank_name: "",
        address_1: "",
        address_2: "",
        city: "",
        state: "",
        zipcode: "",
        phone: ""
      },
      domestic_spv_wire: {
        bank_name: "",
        routing_number: "",
        account_number: "",
        account_type: "",
        address_1: "",
        address_2: "",
        city: "",
        state: "",
        zipcode: "",
        phone: "",
        beneficiary_name: "",
        beneficiary_address_1: "",
        beneficiary_address_2: "",
        beneficiary_city: "",
        beneficiary_state: "",
        beneficiary_zipcode: "",
        beneficiary_phone: ""
      }
    }
  }
end

def fintech_clearing_data
  {
    id: 5,
    name: 'Fintech Clearing',
    provider_key: 'fintech_clearing',
    properties: {
      requires_instructions_international_direct_check: false,
      requires_instructions_international_direct_wire: true,
      requires_instructions_international_spv_check: false,
      requires_instructions_international_spv_wire: false,
      requires_instructions_domestic_direct_check: false,
      requires_instructions_domestic_direct_wire: true,
      requires_instructions_domestic_spv_check: false,
      requires_instructions_domestic_spv_wire: false,
      requires_offering_funding_details: true,
      provides_realtime_investment_api: true,
      allows_refund_account: false,
      wire_code: 'PMERUS66',
      show_payment_details: true,
      active: true
    },
    default_instructions: {
      international_direct_check: {
        payable_to: "",
        bank_name: "",
        address_1: "",
        address_2: "",
        city: "",
        state: "",
        zipcode: "",
        phone: ""
      },
      international_direct_wire: {
        bank_name: "Pacific Mercantile Bank",
        routing_number: "122242869",
        account_number: "0001476100",
        account_type: "Checking",
        address_1: "949 South Coast Dr.",
        address_2: "",
        city: "Costa Mesa",
        state: "CA",
        zipcode: "92626",
        phone: "",
        beneficiary_name: "Fund America Securities, LLC",
        beneficiary_address_1: "3455 Peachtree Rd, NE",
        beneficiary_address_2: "5th Floor",
        beneficiary_city: "Atlanta",
        beneficiary_state: "GA",
        beneficiary_zipcode: "30326",
        beneficiary_phone: "4043889324"
      },
      international_spv_check: {
        payable_to: "",
        bank_name: "",
        address_1: "",
        address_2: "",
        city: "",
        state: "",
        zipcode: "",
        phone: ""
      },
      international_spv_wire: {
        bank_name: "Pacific Mercantile Bank",
        routing_number: "122242869",
        account_number: "0001476100",
        account_type: "Checking",
        address_1: "949 South Coast Dr.",
        address_2: "",
        city: "Costa Mesa",
        state: "CA",
        zipcode: "92626",
        phone: "",
        beneficiary_name: "Fund America Securities, LLC",
        beneficiary_address_1: "3455 Peachtree Rd, NE",
        beneficiary_address_2: "5th Floor",
        beneficiary_city: "Atlanta",
        beneficiary_state: "GA",
        beneficiary_zipcode: "30326",
        beneficiary_phone: "4043889324"
      },
      domestic_direct_check: {
        payable_to: "Fund America Securities",
        bank_name: "Fund America Securities",
        address_1: "3455 Peachtree Rd, NE",
        address_2: "5th Floor",
        city: "Atlanta",
        state: "GA",
        zipcode: "30326",
        phone: "4043889324"
      },
      domestic_direct_wire: {
        bank_name: "BofI Federal Bank",
        routing_number: "122287251",
        account_number: "200000147849",
        account_type: "Checking",
        address_1: "4350 La Jolla Village Drive",
        address_2: "Suite 140",
        city: "San Diego",
        state: "CA",
        zipcode: "92122",
        phone: "8772478002",
        beneficiary_name: "Fund America Securities, LLC",
        beneficiary_address_1: "3455 Peachtree Rd, NE",
        beneficiary_address_2: "5th Floor",
        beneficiary_city: "Atlanta",
        beneficiary_state: "GA",
        beneficiary_zipcode: "30326",
        beneficiary_phone: "4043889324"
      },
      domestic_spv_check: {
        payable_to: "Fund America Securities",
        bank_name: "Fund America Securities",
        address_1: "3455 Peachtree Road, NE",
        address_2: "5th Floor",
        city: "Atlanta",
        state: "GA",
        zipcode: "30326",
        phone: "4043889324"
      },
      domestic_spv_wire: {
        bank_name: "BofI Federal Bank",
        routing_number: "122287251",
        account_number: "200000147849",
        account_type: "Checking",
        address_1: "4350 La Jolla Village Drive",
        address_2: "Suite 140",
        city: "San Diego",
        state: "CA",
        zipcode: "92122",
        phone: "8772478002",
        beneficiary_name: "Fund America Securities, LLC",
        beneficiary_address_1: "3455 Peachtree Rd, NE",
        beneficiary_address_2: "5th Floor",
        beneficiary_city: "Atlanta",
        beneficiary_state: "GA",
        beneficiary_zipcode: "30326",
        beneficiary_phone: "4043889324"
      }
    }
  }
end
