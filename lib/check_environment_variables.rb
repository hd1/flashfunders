# This class allows you to check whether the environment variables you added to the 
# branch you are deploying exist in the environment you are deploying to. It gets called 
# automatically during deploy, via the environment_variables:check rake task, like this:
# bundle exec rake environment_variables:check['review']
# If you have added environment variables to the branch you are deploying, and those 
# environment variables are not on the deploy box, the deploy will fail, and notify you of
# the missing environment variables.
# Please note that this task 1) works only for the standard branches / deploy env pairs,
# ([:acceptance, :review], [:production, :master]), and 2) does not check
# the validity of any environment variables you set.
class CheckEnvironmentVariables

  attr_reader :remote

  def initialize(environment)
    raise "#{environment} is not permitted" unless permitted_envs.include?(environment)
    @remote = @environment = environment
    add_and_fetch_remote
  end

  def unmatched_keys
    local_environment_variable_keys - remote_environment_variable_keys
  end

  def unmatched_keys?
    unmatched_keys.length > 0
  end

  private

  def remote_environment_variable_keys
    @remote_environment_variable_keys ||=
      `heroku config -a flashfunders-#{remote}`
        .split("\n")
        .map{|e| e.scan(/^(.*?)\:/)}
        .flatten
        .compact
        .uniq
  end

  def local_environment_variable_keys
    @local_environment_variable_keys ||= begin
      output = []
      diff.split("\n").each do |line|
        output += line.scan(/ENV\[.(.*?).\]/)
        output += line.scan(/ENV\.fetch\(.(.*?).\)/)
      end
      output.flatten
    end
  end

  def permitted_envs
    ['review', 'production']
  end

  def remote_branch
    'master'
  end

  def local_branch
    case @environment
    when 'review'
      'acceptance'
    when 'production'
      'master'
    end
  end

  def add_and_fetch_remote
    `git remote add #{remote} git@heroku.com:flashfunders-#{remote}.git`
    `git fetch #{remote}`
  end

  def diff
    `git diff -U0 #{remote}/#{remote_branch} #{local_branch}  -- lib/ config/ gems/ app/ bin/ | grep -v '^+++' | grep '^+' | grep ENV`
  end

end
