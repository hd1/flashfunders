require 'logger'
require 'singleton'

module FintechClearing
  class Config

    class AlreadyConfigured < StandardError ; end
    class NotYetConfigured < StandardError ; end

    include Singleton
    class << self
      private :instance
    end

    ATTRIBUTES = [
      :api_keys, :basic_auth, :logger, :api_url
    ]

    attr_reader(*ATTRIBUTES)

    def self.configuration
      raise NotYetConfigured unless instance.instance_variable_get(:@configured)
      instance
    end

    def self.configure(&block)
      raise AlreadyConfigured if instance.instance_variable_get(:@configured)

      config_proxy = Struct.new(*ATTRIBUTES).new

      yield config_proxy

      config_proxy.each_pair do |attr_name, attr_val|
        instance.instance_variable_set("@#{attr_name}", attr_val)
      end

      unless instance.logger
        instance.instance_variable_set(:@logger, Logger.new(STDOUT))
      end

      instance.instance_variable_set(:@configured, true)
    end
  end
end
