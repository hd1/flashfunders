require 'httparty'
require 'fintech_clearing/config'

module FintechClearing
  class ServiceError < RuntimeError; end

  class BaseService
    include HTTParty

    attr_reader :logger

    def initialize(config = FintechClearing::Config.configuration)
      @config = config
      @api_url = config.api_url
      self.class.base_uri @api_url
      @http_auth_headers = {}

      @logger = config.logger
    end

    def post(path, body, opts={})
      set_auth(opts[:api])
      self.class.post(path, query: body, headers: request_header)
    end

    def put(path, body, opts={})
      set_auth(opts[:api])
      self.class.put(path, query: body, headers: request_header)
    end

    private

    def set_auth(_api)
      return true if @http_auth_headers.present?

      if @config.basic_auth[:email] && @config.basic_auth[:password]
        @http_auth_headers = get_auth_header_by_login
      else
        # The backend has not supported ditect token yet.
        # @http_auth_headers['access-token'] =  @config.api_keys[api].presence || @config.api_keys[:default]
      end
    end

    def request_header
      @http_auth_headers.transform_values(&:join).slice('access-token', 'client', 'expiry', 'uid')
    end

    def get_auth_header_by_login
      response = self.class.post('/sessions/v1/sign_in', query: @config.basic_auth)

      unless response.success?
        error_message = sprintf('Authentication failed: %s',  response)
        raise ServiceError.new(error_message)
      end

      response.headers
    end
  end
end
