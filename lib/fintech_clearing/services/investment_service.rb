require 'fintech_clearing/services/base_service'
require 'fintech_clearing/models/investment'

module FintechClearing
  class InvestmentService < BaseService
    def create(investment_attributes, opts={})
      logger.info('Creating new investment for offering ID: ' + investment_attributes[:investment][:offering_uuid])

      response = post('/investments/v1/', investment_attributes, opts)

      unless response.success?
        error_message = sprintf('unable to create investment: %s',  response)
        raise ServiceError.new(error_message)
      end

      Investment.new(response)
    end

    def update(investment_attributes, opts={})
      investment_uuid = investment_attributes[:investment][:uuid]
      raise ArgumentError.new('missing required attribute: `id`') unless investment_uuid
      logger.info('Updating investment with id: ' + investment_uuid)

      response = put("/investments/v1/#{investment_uuid}", investment_attributes, opts)

      unless response.success?
        error_message = sprintf('unable to update investment: %s',  response)
        raise ServiceError.new(error_message)
      end
      Investment.new(response)
    end
  end
end
