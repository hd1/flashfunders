class FintechClearing::BaseModel

  def initialize(response)
    response = JSON.parse(response.parsed_response)
    response.each do |key, val|
      instance_variable_set("@#{key}", val)
    end
  end
end
