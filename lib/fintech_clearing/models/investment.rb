require 'fintech_clearing/models/base_model'

class FintechClearing::Investment < FintechClearing::BaseModel
  attr_reader :uuid, :offering_uuid, :amount, :funding_method,
    :investor_first_name, :investor_last_name
end
