# MultipartSanitizer
# * Taken from https://github.com/rack/rack/issues/903
# * Handles multipart/form-data where there's empty content in the boundary value
class MultipartSanitizer
  def initialize(app)
    @app = app
  end

  def call(env)
    if env['CONTENT_TYPE'] =~ /multipart\/form-data/
      begin
        Rack::Multipart.parse_multipart(env)
      rescue EOFError => ex
        # set content-type to multipart/form-data without the boundary part
        # to handle the case where empty content is submitted
        env['CONTENT_TYPE'] = 'multipart/form-data'
      end
    end
    @app.call(env)
  end
end
