# This class was taken from this page:
# https://github.com/plataformatec/devise/wiki/How-To:-Redirect-to-a-specific-page-when-the-user-can-not-be-authenticated

class OverrideDefaultRedirect < Devise::FailureApp
  def redirect_url
    if warden_message == :unconfirmed
      if params[:user].blank? || params[:user][:email].blank?
        new_user_confirmation_path
      else
        new_user_confirmation_path(user: { email: params['user']['email'] })
      end
    elsif redirect_to_sign_up?
      new_user_registration_path
    elsif warden_message == :timeout
      store_location_for(:user, params[:return_to])
      params['controller'] == 'sessions' ? user_log_me_out_path : attempted_path
    else
      new_user_session_path
    end
  end

  def redirect
    store_location!
    case warden_message
      when :timeout
        flash[:timedout] = true
      else
        flash[:alert] = i18n_message
    end
    redirect_to redirect_url
  end

  # You need to override respond to eliminate recall
  def respond
    if http_auth?
      http_auth
    else
      redirect
    end
  end

  def redirect_to_sign_up?
    investment_flow? || issuer_applications_flow?
  end

  def investment_flow?
    is_investment_controller = params[:controller].match(/(invest|investment_flow)\/investments$/).present?
    is_action_valid = (params[:action] == 'new' || params[:action] == 'create')

    is_investment_controller && is_action_valid
  end

  def issuer_applications_flow?
    (params[:controller] == 'issuer_applications') && (params[:action] == 'new' || params[:action] == 'create')
  end
end
