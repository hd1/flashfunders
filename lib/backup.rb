class Backup
  MAX_AGE_IN_DAYS = 1
  MIN_FILE_SIZE = 100000
  BUCKET_NAME = ENV['PGBACKUPS_BUCKET']
  SUB_BUCKET = "pgbackups/production"

  def self.operational?(connection = nil)
    new(connection).operational?
  end

  attr_reader :connection

  def initialize(connection = nil)
    @connection = connection
    @connection ||=  Fog::Storage.new({
      provider: 'AWS',
      aws_access_key_id: ENV['PGBACKUPS_AWS_ACCESS_KEY_ID'],
      aws_secret_access_key: ENV['PGBACKUPS_AWS_SECRET_ACCESS_KEY'],
    })
  end

  def operational?
    backup = last_backup
    backup["LastModified"] > MAX_AGE_IN_DAYS.days.ago && backup["Size"] > MIN_FILE_SIZE if backup
  end 

  private 
  
  def last_backup
    bucket_contents.sort_by { |file| file["LastModified"] }.last
  end

  def bucket_contents
    begin
      response = connection.get_bucket(BUCKET_NAME, :prefix => SUB_BUCKET)
      while response.body['IsTruncated']
        response = connection.get_bucket(BUCKET_NAME, :prefix => SUB_BUCKET, :marker => response.body['Contents'].last['Key'])
      end
      response.body["Contents"]
    rescue
      []
    end
  end
end

