module CarrierWaveTimestamp

  def filename
    return unless original_filename.present? and super.present?

    @name ||= begin
      file_parts = super.split('.')
      file_parts[-2] = file_parts[-2] + "-#{timestamp}"
      file_parts.join('.')
    end
  end

  private

  def timestamp
    var = :"@#{mounted_as}_timestamp"
    model.instance_variable_get(var) or model.instance_variable_set(var, Time.now.to_s(:number))
  end

end

