namespace :escrow_providers do
  desc 'Create EscrowProvider rows'
  task :create => :environment do
    require Rails.root + 'db/escrow_provider_seed_data.rb'
    escrow_provider_seed_data.each do |provider|
      ep = EscrowProvider.find_or_create_by(id: provider[:id])
      ep.name = provider[:name]
      ep.provider_key = provider[:provider_key]
      ep.properties = provider[:properties]
      ep.default_instructions = provider[:default_instructions]
      ep.save
    end
  end

  desc 'Link Offerings to the appropriate EscrowProvider row'
  task :link_offerings => :environment do
    ActiveRecord::Base.transaction do
      class Offering < ActiveRecord::Base
        enum provider: { banc_box: 1, manual: 2, fund_america: 3, first_century: 4, fintech_clearing: 5 }
      end
      Offering.all.each do |offering|
        offering.update_attribute(:escrow_provider_id, Offering.providers[offering.provider]) if offering.escrow_provider_id.blank?
      end
    end
  end
end
