require 'open-uri'

namespace :legal do

  desc 'Generate all documents'
  task generate_all: [:environment, :generate_privacy_policy, :generate_terms_of_use, :generate_cip_notice, :generate_email_disclaimer]  do
  end

  desc 'Generate an updated privacy policy'
  task generate_privacy_policy: :environment do
    privacy_template = Rails.root.join('app', 'views', 'static', 'privacy.html')
    privacy_policy_url = 'https://docs.google.com/document/d/1vyDui3ZX7WXAWauwonnU11sFJw1IZ37WVJBG1rjSqVg/pub?embedded=true'


    generate_legal_document(privacy_template, privacy_policy_url)
  end

  desc 'Generate an updated terms of use'
  task generate_terms_of_use: :environment do
    terms_template = Rails.root.join('app', 'views', 'static', 'tos.html')
    terms_url = 'https://docs.google.com/document/d/1fdO2hp6QxSSlVsa2Y2V3-LLgFoumPuapGcga-xdwZMM/pub?embedded=true'

    generate_legal_document(terms_template, terms_url)
  end

  desc 'Generate an updated email disclaimer'
  task generate_email_disclaimer: :environment do
    [{ url:      'https://docs.google.com/document/d/1CElKoTFgo-KzYd7BJ1vsVC9RJ-mmvmLlPIL6NyjJxXo/pub?embedded=true',
       template: 'email_disclaimer.html' },
     { url:      'https://docs.google.com/document/d/1e4StHp09nPKAggiPD5g7F6SKBbcL-bji474MvIZYxus/pub?embedded=true',
       template: 'email_disclaimer_to_investor.html' },
     { url:      'https://docs.google.com/document/d/1bnWyXcm6OoqMclQGuBmfjXZknQX2Ai2eqZPu4W6Oyuc/pub?embedded=true',
       template: 'email_disclaimer_to_issuer.html' },
    ].each { |pair|
      template = Rails.root.join('app', 'views', 'static', pair[:template])
      generate_legal_document(template, pair[:url])
    }
  end

  desc 'Generate an updated cip notice'
  task generate_cip_notice: :environment do
    cip_notice_template = Rails.root.join('app', 'views', 'static', 'cip_notice.html')
    cip_notice_url = 'https://docs.google.com/document/d/1HoBHXke8nYpFpo7FAWdv-bAteV0v4lBOdLT5Pp_H6qI/pub?embedded=true'

    generate_legal_document(cip_notice_template, cip_notice_url)
  end

  desc 'Generate an updated Electronic Consent & Delivery Agreement'
  task generate_econsent: :environment do
    econsent_template = Rails.root.join('app', 'views', 'static', 'econsent.html')
    econsent_url = 'https://docs.google.com/document/d/163c_q7W1_L-CL4riQqUvHYt0ewITaMmygwfvuRy87Cs/pub?embedded=true'

    style, content = pull_styles_and_content_from_google_doc(econsent_url)

    generate_legal_document(econsent_template, style, content)
  end

  def pull_styles_and_content_from_google_doc(url)
    doc = Nokogiri::HTML(open(url))

    style = doc.css('html head style').to_s
    content = doc.css('html body').children.to_s
    [style, content]
  end

  def generate_legal_document(template_path, url)
    style, content = pull_styles_and_content_from_google_doc(url)

    File.open(template_path, 'w') do |file|
      file.puts "#{style}\n<div class='legal-container'><div class='legal-document'>\n#{content}\n</div></div>"
    end
  end
end
