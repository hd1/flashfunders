namespace :accreditation do

  desc 'Update cached accreditation statuses'
  task :update_statuses => :environment do
    Accreditation::StatusUpdater.new.update!
  end

  desc 'Update accreditor spans'
  task :update_spans => :environment do
    Accreditor::SpanCreator.populate_all
  end

  desc 'Update accreditor spans'
  task :update_all => [:update_spans, :update_statuses]

end
