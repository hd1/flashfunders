namespace :hubspot do
  desc 'Update user information'
  task :update_user_info => :environment do
    report_results(*HubspotTracker.batch_update_user_info(0, true))
  end

  desc 'Update subscriber information'
  task :update_subscriber_info => :environment do
    report_results(*HubspotTracker.batch_update_subscriber_info(0, true))
  end

  desc 'Update all'
  task :update_all => :environment do
    report_results(*HubspotTracker.batch_update_subscriber_info(0, true))
    report_results(*HubspotTracker.batch_update_user_info(0, true))
  end

  desc 'Create missing deals'
  task :create_deals => :environment do
    report_results(*HubspotTracker.batch_create_deals(0, true))
  end

  desc 'Update user type for users with offerings or issuer applications'
  task :update_issuers => :environment do
    (IssuerApplication.all.map(&:user) + Offering.all.map(&:user) + Offering.all.map(&:issuer_user)).uniq.each do |user|
      if user.present? && !user.user_type_entrepreneur?
        user.update_attribute(:user_type, :user_type_entrepreneur)
      end
    end
    report_results(*HubspotTracker.batch_update_issuer_info(0, true))
  end

  def report_results(counter, errors)
    puts "\n#{counter} records sent to HubSpot"
    if errors.count > 0
      puts "There were #{errors.count} errors"
      errors.each do |e|
        puts e.inspect
      end
    end
  end

end
