# This task checks whether the environment variables you added to the branch you are
# deploying exist in the environment you are deploying to. It gets called automatically
# during deploy, like this:
# bundle exec rake environment_variables:check['review']
# If you have added environment variables to the branch you are deploying, and those 
# environment variables are not on the deploy box, the deploy will fail, and notify you of
# the missing environment variables.
# Please note that this task 1) works only for the standard branches / deploy env pairs,
# ([:acceptance, :review], [:production, :master]), and 2) does not check
# the validity of any environment variables you set.
namespace :environment_variables do
  require_relative '../../lib/check_environment_variables'
  task :check, [:rails_env] do |t, args|
    c = CheckEnvironmentVariables.new(args[:rails_env])
    raise "ENV vars #{c.unmatched_keys} are not deployed to #{c.remote}" if c.unmatched_keys?
  end
end
