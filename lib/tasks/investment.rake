namespace :investments do
  desc 'Add pending_email_sent and cancelled_email_sent to a hstore'
  task add_investment_email_flags: :environment do |_, args|
    Investment.find_each(batch_size: 500) do |investment|
      investment.update_attributes(
        flags: {
          pending_email_sent: investment.admin_email_sent,
          cancelled_email_sent: false })
    end
  end
end
