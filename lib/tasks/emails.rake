namespace :emails do

  desc 'Send out emails to investors when offering is counter signed'
  task counter_signed: :environment do |_, args|
    dry_run = args.extras.count > 0
    CounterSignedEmail.where(sent_at: nil).each { |entry| send_investment_email(entry, dry_run) }
  end

  desc 'Send out emails to investors when offering limit is reached'
  task limit_reached: :environment do |_, args|
    dry_run = args.extras.count > 0
    LimitReachedEmail.where(sent_at: nil).each { |entry| send_investment_email(entry, dry_run) }
  end

  desc 'Send out queued emails to investors'
  task send_all: :environment do |_, args|
    dry_run = args.extras.count > 0
    InvestmentEmail.where(sent_at: nil).each { |entry| send_investment_email(entry, dry_run) }
  end

  desc 'Report on emails sent to investors'
  task report_sent: :environment do
    puts "Sent At\tType\tOffering\t# Sent\tInvestment Ids"
    InvestmentEmail.where.not(sent_at: nil).each do |entry|
      offering = Offering.find(entry.offering_id)
      puts "#{entry.sent_at.to_s}\t#{entry.class.to_s}\t#{offering.company.name} [#{offering.id}]\t#{entry.investment_ids.count}\t#{entry.investment_ids}"
    end
  end

  def send_investment_email(investment_email, dry_run = false)
    investment_ids = [].tap do |ids|
      investment_email.investments.each do |investment|
        offering_presenter ||= OfferingPresenter.new(investment.offering)
        ids << investment.id
        unless dry_run
          case investment_email.class.to_s
            when 'LimitReachedEmail'
              InvestmentMailer.offering_target_reached(investment.id, investment_email.data).deliver_later
            when 'CounterSignedEmail'
              InvestmentMailer.counter_signed(investment.id, investment_email.data).deliver_later
          end
        end
      end
    end
    investment_email.update_attributes(investment_ids: investment_ids, sent_at: Time.now) unless dry_run
    puts "#{investment_ids.count} emails sent. #{investment_ids}"
  end

end

