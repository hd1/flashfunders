namespace :cache do
  desc 'Force writes a cache on pages identified in CacheManager'
  task :recache_pages => :environment do
    CacheManager.paths.keys.each do |page|
      if CacheManager.schedule_jobs(page).empty?
        CacheManager.delay.recursive_recache_page(page)
      end
    end
  end
end
