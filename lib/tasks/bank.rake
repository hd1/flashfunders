namespace :bank do |args|
  desc 'Update routing numbers'

  require 'optparse'
  task :update_routing_numbers => :environment do

    instructions = <<-EOS

    Instructions
    ============

    Go to this URL: https://www.frbservices.org/EPaymentsDirectory/download.html
    Prove you are not a robot, then select:
         "Receive All FedACH Participant RDFIs with commercial receipt volume"
    This will show the entire list in your browser.
    Wait for the download to finish, then save the contents to a text file
    Upload the file to an S3 folder (the flashfunding folder is convenient for this)
    Mark the file as public (access by everyone)

    To run this rake task: from the command line, type:

      bundle exec rake bank:update_routing_numbers -- '-f https://s3-us-west-2.amazonaws.com/flashfunding/ACH_routing_data_april_2016.txt'

      Don't forget the extra two dashes before the first parameter (that's for rake)
      Don't forget the quotes around the -f block. If you need to add -d or -v, they go outside the quote block. Weird, huh?

    EOS

    options = OpenStruct.new
    options.ach_file = ""
    options.dry_run = false
    options.verbose = false

    opt_parser = OptionParser.new do |opts|
      opts.banner = 'Usage: [bundle exec] rake bank:update_routing_numbers [options]'
      opts.on('-f', '--file [file name]', 'input file name') { |v| options[:ach_file] = v }
      opts.on('-d', '--dry-run', 'Dry run - parse input file, but no database updates') { |v| options[:dry_run] = true }
      opts.on('-v', '--verbose', 'Print each row to be inserted in the database') { |v| options[:verbose] = true }

      opts.on_tail('-h', '--help', 'Show this message') do
        puts opts
        puts instructions
        exit
      end
    end

    args = opt_parser.order!(ARGV) {}
    opt_parser.parse!(args)

    unless options[:ach_file].present?
      log_it "Cannot find input file #{options[:ach_file]}"
      raise "Terminating"
    end
    log_it "Reading file #{options[:ach_file]}"
    puts "DRY RUN - no database update will be performed" if options[:dry_run]

    db_config = ActiveRecord::Base.configurations[Rails.env]

    database_connection = Sequel.postgres(
      host: db_config['host'],
      database: db_config['database'],
      user: db_config['username'],
      password: db_config['password'],
      port: db_config['port']
    )

    log_it "Updating database host: #{db_config['host']}, database: #{db_config['database']} username: #{db_config['username']}"
    RoutingNumberUpdater.run(database_connection, options[:ach_file], options[:dry_run], options[:verbose])
  end

  desc 'List most recent data for escrows'
  task :update_escrows => :environment do
    EscrowUpdater.new.fetch_and_process_all_escrows
  end
end

def log_it(msg)
  puts msg
end
