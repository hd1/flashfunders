namespace :user do

  desc 'Updated US citizen status and residency'
  task update_us_citizen_status_and_residency: :environment do
    User.transaction do
      User.joins(:investor_entities).each do |user|
        most_recent_entity = user.investor_entities.order(updated_at: :desc).first
        src_pfii = most_recent_entity.personal_federal_identification_information
        unless src_pfii.blank?
          dst_pfii = user.personal_federal_identification_information || PersonalFederalIdentificationInformation.new
          dst_pfii.us_citizen = src_pfii.us_citizen if user.us_citizen.nil?
          dst_pfii.country = src_pfii.country if user.country.blank?
          user.personal_federal_identification_information = dst_pfii
          user.save
        end
      end
    end
  end

  desc 'Confirm all unconfirmed users'
  task confirm_all: :environment do
    auto_confirm = "auto_confirmed at: #{Time.now}"
    User.transaction do
      User.where(confirmed_at: nil).each do |user|
        user.update_attribute(:confirmed_at, auto_confirm)
      end
    end
  end

end
