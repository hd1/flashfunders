desc 'export dns zonefiles'
task :export_dns => :environment do
  require 'mkmf'
  puts <<-EOF
=========
Hello,

You must set your personal AWS access keys before running this script. The app keys are not sufficient.

Try to have a nice day. ^_^

Sincerly,

~~==~~DNS Export Script~~==~~
========
EOF


  export_dns('flashdough.com')
  export_dns('flashfunders.com')
end

def export_dns(domain)
  # See https://github.com/barnybug/cli53
  raise RuntimeError.new('Must install cli53 before exporting dns') unless find_executable('cli53')

  out_file = File.join(Rails.root, 'config', 'zone_files', domain)
  cmd = "cli53 export #{domain} > #{out_file}"
  puts cmd
  `#{cmd}`
end

