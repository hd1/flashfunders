namespace :db do
  desc 'Check the timestamp and size of the most recent database backup'
  task :check_most_recent_backup => :environment do
    ERRMSG = 'A recent backup has not been found.'
    unless Backup.operational?
      Honeybadger.notify(error_message: ERRMSG)
      Rails.logger.warn(ERRMSG)
    end
  end

  desc 'Confirm the users that signed up before we enabled confirmation'
  task :confirm_legacy_users => :environment do
    confirmation_enabled_at = '2016-03-16 15:19 PDT'
    User.where('created_at < ?', confirmation_enabled_at)
      .where(confirmed_at: nil, needs_password_set: false)
      .update_all({ confirmed_at: Time.now })
  end

  desc 'Sanitize the data for non-production usage'
  task sanitize: :environment do
    DatabaseSanitizer.sanitize
  end

end
