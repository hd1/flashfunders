require 'bigdecimal'
require 'active_support'

class Currency

  include ActiveSupport::NumberHelper

  attr_reader :amount

  class << self
    def parse(value)
      new(value) do
        raise TypeError, "Cannot convert '#{value}' into a Currency object"
      end
    end
  end

  def initialize(value, &block)
    @amount = convert_to_decimal(value)

    # when unable to properly initialize amount
    yield if block_given? && @amount.nil?

    @amount ||= 0
  end

  def to_d
    amount
  end

  def to_i
    amount.round(0).to_i
  end

  def to_s(options = {})
    to_str(options)
  end

  def to_str(options = {})
    number_to_currency(amount, options)
  end

  private

  def convert_to_decimal(value)
    if value.is_a?(String)
      number = value.sub(/^\$/, '').gsub(',', '')
      number =~ /\A(\d+(\.\d+)?)\z/ ? BigDecimal(number) : nil
    else
      BigDecimal(value.to_s)
    end
  end

end
