module CSVGenerator
  class Base
    attr_reader :filename

    def initialize(csv_type, columns, resources, opts={})
      @csv_type_class = Object.const_get("CSVGenerator::#{csv_type.to_s.camelcase}")
      @columns = columns
      @resources = resources
      @filename = set_filename(opts)
    end

    def csv_file
      {data: generate, filename: @filename}
    end

    def generate
      if @csv_type_class.respond_to?(:get_row)
        csv_data { |resource| @csv_type_class.get_row(resource) }
      end
    end

    def set_filename(opts={})
      filename = @csv_type_class.filename(opts) if @csv_type_class.respond_to?(:filename)
      (filename || 'report') + "_#{Time.zone.now.strftime('%F')}.csv"
    end

    private

    def safe_resources
      if @csv_type_class.respond_to?(:permitted_resources)
        @resources.select { |resource| resource.class.in?(@csv_type_class.permitted_resources) }
      end
    end

    def csv_data(&block)
      CSV.generate do |csv|
        csv << @columns
        safe_resources.to_a.each { |resource| csv << yield(resource).to_a }
      end
    end

  end
end
