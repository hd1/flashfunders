module CSVGenerator
  class InvestmentActivity
    class << self
      def get_row(resource)
        if resource.is_a?(InvestmentPresenter)
          row_for_investor(resource)
        elsif resource.is_a?(OfferingFollower)
          row_for_follower(resource)
        end
      end

      def filename(opts)
        "Investment_Activity_#{opts[:offering_name]}"
      end

      def permitted_resources
        [InvestmentPresenter, OfferingFollower]
      end

      private

      def row_for_investor(investment)
        [
          investment.formatted_updated_at,
          investment.investor_name,
          investment.investor_email,
          investment.formatted_amount,
          investment.followed_date.try(:strftime, '%b %e, %Y %I:%M %P %Z'),
          investment.created_at.try(:strftime, '%b %e, %Y %I:%M %P %Z'),
          investment.has_signed_docs?,
          investment.escrowed?
        ]
      end

      def row_for_follower(follower)
        [
          "",
          follower.user.registration_name,
          follower.user.email,
          "",
          follower.updated_at.try(:strftime, '%b %e, %Y %I:%M %P %Z'),
          "",
          "",
          ""
        ]
      end

    end
  end
end
