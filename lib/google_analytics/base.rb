require 'staccato'

module GoogleAnalytics
  class Base
    attr_reader :tracker

    def initialize
      @tracker = Staccato.tracker(ENV['GOOGLE_ANALYTICS_KEY'])
    end

  end
end
