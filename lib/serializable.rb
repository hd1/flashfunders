module Serializable

  def self.with_serialization(xact_key=nil)
    xact_num = xact_key.is_a?(Numeric) ? xact_key :  my_hash(xact_key)
    begin
      lock(xact_num)
      yield
    ensure
      unlock(xact_num)
    end
  end

  private

  def self.lock(key)
    ActiveRecord::Base.connection.execute("SELECT pg_advisory_lock(#{key});")
  end

  def self.unlock(key)
    ActiveRecord::Base.connection.execute("SELECT pg_advisory_unlock(#{key});")
  end

  def self.my_hash(key)
    Digest::MD5.hexdigest(key)[0..14].to_i(16)
  end

end
