module StoreAccessorBoolean
  module ActiveRecordExtensions
    def store_accessor_boolean(store_attribute, *keys)
      store_accessor store_attribute, *keys
      keys = keys.flatten
      _store_accessors_module.module_eval do
        keys.each do |key|
          define_method("#{key}?".to_sym) do
            ActiveRecord::ConnectionAdapters::Column::TRUE_VALUES.include?(read_store_attribute(store_attribute, key))
          end
        end
      end
    end
  end
end