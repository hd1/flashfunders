# This file is used by Rack-based servers to start the application.

# Unicorn worker killer
require 'unicorn/worker_killer'

# Max requests per worker
use Unicorn::WorkerKiller::MaxRequests, 512, 1024

# Max memory size (RSS) per worker
use Unicorn::WorkerKiller::Oom, (350*(1024**2)), (512*(1024**2))

require ::File.expand_path('../config/environment',  __FILE__)
run Rails.application
