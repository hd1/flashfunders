class EinFormatValidator < ActiveModel::Validator
  def validate(record)
    unless valid_ein?(record.ein)
      record.errors.add(:ein, 'Provide EIN in xxxxxxxxx format.')
    end
  end

  private

  def valid_ein?(ein)
    ein =~ /\A\d{9}\Z/
  end
end