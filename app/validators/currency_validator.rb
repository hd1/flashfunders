class CurrencyValidator < ActiveModel::EachValidator

  @@default_options = {}

  def self.default_options
    @@default_options
  end

  def validate_each(record, attribute, value)
    options = @@default_options.merge(self.options)

    unless value && valid_currency?(value)
      record.errors.add(attribute, options[:message] || :invalid)
    end
  end

  private

  def valid_currency?(value)
    !!Currency.parse(value)
  rescue TypeError
    false
  end

end

