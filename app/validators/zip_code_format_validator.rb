class ZipCodeFormatValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    unless valid_standard_zip?(value) || valid_extended_zip?(value)
      record.errors[attribute] << 'Provide zip code in xxxxx or xxxxx-xxxx format.'
    end
  end

  private

  def valid_standard_zip?(zip_code)
    zip_code =~ /\A\d{5}\z/
  end

  def valid_extended_zip?(zip_code)
    zip_code =~ /\A\d{5}-\d{4}\z/
  end
end