class WebUrlFormatValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    return unless value.present?

    record.errors[attribute] << 'Provide URL in http://example.com format.' and return false unless value =~ URI::regexp

    begin
      url = URI.parse(value)

      if !valid_protocol?(url)
        record.errors[attribute] << "Provide a valid URL in these schemes: #{allowed_protocols.join(', ')}."
      elsif url.host.nil?
        record.errors[attribute] << 'Missing URL domain'
      end
    rescue URI::InvalidURIError => e
      record.errors[attribute] << 'is invalid'
    end
  end

  private

  def allowed_protocols
    @protocols ||= options.fetch(:allowed_protocols, [:http, :https])
  end

  def valid_protocol?(url)
    allowed_protocols.any? { |protocol| url.kind_of?("URI::#{protocol.upcase}".constantize) }
  end
end
