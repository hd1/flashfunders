class CastStringAttribute
  class InputNotAString < ArgumentError;
  end

  class << self
    def boolean(string_attr)
      return if string_attr.nil?
      verify_input_is_string(string_attr)

      case string_attr
        when 'true'
          true
        when 'false'
          false
        else
          string_attr
      end
    end

    def integer(string_attr)
      return if string_attr.blank?
      verify_input_is_string(string_attr)

      string_attr.to_i
    end

    def array_of_integers(array_of_strings)
      array_of_strings ||= []
      array_of_strings.map(&:presence).compact.map do |string_attr|
        self.integer(string_attr)
      end
    end

    private

    def verify_input_is_string(string_attr)
      raise InputNotAString unless string_attr.is_a?(String)
    end
  end
end