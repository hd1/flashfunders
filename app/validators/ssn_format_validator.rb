class SsnFormatValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    unless valid_ssn?(value)
      record.errors[attribute] << 'Provide SSN in xxx-xx-xxxx format.'
    end
  end

  private

  def valid_ssn?(ssn)
    return false if ssn.blank?

    ssn.gsub(/[\W_]/, '') =~ /\A\d{9}\z/
  end
end