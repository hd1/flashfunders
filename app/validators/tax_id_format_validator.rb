class TaxIdFormatValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    unless valid_tax_id?(value)
      record.errors[attribute] << 'Provide a nine digit tax ID.'
    end
  end

  private

  def valid_tax_id?(tax_id)
    return false if tax_id.blank?

    tax_id.gsub(/[\W_]/, '') =~ /\A\d{9}\z/
  end
end