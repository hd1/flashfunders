class UsPhoneNumberFormatValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    record.errors[attribute] << 'Provide phone number in 555-555-5555 format.' unless valid_us_phone_number?(value)
  end

  private

  def valid_us_phone_number?(phone_number)
    phone_number.blank? || phone_number.gsub(/[\W_]/, '') =~ /\A1?\d{10}\z/
  end
end
