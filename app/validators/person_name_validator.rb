class PersonNameValidator < ActiveModel::EachValidator

  def validate_each(model, attribute, value)
    message = options[:message] || 'Only letters, periods, numbers and spaces allowed'

    if value.to_s.match(/[^\sa-z0-9. ]/i)
      model.errors.add(attribute, message)
    end
  end

end
