class ValidVanityUrlValidator < ActiveModel::EachValidator

  def validate_each(record, attribute, value)
    unless valid_vanity_url?(value)
      record.errors[attribute] << 'Invalid URL. Please try another URL.'
    end
  end

  private

  def valid_vanity_url?(val)
    !rails_routes.include?(val)
  end

  def rails_routes
    routes = Rails.application.routes.routes
    routes.collect {|r| r.path.spec.try(:right) ? r.path.spec.right.left.to_s : r.path.spec.to_s }.uniq
  end

end