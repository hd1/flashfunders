class MustBeEighteenYearsOldValidator < ActiveModel::EachValidator

  def validate_each(record, attribute, value)
    return unless value.present?
    begin
      date = Date.strptime(value, '%m/%d/%Y')
    rescue ArgumentError
      return
    end

    record.errors[attribute] << 'Must be 18 years old.' if date + 18.years > Date.today
  end
end
