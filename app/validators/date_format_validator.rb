class DateFormatValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    return unless value.present?
    record.errors[attribute] << 'Provide date in mm/dd/yyyy format.' unless value.match(/(?:0?[1-9]|1[0-2])\/(?:0?[1-9]|[1-2]\d|3[01])\/\d{4}/)
    begin
      Date.strptime(value, '%m/%d/%Y')
    rescue ArgumentError
      record.errors[attribute] << 'Is not an actual calendar date.'
    end
  end
end