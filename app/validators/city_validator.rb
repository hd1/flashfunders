class CityValidator < ActiveModel::EachValidator

  def validate_each(model, attribute, value)
    value.to_s.scan(/[^\sa-z]/i).each do |bad_char|
      model.errors[attribute] << 'Only letters and spaces allowed'
    end
    model.errors[attribute].uniq!
  end

end

