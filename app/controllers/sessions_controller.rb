class SessionsController < Devise::SessionsController

  layout 'registration'

  prepend_before_action :skip_devise_tracking, only: :session_info
  before_action :store_return_to, only: [:new, :create]
  skip_before_action :track_visitor, only: :session_info

  def new
    old_flash = session['flash']
    flash.alert.clear if flash.alert
    unless old_flash.blank?
      flashes = old_flash['flashes']
      unless flashes.blank?
        set_flash_message(:alert, :timeout) if flashes['timedout']
      end
    end
    super
  end

  def create
    if self.resource = warden.authenticate(auth_options)
      handle_successful_sign_in
    else
      handle_temporary_user_sign_in || handle_unsuccessful_sign_in
    end
  end

  def destroy
    # clear impersonation session when signed out. There is no reason to keep it
    # in session when no current_user.
    super do
      session[:impersonator_id] = nil
      cookies.delete('_startup')
    end
  end

  def session_info
    if user_session && user_session["last_request_at"]
      self.resource = warden.authenticate(auth_options)
      set_last_request
      render json: { success: true }.merge(timeout_info(user_session["last_request_at"]))
    else
      head :unauthorized
    end
  end

  def log_me_out
    if current_user
      destroy
    else
      redirect_to user_logout_path
    end
  end

  def logout
    return log_me_out if current_user

    self.resource = resource_class.new(sign_in_params)
  end

  private

  def set_last_request
    last_event = Time.now.to_i - params[:last_event].to_i if params[:last_event]
    user_session["last_request_at"] = last_event if last_event && user_session["last_request_at"] < last_event
  end

  def timeout_info(last_request_at)
    {
      total_session_time: Devise.timeout_in,
      warning_time: (ENV['USER_TIMEOUT_WARNING'] || 5).to_i * 60,
      time_left: last_request_at - resource.timeout_in.ago.to_i
    }
  end

  def skip_devise_tracking
    warden.request.env["devise.skip_trackable"] = true unless params[:reset_timer] == 'true'
  end

  def handle_temporary_user_sign_in
    self.resource = User.find_for_authentication(email: sign_in_params[:email])
    if resource&.has_no_password? && !resource.confirmed?

      respond_to do |format|
        format.html { redirect_to new_user_registration_path(email: resource.email, registration_name: resource.registration_name) }
        format.js { render :create, locals: { success: false, needs_confirmation: true } }
      end
    end
  end

  def handle_unsuccessful_sign_in
    self.resource = resource_class.new(sign_in_params)
    clean_up_passwords(resource)

    if locked_account?(resource)
      flash.now.alert = t('devise.sessions.locked')
    else
      flash.now.alert = t('devise.failure.invalid')
    end
    flash[:analytics_sign_in_email] = { failed: resource.email }

    respond_to do |format|
      format.html { render :new }
      format.js { render :create, locals: { success: false, needs_confirmation: false } }
    end
  end

  def handle_successful_sign_in
    set_flash_message(:notice, :signed_in) if is_flashing_format?
    sign_in(resource_name, resource)
    flash[:analytics_sign_in_email] = { success: current_user.email }
    set_offering_context
    yield resource if block_given?

    respond_to do |format|
      format.html { respond_with resource, location: after_sign_in_path_for(resource) }
      format.js { render :create, locals: {
        success: true,
        redirect_to: after_sign_in_path_for(resource)
      } }
    end
  end

  def store_return_to
    return unless params[:return_to]

    return_to_path, fragment = params[:return_to].split("#")
    session[:user_return_to_fragment] = fragment if fragment
    store_location_for(:user, return_to_path)
  end

  def locked_account?(resource)
    User.find_by_email(resource.email).try(:access_locked?)
  end

  def after_sign_in_path_for(resource)
    context = resource.restore_context(session)
    context[stored_location_key_for(:user)] || context['path'] || super(resource)
  end

end
