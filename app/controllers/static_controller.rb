class StaticController < ApplicationController
  include ActiveSupport::NumberHelper
  include AnalyticsTracker
  layout 'style_guide', only: [:style_guide]

  skip_before_filter :authenticate_user!
  skip_before_action :store_location, only: [:regions, :lookup_routing_number]
  skip_before_action :track_visitor, only: [:regions, :lookup_routing_number]
  skip_before_action :set_error_context, only: [:regions, :lookup_routing_number]
  skip_before_action :flash, only: [:regions, :lookup_routing_number]
  before_action :track_facebook_pixel_campaign, only: [
    :startups, :investors, :team, :press, :contact, :tos, :privacy, :econsent, :faq_startup, :faq_investor
  ]

  def regions
    @@state_options ||= State.get_regions.to_json

    render json: @@state_options
  end

  def lookup_routing_number
    entry = RoutingNumber.find_by_routing_number(params[:routing_number])

    if entry.blank?
      render json: { status: 'notFound' }
    else
      render json: entry.as_json(except: [:id]).merge(status: 'found')
    end
  end

  def style_guide
    render 'static/style_guide/index'
  end

  private

  def track_facebook_pixel_campaign
    track_facebook_pixel(ff_facebook_pixel_id, 'ViewContent')
  end

  def store_location
    # prevent storing url to session
    super unless request.path == terms_path
  end
end
