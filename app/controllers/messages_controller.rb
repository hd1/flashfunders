class MessagesController < ApplicationController
  skip_before_filter :authenticate_user!, :only => :show
  include FormAuditing

  before_action do
    @offering = set_offering_context(offering_id)
  end
  before_action :message_authorization_gate, except: [:index]

  def index
    messages  = OfferingMessage.distinct_offerings_for_user(current_user.id)

    messages  |= OfferingMessage.distinct_users_for_offering(@offering.id) if @offering
    @messages = message_presenter(messages.sort_by(&:created_at).reverse)
  end

  def show
    if current_user
      if my_offering?
        message_scope.sent_by_user.update_all(unread: false)
        @contact = User.find(user_id)
      end

      if my_user?
        message_scope.sent_by_offering.update_all(unread: false)
      end

      @messages = message_scope.sort_by(&:created_at).reverse

      @message = OfferingMessage.new(message_criteria)
    else # not logged in
      user = find_user
      if user.present? && !user.confirmed?
        # unconfirmed user - confirm them and return here
        redirect_to(confirmation_url(user, uuid: params[:user_id], return_to: request.original_url))
      else # confirmed or unknown user - make them sign in and return here
        redirect_to new_user_session_path(return_to: request.original_url)
      end
    end
  end

  def create
    @offering_message = OfferingMessage.create(permitted_params.merge(sent_by_user: !my_offering?))
    if @offering_message.persisted?
      send_email
      audit_form!
      flash[:notice] = I18n.t('offering_message.confirm')
    else
      flash[:alert] = I18n.t('offering_message.error.blank_message')
    end
    redirect_to dashboard_conversation_path(permitted_params[:offering_id], user_id: @offering_message.user.uuid)
  end

  def change_context_path(id)
    issuer_change_context_path(id: id, target: dashboard_conversations_path)
  end

  private

  def find_user
    if params[:sent_by_issuer]
      User.find_by_uuid(params[:user_id])
    else
      offering = Offering.find(offering_id)
      offering.issuer_user unless offering.nil?
    end
  end

  def message_presenter(messages)
    messages.map { |message| MessagePresenter.new(message, current_offering: @offering) }
  end

  def can_message_from_my_offering
    my_offering? && (my_investor? || my_commenter? || conversation_exists?)
  end

  def can_message_to_offering
    my_user? && conversation_exists?
  end

  def message_authorization_gate
    if current_user && !(can_message_from_my_offering || can_message_to_offering)
      render_404
    end
  end

  def auditor
    Auditor.new
  end

  def audit_params
    permitted_params
  end

  def conversation_exists?
    message_scope.exists?
  end

  def message_scope
    OfferingMessage.where(message_criteria)
  end

  def my_offering?
    @offering && offering_id == @offering.id
  end

  def my_user?
    user_id == current_user.id
  end

  def my_investor?
    Investment.where(message_criteria).exists?
  end

  def my_commenter?
    OfferingComment.visible.where(message_criteria).exists?
  end

  def user_id
    @user_id ||= begin
      if action_name.to_sym == :create
        permitted_params[:user_id] = User.uuid_to_id(permitted_params[:user_id])
      else
        params[:user_id] = User.uuid_to_id(params[:user_id])
      end
    end
  end

  def message_criteria
    { user_id: user_id, offering_id: offering_id }
  end

  def send_email
    if @offering_message.sent_by_user
      OfferingMailer.delay.message_from_user(offering_id, user_id, @offering_message)
    else
      OfferingMailer.delay.message_from_issuer(@offering.id, user_id, @offering_message)
    end
  end

  def permitted_params
    @permitted_params ||= params.require(:offering_message).permit(:offering_id, :user_id, :body)
  end

  def offering_id
    (action_name.to_sym == :create ? permitted_params[:offering_id] : params[:id]).to_i
  end

end
