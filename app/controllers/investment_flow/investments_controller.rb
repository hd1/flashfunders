module InvestmentFlow

  class InvestmentsController < InvestController
    include AnalyticsTracker

    before_action :authenticate_user!

    def new
      @form ||= InvestmentFlowForm.new(form_params, @offering, @investment)
      track_facebook_pixel(@offering.facebook_pixel_id, 'AddToCart')
      track_google_analytics(@offering.google_tracking_id)
      track_google_adwords(@offering.google_adwords_conversion_id, @offering.google_adwords_conversion_label)
    end

    def create
      @form = InvestmentFlowForm.new(valid_params, @offering, @investment)
      if @form.valid?
        @form.create_or_update_investment(current_user.id)
        audit_form!
        redirect_for_investment
      else
        render :new
      end
    end

    def edit
      @form ||= InvestmentFlowForm.new(form_params, @offering, @investment)
      render :new, locals: { path: 'edit' }
    end

    def audit_params
      valid_params.merge({ shares: @investment.shares,
                           percent_ownership: @investment.percent_ownership })
    end

    private

    def internal_investments_sum
      @internal_investments_sum ||= OfferingInvestments.new(nil, current_user).reg_c_investments_total(@investment.id)
    end

    def reg_c_cutoff
      @reg_c_cutoff ||= 20000
    end

    def reg_c_min
      @offering_presenter = OfferingPresenter.new(@offering)
      @offering_presenter.minimum_investment_amount
    end

    def reg_s_min
      reg_c_min
    end

    def form_params
      {}.tap do |h|
        h[:internal] = internal_investments_sum
        h[:international] = current_user.international?
        h[:reg_c_cutoff] = reg_c_cutoff
        h[:reg_c_min] = reg_c_min
        h[:reg_s_min] = reg_s_min
        h[:amount] = @investment.amount
        unless @investment.calc_data.blank?
          h[:selector] = @investment.reg_selector
          h[:international] = @investment.reg_s_investor?
          h[:income] = @investment.reg_c_income
          h[:net_worth] = @investment.reg_c_net_worth
          h[:external] = @investment.reg_c_external
        end
      end
    end

    def valid_params
      {}.tap do |h|
        params.require(:investment).permit(*InvestmentFlowForm::ATTRIBUTES).each do |k, v|
          if InvestmentFlowForm::ALPHA_ATTRIBUTES.include?(k.to_sym)
            h[k] = v
          else
            h[k] = v.gsub(/[^0-9.-]/, '').to_f
          end
        end
        h[:server_internal] = internal_investments_sum
        h[:server_reg_c_cutoff] = reg_c_cutoff
      end
    end

  end

end
