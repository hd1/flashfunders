module InvestmentFlow
  class FundsController < InvestController

    include FormAuditing
    include InvestmentSummaryAssignment
    include AnalyticsTracker
    include FundAmericaEscrowHelper

    before_action :assign_existing_accounts
    before_action :assign_company
    before_action :assign_investment_summary

    skip_before_action :assign_progress_bar, only: [:complete]

    def new
      @form = InvestorFundForm.new(current_user, investment)
      render_new_form(false, default_funding_method)
    end

    def create
      @form = InvestorFundForm.new(current_user, investment, { params: permitted_params })
      if @form.valid?
        Serializable.with_serialization(investment.id) do
          @form.save
          audit_form!
          controller_params = {
            ip_address: request.remote_ip,
            user_agent: request.user_agent
          }
          @investment.security.escrow_provider.notify_completion!(@investment, controller_params)
          send_payment_instructions_email(investment)
        end
        redirect_to complete_offering_investment_flow_fund_path(@offering)
      else
        @investment.funding_method = permitted_params[:funding_method]
        render_new_form(false, @investment.funding_method)
      end
    end

    def edit
      @bank_account = Bank::InvestorAccount.find(investment.investor_account_id)
      existing_data = {
        user_id: current_user.id,
        bank_account_holder: @bank_account.account_holder,
        bank_account_number: @bank_account.mask(2),
        bank_account_routing: @bank_account.routing_number,
        bank_account_type: @bank_account.account_type,
        funding_type: @bank_account.funding_type }

      @form = InvestorFundForm.new(current_user, investment, { params: existing_data })
      render_new_form(true, investment.funding_method)
    end

    def update
      previous_funding_method = @investment.funding_method
      params = permitted_params
      investor_account = Bank::InvestorAccount.find(@investment.investor_account_id)
      if params[:bank_account_number] && params[:bank_account_number].include?('**')
        params[:bank_account_number] = investor_account.account_number
      end
      @form = InvestorFundForm.new(current_user, investment, { params: params })
      if @form.valid?
        @form.update investor_account
        send_payment_instructions_email(investment) unless previous_funding_method == @investment.reload.funding_method
        audit_form!
        if @investment.ach_debit_failed?
          resubmit_ach_instruction(@investment)
        end
        redirect_for_investment
      else
        render_new_form(true, params[:funding_method])
      end
    end

    def complete
      load_data
      @show_expanded = true
      track_facebook_pixel(@offering.facebook_pixel_id, 'Purchase')
      track_facebook_pixel(ff_facebook_pixel_id, 'Purchase')
      track_google_analytics(@offering.google_tracking_id)
      track_google_adwords(@offering.google_adwords_conversion_id, @offering.google_adwords_conversion_label)
      render layout: 'application'
    end

    private

    def default_funding_method
      options = investment.funding_options
      if options.length == 1
        options[0]
      end
    end

    def render_new_form(suppress_navbar = false, funding_method)
      @suppress_navbar = suppress_navbar
      gon.fundingMethod = funding_method
      gon.offeringId = @offering.id
      gon.achRefundTitle = I18n.t('fund_flow.ach.link_refund_account.title')
      gon.achRefundDesc = I18n.t('fund_flow.ach.link_refund_account.description', more_link: view_context.link_to(I18n.t('fund_flow.ach.link_refund_account.link_title'), "#{faq_investor_path}#how_to_invest_flow-what-happens-if-an-offering-fails-to-close", target: '_blank')).html_safe
      gon.wireRefundTitle = I18n.t('fund_flow.wire.link_refund_account.title')
      gon.wireRefundDesc = I18n.t('fund_flow.wire.link_refund_account.description')
      render :new, locals: { funding_method: funding_method, funding_options: investment.funding_options }
    end

    def assign_existing_accounts
      @existing_accounts = current_user.investor_accounts
    end

    def save_linked_bank_account(bank_account_info)
      investor_account = Bank::InvestorAccount.new({
                                                     user_id: current_user.id,
                                                     account_holder: bank_account_info[:bank_account_holder],
                                                     account_number: bank_account_info[:bank_account_number],
                                                     routing_number: bank_account_info[:bank_account_routing],
                                                     account_type: bank_account_info[:bank_account_type],
                                                     funding_type: bank_account_info[:funding_type],
                                                   })
      if investor_account.save
        create_banking_data(investor_account)
        audit_form!
        true
      else
        @errors = investor_account.errors
      end

    end

    def permitted_params
      params.require(:investor_fund_form).permit(InvestorFundForm::ATTRIBUTES)
    end

    def auditor
      InvestorFundFormAuditor.new
    end

    def audit_params
      permitted_params
    end

    def create_banking_data(investor_account)
      investment.update_attributes(investor_account_id: investor_account.id)
    end

    def campaign_presenter
      OfferingPresenter.new(offering)
    end

    def social_presenter
      SocialPresenterFactory.build(offering_url(investment.offering_id, protocol: 'http'), t('show_campaign.social.twitter.account'), campaign_presenter.company_name, offering.tagline)
    end

    def load_data
      investment_presenter = InvestmentPresenter.new(investment)
      @investment_hash = {
        investment_summary: @investment_summary,
        investment: investment_presenter,
        campaign: campaign_presenter,
        funding_detail: investment_presenter.funding_detail_domestic,
        funding_international_detail: investment_presenter.funding_detail_international,
        social: social_presenter,
      }
    end

    def send_payment_instructions_email(investment)
      queue_minimum_reached_email(investment)
      if investment.security.escrow_provider.show_payment_details?
        InvestmentMailer.funding_method_selected(investment.id).deliver_now
      end
    end

    def queue_minimum_reached_email(investment)
      if investment.is_reg_c?
        offering_presenter = OfferingPresenter.new(investment.offering)
        if offering_presenter.cf_minimum_reached? && LimitReachedEmail.where(offering_id: investment.offering.id).empty?
          LimitReachedEmail.new(investment.offering, trigger_investment_id: investment.id).save
        end
      end
    end

    def resubmit_ach_instruction(investment)
      investment.update_attribute(:fund_america_id, nil)
      investment.investor_entity.update_attributes(fund_america_id: nil, fund_america_ach_authorization_id: nil)
      controller_params = {
        ip_address: request.remote_ip,
        user_agent: request.user_agent
      }
      investment.security.escrow_provider.notify_completion!(investment, controller_params)
      investment.select_funding_method!
    end
  end
end
