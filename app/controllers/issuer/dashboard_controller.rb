module Issuer
  class DashboardController < ApplicationController

    before_action do
      @offering = set_offering_context(params[:id])
    end

    ITEMS_PER_PAGE = 10
    UP_ARROW       = '&and;' #0x2304.chr
    DOWN_ARROW     = '&or;' #0x5e.chr

    def show
      if @offering
        set_params
        respond_to do |format|
          format.html do
            @offering_presenter = OfferingPresenter.new(@offering)
          end
          format.js do
            case params[:section]
              when 'investments'
                render partial: 'issuer/dashboard/panel_ajax', locals: { section: params[:section], data: @investment_data }
            end
          end
        end
      else
        render_404
      end
    end

    def export_csv
      csv_info = case params[:section]
        when 'investments'
          investment_activity_csv
        end

      send_data csv_info[:data], filename: csv_info[:filename]
    end

    private

    def set_params
      @investment_data = gen_params(:investments, investments,
                                    %w(started signed funded messages),
                                    params[:section] == 'investments' ? params : {})
      @offering.dashboard_last_viewed_at = Time.now
      @offering.save
    end

    def gen_params(kind, items, headers, params)
      number_to_display = (params[:count] || 0).to_i + ITEMS_PER_PAGE
      page              = params[:page] || 0
      sort_by           = params[:sort_by] || 'updated_at'
      sort_dir          = params[:sort_dir] || 'desc'
      sorted_items      = sort_items(items, sort_dir, sort_by)
      displayed         = [number_to_display, items.count].min

      build_params_hash(displayed, headers, items, kind, number_to_display, page, params, sort_by, sort_dir, sorted_items)
    end

    def build_params_hash(displayed, headers, items, kind, number_to_display, page, params, sort_by, sort_dir, sorted_items)
      { sort_by:      sort_by,
        sort_dir:     sort_dir,
        page:         page,
        remaining:    items.count - number_to_display,
        displayed:    displayed,
        headers:      headers,
        sort_headers: gen_sortable_headers(kind, sort_by, sort_dir),
        items:        Kaminari.paginate_array(sorted_items, total_count: sorted_items.count)
                        .page(page)
                        .per(displayed),
        last_viewed:  params[:last_viewed] || @offering.dashboard_last_viewed_at || Time.parse(I18n.t('issuer-dashboard.dawn'))
      }
    end

    def sort_items(items, sort_dir, sort_by)
      items.sort { |a, b|
        if sort_dir == 'asc'
          a.send(sort_by) <=> b.send(sort_by)
        else
          b.send(sort_by) <=> a.send(sort_by)
        end
      }
    end

    def include_user?(user)
      (user.email =~ /@flashfunders\.com/).nil? && !user.rejected?
    end

    def investments
      @all_investments ||= Investment.where(offering_id: @offering.id)
                             .select { |i| include_user?(i.user) }
                             .map { |i| InvestmentPresenter.new(i) }
    end

    def gen_sortable_headers(spec, current_column, current_dir)
      header_spec[spec].map do |h|
        sort_dir   = 'desc'
        sort_arrow = ''
        if current_column == h[:sort]
          if current_dir == 'desc'
            sort_dir   = 'asc'
            sort_arrow = '&nbsp;' + UP_ARROW
          else
            sort_arrow = '&nbsp;' + DOWN_ARROW
          end
        end
        { title: "#{h[:title]}#{sort_arrow}", sort_by: h[:sort], sort_dir: sort_dir, id: h[:title].parameterize.underscore, section: spec }
      end
    end

    def header_spec
      { investments:
               [{ title: 'last active', sort: 'updated_at' },
                { title: 'name', sort: 'investor_name' },
                { title: 'email', sort: 'investor_email' },
                { title: 'amount', sort: 'amount' }]
      }
    end

    def investment_activity_csv
      columns = (header_spec[:investments].map {|hash| hash[:title]} | %w(followed started\ investment signed funded))
      resources = sort_items(investments, params[:sort_dir], params[:sort_by])
      resources = resources + followers
      CSVGenerator::Base.new(:investment_activity, columns, resources, offering_name: @offering.company.name).csv_file
    end

    def followers
      OfferingFollower.follower.where(offering_id: @offering.id)
    end

  end
end
