module Issuer
  class IssuerController < ApplicationController

    def change_context
      set_offering_context(params[:id])
      redirect_to URI.parse(params[:target]).path
    end

  end
end
