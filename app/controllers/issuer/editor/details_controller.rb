module Issuer
  module Editor
    class DetailsController < BaseController

      before_action :assign_offering
      before_action :assign_uploader

      def edit
        offering.company ||= Company.new
      end

      def update
        if offering.update_attributes(permitted_params)
          audit_form!
          flash.notice = "Your changes have been saved"
          redirect_to edit_issuer_editor_detail_path
        else
          flash.now.alert = "The following information is missing or invalid:"
          render :edit
        end
      end

      private

      def permitted_params
        return {} unless params[:offering]

        params.require(:offering).permit(
          :public_contact_name, :tagline, :tagline_browse,
          :photo_swatch_key, :photo_logo_key, :photo_cover_key, :vanity_path, :photo_header_key,
          :facebook_pixel_id, :google_tracking_id,
          :google_adwords_conversion_id, :google_adwords_conversion_label,
          company_attributes: [:id, :name, :location, :website_url]
        )
      end

      def assign_offering
        @offering ||= Offering.for_user(current_user)
          .includes(:company)
          .find(params[:id])
      end
      alias_method :offering, :assign_offering

      def assign_uploader
        @uploader = Offering.new.photo_cover
        @uploader.success_action_status = '201'
      end

    end
  end
end
