module Issuer
  module Editor
    class PitchesController < BaseController

      before_action :assign_offering
      before_action :assign_uploader, only: [:edit, :update]

      def edit
      end

      def update
        if offering.update_attributes(permitted_params)
          audit_form!
          flash.notice = "Your changes have been saved"
          redirect_to edit_issuer_editor_pitch_path(offering)
        else
          flash.now.alert = "The following information is missing or invalid:"
          render :edit
        end
      end

      private

      def permitted_params
        return {} unless params[:pitch]

        params[:pitch].permit(
          pitch_sections_attributes: [
            :id, '_destroy', :title, :rank, content_items_attributes: [
              :id, '_destroy', :content_type, :rank, content_attributes: [
                :id, :body, :url, :html, :key, :expandable, carousel_images_attributes: [
                  :id, '_destroy', :key, :rank
                ]
              ]
            ]
          ]
        )
      end

      def assign_offering
        @offering ||= Offering.for_user(current_user)
          .includes(:company, pitch_sections: {content_items: :content})
          .find(params[:id])
      rescue ActiveRecord::RecordNotFound
        render_404
      end
      alias_method :offering, :assign_offering

      def assign_uploader
        @uploader = Pitch::Image.new.photo
        @uploader.success_action_status = '201'
      end
    end
  end
end
