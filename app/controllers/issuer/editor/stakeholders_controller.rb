module Issuer
  module Editor
    class StakeholdersController < BaseController

      before_action :assign_offering
      before_action :assign_uploader

      def edit
      end

      def update
        offering.update_attribute(:notable_investor_id, nil) # the next step will set it again if there is a new one
        if offering.update_attributes(permitted_params)
          audit_form!
          redirect_to edit_issuer_editor_stakeholder_path(offering)
          flash.notice = "Your changes have been saved"
        else
          flash.now.alert = "The following information is missing or invalid:"
          render 'edit'
        end
      end

      private

      def permitted_params
        return {} unless params[:stakeholders]

        params[:stakeholders].permit(
          team_members_attributes: [
            :id, :_destroy, :stakeholder_type_id, :full_name, :rank,
            :position, :bio, :linkedin_url, :twitter_url, :facebook_url,
            :website, :avatar_photo_key, :is_notable_investor
          ], investors_attributes: [
            :id, :_destroy, :stakeholder_type_id, :full_name, :rank,
            :position, :bio, :linkedin_url, :twitter_url, :facebook_url,
            :website, :avatar_photo_key, :is_notable_investor
          ]
        )
      end

      def assign_offering
        @offering ||= Offering.for_user(current_user)
        .includes(:stakeholders)
        .find(params[:id])
      end
      alias_method :offering, :assign_offering

      def assign_uploader
        @uploader = Stakeholder.new.avatar_photo
        @uploader.success_action_status = '201'
      end

    end
  end

end

