module Issuer
  module Editor
    class QuestionsController < BaseController

      before_action :assign_offering

      def edit
      end

      def update
        if offering.update_attributes(permitted_params)
          audit_form!
          flash.notice = "Your changes have been saved"
          redirect_to edit_issuer_editor_question_path(offering)
        else
          flash.now.alert = "The following information is missing or invalid:"
          render :edit
        end
      end

      private

      def permitted_params
        return {} unless params[:faq]

        params[:faq].permit(
          investor_faqs_attributes: [
            :id, '_destroy', :question, :answer, :rank
          ]
        )
      end

      def assign_offering
        @offering ||= Offering.for_user(current_user)
          .includes(:company, :investor_faqs)
          .find(params[:id])
      end
      alias_method :offering, :assign_offering

    end
  end
end

