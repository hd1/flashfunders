module Issuer
  module Editor
    class BaseController < ApplicationController

      include FormAuditing
      helper :offering_editor

      private

      def auditor
        Auditor.new
      end

      def audit_params
        permitted_params
      end

    end
  end
end

