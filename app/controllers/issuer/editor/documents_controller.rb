module Issuer
  module Editor
    class DocumentsController < BaseController

      before_action :assign_offering
      before_action :assign_uploader

      def edit
      end

      def update
        if offering.update_attributes(permitted_params)
          audit_form!
          flash.notice = "Your changes have been saved"
          redirect_to edit_issuer_editor_document_path(offering)
        else
          flash.now.alert = "The following information is missing or invalid:"
          render :edit
        end
      end

      private

      def permitted_params
        return {} unless params[:documents]

        params[:documents].permit(
          campaign_documents_attributes: [
            :id, '_destroy', :name, :document_new_key, :rank
          ],
          deal_documents_attributes: [
            :id, '_destroy', :name, :document_new_key, :rank, :description
          ]

        )
      end

      def assign_offering
        @offering ||= Offering.for_user(current_user)
          .includes(:company, :campaign_documents)
          .find(params[:id])
      end
      alias_method :offering, :assign_offering

      def assign_uploader
        @document_uploader = Document.new.document_new
        @document_uploader.success_action_status = '201'
      end

    end
  end
end


