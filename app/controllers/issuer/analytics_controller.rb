module Issuer

  class AnalyticsController < ApplicationController
    skip_before_filter :store_location
    skip_before_filter :track_visitor
    skip_before_filter :set_error_context
    skip_before_filter :flash

    before_action do
      @offering = set_offering_context(params[:id])
    end
    GA_EXPIRE_TIMEOUT = 3600
    GA_NAMESPACE = "GA"

    def get_data
      if @offering
        setup_dates

        data = cached_or_fresh(params)
        if data
          render json: { offering_id: @offering.id }.merge(data)
        else
          render nothing: true
        end
      else
        render nothing: true
      end
    end

    private

    def setup_dates

      offering_end_date = @offering.escrow_end_date || @offering.ends_at || Time.now

      date_start = params['start_date'].present? ? Date.parse(params['start_date']) : @offering.created_at.to_date
      date_end = params['end_date'].present? ? Date.parse(params['end_date']) : offering_end_date.to_date

      my_start_date = (date_start > @offering.created_at && date_start <= offering_end_date) ? date_start : @offering.created_at.to_date
      my_end_date = (date_end < offering_end_date && date_end >= @offering.created_at) ? date_end : offering_end_date

      if(my_start_date > my_end_date)
        my_start_date = @offering.created_at.to_date
        my_end_date = offering_end_date > my_start_date ? offering_end_date : Time.now
      end

      my_end_date = Date.today if my_end_date > Date.today

      @start_date = my_start_date.to_s
      @end_date = my_end_date.to_s

    end

    def analyzer
      @analyzer ||= IssuerAnalytics.new
    end

    def cached_or_fresh(params)
      ga_key = get_key(params)
      with_rescue { get_cached(ga_key) } || get_fresh(ga_key, params)
    end

    def get_fresh(ga_key, params)
      Rails.logger.info("getting fresh #{ga_key}")
      method = analyzer.method(params[:data_set])
      begin
        data = method.call(@start_date, @end_date, @offering, params)
      rescue Google::Apis::ServerError => e
        Honeybadger.notify e
        return nil
      end
      with_rescue do
        cache!(ga_key, data)
      end
      data
    end

    def get_key(params)
      key = "#{GA_NAMESPACE}/#{params[:data_set]}/offering:#{@offering.id}|start_date:#{@start_date}|end_date:#{@end_date}"
      key += "|frequency:#{params[:frequency]}" if params[:data_set] == "page_visit_info"
      key
    end

    def with_rescue
      begin
        yield
      rescue Redis::CannotConnectError => e
        Rails.logger.error e
        nil
      rescue e
        Honeybadger.notify e
        nil
      end
    end

    def get_cached(key)
      data = $redis.get(key)
      if data
        Rails.logger.info("hit - #{key}")
      else
        Rails.logger.info("miss - #{key}")
      end
      JSON.parse(data) if data
    end

    def cache!(key, val)
      $redis.set(key, val.to_json)
      $redis.expire(key, GA_EXPIRE_TIMEOUT)
      Rails.logger.info("caching #{key}")
    end

  end
end
