class ThirdPartyQualificationsController < ApplicationController
  include FormAuditing

  skip_before_filter :authenticate_user!

  before_action :assign_qualification
  before_action :assign_investor_entity
  before_action :render_if_qualification_complete!

  def edit
    @qualification_form = ThirdPartyQualificationForm.new({}, qualification)
  end

  def update
    @qualification_form = ThirdPartyQualificationForm.new(permitted_params, qualification, validator: validator)

    if @qualification_form.save
      audit_form_without_user!

      render :complete
    else
      render :edit
    end
  end

  def decline
    @qualification.update!(declined_by_third_party_at: Time.current)

    render :declined
  end

  def disabled_form?
    current_user.try(:id) == qualification.user_id || (!current_user.nil? && current_user.impersonator?)
  end
  helper_method :disabled_form?

  private

  def validator
    ThirdPartyQualificationFormValidator.new(permitted_params.merge(investor_entity: investor_entity))
  end

  def auditor
    Auditor.new
  end

  def audit_params
    permitted_params.merge(statement: params[:statement].to_s, license_state: qualification.license_state, user_id: qualification.user_id)
  end

  def permitted_params
    params.require(:third_party_qualification_form).permit(
      :name, :role, :company_name, :license_number, :license_state,
      :phone_number, :email, :signature, :verified_by_assets, :verified_by_accredited)
  end

  def qualification
    @qualification ||= Accreditor::ThirdPartyQualification.find_by_uuid!(params[:id])
  end
  alias_method :assign_qualification, :qualification

  def investor_entity
    @investor_entity ||= InvestorEntity.find(@qualification.investor_entity_id)
  end
  alias_method :assign_investor_entity, :investor_entity

  def render_if_qualification_complete!
    if !disabled_form?
      if qualification.declined_by_third_party?
        render :declined
      elsif qualification.signed_by_third_party?
        render :complete
      end
    end
  end
end
