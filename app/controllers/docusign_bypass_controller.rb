class DocusignBypassController < ApplicationController
  skip_before_filter :authenticate_user!
  before_filter :assign_investment, only: [:issuer_sign, :sign, :redirect, :success, :agreement]
  before_filter :assign_company_name, only: :success
  before_filter :check_investment_agreed, only: :sign

  def sign
    envelope = DocusignInvestmentAgreementEnvelope.new(@investment.envelope_id)

    if envelope.investor_signed? || envelope.investor_declined?
      redirect_to sign_success_path
      return
    end

    @agreement_url = DocusignInvestmentAgreementUrl.get_embedded_url(@investment.envelope_id, docusign_params.investor_name, docusign_params.investor_email, return_url)
  end

  def issuer_sign
    @agreement_url = DocusignInvestmentAgreementUrl.get_embedded_url(@investment.envelope_id, docusign_params.issuer_name, docusign_params.issuer_email, return_url)

    render :sign
  end

  def redirect
    @redirect_to_url = if params[:event].in?(['signing_complete', 'decline', 'viewing_complete'])
                         DocusignMailer.delay.notify(@investment.id)
                         sign_success_path(investment_uuid: @investment.uuid)
                       else
                         root_path
                       end
    render layout: false
  end

  def agreement
    send_file(investment_agreement_path, disposition: 'inline', type: 'application/pdf')
  end

  private

  def assign_company_name
    @company_name = Company.where(offering_id: @investment.offering_id).first.try(:name)
  end

  def assign_investment
    @investment = Investment.find_by_uuid(params[:investment_uuid])

    render_404 unless @investment
  end
  
  def check_investment_agreed
    redirect_to root_url unless @investment.accreditation_verified? || @investment.signed_documents?
  end

  def docusign_params
    @docusign_params ||= DocusignInvestorAgreementParameterBuilder.new(@investment.id)
  end

  def return_url
    sign_redirect_url(investment_uuid: @investment.uuid)
  end

  def investment_agreement_path
    docusign_client = DocusignRest::Client.new
    temp_file_path = "/tmp/agreement_#{Time.now.to_i}.pdf"
    response = docusign_client.get_combined_document_from_envelope(
      envelope_id: @investment.envelope_id,
      show_changes: true,
      watermark: true,
      certificate: true,
      local_save_path: temp_file_path
    )
    temp_file_path
  end

end
