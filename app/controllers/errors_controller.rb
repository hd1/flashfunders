class ErrorsController < ApplicationController

  skip_before_filter :authenticate_user!
  newrelic_ignore

  def not_found
    render_404
  end

  def internal_error
    render_500
  end
end
