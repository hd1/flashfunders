class InvestmentsController < ApplicationController
  include OfferingHelper

  def index
    @investments = Investment.investment_order(current_user).map do |investment|
      offering = investment.offering
      company = offering.company
      offering_presenter = OfferingPresenter.new(offering)
      investment_presenter = InvestmentPresenter.new(investment)
      investment_hash = {
        investment_summary: InvestorInvestmentSummary.new(investment, offering),
        investment: investment_presenter,
        campaign: offering_presenter,
        funding_detail: investment_presenter.funding_detail_domestic,
        funding_international_detail: investment_presenter.funding_detail_international,
        social: SocialPresenterFactory.build(offering_vanity_url(offering_presenter), t('show_campaign.social.twitter.account'), company.name, offering.tagline),
      }
      investment_hash
    end
  end

  def download_docs
    investment = Investment.find(params[:id])
    send_data investment.download_docs_stream, type: 'application/octet-stream', disposition: 'inline', filename: "Investment Agreement Documents #{investment.offering.corporate_name}.pdf"
  end

  def cancel_investment
    investment = Investment.find_by(id: params[:id], user_id: current_user&.id)
    if investment && InvestmentPresenter.new(investment).can_cancel_investment?
      investment.cancel!
      redirect_to action: :index
    else
      render :index
    end
  end

end
