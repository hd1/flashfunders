class BrowseController < ApplicationController
  include AnalyticsTracker

  skip_before_action :authenticate_user!
  before_action :expire_cache, only: [:show]

  PLACEHOLDER_SPOTS = 3

  def show
    @subscription = Subscription.new

    @open_campaigns = { reg_cf: sorted_open_campaigns(:reg_cf),
                        reg_d: sorted_open_campaigns(:reg_d) }
    @placeholder_campaigns = { reg_cf: get_placeholder_offerings(@open_campaigns[:reg_cf], PLACEHOLDER_SPOTS),
                               reg_d: get_placeholder_offerings(@open_campaigns[:reg_d], PLACEHOLDER_SPOTS) }
    @closed_campaigns = { reg_cf: find_closed_campaigns(:reg_cf),
                          reg_d: find_closed_campaigns(:reg_d) }

    track_facebook_pixel(ff_facebook_pixel_id, 'ViewContent')
    if no_campaigns?
      render :show_empty
    end
  end

  private

  def no_campaigns?
    @open_campaigns[:reg_cf].empty? && @open_campaigns[:reg_d].empty? && @closed_campaigns[:reg_cf].empty? && @closed_campaigns[:reg_d].empty?
  end

  def sorted_open_campaigns(kind)
    find_open_campaigns(kind).sort_by! { |c| [c.ended? ? 1 : 0, -c.formatted_percent_complete.to_i ] }
  end

  def find_open_campaigns(kind)
    offering_presenters((Offering.includes(:company).visibility_public.open | Offering.includes(:company).visibility_public.closed.pending).select{ |o| check_campaign_type(o, kind)})
  end

  def expire_cache
    expire_cache_if_recache
  end

end
