class OfferingCommentFormsController < ApplicationController

  skip_before_filter :verify_authenticity_token, only: [:destroy]

  def create
    comment = OfferingCommentForm.new(permitted_params).save

    if comment.valid?
      if comment.is_reply?
        send_email_to_user(comment)
        send_email_to_followers(comment)
      else
        send_email_to_issuer(comment)
      end
      flash.notice = t('offering_comment.successful')
    else
      comment_alert(comment)
    end

    redirect_to offering_comment_path
  end

  def destroy
    offering = Offering.find(params[:offering_id])
    comment = OfferingComment.where(permitted_params_for_destroy).first
    if comment.owner?(current_user) || offering.belongs_to_user?(current_user.id)
      comment.hide!(current_user)
      flash.notice = t('offering_comment.deleted')
    end

    redirect_to offering_comment_path
  end

  private

  def permitted_params_for_destroy
    params.permit(parent(params[:parent_id]), :id)
  end

  def permitted_params
    required_hash = params.require(:offering_comment_form)

    required_hash
      .permit(:body, parent(required_hash[:parent_id]))
      .merge(user_id: current_user.id)
  end

  def offering_comment_path
    case action_name.to_sym
    when :create
      params[:offering_comment_form][:return_to]
    when :destroy
      params[:return_to]
    end
  end

  def parent(parent_id)
    parent_id.present? ? :parent_id : :offering_id
  end

  def send_email_to_user(comment)
    comment_user_id = OfferingComment.find(permitted_params[:parent_id]).user.id
    OfferingCommentMailer.delay.reply_comment_from_issuer(params[:offering_id].to_i, comment_user_id, comment.id)
  end

  def send_email_to_issuer(comment)
    comment_user_id = permitted_params[:user_id]
    OfferingCommentMailer.delay.comment_from_user(params[:offering_id].to_i, comment_user_id, comment.id)
  end

  def send_email_to_followers(reply)
    commenter = reply.parent
    issuer = Offering.find(params[:offering_id].to_i).issuer_user

    followers = OfferingFollower.where(offering_id: params[:offering_id].to_i).where.not(user_id: [commenter.id, issuer.id])
    OfferingFollowerMailer.delay.notify_issuer_reply(params[:offering_id].to_i, reply.id) unless followers.blank?
  end

  def comment_alert(comment)
    if comment.errors.messages.has_key?(:body)
      flash.alert = t('offering_comment.body_exceeded', comment_max_length: OfferingCommentFormValidator::MAX_COMMENT_LENGTH)
    else
      flash.alert = t('offering_comment.failed')
      Honeybadger.notify(error_message: comment.errors.messages)
    end
  end

end
