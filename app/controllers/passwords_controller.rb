class PasswordsController < Devise::PasswordsController

  layout 'registration'

  def create
    user = User.find_for_authentication(email: params[:user][:email])
    if user.present? && user.has_no_password?
      redirect_to new_user_confirmation_path(user: params[:user])
    else
      super
    end
  end

  protected
  def after_resetting_password_path_for(resource_name)
    sign_out
    flash[:notice] = t('devise.passwords.updated_not_active')
    new_user_session_path
  end
end