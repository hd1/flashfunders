class OfferingFollowersController < ApplicationController

  respond_to :json

  def update
    follower.follow_as!(:follower)
  end

  def destroy
    follower.unfollow!
    flash[:notice] = I18n.t('offering_message.ask_a_question.unfollow', company: company_name)
  end

  def follow
    update
    redirect_to offering_path(params[:offering_id])
  end

  private

  def company_name
    @company_name ||= Offering.find(follower.offering_id).company.try(:name) || 'the company'
  end

  def follower
    @follower ||= OfferingFollower.find_or_initialize_by(
      offering_id: params[:offering_id],
      user: current_user)
  end
end
