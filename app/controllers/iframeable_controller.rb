class IframeableController < ActionController::Base
  layout 'iframe'
  before_action :configure_response_header
  before_action :set_blog_flag

  def header
    @stylesheet_file = 'iframe/if_header'
    @optional_styles = ['//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css']
    # @javascript_file = 'iframe/if_header'
  end

  def footer
    @stylesheet_file = 'iframe/if_footer'
  end

  # This is for preventing error raising when site header is being rendered within blog.
  # This is unused for now since we currently have disabled any javascript on the header.
  def change_context_path(id)
    issuer_change_context_path(id: id, target: blog_path)
  end

  private

  def configure_response_header
    response.headers.delete('X-Frame-Options')
    # TODO: finding a way to use frame-ancestors for all browsers instead of allowing anyone embeds it.
    # response.headers["X-Content-Security-Policy"] = "frame-ancestors https://preview.hs-sites.com http://blog.flashfunders.com";
    # response.headers["Content-Security-Policy"] = "frame-ancestors https://preview.hs-sites.com http://blog.flashfunders.com";
  end

  def set_blog_flag
    @within_blog = true
  end

end
