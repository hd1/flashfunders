class ImpersonationsController < ApplicationController

  include FormAuditing

  before_action :authorize_impersonator
  after_action :set_offering_context, only: [:create, :destroy]

  def create
    user = User.find(params[:user_id])

    if user.confirmed?
      audit_form!
      impersonate(user)
    end

    redirect_to root_path
  end

  def destroy
    if impersonating?
      audit_form!
      revert_impersonation
    end

    redirect_to root_path
  end

  private

  def authorize_impersonator
    return if current_user.impersonator? || impersonating?
    redirect_to root_path
  end

  def impersonate(user)
    unless impersonating?
      session[:impersonator_id] = current_user.id
      session[:impersonated_id] = user.id
    end
    sign_in(user)
  end

  def revert_impersonation
    sign_out(current_user)
    sign_in(impersonating_user)
    session[:impersonator_id] = nil
    session[:impersonated_id] = nil
  end

  def auditor
    Auditor.new
  end

  def audit_params
    {}
  end

end

