class SubscriptionsController < ApplicationController
  skip_before_filter :authenticate_user!
  respond_to :json

  def new
    @subscription = Subscription.new
  end

  def create
    subscription = Subscription.new(permitted_params)
    if subscription.save
      set_user_cookie('_subscribed' => { value: true, expires: 10.years.from_now } )
      if subscription.id.blank? # a subscription already exists for this email
        subscription = Subscription.find_by(email: subscription.email)
      end
      track_visitor(subscription)
      SubscriptionMailer.welcome_email(subscription.email).deliver_later
      render nothing: true, status: 201
    else
      render nothing: true, status: 422
    end
  end

  def dismiss
    set_user_cookie('_dismissed' => { value: true, expires: 10.years.from_now } )
    render nothing: true, status: 200
  end

  private

  def permitted_params
    params.require(:subscription).permit(:email, :source_page)
  end

end
