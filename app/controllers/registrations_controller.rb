class RegistrationsController < Devise::RegistrationsController

  layout 'registration'

  respond_to :html, :js

  skip_before_filter :verify_authenticity_token, :only => :create

  before_action :store_return_to, only: [:new, :create]
  before_action :configure_permitted_parameters

  def build_resource(params={})
    self.resource = RegistrationForm.new(params)
  end

  def new
    build_resource(params)
    @validatable = devise_mapping.validatable?
    if @validatable
      @minimum_password_length = resource_class.password_length.min
    end
    respond_with self.resource
  end

  def create
    flash.alert.clear if flash.alert

    super do |form|
      if form.errors.any?
        flash[:analytics_sign_up_email] = { failed: resource.email }
      else
        self.resource = form.user
        audit!(resource)
        flash[:analytics_sign_up_email] = { success: resource.email }
        track_visitor(resource)
        @user.skip_confirmation!
        send_welcome(resource)
        InvestorMailer.delay_for(2.weeks).checking_in(resource.id) if resource.user_type_investor?
      end
    end
  end

  private

  def configure_permitted_parameters
    [:agreed_to_tos, :registration_name, :user_type, :us_citizen, :country].each do |p|
      devise_parameter_sanitizer.for(:sign_up) << p
    end
  end

  def audit_params
    params.fetch(:user)
  end

  def send_welcome(resource)
    resource.send_confirmation_instructions({}, :welcome_instructions) unless resource.has_no_password?
  end

  def session_offering_id
    path = session[stored_location_key_for(:user)]
    path.blank? ? 0 : path.split('/')[2]
  end

  def store_return_to
    return unless params[:return_to]

    return_to_path, fragment = params[:return_to].split("#")
    session[:user_return_to_fragment] = fragment if fragment
    session[:redirect_after_signup] = true
    store_location_for(:user, return_to_path)
  end

  def after_inactive_sign_up_path_for(resource)
    new_user_confirmation_path(user: { email: resource.email, new: true })
  end

  def audit!(user)
    auditor.write(audit_params, user.id, request.ip, request.path, registered_at)
  end

  def auditor
    RegistrationFormAuditor.new
  end

  def registered_at
    @registered_at ||= Time.now.utc
  end

end
