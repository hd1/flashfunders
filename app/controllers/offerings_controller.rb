class OfferingsController < ApplicationController
  include OfferingHelper
  include AnalyticsTracker
  include JSVariables

  skip_before_filter :authenticate_user!, only: [:show, :vanity_show]

  before_action :expire_cache, only: [:vanity_show]
  before_action :empty_cache_key, only: [:show]
  before_action :assign_offering, only: [:show, :vanity_show]
  before_action :assign_company, only: [:show]
  before_action :assign_investment, only: [:show, :vanity_show]
  before_action :ensure_campaign_is_visible, only: [:show, :vanity_show]
  before_action :setup_offering_js, only: [:show, :vanity_show]

  def show
    @offering_presenter = OfferingPresenter.new(offering)
    @social_urls = SocialPresenterFactory.build(offering_vanity_url(@offering_presenter), t('show_campaign.social.twitter.account'), company.try(:name), offering.tagline)

    @team_information = company.blank? ? [] : offering.team_members
    @investor_advisor_information = company.blank? ? [] : offering.investors
    @offering_documents = CampaignDocument.active(offering)

    @invest_button_path = new_offering_investment_path(offering)
    @invest_button_text_key = invest_button_text_key

    @faqs = InvestorFaq.where(offering_id: offering.id).order(:id)

    @follower = OfferingFollower.find_or_initialize_by(offering_id: offering.id, user: current_user)

    @offering_comments = OfferingComment.visible.where(offering_id: offering.id)
    @comment = OfferingCommentForm.new

    set_additional_javascript_vars
    track_facebook_pixel(offering.facebook_pixel_id, 'ViewContent')
    track_facebook_pixel(ff_facebook_pixel_id, 'ViewContent')
    track_google_analytics(offering.google_tracking_id)
    track_google_adwords(offering.google_adwords_conversion_id, offering.google_adwords_conversion_label)
  end

  def vanity_show
    render_404 and return unless user_can_view_campaign?(offering)

    show

    render :show
  end

  def change_context_path(id)
    offering = Offering.find(id)
    target = offering.vanity_path.blank? ? offering_path(id: id) : offering_vanity_path(path: offering.vanity_path)
    issuer_change_context_path(id: id, target: target)
  end

  private

  def expire_cache
    @cache_key = {}
    CacheManager::OFFERING_PARTIALS.each do |partial|
      @cache_key[partial] = "offering_#{offering.vanity_path}_show_#{partial}"
    end
    if params[:recache] == 'true'
      begin
        @cache_key.each do |k, v|
          expire_fragment(v)
        end
      rescue SocketError => e
        Honeybadger.notify e
      end
    end
  end

  def empty_cache_key
    @cache_key = {}
  end

  def investment
    @investment ||= Investment.where(offering_id: offering.id, user_id: current_user).first_or_initialize
  end

  alias_method :assign_investment, :investment

  def offering
    @offering ||= begin
      if params[:id]
        Offering.includes(:company, pitch_sections: { content_items: :content }).find(params[:id])
      elsif params[:path]
        Offering.includes(:company, pitch_sections: { content_items: :content }).find_by!(vanity_path: params[:path].downcase)
      end
    end
  end

  alias_method :assign_offering, :offering

  def company
    offering.company
  end

  alias_method :assign_company, :company

  def ensure_campaign_is_visible
    render_404 unless user_can_view_campaign?(offering)
  end

  def user_can_view_campaign?(offering)
    return true if offering.visibility_public?
    return true if offering.visibility_preview? && offering.uuid == params[:preview]
    return true if offering.visibility_preview? && (params[:path] && offering.vanity_path == params[:path].downcase)
    return true if offering.visibility_restricted? && (current_user && (offering.user_id == current_user.id || offering.issuer_user_id == current_user.id))
    false
  end

  def invest_button_text_key
    if @investment&.live?
      if @investment.resumable?
        'investor_investments.invest.resume'
      else
        'investor_investments.invest.complete'
      end
    else
      'investor_investments.invest.start'
    end
  end

  def set_additional_javascript_vars
    gon.resume_intro_to_founder = session.delete('complete_intro_to_founder')
  end

end
