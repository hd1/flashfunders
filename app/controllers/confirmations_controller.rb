class ConfirmationsController < Devise::ConfirmationsController

  skip_before_filter :authenticate_user!

  before_action :store_return_to, only: [:show, :confirm, :new]

  layout 'registration'

  def new
    self.resource = current_user || find_user_by_email
    save_user_context(resource)
    set_alert_message
    super if resource.nil?
  end

  def create
    @form = ConfirmationForm.new(form_params, nil)
    @user = @form.user

    params[:user].merge!({ return_to: session[stored_location_key_for(:user)] })
    if @user.present?
      if current_user.present? && current_user != @user
        set_flash_message(:alert, :invalid_email)
        redirect_to action: :new
      elsif @user.confirmed?
        set_flash_message(:notice, :already_confirmed)
        redirect_to(new_user_session_path)
      else
        self.resource = @user
        super
      end
    else
      self.resource = current_user
      super
    end
  end

  def show
    @user = find_user_by_token

    if @user.blank?
      @user = find_user_by_uuid
      found_by_uuid = true
    else
      found_by_uuid = false
    end

    # TODO: Remove the following line when we restore confirmations. In the meantime, the code after the return is unreachable by design.
    return bypass_confirmation_process

    @form ||= ConfirmationForm.new(form_params, @user)

    if @user.blank?
      self.resource = @form
      super
    elsif @user.confirmed?
      set_flash_message(:notice, :already_confirmed)
      redirect_to new_user_session_path(user: { email: @user.email })
    elsif @user.send(:confirmation_period_expired?)
      render action: 'new', expired: true
    elsif found_by_uuid
      redirect_to action: 'new', token_mismatch: true, email: @user.email
    elsif !@user.has_no_password?
      do_confirm(false)
    end
  end

  def confirm
    @form = ConfirmationForm.new(form_params, find_user)

    if @form.save
      @user = @form.user
      do_confirm
    elsif @form.user.nil?
      self.resource = @form
      render action: 'new'
    else
      render action: 'show'
    end
  end

  private

  def bypass_confirmation_process
    if @user.blank?
      redirect_to(new_user_registration_path)
    else
      if !@user.confirmed?
        @user.confirm!  # automatically confirm outstanding requests
      end
      if @user.needs_password_set?
        redirect_to(new_user_registration_path(email: @user.email, registration_name: @user.registration_name))
      else
        set_flash_message(:notice, :password_already_set)
        redirect_to(new_user_session_path(user: { email: @user.email }))
      end
    end
  end

  def form_params
    (params[:user] ||
      { uuid: params[:uuid], confirmation_token: params[:confirmation_token], email: params[:email] }
    ).merge(tos_params)
  end

  def tos_params
    @tos_params ||= {
      agreed_to_tos_ip: request.ip,
      agreed_to_tos_datetime: Time.now.utc,
      agreed_to_tos: true
    }
  end

  def find_user_by_email
    User.find_for_authentication(email: lookup_param(:email))
  end

  def find_user
    find_user_by_token || find_user_by_uuid
  end

  def find_user_by_token
    @confirmation_token = lookup_param(:confirmation_token)
    digested_token = Devise.token_generator.digest(self, :confirmation_token, @confirmation_token)
    User.find_by_confirmation_token(digested_token)
  end

  def find_user_by_uuid
    @uuid = lookup_param(:uuid)
    User.find_by_uuid(@uuid)
  end

  def lookup_param(key)
    params[key] || params[:user].try(:[], key)
  end

  def set_alert_message
    if params[:expired]
      flash[:alert] = I18n.t('devise.registrations.expired_token')
    elsif params[:token_mismatch]
      flash[:alert] = I18n.t('devise.registrations.token_not_found')
    elsif params[:new]
      flash[:alert] = I18n.t('devise.registrations.signed_up_but_unconfirmed')
    else
      flash[:alert] = confirmation_alert_text(resource)
    end
  end

  def save_user_context(user)
    return if user.nil?
    return_to = session[:redirect_after_signup] ? stored_location_for(user) : get_started_path
    user.save_context(session, return_to)
  end

  def after_sign_in_path_for(user)
    context = user.restore_context(session)
    context['path'] || stored_location_for(user) || get_started_path
  end

  def store_return_to
    return unless params[:return_to]

    session[:redirect_after_signup] = true
    store_location_for(:user, params[:return_to])
  end

  def do_confirm(sign_in=true)
    if @user.confirmed? || @user.confirm!
      set_flash_message :notice, :confirmed
      if sign_in
        sign_in_and_redirect :user, @user
      else
        redirect_to(new_user_session_path)
      end
    else
      render action: 'new'
    end
  end
end
