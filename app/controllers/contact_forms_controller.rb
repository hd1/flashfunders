class ContactFormsController < ApplicationController
  skip_before_filter :authenticate_user!

  respond_to :json

  def new
    @contact_form = ContactForm.new
  end

  def create
    @contact_form = ContactForm.new(permitted_params)
    @contact_form.name = current_user.first_name + ' ' + current_user.last_name if current_user
    @contact_form.email = current_user.email if current_user
    @contact_form.user_id = current_user.id if current_user
    @contact_form.deliver
  end

  private

  def permitted_params
    params.require(:contact_form).permit(:user_type, :name, :email, :message, :page_name)
  end

end

