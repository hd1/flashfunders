module InvestmentSummaryAssignment
  def investment_summary
    @investment_summary ||= investor_investment_summary
  end
  alias_method :assign_investment_summary, :investment_summary

  def investor_investment_summary
    InvestorInvestmentSummary.new(@investment, @offering)
  end
end