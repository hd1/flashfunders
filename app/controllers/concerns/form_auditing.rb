module FormAuditing

  def audit_form!(user=nil)
    user ||= current_user
    auditor.write(audit_params, user.id, request.ip, request.path, Time.now.utc, impersonating_user.try(:id))
  end

  def audit_form_without_user!
    auditor.write(audit_params, nil, request.ip, request.path, Time.now.utc)
  end

  def auditor
    raise "#{self.class} must implement #auditor"
  end

  def audit_params
    raise "#{self.class} must implement #audit_params"
  end

end
