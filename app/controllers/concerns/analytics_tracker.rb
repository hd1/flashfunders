module AnalyticsTracker
  def track_facebook_pixel(id, event='ViewContent', opts={})
    if id.present?
      @facebook_pixels ||= []
      @facebook_pixels << {
        pixel_id: id,
        event: event,
        opts: opts
      }
    end
  end

  def track_google_analytics(id, hit_type='pageView', opts={})
    @offering_google_tracking = {
      id: id,
      hit_type: hit_type
    }
  end

  def track_google_adwords(id, label)
    @offering_google_adwords = {
      conversion_id: id,
      conversion_label: label
    }
  end

  def ff_facebook_pixel_id
    ENV['FB_PIXEL_ID']
  end
end
