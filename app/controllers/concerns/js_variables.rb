module JSVariables
  def setup_offering_js(offering = @offering)
    unless offering.nil?
      gon.offering_id = offering.id
    end
  end
end
