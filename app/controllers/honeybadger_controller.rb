class HoneybadgerController < ApplicationController

  skip_before_filter :authenticate_user!

  def blow_up
    raise 'honeybadger dont care'
  end
end
