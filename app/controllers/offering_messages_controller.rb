class OfferingMessagesController < ApplicationController
  skip_before_filter :authenticate_user!, :only => :create
  include FormAuditing
  respond_to :js

  before_action :assign_offering

  def create
    clear_flash
    sender, new_user = find_or_create_user
    @follower = follower(sender)

    if sender && !needs_confirmation?(sender, new_user)
      @follower.follow_as!(:follower)
      OfferingFollowerMailer.delay.welcome_follower(@follower.id)
    end
  end

  private

  def clear_flash
    flash.alert.clear if flash.alert
    flash.notice.clear if flash.notice
  end

  def find_or_create_user
    sender = current_user || User.find_for_authentication(email: params[:investor_email])
    new_user = sender.blank?
    if new_user
      sender = temporary_user_valid? && create_temporary_user(params[:investor_name], params[:investor_email])
    end
    return sender, new_user
  end

  def needs_confirmation?(sender, new_user)
    # existing unconfirmed user (Use Case #5, #12)
    unless sender.confirmed? || new_user
      @needs_confirmation = true
      session[:complete_intro_to_founder] = true
      sender.save_context(session, after_sign_in_path_for(sender))
      flash[:alert] = confirmation_alert_text(sender, :intro_to_founder)
    end
  end

  def save_and_send_message(sender)
    save_message(sender)
    current_user ? audit_form! : audit_form_without_user!
    OfferingMailer.delay.message_from_user(@offering.id, sender.id, @offering_message)
  end

  def create_temporary_user(name, email)
    user = User.create_temporary_user(name, email)
    if user.errors.messages.present?
      flash[:alert] = []
      user.errors.messages.each do |_, value|
        value.each do |message|
          flash.now[:alert] << message
        end
      end
      return nil
    end
    user
  end

  def save_message(sender)
    OfferingMessage.transaction do
      @offering_message = OfferingMessage.create(offering_id: @offering.id,
                                                 body: t('offering_message.ask_a_question.default_message', name: sender.registration_name),
                                                 name: sender.registration_name,
                                                 email: sender.email,
                                                 user_id: sender.id,
                                                 sent_by_user: true)

     follower = follower(sender)
     follower.follow_as!(:asker)
    end
  end

  def follower(sender)
    OfferingFollower.find_or_initialize_by(offering_id: @offering.id, user: sender)
  end

  def first_time_sending?(sender)
    sender && !OfferingMessage.find_by(user_id: sender.id, sent_by_user: true, offering_id: @offering.id)
  end

  def temporary_user_valid?
    unless present_in_params?(:investor_name, :investor_email)
      flash.now.alert = t('offering_message.ask_a_question.error.missing_info')
      return false
    end

    unless valid_email?(params[:investor_email])
      flash.now.alert = t('offering_message.ask_a_question.error.bad_email')
      return false
    end

    true
  end

  def valid_email?(email)
    email.present? && (email =~ /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i)
  end

  def auditor
    Auditor.new
  end

  def audit_params
    { name: @offering_message.name, email: @offering_message.email, body: @offering_message.body }
  end

  def assign_offering
    @offering = Offering.includes(:company).find(params[:offering_id])
  end

  def permitted_params
    params.permit(:investor_email, :investor_name, :offering_id, :id)
  end

  def offering_id
    (action_name.to_sym == :create ? permitted_params[:offering_id] : params[:id]).to_i
  end

end
