class HomeController < ApplicationController
  include AnalyticsTracker
  skip_before_filter :authenticate_user!
  before_action :expire_cache, only: [:show]

  def show
    @subscription = Subscription.new
    @offerings = Offering.visibility_public.where("rank IS NOT NULL AND closed_status != 2").order(:rank).map do |offering|
      OfferingPresenter.new(offering)
    end.reject { |offering| offering.closed_pending?  }.first(6)
    @placeholder_offerings = get_placeholder_offerings(@offerings, 3)

    track_facebook_pixel(ff_facebook_pixel_id, 'ViewContent')
    respond_to :html
  end

  def show_less
    render layout: 'stripped_layout'
  end

  def robots
    suffix = %w(production review).include?(Rails.env) ? Rails.env : 'default'

    robots = File.read(Rails.root + "config/robots/robots.#{suffix}.txt")
    render :text => robots, :layout => false, :content_type => "text/plain"
  end

  private

  def expire_cache
    expire_cache_if_recache
  end

end
