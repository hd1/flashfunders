class ApplicationController < ActionController::Base
  protect_from_forgery

  before_action :enforce_email_consent
  before_action :store_location
  before_action :track_visitor
  before_action :authenticate_user!
  before_action :set_error_context
  before_action :set_user_signed_in
  # Just calling flash causes rails to mark the flash entries for deletion.
  # Without this, the flash persists beyond the first redirect
  before_action :flash

  def handle_unverified_request
    action_name == 'create' ? redirect_to(root_path) : super
  end

  def recreate
    recreate_params = lookup_recreate_params
    if recreate_params
      params.merge!(recreate_params)
      create
    else
      false
    end
  end

  def impersonating?
    session[:impersonator_id].present?
  end

  helper_method :impersonating?

  def impersonating_user
    @impersonating_user ||= impersonating? ? User.find(session[:impersonator_id]) : nil
  end

  helper_method :impersonating_user

  def impersonated_user
    impersonating? && User.find(session[:impersonated_id])
  end

  def expire_cache_if_recache
    @cache_key = "#{controller_name}_#{action_name}"
    if params[:recache] == 'true'
      begin
        expire_fragment(@cache_key)
      rescue SocketError => e
        Honeybadger.notify e
      end
    end
  end
  helper_method :expire_cache_if_recache

  def present_in_params?(*args)
    args.all? { |field| params[field].present? }
  end

  def change_context_path(id)
    issuer_change_context_path(id: id, target: issuer_dashboard_path(id: id))
  end

  private

  def utm_params
    params.select { |key, value| key =~ /^utm_.+/ && value.present? }
  end

  def get_ga_client_id
    google_analytics_cookie = cookies['_ga'] || ''
    client_id = google_analytics_cookie.gsub(/^GA\d\.\d\./, '')
    client_id =~ /^\d+\.\d{10}$/ ? client_id : ''
  end

  def track_visitor(resource=nil)
    client_id = get_ga_client_id

    if client_id.present?
      Utm.track!(client_id, utm_params, current_user, resource)
    end
    track_returning_visitor
    send_info_to_hubspot current_user || resource
  end

  def track_returning_visitor
    unless cookies['_first_visit'].present?
      set_user_cookie('_first_visit' => { value: Time.now.to_i } )
    end
  end

  def send_info_to_hubspot(resource)
    unless resource.nil?
      HubspotTracker.delay.send_contact_info(resource.class, resource.id, get_hs_context)
    end
    cookies['_ffemail'] = resource&.email if cookies['_ffemail'].blank?
    gon.email = cookies['_ffemail']
  end

  def get_hs_context(page_name=nil, redirect_url=nil)
    { 'hutk' => cookies['hubspotutk'],
      'ipAddress': request.remote_ip,
      'pageUrl': request.url
    }.tap do |context|
      context['pageName'] = page_name unless page_name.blank?
      context['redirectUrl'] = redirect_url unless redirect_url.blank?
    end
  end

  def set_user_cookie(params={})
    params.map { |k, v| cookies[k] = v }
  end

  def store_location
    return if !request.get? || devise_controller? || request.xhr? || request.path == user_email_consent_path

    gon.path_fragment = session[:user_return_to_fragment]
    session[:redirect_after_signup] = false
    store_location_for(:user, request.path)
  end

  def post_confirmation_target(user=nil)
    # symbolize the return_to path to use as a key
    return_to = session[stored_location_key_for(:user)] || user.try(:saved_path)
    if return_to
      data = Rails.application.routes.recognize_path(return_to)
      data[:controller].gsub(/\//, '_').to_sym
    end
  end

  def confirmation_alert_text(user, target=nil)
    return I18n.t('devise.registrations.signed_up_but_unconfirmed') unless user.present?
    user_key = user.has_no_password? ? 'partially' : 'fully'
    key = "#{target || post_confirmation_target(user)}.#{user_key}_registered"
    I18n.t("devise.confirmations.user.#{key}", default: I18n.t("devise.confirmations.user.#{user_key}_registered"))
  end

  def after_sign_out_path_for(resource)
    root_path
  end

  def render_404
    render 'errors/not_found', status: :not_found
  end

  def render_500
    render 'errors/internal_error', status: :internal_server_error
  end

  def set_error_context
    return unless current_user

    Honeybadger.context({
      :user_id => current_user.id,
      :user_email => current_user.email
    })
  end

  rescue_from(ActionController::UnknownFormat) do |e|
    Rails.logger.error e.inspect
    head :not_found
  end

  rescue_from(ActionView::MissingTemplate) do |e|
    f = request.format
    if f.html? || f.javascript? || f.json? || f.xml?
      # If this seems to be a legit request, it may be a legit error
      raise e
    else
      Rails.logger.error e.inspect
      head :not_acceptable
    end
  end

  def set_offering_context(offering_id=0)
    offerings = Offering.for_user(impersonated_user || current_user)
    offering = offerings.detect { |o| o.id == offering_id.to_i } ||
      offerings.detect { |o| o.id == get_offering_context } ||
      offerings.last
    cookies['_startup'] = offering.blank? ? nil : offering.id
    offering
  end

  def get_offering_context
    cookies['_startup'].to_i
  end

  def store_recreate_to_session(recreate_path, recreate_params)
    return if user_signed_in?

    session[:recreate_params] = recreate_params
    session[:redirect_after_signup] = true
    store_location_for(:user, recreate_path)
  end

  def lookup_recreate_params
    session.delete(:recreate_params)
  end

  def get_placeholder_offerings(open_offerings, placeholder_spots)
    return [] unless open_offerings.length < placeholder_spots
    num_of_placeholders = placeholder_spots - open_offerings.length
    # Don't show the last row of placeholders if we have 3 live offerings
    num_of_cols = 3
    num_of_rows = placeholder_spots / num_of_cols
    num_of_placeholders = 0 if num_of_placeholders <= num_of_cols * (num_of_rows - 1)
    offset = open_offerings.length + 1
    Array.new(num_of_placeholders) do |i|
      PlaceholderOfferingPresenter.new("browse/browse-placeholder-#{i+offset}.png")
    end
  end

  def find_closed_campaigns(kind)
    offering_presenters(Offering.includes(:company).visibility_public.successful.closed.select{ |o| check_campaign_type(o, kind, true) })
  end

  def offering_presenters(offerings)
    offerings.map do |offering|
      OfferingPresenter.new(offering)
    end
  end

  def check_campaign_type(offering, kind, check_funded=false)
    security = case kind
                 when :reg_cf
                   offering.reg_c_security
                 when :reg_d
                   offering.reg_d_direct_security
               end
    security.present? && (!check_funded || SecurityPresenters::Security.new(security).funded?)
  end

  def enforce_email_consent
    return unless current_user
    return if request.xhr?
    return if [user_email_consent_path, user_confirmation_path, new_user_confirmation_path, user_confirm_path].include? request.path

    if current_user.consent_needed? && !impersonating?
      store_location_for(:user, request.path)
      redirect_to user_email_consent_path
    end
  end

  def set_user_signed_in
    gon.user_signed_in = (cookies["_subscribed"] == "true" || user_signed_in?)
  end

  def serialization_key
    "#{controller_name}#{action_name}#{params.to_s}"
  end

end
