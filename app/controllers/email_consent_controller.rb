class EmailConsentController < ApplicationController
  include FormAuditing
  layout 'registration'

  def new
    @form = EmailConsentForm.new(form_params)
  end

  def create
    @form = EmailConsentForm.new(permitted_params)
    unless @form.valid?
      render :new
      return
    end
    @form.save
    audit_form!
    redirect_to(stored_location_for(:user) || root_path)
  end

  def audit_params
    @form.attributes.merge(tos_params)
  end

  private

  def form_params
    {}.tap do |h|
      h[:email_consent] = '0'
      h[:user] = current_user
    end
  end

  def tos_params
    @tos_params ||= {
      agreed_to_tos: true,
      agreed_to_tos_ip: request.ip,
      agreed_to_tos_datetime: Time.now.utc,
      email_consent_at: Time.now.utc
    }
  end

  def current_user_with_tos
    user = current_user
    user.assign_attributes(tos_params.except(:email_consent_at)) unless user.agreed_to_tos
    user.assign_attributes(tos_params.slice(:email_consent_at)) unless user.email_consent_at
    user
  end

  def permitted_params
    params.require(:email_consent_form).permit(*EmailConsentForm::ATTRIBUTES).merge(user: current_user_with_tos)
  end

  def auditor
    EmailConsentFormAuditor.new
  end

end
