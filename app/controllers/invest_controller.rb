class InvestController < ApplicationController
  include FormAuditing
  include JSVariables

  before_action :assign_offering
  before_action :assign_investment
  before_action :assign_progress_bar

  before_action :redirect_to_correct_path_for_state

  layout 'investment_flow'

  private

  def auditor
    Auditor.new
  end

  def investment
    @investment ||= Investment.where(offering: offering, user: current_user).where.not(state: %w(cancelled refunded)).first_or_initialize
  end
  alias_method :assign_investment, :investment

  def campaign
    @offering ||=
      begin
        offering = Offering.find(params[:offering_id])
        raise ActiveRecord::RecordNotFound unless offering
        offering
      end
  end
  alias_method :assign_offering, :campaign
  alias_method :offering, :campaign

  def company
    @company ||= Company.where(offering_id: campaign.id).first
  end
  alias_method :assign_company, :company

  def path_mapper
    @path_mapper ||= InvestmentPathMapper.new(investment)
  end

  def assign_progress_bar
    @progress_bar ||= InvestmentProgressBar.new(investment, controller_name)
  end

  def editing_via_progress_bar?
    @progress_bar.applicable? && !@progress_bar.last_step?
  end

  def redirect_to_correct_path_for_state
    method = request.request_method.downcase.to_sym
    if path_mapper.prohibited?(request.path, method)
      redirect_for_investment
    end
  end

  def redirect_for_investment
    redirect_to path_mapper.target_for
  end

end

