class IssuerApplicationsController < ApplicationController
  include FormAuditing
  include AnalyticsTracker

  skip_before_action :authenticate_user!

  def new
    @form = IssuerApplicationForm.new({}, current_user)
    track_facebook_pixel(ff_facebook_pixel_id, 'NewIssuerApplication')
  end

  def create
    @form = IssuerApplicationForm.new(params.require(:issuer_application_form), current_user)
    user = User.find_by_email(@form.email)
    if logged_out_user?(user)
      respond_to_logged_out_user(user)
    elsif @form.save
      @user = @form.user
      @user.update_attributes(user_type: :user_type_entrepreneur)
      audit_form!(@user)
      issuer_application = IssuerApplication.where(user_id: @user.id).last
      HubspotTracker.delay.create_deal(issuer_application.id)
      cookies['_ffemail'] ||= @user.email
      send_welcome(@user)
      redirect_to action: :success
    else
      render :new
    end
  end

  def recreate
    unless super
      redirect_to action: :new
    end
  end

  private

  def logged_out_user?(user)
    !user_signed_in? && user.present?
  end

  def respond_to_logged_out_user(user)
    save_recreate_params
    if user.confirmed?
      if user.has_no_password?
        redirect_to(new_user_registration_path(email: user.email, registration_name: user.registration_name))
      else
        redirect_to(new_user_session_path(user: { email: user.email }))
      end
    else
      redirect_to(new_user_confirmation_path({ email: user.email }))
    end
  end

  def save_recreate_params
    store_recreate_to_session(recreate_issuer_applications_path,
                              { 'issuer_application_form' => params['issuer_application_form'] })
  end

  def auditor
    Auditor.new
  end

  def audit_params
    params[:issuer_application_form].merge(user_id: @user.id)
  end

  def send_welcome(user)
    if user.has_no_password? && user.user_type_entrepreneur?
      user.send_confirmation_instructions({}, :issuer_welcome)
    end
  end

end
