require 'entity_form'

module Invest
  class InvestmentProfilesController < InvestController
    before_action :assign_company
    before_action :initialize_forms
    before_action :assign_active_form
    before_action :assign_active_form_key
    before_action :assign_form_labels

    def new
      reset_risk_profile if investment.is_reg_c?
      render_new_or_edit
    end

    def edit
      render_new_or_edit
    end

    def create
      if active_form.save
        audit_form!

        entity = active_form.entity
        status = entity.accreditation.status.approved? ? :verified : :pending
        investment.update_attributes investor_entity: entity, accreditation_status_id: status
        idology_locate(entity)
        InvestorEntity.where(user_id: current_user.id).where('id NOT IN (?)', active_investor_entity_ids).destroy_all
        if investment.can_edit_profile?
          investment.edit_profile!
        else
          investment.select_entity!
        end

        advance_to_next_step(investment)

        redirect_for_investment
      else
        setup_javascript_info
        template = investment.is_reg_c? ? :new_individual : :new
        render template, locals: { path: editing_via_progress_bar? ? 'edit' : 'submit' }
      end
    end

    private

    def render_new_or_edit
      setup_javascript_info
      template = investment.is_reg_c? ? :new_individual : :new
      @offering_presenter = OfferingPresenter.new(@offering)
      render template, locals: { path: editing_via_progress_bar? ? 'edit' : 'submit' }
    end

    def advance_to_next_step(investment)
      if investment.is_reg_s? || investment.is_reg_c?
        investment.verify_accreditation!
      end
    end

    def setup_javascript_info
      gon.activeEntityId = @active_form_data[:entity_id] if @active_form_data
      gon.activeForm = @active_form_key
      gon.formLabels = @form_labels
      gon.offeringId = @offering.id
    end

    def reset_risk_profile
      form = @forms[:individual]
      form.clear_core_params unless form.blank?
    end

    def initialize_forms
      @forms ||= set_forms
    end

    def set_forms
      Hash.new.tap do |hash|
        hash[:individual] = IndividualForm.new(params[:individual_form], current_user, { investment: investment })
        hash[:new] = EntityForm.new(params[:entity_new], current_user, { investment: investment })

        non_individual_entities.each do |e|
          entity = e[:entity]
          hash[entity.id] = EntityForm.new(params["entity_#{entity.id}"], current_user, { entity: entity, investment: investment })
        end
      end
    end

    def form_labels
      @form_labels ||= {}.tap do |hash|
        [:trust, :llc, :partnership, :corporation].each do |key|
          hash[key] = [{ key: '#entity-info > .jq-entity-title', text: I18n.t("investment_profile.new.entity.form.#{key.to_s}.information.title") },
                       { key: '#entity-info .jq-entity-name label', text: I18n.t("investment_profile.new.entity.form.#{key.to_s}.information.name") },
                       { key: '#contact-info > .jq-entity-contact', text: I18n.t("investment_profile.new.entity.form.#{key.to_s}.contact.title") },
                       { key: '.signatory_form .jq-entity-auth label', text: I18n.t("investment_profile.new.entity.form.#{key.to_s}.personal.confirmed_authorization") },
                       { key: '.signatory_form .jq-entity-position label', text: I18n.t("investment_profile.new.entity.form.#{key.to_s}.personal.position") }]
        end
        hash
      end
    end

    alias :assign_form_labels :form_labels

    def non_individual_entities
      @non_individual_entities ||=
        current_user.investor_entities.order(:created_at).reject { |i| i.is_a?(InvestorEntity::Individual) }.map do |e|
          {
            entity: e,
            entity_id: e.id,
            kind: entity_to_type(e),
            key: forms_key(:entity, e.id)
          }
        end
    end

    def active_form
      return nil unless active_form_data

      @active_form ||=
        if active_form_data[:entity_type] == 'individual'
          @forms[:individual]
        else
          @forms[active_form_data[:entity_id]]
        end
    end

    alias :assign_active_form :active_form

    def assign_active_form_key
      return unless active_form_data
      @active_form_key = forms_key(active_form_data[:entity_type], active_form_data[:entity_id])
    end

    def active_form_data
      @active_form_data ||= get_active_form_data
    end

    def get_active_form_data
      [:new].concat(non_individual_entities.map { |x| x[:entity_id] }).each do |entity_id|
        entity_types.each do |entity_type|
          key = forms_key(:entity, entity_id)
          return { entity_id: entity_id, entity_type: entity_type } if params.has_key?(key)
        end
      end

      return { entity_id: nil, entity_type: 'individual' } if params.has_key?('individual_form')

      existing_entity_data
    end

    def existing_entity_data
      entity = investment.investor_entity
      if entity
        Hash.new.tap do |hash|
          hash[:entity_type] = entity_to_type(entity)
          hash[:entity_id] = entity.id unless entity.is_a?(InvestorEntity::Individual)
        end
      end
    end

    def entity_to_type(entity)
      entity.class.name.demodulize.downcase
    end

    def forms_key(entity_type, entity_id)
      if entity_id then
        entity_id == 'new' ? :new : "entity_#{entity_id}".to_sym
      else
        entity_type
      end
    end

    def entity_types
      @entity_types ||= %w{ trust llc partnership corporation }
    end

    def active_investor_entity_ids
      Investment.where(user_id: current_user.id).pluck(:investor_entity_id).compact
    end

    def auditor
      EntityFormAuditor.new
    end

    def audit_params
      attrs = active_form.attributes.clone
      attrs['form_class'] = active_form.class.to_s
      attrs
    end

    def idology_locate(entity)
      if entity.is_domestic_individual_entity?
        begin
          entities = IdologyEntityParameterBuilder.build(entity)
          result = IDology::Subject.new(entities).locate
          update_investor_info(entity, result)
        rescue Exception => e
          Honeybadger.notify e
        end
      end
    end

    def update_investor_info(entity, result)
      entity.update_cip_status(result[:identified])
      entity.update_attribute(:idology_id, result[:response].id.to_s)
      current_user.allowed! if entity.verified? && current_user.pending?
    end

  end
end
