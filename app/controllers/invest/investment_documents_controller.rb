module Invest
  class InvestmentDocumentsController < InvestController
    include InvestmentSummaryAssignment

    before_action :assign_company
    before_action :assign_investment_summary
    before_action :setup_offering_js, only: [:show]

    layout "investment_flow"

    def show
      @investment_presenter = InvestmentPresenter.new(@investment)
      @documents = DealDocument.active(@investment.security)
      @offering_presenter = OfferingPresenter.new(@offering)

      additional_javascript_vars
    end

    def accept
      if (params[:reviewed_information].present?)
        set_docusign_requirements
        render partial: "sign"
      else
        @errors = { base: [], reviewed_information: I18n.t('investment_documents.sections.review.error_message') }
        render json: @errors
      end
    end

    def docusign_redirect
      last_event = params[:event]
      @redirect_to_url =
          case last_event
            when 'cancel'
              offering_invest_investment_documents_path(@offering.id)
            when 'signing_complete', 'decline', 'viewing_complete'
              docusign_handler_offering_invest_investment_documents_path(@offering.id)
            else
              dashboard_investments_path
          end
      gon.event = last_event
      render layout: false
    end

    def docusign_handler
      envelope = DocusignInvestmentAgreementEnvelope.new(@investment.envelope_id)

      if envelope.investor_signed?
        @investment.update_attribute :docs_signed_at, envelope.investor_signed_at
        sign_documents
      elsif envelope.investor_declined?
        @investment.decline_documents! unless @investment.declined_documents?
        @investment.update_attributes envelope_email: nil, envelope_id: nil
        redirect_to offering_invest_investment_documents_path(@offering)
      else
        redirect_to dashboard_investments_path
      end
    end

    private

    def set_docusign_requirements
      if @investment.cached_envelope_details.blank? || !(@investment.cached_envelope_details.symbolize_keys == docusign_params.envelope_details)
        envelope = DocusignInvestmentAgreementEnvelope.create(docusign_params.template_id, docusign_params.envelope_details)
        @investment.update_attributes(envelope_id: envelope.envelope_id, envelope_email: @investment.user.email, cached_envelope_details: docusign_params.envelope_details)
      end

      return_url     = docusign_redirect_offering_invest_investment_documents_url(@offering.id)
      @agreement_url = DocusignInvestmentAgreementUrl.get_embedded_url(@investment.envelope_id, docusign_params.investor_name, docusign_params.investor_email, return_url)
    rescue DocusignInvestmentAgreementEnvelope::EnvelopeNotCreatedError

      message = "DocuSign error for investment with ID# #{@investment.id}"
      logger.warn(message)
      Honeybadger.notify(
          error_message: message,
          parameters:    {}
      )
      raise Exception.new message
    end

    def sign_documents
      @investment.sign_documents!
      audit_form!

      redirect_for_investment
    end

    def docusign_params
      @docusign_params ||= DocusignInvestorAgreementParameterBuilder.new(@investment.id)
    end

    def audit_params
      {
          investment_amount:            @investment.amount.to_s,
          investment_shares:            @investment.shares.to_s,
          investment_percent_ownership: @investment.percent_ownership.to_s
      }
    end

    def confirmation_notice
      I18n.t('devise.failure.unconfirmed.sign_documents')
    end

    def additional_javascript_vars
      gon.reviewed_info_endpoint = accept_offering_invest_investment_documents_path(@offering)
    end

  end
end

