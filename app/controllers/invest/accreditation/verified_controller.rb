require 'accreditation_form'

module Invest
  module Accreditation
    class VerifiedController < BaseController
      before_action :setup_offering_js

      def new
        @accreditation_form = VerifiedEntityForm.new params
      end

      def create
        @accreditation_form = VerifiedEntityForm.new permitted_params

        if @accreditation_form.save
          @investment.verify_accreditation!
          audit_form!

          redirect_for_investment
        else
          render :new
        end
      end

      private

      def permitted_params
        params.require(:verified_entity_form).permit(:promise)
      end

      def audit_params
        permitted_params.merge(user_id: current_user.id)
      end
    end
  end
end
