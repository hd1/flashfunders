require 'accreditation_form'

module Invest
  module Accreditation
    class NewController < BaseController

      before_action :initialize_forms
      before_action :assign_active_form
      before_action :assign_document_uploader
      before_action :setup_offering_js

      skip_before_action :redirect_by_entity_status, only: [:edit, :update]


      def new
        @action_url    = offering_invest_accreditation_new_path(@offering)
        @action_method = :post
        render_accreditation_form
      end

      def create
        if active_form.save
          investment.verify_accreditation! if investment.can_verify_accreditation?

          audit_form!

          redirect_for_investment
        else
          @action_url    = offering_invest_accreditation_new_path(@offering)
          @action_method = :post
          render_accreditation_form
        end
      end

      def edit
        @action_url      = offering_invest_accreditation_new_path(@offering)
        @action_method   = :patch
        @suppress_navbar = !editing_via_progress_bar? || @investment.funding_method_selected?
        render_accreditation_form
      end

      def update
        previous_investment_qualification = @investment.current_accreditor_qualification
        if active_form.save

          @investment.regenerate_accreditations

          qualification = @investment.reload.current_accreditor_qualification
          if qualification.is_a? Accreditor::ThirdPartyQualification
            if previous_investment_qualification.is_a? Accreditor::ThirdPartyQualification
              if qualification.email != previous_investment_qualification.email
                @investment.trigger_investor_verification if @investment.has_signed_documents?
              end
            else
              @investment.trigger_investor_verification if @investment.has_signed_documents?
            end
          end
          audit_form!

          redirect_for_investment
        else
          @action_url      = offering_invest_accreditation_new_path(@offering)
          @action_method   = :patch
          @suppress_navbar = !editing_via_progress_bar?
          render_accreditation_form
        end
      end

      private

      def render_accreditation_form
        additional_javascript_vars
        if @investment.by_individual?
          render :new
        else
          render :new_entity
        end
      end

      def assign_document_uploader
        @uploader                       = ::Accreditation::Document.new.document
        @uploader.success_action_status = '201'
      end

      def initialize_forms
        @forms = {}.tap do |hash|
          AccreditationForm.descendants.map { |form_class|
            form_name              = form_class.name.underscore
            hash[form_name.to_sym] = form_class.new(form_params(form_name), current_user, investor_entity)
          }
        end
      end

      def form_params(form_name)
        params[form_name] || (accreditation.try(:qualification) ? augment_params(accreditation.qualification, form_name) : {})
      end

      def augment_params(qualification, form_name)
        augmented_params = qualification.data.symbolize_keys
        if form_name == form_class_for_edit
          augmented_params[:file_keys] = qualification.documents.map { |doc| doc.document.path }
        end
        augmented_params
      end

      def active_form
        @active_form ||= form_class.nil? ? form_class : @forms[form_class.to_sym]
      end

      alias :assign_active_form :active_form

      def form_class
        @form_class = @forms.keys.detect { |key| params.has_key?(key) } || form_class_for_edit
      end

      def form_class_for_edit
        if accreditation.try(:qualification)
          accreditation.qualification.class.to_s.demodulize.underscore.gsub('_qualification', '_form')
        end
      end

      def auditor
        AccreditationFormAuditor.new
      end

      def audit_params
        active_form.attributes
      end

      def additional_javascript_vars
        gon.form_class = @form_class
      end

    end
  end
end
