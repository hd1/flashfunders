require 'accreditation_form'

module Invest
  module Accreditation
    class BaseController < InvestController
      before_action :assign_company
      before_action :assign_investor_entity
      before_action :assign_accreditation
      before_action :redirect_by_entity_status

      private

      def investor_entity
        @investor_entity ||= @investment.investor_entity
      end
      alias_method :assign_investor_entity, :investor_entity

      def accreditation
        @accreditation ||= investment.accreditation
      end
      alias_method :assign_accreditation, :accreditation

      def auditor
        AccreditationFormAuditor.new
      end

      def redirect_by_entity_status
        if accreditation.status.approved?
          redirect_to new_offering_invest_accreditation_verified_path unless controller_name == 'verified'
        else
          redirect_to new_offering_invest_accreditation_new_path unless controller_name == 'new'
        end
      end
    end
  end
end
