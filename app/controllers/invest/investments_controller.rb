module Invest
  class InvestmentsController < InvestController
    include InvestmentSummaryAssignment
    prepend_before_action :save_recreate_params, only: :create

    skip_before_action :authenticate_user!, only: [:new, :create]
    skip_before_action :redirect_to_correct_path_for_state, only: [:update_payment, :resend_verification]

    before_action :set_offering_presenter

    def new
      @investor_investment = InvestorInvestmentForm.new({ offering_id: @offering.id })
      @offering_presenter = OfferingPresenter.new(@offering)
      if current_user
        offering_follower = OfferingFollower.find_or_initialize_by(offering_id: @offering_presenter.offering_id, user: current_user)
        offering_follower.follow_as!(:investor)
      end
      @unrounded_investment_amount = 0
      @investment_amount = 0
      @investment_shares = 0
      @investment_percent_ownership = 0
      render investment_form, locals: { path: 'commit' }
    end

    def create
      @investor_investment = InvestorInvestmentForm.new(form_params, investor_investment_form_validator)

      if investor_investment_form_validator.valid?
        authenticate_user!
        offering_follower = OfferingFollower.find_or_initialize_by(offering_id: @offering_presenter.offering_id, user: current_user)
        offering_follower.follow_as!(:investor)

        @investment = @investor_investment.create

        audit_form!

        redirect_for_investment
      else
        render_errors('commit', @investor_investment)
      end
    end

    def recreate
      unless !investment.persisted? && super
        redirect_for_investment
      end
    end

    def edit
      @investor_investment = InvestorInvestmentForm.new({ offering_id: @offering.id, investment_id: @investment.id })
      @offering_presenter = OfferingPresenter.new(offering)

      @unrounded_investment_amount = @investment.amount || 0
      @investment_amount = @investment.amount || 0
      @investment_shares = @investment.shares || 0
      @investment_percent_ownership = @investment.percent_ownership || 0

      render investment_form, locals: { path: 'edit' }
    end

    def update
      @investor_investment = InvestorInvestmentForm.new(form_params.merge({ investment_id: @investment.id }), investor_investment_form_validator)

      if investor_investment_form_validator.valid?
        @investor_investment.update

        audit_form!

        redirect_for_investment
      else
        render_errors('edit', @investor_investment)
      end
    end

    def update_payment
      sent = params[:yes_or_no] == 'true'
      if sent
        @investment.send_funds! if @investment.funding_method_selected?
      else
        @investment.select_funding_method! if @investment.funds_sent?
      end
      result = { payment_sent: sent }
      result.merge({ kind: @investment.funding_method }) if @investment.funding_method.present?
      render json: result
    end

    def resend_verification
      @investment.resend_verification_email
      render json: { sent: true }
    end

    private

    def save_recreate_params
      store_recreate_to_session(recreate_offering_investment_url(params[:offering_id]),
                                { 'investment' => params['investment'] })
    end

    def render_errors(path, form)
      Rails.logger.warn(form.errors.first)
      present_invalid_investment(permitted_params[:unrounded_investment_amount])
      render investment_form, locals: { path: path }
    end

    def investment_form
      @offering_presenter.convertible? ? 'investment_notes_form' : 'investment_stock_form'
    end

    def permitted_params
      params.require(:investment).permit(
        :investment_amount,
        :investment_shares,
        :investment_percent_ownership,
        :unrounded_investment_amount)
    end

    def form_params
      {
        user: current_user,
        offering_id: @offering.id,
        investment_amount: investor_investment_calculator.projected_cost,
        investment_shares: investor_investment_calculator.projected_shares,
        investment_percent_ownership: investor_investment_calculator.projected_percent_ownership
      }
    end

    def present_invalid_investment(unrounded_investment_amount)
      @unrounded_investment_amount = unrounded_investment_amount
      @investment_amount = 0
      @investment_shares = 0
      @investment_percent_ownership = 0
    end

    def offering_presenter
      @offering_presenter ||= OfferingPresenter.new(@offering)
    end
    alias :set_offering_presenter :offering_presenter

    def investor_investment_calculator
      @calculator ||= InvestorInvestmentCalculator.get_calc(permitted_params[:unrounded_investment_amount], offering_presenter)
    end

    def investor_investment_form_validator
      @validator ||= InvestorInvestmentFormValidator.new(permitted_params, investor_investment_calculator, offering_presenter)
    end

    def audit_params
      {
        desired_investment_amount: permitted_params[:unrounded_investment_amount],
        displayed_investment_amount: permitted_params[:investment_amount],
        actual_investment_amount: investor_investment_calculator.projected_cost.to_s,
        displayed_investment_shares: permitted_params[:investment_shares],
        actual_investment_shares: investor_investment_calculator.projected_shares.to_s,
        displayed_investment_percent_ownership: permitted_params[:investment_percent_ownership],
        actual_investment_percent_ownership: investor_investment_calculator.projected_percent_ownership.to_s
      }
    end

    def additional_javascript_vars
      gon.investment_form = investment_form

    end

  end
end
