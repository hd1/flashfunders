module Invest
  class FundsController < InvestController
    include FormAuditing
    include InvestmentSummaryAssignment
    include AnalyticsTracker

    before_action :assign_existing_accounts
    before_action :assign_company
    before_action :assign_investment_summary
    before_action :setup_offering_js, only: [:new, :create, :edit, :update]

    def new
      @form = InvestorFundForm.new(current_user, investment)
      render_form
    end

    def create
      @form = InvestorFundForm.new(current_user, investment, { params: permitted_params })
      if @form.valid?
        @form.save
        send_payment_instructions_email(investment)
        audit_form!
        redirect_to complete_offering_invest_fund_path(@offering)
      else
        @investment.funding_method = permitted_params[:funding_method]
        render_form
      end
    end

    def edit
      @bank_account = Bank::InvestorAccount.find(investment.investor_account_id)
      existing_data = {
          user_id:              current_user.id,
          bank_account_holder:  @bank_account.account_holder,
          bank_account_number:  @bank_account.mask(2),
          bank_account_routing: @bank_account.routing_number,
          bank_account_type:    @bank_account.account_type,
          funding_type:         @bank_account.funding_type }

      @form            = InvestorFundForm.new(current_user, investment, { params: existing_data })
      @suppress_navbar = true
      render_form
    end

    def update
      previous_funding_method = @investment.funding_method
      params = permitted_params
      investor_account = Bank::InvestorAccount.find(@investment.investor_account_id)
      if params[:bank_account_number] && params[:bank_account_number].include?('**')
        params[:bank_account_number] = investor_account.account_number
      end
      @form = InvestorFundForm.new(current_user, investment, { params: params })
      if @form.valid?
        @form.update investor_account
        send_payment_instructions_email(investment) unless previous_funding_method == @investment.reload.funding_method
        audit_form!
        redirect_for_investment
      else
        @suppress_navbar = true
        render_form
      end
    end

    def complete
      @investment.security.escrow_provider.notify_completion!(@investment)
      load_data
      @show_expanded = true

      track_facebook_pixel(ff_facebook_pixel_id, 'Purchase')
      render layout: 'application'
    end

    private

    def assign_existing_accounts
      @existing_accounts = current_user.investor_accounts
    end

    def save_linked_bank_account(bank_account_info)
      investor_account = Bank::InvestorAccount.new({
                                                       user_id:        current_user.id,
                                                       account_holder: bank_account_info[:bank_account_holder],
                                                       account_number: bank_account_info[:bank_account_number],
                                                       routing_number: bank_account_info[:bank_account_routing],
                                                       account_type:   bank_account_info[:bank_account_type],
                                                       funding_type:   bank_account_info[:funding_type],
                                                   })
      if investor_account.save
        create_banking_data(investor_account)
        audit_form!
        true
      else
        @errors = investor_account.errors
      end

    end

    def permitted_params
      params.require(:investor_fund_form).permit(:bank_account_id, :funding_method, :bank_account_holder, :bank_account_type, :bank_account_routing, :bank_account_number, :passport_image, :funding_type, :funding_international)
    end

    def auditor
      InvestorFundFormAuditor.new
    end

    def audit_params
      permitted_params
    end

    def create_banking_data(investor_account)
      investment.update_attributes(investor_account_id: investor_account.id)
    end

    def campaign_presenter
      OfferingPresenter.new(offering)
    end

    def social_presenter
      SocialPresenterFactory.build(offering_url(investment.offering_id, protocol: 'http'), t('show_campaign.social.twitter.account'), campaign_presenter.company_name, offering.tagline)
    end

    def load_data
      investment_presenter = InvestmentPresenter.new(investment)
      @investment_hash = {
          investment_summary: @investment_summary,
          investment:         investment_presenter,
          campaign:           campaign_presenter,
          funding_detail:     investment_presenter.funding_detail_domestic,
          funding_international_detail: investment_presenter.funding_detail_international,
          social:             social_presenter,
      }
    end

    def send_payment_instructions_email(investment)
      if investment.security.escrow_provider.show_payment_details?
        InvestmentMailer.funding_method_selected(investment.id).deliver_now
      end
    end

    def additional_javascript_vars
      gon.funding_method = investment.funding_method
    end

    def render_form
      additional_javascript_vars
      render :new, locals: { funding_method: investment.funding_method }
    end

  end
end
