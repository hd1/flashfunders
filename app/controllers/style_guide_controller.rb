class StyleGuideController < ApplicationController

  skip_before_filter :authenticate_user!
  
  layout 'style_guide'

  def index
    @style_guide_form = StyleGuideForm.new
  end

  private
  class StyleGuideForm
    extend ActiveModel::Naming
    include ActiveModel::Conversion

    ATTRIBUTES= [
    :first_name, :middle_initial, :last_name, :date_of_birth, :ssn, :address1,
    :address2, :city, :zip_code, :phone_number, :state_id, :binary_radio, :checkboxes,
    :select_collection_id, :checkbox_collection_ids,
    ]

    attr_reader *ATTRIBUTES

    def initialize
      @checkbox_collection_ids = collection.map(&:id)
    end

    def persisted?
      false
    end

  end
end
