class OmniauthController < Devise::OmniauthCallbacksController
  def facebook
    flash.alert.clear if flash.alert
    @user = User.from_omniauth(request.env['omniauth.auth'])

    if @user.persisted? # signin
      @user.skip_confirmation!
      sign_in_and_redirect @user
    elsif @user.valid? # signup
      @user.save!
      @user.skip_confirmation!
      after_signup_tasks(@user)
      sign_in_and_redirect @user
    else
      # This happens once for the first time user don't give his email
      # This is not going to get called second time because facebook will disable
      # the sumbit button if you don't give us an email. (This works with 'rerequest' configured)
      redirect_to user_omniauth_authorize_path(:facebook)
    end
  end

  def failure
    redirect_to root_path
  end

  private

  def after_signup_tasks(user)
    flash[:analytics_sign_up_email] = { success: user.email } if is_navigational_format?
    audit!(user)
    track_visitor(user)
    send_welcome(user)
    send_checkin_in(user)
  end

  def send_welcome(resource)
    resource.send_confirmation_instructions({}, :welcome_instructions) unless resource.has_no_password?
  end

  def send_checkin_in(resource)
    InvestorMailer.delay_for(2.weeks).checking_in(resource.id) if resource.user_type_investor?
  end

  def audit_params
    request.env["omniauth.auth"]
  end

  def audit!(user)
    auditor.write(audit_params, user.id, request.ip, request.path, registered_at)
  end

  def auditor
    RegistrationFormAuditor.new
  end

  def registered_at
    @registered_at ||= Time.now.utc
  end

end
