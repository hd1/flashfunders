class AccreditationMailer < ApplicationMailer
  default from: I18n.t('contacts.other')

  layout 'layouts/mailer_with_closing'
  helper [:application, :email]

  def third_party_person_verification_email(qualification, options = {})
    @qualification = qualification
    @investor_name = options.fetch(:investor_name)
    investor_email = options.fetch(:investor_email)

    subject = t('accreditation_mailer.third_party.person_verification.subject')
    address = "#{@investor_name} <#{I18n.t('contacts.accreditation')}>"

    mail(to: qualification.email, reply_to: investor_email, cc: investor_email, from: address, subject: subject) do |format|
      format.html { render layout: 'layouts/snazzy_mailer' }
    end
  end

  def third_party_entity_verification_email(qualification, options = {})
    @qualification = qualification
    @entity_name = options.fetch(:entity_name)
    @investor_name = options.fetch(:investor_name)
    investor_email = options.fetch(:investor_email)

    subject = t('accreditation_mailer.third_party.entity_verification.subject')
    address = "#{@investor_name} <#{I18n.t('contacts.accreditation')}>"

    mail(to: qualification.email, reply_to: investor_email, cc: investor_email, from: address, subject: subject) do |format|
      format.html { render layout: 'layouts/snazzy_mailer' }
    end
  end

end
