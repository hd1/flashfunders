class OfferingCommentMailer < ApplicationMailer
  layout 'layouts/snazzy_mailer'

  helper [:application, :email, :offering]

  def reply_comment_from_issuer(offering_id, comment_user_id, reply_id)
    @offering = Offering.find(offering_id)
    @offering_presenter = OfferingPresenter.new(@offering)
    @user = User.find(comment_user_id)
    @reply_comment = OfferingComment.find_by(id: reply_id, user_id: @offering.issuer_user.id)

    mail \
      to: @user.email,
      from: formatted_from(@offering.public_contact_name),
      subject: I18n.t('offering_comment_mailer.reply_comment_from_issuer.subject', issuer_name: @offering.public_contact_name)
  end

  def comment_from_user(offering_id, comment_user_id, comment_id)
    @offering = Offering.find(offering_id)
    @offering_presenter = OfferingPresenter.new(@offering)
    @user = User.find(comment_user_id)
    @comment = OfferingComment.find_by(id: comment_id, user_id: @user.id)

    mail \
      to: @offering.issuer_user.email,
      from: formatted_from(@user.full_name),
      subject: I18n.t('offering_comment_mailer.comment_from_user.subject')
  end

  private

  def formatted_from(name)
    "\"#{name} (via FlashFunders)\" <no-reply@flashfunders.com>"
  end

end
