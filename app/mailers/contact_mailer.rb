class ContactMailer < ApplicationMailer
  layout 'layouts/mailer'

  helper :application


  USER_TYPES = {
    'investor' => I18n.t('contacts.investors'),
    'startup'  => I18n.t('contacts.startups'),
    'other'    => I18n.t('contacts.other')
  }

  def contact_us(user_type, name, email, content, page_name, user_id)
    @message   = content
    user_type = 'other' unless USER_TYPES.include? user_type

    mail to: USER_TYPES[user_type],
      from: %("#{name}" <#{email}>),
      subject: "New email from a user #{' (' + user_id.to_s + ')' unless user_id.nil? } on the '#{page_name}' page"
  end

end

