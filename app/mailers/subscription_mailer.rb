class SubscriptionMailer < ApplicationMailer
  default from: "#{I18n.t('subscription_mailer.welcome.email.name')} <#{I18n.t('contacts.investors')}>"

  helper [:application, :email]
  layout 'layouts/snazzy_mailer'

  def welcome_email(subscriber_email)
    mail(to: subscriber_email, subject: t('subscription_mailer.welcome.subject'))
  end
end
