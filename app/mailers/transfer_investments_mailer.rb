class TransferInvestmentsMailer < ApplicationMailer
  default from: I18n.t('contacts.other')

  layout 'layouts/mailer_with_closing'
  helper :application

  def send_email(investment_id, error_s)
    @investment = Investment.find(investment_id)
    @error = error_s

    subject = "An Escrow Provider error has occurred"
    params = internal_email_params

    mail(to: params[:recipient], cc: params[:cc_email], subject: subject, from: I18n.t('contacts.no_reply'))
  end

end
