class DocusignMailer < ApplicationMailer

  def notify(investment_id)
    params = internal_email_params
    subject = "Investment #{investment_id} has been signed out of band"

    mail(to: params[:recipient], cc: params[:cc_email], subject: subject, from: I18n.t('contacts.no_reply'))
  end
end
