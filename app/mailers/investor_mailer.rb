class InvestorMailer < ApplicationMailer
  default from: "#{I18n.t('investor_mailer.checking_in.email.name')} <#{I18n.t('contacts.investors')}>"

  layout 'layouts/snazzy_mailer'
  helper [:application, :email]

  def checking_in(user_id, skip_check = false)
    @user = User.find(user_id)

    if @user && (needs_checkin_reminder?(@user) || skip_check)
      mail(to: @user.email, subject: t('investor_mailer.checking_in.subject'))
    end
  end

  private

  def needs_checkin_reminder?(user)
    # User registered as an investor and hasn't started an investment or sent a message
    user.user_type_investor? &&
      user.investments.count == 0 &&
      user.offering_messages.count == 0
  end

end
