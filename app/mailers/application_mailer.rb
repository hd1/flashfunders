class ApplicationMailer < ActionMailer::Base
  default 'Content-Transfer-Encoding' => 'quoted-printable'

  def internal_email_params
    {}.tap do |hash|
      if Rails.env.production?
        hash[:recipient] = I18n.t('contacts.operations')
        hash[:cc_email] = I18n.t('contacts.engineering')
      else
        hash[:recipient] = I18n.t('contacts.engineering')
        hash[:cc_email] = ''
      end
    end
  end

  def mail(*args)
    mailer_category_header message, "#{self.class.name}-#{self.action_name}"
    super
  end

  def mailer_category_header(message, source_class)
    message.headers "X-SMTPAPI" => {
      category:    [ "#{source_class}" ]
    }.to_json
  end
end
