class OfferingMailer < ApplicationMailer
  layout 'layouts/snazzy_mailer'
  default from: I18n.t('contacts.no_reply')

  helper [:application, :email]

  def message_from_user(offering_id, user_id, content)
    @offering = Offering.find(offering_id)
    @user = User.find(user_id)
    @salutation = @user.full_name
    @offering_message = content
    @legal_key = 'to_issuer'

    mail \
      to: @offering.public_email,
      from: formatted_from(@salutation),
      subject: I18n.t('offering_mailer.from_user.subject', company: @offering.company.name)
  end

  def message_from_issuer(offering_id, user_id, content)
    @offering = Offering.find(offering_id)
    @user = User.find(user_id)
    @offering_message = content
    @legal_key = 'to_investor'

    mail \
      to: @user.email,
      from: formatted_from(@offering.public_contact_name),
      subject: I18n.t('offering_mailer.from_issuer.subject', company: @offering.company.name)
  end

  private

  def formatted_from(name)
    "\"#{name} (via FlashFunders)\" <#{I18n.t('contacts.no_reply')}>"
  end
end
