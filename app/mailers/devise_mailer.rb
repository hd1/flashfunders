class DeviseMailer < Devise::Mailer
  include Devise::Controllers::UrlHelpers
  helper [:email, :application]
  layout 'layouts/snazzy_mailer'

  def confirmation_instructions(record, token, options={}, action_name=nil)
    super(record, token, options)
  end

  def welcome_instructions(record, token, options={})
    unless options.has_key?(:template_name)
      options[:template_name] = 'welcome'
      options[:subject] = 'Welcome to FlashFunders!'
    end
    @company = options[:company] || 'Unknown Company'
    confirmation_instructions(record, token, options, self.action_name)
  end

  def issuer_welcome(record, token, options={})
    options[:template_name] = 'completed_issuer_application'
    options[:subject] = I18n.t('confirmation_instructions.issuer_application.subject')
    confirmation_instructions(record, token, options, self.action_name)
  end
end
