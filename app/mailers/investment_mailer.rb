class InvestmentMailer < ApplicationMailer
  default from: I18n.t('contacts.investor_team')
  helper [:application, :email]

  def funds_received(investment_id)
    @investment = Investment.find(investment_id)
    @presenter = OfferingPresenter.new(@investment.offering)
    @social_urls = social_urls(investment_id)
    @offering_photo = offering_photo(@investment)

    mail(to: @investment.user.email,
         bcc: t('contacts.compliance'),
         subject: t('investment_mailer.funds_received.subject')) do |format|

      format.html { render layout: 'layouts/snazzy_mailer' }
    end
  end

  def reminds_to_complete(investment_id)
    investment = Investment.find(investment_id)
    return unless investment.intended?
    @investment = investment
    @offering_photo = offering_photo(@investment)

    mail(from: I18n.t('contacts.investors'), to: investment.user.email, subject: t('investment_mailer.reminds_to_complete.subject')) do |format|
      format.html { render layout: 'layouts/snazzy_mailer' }
      format.text { render layout: 'layouts/mailer' }
    end
  end

  def funding_method_selected(investment_id)
    investment = Investment.find(investment_id)
    @offering_presenter = OfferingPresenter.new(investment.offering)

    load_payment_methods_resources(investment, investment.funding_method) unless investment.ach?

    do_mail(investment, I18n.t('investment_mailer.funding_method_selected.subject', company: @offering_presenter.company_name), self.action_name, layout: 'layouts/snazzy_mailer')
  end

  def offering_target_reached(investment_id, data={})
    @social_urls = social_urls(investment_id)
    do_reg_c_mail(investment_id, 'investment_mailer.offering_target_reached.subject', self.action_name, layout: 'layouts/snazzy_mailer')
  end

  def counter_signed(investment_id, data={})
    do_reg_c_mail(investment_id, 'investment_mailer.counter_signed.subject', self.action_name, layout: 'layouts/snazzy_mailer')
  end

  private

  def do_reg_c_mail(investment_id, key, action_name, opts={})
    investment = Investment.find(investment_id)
    @offering_presenter = OfferingPresenter.new(investment.offering)

    do_mail(investment, I18n.t(key, company: @offering_presenter.company_name), action_name, opts)
  end

  def do_mail(investment, subject, action_name, opts={})
    @investment = investment
    @offering_photo = offering_photo(@investment)

    mail(to: investment.user.email,
         subject: subject,
         bcc: t('contacts.compliance')) do |format|
      format.html { render opts.reverse_merge(layout: 'layouts/mailer') }
    end
  end

  def load_payment_methods_resources(investment, method)
    @investment = investment
    funding_detail = TransferFundsPresenter.new(investment, 'domestic')
    method == 'check' ?
      funding_international_detail = nil :
      funding_international_detail = TransferFundsPresenter.new(investment, 'international')
    @funding_details = {domestic: funding_detail, international: funding_international_detail}
  end

  def social_urls(investment_id)
    investment = Investment.find(investment_id)
    presenter = OfferingPresenter.new(investment.offering)
    SocialPresenterFactory.build(
      offering_url(investment.offering_id, protocol: 'http'),
      t('show_campaign.social.twitter.account'),
      presenter.company_name,
      presenter.tagline)
  end

  def offering_photo(investment)
    investment.offering.photo_swatch_url || 'mailer/header_photo3.png'
  end

end
