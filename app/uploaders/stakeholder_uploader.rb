class StakeholderUploader < CarrierWave::Uploader::Base
  include CarrierWaveDirect::Uploader

  def store_dir
    "offering/stakeholders"
  end

end


