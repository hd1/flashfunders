class PassportUploader < CarrierWave::Uploader::Base

  def fog_public
    false
  end

  def store_dir
    "uploads/passport/#{model.user.uuid}"
  end

end
