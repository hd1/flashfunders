class AccreditationDocumentUploader < CarrierWave::Uploader::Base

  include CarrierWaveDirect::Uploader

  def store_dir
    "uploads/accreditation"
  end

end

