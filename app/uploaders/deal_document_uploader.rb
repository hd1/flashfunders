require 'carrierwave_timestamp'

class DealDocumentUploader < CarrierWave::Uploader::Base
  include CarrierWaveTimestamp

  def store_dir
    "#{model.documentable_type.underscore}/#{model.documentable_id}/#{model.type.underscore}"
  end

end

