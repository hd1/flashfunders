class DocumentUploader < CarrierWave::Uploader::Base
  include CarrierWaveDirect::Uploader

  def store_dir
    "offering/document"
  end

  def will_include_content_type
    true
  end

end

