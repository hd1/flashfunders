module ApplicationHelper
  def resource
    @resource ||= User.new
  end

  def nav_link(link_text, link_path, opts={})
    class_name = current_page?(link_path) ? 'active' : ''
    class_name = [class_name, opts[:class]].join(' ').strip

    content_tag(:li, class: class_name, data: opts[:data]) do
      link_to link_text, link_path, title: link_text, target: opts[:target]
    end
  end

  def page_title_prefix
    env_prefix = Rails.env.production? ? '' : "(#{Rails.env.titleize}) "
    "#{env_prefix}FlashFunders | "
  end

  def page_meta_title
    yml_key = page_meta_key
    t("meta.title.#{yml_key}", default: t('meta.title.default'))
  end

  def page_meta_description
    yml_key = page_meta_key
    { name: 'description', content: t("meta.description.#{yml_key}", default: t('meta.description.default')) }
  end

  def home_page?
    controller_name == 'home'
  end

  def browse_page?
    controller_name == 'browse'
  end

  def subscribe_page?
    controller_name == 'subscriptions'
  end

  RESPONSIVE_PAGES = {
    'home'                => { pages: %w(show) },
    'browse'              => { pages: %w(show) },
    'static'              => { pages: %w(contact press team startups investors faq_startup faq_investor), patterns: [] },
    'offerings'           => { pages: %w(show vanity_show) },
    'dashboard'           => { pages: %w(show) },
    'issuer_applications' => { pages: %w(new create) },
    'messages'            => { pages: %w(index show) }
  }

  def responsive_page?
    entry = RESPONSIVE_PAGES[controller_name]
    entry &&
      ((entry[:pages] && entry[:pages].include?(action_name)) ||
        (entry[:patterns] && entry[:patterns].detect { |pattern| action_name =~ pattern })
      )
  end

  def resource_name
    :user
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end

  def removable_error(f, field, opts={})
    f.error field, opts.merge('data-error-for' => field)
  end

  def doc_yaml_key(document)
    document.name.gsub(' ', '_').downcase
  end

  def with_popover(text, popover_text, title = nil)
    "#{text}<span class='in-label' data-info-popover='true' data-title='#{title}' data-content='#{popover_text}'></span>".html_safe
  end

  def inline_popover(text, popover_text, title, klass='inline-popover')
    title = title.split.map(&:capitalize).join(' ')
    "<span class='#{klass}' data-info-popover='true' data-title='#{title}' data-content='#{popover_text}'>#{text}</span>".html_safe
  end

  def link_to_finra(text='FINRA', opts={})
    link_to(text, 'http://www.finra.org/', opts.reverse_merge(target: '_blank'))
  end

  def link_to_sipc(text='SIPC', opts={})
    link_to(text, 'http://www.sipc.org/', opts.reverse_merge(target: '_blank'))
  end

  def link_to_terms(text='Terms of Use', opts={})
    link_to(text, terms_url, opts.reverse_merge(target: '_blank'))
  end

  def link_to_privacy(text='Privacy Policy', opts={})
    link_to(text, privacy_url, opts.reverse_merge(target: '_blank'))
  end

  def or_error(f, field, error_msg)
    "<span class=#{'error' if f.error(field)}>#{error_msg}</span>".html_safe
  end

  def us_states_options
    State.all.sort do |a, b|
      # States first, then sorted by name
      [(a.type == 'state' ? 0 : 1), a.name] <=> [(b.type == 'state' ? 0 : 1), b.name]
    end.map { |x| [x.name, x.code] }

  end

  def formatted_message(content)
    CGI::escapeHTML(content).gsub(/\n/, '<br />').html_safe
  end

  def user_has_subscribed_to_newsletter?
    cookies['_subscribed'] == 'true'
  end

  def has_dismissed_email_capture?
    cookies['_dismissed'] == 'true'
  end

  def should_render_newletter_modal?
    ! (has_dismissed_email_capture? || user_has_subscribed_to_newsletter? || user_signed_in? || subscribe_page?)
  end

  def with_http(image_url)
    unless image_url =~ /^(http|https)/
      "http:#{image_url}"
    else
      image_url
    end
  end

  def with_https(image_url)
    unless image_url =~ /^(http|https)/
      "https:#{image_url}"
    else
      image_url
    end
  end

  def footer_legal_links(link_attrs={})
    {
        finra_link: link_to_finra('FINRA', link_attrs),
        sipc_link: link_to_sipc('SIPC', link_attrs),
        terms_link: link_to_terms('Terms of Use', link_attrs),
        privacy_link: link_to_privacy('Privacy Policy', link_attrs),
        compensation_link: link_to('compensation', startups_path(anchor: :pricing), link_attrs)
    }
  end

  def link_target
    @within_blog ? '_parent' : '_self'
  end

  private

  def page_meta_key
    key    = request.path
    key[0] = '' if key[0] == '/'
    key    = 'default' if key.blank?
    key
  end

end
