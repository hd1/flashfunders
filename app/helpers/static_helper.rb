module StaticHelper
  def blog_url
    if Rails.env.production?
      'http://blog.flashfunders.com'
    else
      'http://blog.flashdough.com'
    end
  end

end
