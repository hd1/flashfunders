module DashboardHelper

  def offering_title_tabs
    @title_tabs ||= find_offerings_for_user.map do |offering|
      {
        tab_code_name: offering.company.name,
        name: offering_tab_name(offering),
        dashboard_path: controller.change_context_path(offering.id),
        current_offering: get_offering_context == offering.id ? 'current-offering' : ''
      }
    end
  end

  private

  def find_offerings_for_user
    @offerings_for_user ||= Offering.for_user(current_user)
  end

  def unread_for_issuer_offerings
    @unread_for_issuer_offerings ||= OfferingMessage.unread_for_issuer(current_user.id)
  end

  def offering_tab_name(offering)
    "#{offering.company.name}#{unread_str(unread_for_issuer_offerings[offering.id].to_i)}"
  end

  def unread_str(unread)
    unread > 0 ? " (#{unread})" : ''
  end

  def get_offering_context
    cookies['_startup'].to_i
  end

end
