module OfferingEditorHelper

  def add_section_button_tag(name, f, association, options = {})
    new_object = f.object.send(association).klass.new
    id = new_object.object_id

    template = f.simple_fields_for(association, new_object, child_index: id) do |builder|
      render("template_" + association.to_s.singularize, f: builder)
    end

    options[:class] ||= ''
    options[:class] << ' add-template'

    options[:data] ||= {}
    options[:data][:id] = id
    options[:data][:target] = '.content-creation'
    options[:data][:template] = template.gsub("\n", "")

    button_tag(name, options)
  end

end

