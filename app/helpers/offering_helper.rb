module OfferingHelper
  def follower_css(follower)
    'followed' if follower.following?
  end

  def offering_short_path(offering_presenter, opts={})
    if offering_presenter.vanity_path.present?
      offering_vanity_path({path: offering_presenter.vanity_path}.merge(opts))
    else
      offering_path({id: offering_presenter.offering_id}.merge(opts))
    end
  end

  def offering_vanity_url(offering_presenter, opts={})
    URI.join(root_url, offering_short_path(offering_presenter, opts))
  end

  def comment_tab_path(offering)
    @comment_path ||= "#{offering_short_path(offering)}#comment"
  end

  def path_for_delete(comment, offering_presenter, parent_id = nil)
    offering_comment_form_path(params_for_delete(comment, offering_presenter, parent_id))
  end

  def params_for_delete(comment, offering_presenter, parent_id)
    {
      offering_id: offering_presenter.offering_id,
      id: "#{comment.id}",
      parent_id: parent_id,
      return_to: comment_tab_path(offering_presenter)
    }
  end

  def is_issuer_user?(offering, user)
    offering.belongs_to_user?(user.try(:id))
  end

  def can_reply?(offering, user, comment)
    is_issuer_user?(offering, user) &&
      !comment.owner?(user) &&
      !comment.is_reply? &&
      !comment.has_visible_issuer_reply?
  end

  def can_message?(offering, user, comment)
    is_issuer_user?(offering, user) && !comment.owner?(user)
  end

  def can_delete?(offering, user, comment)
    is_issuer_user?(offering, user) || comment.owner?(user)
  end

  def total_visible_comments(offering_comments)
    @total ||= begin
      total = offering_comments.count +
        offering_comments.map { |comment| comment.children.visible }.select(&:present?).count

      total > 999 ? I18n.t('offering_comment.max_comment') : total
    end
  end

  def sort_securities
    @offering_presenter.security_presenters.sort_by do |security_presenter|
      [
        security_presenter.reg_cf? ? -1:1,
        security_presenter.reg_d? ? -1:1,
        security_presenter.is_spv? ? 1:-1
      ]
    end
  end

  def carousel_offerings
    offering_data.map do |company|
      {
        offering_presenter: carousel_tiles.detect { |tile| tile.company_name == company[:company_name] },
        bullets: company[:bullets]
      }
    end
  end

  private

  def carousel_tiles
    selected_companies = offering_data.map { |data| data[:company_name] }
    @tiles ||= Offering.includes(:company).where(companies: {name: selected_companies})
      .map { |offering| OfferingPresenter.new(offering) }
  end

  def offering_data
    [
      { company_name: 'The Influential Network',
        bullets: [
          { text: 'Successfully closed <span class="balance">$1M</span> on FlashFunders', date: "Sept '15" },
          { text: 'Closed a $5M Series A', date: "July '16" }
        ]
      },
      { company_name: 'MobileSpike',
        bullets: [
          { text: 'successfully closed <span class="balance">$112,034</span> on FlashFunders', date: "July '16" },
          { text: 'marked first successful Reg CF closing ever', date: "July '16" }
        ]
      },
      { company_name: 'Cartogram',
        bullets: [
          { text: 'successfully closed <span class="balance">$600,000</span> on FlashFunders', date: "Oct '15" },
          { text: 'ranked #1 indoor map on Google Play - beat Google Maps', date: "July '16" }
        ]
      }
    ]
  end

end
