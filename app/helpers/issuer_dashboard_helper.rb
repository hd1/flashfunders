module IssuerDashboardHelper
  def any_panel_data?(section, data)
    case section.to_sym
    when :investments
      data[:items].count > 0
    when :traffic_overview
      true
    end
  end

  def sorting_info(header_info)
    up_arrow = Issuer::DashboardController::UP_ARROW
    down_arrow = Issuer::DashboardController::DOWN_ARROW

    current_column = header_info
      .detect { |header| header[:title] =~ /(#{up_arrow}|#{down_arrow})/ }
    sort_dir = current_column[:sort_dir] == 'asc' ? 'desc' : 'asc'

    current_column.slice(:sort_by, :section).merge(sort_dir: sort_dir)
  end
end
