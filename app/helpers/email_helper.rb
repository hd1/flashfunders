module EmailHelper
  def color
    {
      primary_blue: '#0AA2F2',
      secondary_blue: '#0880bf',
      white_lilac: '#f6f6fb',
      white: '#ffffff',
      title_gray: '#34405D',
      mid_gray: '#77798C',
      light_gray: '#DDE0E7',
      hover_gray: '#FBFBFD',
      gray_lighter: '#eeeef5',
      helper_blue: '#DAF1FD',
      dark_blue_gradient_start: '#2C304E',
      dark_blue_gradient_end: '#34405D'
    }
  end

  def spacing
    {
      _1: '15px',
      _2: '30px',
      _3: '45px',
      _4: '60px',
      _5: '75px',
      _6: '90px',
      _8: '120px',
      _12: '180px'
    }
  end

  def default_font
    [
      "font-family: 'Roboto', 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif !important;",
      "-webkit-font-smoothing: antialiased;",
      "-moz-osx-font-smoothing: grayscale;",
      "word-break: break-word;",
      "mso-line-height-rule: exactly;",
      "-ms-text-size-adjust: none;",
      "-webkit-text-size-adjust: none;"
    ].join
  end

  def default_link_attrs(opts={})
    sym_to_s({
      target: '_blank',
      style: {
        color: color[:primary_blue],
        text_decoration: 'none'
      }
    }.deep_merge(opts))
  end

  def text_weight(text, weight, font_size='32px')
    content_tag(:span, text, style: "font-weight: #{weight} !important; font-size:#{font_size};" +
      "Color: #{color[:white]}; text-decoration: none; !important; word-break: break-word !important;" + default_font)
  end

  def table_for(trs, opts={})
    all_trs = trs.map do |tds_a_tr, nested_table_opts|
      # Multiple tds of a tr should be wrapped with another table.
      # This ensures tds will be styled independently without side-effect from another tr's.
      if tds_a_tr.count > 1
        nested_joined_tds = safe_join(tds_a_tr)
        nested_table = table_for([[[nested_joined_tds]]], nested_table_opts || {})
        tds_a_tr = [generic_td(nested_table)]
      end

      content_tag(:tr) { safe_join(tds_a_tr) }
    end

    opts[:style] = 'float: none !important; MArGIN: 0 auto !important; table-layout: fixed; mso-table-lspace:0pt; mso-table-rspace:0pt; border-collapse: collapse;' + opts[:style].to_s
    opts = table_attrs_safe(opts)
    content_tag(:table, opts.reverse_merge(cellspacing: '0', cellpadding: '0', align: 'center')) do
      safe_join all_trs
    end
  end

  def mail_body(args, opts={})
    opts = {
      width: '100%',
      bgcolor: color[:white],
      style: {}
    }.deep_merge(opts)

    args.insert(0, v_spacer(5))
    args.insert(-1, v_spacer(5))

    body_width_padding = table_for([
      [[
        generic_td(' ', width: spacing[:_3], style: "margin: 0 #{spacing[:_3]} 0 0 !important; width: #{spacing[:_3]};"),
        generic_td(table_for(args)),
        generic_td(' ', width: spacing[:_3], style: "margin: 0 0 0 #{spacing[:_3]} !important; width: #{spacing[:_3]};")
      ]]
    ], sym_to_s(opts))
  end

  def generic_td(content='', opts={})
    default = "border-spacing: 0; mso-table-lspace:0pt; mso-table-rspace:0pt; border-collapse: collapse !important;"
    opts[:style] = default + opts[:style].to_s + default_font

    opts = table_attrs_safe(opts)
    content_tag(:td, opts) do
      content
    end
  end

  def title(text, opts={})
    opts = {
      style: {
        color: color[:title_gray],
        font_size: '16px',
        line_height: '25px',
        font_weight: '500 !important',
        text_decoration: 'none',
        text_align: 'center',
        MArgin: '0'
      }
    }.deep_merge(opts)
    opts = sym_to_s(opts)

    h4_tag = content_tag(:h4, text.to_s.html_safe, opts)
    generic_td(h4_tag, opts)
  end

  def paragraph(text, opts={})
    opts = {
      style: {
        color: color[:mid_gray],
        font_size: '16px',
        line_height: '25px',
        font_weight: '400 !important',
        text_decoration: 'none',
        word_break: 'break-word',
        text_align: 'left',
        MArgin: '0'
      }
    }.deep_merge(opts)

    p_opts = { style: opts[:style].except(:width) }
    p_tag = content_tag(:p, text.to_s.html_safe, sym_to_s(p_opts))
    generic_td(p_tag, sym_to_s(opts))
  end

  def icon_link(href, file, opts={}, td_opts={})
    opts = {target: '_blank', style: {display: 'inline-block'}}.deep_merge(opts)
    img_attrs = opts.slice(:width, :height, :alt, :border).reverse_merge(width: 32, height: 32, alt: '', border: '0')

    image_link = link_to(href, sym_to_s(opts)) do
      image_tag(image_url(file), sym_to_s(img_attrs))
    end

    generic_td(image_link, sym_to_s(td_opts.reverse_merge(align: 'center')))
  end

  def icon(img_url, opts={}, width='32', height='32')
    opts = {
      style: {
        min_width: '32px',
        min_height: '32px'
      }
    }.deep_merge(opts)
    opts = sym_to_s(opts)
    td_attrs = opts.extract!(:width, :height)

    image = image_tag(image_url(img_url), opts.reverse_merge(width: width, height: height, alt: '', border: '0'))
    generic_td(image, td_attrs.merge(opts).reverse_merge(align: 'center'))
  end

  def center_underline(width, colour=color[:light_gray], thick='1px')
    [[
      v_spacer(thick),
      generic_td(' ', bgcolor: colour, width: width, height: thick,
        style: "background-color: #{colour}; width: #{width}; min-width: #{width};
          line-height: #{thick}; font-size: #{thick}; height: #{thick};"),
      v_spacer(thick),
    ], { width: '100%', height: thick, style: "min-width: #{width}; max-height: #{thick};" }]
  end

  def v_spacer(spacing_number)
    if spacing_number.is_a?(String)
      height = spacing_number
    else
      spacing_key = "_#{spacing_number}".to_sym
      height = spacing[spacing_key]
    end

    polyfill_height = "height: #{height}; line-height: #{height} !important; font-size: #{height} !important;"
    [[generic_td(' ', height: height, style: polyfill_height)]]
  end

  def mail_container(header_text, header_img, mail_body)
    header_img = header_img.presence || 'mailer/header_photo2.png'

    table_for([
      center_underline('100%', color[:primary_blue], '3px'),
      polyfill_bg_image(header_text, header_img),
      [[generic_td(mail_body)]],
      center_underline('87%', color[:hover_gray], '13px'),
    ], width: '100%', style: 'text-align: center;')
  end

  def review_browse_blog_links
    table_for [
      v_spacer(5),
      [[
        generic_td(link_to('Review Your Shares', dashboard_investments_url, default_link_attrs(style: { font_size: '16px', color: color[:mid_gray] }))),
        generic_td('-', style: "padding: 0 #{spacing[:_1]}; text-align: center !important; color: #{color[:mid_gray]}; font-size: 16px;"),
        generic_td(link_to('Browse Other Startups', browse_url, default_link_attrs(style: { font_size: '16px', color: color[:mid_gray] }))),
        generic_td('-', style: "padding: 0 #{spacing[:_1]}; text-align: center !important; color: #{color[:mid_gray]}; font-size: 16px;"),
        generic_td(link_to('Check Out The Blog', blog_url, default_link_attrs(style: { font_size: '16px', color: color[:mid_gray] }))),
      ]]
    ]
  end

  def text_box(text, opts={})
    opts = {
      bgcolor: color[:helper_blue],
      valign: 'top',
      width: '100%',
      style: {
        font_size: '16px',
        text_align: 'center',
        padding: "#{spacing[:_3]} #{spacing[:_2]}",
        color: color[:primary_blue],
        border: "1px solid #{color[:primary_blue]}",
        border_radius: '3px',
        background_color: color[:helper_blue]
      }
    }.deep_merge(opts)

    generic_td(text, sym_to_s(opts))
  end

  def two_cols_boxes(col1, col2, opts={}, nested_table_opts={})
    opts = {
      hspacer: "#{spacing[:_2]}",
      width: '264',
      style: {
        padding: "#{spacing[:_1]} 0",
        display: 'inline-block',
        max_width: '264px !important',
        min_width: '174px !important',
        width: '100% !important'
      }
    }.deep_merge(opts)
    h_spacer = opts.delete(:hspacer)

    elements = [text_box(col1, opts)]
    elements.insert(1, generic_td('', width: '30', height: '0', style: "width: #{h_spacer} !important; display: inline-block; height: 0;")) if col2 && h_spacer != '0'
    elements.insert(2, text_box(col2, opts)) if col2

    nested_table_opts[:style] = 'width: 100% !important;' + nested_table_opts[:style].to_s
    [elements, nested_table_opts]
  end

  def two_cols_info(data, opts={})
    column_style = {
      hspacer: '0', bgcolor: 'transparent', align: 'left',
        style: { max_width: '277px !important', padding: "#{spacing[:_2]} #{spacing[:_1]} 0 0", text_align: 'left !important', background_color: 'transparent', border: '0' }
    }
    defaults = [
      v_spacer(1),
      center_underline('100%')
    ]

    elements = []
    data.each do |box|
      elements += defaults.dup
        .insert(0, v_spacer(5))
        .insert(1, [[title(box[:section_title], style: { text_align: 'left', text_transform: 'uppercase' })]])

      box[:items].each_slice(2) do |pair_items|
        field1 = normal_box_content(*pair_items[0], {align: 'left', style: { text_align: 'left !important' }})
        item_opts = column_style.deep_dup

        if pair_items[1]
          field2 = normal_box_content(*pair_items[1], {align: 'left', style: { text_align: 'left !important' }})
        else
          item_opts.deep_merge!(width: '100%', style: { min_width: '100%', padding: "#{spacing[:_2]} #{spacing[:_2]} 0 3px"})
        end

        elements << two_cols_boxes(field1, field2, item_opts, align: 'left')
      end
    end

    if opts[:with_title]
      elements.insert(0, [[title(opts[:with_title], style: { text_transform: 'uppercase' })]])
    else
      elements.delete_at(0)
    end
    elements
  end

  def normal_box_content(box_title, box_body, opts={})
    opts = {
      title_color: color[:mid_gray],
      body_color: color[:primary_blue],
      style: {
        display: 'block !important;',
        line_height: '24px;',
        width: '100%',
        text_align: 'center'
      }
    }.deep_merge(opts)

    title_color = opts.delete(:title_color)
    body_color = opts.delete(:body_color)
    title_opts = opts.deep_merge(style: {color: title_color})
    body_opts = opts.deep_merge(style: {color: body_color})

    table_for([
      [[title(box_title, title_opts)]],
      [[paragraph(box_body, body_opts)]]
    ], width: '100%', style: 'min-width:100%', align: opts[:align])
  end

  def icn_txt_button(text, img_url, href, opts={})
    opts = {
      style: {
        font_size: '16px', font_weight: 500,
        text_decoration: 'none', text_align: 'center',
        color: color[:mid_gray], background_color: color[:hover_gray] }
    }.deep_merge(opts)

    table_btn = link_to(href, default_link_attrs({style: { padding: '0', display: 'table-cell' }}.merge(opts))) do
      table_for([
        [[
          icon(img_url, {width: '35', style: {padding: "0", display: 'inline-block', vertical_align: 'middle'}}),
          generic_td(text, sym_to_s(opts.deep_merge(width: '65', style: {display: 'inline-block', vertical_align: 'middle'})))
        ], width: '115']
      ])
    end

    td_opts = { bgcolor: color[:hover_gray], valign: 'center', align: 'center',
      style: {
        border: "1px solid #{color[:light_gray]}", display: 'inline-block;', max_width: '115px !important'
      }
    }.deep_merge(opts)

    generic_td(table_btn, sym_to_s(td_opts))
  end

  def normal_button(text, link, width='auto')
    btn_text = content_tag(:p, text, style: "color: #{color[:white]}; MArGIN: 0; text-align: center;" + default_font)
    table_link = link_to(link, default_link_attrs(style: { font_size: '16px', color: color[:white], display: 'block' })) do
      table_for([
          v_spacer('10px'),
          [[generic_td('', width: '30'), generic_td(btn_text, height: '30px;'), generic_td('', width: '30')]],
          v_spacer('10px')
      ])
    end

    [[
      generic_td(' ', width: '15'),
      generic_td(table_link, width: width, bgcolor: color[:primary_blue], style: "background-color: #{color[:primary_blue]}; border-radius: 3px !important;"),
      generic_td(' ', width: '15')
    ], width: width]
  end

  def header_elements(header_text, opts={})
    elements = [
      [[generic_td(header_text, style: "text-align: center; padding: 0 #{spacing[:_1]};")]],
      v_spacer(2),
      center_underline('60px', color[:primary_blue], '3px')
    ]
    opts.fetch(:order, {}).each do |index, elm_row|
      elements.insert(index.to_s.to_i, elm_row).compact!
    end
    table_for(elements, width: '680')
  end

  private

  def sym_to_s(opts)
    copy_opts = opts.dup
    copy_opts[:style] = copy_opts[:style].map { |k,v| "#{k.to_s.dasherize}:#{v};" }.join if opts[:style]
    copy_opts
  end

  def table_attrs_safe(opts)
    copy_opts = opts.dup
    copy_opts.each do |k,v|
      copy_opts[k] = v.gsub('px', '') if k.in? [:height, :width]
    end
  end

  def polyfill_bg_image(header_text, header_img, opts={})
    header_img_url = image_url(header_img)
    opts = {
      width: '100%',
      height: '280px',
      valign:'middle',
      align: 'center',
      style: {}
    }.deep_merge(opts)

    polyfill_bg = "
      <!--[if gte mso]>
      <v:rect xmlns:v='urn:schemas-microsoft-com:vml' fill='true' stroke='false' fillcolor='#{color[:dark_blue_gradient_start]}' style='mso-width-percent:1000;height:#{opts[:height]};'>
        <v:fill type='tile' src='#{header_img_url}' color2='#{color[:dark_blue_gradient_end]}' opacity='85%' />
        <v:textbox inset='0,0,0,0'>
      <![endif]-->
      <div>
      #{header_text}
      </div>
      <!--[if gte mso]>
        </v:textbox>
      </v:rect>
      <![endif]-->".html_safe

    filter_img_url = image_url(filter_or_original(header_img))
    bg_filter = {
      bgcolor: color[:dark_blue_gradient_end],
      background: filter_img_url,
      style: {
        background: "url(#{filter_img_url})",
        color: color[:white], font_size: '32px'
      }
    }.deep_merge(opts.deep_dup)

    [[
      v_spacer(opts[:height]),
      generic_td(header_text, sym_to_s(bg_filter)),
      v_spacer(opts[:height])
    ], sym_to_s(opts.deep_merge(background: header_img_url, style: {background: "url(#{header_img_url}) no-repeat center", background_size: 'cover'}))]
  end

  def filter_or_original(image)
    internal_images = [
      'mailer/header_photo.png',
      'mailer/header_photo2.png',
      'mailer/header_photo3.png']

    image.in?(internal_images) ? image : image_url('mailer/filter.png')
  end

end
