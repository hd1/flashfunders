module FintechClearingEscrowHelper

  class << self

    def update_investment(investment_id, _params={})
      Serializable.with_serialization("Fintech_#{investment_id}") do
        begin
          investment = Investment.find(investment_id)
          investment_parameters = get_investment_parameters(investment)
          fintech_clearing_investment = create_investment(investment, investment_parameters)
          fintech_clearing_investment = fintech_clearing_investment
          investment.update_attribute(:fund_america_id, fintech_clearing_investment.uuid)
          fintech_clearing_investment
        rescue FintechClearing::ServiceError => e
          TransferInvestmentsMailer.send_email(investment_id, e.to_s).deliver_later
          false
        end
      end
    end

    private

    def create_investment(investment, investment_parameters)
      investment.fund_america_id.blank? ?
        FintechClearing::InvestmentService.new.create(investment_parameters) :
        FintechClearing::InvestmentService.new.update(investment_parameters)
    end

    def get_investment_parameters(investment)
      FintechClearingInvestmentParameterBuilder.build(investment)
    end

  end
end
