module ThirdPartyVerificationHelper

  def accreditation_statement(investor_entity, qualification)
    if investor_entity.is_a?(InvestorEntity::Individual)
      if qualification.is_a?(Accreditor::ThirdPartyIncomeQualification)
        t('third_party_verification.review_statement.statement.personal_income')
      elsif qualification.is_a?(Accreditor::ThirdPartyNetWorthQualification)
        t('third_party_verification.review_statement.statement.personal_net_worth')
      else
        ''
      end
    elsif investor_entity.is_a?(InvestorEntity::Trust)
      t('third_party_verification.review_statement.statement.trust')
    else
      t('third_party_verification.review_statement.statement.other_entity')
    end
  end

  def non_individual_entity?(investor_entity)
    !investor_entity.is_a?(InvestorEntity::Individual)
  end

  def trust_entity?(investor_entity)
    investor_entity.is_a?(InvestorEntity::Trust)
  end
end

