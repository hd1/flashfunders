module InvestmentDashboardHelper
  def cancel_button_tag(investment_presenter)
    link_to(
      I18n.t('investor_investments.invest.cancel'),
      "javascript:FF.confirmCancel('#{path_for_cancel(investment_presenter)}')",
      class: 'btn-normal btn-full',
    )
  end

  def hidden_confirm_dialog
    confirm_form = safe_join [
      form_tag('', method: :put) {},
      content_tag(:div, 'Are you sure?', class: 'centered')
    ]

    content_tag(:div, confirm_form, id: 'cancel-investment-dialog', style: 'display: none;')
  end

  private

  def path_for_cancel(investment_presenter)
    cancel_investment_dashboard_investment_path(
      id: investment_presenter.investment_id
    )
  end
end
