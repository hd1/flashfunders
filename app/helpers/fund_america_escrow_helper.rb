module FundAmericaEscrowHelper

  class << self

    def fund_america_update_investment(investment_id, params={})
      Serializable.with_serialization("FA_#{investment_id}") do
        begin
          investment = Investment.find(investment_id)
          opts = get_fund_america_opts(investment)
          investment_parameters = get_investment_parameters(investment, opts, params)
          fund_america_investment = get_fund_america_investment(investment, investment_parameters, opts)
          investment.update_attribute(:fund_america_id, fund_america_investment.id)
          fund_america_investment
        rescue FundAmerica::ServiceError => e
          TransferInvestmentsMailer.send_email(investment_id, e.to_s).deliver_later
          false
        end
      end
    end

    private

    def get_fund_america_opts(investment)
      {}.tap do |h|
        h[:api] = :spv if investment.is_spv?
        h[:api] = :reg_c if investment.is_reg_c?
      end
    end

    def get_fund_america_investment(investment, investment_parameters, opts)
      investment.fund_america_id.blank? ?
        FundAmerica::InvestmentService.new.create(investment_parameters, opts) :
        FundAmerica::InvestmentService.new.update(investment_parameters, opts)
    end

    def get_fund_america_entity(investment, opts)
      entity_parameters = FundAmericaInvestmentParameterBuilder.entity_params_for_person(investment)
      investment.investor_entity.fund_america_id.blank? ?
        FundAmerica::EntityService.new.create(entity_parameters, opts) :
        FundAmerica::EntityService.new.update(entity_parameters, opts)
    end

    def get_investment_parameters(investment, opts, controller_params)
      investment_parameters = FundAmericaInvestmentParameterBuilder.build(investment.offering, investment)
      if investment.ach? && investment.offering.reg_c_enabled?
        get_fund_america_ach_investment_parameters(controller_params, investment, investment_parameters, opts)
      else
        investment_parameters
      end
    end

    def get_fund_america_ach_investment_parameters(controller_params, investment, investment_parameters, opts)
      fund_america_entity = get_fund_america_entity(investment, opts)
      ach_authorization_params = FundAmericaInvestmentParameterBuilder.ach_authorization_params(investment, fund_america_entity, controller_params)
      fund_america_ach_authorization = FundAmerica::AchAuthorizationService.new.create(ach_authorization_params, opts)

      investment.investor_entity.update_attributes(fund_america_id: fund_america_entity.id,
                                                   fund_america_ach_authorization_id: fund_america_ach_authorization.id)

      investment_parameters.delete(:entity)
      investment_parameters[:entity_id] = fund_america_entity.id
      investment_parameters[:ach_authorization_id] = fund_america_ach_authorization.id
      investment_parameters
    end

  end
end
