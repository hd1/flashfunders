module VerifiedHelper
  def already_accredited_promise(investment, entity)
    if investment.by_individual?
      t('investor_accreditation.already_accredited.below_the_fold.promise_individual')
    else
      t('investor_accreditation.already_accredited.below_the_fold.promise', entity_type: entity.display_name).html_safe
    end
  end

  def humanize_qualification_name(qualification)
    case qualification
      when Accreditor::ThirdPartyNetWorthQualification
        'Net Worth'
      when Accreditor::ThirdPartyIncomeQualification
        'Income'
      when Accreditor::ThirdPartyEntityQualification
        '3rd Party Verification'
      when Accreditor::IndividualIncomeQualification
        'Individual Income'
      when Accreditor::IndividualNetWorthQualification
        'Individual Net Worth'
      when Accreditor::JointIncomeQualification
        'Joint Income'
      when Accreditor::JointNetWorthQualification
        'Joint Net Worth'
      when Accreditor::OfflineEntityQualification
        'Offline Verification'
      when Accreditor::InternationalQualification
        'Self Certified (Regulation S)'
    end
  end
end