module InvestHelper
  def signatory_name_and_address(pfii)
    str = ""
    str += pfii.full_name
    str += "<br/>"
    str += pfii.address1
    str += ', ' + pfii.address2 if pfii.address2.present?
    str += "<br/>"
    str += pfii.city
    str += ", "
    str += pfii.state_name
    str += " "
    str += pfii.zip_code
    str += "<br/>"
    str += pfii.country_name
    if pfii.daytime_phone.present? && pfii.mobile_phone.present?
      str += "<br/>"
      str += pfii.daytime_phone
    elsif pfii.daytime_phone.present?
      str += "<br/>"
      str += pfii.daytime_phone
    elsif pfii.mobile_phone.present?
      str += "<br/>"
      str += pfii.mobile_phone
    end
  end

  def investor_entity_name_and_address(investor_entity)
    str = ""
    str += investor_entity.name
    str += "<br/>"
    str += investor_entity.address_1
    str += ', ' + investor_entity.address_2 if investor_entity.address_2.present?
    str += "<br/>"
    str += investor_entity.city
    str += ", "
    str += investor_entity.state_name
    str += " "
    str += investor_entity.zip_code
    str += "<br/>"
    str += investor_entity.country_name
    str += "<br/>" if investor_entity.phone_number.present?
    str += investor_entity.phone_number
  end

  def investor_entity_formation_state_and_country(investor_entity)
    "#{investor_entity.formation_state_name}<br/>#{State.country(investor_entity.formation_country).name}"
  end

  def document_class_for(document)
    document.document_new.path.end_with?(".pdf") ? "pdf" : "doc"
  end

  private

  def bank_account_types
    Bank::Account::TYPES.map do |t|
      ["#{t.titleize} Account", t]
    end
  end

end

