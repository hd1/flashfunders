module HeaderHelper
  def header_menus
    @header_menus ||= generate_header_menus
  end

  private

  def generate_header_menus
    menus = case
              when !current_user
                header_menu_data.values_at(:browse, :raise, :investors, :faq, :blog)
              when get_offering_context > 0
                header_menu_data.values_at(:dashboard, :offering_editor, :browse, :my_investments, :blog, :messages)
              when current_user.user_type_entrepreneur?
                header_menu_data.values_at(:browse, :raise, :my_investments, :blog, :messages)
              else
                header_menu_data.values_at(:browse, :my_investments, :blog, :messages)
            end

    safe_join menus.map { |text, path, data| nav_link text, path, class: "main-header header-#{text.parameterize}", data: data, target: link_target }
  end

  def static_menu_items
    @@static_menu_items =
        {
          browse: [t('header.browse'), browse_path],
          raise: [t('header.raise'), startups_path],
          investors: [t('header.investors'), investors_path],
          faq: [t('header.faq'), faq_investor_path],
          my_investments: [t('nav.my_investments'), dashboard_investments_path],
          blog: [t('header.blog'), blog_url]
        }
  end

  def header_menu_data
    @menus ||= static_menu_items.tap do |hash|
      offering_id = get_offering_context
      if offering_id > 0
        hash[:dashboard] = [t('header.dashboard'), issuer_dashboard_path(id: offering_id)]
        hash[:offering_editor] = [t('header.offering_editor'), edit_issuer_editor_detail_path(id: offering_id)]
      end
      if current_user.present?
        hash[:messages] = [t('nav.messages'), dashboard_conversations_path, { count: OfferingMessage.total_unread_for_user(current_user, offering_id) }]
      end
    end
  end

end
