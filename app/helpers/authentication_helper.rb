module AuthenticationHelper
  def social_auth_button(provider, icon, text, opts={})
    opts = opts.reverse_merge(class: "btn-#{provider.to_s}")

    link_to(user_omniauth_authorize_path(provider), opts) do
      safe_join [
        image_tag(icon),
        content_tag(:span, text)
      ]
    end
  end

  def separator(text)
    content_tag(:div, class: 'separator') do
      safe_join [
        content_tag(:hr),
        content_tag(:span, text),
        content_tag(:hr)
      ]
    end
  end

end
