module InvestmentProfilesHelper
  def format_date(date, opts={})
    if date.is_a?(Date)
      I18n.localize(date, { :format => '%m/%d/%Y' }.merge(opts))
    else
      date
    end
  end
end
