module PitchHelper
  def decode_hidden_photo_key(key)
    URI.decode(key) if key.present?
  end

  def add_content_button_tag(name, f, association, options = {})
    options[:class] ||= ''
    options[:class] << ' add-template'

    build_content_button_tag(name, f, association, options)
  end

  def add_image_button_tag(name, f, association, options = {})
    options[:class] ||= ''
    options[:class] << ' add-image'

    safe_join [
      build_content_button_tag(name, f, association, options),
      file_field_tag('file-chooser', class: 'hidden', accept: 'image/png,image/gif,image/jpeg')
    ]
  end

  def add_carousel_image_button_tag(name, f, association, options = {})
    new_object = f.object.send(association).klass.new
    id = new_object.object_id

    template = f.simple_fields_for(association, new_object, child_index: id) do |builder|
      render("template_" + association.to_s.singularize, f: builder)
    end

    options[:class] ||= ''
    options[:class] << ' add-image'

    safe_join [
      template_button_tag(new_image_label(name), id, template, options),
      file_field_tag('file-chooser', class: 'hidden', accept: 'image/png,image/gif,image/jpeg')
    ]
  end

  def new_image_label(name)
    safe_join([
      content_tag('span', '+', class: 'plus'),
      tag('br'),
      name,
      tag('br'),
      content_tag('span', I18n.t('issuer.editor.pitch.add_image.info'), class: 'carousel'),
    ])
  end

  private

  def template_button_tag(name, id, template, options)
    options[:type] = 'button'

    options[:data] ||= {}
    options[:data][:id] = id
    options[:data][:target] = '.content-creation'
    options[:data][:template] = template.gsub("\n", "")

    button_tag(options) do
      name.html_safe
    end
  end

  def build_content_button_tag(name, f, association, options)
    new_object = f.object.content_items.klass.new
    new_object.content = f.object.send(association).build

    id = new_object.object_id

    template = f.simple_fields_for(:content_items, new_object, child_index: id) do |builder|
      render("template_" + association.to_s.singularize, f: builder)
    end

    name += secondary_button_text(association)
    template_button_tag(name, id, template, options)
  end

  def secondary_button_text(association)
    case association
    when :images
      safe_join [
        tag('br'),
        content_tag('span', I18n.t('issuer.editor.pitch.add_image.info'), class: 'carousel')
      ]
    else
      ''
    end
  end
end
