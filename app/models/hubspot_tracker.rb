class HubspotTracker

  REDIS_NAMESPACE = 'hubspot'

  HUBSPOT_DEAL_PROPERTIES_MAP = {
    # Company Info
    company_name: 'company_name',
    website: 'company_website',
    entity_type: 'entity_type',
    phone_number: 'phone_number',
    # Campaign Info
    amount_needed: 'money_needed_',
    company_criteria: 'company_criteria',
    new_money_committed_range: 'amount_of_new_money_lined_up',
    num_users: 'num_users',
    promotion_budget: 'promotion_budget',
    lead_source: 'how_did_you_hear_about_flashfunders_',
    preferred_partner: 'preferred_partner'
    # Currently deprecated but available on Hubspot to be revived
    # round_status: 'new_round_or_existing_round',
    # active_conversations: 'new_money_lined_up_yes_or_no_',
    # investor_type: 'type_of_investor_angel_institutional_family_friend_other_',
    # use506c:                   'general_solicitation',
    # usecf: 'crowd_funding',
    # accreditation_proof: 'new_investors_can_prove_accreditation',
    # state_id: 'state_of_formation',
    # year_founded: 'year_founded',
  }

  class << self

    def send_contact_info(klass, id, context=nil)
      unless id.blank?
        info = tracking_info(klass.find(id))
        send_info(info, context)
      end
    end

    def send_info(info, context=nil)
      email = info.delete(:email)
      if should_send? email, info
        contact = create_or_update(email, info.slice(*(info.keys - [:_date_generated])), context)
        update_cache(email, info) unless contact.blank?
        contact
      end
    end

    def tracking_info(resource)
      case resource.class.to_s
        when 'Subscription'
          subscription_properties(resource)
        when 'User'
          user_properties(resource)
        else
          {}
      end
    end

    def batch_update_user_info(limit=0, verbose=false)
      batch_query(User.all, :send_tracking_info, limit, verbose)
    end

    def batch_update_subscriber_info(limit=0, verbose=false)
      query = Subscription.where.not(email: User.select(:email))
      batch_query(query, :send_tracking_info, limit, verbose)
    end

    def batch_update_issuer_info(limit=0, verbose=false)
      query = User.where(user_type: User.user_types[:user_type_entrepreneur])
      batch_query(query, :send_tracking_info, limit, verbose)
    end

    def batch_create_deals(limit=0, verbose=false)
      query = IssuerApplication.where.not('properties is null').where.not("properties ? 'hubspot_deal_id'")
      batch_query(query, :send_deal_info, limit, verbose)
    end

    def clear_cache
      keys = $redis.keys(redis_key('*'))
      $redis.del(keys) unless keys.blank?
    end

    def create_deal(issuer_application_id)
      issuer_application = IssuerApplication.find(issuer_application_id)
      if issuer_application.hubspot_deal_id
        # return if we've already told Hubspot about this deal
        return Hubspot::Deal.find(issuer_application.hubspot_deal_id)
      end

      send_deal_info(issuer_application)
    end

    def deal_info(issuer_application)
      deal_properties(issuer_application)
    end

    private

    RETRY_ERRORS = [/CONTACT_EXISTS/]
    LOG_ONLY_ERRORS = [/Email address (.*) is invalid/, /No form found with guid/]

    def batch_query(query, method, limit, verbose)
      query = query.limit(limit) if limit > 0
      batch_update_info(query, method, verbose)
    end

    def send_deal_info(issuer_application)
      begin
        deal = Hubspot::Deal.create!(ENV['HUBSPOT_KEY'], [], get_vids(issuer_application), deal_properties(issuer_application))
        issuer_application.hubspot_deal_id = deal.deal_id
        issuer_application.save
      rescue Hubspot::RequestError => e
        if has_errors?(e.message, LOG_ONLY_ERRORS)
          log_error e, false
        else
          raise e
        end
      end
      deal
    end

    def submit_form(email, info, context)
      guid = ENV['HUBSPOT_CONTACT_FORM']
      begin
        form = Hubspot::Form.find(guid)
        form.submit(info.merge({ 'email' => email, 'hs_context' => context }))
      rescue Hubspot::RequestError => e
        raise e unless has_errors?(e.message, LOG_ONLY_ERRORS)
        false
      end
    end

    def create_or_update(email, info, context=nil, retries=0)
      begin
        contact = Hubspot::Contact.find_by_email(email)
        result = contact.blank? ? create_contact(email, info, context) : update_contact(contact, info)
      rescue Hubspot::RequestError => e
        if has_errors?(e.message, RETRY_ERRORS)
          return retries > 0 ? nil : create_or_update(email, info, context, retries+1)
        end
        raise e unless has_errors?(e.message, LOG_ONLY_ERRORS)
        log_error e, false
      end
      result
    end

    def create_contact(email, info, context=nil)
      if context.present? && submit_form(email, info, context)
        Hubspot::Contact.find_by_email(email)
      else
        Hubspot::Contact.create!(email, info)
      end
    end

    def update_contact(contact, info)
      contact.update!(info)
      contact
    end

    def has_errors?(message, error_list)
      error_list.any? { |msg| message =~ msg }
    end

    def send_tracking_info(resource)
      send_info(tracking_info(resource))
    end

    def batch_update_info(resources, method, verbose=false)
      counter = 0
      errors = []
      resources.each do |resource|
        begin
          if self.send(method, resource)
            print '.' if verbose
            counter += 1
          end
        rescue => e
          errors << e
        end
      end
      [counter, errors]
    end

    def should_send?(email, data)
      email.present? && data != query_cache(email)
    end

    def query_cache(email)
      begin
        info = $redis.get(redis_key(email))
        return eval(info).symbolize_keys unless info.blank?
      rescue => e
        log_error e
      end
      {}
    end

    def update_cache(email, data)
      begin
        $redis.set(redis_key(email), data)
      rescue => e
        log_error e
      end
    end

    def log_error(e, page=true)
      # Redis failed for some reason, report the error and carry on
      Rails.logger.error(e.message)
      Honeybadger.notify e if page
    end

    def redis_key(email)
      "#{REDIS_NAMESPACE}/#{email}"
    end

    def get_vids(issuer_application)
      # ensure Hubspot knows about the user
      contact = send_tracking_info(issuer_application.user) || Hubspot::Contact.find_by_email(issuer_application.user.email)
      contact.blank? ? [] : [contact.vid]
    end

    def subscription_properties(subscription)
      email = subscription.email
      user = User.where(email: email).first
      if user.nil?
        { email: email,
          lifecyclestage: 'subscriber',
          first_subscribed: subscription.created_at.to_s
        }
      else
        user_properties(user)
      end
    end

    def user_properties(user)
      user_shared_properties(user).merge(investor_properties(user).merge(issuer_properties(user)))
    end

    def user_shared_properties(user)
      base = { email: user.email,
               lifecyclestage: get_life_cycle_stage(user),
               firstname: user.first_name,
               lastname: user.last_name,
               user_type: user.user_type || 'user_type_unknown'
      }
      base.merge(pfii_properties(user).merge(subscriber_properties(user)))
    end

    def get_life_cycle_stage(user)
      if user.confirmed?
        'customer'
      elsif user.has_subscriptions?
        'subscriber'
      else
        'marketingqualifiedlead'
      end
    end

    def pfii_properties(user)
      pfii = user.personal_federal_identification_information
      {}.tap do |hash|
        hash[:city] = pfii.city if pfii.try(:city)
        hash[:state] = pfii.state_id if pfii.try(:state_id)
        hash[:country] = pfii.country if pfii.try(:country)
      end
    end

    def subscriber_properties(user)
      { _date_generated: Date.today.to_s }.tap do |h|
        if user.has_subscriptions?
          h[:first_subscribed] = user.first_subscribed.to_s
        end
        if user.is_following_offerings?
          h[:followed_offerings] = "*#{user.followed_offerings.map { |o| o.offering_id.to_s }.join('*')}*"
        end
      end
    end

    # Investors

    def investor_properties(user)
      investments = user.investments.order(updated_at: :desc)
      base = {
        started_investment: investment_property(investments, :started?),
        signed_docs: investment_property(investments, :signed_docs?),
        funded: investment_property(investments, :funded?)
      }
      base.merge!(amount_invested(investments))
      base.merge!(reg_c_properties(user))
      latest_investment_passed_profile = investments.detect { |i| passed_profile?(i) }
      if latest_investment_passed_profile
        base.merge!(investor_entity_properties(latest_investment_passed_profile))
      end
      base
    end

    def investor_entity_properties(investment)
      entity = investment.investor_entity
      return {} unless entity
      pfii = entity.personal_federal_identification_information
      {}.tap do |hash|
        hash[:city] = pfii.city if pfii.try(:city)
        hash[:state] = pfii.state_id if pfii.try(:state_id)
        hash[:phone] = pfii.phone_number if pfii.try(:phone_number)
        hash[:age] = pfii.age if pfii.try(:age)
      end
    end

    def passed_profile?(investment)
      !%w(started intended).include? (investment.state)
    end

    def amount_invested(investments)
      amounts = investments.select { |i| funded?(i) }.map { |i| i.amount }
      { number_of_investments: amounts.count, amount_invested: amounts.sum.to_i }
    end

    def investment_property(investments, filter)
      results = investments.select { |i| self.send(filter, i) }
      results.map { |i| investment_tag(i) }.join(', ')
    end

    def investment_tag(i)
      "#{i.offering.company.name} [Inv# #{i.id}]"
    end

    def started?(investment)
      investment.resumable? && !investment.signed_documents?
    end

    def signed_docs?(investment)
      investment.resumable? && investment.signed_documents?
    end

    def funded?(investment)
      investment.escrowed?
    end

    def reg_c_properties(user)
      # Testing calc_data shouldn't be necessary in production but review has some funky data
      investment = user.investments.where.not(calc_data: nil).order(:created_at).detect { |i| i.is_reg_c? }
      return {} if investment.blank?
      {
        reg_c_income: investment.reg_c_income,
        reg_c_external: investment.reg_c_external,
        reg_c_net_worth: investment.reg_c_net_worth,
        reg_c_annual_limit: eval(investment.reg_c_calc_output)['limit']
      }
    end

    # Issuers / Startups

    def issuer_properties(user)
      offerings = Offering.where(user_id: user.id)

      { company: issuer_companies(offerings)
      }
    end

    def issuer_companies(offerings)
      offerings.map { |o| offering_tag(o) }.join(', ')
    end

    def offering_tag(o)
      "#{o.company.name} (#{o.id})"
    end

    def deal_properties(issuer_application)
      { 'dealname' => issuer_application.company_name,
        'dealstage' => 'appointmentscheduled',
        'hubspot_owner_id' => deal_owner
      }.merge(mapped_deal_properties(issuer_application))
    end

    def mapped_deal_properties(issuer_application)
      {}.tap do |hash|
        HUBSPOT_DEAL_PROPERTIES_MAP.map do |k, v|
          val = deal_property_value_translate(k, issuer_application.send(k))
          hash[v] = val unless val.blank?
        end
      end
    end

    def deal_owner
      contact = Hubspot::Owner.find_by_email(Flashfunders::Application.config.deal_owner)
      contact.owner_id if contact
    end

    def deal_property_value_translate(key, val)
      case key
        when :company_criteria
          get_multiple_vals(val)
        else
          val
      end
    end

    def get_multiple_vals(vals)
      return nil if vals.blank?
      vals = eval(vals) if vals.is_a?(String)
      vals.delete('')
      vals.join(';')
    end

  end
end
