class BankAccountFormValidator < BaseValidator

  ensure_valid :attributes, presence: true
  ensure_valid :bank_account_number,
               presence: true,
               length: { maximum: 17, minimum: 4, too_long: 'Maximum of 17 characters', too_short: 'Minimum of 4 characters', allow_blank: true },
               format: { with: /\A\d*\z/, message: 'Invalid bank account number' }
  ensure_valid :bank_account_holder,
               presence: true,
               length: { maximum: 22, message: 'Maximum of 22 characters' }
  ensure_valid :bank_account_routing,
               presence: true,
               format: { with: /\A[a-zA-Z0-9]*\z/, message: 'Only letters and numbers allowed' }
  ensure_valid :bank_account_type,
               presence: true,
               inclusion: { in: Bank::Account::TYPES,
                            message: 'Invalid bank account type' }
  ensure_valid :funding_international, presence: false
  ensure_valid :funding_type, presence: true, if: :is_funding_international?

  private

  def is_funding_international?
    funding_international == 'true'
  end
end
