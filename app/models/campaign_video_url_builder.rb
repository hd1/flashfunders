class CampaignVideoURLBuilder
  VIMEO_HOST = 'vimeo.com'
  VIMEO_EMBEDDED_URL = '//player.vimeo.com/video/'
  VIMEO_MOOGALOOP_URL = '//vimeo.com/moogaloop.swf?clip_id='

  def convert_vimeo_page_to_embedded_url(video_url)
    return nil unless video_url.present?

    build_video_embed_source(video_url)
  end

  def convert_vimeo_page_to_swf_url(video_url)
    return nil unless video_url.present?

    build_video_swf_source(video_url)
  end

  private

  def build_video_embed_source(video_url)
    uri = URI.parse(video_url)

    if uri.host == VIMEO_HOST
      VIMEO_EMBEDDED_URL + parse_vimeo_video_id(uri)
    else
      nil
    end
  end

  def build_video_swf_source(video_url)
    uri = URI.parse(video_url)

    if uri.host == VIMEO_HOST
      VIMEO_MOOGALOOP_URL + parse_vimeo_video_id(uri)
    else
      nil
    end
  end

  def parse_vimeo_video_id(video_uri)
    video_uri.path[1..-1]
  end
end
