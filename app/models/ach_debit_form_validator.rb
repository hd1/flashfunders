class AchDebitFormValidator < BankAccountFormValidator

  attr_reader :authorized_transfer
  private :authorized_transfer
  validates :authorized_transfer, acceptance: true

end