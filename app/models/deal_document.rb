class DealDocument < Document
  include Rails.application.routes.url_helpers
  mount_uploader :document, DealDocumentUploader

  validates :name, presence: true
  validates :document_new, is_uploaded: true

  def public_path
    return unless document_url && documentable_type == "Offering"
    aws_link_offering_campaign_documents_path(documentable_id, uuid)
  end
end

