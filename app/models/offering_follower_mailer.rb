class OfferingFollowerMailer < ApplicationMailer
  layout 'layouts/snazzy_mailer'
  helper [:application, :email, :offering]

  def welcome_follower(follower_id)
    @follower = OfferingFollower.find_by(id: follower_id)
    @offering = @follower.offering
    @offering_presenter = OfferingPresenter.new(@offering)

    mail \
      to: @follower.user.email,
      from: I18n.t('contacts.no_reply'),
      subject: I18n.t('offering_follow_mailer.welcome_follower.subject', company_name: @offering.company.name)
  end

  def notify_issuer_reply(offering_id, reply_id)
    @offering  = Offering.find_by(id: offering_id)
    @reply = OfferingComment.find_by(id: reply_id)
    @comment = @reply.parent
    @followers = OfferingFollower.where(offering_id: @offering.id).where.not(user_id: [@comment.user.id, @offering.issuer_user.id])
    @offering_presenter = OfferingPresenter.new(@offering)

    mail \
      to: [],
      bcc: @followers.map {|follower| follower.user.email},
      from: I18n.t('contacts.no_reply'),
      subject: I18n.t('offering_follow_mailer.notify_issuer_reply.subject', company_name: @offering.company.name)
  end

end
