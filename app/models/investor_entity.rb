class InvestorEntity < ActiveRecord::Base
  has_one :personal_federal_identification_information, dependent: :destroy
  has_many :investments
  has_many :accreditor_spans, class_name: 'Accreditor::Span', dependent: :destroy
  has_many :accreditor_qualifications, class_name: 'Accreditor::Qualification', dependent: :destroy
  delegate :full_name, to: :personal_federal_identification_information, allow_nil: true

  store_accessor :profile_data,
    :high_risk_agree, :experience_agree, :loss_risk_agree, :time_horizon_agree,
    :cancel_agree, :resell_agree, :idology_id

  enum identification_status_id: {
           pending: 1,
           in_progress: 2,
           verified: 3,
           rejected: 4
       }

  def is_international?
    lives_in_usa = personal_federal_identification_information.try(:country) == 'US'
    us_citizen = personal_federal_identification_information.try(:us_citizen)
    located_in_usa = country == 'US'
    ! (lives_in_usa || located_in_usa || us_citizen)
  end

  def is_domestic_individual_entity?
    kind_of?(Individual) && !is_international?
  end

  def state_name
    get_state_name(country, state_id)
  end

  def country_name
    get_country_name(country)
  end

  def formation_state_name
    get_state_name(formation_country, formation_state_id)
  end

  def formation_country_name
    get_country_name(formation_country)
  end


  def accreditation
    @accreditation ||= EntityAccreditation.new(self)
  end

  def executive_name
    full_name
  end

  def current_accreditor_span(offering_end_date)
    accreditor_spans.detect do |span|
      (span.start_date..span.end_date).include? [Date.current, offering_end_date].min
    end
  end

  def update_cip_status(data)
    data ? verified! : pending!
  end

  private

  def get_state_name(a_country, a_state)
    country = State.country(a_country)
    return "" if country.nil?
    if country.subregions.count > 0
      country.subregions.coded(a_state).name || a_state
    else
      ""
    end
  end

  def get_country_name(a_country)
    carmen_country = State.country(a_country)
    carmen_country.name if carmen_country
  end

  class Individual < InvestorEntity;
    def display_name
      "individual"
    end
  end

  class Signatory < Individual
    def display_name
      "signatory"
    end
  end

  class Entity < InvestorEntity
    def display_name
      'generic'
    end
  end

  class Trust < InvestorEntity
    def display_name
      "trust"
    end
  end

  class LLC < InvestorEntity
    def display_name
      "LLC"
    end
  end

  class Partnership < InvestorEntity
    def display_name
      "partnership"
    end
  end

  class Corporation < InvestorEntity
    def display_name
      "corporation"
    end
  end
end
