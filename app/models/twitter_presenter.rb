class TwitterPresenter
  # https://support.twitter.com/articles/78124-how-to-post-shortened-links-urls
  SHORT_URL_LENGTH   = 22
  MAX_MESSAGE_LENGTH = 140
  ELLIPSIS = '...'

  attr_reader :via, :url, :company_name, :campaign_tagline
  private :via, :url, :company_name, :campaign_tagline

  def initialize(url, via, company_name, campaign_tagline)
    @url              = url
    @via              = via
    @company_name     = company_name
    @campaign_tagline = campaign_tagline
  end

  def offering_url
    build_url truncate(:offering_text_template, offering_text)
  end

  def completed_investment_url
    build_url truncate(:completed_investment_text_template, completed_investment_text)
  end


  private

  def build_url(text)
    uri       = URI('https://twitter.com/share')
    uri.query = URI.encode_www_form([['via', via], ['url', url], ['text', text]])
    uri.to_s
  end

  def truncate(template, text)
    total_size = instantiate_template(template).size
    return text unless total_size > MAX_MESSAGE_LENGTH

    character_overage    = total_size + ELLIPSIS.size - MAX_MESSAGE_LENGTH
    truncate_to_position = text.size - character_overage - 1
    text[0..truncate_to_position] + ELLIPSIS
  end

  def instantiate_template(template)
    send(template, 'x' * SHORT_URL_LENGTH, via, company_name, campaign_tagline)
  end

  def offering_text
    I18n.t('social.twitter.offering.short', company: company_name, tagline: campaign_tagline)
  end

  def offering_text_template(url = nil, via = nil, company_name = nil, campaign_tagline = nil)
    I18n.t('social.twitter.offering.long', company: company_name, tagline: campaign_tagline, url: url, via: via)
  end

  def completed_investment_text
    I18n.t('social.twitter.invest_prefix') + offering_text
  end

  def completed_investment_text_template(url = nil, via = nil, company_name = nil, campaign_tagline = nil)
    I18n.t('social.twitter.invest_prefix') + offering_text_template(url, via, company_name, campaign_tagline)
  end

end
