class RegCCalculator < JavascriptCalculator

  def initialize(income, net_worth, reg_c_non_ff_total, reg_c_ff_total, reg_c_cutoff, share_price)
    @args = { income_amount: income,
              net_worth: net_worth,
              prev_reg_c_ext_total: reg_c_non_ff_total,
              prev_reg_c_total: reg_c_ff_total,
              regCCutoff: reg_c_cutoff-1,
              sharePrice: share_price }
    call_js_calculator('RegCCalculator')
  end

  def limit
    @calc[:limit].to_d.round(2)
  end

  def available
    @calc[:available].to_d.round(2)
  end

  def over_limit
    @calc[:over_limit].to_d.round(2)
  end

end
