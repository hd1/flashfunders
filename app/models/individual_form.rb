class IndividualForm < GenericEntityForm

  attr_reader :user, :federal_identification_information_creator, :validator, :entity, :pfii

  EMPLOYMENT_STATES = ['Employed', 'Self-Employed', 'Not Employed', 'Retired', 'Student', 'Other']

  DEFAULTS = { signatory_country: State.default_country }

  FORM_ATTRS = CORE_ATTRS + SIGNATORY_ATTRS

  form_reader *FORM_ATTRS

  private :user, :federal_identification_information_creator, :validator

  delegate :errors, to: :validator

  def initialize(attrs, user, options={})
    options.merge!({ defaults: DEFAULTS })

    @entity = user.investor_entities.order(:created_at).select { |i| i.is_a?(InvestorEntity::Individual) }.last
    options.merge!({ entity: @entity })
    super

    @attributes.each do |attr, val|
      instance_variable_set("@#{attr}", val)
    end

  end

  def entity_id
    @entity.try(:id)
  end

  def form_type
    :individual
  end

  def signatory_date_of_birth
    super
  end

  def signatory_id_number
    super
  end

  def signatory_ssn
    super
  end

  private

  def entity_attrs
    CORE_ATTRS
  end

  def to_pfii_params
    hash = {}
    attributes.each do |k, v|
      hash[self.class.remove_signatory(k)] = v
    end

    hash
  end

  def pfii_params
    to_pfii_params.slice(*PersonalFederalIdentificationInformation::EDIT_ATTRIBUTES)
  end
end
