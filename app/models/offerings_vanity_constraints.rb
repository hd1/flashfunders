class OfferingsVanityConstraints
  def matches?(request)
    #strip leading slash and downcase
    normalized_path = request.path[1..-1].downcase
    Offering.exists?(vanity_path: normalized_path)
  end
end
