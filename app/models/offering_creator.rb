class OfferingCreator

  def create(attributes)
    ActiveRecord::Base.transaction do
      offering = create_offering_for_user(attributes)
      company = create_company_for_offering(offering.id)
      create_corporate_federal_identification_information_for_company(company.id)
      create_security_for_offering(offering)
      offering.reload
    end
  end

  private

  def create_offering_for_user(hash)
    Offering.create!(hash.merge({visibility: Offering.visibilities[:visibility_restricted], security_type_id: :security_fsp}))
  end

  def create_company_for_offering(offering_id)
    Company.create!(offering_id: offering_id)
  end

  def create_corporate_federal_identification_information_for_company(company_id)
    CorporateFederalIdentificationInformation.new(company_id: company_id).tap do |corporate_identification_information|
      corporate_identification_information.save!(validate: false)
    end
  end

  def create_security_for_offering(offering)
    security = FactoryGirl.create(:security, offering: offering, escrow_provider: FactoryGirl.create(:escrow_provider_fundamerica))
    create_funding_details_for_security(security)
  end

  def create_funding_details_for_security(security)
    security.offering_funding_detail_domestic = FactoryGirl.create(:offering_funding_detail_direct)
    security.offering_funding_detail_international = FactoryGirl.create(:offering_funding_detail_direct_international)
    security.save
  end
end
