class IssuerApplicationForm
  extend ActiveModel::Naming
  include ActiveModel::Conversion

  attr_reader :user, :validator, :attributes
  private :validator

  delegate :errors, to: :validator

  ISSUER_ATTRS = [:company_name, :website, :phone_number, :entity_type,
                  :amount_needed, :company_criteria, :new_money_committed_range,
                  :num_users, :promotion_budget, :lead_source, :preferred_partner]

  USER_ATTRS = [:registration_name, :email]

  attr_reader *ISSUER_ATTRS
  attr_reader *USER_ATTRS

  def initialize(attrs={}, user=nil, options={})
    @attributes = attrs.to_hash.with_indifferent_access
    @user = user
    @validator = options[:validator] || IssuerApplicationFormValidator.new(all_attrs)

    all_attrs.each do |attr, val|
      instance_variable_set("@#{attr}", val)
    end
    if @user
      @registration_name = @user.registration_name
      @email = @user.email
    end
  end

  def save
    return false unless (validator.valid? && find_or_create_user)

    IssuerApplication.new(issuer_attrs.merge(user_id: @user.id)).save
  end

  private

  def find_or_create_user
    @user ||= User.find_for_authentication(email: email) || create_temporary_user(registration_name, email)
  end

  def create_temporary_user(registration_name, email)
    user = User.create_temporary_user(registration_name, email, :user_type_entrepreneur)
    if user.errors.messages.present?
      validator.errors.messages.merge!(user.errors.messages)
      return nil
    end
    user
  end

  def issuer_attrs
    @issuer_attrs ||= attributes.slice(*ISSUER_ATTRS)
  end

  def user_attrs
    @user_attrs ||= attributes.slice(*USER_ATTRS)
  end

  def all_attrs
    issuer_attrs.merge(user_attrs)
  end
end
