class IndividualFormValidator < GenericEntityFormValidator

  ensure_valid :attributes, presence: true
  ensure_valid :signatory_first_name, person_name: true, presence: true, length: { maximum: 45, message: 'Maximum of 45 characters' }
  ensure_valid :signatory_last_name, person_name: true, presence: true, length: { maximum: 45, message: 'Maximum of 45 characters' }
  ensure_valid :signatory_middle_initial, format: { with: /\A[a-zA-Z?]?\Z/, message: 'Only letters allowed', allow_nil: true }, length: { maximum: 1, message: 'Maximum of 1 character' }
  ensure_valid :signatory_address1, presence: true, length: { maximum: 128, message: 'Maximum of 128 characters' }
  ensure_valid :signatory_address2, length: { maximum: 128, message: 'Maximum of 128 characters' }
  ensure_valid :signatory_country, presence: true, inclusion: State.all_country_codes
  ensure_valid :signatory_city, city: true, presence: true, length: { maximum: 45, message: 'Maximum of 45 characters' }
  ensure_valid :signatory_state_id, presence: true, if: :is_usa?
  ensure_valid :signatory_zip_code, presence: true, zip_code_format: true, if: :is_usa?
  ensure_valid :signatory_ssn, presence: true, ssn_format: true, if: :is_usa?
  ensure_valid :signatory_date_of_birth, presence: true, date_format: true, must_be_eighteen_years_old: true
  ensure_valid :signatory_daytime_phone, us_phone_number_format: true, if: :is_usa?
  ensure_valid :signatory_mobile_phone, us_phone_number_format: true, if: :is_usa?

  validate :at_least_one_phone
  validate :has_valid_state

  ensure_valid :signatory_id_type, presence: true, inclusion: { in: :valid_id_types }
  ensure_valid :signatory_id_number, presence: true
  ensure_valid :signatory_location, presence: true
  ensure_valid :signatory_issued_date, presence: true, date_format: true
  ensure_valid :signatory_expiration_date, presence: true, date_format: true
  ensure_valid :signatory_us_citizen, presence: true

  private

  def at_least_one_phone
    if signatory_daytime_phone.blank? && signatory_mobile_phone.blank?
      errors[:signatory_daytime_phone] = 'Please provide a phone number.'
    end
  end

  def has_valid_state
    errors[:signatory_state_id] = 'Required' if !validate_subregion(@signatory_country, @signatory_state_id)
  end

  def validate_subregion(country, region)
    return true if country == ''
    subregions = State.all_codes(country)
    subregions.size() == 0 || subregions.include?(region)
  end

  def region_codes
    State.all_codes(@signatory_country)
  end

  def valid_id_types
    PersonalFederalIdentificationInformation.id_types.keys + PersonalFederalIdentificationInformation.id_types.values
  end

  def is_usa?
    State.is_usa? @signatory_country, @signatory_state_id
  end

end
