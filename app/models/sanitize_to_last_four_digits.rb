class SanitizeToLastFourDigits
  CAPTURE_LAST_FOUR = %r{(....)$}
  ALPHA_NUMERIC = %r{[a-zA-Z0-9]}

  def self.sanitize(string)
    string = string.to_s
    if string.size == 4
      '****'
    else
      string.partition(CAPTURE_LAST_FOUR).tap do |parts|
        parts[0].gsub!(ALPHA_NUMERIC, '*')
      end.join
    end
  end
end