class DocusignInvestmentAgreementEnvelope
  class EnvelopeNotCreatedError < StandardError; end

  attr_reader :envelope_id

  INVESTOR = 'Investor'

  INCOMPLETE = 'incomplete'
  COMPLETED = 'completed'
  DECLINED = 'declined'

  STATUS_MAP = {
      'created' => INCOMPLETE,
      'sent' => INCOMPLETE,
      'delivered' => INCOMPLETE,
      'signed' => COMPLETED,
      'completed' => COMPLETED,
      'declined' => DECLINED,
      'faxpending' => INCOMPLETE,
      'autoresponded' => INCOMPLETE,
  }

  def initialize(envelope_id = nil)
    @envelope_id = envelope_id
  end

  def investor
    @investor ||= recipients['signers'].detect { |s| s['roleName'] == INVESTOR }
  end

  def investor_status
    STATUS_MAP[investor['status']] || INCOMPLETE
  end

  def investor_declined?
    DECLINED == investor_status
  end

  def investor_signed?
    COMPLETED == investor_status
  end

  def investor_signed_at
    investor['signedDateTime'].present? ? Time.zone.parse(investor['signedDateTime']) : nil
  end

  def self.create(docusign_template_id, details)
    individual_investment = details[:entity_name].blank?
    envelope = docusign_client.create_envelope_from_template(
        template_id: docusign_template_id,
        email: {
            subject: "#{details[:corporate_name]} Investment Agreement",
            body: '',
        },
        signers: [
            {
                embedded: true,
                name: details[:investor_name],
                email: details[:investor_email],
                role_name: INVESTOR,
                text_tabs: [
                  {
                    label: '\\*bEntityName',
                    value: individual_investment ? "" : details[:entity_name]
                  },
                  {
                    label: '\\*bInvestorName',
                    value: individual_investment ? "" : details[:investor_name]
                  },
                  {
                    label: '\\*bInvestorRole',
                    value: individual_investment ? "" : details[:investor_role]
                  },
                  {
                    label: '\\*ieAddress1',
                    value: individual_investment ? details[:investor_address1] : details[:entity_address1]
                  },
                  {
                    label: '\\*ieAddress2',
                    value: individual_investment ? details[:investor_address2] : details[:entity_address2]
                  },
                  {
                    label: '\\*ieCity',
                    value: individual_investment ? details[:investor_city] : details[:entity_city]
                  },
                  {
                    label: '\\*ieName',
                    value: individual_investment ? details[:investor_name] : details[:entity_name]
                  },
                  {
                    label: '\\*ieState',
                    value: individual_investment ? details[:investor_state] : details[:entity_state]
                  },
                  {
                      label: '\\*ieCountry',
                      value: individual_investment ? details[:investor_country] : details[:entity_country]
                  },
                  {
                    label: '\\*ieTelephone',
                    value: individual_investment ? details[:investor_phone_number] : details[:entity_phone_number]
                  },
                  {
                    label: '\\*ieZip',
                    value: individual_investment ? details[:investor_zip_code] : details[:entity_zip_code]
                  },
                  {
                    label: '\\*CloseDate',
                    value: details[:closing_date]
                  },
                  {
                    label: '\\*iInvestorEmail',
                    value: details[:investor_email]
                  },
                  {
                    label: '\\*iInvestorShares',
                    value: details[:share_count]
                  },
                  {
                    label: '\\*iInvestorInvestmentAmount',
                    value: details[:investment_amount]
                  },
                  {
                    label: '\\*iInvestorName',
                    value: details[:investor_name]
                  }
            ]},
            {
                embedded: true,
                name: details[:issuer_name],
                email: details[:issuer_email],
                role_name: 'Issuer'
            },
        ],
        status: 'sent',
    )

    unless envelope.has_key?('envelopeId')
      raise EnvelopeNotCreatedError.new("Docusign error creating envelope: #{envelope}. Details hash: #{details}")
    end

    new(envelope.fetch('envelopeId'))
  end

  private

  def self.docusign_client
    DocusignRest::Client.new
  end

  def docusign_client
    self.class.docusign_client
  end

  def recipients
    @recipients ||= docusign_client.get_envelope_recipients(envelope_id: envelope_id) if envelope_id
  end

end
