class Stakeholder < ActiveRecord::Base

  default_scope { order("rank ASC") }

  belongs_to :offering

  enum stakeholder_type_id: { team_member: 1, investor: 2 }

  after_create :update_notable_investor_if_necessary

  mount_uploader :avatar_photo, StakeholderUploader

  [:website, :facebook_url, :linkedin_url, :twitter_url].each do |url|
    validates url, web_url_format: true, allow_blank: true
  end

  def is_notable_investor=(val)
    @is_notable_investor = val.present?
    if persisted?
      offering.update_attribute('notable_investor_id', self.id)
    end
  end

  def is_notable_investor?
    return false if offering.nil?
    offering.notable_investor_id == self.id
  end
  alias_method :is_notable_investor, :is_notable_investor?

  private

  def update_notable_investor_if_necessary
    if @is_notable_investor
      offering.update_attribute('notable_investor_id', self.id)
    end
  end

end
