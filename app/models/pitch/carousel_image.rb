module Pitch
  class CarouselImage < ActiveRecord::Base

    self.table_name = 'pitch_carousel_images'

    default_scope { order(:rank, :id) }

    mount_uploader :carousel_photo, PitchUploader

    validates :carousel_photo, is_uploaded: true

  end
end


