module Pitch
  class Section < ActiveRecord::Base

    self.table_name = 'pitch_sections'

    default_scope { order(:rank, :id) }

    belongs_to :offering

    has_many :content_items, dependent: :destroy
    has_many :images, through: :content_items, source: :content, source_type: 'Pitch::Image'
    has_many :carousels, through: :content_items, source: :content, source_type: 'Pitch::Carousel'
    has_many :text_areas, through: :content_items, source: :content, source_type: 'Pitch::TextArea'
    has_many :videos, through: :content_items, source: :content, source_type: 'Pitch::Video'

    accepts_nested_attributes_for :content_items, allow_destroy: true

    validates :title, presence: true

    def content
      content_items.map(&:content)
    end

  end
end

