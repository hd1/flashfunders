module Pitch
  class Image < ActiveRecord::Base

    self.table_name = 'pitch_images'

    mount_uploader :photo, PitchUploader 

    validates :photo, is_uploaded: true

  end
end

