module Pitch
  class Video < ActiveRecord::Base
    self.table_name = 'pitch_videos'

    validates :url, presence: true, web_url_format: { allowed_protocols: [:https] }

  end
end
