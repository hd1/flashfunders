module Pitch
  class Carousel < ActiveRecord::Base

    self.table_name = 'pitch_carousels'

    has_many :carousel_images, dependent: :destroy

    accepts_nested_attributes_for :carousel_images, allow_destroy: true

  end
end

