module Pitch
  class ContentItem < ActiveRecord::Base

    self.table_name = 'pitch_content_items'

    CONTENT_TYPES = [
      'Pitch::TextArea',
      'Pitch::Video',
      'Pitch::Image',
      'Pitch::Carousel'
    ]

    default_scope { order(:rank, :id) }

    belongs_to :content, polymorphic: true
    accepts_nested_attributes_for :content

    before_destroy :destroy_content

    def build_content(params)
      if !CONTENT_TYPES.include?(content_type)
        raise "Unknown content_type: #{content_type}"
      end

      self.content = content_type.constantize.new(params)
    end

    private

    def destroy_content
      content.destroy
    end

  end
end

