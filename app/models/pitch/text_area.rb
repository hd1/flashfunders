module Pitch
  class TextArea < ActiveRecord::Base

    self.table_name = 'pitch_text_areas'

    validates :body, presence: true

  end
end

