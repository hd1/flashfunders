class IssuerApplication < ActiveRecord::Base

  store_accessor :properties,
                 :round_status,
                 :active_conversations,
                 :new_money_committed_range,
                 :investor_type,
                 :use506c,
                 :usecf,
                 :accreditation_proof,
                 :lead_source,
                 :amount_needed,
                 :hubspot_deal_id,
                 :company_criteria,
                 :num_users,
                 :promotion_budget,
                 :preferred_partner

  ENTITY_TYPES =
    [['C-Corp', 'option0'],
     ['S-Corp', 'option1'],
     ['LLC', 'option2'],
     ['Partnership', 'option3'],
     ['Sole Proprietorship', 'option4'],
     ['Other', 'option5']]
  ROUND_STATUSES =
    [['New', 'option0'],
     ['Existing', 'option1'],
     ['Not Sure', 'option2']]
  ACTIVE_CONVERSATIONS =
    [['Yes', 'option0'],
     ['No', 'option1'],
     ['Not Sure', 'option2']]
  NEW_MONEY_COMMITTED_RANGES =
    [['Less than $50K', 'option0'],
     ['$50K and above', 'option1'],
     ['Not applicable', 'option2']]
  LEAD_SOURCES =
    [['Referral', 'option0'],
     ['Social Media', 'option4'],
     ['Ad', 'option6'],
     ['Media/PR', 'option2'],
     ['Event/Conference', 'option5'],
     ['Preferred Partner', 'option8'],
     ['Other', 'option7']]
  COMPANY_CRITERIA =
    [['I have a lead investor committed for 20% of my round.', 'option0'],
     ['Successfully closed an equity crowdfunding round in the last two years.', 'option1'],
     ['Raised over $50k via traditional crowdfunding (Kickstarter, IndieGogo etc.).', 'option2'],
     ['Completed a successful crowdfunding campaign (Kickstarter, IndieGogo, etc.).', 'option3'],
     ['I\'m working with a crowdfunding agency', 'option4'],
     ['None of the above', 'option5']]
  NUM_USERS =
    [['Over 1000 customers', 'option0'],
     ['Under 1000 customers', 'option1'],
     ['0 customers or N/A', 'option2']]
  PROMOTION_BUDGET =
    [['$10K+', 'option0'],
     ['Under $10K', 'option1'],
     ['$0 or N/A', 'option2']]

  belongs_to :user

end
