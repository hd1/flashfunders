class EntityForm < GenericEntityForm

  ENTITY_ATTRS = [:name, :tax_id_number, :formation_state_id, :formation_country, :formation_date, :address_1, :address_2,
                  :city, :state_id, :country, :zip_code, :phone_number, :confirmed_authorization, :position]

  FORM_ATTRS = CORE_ATTRS + ENTITY_ATTRS + SIGNATORY_ATTRS + [:uuid, :form_type]

  MASK_ATTRS = { tax_id_number: 4 }.merge(GenericEntityForm::MASK_ATTRS)

  DEFAULTS = { country: State.default_country, signatory_country: State.default_country, formation_country: State.default_country }

  form_reader *FORM_ATTRS

  def initialize(params, user, options={})
    options.merge!({ defaults: DEFAULTS })
    super(params, user, options)
  end

  def pfii_params
    {}.tap do |hash|
      attributes.each do |attr, val|
        if SIGNATORY_ATTRS.include?(attr.to_sym)
          hash[self.class.remove_signatory(attr)] = attributes[attr]
        end
      end
    end
  end

  def form_type
    @form_type ||= entity_type
  end

  def tax_id_number
    should_mask?(:tax_id_number) ? mask_number(@tax_id_number, MASK_ATTRS[:tax_id_number]) : @tax_id_number
  end

  def entity_attrs
    ENTITY_ATTRS + CORE_ATTRS
  end

  private

  def entity_type
    entity.class.name.demodulize.downcase.to_sym unless entity.nil?
  end

  def mask_attrs
    MASK_ATTRS
  end

end
