class InvestmentProgressBar
  include Rails.application.routes.url_helpers

  STATES_D = [:started, :intended, :entity_selected, :accreditation_verified, :signed_documents, :funding_method_selected]
  STATES_S = [:started, :intended, [:entity_selected, :accreditation_verified], :signed_documents, :funding_method_selected, :ach_debit_failed]
  STATES_C = STATES_S

  STEPS_D = [:amount, :profile, :verify, :sign, :fund]
  STEPS_S = [:amount, :profile, :sign, :fund]
  STEPS_C = STEPS_S

  def initialize(_investment, _controller)
    @investment = _investment
    @controller = _controller
  end

  def applicable?
    !state_index(@investment.state).nil?
  end

  def last_step?
    step_index = step_index(step_for_controller)
    state_index = state_index(@investment.state)
    step_index == state_index
  end

  def step_state(step)
    # Returns :none, :active, :done to control progress bar display
    step = step.downcase.to_sym
    step_index = step_index(step)
    state_index = state_index(@investment.state)

    if controllers_for_step(step).include? @controller
      :active
    elsif state_index.nil?
      Rails.logger.info 'Missing state: ' + @investment.state
      :none
    elsif step_index < state_index
      :done
    elsif step_index == state_index
      :visited
    else
      :none
    end
  end

  def step_link(step)
    step = step.downcase.to_sym
    step_status = step_state(step)
    state_index = state_index(@investment.state.to_sym)
    signed_index = state_index(:signed_documents)
    show_link = (step_status == :done && state_index < signed_index) || next_step?(step, step_status, state_index)

    path_for_step step if show_link
  end

  def step_name(step)
    I18n.t("investment_progress_bar.steps.#{step.to_s}")
  end

  def steps_for_investment
    if @investment.is_reg_c?
      STEPS_C
    elsif @investment.is_reg_d?
      STEPS_D
    elsif @investment.is_reg_s?
      STEPS_S
    else
      []
    end
  end

  private

  def states_for_investment
    if @investment.is_reg_c?
      STATES_C
    elsif @investment.is_reg_d?
      STATES_D
    elsif @investment.is_reg_s?
      STATES_S
    else
      []
    end
  end

  def next_step?(step, step_state = step_state(step), state_index = state_index(@investment.state.to_sym))
    step_state == :visited && step == steps_for_investment[state_index]
  end

  def step_index(step)
    steps_for_investment.index(step)
  end

  def state_index(state)
    state = state.to_sym
    states_for_investment.each_with_index do |val, index|
      if val.is_a?(Array) && val.include?(state) || val == state
        return index
      end
    end
    return nil
  end

  def path_for_step(step)
    case step.to_s
      when 'amount'
        if @investment.offering.reg_c_enabled?
          edit_offering_investment_flow_path(offering_id: @investment.offering_id)
        else
          edit_offering_investment_path(offering_id: @investment.offering_id)
        end
      when 'profile'
        edit_offering_invest_investment_profile_path(offering_id: @investment.offering_id)
      when 'verify'
        edit_offering_invest_accreditation_new_path(offering_id: @investment.offering_id)
      when 'sign'
        offering_invest_investment_documents_path(offering_id: @investment.offering_id)
      when 'fund'
        offering_invest_fund_path(offering_id: @investment.offering_id)
      else
        nil
    end
  end

  def controllers_for_step(step)
    case step.to_s
      when 'amount'
        %w(investment investments)
      when 'profile'
        %w(investment_profile investment_profiles)
      when 'verify'
        %w(new pending verified)
      when 'sign'
        ['investment_documents']
      when 'fund'
        %w(fund funds)
      else
        nil
    end
  end

  def step_for_controller
    steps_for_investment.each { |step|
      if controllers_for_step(step).include? @controller
        return step
      end
    }
  end

end
