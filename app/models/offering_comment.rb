class OfferingComment < ActiveRecord::Base
  belongs_to :offering
  belongs_to :user
  belongs_to :parent, class_name: 'OfferingComment'
  has_many :children, class_name: 'OfferingComment', foreign_key: :parent_id

  enum display_state: { comment_visible: 1, comment_hidden_by_author: 2, comment_hidden_by_admin: 3 }

  scope :visible, -> { where(display_state: 1).order(created_at: :desc) }

  def username
    user.registration_name
  end

  def body
    self[:body].gsub("\n", "<br />").html_safe
  end

  def hide!(actor)
    comment_hidden_by_author!
    self.update_attribute(:updated_by_user, actor.id)
  end

  def has_visible_issuer_reply?
    children.any? { |reply| offering.belongs_to_user?(reply.user.id) && reply.comment_visible? }
  end

  def is_reply?
    parent_id?
  end

  def owner?(current_user)
    user == current_user
  end

end
