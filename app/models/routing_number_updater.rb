class RoutingNumberUpdater

  def self.run(database_connection, ach_input_file, dry_run, verbose)
    routing_numbers = []
    count = 0
    bank_info_url = URI.parse(ach_input_file)
    http = Net::HTTP.new(bank_info_url.host, bank_info_url.port)
    http.use_ssl = true
    request = Net::HTTP::Get.new(bank_info_url.request_uri)
    response = http.request(request)

    unless response.code == '200' && response.body.present?
      puts "HTTP response: #{response.code}"
    end

    response.body.lines.each do |bank|
      count += 1
      routing_number = bank[0..8]
      name = bank[35..70].strip

      raise InvalidRoutingNumber unless routing_number.match /\d{9}/

      routing_numbers << [routing_number, name]
      puts "#{routing_number} #{name}" if verbose
    end
    puts "Total lines read from input file: #{count}"

    unless dry_run
      table = database_connection[RoutingNumber.table_name.to_sym]
      puts "Updating database ..."
      RoutingNumber.transaction do
        RoutingNumber.delete_all

        table.import([:routing_number, :name], routing_numbers)
      end
      puts "Update complete"
    end
  end

  class InvalidRoutingNumber < RuntimeError

  end
end
