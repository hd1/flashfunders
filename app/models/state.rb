class State
  class << self
    def all(country_code = nil)
      country_code ||= default_country
      country(country_code).subregions
    end

    def all_codes(country_code=nil)
      all(country_code).map(&:code)
    end

    def all_country_codes()
      Carmen::Country.all.map(&:code)
    end

    def find_by_code(code, country_code=nil)
      all(country_code).detect { |s| s.code == code } || code
    end
    alias :find :find_by_code

    def find_by_name(name, country_code=nil)
      all(country_code).detect { |s| s.name == name }
    end

    def find_by_name_or_code(text, country_code=nil)
      all(country_code).detect { |s| s.code == text || s.name == text }
    end

    def country(code)
      Carmen::Country.coded(code)
    end

    def get_countries
      @get_countries ||= Carmen::Country.all.sort.map{|s| [s.name, s.code] }
    end

    def get_regions
      @get_regions ||=
          Carmen::Country.all.inject(Hash.new) do |hash, country|
            hash[country.code] = country.subregions.nil? ? [] : country.subregions.sort.map { |s| {name: s.name, code: s.code} }
            hash
          end
    end

    def get_subregions(country_code)
      # Tweaked so that we only return states for US
      subregions = country(country_code).try(:subregions) || []
      subregions.sort.map { |s| [s.name, s.code] if country_code != 'US' || (s.type == 'state' || ! country(s.code)) }.compact
    end

    def is_usa?(country_code, state_code = '')
      return false if country_code != 'US'
      return true if state_code == ''
      country(country_code).subregions.detect { |s| s.code == state_code && s.type == 'state' }
    end

    def default_country
      'US'
    end
  end
end
