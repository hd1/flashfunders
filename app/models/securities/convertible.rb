module Securities
  class Convertible < Security
    store_accessor :custom_data,
                   :valuation_cap,
                   :valuation_cap_display,
                   :discount_rate,
                   :interest_rate
  end
end
