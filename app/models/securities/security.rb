module Securities
  class Security < ActiveRecord::Base
    belongs_to :offering
    belongs_to :escrow_provider
    belongs_to :offering_funding_detail_domestic, class_name: 'OfferingFundingDetailDirect'
    belongs_to :offering_funding_detail_international, class_name: 'OfferingFundingDetailDirectInternational'

    has_many :deal_documents, as: :documentable

    enum regulation: { reg_d: 1, reg_cf: 2 }

    store_accessor :custom_data,
                   :success_fee,
                   :processing_fee,
                   :counter_signed_preamble

  end
end
