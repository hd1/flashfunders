class InvestorLinkedInternationalAccountFormValidator
  include ActiveModel::Validations

  def self.ensure_valid(attr, opts)
    validates attr, opts
    attr_reader attr
    private attr
  end

  ensure_valid :bank_account_number, presence: true
  ensure_valid :bank_account_holder, presence: true
  ensure_valid :bank_account_routing, presence: true
  ensure_valid :passport_image, presence: true

  def initialize(attrs = {})
    @attributes = attrs

    @attributes.each do |attr, value|
      instance_variable_set("@#{attr}", value)
    end
  end
end
