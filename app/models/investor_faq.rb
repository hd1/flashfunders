class InvestorFaq < ActiveRecord::Base

  default_scope { order(:rank, :id) }

  validates :question, presence: true
  validates :answer, presence: true

end

