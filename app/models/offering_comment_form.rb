require 'offering_comment_form_validator'

class OfferingCommentForm
  extend ActiveModel::Naming
  include ActiveModel::Conversion

  delegate :errors, :valid?, to: :validator

  attr_accessor :body, :validator

  def initialize(params = {})
    @comment_attrs = params
    @comment_attrs[:body] = strip_carriage_return(params[:body]) if params[:body].present?
    @validator = OfferingCommentFormValidator.new(params)
  end

  def save
    if @validator.valid?
      OfferingComment.create(@comment_attrs)
    else
      @validator
    end
  end

  private

  def strip_carriage_return(body)
    body.gsub("\r\n","\n")
  end

end
