class MessagePresenter
  attr_reader :message, :offering, :current_offering_id
  private :message, :offering, :current_offering_id

  delegate :id, :offering_id, :user_id, :body, :created_at, to: :message

  def initialize(message, opts={})
    @message = message
    @offering = message.offering
    @current_offering_id = opts[:current_offering].try(:id)
  end

  def company_name
    offering.company.name unless is_offering_message?
  end

  def user_name
    is_offering_message? ? message.user.full_name : message.offering.public_contact_name
  end

  def user_uuid
    message.user.uuid if is_offering_message?
  end

  def is_offering_message?
    message.offering_id == current_offering_id
  end

  def unread_count
    if is_offering_message?
      OfferingMessage.unread_for_offering(offering_id)[user_id].to_i
    else
      OfferingMessage.unread_for_investor(user_id)[offering_id].to_i
    end
  end

end
