class Subscription < ActiveRecord::Base

  before_validation :sanitize_email

  validates :email,
              presence: { message: I18n.t('subscription.error.email_not_present') },
              email: { message: I18n.t('subscription.error.email_not_valid') }

  def self.dedupe
    # find all models and group them on keys which should be common
    count = 0
    grouped = all.order('created_at asc').group_by{|sub| [sub.email]}
    grouped.values.each do |duplicates|
      # the first one we want to keep right?
      first_one = duplicates.shift # or pop for last one
      # if there are any more left, they are duplicates
      # so delete all of them
      if duplicates.count > 0
        count += 1
        puts first_one.email
        duplicates.each{|double| double.destroy} # duplicates can now be destroyed
      end
    end
    count
  end

  def save
    # Subscription already exists
    return true if  Subscription.where(email: sanitize_email).count > 0
    super
  end

  private

  def sanitize_email
    self.email = email.strip.downcase
  end
end
