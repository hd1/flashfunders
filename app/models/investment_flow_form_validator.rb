class InvestmentFlowFormValidator
  include ActiveModel::Validations

  def initialize(attrs = {}, offering = nil)
    @attributes = attrs
    @offering = offering

    @attributes.each do |attr, value|
      instance_variable_set("@#{attr}", value)
    end
  end

  def self.ensure_valid(attr, opts)
    validates attr, opts
    attr_reader attr
    private attr
  end

  ensure_valid :amount, presence: true
  ensure_valid :selector, presence: true, inclusion: %w(reg-c reg-d)

  validate :calculations_valid?
  validate :server_values_unchanged?

  private

  def server_values_unchanged?
    unless @server_internal.to_f == @internal.to_f && @server_reg_c_cutoff.to_f == @reg_c_cutoff.to_f
      msg = 'Server side calculated values were changed by the client'
      errors.add(:amount, msg)
      Honeybadger.notify(error_message: "#{msg}: @server_internal=#{@server_internal}, @internal=#{@internal}, @server_reg_c_cutoff=#{@server_reg_c_cutoff}, @reg_c_cutoff=#{@reg_c_cutoff}")
    end
    errors.blank?
  end

  def calculations_valid?
    return unless errors.blank?
    if @selector == 'reg-c'
      check_reg_c_output(JSON.parse(@reg_c_output)) unless @international == 'true'
      return unless errors.blank?
      if @amount < @reg_c_min || @amount >= @reg_c_cutoff
        errors.add(:amount, 'Reg C Investment Amount is out of bounds')
      end
    else
      if @amount < @reg_c_cutoff || @amount > @offering.reg_d_direct_security.maximum_total_investment
        errors.add(:amount, 'Reg D Investment Amount is out of bounds')
      end
    end
    errors.blank?
  end

  def check_reg_c_output(form_output)
    @reg_c_calc = RegCCalculator.new(@income, @net_worth, @external, @internal, @reg_c_cutoff, @offering.share_price)
    if @reg_c_calc.limit != form_output['limit'] || @reg_c_calc.available != form_output['available']
      msg = 'Reg C Calculations did not match'
      errors.add(:amount, msg)
      Honeybadger.notify(error_message: msg)
    end
    if @amount > @reg_c_calc.available
      errors.add(:amount, 'Investment is over the amount you have available')
    end
  end

end
