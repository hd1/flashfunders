require 'accreditation_form_validator'

class AccreditationForm

  extend ActiveModel::Naming
  include ActiveModel::Conversion

  attr_reader :user, :investor_entity, :validator, :qualification_class, :attributes, :qualification, :docs
  private :user, :investor_entity, :validator, :qualification_class

  delegate :errors, to: :validator

  def self.form_reader(*attrs)
    @form_readers = attrs
    attr_reader *attrs
  end

  def self.form_readers
    @form_readers || []
  end

  def self.slice_params(params)
    params.slice(*form_readers).to_hash
  end

  def self.default_qualification_class
    "Accreditor::#{self.to_s[0..-5]}Qualification".constantize
  end

  def self.default_validator_class
    "#{self.to_s}Validator".constantize
  end

  def initialize(attrs, user, investor_entity, options={})
    @attributes = self.class.slice_params(attrs)
    @user = user
    @investor_entity = investor_entity
    @validator = options[:validator] || self.class.default_validator_class.new(@attributes.merge(investor_email: @user.email))
    @qualification_class = options[:qualification_class] || self.class.default_qualification_class

    @attributes.each do |attr, val|
      instance_variable_set("@#{attr}", val)
    end
  end

  def save
    if validator.valid?
      attrs = attributes.clone
      file_keys = attrs.delete('file_keys')

      qualification_class.transaction do
        @qualification = qualification_class.create!(
          attrs.merge( {user_id: user.id, investor_entity_id: investor_entity.id} ))

        if file_keys
          @docs = file_keys.map do |key|
            Accreditation::Document.create!(key: key, accreditation_qualification_id: @qualification.id)
          end
        end
      end

      true
    else
      false
    end
  end

end

class ThirdPartyIncomeForm < AccreditationForm
  form_reader :role, :email
end

class ThirdPartyNetWorthForm < AccreditationForm
  form_reader :role, :email
end

class ThirdPartyEntityForm < AccreditationForm
  form_reader :role, :email
end

class IndividualIncomeForm < AccreditationForm
  form_reader :consent_to_income, :file_keys
end

class JointIncomeForm < AccreditationForm
  form_reader :consent_to_income, :file_keys, :spouse_name, :spouse_email
end

class IndividualNetWorthForm < AccreditationForm
  form_reader :credit_report_authorization, :file_keys
end

class JointNetWorthForm < AccreditationForm
  form_reader :credit_report_authorization, :file_keys, :spouse_name, :spouse_email, :spouse_ssn
end

class OfflineEntityForm < AccreditationForm
  form_reader :request_contact
end

class VerifiedEntityForm
  extend ActiveModel::Naming
  include ActiveModel::Conversion

  attr_reader :promise, :validator
  delegate :errors, to: :validator

  def initialize(attrs, opts={})
    @promise = attrs[:promise]
    @validator = opts[:validator] || VerifiedEntityFormValidator.new({promise: @promise})
  end

  def save
    validator.valid?
  end
end
