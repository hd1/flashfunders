class EntityFormValidator < GenericEntityFormValidator

  ensure_valid :name, presence: true
  ensure_valid :tax_id_number, tax_id_format: true, if: :is_usa_entity?
  ensure_valid :address_1, presence: true
  ensure_valid :city, presence: true
  ensure_valid :zip_code, presence: :true, zip_code_format: :true, if: :is_usa_entity?
  ensure_valid :state_id, presence: true, if: :is_usa_entity?
  ensure_valid :country, presence: true, inclusion: { in: State.all_country_codes }
  ensure_valid :phone_number, presence: true, us_phone_number_format: true, if: :is_usa_entity?
  ensure_valid :formation_country, presence: true
  ensure_valid :confirmed_authorization, acceptance: true
  ensure_valid :formation_date, presence: true, date_format: true

  ensure_valid :position, presence: true

  # TODO: enhance validation. Ideally reuse validation from signatory (which inherits from individual)
  ensure_valid :signatory_first_name, person_name: true, presence: true, length: { maximum: 45, message: 'Maximum of 45 characters' }
  ensure_valid :signatory_last_name, person_name: true, presence: true, length: { maximum: 45, message: 'Maximum of 45 characters' }
  ensure_valid :signatory_middle_initial, format: { with: /\A[a-zA-Z?]?\Z/, message: 'Only letters allowed', allow_nil: true }, length: { maximum: 1, message: 'Maximum of 1 character' }
  ensure_valid :signatory_address1, presence: true, length: { maximum: 128, message: 'Maximum of 128 characters' }
  ensure_valid :signatory_address2, length: { maximum: 128, message: 'Maximum of 128 characters' }
  ensure_valid :signatory_country, presence: true, inclusion: State.all_country_codes
  ensure_valid :signatory_city, city: true, presence: true, length: { maximum: 45, message: 'Maximum of 45 characters' }
  ensure_valid :signatory_state_id, presence: true, if: :is_usa_signatory?
  ensure_valid :signatory_zip_code, presence: true, zip_code_format: true, if: :is_usa_signatory?
  ensure_valid :signatory_ssn, presence: true, ssn_format: true, if: :is_usa_signatory?
  ensure_valid :signatory_date_of_birth, presence: true, date_format: true, must_be_eighteen_years_old: true
  ensure_valid :signatory_daytime_phone, us_phone_number_format: true, if: :is_usa_signatory?
  ensure_valid :signatory_mobile_phone, us_phone_number_format: true, if: :is_usa_signatory?

  validate :at_least_one_phone

  ensure_valid :signatory_id_type, presence: true, inclusion: { in: :valid_id_types }
  ensure_valid :signatory_id_number, presence: true
  ensure_valid :signatory_location, presence: true
  ensure_valid :signatory_issued_date, presence: true, date_format: true
  ensure_valid :signatory_expiration_date, presence: true, date_format: true
  ensure_valid :signatory_us_citizen, presence: true

  validate :has_valid_state
  validate :has_valid_signatory_state

  private

  def at_least_one_phone
    if signatory_daytime_phone.blank? && signatory_mobile_phone.blank?
      errors[:signatory_daytime_phone] = 'Please provide a phone number.'
    end
  end

  def has_valid_state
      errors[:state_id] = 'Required' unless validate_subregion(@country, @state_id)
      errors[:formation_state_id] = 'Required' if @formation_country == 'US' && ! validate_subregion(@formation_country, @formation_state_id)
  end

  def has_valid_signatory_state
    errors[:signatory_state_id] = 'Required' unless validate_subregion(@signatory_country, @signatory_state_id)
  end

  def validate_subregion(country, region)
    subregions = State.all_codes(country)
    subregions.size() == 0 || subregions.include?(region)
  end

  def valid_id_types
    PersonalFederalIdentificationInformation.id_types.keys + PersonalFederalIdentificationInformation.id_types.values
  end

  def is_usa_entity?
    State.is_usa? @country, @state_id
  end

  def is_usa_signatory?
    State.is_usa? @signatory_country, @signatory_state_id
  end

end
