class InvestorFundFormAuditor < Auditor
  def process_state(persisted_state)
    persisted_state.stringify_keys.tap do |hash|
      sanitize_bank_account!(hash)
    end
  end

  private

  def sanitize_bank_account!(hash)
    if hash['bank_account_number'].present?
      hash['bank_account_number'] = SanitizeToLastFourDigits.sanitize(hash['bank_account_number'])
    end
  end
end
