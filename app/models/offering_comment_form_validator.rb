class OfferingCommentFormValidator
  include ActiveModel::Validations

  MAX_COMMENT_LENGTH = 1500

  def initialize(attrs = {})
    @attributes = attrs

    @attributes.each do |attr, value|
      instance_variable_set("@#{attr}", value)
    end
  end

  def self.ensure_valid(attr, opts)
    validates attr, opts
    attr_reader attr
    private attr
  end

  ensure_valid :user_id, presence: true
  ensure_valid :offering_id, presence: true, unless: :parent_id
  ensure_valid :parent_id, presence: true, unless: :offering_id
  ensure_valid :offering_id, absence: true, if: :parent_id
  ensure_valid :parent_id, absence: true, if: :offering_id
  ensure_valid :body, length: { minimum: 1, maximum: MAX_COMMENT_LENGTH }

end
