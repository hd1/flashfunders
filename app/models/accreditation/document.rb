require 'accreditation'

module Accreditation
  class Document < ActiveRecord::Base
    include UniquelyIdentifiable

    self.table_name = :accreditation_documents

    mount_uploader :document, AccreditationDocumentUploader
  end 
end

