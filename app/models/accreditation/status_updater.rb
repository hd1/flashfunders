module Accreditation
  class StatusUpdater

    # if docs signed, and approved at any point before offering close
    #   Approved
    # else
    #   Most recent entity status that is before offering close

    attr_reader :day

    def initialize(day = Date.current)
      @day = day
    end

    def update!(dryrun = false)
      Accreditor::SpanCreator.populate_all
      Investment.includes({investor_entity: :accreditor_spans}, :offering).where('investor_entity_id is not null').find_each do |investment|
        update_single_investment(investment, dryrun)
      end
    end

    def update_investment(investment)
      investment.reload
      Accreditor::SpanCreator.new(investment.investor_entity_id).create_for_entity
      update_single_investment(investment)
    end

    private

    def update_single_investment(investment, dryrun = false)
      entity = investment.investor_entity
      status = compute_status(investment, entity)
      if investment.accreditation_status_id.to_sym != status
        change = {entity: entity.id, offering: investment.offering.id, old_status: investment.accreditation_status_id.to_sym, new_status: status, docs_signed_at: investment.docs_signed_at}
        Rails.logger.info change
        investment.update_attribute :accreditation_status_id, status unless dryrun
      end
    end

    def compute_status(investment, entity)
      case
        when approved_after_sign_date(investment, entity)
          :verified
        else
          current_status(investment.offering, entity)
      end
    end

    def approved_after_sign_date(investment, entity)
      return false if investment.docs_signed_at.blank?

      entity.accreditor_spans.any? do |span|
        span.start_date <= investment.offering.escrow_end_date &&
            span.end_date >= investment.docs_signed_at.to_date &&
            span.approved?
      end
    end

    def current_status(offering, entity)
      span = entity.accreditor_spans.detect do |span|
        (span.start_date..span.end_date).include? day_for_status(offering)
      end
      convert_span_status_to_investment_status(span)
    end

    def day_for_status(offering)
      [day, end_of_offering(offering)].min
    end

    def end_of_offering(offering)
      offering.escrow_end_date || offering.ends_at.to_date
    end

    def convert_span_status_to_investment_status(span)
      case
        when span.nil?
          :pending
        when span.approved?
          :verified
        when span.rejected?
          :rejected
        else
          :pending
      end
    end
  end
end
