class EmailConsentFormValidator < BaseValidator

  ensure_valid :email_consent, acceptance: { message: I18n.t('email_consent.not_checked') }

end
