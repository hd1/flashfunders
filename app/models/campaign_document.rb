class CampaignDocument < Document
  include Rails.application.routes.url_helpers

  validates :name, presence: true
  validates :document_new, is_uploaded: true

  def self.preset_names
    [
      'Executive Summary',
      'Pitch Deck',
      'Competitor Analysis'
    ]
  end

  def public_path
    return unless document_url && documentable_type == "Offering"
    aws_link_offering_campaign_documents_path(documentable_id, uuid)
  end
end

