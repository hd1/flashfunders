class Counter
  @index = 0
  @incr = 1
  @start = 0

  def initialize(start, incr = 1)
    @start = start
    @index = start
    @incr = incr
  end

  def reset(*p)
    @index = p.size == 0 ? @start : p[0]
  end

  def next
    @index += @incr
  end

  def current
    @index
  end

  def skip(number)
    @index += number
  end

end
