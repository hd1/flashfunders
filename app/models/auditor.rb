class Auditor
  def write(persisted_state, acting_user_id, acting_user_ip_address, path, persisted_time, impersonating_user_id = nil)
    AuditLogEntry.create(
      acting_user_id: acting_user_id,
      acting_user_ip_address: acting_user_ip_address,
      form_path: path,
      persisted_time: persisted_time,
      persisted_state: process_state(persisted_state),
      impersonating_user_id: impersonating_user_id
    )
  end

  def process_state(persisted_state)
    persisted_state
  end
end

