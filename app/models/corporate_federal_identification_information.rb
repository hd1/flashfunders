class CorporateFederalIdentificationInformation < ActiveRecord::Base

  validates :corporate_name, :state_id, presence: true
  validates :ein, ein_format: true

end