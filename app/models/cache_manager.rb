require 'httparty'
require 'sidekiq/api'

class CacheManager
  @app = ActionDispatch::Integration::Session.new(Rails.application)
  OFFERING_PARTIALS = [:pitch, :team, :fundraising, :questions]

  class << self
    def paths
      return @paths if @paths
      @paths = {}
      @paths[:browse_show] = { path: @app.browse_path, delay: 2.minutes }
      @paths[:home_show] = { path: @app.root_path, delay: 2.minutes }
      offerings = find_open_offerings
      offerings.each do |offering|
        unless offering.vanity_path.blank?
          OFFERING_PARTIALS.each do |partial|
            @paths[("offering_#{offering.vanity_path}_show_#{partial}").to_sym] = {
              path: @app.root_path + offering.vanity_path + "##{partial}",
              delay: 10.minutes
            }
          end
        end
      end
      @paths
    end

    def recursive_recache_page(uniq_key, random = 0)
      if paths[uniq_key]&.has_key?(:path)
        recache_page(paths[uniq_key][:path])
        delete_schedule(uniq_key)
        delay_for((paths[uniq_key][:delay].to_i + random).seconds).recursive_recache_page(uniq_key, rand(-15..15))
      end
    end

    def recache_page(path)
      @app.get(path + '?recache=true')
      @app.follow_redirect! if @app.redirect?

      log_message = "Fetching #{path + '?recache=true'}: Status: #{@app.status}"

      Honeybadger.notify(error_message: log_message) unless @app.response.success?
      Rails.logger.info(log_message)
    end

    def delete_schedule(uniq_key)
      schedule_jobs(uniq_key).map(&:delete)
    end

    def schedule_jobs(task, retry_count = 0)
      begin
        scheduled_set = Sidekiq::ScheduledSet.new
        scheduled_set.select { |schedule| schedule.args[0] =~ /#{task}/ }
      rescue => e
        if retry_count < 3
          sleep((retry_count * 5) + 1)
          schedule_jobs(task, retry_count + 1)
        else
          Rails.logger.error('CacheManager.schedule_jobs redis retries failed')
          Honeybadger.notify e
        end
      end
    end

    private

    def find_open_offerings
      Offering.visibility_public.open | Offering.visibility_public.closed.pending
    end

  end

end
