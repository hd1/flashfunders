class OfferingFollower < ActiveRecord::Base

  belongs_to :user
  belongs_to :offering

  enum mode: { follower: 1, asker: 2, investor: 3 }

  validates :user_id, presence: true
  validates :offering_id, presence: true

  def following?
    !!mode
  end

  def follow_as!(value)
    unless mode_downgrade?(value)
      self.mode = value
    end
    save!
  end

  def unfollow!
    self.mode = nil
    save!
  end

  private

  def mode_downgrade?(value)
    return false unless mode

    list = self.class.modes.keys
    list.index(value.to_s) < list.index(mode.to_s)
  end

end
