class Document < ActiveRecord::Base
  include UniquelyIdentifiable
  belongs_to :documentable, polymorphic: true
  mount_uploader :document_new, DocumentUploader

  default_scope { order(:rank, :id) }
  scope :active, -> (object) { where(documentable: object).where.not(document_new: nil) }

  def self.preset_names
    []
  end

  def to_param
    uuid
  end

  def position
    self.class.preset_names.index(name) || 999
  end

end
