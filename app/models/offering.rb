class Offering < ActiveRecord::Base
  class UnhandledProviderError < RuntimeError; end
  include UniquelyIdentifiable
  store_accessor :custom_data,
                 :facebook_pixel_id,
                 :google_tracking_id,
                 :google_adwords_conversion_id,
                 :google_adwords_conversion_label

  extend ColumnHider::ActiveRecordAttributes

  HIDDEN_COLUMNS = :escrow_provider_id, :offering_funding_detail_direct_id, :offering_funding_detail_direct_international_id,
    :shares_offered, :minimum_total_investment, :minimum_total_investment_display,
    :maximum_total_investment, :maximum_total_investment_display, :minimum_investment_amount,
    :minimum_investment_amount_display, :external_investment_amount, :external_investor_count,
    :docusign_template_id, :docusign_international_template_id, :spv_docusign_template_id,
    :spv_docusign_international_template_id, :fund_america_id, :fund_america_spv_id,
    :cf_minimum_total_investment, :seed_minimum_total_investment, :cf_minimum_total_investment_display,
    :seed_minimum_total_investment_display, :cf_maximum_total_investment, :seed_maximum_total_investment,
    :cf_maximum_total_investment_display, :seed_maximum_total_investment_display, :cf_minimum_investment_amount,
    :seed_minimum_investment_amount, :cf_minimum_investment_amount_display, :seed_minimum_investment_amount_display,
    :cf_shares_offered, :seed_shares_offered, :cf_fully_diluted_shares,
    :seed_fully_diluted_shares, :cf_external_investment_amount, :seed_external_investment_amount,
    :cf_external_investor_count, :seed_external_investor_count, :cf_security_type_id,
    :seed_security_type_id

  column_hider_deprecate_columns *HIDDEN_COLUMNS

  attr_reader :minimum_investment_amount_display  # legacy offerings hack
  belongs_to :issuer_user, class_name: 'User'
  belongs_to :user
  has_many :investments
  has_many :offering_comments
  has_many :securities, class_name: 'Securities::Security'

  validates :vanity_path, uniqueness: true,
                          length: { in: (0..255) },
                          format: { with: /\A[-a-z0-9]+\z/i, message: 'only letters, numbers, and dashes are allowed' },
                          allow_blank: true,
                          valid_vanity_url: true

  validates :share_photo, web_url_format: true, allow_nil: true

  validates :tagline_browse,
            length:      { in: (0..100) },
            allow_blank: true

  enum closed_status: { pending: 1, unsuccessful: 2, successful: 3 }
  enum visibility: { visibility_restricted: 1, visibility_preview: 2, visibility_public: 3 }

  has_one :company

  accepts_nested_attributes_for :company

  has_many :documents, as: :documentable
  has_many :campaign_documents, as: :documentable
  has_many :investor_faqs
  has_many :pitch_sections, class_name: 'Pitch::Section', dependent: :destroy
  has_many :stakeholders
  has_many :investors, -> { investor }, class_name: 'Stakeholder'
  has_many :team_members, -> { team_member }, class_name: 'Stakeholder'

  belongs_to :notable_investor, class_name: 'Stakeholder', foreign_key: 'notable_investor_id'

  accepts_nested_attributes_for :pitch_sections, allow_destroy: true
  accepts_nested_attributes_for :investor_faqs, allow_destroy: true
  accepts_nested_attributes_for :team_members, allow_destroy: true
  accepts_nested_attributes_for :investors, allow_destroy: true
  accepts_nested_attributes_for :campaign_documents, allow_destroy: true

  delegate :corporate_name, to: :company
  delegate :email, to: :issuer_user, allow_nil: true, prefix: 'public'

  mount_uploader :photo_cover, OfferingUploader
  mount_uploader :photo_swatch, OfferingUploader
  mount_uploader :photo_logo, OfferingUploader
  mount_uploader :photo_home, OfferingUploader
  mount_uploader :photo_header, OfferingUploader

  scope :open,        -> { where('ends_at IS null OR ends_at > ?', Time.current.utc) }
  scope :closed,      -> { where('ends_at <= ?', Time.current.utc) }
  scope :for_user,    ->(user) { where('issuer_user_id = ? OR user_id = ?', user, user).order(:id) }

  ### Columns to be deleted
  def model_direct_spv?
    !!securities.detect { |s| s.is_spv? }
  end

  def model_direct?
    !model_direct_spv?
  end

  def reg_d_direct_security
    securities.detect { |s| !s.is_spv? && s.reg_d? }
  end

  def reg_d_securities
    securities.select { |s| s.reg_d? }
  end

  def direct_investment_threshold
    reg_d_securities.map { |s| s.minimum_investment_amount.to_i }.max
  end

  def reg_c_security
    securities.detect { |s| s.reg_cf? }
  end

  def reg_c_enabled?
    reg_c_security.present?
  end

  def reg_c_only?
    reg_c_enabled? && securities.count == 1
  end

  def other_security
    securities.detect { |s| s != reg_d_direct_security }
  end

  def vanity_path=(value)
    self[:vanity_path] = value.downcase
  end

  def belongs_to_user?(id)
    user_id == id || issuer_user_id == id
  end

  def ended?
    return false unless ends_at.present?
    t = Time.now
    ends_at < t
  end

  def escrow_closed?
    return false unless escrow_end_date.present?
    escrow_end_date < Date.current
  end

  def closed_successfully?
    ended? && successful?
  end

  def closed_unsuccessfully?
    ended? && unsuccessful?
  end

  def closed_pending?
    ended? && pending?
  end

  def minimum_investment_amount_display # attr_reader hack for legacy offerings
    legacy_offering? ? "" : self[:minimum_investment_amount_display]
  end

  def legacy_offering?
    [25, 18, 21].include? id  # used to temporarily prevent display of offering minimum investment amount for Vessix, TheraCell and Swggr
  end

  def fsp_return_multiplier
    if id == 61
      "1.5"
    else
      "3"
    end
  end

end
