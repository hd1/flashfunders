class EscrowProvider < ActiveRecord::Base
  extend StoreAccessorBoolean::ActiveRecordExtensions
  has_many :offerings

  store_accessor_boolean :properties,
                         :requires_instructions_international_direct_check,
                         :requires_instructions_international_direct_wire,
                         :requires_instructions_international_spv_check,
                         :requires_instructions_international_spv_wire,
                         :requires_instructions_domestic_direct_check,
                         :requires_instructions_domestic_direct_wire,
                         :requires_instructions_domestic_spv_check,
                         :requires_instructions_domestic_spv_wire,
                         :requires_offering_funding_details,
                         :provides_realtime_investment_api,
                         :allows_refund_account,
                         :show_payment_details,
                         :active

  store_accessor :properties,
                 :wire_code

  def any_funding_instructions?
    requires_instructions_international_direct_check? || requires_instructions_international_direct_wire? ||
      requires_instructions_international_spv_check?  || requires_instructions_international_spv_wire? ||
      requires_instructions_domestic_direct_check?    || requires_instructions_domestic_direct_wire? ||
      requires_instructions_domestic_spv_check?       || requires_instructions_domestic_spv_wire?
  end

  def notify_completion!(investment, controller_params={})
    return unless provides_realtime_investment_api?

    # TODO - the provider key starts with these values, but may also include other values, e.g. 'fund_america_reg_c'
    # https://www.pivotaltracker.com/story/show/132189107
    case provider_key
    when /fund_america/
      delay.fund_america_update_investment(investment.id, controller_params)
    when 'fintech_clearing'
      delay.fintech_clearing_update_investment(investment.id, controller_params)
    end
  end

  def title(long: false)
    if provider_key.include?('fund_america')
      return long ? 'FundAmerica Securities LLC (FundAmerica)' : 'FundAmerica'
    end
    if provider_key.include?('fintech_clearing')
      return long ? 'Fintech Clearing LLC (Fintech Clearing)' : 'Fintech Clearing'
    end
  end

  private

  def fund_america_update_investment(investment_id, controller_params)
    FundAmericaEscrowHelper.fund_america_update_investment(investment_id, controller_params)
  end

  def fintech_clearing_update_investment(investment_id, controller_params)
    FintechClearingEscrowHelper.update_investment(investment_id, controller_params)
  end

end
