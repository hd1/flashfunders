class DatabaseSanitizer

  DOCUSIGN_REG_C_TEMPLATE = '4ECF85B1-7FB1-45C7-91BD-586C8E8EB783'
  DOCUSIGN_REG_D_TEMPLATE = '3C712E13-F984-46B2-BE58-F323BA9B6292'
  DOCUSIGN_REG_S_TEMPLATE = 'E05F3F8B-F62E-4847-B4BF-BC01B560073F'

  class << self

    def sanitize
      check_environment
      sanitize_users
      sanitize_pfii
      sanitize_bank_accounts
      sanitize_securities
      sanitize_investments
      sanitize_misc
    end

    private

    def sanitize_users
      log_step 'Sanitizing users...'
      User.all.each do |u|
        u.update_attributes(password: 'password')
      end
    end

    def sanitize_pfii
      log_step 'Sanitizing pfii'
      PersonalFederalIdentificationInformation.all.each do |p|
        if p.user.present?
          p.update_attributes(ssn: '111111111',
                              date_of_birth: 27.years.ago,
                              id_number: '1234')
        end
      end
    end

    def sanitize_bank_accounts
      log_step 'Sanitizing bank accounts...'
      Bank::Account.all.each do |ba|
        ba.update_attributes(account_number: '111111111',
                             account_holder: 'John Doe')
      end
    end

    def sanitize_securities
      log_step 'Sanitizing securities...'
      Securities::Security.all.each do |s|
        s.update_attributes(fund_america_id: nil, docusign_international_template_id: DOCUSIGN_REG_S_TEMPLATE)
      end
      Securities::Stock.all.each do |s|
        s.update_attributes(docusign_template_id: DOCUSIGN_REG_C_TEMPLATE)
      end
      Securities::FspStock.all.each do |s|
        s.update_attributes(docusign_template_id: DOCUSIGN_REG_D_TEMPLATE)
      end
    end

    def sanitize_investments
      log_step 'Sanitizing investments...'
      Investment.all.each do |i|
        i.update_attributes(fund_america_id: nil, envelope_id: nil, cached_envelope_details: nil)
      end
    end

    def sanitize_misc
      log_step 'Finishing up...'
      ActiveRecord::Base.connection.execute('TRUNCATE fund_america_requests RESTART IDENTITY')
      setup_bank_of_flashdough
    end

    def setup_bank_of_flashdough
      routing_number = '111111111'
      bank_name = 'Bank of Flashdough'
      unless RoutingNumber.where(routing_number: routing_number).count > 0
        RoutingNumber.create(routing_number: routing_number, name: bank_name)
      end
    end

    def log_step(step)
      puts "#{Time.now.to_s}\t#{step}"
    end
    
    def check_environment
      # We do not want to ever run these methods in production!
      if Rails.env.production?
        raise 'Sanitize routines should never be run in the production environment!!'
      end
    end
  end


end
