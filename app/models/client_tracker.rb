class ClientTracker < ActiveRecord::Base
  belongs_to :trackable, polymorphic: true

  validates :client_id, :trackable_id, :trackable_type, presence: true
  validates_uniqueness_of :client_id, scope: [:trackable_id ,:trackable_type]
end
