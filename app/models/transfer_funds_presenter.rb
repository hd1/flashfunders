class TransferFundsPresenter

  include ActiveSupport::NumberHelper

  attr_reader :offering_funding_detail

  delegate :check_payable_to, :check_bank_name, :check_address,
    :wire_beneficiary_name, :wire_beneficiary_address, :wire_bank_address,
    :wire_bank_account, :wire_instructions, to: :offering_funding_detail, allow_nil: true

  def initialize(investment, location)
    @investment = investment
    @offering_funding_detail = location == 'international' ?
        @investment.security.offering_funding_detail_international :
        @investment.security.offering_funding_detail_domestic
  end

  def wire_bank_name
    wire_bank_account.try(:bank_name)
  end

  def check_phone_number
    return unless check_address
    number_to_phone(check_address.phone_number, area_code: true)
  end

  def wire_bank_phone_number
    return unless wire_bank_address
    number_to_phone(wire_bank_address.phone_number, area_code: true)
  end

  def wire_account_number
    wire_bank_account.try(:account_number)
  end

  def wire_routing_number
    wire_bank_account.try(:routing_number)
  end

  def wire_account_type
    wire_bank_account.try(:account_type)
  end

  def formatted_check_address
    multi_line_address(check_address)
  end

  def formatted_wire_bank_address
    multi_line_address(wire_bank_address)
  end

  def formatted_wire_beneficiary_address
    multi_line_address(wire_beneficiary_address)
  end

  def formatted_wire_instructions
    @offering_funding_detail.wire_instructions + " - #{@investment.investor_entity.full_name}"
  end

  def formatted_check_instructions
    @offering_funding_detail.check_memo + " - #{@investment.investor_entity.full_name}"
  end

  private

  def multi_line_address(address)
    return unless address

    formatted_address = "#{address.address1}<br>"
    formatted_address << "#{address.address2}<br>" unless address.address2.blank?
    formatted_address << "#{address.city}, #{address.state} #{address.zip_code}"

    formatted_address.html_safe
  end
end

