class EquityPercentageCalculator
  attr_reader :shares_offered, :fully_diluted_shares
  private :shares_offered, :fully_diluted_shares

  def initialize(shares_offered, fully_diluted_shares)
    @shares_offered = shares_offered || 0
    @fully_diluted_shares = fully_diluted_shares || 0
  end

  def percent_offered
    if shares_offered + fully_diluted_shares == 0
      0
    else
      percent = (shares_offered.to_f / (fully_diluted_shares + shares_offered)) * 100
      percent.round(2)
    end
  end
end