class AccreditationFormAuditor < Auditor
  def process_state(persisted_state)
    persisted_state.stringify_keys.tap do |hash|
      sanitize_ssn!(hash)
    end
  end

  private

  def sanitize_ssn!(hash)
    if hash['spouse_ssn'].present?
      hash['spouse_ssn'] = SanitizeToLastFourDigits.sanitize(hash['spouse_ssn'])
    end
  end
end
