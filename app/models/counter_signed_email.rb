class CounterSignedEmail < InvestmentEmail
  def investments
    return saved_investments unless saved_investments.empty?

    Investment.where(offering_id: offering_id, state: FUNDED_STATES)
  end
end
