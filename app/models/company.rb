class Company < ActiveRecord::Base
  has_one :corporate_federal_identification_information

  delegate :corporate_name, to: :corporate_federal_identification_information

  validates :website_url, web_url_format: true, allow_blank: true
end
