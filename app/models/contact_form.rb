require 'contact_form_validator'

class ContactForm
  extend ActiveModel::Naming
  include ActiveModel::Conversion

  delegate :errors, to: :validator

  attr_accessor :user_type, :page_name, :name, :email, :message, :user_id, :validator

  def initialize(params = nil)
    self.user_type = params[:user_type] if params[:user_type].present?
    self.name      = params[:name] if params[:name].present?
    self.email     = params[:email] if params[:email].present?
    self.page_name = params[:page_name] if params[:page_name].present?
    self.message   = params[:message] if params[:message].present?
    @validator     = ContactFormValidator.new(params)
  end

  def deliver
    if validator.valid?
      ContactMailer.contact_us(user_type, name, email, message, page_name, user_id).deliver_later
      ContactMessage.new({ email: email, name: name, kind: user_type, message: message, user_id: user_id }).save
    end

  end

  def persisted?
    false
  end

end
