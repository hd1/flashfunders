class OfferingFundingDetail < ActiveRecord::Base

  belongs_to :check_address, class_name: 'Address'
  belongs_to :wire_bank_address, class_name: 'Address'
  belongs_to :wire_beneficiary_address, class_name: 'Address'
  belongs_to :wire_bank_account, class_name: 'Bank::Account'
  has_one :security

end
