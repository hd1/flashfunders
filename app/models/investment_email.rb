class InvestmentEmail < ActiveRecord::Base

  FUNDED_STATES = %w(funding_method_selected funds_sent escrowed counter_signed stock_received)

  has_one :offering

  def initialize(offering, data={})
    raise 'Cannot directly instantiate an InvestmentEmail' if self.class == InvestmentEmail
    super(offering_id: offering.id, queued_at: Time.now, data: data)
  end

  def investment_ids
    data['investment_ids']
  end

  def investment_ids=(ids)
    data['investment_ids'] = ids
  end

  def trigger_investment_id
    data['trigger_investment_id']
  end

  def trigger_investment_id=(id)
    data['trigger_investment_id'] = id
  end

  private

  def saved_investments
    @saved_investments ||= Investment.where(id: investment_ids)
  end
end
