class ContactFormValidator
  include ActiveModel::Validations

  def initialize(attrs = {})
    @attributes = attrs

    @attributes.each do |attr, value|
      instance_variable_set("@#{attr}", value)
    end
  end

  def self.ensure_valid(attr, opts)
    validates attr, opts
    attr_reader attr
    private attr
  end

  ensure_valid :user_type, presence: { message: 'Please select one of the above options' }
  ensure_valid :name, presence: { message: 'Please enter your name' }
  ensure_valid :email, email: { message: 'Please provide a valid email address' }
  ensure_valid :message, presence: { message: 'Please include a message' }

end
