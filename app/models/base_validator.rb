class BaseValidator
  include ActiveModel::Validations

  def initialize(attrs = {})
    @attributes = attrs

    @attributes.each do |attr, value|
      instance_variable_set("@#{attr}", value)
    end
  end

  def self.ensure_valid(attr, opts)
    validates attr, opts
    attr_reader attr
    private attr
  end

end