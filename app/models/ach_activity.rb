class AchActivity < ActiveRecord::Base

  has_one :investment
  belongs_to :ach_batch

  attr_encrypted :account_number, key: Rails.application.config.encryption.bank_account_key
  attr_encrypted :account_holder, key: Rails.application.config.encryption.bank_account_holder_key

  enum xact_code: {
    ccredit: 0,
    scredit: 1,
    cdebit: 2,
    sdebit: 3
  }
end
