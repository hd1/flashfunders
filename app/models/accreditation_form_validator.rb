class AccreditationFormValidator
  include ActiveModel::Validations

  def initialize(attrs = {})
    @attributes = attrs

    @attributes.each do |attr, value|
      instance_variable_set("@#{attr}", value)
    end
  end

  def self.ensure_valid(attr, opts)
    validates attr, opts
    attr_reader attr
    private attr
  end

  ensure_valid :attributes, presence: true
  ensure_valid :investor_email, presence: true

  def email_differs_from_investors
    if investor_email == email
      errors.add(:email, I18n.t('investor_accreditation.error.verifier_email_not_present'))
    end
  end

  def spouse_email_differs_from_investors
    if investor_email == spouse_email
      errors.add(:spouse_email, I18n.t('investor_accreditation.error.spouse_email_matches_investors'))
    end
  end
end

class ThirdPartyAccreditationFormValidator < AccreditationFormValidator
  ensure_valid :role, presence: {message: I18n.t('investor_accreditation.error.role_not_present')}

  ensure_valid :email,
    presence: {message: I18n.t('investor_accreditation.error.verifier_email_not_present')},
    email: {message: I18n.t('investor_accreditation.error.email_not_valid')}

  validate :email_differs_from_investors
end

class ThirdPartyIncomeFormValidator < ThirdPartyAccreditationFormValidator; end

class ThirdPartyNetWorthFormValidator < ThirdPartyAccreditationFormValidator; end

class ThirdPartyEntityFormValidator < ThirdPartyAccreditationFormValidator; end

class IndividualIncomeFormValidator < AccreditationFormValidator
  ensure_valid :consent_to_income, acceptance: { message: 'FIXME' }

  ensure_valid :file_keys, presence: {message: I18n.t('investor_accreditation.error.files_not_present')}
end

class JointIncomeFormValidator < AccreditationFormValidator
  ensure_valid :consent_to_income, acceptance: { message: 'FIXME' }

  ensure_valid :file_keys, presence: {message: I18n.t('investor_accreditation.error.files_not_present')}

  ensure_valid :spouse_name, presence: {message: I18n.t('investor_accreditation.error.spouse_name_not_present')}

  ensure_valid :spouse_email,
               presence: {message: I18n.t('investor_accreditation.error.spouse_email_not_present')},
               email: {message: I18n.t('investor_accreditation.error.email_not_valid')}

  validate :spouse_email_differs_from_investors
end

class IndividualNetWorthFormValidator < AccreditationFormValidator
  ensure_valid :credit_report_authorization, acceptance: { message: 'FIXME' }

  ensure_valid :file_keys, presence: {message: I18n.t('investor_accreditation.error.files_not_present')}
end

class JointNetWorthFormValidator < AccreditationFormValidator
  ensure_valid :credit_report_authorization, acceptance: { message: 'FIXME' }

  ensure_valid :file_keys, presence: {message: I18n.t('investor_accreditation.error.files_not_present')}

  ensure_valid :spouse_name, presence: {message: I18n.t('investor_accreditation.error.spouse_name_not_present')}

  ensure_valid :spouse_email,
               presence: {message: I18n.t('investor_accreditation.error.spouse_email_not_present')},
               email: {message: I18n.t('investor_accreditation.error.email_not_valid')}

  ensure_valid :spouse_ssn, presence: {message: I18n.t('investor_accreditation.error.spouse_ssn_not_present')}, ssn_format: true

  validate :spouse_email_differs_from_investors
end

class OfflineEntityFormValidator < AccreditationFormValidator
  ensure_valid :contact_request, acceptance: true
end

class InternationalFormValidator < AccreditationFormValidator
  ensure_valid :verify_option, presence: {message: I18n.t('investor_accreditation.error.international.no_option_selected')}
end

class VerifiedEntityFormValidator
  include ActiveModel::Validations

  def initialize(attrs = {})
    @attributes = attrs

    @attributes.each do |attr, value|
      instance_variable_set("@#{attr}", value)
    end
  end

  validates :promise, presence: true, acceptance: true
  attr_reader :promise
  private :promise
end
