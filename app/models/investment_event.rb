class InvestmentEvent < ActiveRecord::Base
  class BeginEvent < InvestmentEvent
  end

  class IntendedEvent < InvestmentEvent
  end

  class SelectEntityEvent < InvestmentEvent
  end

  class VerifyAccreditationEvent < InvestmentEvent
  end

  class SignDocumentsEvent < InvestmentEvent
  end

  class DeclineDocumentsEvent < InvestmentEvent
  end

  class SelectFundingMethodEvent < InvestmentEvent
  end

  class SendFundsEvent < InvestmentEvent
  end

  class TransferFundsEvent < InvestmentEvent
  end

  class InitiateAchDebitEvent < InvestmentEvent
  end

  class AchDebitFailureEvent < InvestmentEvent
  end

  class AllowFundingEvent < InvestmentEvent
  end

  class RefundEvent < InvestmentEvent
  end

  class EditAmountEvent < InvestmentEvent
  end

  class EditProfileEvent < InvestmentEvent
  end

  class EditVerifyEvent < InvestmentEvent
  end

  class CounterSignEvent < InvestmentEvent
  end

  class ReceiveStockEvent < InvestmentEvent
  end

  # To be deleted but not until this db migration is run
  class AgreeToPrerequisitesEvent < InvestmentEvent
  end

  class SelectWireTransferEvent < InvestmentEvent
  end

  class SelectPaperCheckEvent < InvestmentEvent
  end

  class AcceptInvestmentEvent < InvestmentEvent
  end

  class LinkRefundAccountEvent < InvestmentEvent
  end

  class MailPaymentEvent < InvestmentEvent
  end

  class WirePaymentEvent < InvestmentEvent
  end

  class FailFundEscrowEvent < InvestmentEvent
  end

  class FixFundEscrowEvent < InvestmentEvent
  end

  class EvaluateSuitabilityEvent < InvestmentEvent
  end

  class EditSuitabilityEvent < InvestmentEvent
  end

  class CancelEvent < InvestmentEvent
  end

end
