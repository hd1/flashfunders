class IssuerApplicationFormValidator
  include ActiveModel::Validations

  def initialize(attrs = {})
    @attributes = attrs

    @attributes.each do |attr, value|
      value = value.respond_to?(:strip) ? value.strip : value
      instance_variable_set("@#{attr}", value)
    end
  end

  def self.ensure_valid(attr, opts)
    validates attr, opts
    declare_attr(attr)
  end

  def self.declare_attr(attr)
    attr_reader attr
    private attr
  end

  # Founder info
  ensure_valid :registration_name, presence: true, person_name: true
  ensure_valid :email, email: true, format: { with: Devise.email_regexp }
  ensure_valid :phone_number, presence: true
  # Company Info
  ensure_valid :company_name, presence: true
  ensure_valid :website, presence: true
  ensure_valid :entity_type, inclusion: { in: IssuerApplication::ENTITY_TYPES.map(&:second), message: I18n.t('issuer_application.new.errors.required') }
  # Campaign Info
  ensure_valid :amount_needed, presence: true
  ensure_valid :company_criteria, presence: true
  ensure_valid :new_money_committed_range, presence: true, inclusion: { in: IssuerApplication::NEW_MONEY_COMMITTED_RANGES.map(&:second), message: I18n.t('issuer_application.new.errors.required') }
  ensure_valid :num_users, presence: true, inclusion: { in: IssuerApplication::NUM_USERS.map(&:second), message: I18n.t('issuer_application.new.errors.required') }
  ensure_valid :promotion_budget, presence: true, inclusion: { in: IssuerApplication::PROMOTION_BUDGET.map(&:second), message: I18n.t('issuer_application.new.errors.required') }
  ensure_valid :lead_source, presence: true, inclusion: { in: IssuerApplication::LEAD_SOURCES.map(&:second), message: I18n.t('issuer_application.new.errors.required') }
  declare_attr :preferred_partner

  validate :validate_company_criteria
  validate :validate_preferred_partner


  private

  def validate_company_criteria
    validate_multi_select(:company_criteria, IssuerApplication::COMPANY_CRITERIA.map(&:second))
  end

  def validate_preferred_partner
    if @lead_source == 'option8' && @preferred_partner.blank?
      errors.add(:preferred_partner, I18n.t('issuer_application.new.errors.required'))
      false
    else
      true
    end
  end

  def validate_multi_select(field, set)
    if (instance_variable_get("@#{field.to_s}") & set).count > 0
      true
    else
      errors.add(field, I18n.t('issuer_application.new.errors.required'))
      false
    end
  end
end
