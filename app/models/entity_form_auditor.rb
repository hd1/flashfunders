class EntityFormAuditor < Auditor
  def process_state(persisted_state)
    persisted_state.stringify_keys.tap do |hash|
      sanitize_ssn!(hash)
    end
  end

  private

  def sanitize_ssn!(hash)
    hash['signatory_ssn'] = SanitizeToLastFourDigits.sanitize(hash['signatory_ssn']) if hash.has_key?('signatory_ssn')
  end

end
