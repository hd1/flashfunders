class DocusignInvestorAgreementParameterBuilder
  include ActiveSupport::NumberHelper

  attr_accessor :investment_id

  def initialize(investment_id)
    @investment_id = investment_id
  end

  def envelope_details
    details = {}.tap do |hash|
      hash[:issuer_name]           = issuer_name
      hash[:issuer_email]          = issuer_email
      hash[:corporate_name]        = offering.corporate_name
      hash[:investor_name]         = investor_name
      hash[:investor_email]        = investor_email
      hash[:closing_date]          = offering.ends_at.to_date
      hash[:investor_state]        = investor_info.state_name
      hash[:investor_country]      = investor_info.country_name
      hash[:share_count]           = investment.shares
      hash[:investment_amount]     = number_to_currency((investment.amount * 100.0).floor / 100.0)
      hash[:investor_phone_number] = investor_info.phone_number
      hash[:investor_address1]     = investor_info.address1
      hash[:investor_address2]     = investor_info.address2
      hash[:investor_city]         = investor_info.city
      hash[:investor_zip_code]     = investor_info.zip_code
    end

    if entity_investment?
      details.merge!({}.tap do |hash|
         hash[:investor_role]       = investor_entity.position
         hash[:entity_name]         = investor_entity.name
         hash[:entity_state]        = investor_entity.state_name
         hash[:entity_country]      = investor_entity.country_name
         hash[:entity_phone_number] = investor_entity.phone_number
         hash[:entity_address1]     = investor_entity.address_1
         hash[:entity_address2]     = investor_entity.address_2
         hash[:entity_zip_code]     = investor_entity.zip_code
         hash[:entity_city]         = investor_entity.city
       end
      )
    end

    details
  end

  def template_id
    if investor_entity.is_international?
      investment.security.docusign_international_template_id
    else
      investment.security.docusign_template_id
    end
  end

  def envelope_id
    investment.envelope_id
  end

  def issuer_name
    full_name_for_signatory(issuer_info)
  end

  def issuer_email
    issuer_user.email
  end

  def investor_name
    full_name_for_signatory(investor_info)
  end

  def investor_email
    investor_user.email
  end

  private

  def full_name_for_signatory(federal_identification_information)
    [federal_identification_information.first_name, federal_identification_information.middle_initial, federal_identification_information.last_name].compact.join(' ')
  end

  def investment
    @investment ||= Investment.find(investment_id)
  end

  def offering
    @offering ||= Offering.find(investment.offering_id)
  end

  def issuer_info
    @issuer_info ||= PersonalFederalIdentificationInformation.find_by_user_id(offering.user_id)
  end

  def investor_info
    @investor_info ||= investment.investor_entity.personal_federal_identification_information
  end

  def investor_user
    @investor_user ||= investment.user
  end

  def investor_entity
    @investor_entity ||= investment.investor_entity
  end

  def issuer_user
    @issuer_user ||= offering.user
  end

  def entity_investment?
    @entity_investment ||= !investment.by_individual?
  end

end

