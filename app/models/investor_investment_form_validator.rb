class InvestorInvestmentFormValidator
  include ActiveModel::Validations

  validates :attributes, presence: {message: 'Complete all required fields.'}
  attr_reader :attributes
  private :attributes

  validates :unrounded_investment_amount, presence: {message: 'Provide investment amount in ####.## format.'}
  attr_reader :unrounded_investment_amount
  private :unrounded_investment_amount

  validate :projected_cost_above_minimum_investment_amount
  validate :projected_cost_below_maximum_investment_amount

  def initialize(
      attributes = {},
          investment_calculator = nil,
          offering_presenter = OfferingPresenter.new(nil)
  )
    @attributes = attributes
    @attributes.each do |attr, value|
      instance_variable_set("@#{attr}", value)
    end

    @investment_calculator = investment_calculator

    @offering_presenter = offering_presenter
  end

  private

  def projected_cost_above_minimum_investment_amount
    if @investment_calculator.projected_cost < minimum_investment
      errors.add(:unrounded_investment_amount, "Must be larger than $#{sprintf('%.2f', minimum_investment)}")
    end
  end

  def projected_cost_below_maximum_investment_amount
    if @investment_calculator.projected_cost > maximum_investment
      errors.add(:unrounded_investment_amount, "Must not be larger than $#{sprintf('%.2f', maximum_investment)}")
    end
  end

  def minimum_investment
    @offering_presenter.minimum_investment_amount
  end

  def maximum_investment
    @offering_presenter.maximum_total_investment
  end

end

