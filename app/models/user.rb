  class User < ActiveRecord::Base
  devise :database_authenticatable, :registerable, :confirmable, :async,
         :recoverable, :rememberable, :trackable, :validatable, :lockable,
         :timeoutable, :omniauthable, omniauth_providers: [:facebook]

  include UniquelyIdentifiable

  has_many :investments
  has_many :investor_entities
  has_many :investor_accounts, class_name: 'Bank::InvestorAccount'
  has_many :contact_messages
  has_many :offering_messages
  has_many :offering_comments
  has_one :personal_federal_identification_information
  delegate :country, :us_citizen, :international?, :country=, :us_citizen=, to: :personal_federal_identification_information, allow_nil: true

  enum authorized_to_invest_status: { pending: 1, allowed: 2, rejected: 3 }
  enum user_type: { user_type_investor: 1, user_type_entrepreneur: 2 }

  attr_accessor :agreed_to_subscribe

  validates :email, presence: true

  def forename
    name_parts = registration_name.to_s.split
    name_parts.first || ""
  end

  def surname
    name_parts = registration_name.to_s.split
    name_parts.size > 1 ? name_parts.last : ''
  end

  def first_name
    if personal_federal_identification_information != nil && personal_federal_identification_information.first_name.present?
      personal_federal_identification_information.first_name
    else
      forename
    end
  end

  def last_name
    if personal_federal_identification_information != nil && personal_federal_identification_information.last_name.present?
      personal_federal_identification_information.last_name
    else
      surname
    end
  end

  def full_name
    personal_federal_identification_information.try(:full_name).blank? ? registration_name : personal_federal_identification_information.full_name
  end

  def self.uuid_to_id(uuid)
    find_by(uuid: uuid).try(:id)
  end

  def has_individual_investor_entities?
    investor_entities.where(type: InvestorEntity::Individual.to_s).count > 0
  end

  def subscriptions
    Subscription.where(email: email)
  end

  def has_subscriptions?
    subscriptions.count > 0
  end

  def followed_offerings
    @followed_offerings ||= OfferingFollower.where(user_id: id).where.not(mode: nil)
  end

  def is_following_offerings?
    followed_offerings.count > 0
  end

  def first_subscribed
    subscriptions.order('created_at asc').pluck(:created_at).first
  end

  def password_match?
    self.errors[:password] << "can't be blank" if password.blank?
    self.errors[:password_confirmation] << "can't be blank" if password_confirmation.blank?
    self.errors[:password_confirmation] << "does not match password" if password != password_confirmation
    password == password_confirmation && !password.blank?
  end

  def has_no_password?
    self.encrypted_password.blank? || self.needs_password_set?
  end

  def confirm!
    self.needs_password_set = false
    super
  end

  def send_on_create_confirmation_instructions
    # Suppress default welcome email
  end

  def send_confirmation_instructions(opts={}, method=:confirmation_instructions)
    unless @raw_confirmation_token
      generate_confirmation_token!
    end
    opts[:to] = unconfirmed_email if pending_reconfirmation?
    send_devise_notification(method, @raw_confirmation_token, opts)
  end

  def self.create_temporary_user(name, email, type = :user_type_investor)
    generated_password = Devise.friendly_token.first(8)
    # We need the needs_password_set=true to block the sending of the welcome email.
    user = User.create(email: email, registration_name: name, password: generated_password, needs_password_set: true, user_type: type)
    user.encrypted_password = ''
    user.save
    user.skip_confirmation!
    user
  end

  def consent_needed?
    !email_consent_at? || !agreed_to_tos?
  end

  SAVE_SESSION_KEYS = %w(user_return_to user_return_to_fragment recreate_params complete_intro_to_founder)

  def save_context(session, path)
    context = { path: path || saved_path }
    session.keys.each { |k| context[k] = session[k] if SAVE_SESSION_KEYS.include?(k) }
    update_attribute(:context, context)
  end

  def saved_path
    context['path'] unless context.blank?
  end

  # TODO: Should we clear out session variables that aren't saved in the user context?
  def restore_context(session)
    context = self.context || {}
    context.keys.each do |key|
      session[key] = context[key] unless key == 'path'
    end
    update_attribute(:context, {})
    context
  end

  def self.from_omniauth(auth)
    # Not to store provider and uid if user has already registered through normal form
    where(email: auth.info.email).first_or_initialize do |user|
      user.provider =  auth.provider
      user.uid = auth.uid
      user.password = Devise.friendly_token[0,20]
      user.registration_name = auth.info.name
    end
  end

end
