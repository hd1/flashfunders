class AchDebitForm
  extend ActiveModel::Naming
  include ActiveModel::Conversion
  include ActiveModel::Validations

  ATTRIBUTES = [:bank_account, :ip_address, :current_user_id, :investment_amount, :investment_id, :authorized_transfer]

  attr_reader *ATTRIBUTES
  attr_reader :params
  private :params

  validates :authorized_transfer, acceptance: true

  def initialize(params = {})
    ATTRIBUTES.each do |attr|
      instance_variable_set("@#{attr}", params[attr])
    end

    @params = params
  end

  def save
    return false unless valid?
    AchDebit.create(user_id: current_user_id, investment_id: investment_id, amount: investment_amount, status: AchDebit::PENDING)

    true
  end

  def persisted?
    false
  end

end

