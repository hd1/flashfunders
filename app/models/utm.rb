class Utm < ActiveRecord::Base
  has_many :client_trackers, foreign_key: 'client_id', primary_key: 'client_id', dependent: :destroy

  class << self
    def track!(client_id, utm_params, current_user, resource)
      tie_user(client_id, current_user)
      track_action(client_id, resource)
      store_utms(client_id, utm_params)
    end

    private

    def track_action(client_id, resource)
      return if resource.nil?
      thing = { client_id: client_id, trackable_id: resource.id, trackable_type: resource.class.to_s }
      ClientTracker.create(thing)
    end

    def store_utms(client_id, utm_params)
      if utm_params.present?
        utm_attrs = parse_utms(utm_params).merge(client_id: client_id)
        create(utm_attrs)
      end
    end

    def tie_user(client_id, user)
      if user
        tied_user = {client_id: client_id, trackable_id: user.id,  trackable_type: user.class.to_s }
        ClientTracker.create(tied_user)
      end
    end

    def parse_utms(utm_params)
      utm_attrs = {}
      utm_params.each { |key, value| utm_attrs[key.gsub(/^utm_/, '')] = value }
      utm_attrs['other'] = utm_attrs.slice!('source', 'medium', 'campaign', 'term', 'content')
      utm_attrs.select { |_, val| val.present? }
    end

  end
end
