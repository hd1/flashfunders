class ConfirmationForm < ResidencyForm
  delegate :errors, :valid?, to: :validator

  USER_ATTRIBUTES = [:confirmation_token, :email, :uuid, :registration_name]
  CONFIRM_ATTRIBUTES = [:password, :password_confirmation, :agreed_to_tos_ip, :agreed_to_tos_datetime, :agreed_to_tos]
  ATTRIBUTES = USER_ATTRIBUTES + CONFIRM_ATTRIBUTES + ResidencyForm::ATTRIBUTES

  attr_reader *ATTRIBUTES, :validator, :user, :attributes

  def initialize(attrs={}, user=nil, opts={})
    @attributes = attrs.to_hash.with_indifferent_access
    @user = user || User.find_for_authentication(email: @attributes[:email])
    opts[:validator_klass] ||= ConfirmationFormValidator
    super(attrs, @user, opts)
  end

  def save
    if validator.valid?
      # Create the user and pfii record
      @user ||= User.find_for_authentication(email: @email)
      if @user && @user.has_no_password?
        @user.assign_attributes(user_params)
      end
      @user ||= User.new(user_params)
      super
    else
      false
    end
  end

  def persisted?
    false
  end

  private

  def valid_params
    attributes.slice(*(ATTRIBUTES - [:confirmation_token, :uuid])).merge({}.tap do |h|
      if @user
        h[:email] = @user.email
        h[:registration_name] = @user.registration_name
      end
    end)
  end

  def user_params
    attributes.slice(*CONFIRM_ATTRIBUTES).merge(needs_password_set: false)
  end

  def pfii_params
    attributes.slice(*ResidencyForm::ATTRIBUTES)
  end
end