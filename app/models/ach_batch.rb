class AchBatch < ActiveRecord::Base

  has_one :escrow_provider
  has_many :ach_activities

  enum kind: {
    credit: 0,
    debit: 1
  }

end
