class ResidencyFormValidator
  include ActiveModel::Validations

  def initialize(attrs = {}, user=nil)
    @attributes = attrs
    @user = user

    @attributes.each do |attr, value|
      instance_variable_set("@#{attr}", value)
    end
  end

  def self.ensure_valid(attr, opts)
    validates attr, opts
    attr_reader attr
    private attr
  end

  validate :user_valid?

  private

  def user_valid?
    unless @user.present?
      errors.add(:user, 'No user provided')
    end
    errors.blank?
  end

end
