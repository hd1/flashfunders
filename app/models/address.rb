class Address < ActiveRecord::Base

  def phone_number=(phone)
    write_attribute :phone_number, phone.gsub(/\D/, '')
  end

end

