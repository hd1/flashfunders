class InvestmentPresenter
  include ActiveSupport::NumberHelper

  attr_reader :investment
  private :investment

  delegate :funding_method_selected?, :funds_sent?, :path_to_current_step, :pending_transfer?, :amount, :is_spv?,
           :offering_id, :id, :funding_method, :escrowed?, :counter_signed?, :stock_received?, :offering,
           :has_international_funding_detail?, :created_at, :docs_signed_at, :escrowed_at, :updated_at,
           :is_reg_s?, :is_reg_c?, :is_reg_d?, :security,
           to: :investment

  delegate :escrow_provider,
           to: :security

  delegate :wire_code, :provider_key,
           to: :escrow_provider

  def initialize(investment)
    @investment = investment
  end

  def funding_detail_domestic
    TransferFundsPresenter.new(investment, 'domestic')
  end

  def funding_detail_international
    return nil unless has_international_funding_detail?
    TransferFundsPresenter.new(investment, 'international')
  end

  def formatted_amount
    number_to_currency(investment.amount, precision: 2)
  end

  def formatted_updated_at
    investment.updated_at.strftime('%m/%d')
  end

  def investor_name
    investment.user.registration_name
  end

  def investor_email
    investment.user.email
  end

  def payment_type
    is_reg_d? ? 'reg_d_' + investment.funding_method : investment.funding_method
  end

  def user_id
    investment.user.id
  end

  def user_uuid
    investment.user.uuid
  end

  def investor_messages
    OfferingMessage.where(offering_id: investment.offering_id, user_id: investment.user_id)
  end

  def followed_date
    OfferingFollower.where.not(mode: nil)
      .find_by(offering_id: investment.offering_id, user_id: investment.user_id)
      .try(:updated_at)
  end

  def shares_owned
    number_to_delimited(investment.shares)
  end

  def formatted_percent_ownership
    number_to_percentage(investment.percent_ownership)
  end

  def investor_account_state
    verbiage_per_answer true
  end

  def accreditation_state
    if investment.has_verified_accreditation?
      investment.verified? ? 'complete' : 'pending'
    else
      'incomplete'
    end
  end

  def investment_display_state
    if investment.pending_transfer?
      'pending'
    elsif investment.complete?
      investment.offering.unsuccessful? ?  'unsuccessful' : 'complete'
    elsif investment.cancelled?
      'cancelled'
    elsif investment.refunded?
      'refunded'
    elsif investment.ach_debit_failed?
      'ach_failed'
    else
      'incomplete'
    end
  end

  def current_state_timestamp
    InvestmentEvent.where(investment_id: investment.id).order(:updated_at).last
      .updated_at.strftime('%F')
  end

  def accreditor_data
    return @accreditor_data if @accreditor_data

    qualification = investment.current_accreditor_qualification

    @accreditor_data = {}.tap do |hash|
      hash[:state] = qualification_state(qualification)
      hash[:method] = (qualification.class.to_s.demodulize.gsub("Qualification", '')).titleize

      case qualification
        when NilClass
          # Do nothing
        when Accreditor::OfflineEntityQualification
          hash[:type] = :offline
        when Accreditor::ThirdPartyQualification
          hash[:type] = :third_party
          hash[:third_party] = true
          hash[:role] = qualification.role
          hash[:email] = qualification.email
        when Accreditor::InternationalQualification
          hash[:type] = :international
        else
          hash[:type] = :documents
          hash[:docs] = qualification.documents.map { |doc| {name: File.basename(doc.document.filename), url: doc.document.url} }
      end
    end
  end

  def can_download_docs?
    return true if investment.envelope_id.present? && has_signed_docs?
    false
  end

  def can_cancel_investment?
    is_reg_c? && (investment_display_state == 'pending' || escrowed?) && !cooling_off_ended?
  end

  def cooling_off_ended?
    investment.offering.ends_at&. < 48.hours.from_now
  end

  def has_signed_docs?
    %w(signed_documents funding_method_selected funds_sent escrowed counter_signed stock_received).include? investment.state
  end

  def investment_id
    investment.id
  end

  def offering_closed?
    investment.offering.escrow_closed?
  end

  def display_accreditation_verification_panel?
    !investment.investor_entity_id.nil? && accreditation_state == 'pending' && !investment.current_accreditor_span.nil? && investment.pending_transfer? && investment.current_accreditor_span.accreditor_qualification.present? && !investment.is_reg_c?
  end

  def display_payment_panel?
    !investment.offering.escrow_closed? && investment.pending_transfer? && investment.security.escrow_provider.requires_offering_funding_details? && investment.security.escrow_provider.show_payment_details?
  end

  def can_edit_verification?
    accreditor_data[:state] != "verified" && !investment.is_reg_s?
  end

  private

  def qualification_state(qualification)
    return "" if qualification.nil?

    return "expired" if qualification.expires_at.present? && qualification.expires_at <= Date.today
    return "declined" if qualification.declined_at.present?
    return "verified" if qualification.verified_at.present?

    if qualification.is_a? Accreditor::ThirdPartyQualification
      return "pending" if qualification.emailed_at.present?
      return ""
    end
    "pending"
  end


end
