class RegistrationFormAuditor < Auditor
  def process_state(persisted_state)
    persisted_state.stringify_keys.tap do |hash|
      remove_password_and_confirmation!(hash)
    end
  end

  private

  def remove_password_and_confirmation!(hash)
    hash.delete('password') if hash.has_key?('password')
    hash.delete('password_confirmation') if hash.has_key?('password_confirmation')
  end
end