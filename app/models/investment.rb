class Investment < ActiveRecord::Base
  extend StoreAccessorBoolean::ActiveRecordExtensions
  include UniquelyIdentifiable

  belongs_to :investor_entity
  belongs_to :security, class_name: 'Securities::Security'
  belongs_to :offering
  belongs_to :user

  enum accreditation_status_id: { pending: 1, verified: 2, rejected: 3 }
  enum funding_method: {
           wire:        0,
           check:       1,
           manual:      2,
           ach:         3,
       }

  serialize :cached_envelope_details

  store_accessor :calc_data,
                 :reg_c_income,
                 :reg_c_net_worth,
                 :reg_c_external,
                 :reg_c_calc_output,
                 :reg_selector

  store_accessor_boolean :calc_data,
                         :reg_s_investor

  ACH_CUTOFF = 1000
  ACH_AND_WIRE_CUTOFF = 20000
  complete_state_list = %w(escrowed counter_signed stock_received)
  scope :open_investments, ->(user_id) {
    joins(:offering)
    .where(['investments.user_id = ?', user_id])
    .where('((offerings.escrow_end_date >= ? OR offerings.escrow_end_date IS NULL) AND (offerings.closed_status != ?)) OR investments.state IN (?)', Date.current, 2, complete_state_list)
    .order('created_at DESC')
  }
  scope :investment_order, ->(user_id) {
    open_investments(user_id).sort_by do |i|
      [
        i.incomplete_transfer_state? ? 0:1,
        i.pending_transfer? ? 0:1,
        i.complete? ? 0:1,
        i.unsuccessful? ? 0:1,
        i.cancelled? ? 0:1
      ]
    end
  }

  def by_individual?
    investor_entity.is_a?(InvestorEntity::Individual)
  end

  def is_reg_s?
     reg_s_investor? || (is_reg_d? && investor_entity&.is_international?)
  end

  def is_reg_c?
    security.reg_cf?
  end

  def is_reg_d?
    security.reg_d?
  end

  def agreement_url
    if Rails.env.production?
      "https://flashfunders.com/investment_agreement/#{uuid}"
    elsif Rails.env.review?
      "https://flashdough.com/investment_agreement/#{uuid}"
    else
      "http://localhost:3000/investment_agreement/#{uuid}"
    end
  end

  state_machine :state, initial: :started do

    after_transition do |investment, transition|
      investment.event_fired transition.from, transition.to, transition.event
    end

    after_transition on: :sign_documents, do: :trigger_investor_verification
    after_transition on: :verify_accreditation, do: :regenerate_accreditations

    before_transition to: :intended, do: :check_incomplete_investment_process

    event :begin do
      transition started: :intended
    end

    event :select_entity do
      transition intended: :entity_selected
    end

    event :verify_accreditation do
      transition entity_selected: :accreditation_verified
    end

    event :sign_documents do
      transition [:accreditation_verified, :declined_documents] => :signed_documents
    end

    event :decline_documents do
      transition accreditation_verified: :declined_documents
    end

    event :select_funding_method do
      transition [:signed_documents, :funds_sent, :ach_debit_failed] => :funding_method_selected
    end

    event :transfer_funds do
      transition [:funding_method_selected, :funds_sent] => :escrowed
    end

    event :cancel do
      transition [:funding_method_selected, :funds_sent, :escrowed] => :cancelled
    end

    event :ach_debit_failure do
      transition [:funding_method_selected, :funds_sent] => :ach_debit_failed
    end

    event :counter_sign do
      transition escrowed: :counter_signed
    end

    event :receive_stock do
      transition counter_signed: :stock_received
    end

    event :send_funds do
      transition funding_method_selected: :funds_sent
    end

    event :refund do
      transition escrowed: :refunded
    end

    # New Investment Workflow Edit transitions
    event :edit_amount do
      transition [:intended, :entity_selected, :accreditation_verified] => :intended
    end

    event :edit_profile do
      transition [:declined_documents, :entity_selected, :accreditation_verified] => :entity_selected
    end

    event :edit_verify do
      transition [:accreditation_verified] => :accreditation_verified
    end

  end

  def has_selected_entity?
    InvestmentEvent::SelectEntityEvent.where(investment_id: id).present?
  end

  def has_verified_accreditation?
    InvestmentEvent.where(type: InvestmentEvent::VerifyAccreditationEvent, investment_id: id).present?
  end

  def has_signed_documents?
    InvestmentEvent.where(type: InvestmentEvent::SignDocumentsEvent, investment_id: id).present?
  end

  def complete?
    escrowed? || counter_signed? || stock_received?
  end

  def is_spv?
    security.is_spv?
  end

  def is_direct?
    !is_spv?
  end

  def unsuccessful?
    !pending_transfer? && !complete? && offering.unsuccessful?
  end

  def path_to_current_step
    InvestmentPathMapper.new(self).target_for
  end

  def pending_transfer?
    funding_method_selected? || funds_sent?
  end

  def resumable?
    !(escrowed? || refunded? || cancelled? || pending_transfer? || (offering && offering.escrow_closed?))
  end

  def live?
    !(started? || refunded? || cancelled?)
  end

  def check_incomplete_investment_process
    InvestmentMailer.delay_for(90.minutes).reminds_to_complete(self.id)
  end

  def accreditation
    EntityAccreditation.new(investor_entity)
  end

  def current_accreditor_span
    end_target = offering.escrow_end_date.present? ? offering.escrow_end_date : offering.ends_at
    investor_entity.current_accreditor_span end_target
  end

  def current_accreditor_qualification
    current_accreditor_span.accreditor_qualification
  end

  def event_fired(current_state, new_state, event)
    "InvestmentEvent::#{event.to_s.camelcase}Event".constantize.create! investment_id: self.id
  end

  def resend_verification_email
    email_third_party current_accreditor_qualification
  end

  def trigger_investor_verification
    return if is_reg_c?
    qualification = current_accreditor_qualification
    if qualification.is_a?(Accreditor::InternationalQualification)
      verify_international_investor(qualification)
    else
      email_third_party(qualification)
    end
  end

  def has_international_funding_detail?
    question = 'requires_instructions_international_'
    question += is_spv? ? 'spv_' : 'direct_'
    question += funding_method == 'wire' ? 'wire?' : 'check?'
    security.escrow_provider.send(question.to_sym)
  end

  def download_docs_stream
    return if envelope_id.nil?
    
    old_logger = DocusignRest.logger
    DocusignRest.logger = Logger.new("/dev/null")
    client = DocusignRest::Client.new

    docs_arr = []
    docs_arr << get_docusign_document(client, envelope_id)
    if security.docusign_issuer_countersign_omnibus_id && (counter_signed? || stock_received?)
      docs_arr << get_docusign_document(client, security.docusign_issuer_countersign_omnibus_id)
    end

    DocusignRest.logger = old_logger

    combine_pdfs(docs_arr)
  end

  def incomplete_transfer_state?
    !pending_transfer? && !complete?
  end

  def closed_campaign?
    offering.escrow_closed? && !offering.successful?
  end

  def regenerate_accreditations
    if is_reg_s?
      Accreditor::InternationalQualification.create!(signature_date: Date.today, user_id: user.id, investor_entity_id: investor_entity.id)
    end
    ::Accreditation::StatusUpdater.new.update_investment self
  end

  def funding_options
    unless security.escrow_provider.any_funding_instructions?
      return ['manual']
    end

    [].tap do |arr|
      if is_reg_s?
        arr << 'wire'
      else
        if amount < ACH_CUTOFF
          arr << 'ach'
        elsif amount < ACH_AND_WIRE_CUTOFF
          arr << 'ach'
          arr << 'wire'
        else
          arr << 'wire'
        end
      end
    end
  end

  private

  def verify_international_investor(qualification)
    qualification.update_attributes(verified_at: Time.now,
                                    verified_by: 'self',
                                    expires_at:  1.year.from_now)
    Accreditation::StatusUpdater.new.update_investment self
  end

  def email_third_party(qualification)
    if qualification.is_a?(Accreditor::ThirdPartyIncomeQualification) || qualification.is_a?(Accreditor::ThirdPartyNetWorthQualification)
      AccreditationMailer
          .delay
          .third_party_person_verification_email(
              qualification,
              investor_name:  investor_entity.full_name,
              investor_email: user.email)
    elsif qualification.is_a?(Accreditor::ThirdPartyEntityQualification)
      AccreditationMailer
          .delay
          .third_party_entity_verification_email(
              qualification,
              investor_name:  investor_entity.full_name,
              investor_email: user.email,
              entity_name:    investor_entity.name)
    end
  end

  def get_docusign_document(client, envelope_id)
    return nil if envelope_id.blank?
    temp_file = Tempfile.new(%w(docusign .pdf))
    response = client.get_combined_document_from_envelope({envelope_id: envelope_id, return_stream: true})
    File.open(temp_file, 'wb') do |output|
      output << response
    end
    temp_file
  end

  def combine_pdfs(files)
    output_pdf = CombinePDF.new
    files.each do |f|
      output_pdf << CombinePDF.load(f.path, allow_optional_content: true)
      File.delete(f.path)
    end
    output_pdf.to_pdf
  end
end
