class LinkedInPresenter

  def initialize(url, name, tagline)
    @url = url
    @name = name 
    @tagline = tagline
  end

  def offering_url
    uri = URI('https://linkedin.com/shareArticle')
    # At some point, this became buggy and moving mini=true to the last param fixed it.
    uri.query = URI.encode_www_form([['url', @url],
                                     ['title', @name],
                                     ['summary', @tagline],
                                     ['mini', true]])
    uri.to_s
  end

end
