require 'entity_form_validator'

class GenericEntityForm
  extend ActiveModel::Naming
  include ActiveModel::Conversion

  def self.remove_signatory(attr)
    attr.to_s.gsub(/signatory_/, '').to_sym
  end

  def self.add_signatory(attr)
    attr =~ /^signatory/ ? attr : "signatory_#{attr}".to_sym
  end

  SIGNATORY_ATTRS = PersonalFederalIdentificationInformation::EDIT_ATTRIBUTES.map { |a| add_signatory(a) }

  MASK_ATTRS = { signatory_date_of_birth: 0, signatory_ssn: 4, signatory_id_number: 2 }

  CORE_ATTRS = [:high_risk_agree, :experience_agree, :loss_risk_agree, :time_horizon_agree, :cancel_agree, :resell_agree]

  attr_reader :user, :validator, :entity_class, :attributes, :entity
  private :user, :validator, :entity_class

  delegate :errors, to: :validator

  def self.form_reader(*attrs)
    @form_readers ||= []
    @form_readers.concat attrs
    attr_reader *attrs
  end

  def self.form_readers
    @form_readers || []
  end

  def self.key
    self.to_s.demodulize.underscore
  end

  def self.slice_params(params)
    params ? params.slice(*form_readers).to_hash : {}
  end

  def self.default_entity_class
    "InvestorEntity::#{self.to_s[0..-5]}".constantize
  end

  def self.default_validator_class
    "#{self.to_s}Validator".constantize
  end

  def initialize(attrs, user, options={})
    @entity = options[:entity]
    @investment = options.delete(:investment)
    @attributes = self.class.slice_params(attrs)
    @attributes.each { |attr, val| @attributes[attr] = get_unmasked_val(attr.to_sym, val) }
    @user = user
    @validator = options[:validator] || self.class.default_validator_class.new(@attributes, @investment)

    assign_attributes
    assign_instance_vars(options)

  end

  def assign_instance_vars(options)
    default_params(options[:defaults]).merge(@attributes).each do |attr, val|
      instance_variable_set("@#{attr}", val)
    end
  end

  def assign_attributes
    if @attributes.empty? && @entity
      @attributes = self.class.slice_params(@entity.attributes.merge(signatory_params).symbolize_keys).merge(core_params)
    end
  end

  def signatory_params
    {}.tap do |h|
      SIGNATORY_ATTRS.each do |attr|
        h[attr] = @entity.personal_federal_identification_information.send(self.class.remove_signatory(attr))
      end
    end
  end

  def clear_core_params
    CORE_ATTRS.each do |attr|
      instance_variable_set("@#{attr}", '0')
    end
  end

  def core_params
    {}.tap do |h|
      CORE_ATTRS.each do |attr|
        h[attr] = @entity.send(attr)
      end
    end
  end

  def save
    return false unless validator.valid?

    matching_entity = find_existing_entity
    if matching_entity
      @entity = matching_entity
      @pfii = @entity.personal_federal_identification_information
    else
      create_new_entity_and_pfii
    end
    true
  end

  def signatory_date_of_birth
    should_mask?(:signatory_date_of_birth) ? mask_date(@signatory_date_of_birth) : @signatory_date_of_birth
  end

  def signatory_id_number
    should_mask?(:signatory_id_number) ? mask_number(@signatory_id_number, MASK_ATTRS[:signatory_id_number]) : @signatory_id_number
  end

  def signatory_ssn
    should_mask?(:signatory_ssn) ? mask_number(@signatory_ssn, MASK_ATTRS[:signatory_ssn]) : @signatory_ssn
  end

  def mask_number(value, number_to_show)
    return value if value.nil?

    number_to_show = [value.size, number_to_show].min
    '*' * (value.size - number_to_show) + value[-number_to_show, number_to_show]
  end

  def mask_date(date)
    if date.is_a?(Date)
      I18n.localize(date, { format: '%m/**/%Y' })
    else
      date
    end
  end

  private

  def entity_class
    @entity.nil? ? self.class.default_entity_class : @entity.class
  end

  def should_mask?(attr)
    @entity.present? && errors[attr].blank?
  end

  def mask_attrs
    MASK_ATTRS
  end

  def get_unmasked_val(attr, val)
    return val unless mask_attrs.keys.include?(attr) && @entity.present? && @entity.personal_federal_identification_information.present?

    orig_val = database_value(attr)
    new_masked_val = orig_val.is_a?(Date) ? mask_date(orig_val) : mask_number(orig_val, mask_attrs[attr])

    if val == new_masked_val
      orig_val.is_a?(Date) ? orig_val.strftime("%m/%d/%Y") : orig_val
    else
      val
    end
  end

  def entity_params
    attributes.dup.symbolize_keys.slice(*entity_attrs)
  end

  def database_value(attr)
    if SIGNATORY_ATTRS.include?(attr)
      @entity.personal_federal_identification_information.send(self.class.remove_signatory(attr))
    else
      @entity.send(attr)
    end
  end

  def find_existing_entity
    entity_class.where(user_id: user.id).order(:updated_at).detect do |existing_entity|
      is_existing?(existing_entity)
    end
  end

  def is_existing?(entity)
    entity_equal?(entity) && pfii_equal?(entity.personal_federal_identification_information)
  end

  def pfii_equal?(pfii)
    pfii.equals_to(pfii_params)
  end

  def entity_equal?(entity)
    compare_attrs(entity) == compare_attrs(InvestorEntity.new(entity_params))
  end

  def compare_attrs(entity)
    entity.attributes.symbolize_keys.slice(*entity_attrs)
  end

  def create_new_entity_and_pfii
    ActiveRecord::Base.transaction do
      args = entity_params.merge({ 'user_id' => user.id })
      @entity = create_new_entity(args)
      defaults = { country: 'US', us_citizen: true }
      args = defaults.merge(pfii_params.merge({ investor_entity_id: @entity.id }))
      @pfii = PersonalFederalIdentificationInformation.create(args)
    end
  end

  def create_new_entity args
    case @form_type
      when 'trust'
        InvestorEntity::Trust.create!(args)
      when 'llc'
        InvestorEntity::LLC.create!(args)
      when 'partnership'
        InvestorEntity::Partnership.create!(args)
      when 'corporation'
        InvestorEntity::Corporation.create!(args)
      else
        InvestorEntity::Individual.create!(args)
    end
  end

  def persisted?
    false
  end

  def default_params(defaults)
    overlay = { signatory_first_name: user.first_name,
                signatory_last_name: user.last_name }.tap do |h|
      h[:signatory_us_citizen] = user.us_citizen unless user.us_citizen.nil?
      h[:signatory_country] = user.country unless user.country.nil?
    end
    (defaults || {}).merge(overlay)
  end

end
