class InvestmentFlowForm
  delegate :errors, :valid?, to: :validator

  ALPHA_ATTRIBUTES = [:selector, :international, :reg_c_output]
  ATTRIBUTES = [:amount, :income, :net_worth, :external, :internal, :reg_c_cutoff, :reg_c_min, :reg_s_min, :server_internal, :server_reg_c_cutoff] + ALPHA_ATTRIBUTES

  attr_accessor *ATTRIBUTES
  attr_reader :validator, :attributes

  def initialize(attrs={}, offering=nil, investment=nil)
    @attributes = attrs.to_hash.with_indifferent_access
    @validator = InvestmentFlowFormValidator.new(valid_params, offering)
    @investment = investment
    @offering = offering

    @attributes.each do |attr, val|
      instance_variable_set("@#{attr}", val)
    end
  end

  def persisted?
    false
  end

  def selector
    @offering.reg_c_only? ? 'reg-c' : @selector
  end

  def create_or_update_investment(user_id)
    investment_calc = InvestorInvestmentCalculator.get_calc(@amount, OfferingPresenter.new(@offering))
    is_new_investment = !@investment.persisted?
    @investment.update_attributes(
      user_id: user_id,
      offering_id: @offering.id,
      amount: investment_calc.projected_cost,
      shares: investment_calc.projected_shares,
      percent_ownership: investment_calc.projected_percent_ownership,
      security_id: security_id,
      reg_selector: @selector,
      reg_s_investor: @international,
      reg_c_income: @income,
      reg_c_net_worth: @net_worth,
      reg_c_external: @external,
      reg_c_calc_output: @reg_c_output.blank? ? nil : JSON.parse(@reg_c_output)
    )
    if is_new_investment
      @investment.begin!
    else
      @investment.edit_amount! if @investment.can_edit_amount?
    end
  end

  private

  def security_id
    @selector == 'reg-c' ? @offering.reg_c_security.id : @offering.reg_d_direct_security.id
  end

  def valid_params
    attributes.slice(*ATTRIBUTES)
  end

end
