module SecurityPresenters
  class Security
    include ActiveSupport::NumberHelper

    attr_accessor :security
    private :security

    def initialize(security)
      @security = security
    end

    delegate :id, :shares_offered, :is_spv?, :reg_d?, :reg_cf?,
             :minimum_total_investment, :minimum_total_investment_display,
             :maximum_total_investment, :maximum_total_investment_display,
             :minimum_investment_amount, :minimum_investment_amount_display,
             :security_title, :security_title_plural, :deal_documents,
             :success_fee, :processing_fee, :counter_signed_preamble,
             to: :security

    def section_title
      title = "Reg #{security.regulation.split('_').last.upcase} Offering Summary"
      security.is_spv? ? "#{title} (SPV)" : title
    end

    def yaml_key
      'show_campaign.offering_information'
    end

    def security_title
      key = "#{yaml_key}.#{self.class.to_s.demodulize.underscore}.security_offered"

      I18n.exists?(key) ? I18n.t(key) : security.security_title
    end

    def security_subtitle
      key = "#{yaml_key}.#{security.regulation}_subtitle"

      I18n.exists?(key) ? I18n.t(key) : ""
    end

    def min_popover
      if unpresentable_minimum? && security.reg_d?
        I18n.t("#{yaml_key}.offering_min.reg_d_no_min_popover_body")
      else
        I18n.t("#{yaml_key}.offering_min.#{security.regulation}_popover_body", offering_min: minimum_total_investment_display)
      end
    end

    def max_popover
      I18n.t("#{yaml_key}.offering_max.#{security.regulation}_popover_body", security_title: security_title)
    end

    def option_pool_popover
      I18n.t("#{yaml_key}.option_pool_popover", maximum_total_investment_display: security.maximum_total_investment_display)
    end

    def convertible?
      false
    end

    def revenue_note?
      false
    end

    def funded?
      minimum_total_investment <= accredited_balance && accredited_balance > 0
    end

    def accredited_balance
      investments.sum(:amount).to_f + security.external_investment_amount.to_f
    end

    def shares_sold
      investments.sum(:shares).to_i
    end

    def number_of_investors
      investments.count + security.external_investor_count.to_i
    end

    def fees
      {}.tap do |h|
        h[:success] = success_fee.to_f if success_fee.to_f > 0
        h[:processing] = processing_fee.to_f if processing_fee.to_f > 0
      end
    end

    private

    def unpresentable_minimum?
      minimum_total_investment.blank? || minimum_total_investment == 0
    end

    def investments
      @investments ||= OfferingInvestments.new(security.offering).committed_security(security.id)
    end

  end
end
