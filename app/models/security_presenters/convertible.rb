module SecurityPresenters
  class Convertible < Security

    delegate :valuation_cap,
             :valuation_cap_display,
             :discount_rate,
             to: :security

    def valuation_cap_display
      return security.valuation_cap_display unless security.valuation_cap_display.nil?
      number_to_currency(security.valuation_cap, precision: 0)
    end

    def option_pool_popover
      I18n.t('show_campaign.offering_information.option_pool_popover_convertible')
    end

    def convertible?
      true
    end
  end
end
