class SocialPresenterFactory
  def self.build(url, via, company_name, campaign_tagline)
    OpenStruct.new(
        facebook: FacebookPresenter.new(url),
        twitter: TwitterPresenter.new(url, via, company_name, campaign_tagline),
        linkedin: LinkedInPresenter.new(url, company_name, campaign_tagline),
    )
  end
end
