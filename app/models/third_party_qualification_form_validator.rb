class ThirdPartyQualificationFormValidator
  include ActiveModel::Validations

  def initialize(attrs = {})
    @attributes = attrs

    @attributes.each do |attr, value|
      instance_variable_set("@#{attr}", value)
    end
  end

  def self.ensure_valid(attr, opts)
    validates attr, opts
    attr_reader attr
    private attr
  end

  ensure_valid :name, presence: true
  ensure_valid :role, presence: true
  ensure_valid :company_name, presence: true
  ensure_valid :license_state, presence: true, inclusion: Accreditor::ThirdPartyQualification::STATES.keys
  ensure_valid :license_number, presence: true
  ensure_valid :phone_number, presence: true, us_phone_number_format: true
  ensure_valid :signature, presence: true


  validate :signature_matches_name

  attr_reader :verified_by_assets, :verified_by_accredited
  validate :verification_criteria_selected

  private

  def verification_criteria_selected
    case @investor_entity
    when NilClass, InvestorEntity::LLC, InvestorEntity::Partnership, InvestorEntity::Corporation, InvestorEntity::Trust
      unless verified_by_assets == '1' || verified_by_accredited == '1'
        errors.add(:verified_by_assets, 'Select One')
      end
    end
  end

  def signature_matches_name
    return if signature.nil? || name.nil?

    if signature.strip.downcase != name.strip.downcase
      @errors.add(:signature, 'does not match name above')
    end
  end
end

