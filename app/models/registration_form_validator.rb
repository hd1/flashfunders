class RegistrationFormValidator < ConfirmationFormValidator

  ensure_valid :agreed_to_tos, acceptance: true
  ensure_valid :registration_name, presence: true, person_name: true, length: { maximum: 90, message: 'Maximum of 90 characters.' }

  private

  def lookup_user
    user = User.find_for_authentication(email: @email)
    user if user && user.has_no_password?
  end

end
