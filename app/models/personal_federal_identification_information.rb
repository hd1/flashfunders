class PersonalFederalIdentificationInformation < ActiveRecord::Base

  EDIT_ATTRIBUTES = [:first_name, :last_name, :middle_initial, :date_of_birth, :ssn,
                     :address1, :address2, :city, :zip_code, :country, :daytime_phone, :mobile_phone, :state_id,
                     :id_type, :id_number, :location, :issued_date, :expiration_date, :employment_status,
                     :employment_role, :employer_name, :employer_industry, :us_citizen]

  enum id_type: { "Driver's License" => 0, 'Passport' => 1, 'State ID' => 2, 'Other Government-Issued ID' => 3 }

  store_accessor :employment_data,
                 :employment_status, :employment_role, :employer_name,
                 :employer_industry

  attr_encrypted :ssn, key: Rails.application.config.encryption.ssn_key

  belongs_to :investor_entity
  belongs_to :user

  before_save :set_country
  before_save :sanitize_ssn
  before_save :sanitize_daytime_phone
  before_save :sanitize_mobile_phone

  mount_uploader :passport_image, PassportUploader

  def state_name
    if country && Carmen::Country.coded(country).subregions.count > 0
      State.find(state_id, country).name
    else
      ""
    end
  end

  def country_name
    carmen_country = State.country(country)
    carmen_country.name if carmen_country
  end

  def international?
    country != State.default_country && us_citizen == false
  end

  def passport_number
    id_number if Passport?
  end

  def phone_number
    daytime_phone.present? ? daytime_phone : mobile_phone
  end

  def ssn_last_four
    SanitizeToLastFourDigits.sanitize(ssn) if ssn.present?
  end

  def full_name
    middle = "#{middle_initial} " if middle_initial.present?
    "#{first_name} #{middle}#{last_name}"
  end

  def equals_to(pfii_params)
    pfii_equal?(compare_attrs(self), compare_attrs(PersonalFederalIdentificationInformation.new(pfii_params)))
  end

  def employer_industry
    return (super == 'true') if %w{true false}.include? super
    super
  end

  def age
    if date_of_birth.present?
      now = Date.today
      age = now.year - date_of_birth.year
      if date_of_birth.month > now.month || (date_of_birth.month == now.month && date_of_birth.day > now.day)
        # Birthday still coming up for this year?
        age -= 1
      end
      age
    end
  end

  private

  def pfii_equal?(old_attrs, new_attrs)
    old_attrs.each do |key, val|
      return false unless compare_attr(key, val, new_attrs[key])
    end
    true
  end

  def compare_attr(attr, old_val, new_val)
    case attr
      when :ssn
        new_val == new_val.gsub(/\D/, '') if new_val
      when :daytime_phone, :mobile_phone
        new_val = sanitize_phone(new_val)
    end
    old_val == new_val || (old_val.nil? && new_val == '') || (old_val == '' && new_val.nil?)
  end

  def compare_attrs(pfii)
    pfii.attributes.symbolize_keys.slice(*EDIT_ATTRIBUTES)
  end

  def set_country
    self.country ||= State.default_country
  end

  def sanitize_ssn
    self.ssn = ssn.gsub(/\D/, '') if ssn
  end

  def sanitize_daytime_phone
    self.daytime_phone = sanitize_phone(daytime_phone)
  end

  def sanitize_mobile_phone
    self.mobile_phone = sanitize_phone(mobile_phone)
  end

  def sanitize_phone(number)
    return if number.blank?
    return number if country != State.default_country
    match = /^1?(\d{10})$/.match(number.gsub(/[\W_]/, ''))
    return number if match.nil?
    match.captures.join
  end
end
