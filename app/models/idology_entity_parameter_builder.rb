class IdologyEntityParameterBuilder
  class << self
    def build(data)
      domestic_individual_entity(data)
    end

    private

    def domestic_individual_entity(entity)
      pfii = entity.personal_federal_identification_information
      {
        firstName: pfii.first_name,
        lastName: pfii.last_name,
        address: pfii.address1 + pfii.address2,
        city: pfii.city,
        state: pfii.state_id,
        zip: pfii.zip_code,
        dobMonth: pfii.date_of_birth.month,
        dobYear: pfii.date_of_birth.year,
        telephone: pfii.phone_number,
        ssn: pfii.ssn
      }
    end
  end
end
