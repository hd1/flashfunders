class FundAmericaInvestmentParameterBuilder
  def self.build(offering, investment)
    hash = {
      id: investment.fund_america_id,
      amount: sprintf('%.2f', investment.amount),
      equity_share_price: offering.share_price.present? ? sprintf('%.2f', offering.share_price) : nil,
      offering_id: investment.security.fund_america_id,
      equity_share_count: investment.shares,
      payment_method: investment.funding_method,
      subscription_agreement_url: investment.agreement_url,
      entity: investment.by_individual? ? entity_params_for_person(investment) : entity_params_for_company(investment),
    }
    hash[:equity_share_count] = [1, investment.shares.to_i].max
    hash
  end

  def self.entity_params_for_person(investment)
    entity = investment.investor_entity
    pfii = investment.investor_entity.personal_federal_identification_information
    {
      id: entity.fund_america_id,
      type: 'person',
      city: pfii.city,
      country: pfii.country,
      date_of_birth: pfii.date_of_birth.to_s('%Y'),
      email: investment.user.email,
      name: entity.full_name,
      phone: pfii.phone_number,
      postal_code: pfii.zip_code,
      region: pfii.state_id,
      street_address_1: pfii.address1,
      tax_id_number: pfii.ssn,
    }
  end

  def self.entity_params_for_company(investment)
    entity = investment.investor_entity
    {
      type: 'company',
      city: entity.city,
      # There is no interface to specify country for investors. It's usually
      # nil. In the case of international investors, set this field manually
      # (e.g. in the console)
      country: entity.country || State.default_country,
      email: investment.user.email,
      contact_name: entity.full_name,
      name: entity.name,
      phone: entity.phone_number,
      postal_code: entity.zip_code,
      region: entity.state_id,
      street_address_1: entity.address_1,
      tax_id_number: entity.tax_id_number,
      executive_name: entity.executive_name,
      state_formed_in: entity.formation_state_id
    }
  end

  def self.ach_authorization_params(investment, fund_america_entity, controller_params)
    bank = Bank::InvestorAccount.find(investment.investor_account_id)
    entity = investment.investor_entity
    {
      id: entity.fund_america_ach_authorization_id,
      account_number: bank.account_number,
      account_type: bank.account_type.downcase,
      name_on_account: bank.account_holder,
      routing_number: bank.routing_number,
      entity_id: fund_america_entity.id,
      check_type: 'personal',
      email: investment.user.email,
      literal: investment.user.full_name,
      ip_address: controller_params[:ip_address],
      user_agent: controller_params[:user_agent]
    }
  end
end
