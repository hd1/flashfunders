class GenericEntityFormValidator
  include ActiveModel::Validations

  def initialize(attrs = {}, investment)
    @attributes = attrs
    @investment = investment

    @attributes.each do |attr, value|
      instance_variable_set("@#{attr}", value)
    end
  end

  def self.ensure_valid(attr, opts)
    validates attr, opts
    attr_reader attr
    private attr
  end

  attr_accessor :high_risk_agree, :experience_agree, :loss_risk_agree, :time_horizon_agree, :cancel_agree, :resell_agree

  validates_inclusion_of :high_risk_agree, in: ['1'], message: I18n.t('investment_profile.new.suitability.errorset.required')
  validates_inclusion_of :experience_agree, in: ['1'], message: I18n.t('investment_profile.new.suitability.errorset.required')
  validates_inclusion_of :loss_risk_agree, in: ['1'], message: I18n.t('investment_profile.new.suitability.errorset.required')
  validates_inclusion_of :time_horizon_agree, in: ['1'], message: I18n.t('investment_profile.new.suitability.errorset.required')
  validate :cancel_agree_if_reg_c
  validate :resell_agree_if_reg_c

  private

  def cancel_agree_if_reg_c
    return unless @investment && @investment.is_reg_c?
    unless cancel_agree == '1'
      errors[:cancel_agree] << I18n.t('investment_profile.new.suitability.errorset.required')
    end
  end

  def resell_agree_if_reg_c
    return unless @investment && @investment.is_reg_c?
    unless resell_agree == '1'
      errors[:resell_agree] << I18n.t('investment_profile.new.suitability.errorset.required')
    end
  end

end
