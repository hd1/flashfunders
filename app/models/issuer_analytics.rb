require 'google/api_client/client_secrets'
require 'google/apis/analytics_v3'

class IssuerAnalytics
  attr_reader :client

  Analytics = Google::Apis::AnalyticsV3

  def initialize
    scopes                = [Analytics::AUTH_ANALYTICS]
    @client               = Analytics::AnalyticsService.new
    @client.authorization = Google::Auth.get_application_default(scopes)
  end

  def page_visit_info(start_date, end_date, offering, opts={})
    metrics    = 'ga:pageviews,ga:uniquePageviews,ga:avgTimeOnPage'
    dimensions = page_visit_info_dimensions(opts[:frequency])
    data       = @client.get_ga_data(profile_id, start_date, end_date, metrics,
                                     dimensions: dimensions,
                                     filters:    filters(offering),
                                     sort:       page_visit_info_sort(opts[:frequency]))
    page_visit_data(data, dimensions.split(',').count)
  end

  def referral_info(start_date, end_date, offering, opts={})
    metrics    = 'ga:pageviews'
    dimensions = 'ga:source'
    data       = @client.get_ga_data(profile_id, start_date, end_date, metrics,
                                     dimensions:         dimensions,
                                     filters:            filters(offering),
                                     include_empty_rows: false,
                                     sort:               '-ga:pageviews')
    referral_data(data)
  end

  private

  def page_visit_data(data, num_dimensions)
    return {} unless data.total_results > 0

    { x_series: build_x_series(data, num_dimensions),
      y_series: build_y_series(data, num_dimensions),
      totals:   data.totals_for_all_results
    }
  end

  def build_x_series(data, num_dimensions)
    data.rows.map { |row| row.slice(0, num_dimensions) }
  end

  def build_y_series(data, num_dimensions)
    Hash.new.tap do |h|
      data.column_headers.each_with_index do |header, i|
        h[header.name] = data.rows.map { |row| row[i].to_f } unless i<num_dimensions
      end
    end
  end

  def referral_data(data)
    return {} unless data.total_results > 0

    total = data.rows.map { |row| row[1].to_f }.reduce(:+)
    rows = data.rows.map { |row| [row.first, row[1].to_f, row[1].to_f / total] }
    { data:  rows[0, 9],
      total: total
    }
  end

  def filters(offering)
    filter = %W(ga:landingPagePath==/offering/#{offering.id})
    filter << "ga:landingPagePath=~#{offering.vanity_path}" if offering.vanity_path.present?
    filter.join(',')
  end

  def page_visit_info_dimensions(frequency)
    if frequency.blank? || frequency == 'daily'
      'ga:date'
    elsif frequency == 'weekly'
      'ga:week,ga:year'
    elsif frequency == 'monthly'
      'ga:month,ga:year'
    else
      'ga:date'
    end
  end

  def page_visit_info_sort(frequency)
    if frequency.blank? || frequency == 'daily'
      'ga:date'
    elsif frequency == 'weekly'
      'ga:year,ga:week'
    elsif frequency == 'monthly'
      'ga:year,ga:month'
    else
      'ga:date'
    end
  end

  def profile_id
    "ga:#{ENV['GOOGLE_PROFILE_ID']}"
  end

end
