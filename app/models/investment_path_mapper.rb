class InvestmentPathMapper
  include Rails.application.routes.url_helpers

  attr_accessor :investment

  def initialize(_investment)
    self.investment = _investment
  end

  def prohibited?(current_path, current_method)
    data = map_to_path
    allowed = data[:allowed][current_method] ? data[:allowed][current_method] : []
    Rails.logger.info("Investment state: #{investment.state} - Requested path: #{current_method} #{current_path} - allowed: #{allowed} - target: #{data[:target]}")
    !(allowed.include?(current_path) || data[:target] == current_path)
  end

  def target_for
    map_to_path[:target]
  end

  private

  def map_to_path
    offering_id = investment.offering_id

    if investment.offering.ended?
      return path_offering_closed(offering_id)
    end

    unless investment.persisted?
      return path_transient(offering_id)
    end

    case investment.state
      when 'started'
        path_started offering_id
      when 'intended'
        path_intended offering_id
      when 'entity_selected'
        path_entity_selected offering_id
      when 'accreditation_verified', 'declined_documents'
        path_verified offering_id
      when 'signed_documents'
        path_signed offering_id
      when 'funding_method_selected', 'funds_sent', 'ach_debit_failed'
        path_funding offering_id
      else
        path_default
    end
  end

  def path_offering_closed(offering_id)
    {
      target: offering_path(id: offering_id),
      allowed: {}
    }
  end

  def path_transient(offering_id)
    if investment.offering.reg_c_enabled?
      {
        target: new_offering_investment_flow_path(offering_id: offering_id),
        allowed: {
          post: [
            offering_investment_flow_path(offering_id: offering_id)
          ]
        }
      }
    else
      {
        target: new_offering_investment_path(offering_id: offering_id),
        allowed: {
          get: [
            recreate_offering_investment_path(offering_id: offering_id)
          ],
          post: [
            offering_investment_path(offering_id: offering_id)
          ]
        }
      }
    end
  end

  def path_default
    {
      target: dashboard_investments_path,
      allowed: {}
    }
  end

  def path_funding(offering_id)
    if investment.offering.reg_c_enabled?
      if investment.ach_debit_failed?
        target = edit_offering_investment_flow_fund_path(offering_id: offering_id)
      else
        target = dashboard_investments_path
      end
      {
        target: target,
        allowed: {
          get: [
            complete_offering_investment_flow_fund_path(offering_id: offering_id),
            offering_investment_flow_fund_path(offering_id: offering_id),
            edit_offering_investment_flow_fund_path(offering_id: offering_id),
            edit_offering_invest_accreditation_new_path(offering_id: offering_id)
          ],
          patch: [
            offering_investment_flow_fund_path(offering_id: offering_id)
          ],
          post: [
            offering_investment_flow_fund_path(offering_id: offering_id)
          ]
        }
      }
    else
      {
        target: dashboard_investments_path,
        allowed: {
          get: [
            complete_offering_invest_fund_path(offering_id: offering_id),
            offering_invest_fund_path(offering_id: offering_id),
            edit_offering_invest_fund_path(offering_id: offering_id),
            edit_offering_invest_accreditation_new_path(offering_id: offering_id)
          ],
          patch: [
            offering_invest_fund_path(offering_id: offering_id),
            offering_invest_accreditation_new_path(offering_id: offering_id)
          ],
          post: [
            offering_invest_fund_path(offering_id: offering_id)
          ]
        }
      }
    end
  end

  def path_signed(offering_id)
    if investment.offering.reg_c_enabled?
      {
        target: offering_investment_flow_fund_path(offering_id: offering_id),
        allowed: {
          get: [
            new_offering_investment_flow_fund_path(offering_id: offering_id),
            edit_offering_invest_accreditation_new_path(offering_id: offering_id),
          ],
          patch: [
            offering_investment_flow_fund_path(offering_id: offering_id),
            offering_invest_accreditation_new_path(offering_id: offering_id)
          ],
          post: [
            offering_investment_flow_fund_path(offering_id: offering_id)
          ]
        }
      }
    else
      {
        target: offering_invest_fund_path(offering_id: offering_id),
        allowed: {
          get: [
            new_offering_invest_fund_path(offering_id: offering_id),
            edit_offering_invest_accreditation_new_path(offering_id: offering_id)
          ],
          patch: [
            offering_invest_fund_path(offering_id: offering_id),
            offering_invest_accreditation_new_path(offering_id: offering_id)
          ],
          post: [
            offering_invest_fund_path(offering_id: offering_id)
          ]
        }
      }
    end
  end

  def path_verified(offering_id)
    {
      target: offering_invest_investment_documents_path(offering_id: offering_id),
      allowed: {
        get: [
          accept_offering_invest_investment_documents_path(offering_id: offering_id),
          edit_offering_investment_path(offering_id: offering_id),
          edit_offering_investment_flow_path(offering_id: offering_id),
          docusign_handler_offering_invest_investment_documents_path(offering_id: offering_id),
          docusign_redirect_offering_invest_investment_documents_path(offering_id: offering_id),
          # backward
          edit_offering_invest_investment_profile_path(offering_id: offering_id),
          edit_offering_invest_accreditation_new_path(offering_id: offering_id),
          new_offering_invest_accreditation_verified_path(offering_id: offering_id)
        ],
        post: [
          accept_offering_invest_investment_documents_path(offering_id: offering_id),
          # backward
          offering_invest_investment_profile_path(offering_id: offering_id),
          offering_invest_accreditation_new_path(offering_id: offering_id),
          offering_investment_flow_path(offering_id: offering_id)
        ],
        patch: [
          offering_investment_path(offering_id: offering_id),
          offering_invest_accreditation_new_path(offering_id: offering_id)
        ]
      }
    }
  end

  def path_entity_selected(offering_id)
    {
      target: new_offering_invest_accreditation_new_path(offering_id: offering_id),
      allowed: {
        get: [
          offering_invest_accreditation_new_path(offering_id: offering_id),
          new_offering_invest_accreditation_verified_path(offering_id: offering_id),
          offering_invest_accreditation_verified_path(offering_id: offering_id),
          edit_offering_investment_path(offering_id: offering_id),
          edit_offering_investment_flow_path(offering_id: offering_id),
          # backward
          edit_offering_invest_investment_profile_path(offering_id: offering_id)
        ],
        patch: [
          offering_investment_path(offering_id: offering_id)
        ],
        post: [
          offering_invest_accreditation_verified_path(offering_id: offering_id),
          offering_invest_accreditation_new_path(offering_id: offering_id),
          # backward
          offering_invest_investment_profile_path(offering_id: offering_id),
          offering_investment_flow_path(offering_id: offering_id)
        ]
      }
    }
  end

  def path_intended(offering_id)
    if investment.offering.reg_c_enabled?
      {
        target: new_offering_invest_investment_profile_path(offering_id: offering_id),
        allowed: {
          get: [
            offering_invest_investment_profile_path(offering_id: offering_id),
            edit_offering_investment_flow_path(offering_id: offering_id),
          ],
          post: [
            offering_invest_investment_profile_path(offering_id: offering_id),
            offering_investment_flow_path(offering_id: offering_id)
          ]
        }
      }
    else
      {
        target: new_offering_invest_investment_profile_path(offering_id: offering_id),
        allowed: {
          get: [
            offering_invest_investment_profile_path(offering_id: offering_id),
            edit_offering_investment_path(offering_id: offering_id),
          ],
          post: [
            offering_invest_investment_profile_path(offering_id: offering_id)
          ],
          patch: [
            offering_investment_path(offering_id: offering_id)
          ]
        }
      }
    end
  end

  def path_started(offering_id)
    if investment.offering.reg_c_enabled?
      {
        target: new_offering_investment_flow_path(offering_id: offering_id),
        allowed: {
          get: [
          ],
          post: [
            offering_investment_flow_path(offering_id: offering_id)
          ]
        }
      }
    else
      {
        target: new_offering_investment_path(offering_id: offering_id),
        allowed: {
          get: [
            recreate_offering_investment_path(offering_id: offering_id)
          ],
          post: [
            offering_investment_path(offering_id: offering_id)
          ],
          patch: [
            offering_investment_path(offering_id: offering_id)
          ]
        }
      }
    end
  end
end
