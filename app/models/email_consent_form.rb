class EmailConsentForm
  extend ActiveModel::Naming
  include ActiveModel::Conversion
  delegate :valid?, :errors, to: :validator

  ATTRIBUTES = [:email_consent, :user]

  attr_accessor *ATTRIBUTES, :validator, :user, :attributes

  def initialize(attrs={}, opts={})
    @attributes = attrs.to_hash.with_indifferent_access
    @validator = opts[:validator_klass] || EmailConsentFormValidator.new(@attributes)

    @attributes.each do |attr, val|
      instance_variable_set("@#{attr}", val)
    end
  end

  def save
    @user.update_attribute(:email_consent_at, Time.now)
  end

  def persisted?
    false
  end
end
