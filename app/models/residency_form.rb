class ResidencyForm
  delegate :errors, :valid?, to: :validator

  ATTRIBUTES = []

  attr_reader *ATTRIBUTES, :validator, :user, :attributes

  def initialize(attrs={}, user=nil, opts={})
    @attributes = attrs.to_hash.with_indifferent_access
    @user = user
    @validator = (opts[:validator_klass] || ResidencyFormValidator).new(valid_params, @user)

    @attributes.each do |attr, val|
      instance_variable_set("@#{attr}", val)
    end
  end

  def save
    if validator.valid?
      # Create the user and pfii record
      pfii = @user.personal_federal_identification_information || PersonalFederalIdentificationInformation.new
      pfii.assign_attributes(pfii_params)
      @user.personal_federal_identification_information = pfii
      @user.save
    else
      false
    end
  end

  def persisted?
    false
  end

  private

  def valid_params
    attributes.slice(*ATTRIBUTES)
  end

  def pfii_params
    attributes.slice(*ResidencyForm::ATTRIBUTES)
  end

end
