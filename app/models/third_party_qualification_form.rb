class ThirdPartyQualificationForm

  extend ActiveModel::Naming
  include ActiveModel::Conversion

  attr_reader :validator

  delegate :errors, to: :validator

  ATTRIBUTES = [
      :name, :role, :company_name, :license_number, :license_state, :phone_number, :email, :signature,
      :verified_by_assets, :verified_by_accredited
  ]

  attr_reader *ATTRIBUTES

  def initialize(form_attrs, qualification, options = {})
    @attributes = form_attrs
    @qualification = qualification

    @validator = options.fetch(:validator) { ThirdPartyQualificationFormValidator.new(@attributes) }

    ATTRIBUTES.each do |attr|
      attr_value = @attributes[attr.to_s] || qualification.send(attr)
      instance_variable_set("@#{attr}", attr_value)
    end
  end

  def save
    if @validator.valid?
      @qualification.attributes = @attributes
      @qualification.signed_by_third_party_at = Time.current
      @qualification.save!

      true
    else
      false
    end
  end

end

