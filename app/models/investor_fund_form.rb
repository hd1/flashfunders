class InvestorFundForm
  extend ActiveModel::Naming
  include ActiveModel::Conversion

  ATTRIBUTES = [:bank_account_id, :bank_account_number, :bank_account_holder, :bank_account_routing, :bank_name, :bank_account_type, :passport_image, :funding_method, :funding_type, :funding_international, :authorization]

  attr_reader *ATTRIBUTES

  attr_reader :user, :validator
  private :user, :validator

  delegate :valid?, :errors, to: :validator

  def initialize(user, investment, options = {})
    @user       = user
    @investment = investment
    @offering   = investment.offering

    params = options.fetch(:params, {})

    params[:bank_account_routing] = sanitized_routing_number(params[:bank_account_routing])
    @validator                    = options.fetch(:validator) { InvestorFundFormValidator.new params }

    ATTRIBUTES.each do |attr|
      instance_variable_set("@#{attr}", params[attr])
    end
  end

  def save
    ActiveRecord::Base.transaction do
      @investment.select_funding_method! unless @investment.funding_method.present?
      @investment.funding_method = funding_method

      investor_account                = create_bank_account account_params
      @investment.investor_account_id = investor_account.id
      @investment.save
      true
    end
  end

  def update(account)
    params = account_params

    ActiveRecord::Base.transaction do
      if account_data_changed?(account, params)
        matching_account = find_matching_bank_account params
        if !matching_account.nil?
          @investment.investor_account_id = matching_account.id
        elsif account_used_for_multiple_investments?(account.id)
          account                         = create_bank_account params
          @investment.investor_account_id = account.id
        else
          account.update_attributes(params)
        end
      end
      @investment.funding_method = funding_method
      @investment.save
    end

    delete_unused_accounts(@investment.user_id)
  end

  def persisted?
    false
  end

  def bank_account_types
    linked_external_account_type_mapper = Struct.new(:type, :i18n_key)
    Bank::Account::TYPES.each.map do |type|
      linked_external_account_type_mapper.new(type, bank_account_type_i18n_label(type))
    end
  end

  def bank_account_type_i18n_label(my_type = nil)
    index = Bank::Account::TYPES.index(my_type || bank_account_type)
    "linked_external_account_type.option_#{index + 1}"
  end



  private
  def sanitized_routing_number(routing_number)
    routing_number.gsub(/[^0-9a-z]/i, '') if routing_number
  end

  def create_bank_account(params)
    Bank::InvestorAccount.create!(params)
  end

  def find_matching_bank_account(params)
    Bank::InvestorAccount.where(user_id:        @investment.user_id,
                                account_type:   params[:account_type],
                                routing_number: params[:routing_number]).each do |account|
      # Need to test outside of query because of encryption
      if account.account_number == params[:account_number] &&
          account.account_holder == params[:account_holder]
        return account
      end
    end
    nil
  end

  def account_data_changed?(account, params)
    account.account_type != params[:account_type] ||
        account.routing_number != params[:routing_number] ||
        account.account_number != params[:account_number] ||
        account.account_holder != params[:account_holder]
  end

  def account_params
    { user_id:        user.id,
      account_holder: bank_account_holder,
      account_number: bank_account_number,
      routing_number: bank_account_routing,
      account_type:   bank_account_type,
      funding_type:   funding_type
    }
  end

  def delete_unused_accounts(user_id)
    Bank::InvestorAccount.where(user_id: user_id).each do |account|
      if !account_used_for_any_investment? account.id
        account.delete
      end
    end
  end

  def account_used_for_any_investment?(id)
    number_of_linked_investments(id) > 0
  end

  def account_used_for_multiple_investments?(id)
    number_of_linked_investments(id) > 1
  end

  def number_of_linked_investments(id)
    Investment.where(investor_account_id: id).count
  end

end

