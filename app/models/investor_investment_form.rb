class InvestorInvestmentForm
  extend ActiveModel::Naming
  include ActiveModel::Conversion

  ATTRIBUTES = [:offering_id, :investment_shares, :investment_amount, :unrounded_investment_amount, :user,
                :investment_percent_ownership, :investment_id]

  attr_reader *ATTRIBUTES
  attr_reader :investment_id

  attr_reader :validator

  delegate :errors, to: :validator

  def initialize(attributes = {}, validator = InvestorInvestmentFormValidator.new)
    ATTRIBUTES.each do |attr|
      instance_variable_set("@#{attr}", attributes[attr])
    end

    @validator = validator
  end

  def persisted?
    investment_id.present?
  end

  def create

    investment = Investment.create(
      user_id: user.id,
      offering_id: offering_id,
      security: get_security,
      amount: investment_amount,
      shares: investment_shares,
      percent_ownership: investment_percent_ownership
    )

    @investment_id = investment.id

    investment.begin!
    investment
  end

  def update
    investment = Investment.find(investment_id)
    investment.update_attributes(amount: investment_amount,
                                 security: get_security,
                                 shares: investment_shares,
                                 percent_ownership: investment_percent_ownership)

    investment.edit_amount! if investment.can_edit_amount?
    investment
  end

  private

  def get_security
    offering = Offering.find(offering_id)

    offering.other_security && investment_amount < offering.direct_investment_threshold ? offering.other_security : offering.reg_d_direct_security
  end
end

