class ConfirmationFormValidator < ResidencyFormValidator

  ensure_valid :password, presence: true
  ensure_valid :password_confirmation, presence: true

  def initialize(attrs={}, user=nil)
    super(attrs, user)
  end

  private

  def user_valid?
    user = lookup_user
    if user
      user.assign_attributes(@attributes)
    else
      user = User.new(@attributes)
    end
    check_user_valid?(user)
  end

  def lookup_user
    @user
  end
  
  def check_user_valid?(user)
    return true if user.valid?
    user.errors.messages.each do |k, vals|
      vals.each { |v| errors.add(k, v) }
    end
    false
  end
end
