class EntityAccreditation

  attr_reader :entity, :status_history

  def initialize(entity, options = {})
    @entity = entity
    @status_history = options[:status_history] || Accreditor::StatusHistory.new(entity)
  end

  def status
    @current_status ||= status_history.get_status(Date.current)
  end

  def previously_approved?
    status_history.has_occurred?(Accreditor::Status.approved)
  end

  def qualification
    status.qualification
  end

  def expiration_date
    status.expiration_date
  end

  def days_until_expiration
    (expiration_date - Date.current).to_i
  end

end

