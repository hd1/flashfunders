module Bank
  class Account < ActiveRecord::Base
    self.table_name = :bank_accounts

    TYPES = %w(CHECKING SAVINGS)

    enum funding_type: { 
      'Income'                        => 0, 
      'Pension or Retirement Savings' => 1, 
      'Sale of Investments'           => 2, 
      'Sale of Property'              => 3, 
      'Loan'                          => 4, 
      'Inheritance'                   => 5, 
      'Insurance Policy Claim'        => 6, 
      'Social Security Benefits'      => 7, 
      'Other'                         => 8
    }

    attr_accessor :funding_international

    attr_encrypted :account_number, key: Rails.application.config.encryption.bank_account_key
    attr_encrypted :account_holder, key: Rails.application.config.encryption.bank_account_holder_key

    def international?
      country != State.default_country
    end

    def last_four
      account_number[-4..-1]
    end

    def last_two
      account_number[-2..-1]
    end

    def mask(len)
      '*' * (account_number.size - len) + account_number[-len, len]
    end

    def bank_name
      return attributes['bank_name'] unless attributes['bank_name'].blank?

      routing_info = RoutingNumber.find_by_routing_number(routing_number)
      routing_info ? routing_info.name : "Unknown"
    end
  end
end
