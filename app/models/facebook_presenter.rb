class FacebookPresenter
  attr_reader :url
  private :url

  def initialize(url)
    @url = url
  end

  def share_url
    uri = URI('https://www.facebook.com/dialog/share')
    uri.query = URI.encode_www_form([['app_id', ENV['FB_APP_ID']],
                                     ['display', 'popup'],
                                     ['href', url],
                                     ['redirect_uri', ENV['FB_REDIRECT_URL']]])
    uri.to_s
  end

  def offering_url
    uri = URI('https://facebook.com/sharer/sharer.php')
    uri.query = URI.encode_www_form([['u', url]])

    uri.to_s
  end

end