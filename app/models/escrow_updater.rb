class EscrowUpdater

  def fetch_and_process_all_escrows
    process_investments!
  end

  def process_investments!
    Investment.where("state <> ? and escrowed_at IS NOT NULL", 'escrowed').each do |investment|
      if investment.can_transfer_funds?
        InvestmentMailer.funds_received(investment.id).deliver_now
        investment.transfer_funds!
        GoogleAnalytics::Base.new.tracker.event(category: 'investment', action: 'Funded', label: investment.offering_id)
      end
    end
  end

end
