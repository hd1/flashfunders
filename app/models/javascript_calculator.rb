require 'execjs'
require 'bigdecimal/util'

class JavascriptCalculator

  JSCALC = ExecJS.compile(File.read(File.expand_path(Rails.root.join('app', 'assets', 'javascripts', 'investor_investments/investment_calculator.js'), __FILE__)))

  def javascript_args
    args_to_javascript @args
  end

  private

  def call_js_calculator(name)
    cmd   = "new FF.#{name}(#{args_to_javascript(@args)})"
    @calc = hash_from_javascript JSCALC.eval(cmd)
  end

  def args_to_javascript(args)
    Hash[args.map { |k, v| [k.to_s.camelize(:lower), v] }].to_json
  end

  def hash_from_javascript(args)
    Hash[args.map { |k, v| [k.to_s.underscore.to_sym, v] }]
  end

  def make_big_decimal(value)
    case value
      when NilClass then
        value.to_s.to_d
      when String then
        value.gsub(/[^0-9.-]/, '').to_d
      else
        value.to_d
    end
  end

end