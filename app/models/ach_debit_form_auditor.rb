class AchDebitFormAuditor < Auditor
  def process_state(persisted_state)
    persisted_state.stringify_keys.tap do |hash|
      sanitize_bank_account_number!(hash)
      convert_investment_amount_to_string!(hash)
    end
  end

  private

  def sanitize_bank_account_number!(hash)
    if hash.has_key?('bank_account_number')
      hash['bank_account_number'] = SanitizeToLastFourDigits.sanitize(hash['bank_account_number'])
    end
  end

  def convert_investment_amount_to_string!(hash)
    if hash.has_key?('investment_amount')
      hash['investment_amount'] = sprintf('%.2f', hash['investment_amount'])
    end
  end
end