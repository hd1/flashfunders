class PlaceholderOfferingPresenter
  attr_reader :photo, :company_name, :tagline

  def initialize(photo=nil)
    @photo = photo || 'browse/browse-placeholder-1.png'
    @company_name = 'Lorem Ipsum'
    @tagline = 'Aliquam tincidunt mauris eu risus'
  end
end