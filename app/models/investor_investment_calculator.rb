class InvestorInvestmentCalculator < JavascriptCalculator

  def self.get_calc(amount, offering_presenter)
    if offering_presenter&.convertible?
      ConvertibleCalculator.new(amount, offering_presenter)
    else
      UnitsBasedCalculator.new(amount, offering_presenter)
    end
  end

  def initialize(investment_value, offering_presenter)
    direct_spv = offering_presenter.model_direct_spv?
    @args = { amount: [0, make_big_decimal(investment_value)].max.to_f,
              total: offering_presenter.maximum_total_investment.to_f,
              valuation: (offering_presenter.convertible? ? offering_presenter.valuation_cap : offering_presenter.pre_money_valuation).to_f,
              model_direct_spv: direct_spv,
              spv_total: direct_spv ? offering_presenter.spv_total_investment.to_f : 0,
              goal: direct_spv ? offering_presenter.spv_minimum_goal.to_f : 0,
              threshold: direct_spv ? offering_presenter.direct_investment_threshold.to_f : 0,
              setup_fee: direct_spv ? offering_presenter.setup_fee_dollar.to_f : 0 }
  end

  def projected_shares
    @calc[:projected_shares]
  end

  def projected_cost
    @calc[:projected_cost].to_d.round(2)
  end

  def projected_percent_ownership
    @calc[:projected_ownership_pct]
  end

  private

  def call_js_calculator
    super('InvestmentCalculator')
  end

end

class ConvertibleCalculator < InvestorInvestmentCalculator
  def initialize(investment_value, offering_presenter)
    super(investment_value, offering_presenter)
    @args[:calc_type] = :convertible
    @args[:security_plural] = offering_presenter.other_security_type_title_plural || ''
    call_js_calculator
  end
end

class UnitsBasedCalculator < InvestorInvestmentCalculator
  def initialize(investment_value, offering_presenter)
    super investment_value, offering_presenter
    if offering_presenter.revenue_note?
      @args[:calc_type] = :revenue_note
    else
      @args[:calc_type] = :units_based
    end
    @args[:share_price] = (offering_presenter.share_price || 0).to_f
    @args[:security_plural] = offering_presenter.other_security_type_title_plural || ''
    call_js_calculator
  end
end

