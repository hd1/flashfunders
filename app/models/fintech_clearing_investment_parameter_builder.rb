class FintechClearingInvestmentParameterBuilder
  def self.build(investment)
    hash = {
      investment: {
        uuid: investment.fund_america_id,
        offering_uuid: investment.security.fund_america_id,
        amount: sprintf('%.2f', investment.amount),
        funding_method: investment.ach? ? 'funding_method_ach' : 'funding_method_wire',
      }.merge(bank_info(investment))
    }

    if investment.by_individual?
      hash.deep_merge({ investment: entity_params_for_person(investment) })
    else
      hash.deep_merge({ investment: entity_params_for_company(investment) })
    end
  end

  def self.entity_params_for_person(investment)
    pfii = investment.investor_entity.personal_federal_identification_information
    {
      investor_first_name: pfii.first_name,
      investor_last_name: pfii.last_name,
      investor_phone: pfii.phone_number,
      investor_email: investment.user.email,
      investor_address1: pfii.address1,
      investor_address2: pfii.address2,
      investor_city: pfii.city,
      investor_state: pfii.state_id,
      investor_country: pfii.country,
      investor_postal_code: pfii.zip_code,
      investor_date_of_birth: pfii.date_of_birth.to_s,
      investor_ssn: pfii.ssn
    }
  end

  def self.entity_params_for_company(investment)
    entity = investment.investor_entity
    {
      entity_name: entity.full_name,
      entity_type: entity.display_name,
      entity_signatory_title: entity.position,
      entity_phone: entity.phone_number,
      entity_ein: entity.tax_id_number,
      entity_state_of_domicile: entity.formation_state_id,
      entity_date_of_formation: entity.formation_date ,
      entity_address1: entity.address_1,
      entity_address2: entity.address_2,
      entity_city: entity.city,
      entity_state: entity.state_id,
      entity_country: entity.country || State.default_country,
      entity_postal_code: entity.zip_code
    }
  end

  def self.bank_info(investment)
    bank_account = Bank::InvestorAccount.find(investment.investor_account_id)
    {
      bank_routing_number: bank_account.routing_number,
      bank_account_type: bank_account.account_type == 'CHECKING' ? 'bank_account_type_checking' : 'bank_account_type_savings',
      bank_account_number: bank_account.account_number,
      bank_account_holder: bank_account.account_holder
    }
  end

end
