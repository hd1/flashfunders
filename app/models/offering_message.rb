class OfferingMessage < ActiveRecord::Base
  belongs_to :offering
  belongs_to :user

  validates :body, presence: true

  scope :unread, -> { where(unread: true) }
  scope :sent_by_offering, -> { where(sent_by_user: false) }
  scope :sent_by_user, -> { where(sent_by_user: true) }
  scope :for_user, ->(user_id) { where(user_id: user_id) }
  scope :for_offering, ->(offering_id) { where(offering_id: offering_id) }

  scope :distinct_offerings_for_user, ->(user_id) {
      select('distinct on (offering_id) *').
      where(user_id: user_id).
      order(offering_id: :desc, created_at: :desc)
  }

  scope :distinct_users_for_offering, ->(offering_id) {
      select('distinct on (user_id) *').
      where(offering_id: offering_id).
      order(user_id: :desc, created_at: :desc)
  }

  scope :unread_for_issuer, ->(issuer_id) {
    unread.sent_by_user.
      where(offering_id: Offering.for_user(issuer_id).pluck(:id)).
      group(:offering_id).count
  }

  scope :unread_for_investor, ->(investor_id) {
    unread.sent_by_offering.for_user(investor_id).group(:offering_id).count
  }

  scope :total_unread_for_investor, ->(investor_id) {
    unread_for_investor(investor_id).values.sum
  }

  scope :unread_for_offering, ->(offering_id) {
    unread.sent_by_user.where(offering_id: offering_id).group(:user_id).count
  }

  scope :total_unread_for_offering, ->(offering_id) {
    unread_for_offering(offering_id).values.sum
  }

  scope :unread_for_issuer_current_offering, ->(issuer_id, current_offering_id) {
    unread_for_issuer(issuer_id)[current_offering_id].to_i
  }

  scope :total_unread_for_user, ->(user_id, current_offering_id) {
    total_unread_for_investor(user_id) + unread_for_issuer_current_offering(user_id, current_offering_id)
  }
end
