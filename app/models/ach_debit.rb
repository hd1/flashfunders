class AchDebit < ActiveRecord::Base
  include UniquelyIdentifiable

  COMPLETED = 'COMPLETED'
  FAILED = 'FAILED'
  CANCELLED = 'CANCELLED'
  PENDING = 'PENDING'
  BANCBOX_ERROR = 'BANCBOX_ERROR'
  UNKNOWN = 'UNKNOWN'

  belongs_to :investment
end