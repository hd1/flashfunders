class LimitReachedEmail < InvestmentEmail
  def investments
    return saved_investments unless saved_investments.empty?

    investment_id = trigger_investment_id || Investment.last.id
    Investment.where('id < ?', investment_id)
      .where(offering_id: offering_id, state: FUNDED_STATES)
  end
end
