class RegistrationForm < ConfirmationForm

  USER_ATTRIBUTES = [:registration_name, :email, :password, :password_confirmation, :agreed_to_tos_ip, :agreed_to_tos_datetime, :agreed_to_tos, :email_consent_at, :provider, :uid]
  ATTRIBUTES = USER_ATTRIBUTES + ResidencyForm::ATTRIBUTES

  attr_reader *ATTRIBUTES, :validator, :user, :attributes

  def initialize(attrs={}, user=nil, opts={})
    super(attrs, user, opts.merge({ validator_klass: RegistrationFormValidator }))
  end

  private

  def user_params
    attributes.slice(*USER_ATTRIBUTES).merge(needs_password_set: false)
  end

end
