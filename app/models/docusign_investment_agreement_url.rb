class DocusignInvestmentAgreementUrl
  class EnvelopeNotRetrieveUrlError < StandardError; end

  def self.get_embedded_url(envelope_id, signer_name, signer_email, return_url)
    request_details = { envelope_id: envelope_id, name: signer_name, email: signer_email, return_url: return_url }
    response = docusign_client.get_recipient_view(request_details)

    unless response.has_key?('url')
      raise EnvelopeNotRetrieveUrlError.new("Docusign error retrieving the url for embedded signing:
        request: #{request_details}.
        Response: #{response}")
    end

    response.fetch('url')
  end

  private

  def self.docusign_client
    DocusignRest::Client.new
  end
end
