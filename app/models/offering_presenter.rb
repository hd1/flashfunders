class OfferingPresenter
  include ActiveSupport::NumberHelper

  attr_reader :company, :offering, :escrow_provider
  private :company, :escrow_provider

  delegate :share_price, :photo_cover_url, :photo_logo_url, :photo_swatch_url, :share_photo, :photo_header_url,
    :pitch_sections, :pre_money_valuation_display, :public_email, :tagline, :vanity_path,
    :model_direct?, :model_direct_spv?, :direct_investment_threshold, :setup_fee_dollar,
    :photo_swatch, :ended?, :successful?,
    :investable?, :escrow_closed?, :pre_money_valuation,
    :closed_successfully?, :closed_unsuccessfully?, :closed_pending?,
    :photo_home_url, :photo_home, to: :offering

  delegate :wire_code, :any_funding_instructions?, to: :escrow_provider

  def initialize(offering)
    @offering = offering
  end

  ## company
  def company_name
    offering.company.try(:name) || 'N/A'
  end

  def company_location
    offering.company.try(:location) || 'N/A'
  end

  def website_link_text
    URI.parse(offering.company.try(:website_url) || '').host
  end

  def website_url
    offering.company.try(:website_url) || ''
  end

  def tagline_browse
    offering.tagline_browse || offering.tagline
  end

  ## securities

  def security_presenters
    @security_presenters ||= begin
      offering.securities.map do |p|
        begin
          Object.const_get("SecurityPresenters::#{p.class.to_s.demodulize}").new(p)
        rescue NameError
          SecurityPresenters::Security.new(p)
        end
      end
    end
  end

  def llcs?
    !!security_presenters.detect { |s| s.is_a?(SecurityPresenters::Llc) }
  end

  def discount_rate
    convertible_security.try(:discount_rate)
  end

  ## escrow
  def escrow_end_date
    return '' unless offering.escrow_end_date.present?
    offering.escrow_end_date.at_beginning_of_day.to_s(:abbreviated_full_date)
  end

  def number_of_investors
    security_presenters.map(&:number_of_investors).sum
  end

  def spv_total_investment
    spv_security.try(:accredited_balance).to_i
  end

  def spv_minimum_goal
    spv_security.try(:minimum_total_investment).to_i
  end

  ## offering
  def streaming_url
    return offering.streaming_url if Rails.env.development? || Rails.env.test?
    offering.streaming_url.gsub('http:', 'https:') if offering.streaming_url
  end

  def formatted_cancel_date
    if offering.ends_at
      (offering.ends_at - 2.days).to_s(:short_date)
    end
  end

  def formatted_escrow_end_date
    offering.escrow_end_date&.to_s(:short_date)
  end

  def formatted_end_date
    offering.ends_at.try(:to_s, :short_date)
  end

  def formatted_end_date_for_popover
    offering.ends_at.try(:to_s, :abbreviated_full_date)
  end

  def formatted_end_date_for_banner
    offering.ends_at.try(:to_s, :long_date)
  end

  def option_pool
    offering.option_pool.presence || "N/A"
  end

  def offering_id
    offering.id
  end

  def funded?
    security_presenters.all?(&:funded?)
  end

  def video_present?
    streaming_url.present?
  end

  def campaign_embedded_video_url
    CampaignVideoURLBuilder.new.convert_vimeo_page_to_embedded_url(offering.video_url)
  end

  def campaign_flash_video_url
    CampaignVideoURLBuilder.new.convert_vimeo_page_to_swf_url(offering.video_url)
  end

  def formatted_percent_equity
    number_to_percentage(equity_percentage_calculator.percent_offered, precision: 2)
  end

  def formatted_days_left
    return 'N/A' unless offering.ends_at.present?
    TextHelper.pluralize(days_left, 'day')
  end

  def days_left
    return 'N/A' unless offering.ends_at.present?
    [(offering.ends_at.to_date - Date.current).to_i, 0].max
  end

  def minimum_total_investment
    security_presenters.map(&:minimum_total_investment).map(&:to_i).max
  end

  def maximum_total_investment
    securities_without_spv.map(&:maximum_total_investment).map(&:to_f).append(1).max
  end

  def option_pool_popover
    security_presenters.first&.option_pool_popover.to_s
  end

  def minimum_investment_amount
    security_presenters.map(&:minimum_investment_amount).map(&:to_i).min
  end

  def formatted_percent_complete
    number_to_percentage(percent_complete, precision: 0)
  end

  def formatted_minimum_total
    number_to_currency(minimum_total_investment.to_f.floor, precision: 0)
  end

  def percent_complete
    @percent_complete ||= compute_percent_complete
  end

  def formatted_accredited_balance
    number_to_currency(accredited_balance.round, precision: 0)
  end

  def accredited_balance
    security_presenters.map(&:accredited_balance).sum
  end

  def minimum_investment_amount_display
    return 'N/A' unless security_presenters.present?
    security_presenters.detect { |s| s.minimum_investment_amount.to_i == minimum_investment_amount.to_i }.minimum_investment_amount_display
  end

  def minimum_total_investment_display
    return 'N/A' unless security_presenters.present?
    security_presenters.detect { |s| s.minimum_total_investment.to_i == minimum_total_investment.to_i }.minimum_total_investment_display
  end

  def maximum_total_investment_display
    return 'N/A' unless security_presenters.present?
    number_to_currency(securities_without_spv.sum { |sp| sp.maximum_total_investment.to_f }, precision: 0)
  end

  def formatted_direct_investment_threshold
    number_to_currency(offering.direct_investment_threshold)
  end

  def unpresentable_minimum?
    minimum_total_investment.blank? || minimum_total_investment == 0
  end

  def first_team_title
    offering.try(:first_team_title) || 'Founding Team'
  end

  def second_team_title
    offering.try(:second_team_title) || 'Investors & Advisors'
  end

  def convertible_offering_valuation_cap_display
    convertible_security.try(:valuation_cap_display)
  end

  def cf_amount_raised
    cf_security.try(:accredited_balance) || 0
  end

  def cf_minimum_reached?
    cf_amount_raised > 0 && cf_amount_raised >= cf_security.minimum_total_investment
  end

  def cf_shares_sold
    cf_security&.shares_sold || 0
  end

  def cf_fees
    cf_security&.fees || {}
  end

  def counter_signed_preamble
    cf_security&.counter_signed_preamble
  end

  def other_security_type_title_plural
    security_title_plural
  end

  def security_title_plural
    securities_without_spv.first.security_title_plural
  end

  def security_title
    securities_without_spv.first.security_title
  end

  def valuation_cap
    convertible_security.try(:valuation_cap)
  end

  def convertible?
    convertible_security.present?
  end

  def revenue_note?
    revenue_note_security.present?
  end

  def shares_offered
    securities_without_spv.map(&:shares_offered).map(&:to_i).sum
  end

  def secondary_nav
    %w(pitch team fundraising questions comment)
  end

  def pre_money_valuation_display
    offering.pre_money_valuation_display.presence || 'N/A'
  end

  def formatted_share_price
    number_to_currency(offering.share_price, precision: 2).presence || 'N/A'
  end

  def has_reg_d_and_reg_cf_securities?
    security_presenters.any? { |s| s.reg_d? } && security_presenters.any? { |s| s.reg_cf? }
  end

  private

  def spv_security
    security_presenters.detect { |s| s.is_spv? }
  end

  def cf_security
    security_presenters.detect { |s| s.reg_cf? }
  end

  def equity_percentage_calculator
    EquityPercentageCalculator.new(shares_offered, offering.fully_diluted_shares)
  end

    # Percent invested over the minimum investment
  def compute_percent_complete
    unpresentable_minimum? ? 100 : ((accredited_balance.to_f / minimum_total_investment.to_f) * 100).floor
  end

  def securities_without_spv
    security_presenters.select { |sp| !sp.is_spv? }
  end

  def convertible_security
    security_presenters.detect { |s| s.is_a?(SecurityPresenters::Convertible) && !s.is_spv? }
  end

  def revenue_note_security
    security_presenters.detect { |s| s.is_a?(SecurityPresenters::RevenueNote) }
  end

end
