class InvestorInvestmentSummary

  include ActiveSupport::NumberHelper

  def initialize(investment, offering)
    @investment = investment
    @offering = offering
  end

  def company_name
    @offering.company.name
  end

  def campaign_tagline
    @offering.tagline
  end

  def formatted_amount
    number_to_currency(@investment.amount)
  end

  def formatted_ownership_percentage
    number_to_percentage(@investment.percent_ownership, precision: 3)
  end

  def formatted_share_price
    number_to_currency(@offering.share_price, precision: 3)
  end

  def number_of_shares
    number_to_delimited(@investment.shares)
  end
end