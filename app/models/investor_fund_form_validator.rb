class InvestorFundFormValidator < BankAccountFormValidator

  ensure_valid :funding_method, presence: true
  ensure_valid :authorization, presence: false

  validate :authorized_for_ach
  validate :routing_number_for_ach

  private

  def authorized_for_ach
    return unless funding_method == 'ach'

    unauthorized = authorization.is_a?(String) ? authorization.to_i.zero? : !authorization
    if unauthorized
      errors.add(:authorization, I18n.t('fund_flow.authorization.error_message'))
    end
  end

  def routing_number_for_ach
    return unless funding_method == 'ach'

    routing_info = RoutingNumber.find_by_routing_number(bank_account_routing)
    if routing_info.blank?
      errors.add(:bank_account_routing, I18n.t('fund_flow.ach.link_refund_account.invalid.routing_number'))
    end
  end

end
