class ImageUploadInput < UploadInput

  def button_content(text, existing_file = nil)
    content = if existing_file 
                image_tag(existing_file .url)
              else
                safe_join([
                  tag('span', class: 'prompt'),
                  content_tag('span', '+', class: 'plus'),
                  tag('br'),
                  text
                ])
              end

    content_tag('div', content, class: 'file-content')
  end

end

