class FileUploadInput < UploadInput

  def button_content(text, existing_file = nil)
    content = if existing_file
                File.basename(existing_file.path || existing_file.key)
              else
                safe_join([
                  content_tag('div', '+', class: 'plus'),
                  text
                ])
              end

    content_tag('div', content, class: 'file-content')
  end

end


