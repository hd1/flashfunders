class UploadInput < SimpleForm::Inputs::Base

  include ActionView::Helpers

  def input(wrapper_options = nil)
    file_types = input_html_options[:accept] || ''
    button_text = options[:text]

    safe_join([
      file_chooser_tag(file_types),
      upload_key_tag,
      custom_button_tag(button_text)
    ])
  end

  protected

  def file_chooser_tag(file_types)
    file_field_tag('file-chooser', accept: file_types, class: 'hidden')
  end

  def upload_key_tag
    @builder.hidden_field(key_name.to_sym, value: decoded_key, class: 'file-key')
  end

  def custom_button_tag(button_text)
    input_html_classes << ' btn-upload-file'
    input_html_classes << ' btn-update-file' if file

    button = safe_join([
      content_tag('div', 'Update', class: 'btn-overlay'),
      button_content(button_text, file)
    ])

    content_tag('div', button, class: input_html_classes)
  end

  def button_content(text, file)
    raise NotImplementedError
  end

  private

  def file
    return unless object.send("has_#{column.name}_upload?")
    object.send(column.name)
  end

  def key_name
    "#{attribute_name}_key"
  end

  def decoded_key
    if key = object.send(key_name)
      URI.decode(key)
    end
  end
end

