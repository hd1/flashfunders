FF.CalculatePopupPosition = function(outerWidth, outerHeight, popupWidth, popupHeight, offsetX, offsetY) {
  return {
    left: Math.floor((outerWidth - popupWidth) / 2) + offsetX,
    top: Math.floor((outerHeight - popupHeight) / 2) + offsetY,
  };
};