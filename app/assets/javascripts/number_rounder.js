FF.NumberRounder = function(number) {

  function RoundWithPrecision(num, precision) {
    return Math.round(num * precision) / precision;
  }

  this.roundToThousandths = function() {
    return RoundWithPrecision(number, 1000);
  };
};

