$(function () {

  var $submitButton = $('#submit-form-btn'),
      $formsContainer = $('.investment-accreditation'),
      $topDivider = $('.workspace-header-divider');

  function clearErrors() {
    $('.error').addClass('hidden');
  }

  function getForm() {

    var forms = $('form:visible');
    if(forms.length == 1) {
      return forms.first();
    } else {
      return [];  // This should never happen
    }
  }

  function clearTopError() {
    $topDivider.removeClass('errors');
    $('#errors').remove();
  }

  function addTopError(msg) {
    clearTopError();
    $topDivider.addClass('errors').append('<div id="errors" ><p>' + msg + '</p></div>');
  }

  function displayErrors() {
    if ($('#income-or-net-worth li.active').length == 0) {
      showTopError("Please select one of the options below to continue.");
    } else {
      if( ($('#third-party-or-docs li.active').length == 0) && ($('#net-worth-third-party-or-docs li.active').length == 0) ) {
        showTopError("Please select a method to verify your accreditation status.");
      }
    }
  }

  function showTopError(message) {
    addTopError(message);
    $('html,body').animate({ scrollTop: 0}, 'fast');
  }

  $submitButton.click(function (e) {
    var $activeForm = getForm();

    e.preventDefault();

    if ($activeForm.length > 0) {
      $(e.target).attr('disabled', true);
      if ($activeForm.hasClass('batch-uploader'))
      {
        $("#uploading-spinner").modal({
          keyboard: false,
          backdrop: false
        });
      }
      // Don't ask about leaving page on a submit
      $activeForm.removeClass('dirty');
      $activeForm.submit();
    } else {
      displayErrors();
    }
  });

  $('li.tab').click(function(e) {
    $(e.target).find('a').click();
  });

  $('.boxed-radio').click(function(e) {
    var obj = $(e.target);
    var radio = obj.find('input[type=radio]');
    radio.click();
  });

  $('.boxed-radio input[type="radio"]').click(function(e) {
    var obj = $(e.target);
    var box = obj.closest('.boxed-radio');
    var radio = box.find('input[type=radio]');
    $('.boxed-radio.active').removeClass('active');
    clearErrors();
    box.addClass('active');
  });
  

  $('form.batch-uploader', $formsContainer).batchUploaderForm({
    onBatchFailure: function () {
      showTopError("Unable to upload your documents. Please try again.");
      $('#uploading-spinner').modal('hide');
      $('#spinner').hide();
    }
  });
});
