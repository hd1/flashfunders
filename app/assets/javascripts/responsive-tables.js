$(document).ready(function () {
  var switched = {};
  var updateTables = function () {

    if (im.lessThan('desktop')) {
      $('table.responsive').each(function (i, element) {
        var tableName = $(element).attr('id');
        if (! switched[tableName]) {
          switched[tableName] = true;
          splitTable($(element));
        }
      });
      return true;
    }
    else {
      $("table.responsive").each(function (i, element) {
        var tableName = $(element).attr('id');
        if (switched[tableName]) {
          switched[tableName] = false;
          unsplitTable($(element));
        }
      });
    }
  };
  $(window).load(updateTables);
  $(window).bind("resize", updateTables);
  function splitTable(original) {
    original.wrap("<div class='table-wrapper' />");
    var copy = original.clone();
    copy.find("td:not(:first-child), th:not(:first-child)").css("display", "none");
    copy.removeClass("responsive");
    original.closest(".table-wrapper").append(copy);
    copy.wrap("<div class='pinned' />");
    original.wrap("<div class='scrollable' />");
  }

  function unsplitTable(original) {
    original.closest(".table-wrapper").find(".pinned").remove();
    original.unwrap();
    original.unwrap();
  }

  window.FF.updateTables = function(tableName) { switched[tableName]=false; updateTables(tableName); };
});
