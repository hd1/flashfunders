(function (exports) {
  /* code from qodo.co.uk */
// create as many regular expressions here as you need:
  var digitsOnly = /[1234567890]/g;
  var floatOnly = /[0-9\.]/g;
  var alphaOnly = /[A-Za-z]/g;

  function restrictCharacters(e, character, restrictionType) {
    // if they pressed esc... remove focus from field...
    var code = e.keyCode;
    if (code == 27) {
      this.blur();
      return false;
    }
    // ignore if they are press other keys
    // strange because code: 39 is the down key AND ' key...
    // and DEL also equals .
    if (!e.ctrlKey && code != 9 && code != 8 && code != 36 && code != 37 && code != 38 && (code != 39 || (code == 39 && character == "'")) && code != 40) {
      if (character.match(restrictionType)) {
        return true;
      }
    }
    return false;
  }

  function ignoreCharacters(e) {
    return e.ctrlKey || e.altKey || e.metaKey ||
      e.keyCode == $.ui.keyCode.LEFT ||
      e.keyCode == $.ui.keyCode.RIGHT ||
      e.keyCode == $.ui.keyCode.TAB ||
      e.keyCode == $.ui.keyCode.DEL ||
      e.keyCode == $.ui.keyCode.BACKSPACE;
  }

  function getChar(e) {
    if (!e) e = window.event;
    var code = null;
    if (e.keyCode) {
      code = e.keyCode;
    } else if (e.which) {
      code = e.which;
    }
    return String.fromCharCode(code);
  }

  function getVal(newChar) {
    var val = document.activeElement.value.replace(/,/g, "");
    return val.slice(0, document.activeElement.selectionStart) + newChar + val.slice(document.activeElement.selectionEnd);
  }

  function floatInput(e, max_value) {
    var curVal = getVal("");
    var newChar = getChar(e);
    if (restrictCharacters(e, newChar, floatOnly) && (newChar != '.' || curVal.indexOf('.') < 0)) {
      // Check max value
      var newVal = getVal(newChar);
      return max_value <= 0 || accounting.unformat(newVal) <= max_value;
    }
    return ignoreCharacters(e);
  }

  exports.FF.restrictCharacters = restrictCharacters;
  exports.FF.floatInput = floatInput;

})(window);
