$(document).ready(function() {
  // Click event on Burger Menu Icon
  var primary_menu = $('.js-nav');
  var burger_menu = $('.js-burger-menu');
  burger_menu.click(function(){
    primary_menu.toggle('slide');
    $('.offering-choice .nav-offering:last').addClass('with-separator');
    return false;
  });

  $(window).resize(function() {
    primary_menu.attr('style', '');
    $('.user-menu').attr('style','');
    $('#investors-nav ul').attr('style', '');
    $('#startups-nav ul').attr('style', '');
    $('#startups-toggle').removeClass('expanded');
    $('#investors-toggle').removeClass('expanded');
    $('#startups-toggle').addClass('contracted');
    $('#investors-toggle').addClass('contracted');
  });

  $('nav .closer').click(function() {
    primary_menu.toggle('slide');
  });

  $('.toggle-user-menu').click(function() {
    $('.user-menu').fadeToggle();
  });

  function toggleCaret(obj) {
    var elem = $(obj);
    if(elem.hasClass('expanded')) {
      elem.removeClass('expanded');
      elem.addClass('contracted');
    } else {
      elem.removeClass('contracted');
      elem.addClass('expanded');
    }
  }

  $('#startups-toggle').click(function() {
    toggleCaret(this);
    $('#startups-nav ul').toggle('slideDown');
  });

  $('#investors-toggle').click(function() {
    toggleCaret(this);
    $('#investors-nav ul').toggle('slideDown');
  });

  //Open links in new window
  $("a[rel=external]").each(function(){
    this.target = "_blank";
  });

  //default validate action
  $('form').attr('novalidate',true);

  $("a.print").click(function(){
    window.print();
    return false;
  });
});
