$(function () {
  'use strict';
  var shareSection = $('.social-share-section');

  $('.twitter-share', shareSection)
    .socialSharingPopup();
  $('.linkedin-share', shareSection)
    .socialSharingPopup({width: 520, height: 455});
  $('.facebook-share', shareSection)
    .socialSharingPopup();
});
