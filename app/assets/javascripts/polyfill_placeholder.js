var PolyfillPlaceholder = function() {
  function nativePlaceholderSupport() {
    var i = document.createElement('input');
    return 'placeholder' in i;
  }

  if(!nativePlaceholderSupport()) $('input, textarea').placeholder();
};
