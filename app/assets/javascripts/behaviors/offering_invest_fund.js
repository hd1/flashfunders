$.scope('#offering-invest-fund', function(ignore) {
  'use strict';

  var $topDivider = $('.workspace-header-divider');
  var active_form = null;
  var pageLoaded = false;

  var getActiveForm = function() {
    var $forms = $('form:visible');
    return ($forms.length === 1) ? $forms.first() : undefined;
  };

  var clearTopError = function() {
    $topDivider.removeClass('errors');
    $('#errors').remove();
  };

  var addTopError = function(msg) {
    clearTopError();
    $topDivider.addClass('errors').append('<div id="errors" ><p>' + msg + '</p></div>');
  };

  var displayErrors = function() {
    addTopError('Please select one of the methods below to continue');
  };

  $('button[type="submit"]').click(function() {
    var active_form = getActiveForm();
    if(active_form !== undefined) {
      FF.AnalyticsTracker.fundSubmit(gon.offering_id);
      active_form.submit();
    } else {
      displayErrors();
    }
  });

  $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    var target = $(e.target).attr("href"); // activated tab
    var form = getActiveForm();

    switch (target) {
      case '#wire-info':
        $('#investor_fund_form_funding_method').val('wire');
        $('#link-refund-account-form').removeClass('hidden');
        $.fn.autoScroll.focus_unless_error('#wire-info form #investor_fund_form_bank_account_holder');
        if (pageLoaded) { FF.AnalyticsTracker.fundWire(gon.offering_id); }
        break;
      case '#check-info':
        $('#investor_fund_form_funding_method').val('check');
        $('#link-refund-account-form').removeClass('hidden');
        $.fn.autoScroll.focus_unless_error('#check-info form #investor_fund_form_bank_account_holder');
        if (pageLoaded) { FF.AnalyticsTracker.fundCheck(gon.offering_id); }
        break;
    }
    $(form).areYouSure();
    if (active_form !== form) { $(form).addClass('dirty'); }
    active_form = form;
  });

  switch (gon.funding_method) {
    case 'wire':
      $('a[href=#wire-info]').click();
      break;
    case 'check':
      $('a[href=#check-info]').click();
      break;
    default:
      break;
  }

  $('a').filter(function(index) { return $(this).text() === 'Finish Later'; }).on('click', function() {
    FF.AnalyticsTracker.fundLater(gon.offering_id);
  });

  pageLoaded = true;
});
