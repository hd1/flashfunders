(function ($, window) {
  "use strict";
  var pluginName = "trafficOverview";

  function Plugin(container_obj, options) {
    Chart.defaults.global.legend.display = false;
    Chart.defaults.global.responsive = true;

    this.container = $(container_obj);
    this.canvas = $("canvas", this.container);
    this.metricSelect = $("select", this.container);
    this.currentMetric = "Pageviews";
    this.currentFrequency = "daily";
    this.frequencies = ["daily", "weekly", "monthly"];
    this.chartTitle = {"startDate": moment(0), "endDate": moment(0)};
    this.metrics = {
      "Pageviews": "ga:pageviews",
      "UniquePageviews": "ga:uniquePageviews",
      "AvgTimeOnPage": "ga:avgTimeOnPage"
    };
    this.maxDataPoints = {mobile: 7, tablet: 14, desktop: 999};
    this.maxRetries = 2;

    this.init();
  }

  Plugin.prototype = {

    init: function () {
      var $this = this;
      this.chartObj = this.createLineChart();
      this.setupDateRangesClickBinding();
      this.setupMetricsOnChange();

      this.waitWithSpinner("on");

      this.loadChartData("daily").done(function () {
        $this.redrawChart();
        $this.loadChartData("weekly").done(function () {
          $this.loadChartData("monthly");
        });
      });
    },

    createLineChart: function () {
      var $this = this;
      var context = this.canvas.get(0).getContext("2d");
      var gradient = context.createLinearGradient(0, 0, 0, 500);

      gradient.addColorStop(0, "rgba(10,162,242,1)");
      gradient.addColorStop(1, "rgba(10,162,242,0.2)");

      var data = {
        labels: [],
        datasets: [
          {
            backgroundColor: gradient,
            borderColor: "rgba(10,162,242,1)", // $primary-blue
            pointBackgroundColor: "#fff",
            pointBorderColor: "rgba(10,162,242,1)",
            pointHoverBackgroundColor: "#fff",
            pointHoverBorderColor: "rgba(10,162,242,1)",
            pointBorderWidth: 2,
            pointHoverRadius: 5,
            tension: 0.5,
            data: []
          }
        ]
      };

      var maxRotation = 25;

      if (im.lessThan("tablet")) {
        maxRotation = 10;
      }

      return new Chart(this.canvas, {
        type: "line",
        data: data,
        options: {
          scales: {
            xAxes: [{
              ticks: {maxRotation: maxRotation},
              gridLines: {drawOnChartArea: false}
            }],
            yAxes: [{
              gridLines: {color: "#EEEEF5"},
              ticks: {
                callback: function(yLabel) {
                  return $this.finaliseYValue(yLabel);
                }
              }
            }]
          },
          elements: {
            point: {
              radius: 4,
              hitRadius: 15
            },
            line: {borderWidth: 2.5}
          },
          tooltips: {
            backgroundColor: "rgba(50,59,89,0.7)",
            bodyFontSize: 14,
            xPadding: 30,
            yPadding: 10,
            callbacks: {
              title: function (tooltipItem, data) {
                if ($this.currentFrequency === "weekly") {
                  var idx = tooltipItem[0].index - 1;
                  if (idx >= 0) {
                    return data.labels[idx] + " - " + tooltipItem[0].xLabel;
                  }
                }
                return tooltipItem[0].xLabel;
              },
              label: function (tooltipItem, data) {
                var selected = $this.metricSelect.find("option:checked").text();
                return $this.finaliseYValue(tooltipItem.yLabel) + " " + selected;
              }
            }
          }
        }
      });
    },

    fetchChartData: function (frequency) {
      var params = {
        data_set: "page_visit_info",
        frequency: frequency
      };
      return $.ajax({
        url: "/api/issuer/analytics/get_data",
        data: params
      });
    },

    loadChartData: function (frequency, attempt) {
      var $this = this, deferred = $.Deferred();
      if (attempt === undefined) {
        attempt = 0;
      }

      this.fetchChartData(frequency).done(function (data, status, jqXHR) {
          if (status === "success") {
            $this.cacheData(data, frequency);
            deferred.resolve();
          } else if (attempt < $this.maxRetries) {
            $this.loadChartdata(frequency, attempt + 1);
          } else {
            deferred.reject();
          }
        })
        .fail(function (data, status, jqXHR) {
          deferred.reject();
        });

      return deferred.promise();
    },

    cacheData: function (data, frequency) {
      this.canvas.data(frequency, data);
    },

    setupDateRangesClickBinding: function () {
      var $this = this;
      $.each(this.frequencies, function (_, value) {
        $("#date-ranges ul li." + value).on("click", function () {
          $("li", $(this).parent()).removeClass("active");
          $(this).addClass("active");
          $this.currentFrequency = value;
          $this.redrawChart();
        });
      });
    },

    setupMetricsOnChange: function () {
      var $this = this;
      this.metricSelect.on("change", function () {
        $this.currentMetric = $(this).val();
        $this.redrawChart();
      });
    },

    redrawChart: function () {
      this.waitWithSpinner("off");
      this.canvas.show();
      this.updateChartData();
    },

    updateChartData: function () {
      var new_data = this.getDataSetForDisplay(this.metrics[this.currentMetric]);
      if (new_data === undefined) {
        this.showEmpty(true);
        return;
      }

      this.showEmpty(false);

      this.chartObj.data.labels = this.formatDateSeries(new_data.x, this.currentFrequency);
      this.chartObj.data.datasets[0].data = new_data.y;
      if (new_data.x.length > 30) {
        this.chartObj.options.elements.point.radius = 0.5;
        this.chartObj.options.elements.point.hitRadius = 2;
        this.chartObj.options.elements.line.borderWidth = 2;
      } else {
        this.chartObj.options.elements.point.radius = 4;
        this.chartObj.options.elements.point.hitRadius = 15;
        this.chartObj.options.elements.line.borderWidth = 2.5;
      }

      this.displayChartLegends(new_data);
      this.chartObj.update();
    },

    getDataSetForDisplay: function (metric) {
      var x_series = [], y_series = [], total, trimmed_y_series = [], trimmed_x_series = [], num_data_points;

      if (im.greaterThan("desktop")) {
        num_data_points = this.maxDataPoints.desktop;
      } else if (im.lessThan("tablet")) {
        num_data_points = this.maxDataPoints.mobile;
      } else if (im.lessThan("desktop")) {
        num_data_points = this.maxDataPoints.tablet;
      }

      var data = this.canvas.data(this.currentFrequency);

      if (data.length === 0) {
        return undefined;
      }

      trimmed_y_series = this.trimLeadingZeros(data.y_series[metric]);
      if (trimmed_y_series.length === 0) {
        return undefined;
      }
      if (trimmed_y_series.length !== data.x_series) {
        trimmed_x_series = this.lastElements(data.x_series, trimmed_y_series.length);
      } else {
        trimmed_x_series = data.x_series;
      }

      x_series = this.lastElements(trimmed_x_series, num_data_points);
      y_series = this.lastElements(trimmed_y_series, num_data_points);

      if (this.currentMetric === "AvgTimeOnPage") {
        var daily_data = this.canvas.data("daily"),
          daily_y_series = this.trimLeadingZeros(daily_data.y_series[metric]);

        total = this.average(daily_y_series);
        total = this.finaliseYValue(total);
      } else {
        total = this.sumArr(y_series);
      }

      return {x: x_series, y: y_series, total: total.toLocaleString('en')};
    },

    trimLeadingZeros: function (arr) {
      var new_arr = [], trimIndex = 0, new_length, i;
      for (i = 0; i < arr.length; i += 1) {
        if (arr[i] === 0) {
          trimIndex = i;
        } else {
          break;
        }
      }
      if (trimIndex === 0) {
        return arr;
      }
      new_length = arr.length - (trimIndex + 1);
      new_arr = this.lastElements(arr, new_length);

      return new_arr;
    },

    sumArr: function (arr) {
      var sum = 0, i;
      for (i = 0; i < arr.length; i += 1) {
        sum = sum + arr[i];
      }
      return sum;
    },

    average: function (arr) {
      return Math.round((this.sumArr(arr) / arr.length) * 100) / 100;
    },

    lastElements: function (array, size) {
      return array.slice(Math.max(array.length - size, 0));
    },

    formatDateSeries: function (date_array, frequency) {
      var i = 0, date;
      switch (frequency) {
        case "weekly":
          this.chartTitle.startDate = moment().year(date_array[0][1]).week(date_array[0][0]);
          this.chartTitle.endDate = moment().year(date_array[date_array.length - 1][1]).week(date_array[date_array.length - 1][0]);
          for (i = 0; i < date_array.length; i += 1) {
            date = moment().year(date_array[i][1]).week(date_array[i][0]);
            date_array[i] = moment(date).format("MMM DD");
          }
          break;
        case "monthly":
          this.chartTitle.startDate = moment([date_array[0][1] + date_array[0][0]], "YYYYMM");
          date = moment([date_array[date_array.length - 1][1] + date_array[date_array.length - 1][0]], "YYYYMM");
          this.chartTitle.endDate = moment(date).endOf('month');
          for (i = 0; i < date_array.length; i += 1) {
            date = moment([date_array[i][1] + date_array[i][0]], "YYYYMM");
            date_array[i] = moment(date).format("MMM");
          }
          break;
        default:
          this.chartTitle.startDate = moment(date_array[0][0], "YYYYMMDD");
          this.chartTitle.endDate = moment(date_array[date_array.length - 1][0], "YYYYMMDD");
          for (i = 0; i < date_array.length; i += 1) {
            date = moment(date_array[i][0], "YYYYMMDD");
            date_array[i] = moment(date).format("MMM DD");
          }
      }

      return date_array;
    },

    finaliseYValue: function (value) {
      if (this.currentMetric === "AvgTimeOnPage") {
        var zeroPad = function(n) { return (n < 10) ? ("0" + n) : n; };
        var duration = moment.duration(value * 1000); // millisecond

        return zeroPad(duration.hours()) + ':' + zeroPad(duration.minutes()) + ':' + zeroPad(duration.seconds());
      }

      return value.toLocaleString('en');
    },

    waitWithSpinner: function (status) {
      var spinner;
      switch (status) {
        case "on":
          spinner = '<div class="spinner-container"><div class="la-ball-clip-rotate la-dark"><div></div>';
          $(spinner).insertAfter($("#pageview-date-range-text"));
          break;
        case "off":
          $('#issuer-traffic_overview .spinner-container').hide();
          break;
      }
    },

    displayChartLegends: function (data) {
      var startDate = moment(this.chartTitle.startDate).format("MMM D, YYYY");
      var endDate = moment(this.chartTitle.endDate).format("MMM D, YYYY");
      $("#pageview-date-range-text p").text(startDate + " - " + endDate);

      $("option[value='" + this.currentMetric + "']", this.select).attr("selected", "selected");
      $("#issuer-traffic_overview small").hide();
      $("#issuer-traffic_overview span.number").text(data.total);
      $("#" + this.currentMetric + "-legend").css("display", "inline-block");
    },

    showEmpty: function (val) {
      if (val) {
        $(".empty", this.container).show();
        $(".with-data", this.container).hide();
      } else {
        $(".empty", this.container).hide();
        $(".with-data", this.container).show();
      }
    }
  };

  $.fn[pluginName] = function (methodOrOptions) {
    return this.each(function () {
      var instance = $.data(this, pluginName);

      if (instance && $.isFunction(Plugin.prototype[methodOrOptions])) {
        // call a public method on an instance
        instance[methodOrOptions].apply(instance, Array.prototype.slice.call(arguments, 1));
      } else if (typeof methodOrOptions === "object" || !methodOrOptions) {
        // initialize the plugin
        $.data(this, pluginName, new Plugin(this, methodOrOptions));
      } else if (!instance) {
        // attempting to call a method prior to initializing
        $.error("Plugin must be initialised before using method: " + methodOrOptions);
      } else {
        // invalid method called
        $.error("Method " + methodOrOptions + "does not exist.");
      }
    });
  };
}(jQuery, window));

$.scope("#issuer-traffic_overview", function (chart_section) {
  "use strict";

  chart_section.trafficOverview();
});
