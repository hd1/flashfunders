$.scope('#team-peek', function (page) {
  'use strict';

  var team_facts = [
    ["6","Pounds of gummy bears consumed in a week"],
    ["8","Languages spoken among team including Spanish, Korean, Hebrew, Dutch, French, German, Russian, and Mandarin"],
    ["149","Countries traveled for our whole team"],
    ["46","VC’s we personally know."]
  ];

  var flash_facts = [
    "FlashFact Common Stock is the type of stock that gets the least amount of rights, privileges and preferences.",
    "Silicon Beach is home to over 500 tech startup companies and 800+ startups.",
    "There are 4.1 jobs created in the U.S. per angel investment.",
    "Venture Capital Fund investments are generally characterized as high-risk/high-return opportunities.",
    "It’s estimated that people between 55 and 64 have the highest rate of entrepreneurship in America.",
    "Pivoting describes a shift in startup strategy during the process of finding the right customer, value prop & positioning.",
    "A “Down Round” is a financing round that is at a lower valuation than the previous round.",
    "To be consider an accredited investor in the U.S. you must have a net worth of at least $1M US or an annual income more than $200K",
    "Startup “unicorns” are companies that have soared to a $1billion valuation or higher, based on fundraising.",
    "Venture Capital is considered a segment of private Equity. However, VCs target startups, while PEs target more mature companies.",
    "Funding your startup with a bank loan requires using earned money to repay it. Equity funding lets you use that money on your biz instead."
  ];

  var container = $('#team-peek .team-collage');
  var original_images = $("#team-peek .team-collage img.team-images");
  var images = $.map(original_images, function(img, i) {
    return $(img).attr('src');
  });

  var team_fact_img = $("#team-peek .team-collage img.team-fact").attr('src');
  var flash_fact_img = $("#team-peek .team-collage img.flash-fact").attr('src');

  // http://stackoverflow.com/questions/6274339/how-can-i-shuffle-an-array-in-javascript
  function shuffle(o){
    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
  }

  function flash_fact(str) {
    var box = $('<div>').addClass('flash_fact');
    var img = $('<img>').attr('src', flash_fact_img);
    box.append(img);
    var span = $('<span>').text(str);
    box.append(span);
    return box.hide();
  }

  function team_fact(arr) {
    var box = $('<div>').addClass('team_fact');
    var img = $('<img>').attr('src', team_fact_img);
    box.append(img);
    var h1 = $('<h1>').text(arr[0]);
    box.append(h1);
    var span = $('<span>').text(arr[1]);
    box.append(span);
    return box.hide();
  }

  function team_image(num) {
    return $('<img>').attr('src', num).hide();
  }

  var replaceSingle = function(old_elem, new_elem, time) {
    setTimeout(function() {
      old_elem.fadeOut(500).replaceWith(new_elem);
      new_elem.fadeIn(1000);
    }, time);
  };

  var timeouts = [];


  var scrambleEverything = function() {

    var random_team_fact = ["team_fact", team_facts[Math.floor(Math.random() * team_facts.length)]];
    var random_flash_fact = ["flash_fact", flash_facts[Math.floor(Math.random() * flash_facts.length)]];

    var to_shuffle = jQuery.extend([], images);
    to_shuffle.push(random_team_fact);
    to_shuffle.push(random_flash_fact);

    var shuffled = shuffle(to_shuffle);

    var children = $(container).children();
    for(var idx = 0; idx < children.length ; idx ++) {

      var old_elem = $(children).eq(idx);
      var elem = shuffled[idx];


      if(typeof elem === "object") {
        var type = elem[0];
        var obj = elem[1];
        if(type == "flash_fact") {
          var new_elem = flash_fact(obj);
          replaceSingle(old_elem, new_elem, (idx+1)*300);
        } else if(type == "team_fact") {
          var new_elem = team_fact(obj);
          replaceSingle(old_elem, new_elem, (idx+1)*300);
        }
      } else {
        var new_elem = team_image(elem);
        replaceSingle(old_elem, new_elem, (idx+1)*300);
      }
    }

    doScramble();
  };

  function doScramble() {
    setTimeout(scrambleEverything, 7000);
  }

  scrambleEverything();

});
