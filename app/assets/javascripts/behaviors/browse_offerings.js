$.scope('#browse-offerings', function (page) {

  $(document).ready(function(){
    $('.funded-cards.reg_d #offering-card').slice(3).hide();
    $('.funded-cards.reg_cf #offering-card').slice(3).hide();
    $('.load-more').on('click', function() {
      $(this).fadeOut(300);
      $('.funded-cards.' + $(this).data('kind') + ' #offering-card').slice(3).show();
    });
  });
  
});
