$.scope('#investment-flow-container', function () {

  'use strict';

  var userInputMax = 1000000000;
  var regCMin = $('#investment_reg_c_min').val();
  var regSMin = $('#investment_reg_s_min').val();
  var regCCutoff = $('#investment_reg_c_cutoff').val();
  var $investmentInternational = $('#investment_international');
  var isInternational = $investmentInternational.val() === 'true';
  var minInvestmentAmt = isInternational ? regSMin : regCMin;
  var maxInvestmentAmt = -1;
  var $investmentAmt = $('#investment_amount');
  var calcArgs = $investmentAmt.data('calcArgs');

  function formatForPage(val, precision) {
    return (val === undefined || !$.isNumeric(val) || val === 0 ? '--' : accounting.formatNumber(val, precision));
  }

  function updateRevenueNoteCaption($caption, shares, cost, price) {
    var shareCaption = 'Own <span id="num_shares"/> <span id="security-name">' + calcArgs.securityPlural + '</span> for <span id="projected-cost"/> at <span id="share-price"/> per unit';

    $caption.html(shareCaption);
    $('#num_shares').text(shares);
    $('#projected-cost').text(cost);
    $('#share-price').text(price);
  }

  function updateConvertibleCaption($caption, isSpv, pctOwned, cost) {
    var shareCaption = '';

    if (isSpv || calcArgs.securityPlural === '') {
      shareCaption = 'Own <span id="percent-ownership"/> of the company';
    } else {
      shareCaption = 'Own  <span id="projected-cost"/> of <span id="security-name">' + calcArgs.securityPlural + '</span> and <span id="percent-ownership"/> of the company';
    }
    $caption.html(shareCaption);
    $('#percent-ownership').text(pctOwned);
    $('#projected-cost').text(cost);
  }

  function isSpv(results) {
    return calcArgs.modelDirectSpv && results.projectedCost < calcArgs.threshold;
  }

  function updateStockCaption($caption, pctOwned, cost, price) {
    var shareCaption = 'Own <span id="percent-ownership"/> of the company for <span id="projected-cost"/> at <span id="share-price"/> per share';

    $caption.html(shareCaption);
    $('#percent-ownership').text(pctOwned);
    $('#projected-cost').text(cost);
    $('#share-price').text(price);
  }

  function updateAmountCaption($caption, results) {
    var pctOwned = formatForPage(results.projectedOwnershipPct, 3) + '%';
    var cost = '$' + formatForPage(results.projectedCost, 2);
    var price = '$' + formatForPage(calcArgs.sharePrice, 2);

    switch(calcArgs.calcType) {
      case 'convertible':
        updateConvertibleCaption($caption, isSpv(results), pctOwned, cost);
        break;
      case 'revenue_note':
        updateRevenueNoteCaption($caption, results.projectedShares, cost, price);
        break;
      case 'units_based':
        updateStockCaption($caption, pctOwned, cost, price);
        break;
      default:
        updateStockCaption($caption, pctOwned, cost, price);
        break;
    }
  }

  function setFormResults(context, results) {
    $('#investment_income').val(context.incomeAmount);
    $('#investment_net_worth').val(context.netWorth);
    $('#investment_external').val(context.prevRegCExtTotal);
    $('#investment_reg_c_output').val(JSON.stringify(results));
  }

  function getLimitText(amount) {
    if (maxInvestmentAmt < 0) { // When first displayed
      return '';
    }
    if (maxInvestmentAmt === 0) {
      return 'You have reached your investment limit.';
    }
    if (amount > maxInvestmentAmt) {
      return 'You can invest up to ' + accounting.formatMoney(maxInvestmentAmt, '$', 0) + '. Please update your investment amount.';
    }
    if (amount > 0 && amount < minInvestmentAmt) {
      return 'You must invest at least ' + accounting.formatMoney(minInvestmentAmt, '$', 0);
    }
    return '';
  }

  function checkLimit(amount) {
    var $limit = $('.invest-limit');

    // Change colors on input amount and input bottom border
    var limitText = getLimitText(amount);
    if (limitText !== '') {
      $investmentAmt.addClass('limit-error');
      $limit.addClass('limit-error');
      $limit.text(limitText);
      $('button[type=submit]').attr('disabled', true);
      return false;
    } else {
      $investmentAmt.removeClass('limit-error');
      $limit.removeClass('limit-error');
      if (amount > 0) {
        $('button[type=submit]').attr('disabled', false);
      }
      return true;
    }
  }

  function doInvestmentCalc() {
    calcArgs.amount = accounting.unformat($investmentAmt.val());
    var $caption = $('.owner-percent');
    var showCaption = 'hidden';

    if (calcArgs.amount > 0) {
      var newVal = accounting.formatMoney(calcArgs.amount, '$', 0);
      if ($investmentAmt.val() !== newVal) {
        $investmentAmt.val(newVal);
      }
      var results = new FF.InvestmentCalculator(calcArgs);
      updateAmountCaption($caption, results);
      showCaption = 'visible';
    } else {
      $investmentAmt.val('');
    }
    // Don't show the caption if we're displaying an error
    $caption.css('visibility', checkLimit(calcArgs.amount) ? showCaption : 'hidden');
  }

  function getContext() {
    return {
      incomeAmount: accounting.unformat($('#income').val()),
      netWorth: accounting.unformat($('#net_worth').val()),
      prevRegCExtTotal: accounting.unformat($('#external').val()),
      prevRegCTotal: $('#investment_internal').val(),
      regCCutoff: $('#investment_reg_c_cutoff').val() - 1,
      sharePrice: calcArgs.sharePrice
    };
  }

  function doRegCCalc() {
    var context = getContext();
    var results = new FF.RegCCalculator(context);
    $('#reg-c-limit .computed-value').text(accounting.formatMoney(results.limit, '$', 0));
    $('#reg-c-total .computed-value').text(accounting.formatMoney(results.available, '$', 0));
    maxInvestmentAmt = results.available;
    setFormResults(context, results);
    doInvestmentCalc();
  }

  function updateUserInputValue(val, obj) {
    if (val.length === 0 || val === '$') {
      obj.val('');
      return;
    }
    obj.val(val);
  }

  function updateAttrValue(val, obj) {
    obj.attr('value', obj.val());
  }

  function handleUserInput(e) {
    var val = accounting.formatMoney(e.target.value, '$', 0);

    var obj = $(e.target);
    var oldVal = obj.data('oldVal');

    if (val === oldVal || e.keyCode === 9) {
      return;
    }
    updateUserInputValue(val, obj);
    updateAttrValue(val, obj);
    obj.data('oldVal', obj.val());
    if (e.target.id === 'investment_amount') {
      doInvestmentCalc(obj);
    } else {
      doRegCCalc();
    }
  }

  function floatInput(event) {
    return FF.floatInput(event, userInputMax);
  }

  function setInvestmentLimitAndRecalc(target) {
    if (target !== '') {
      $('#investment-flow-calculator').attr('style', 'visibility: visible');
    }

    if (target === '#reg-c-panel') {
      minInvestmentAmt = isInternational ? regSMin : regCMin;
      maxInvestmentAmt = regCCutoff - 1;
      $('#investment_selector').val('reg-c');
      $(target).show();
      $('#income').focus();
      doRegCCalc();
    }
    if (target === '#reg-d-panel') {
      $('#investment_selector').val('reg-d');
      minInvestmentAmt = regCCutoff;
      maxInvestmentAmt = calcArgs.total;
    }
    if (target === '#reg-d-panel') {
      $('#reg-c-panel').hide();
      $investmentAmt.focus();
      doInvestmentCalc();
    }
    $investmentAmt.attr('placeholder', 'minimum investment ' + accounting.formatMoney(minInvestmentAmt, '$', 0));
  }

  function setSiteFooterFor(target) {
    $('footer p').removeClass('active');
    $(target + '-footer').addClass('active');
  }

  $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    var target = $(e.target).attr("href"); // activated tab
    setInvestmentLimitAndRecalc(target);
    setSiteFooterFor(target);
  });

  function selectPanel() {
    var pattern = 'a[href=#' + $('#investment_selector').val() + '-panel]';
    $(pattern).click();
  }

  function setupRegCParams() {
    var fields = ['income', 'net_worth', 'external'];
    $.each(fields, function (ignore, name) {
      var field = $('#' + name);
      var value = $('#investment_' + name).val();
      field.val(value === 0 ? '' : accounting.formatMoney(value, '$', 0));
      field.focus(function () {
        this.setSelectionRange(this.value.length, this.value.length);
      });
    });
  }

  function initializeForm() {
    setupRegCParams();
    selectPanel();
    $('form').areYouSure();
  }

  window.floatInput = floatInput;
  window.handleUserInput = handleUserInput;

  initializeForm();
});
