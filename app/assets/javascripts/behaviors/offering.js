$.scope('#offering-landing', function(page) {
  'use strict';

  var $replyForm = $('#reply-form-js');
  FF.offering = {
    id: gon.offering_id,
    resume_intro_to_founder: gon.resume_intro_to_founder
  };

  $('#intro-to-founder-modal').on('shown.bs.modal', function() {
    FF.AnalyticsTracker.modalShown('Show Question Modal Form');
  });

  $('#invest-offering').on('click', function () {
    FF.AnalyticsTracker.invest(FF.offering.id);
  });

  $('a[href="#nav-pitch"]').on('click', function () {
    FF.AnalyticsTracker.pitch(FF.offering.id);
  });

  $('a[href="#nav-team"]').on('click', function () {
    FF.AnalyticsTracker.team(FF.offering.id);
  });

  $('a[href="#nav-documents"]').on('click', function () {
    FF.AnalyticsTracker.documents(FF.offering.id);
  });

  $('a[href="#nav-questions"]').on('click', function () {
    FF.AnalyticsTracker.questions(FF.offering.id);
  });

  $('a[href="#nav-fundraising"]').on('click', function () {
    FF.AnalyticsTracker.fundraising(FF.offering.id);
  });

  var $header = $('.offering-head', page),
      $playBtn = $('.play-btn', page);

  page.offeringNavigation({});
  page.offeringCarouselSize({});

  $('a.fa:not(.fa-envelope)', page).socialSharingPopup();

  // *****************************
  // Setup follow offering toggle
  // *****************************
  $('.offering-follower', page)
    .on('click', '.follow', function() {
      $(this).next('.unfollow')
        .addClass('following')
        .removeClass('unfollow');
      FF.AnalyticsTracker.followStart(FF.offering.id);
    })
    .on('mouseout', '.following', function() {
      $(this)
        .addClass('unfollow')
        .removeClass('following');
    })
    .on('mouseover', '.unfollow', function() {
      $(this)
        .data('altText', $(this).html())
        .html('Unfollow');
    })
    .on('mouseout', '.unfollow', function() {
      $(this)
        .html( $(this).data('altText') );
    });

  // *****************************
  // Setup the video player
  // *****************************

  // initialize the video player
  if ($('.no-video').length == 0) {
    $header.videoPlayer({
      onHide: function () {
        $playBtn.fadeIn('fast');
      },
      onPlay: function() {
        FF.AnalyticsTracker.videoStart(FF.offering.id);
      },
      onFinish: function() {
        FF.AnalyticsTracker.videoFinish(FF.offering.id);
      }
    });
  }

  $('video').on('canplaythrough', function() {
    $playBtn.css('visibility', 'visible');

    // setup handler to play video
    $playBtn.click(function () {
      var btn = $(this),
          offerHead = btn.closest('.offering-head');

      btn.fadeOut('fast');
      offerHead.videoPlayer('show');
    });
  });

  // prevent keyboard shortcuts on video when a modal is shown
  if ($('.no-video').length === 0) {
    $(document)
      .on('shown.bs.modal expando.show', function () { $header.videoPlayer('disableShortcuts'); })
      .on('hidden.bs.modal expando.hide', function () { $header.videoPlayer('enableShortcuts'); });
  }

  var bindCharCounter = function(context) {
    $("#offering_comment_form_body", context).charCount({
      allowed: $('#comments-tab').data('comment-max-length'),
      warning: 0,
      counterText: 'Chars left: '
    });

    $("#offering_comment_form_body", context).on('keyup', function() {
      var isCounterExceeded = $(".counter", context).hasClass('exceeded');
      $(":submit", context).attr('disabled', isCounterExceeded);
    });
  };

  var scrollToComment = function(comment_id) {
    $('html, body').animate({scrollTop: $("#comment-" + comment_id).offset().top}, 0);
  };

  var onReply = function() {
    $('.user-comments [data-info="comment"]').each(function() {
      $(this).on('click', function() {
        $replyForm = $replyForm.detach();

        if ($(this).hasClass('active')) {
          $(this).removeClass("active");
        } else {
          var parentId = $(this).data('id'),
            $replyDiv = $('#reply-form-' + parentId);

          $("#offering_comment_form_parent_id", $replyForm).val(parentId);
          $replyForm.appendTo($replyDiv);
          $replyForm.show();

          $('.user-comments [data-info="comment"]').removeClass('active');
          $(this).addClass("active");
          $("#offering_comment_form_body", $replyForm).focus();
        }
      });
    });
  };

  var confirmDelete = function (path) {
    $('#delete-comment-dialog').dialog({
      dialogClass: 'confirm-dialog',
      modal: true,
      draggable: false,
      resizable: false,
      appendTo: '#nav-comment',
      buttons: [{
        text: 'Cancel',
        class: 'btn btn-medium',
        click: function () {
          $(this).dialog("close");
        }
      },
        {
          id: 'delete-comment-btn',
          text: 'Yes',
          type: 'submit',
          class: 'btn btn-medium btn-primary',
          click: function () {
            $('#delete-comment-dialog form').submit();
          }
        }
      ],
      open: function(event, ui) {
        $('#delete-comment-dialog form').attr('action', path);
      }
    });
  };

  $(document).ready(function() {
    if (gon.path_fragment) { window.location.hash = gon.path_fragment; }
    $("time.ago").timeago();
    bindCharCounter('#comment-form-js');
    bindCharCounter('#reply-form-js');
    onReply();

    if (document.location.hash == '#comment') {
      if (document.location.search.split('comment_id=')[1] != undefined) {
        scrollToComment(document.location.search.split('comment_id=')[1]);
      }
    };

    // Moved from partials
    $('video').trigger('canplaythrough');

    $('a.expand-collapse').click(function () {
      var bio_id = '#bio-' + this.id;
      var full_id = '#full-bio-' + this.id;
      $(bio_id).toggle();
      $(full_id).toggle();
    });

    var $founder_modal = $('#intro-to-founder-modal');
    $founder_modal.on('shown.bs.modal', function(){
      $('#investor_name', this).focus();
    });

    $founder_modal.find('form').submit(function(e) {
      $(this).data('reset-form', true);
    });

    if (FF.offering.resume_intro_to_founder) {
      $founder_modal.find('form').submit();
    }

    $('.modal').on('show.bs.modal', function(){
      var $form = $('form', this);
      if ($form.data('reset-form')) {
        $form.data('reset-form', false);
        $form[0].reset();
      }
    });


    FF.confirmDelete = confirmDelete;
  });

});
