$.scope('#invest_accreditation_new_entity', function(ignore) {
  'use strict';

  var active_form = gon.form_class;
  var offeringId = gon.offering_id;
  var pageLoaded = false;

  $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    var target = $(e.target).attr("href"); // activated tab
    switch (target) {
      case '#entity-third-party':
        if (pageLoaded) {
          FF.AnalyticsTracker.verifyEntityThirdParty(offeringId);
        }
        break;
      case '#entity-offline':
        if (pageLoaded) {
          FF.AnalyticsTracker.verifyEntityOffline(offeringId);
        }
        break;
    }
  });

  $( function(){
    switch (active_form) {
      case 'third_party_entity_form':
        $('a[href=#entity-third-party]').click();
        var form_name = '#new_' + active_form;
        $(form_name).areYouSure();
        break;
      case 'offline_entity_form':
        $('a[href=#entity-offline]').click();
        break;
    }

    pageLoaded = true;
  });

  $('button[type=submit]').on('click', function() {
    FF.AnalyticsTracker.verifySubmit(gon.offering_id);
  });

  $('a').filter(function(ignore) { return $(this).text() === 'Finish Later'; }).on('click', function() {
    FF.AnalyticsTracker.verifyLater(gon.offering_id);
  });

});
