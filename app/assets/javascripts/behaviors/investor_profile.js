$.scope('#investor-profile', function () {
  'use strict';

  function openSubforms(entity, entity_id) {
    //console.log('openSubforms(' + entity + ',' + entity_id + ')');
    $('a[href=#entity-' + entity_id + ']').click();
    var entity_type = $('#' + entity + ' form .form_type > .hidden input').val();
    $('a[href=#' + entity + '][data-type="' + entity_type + '"]').click();
  }

  function openForm(entity, active_entity_id) {
    $('a[href=#entity]').click();
    if (active_entity_id != '') openSubforms(entity, active_entity_id);
  }

  var activeEntityId = gon.activeEntityId;
  var activeForm = gon.activeForm;
  var formsInitialized = false;
  var formLabels = gon.formLabels;

  $(function () {
    if (activeForm !== null && activeForm.length > 0 && activeForm != "individual") {
      openForm(activeForm, activeEntityId);
    } else if (activeForm == "individual") {
      $('a[href=#individual]').click();
    }
    $('form').areYouSure();
    formsInitialized = true;
  });

  function is_us_citizen(form) {
    var value = $('.' + form + '_signatory_us_citizen .active').data('custom-radio-input-value');
    return (value === undefined) || value;
  }

  function toggle_international_fields(isUSA, formType) {
    var usTag = '.' + formType + '-us-only';
    var nonUSTag = '.' + formType + '-non-us';
    if (isUSA) {
      $(usTag).removeClass('hidden');
      $(nonUSTag).addClass('hidden');
    } else {
      $(usTag).addClass('hidden');
      $(nonUSTag).removeClass('hidden');
    }
  }

  function respond_to_country_change_on_form(country, tag, data) {
    var form_type = data['form'];
    var us_citizen = is_us_citizen(form_type);
    var isUSA = us_citizen || country == 'US';

    $('.' + tag).each(function () {
      switch ($(this).attr('id')) {
        case form_type + "_signatory_ssn":
          $(this).attr('placeholder', isUSA ? '###-##-####' : '');
          break;
        case form_type + "_signatory_daytime_phone":
        case form_type + "_signatory_mobile_phone":
          $(this).attr('placeholder', country == 'US' ? '###-###-####' : '');
          break;
      }
    });
    // Toggle usa-only / non-usa elements
    toggle_international_fields(us_citizen, form_type);
  }

  function respond_to_country_change_on_entity_form(country, tag, data) {
    var isUSA = country == 'US';

    $('.' + tag).each(function () {
      if (/phone_number/i.test($(this).attr('id'))) {
        $(this).attr('placeholder', isUSA ? '###-###-####' : '');
      }
      switch ($(this).attr('id')) {
        case data['form'] + '_tax_id_number':
          $(this).attr('placeholder', isUSA ? 'xx-xxxxxxx' : '');
          break;
      }
    });
  }

  function respond_to_citizenship_change(data) {
    respond_to_country_change_on_form($('#' + data['form'] + '_signatory_country').val(), 'country-sensitive', data);
  }

  function update_form_status(form) {
    // Don't touch the original form
    $('form[id != "new_' + activeForm + '"]').removeClass('dirty');
    // Mark form as modified unless it's the original form
    if (form != activeForm) {
      $('form[id = "new_' + form + '"]').addClass('dirty');
    }
    // Run country updates
    var field = '#' + form + '_signatory_country';
    respond_to_country_change_on_form($(field).val(), 'country-sensitive', {form: form});
    respond_to_country_change_on_entity_form($(field).val(), 'country-sensitive', {form: form});
  }

  function handle_select_entity(id, kind) {
    var pattern = 'a[href="#entity_' + id + '"][data-type="' + kind + '"]';
    //console.log('handle_select_entity: ' + pattern);
    $(pattern).click();
    update_form_status('entity_' + id);
  }

  function labelsDiffer(source, target) {
    return target !== undefined && target.indexOf(source) < 0;
  }

  function updateLabels(kind, prefix) {
    var labels = formLabels[kind];
    for (var i = 0; i < labels.length; i++) {
      var item = labels[i];
      var pattern = prefix + item['key'];
      //console.log(pattern);
      if (labelsDiffer(item['text'], $(pattern).text())) {
        $(pattern).text(item['text']);
      }
    }
  }

  function gen_select_option(name, current) {
    var selected = name == current ? " selected " : "";
    return '<option value="' + name + '" ' + selected + '>' + name + '</option>';
  }

  function get_position_html(kind, name, value, index, position) {
    switch (kind) {
      case 'trust':
        return '<input tabindex="' + index + '" value="Trustee" class="string optional" readonly type="text" name="' + name + '[position]" id="' + name + '_position">';
      case 'partnership':
        return '<div class="styled-select"> <select tabindex="' + index + '" class="select optional" name="' + name + '[position]" id="' + name + '_position" style="color: rgb(143, 156, 163);"><option value="">Select One</option>' + gen_select_option("General Partner", position) + gen_select_option("Authorized Signatory", position) + '</select></div>';
      default:
        return '<input tabindex="' + index + '" class="string optional" type="text" value="' + position + '" name="' + name + '[position]" id="' + name + '_position">';
    }
  }

  function handle_change_entity_type(id, kind) {
    var old_input = $('#' + id + ' form .form_type > .hidden input:visible');
    var old_type = old_input.val();
    var prefix = '.tab-pane.active > form ';

    updateLabels(kind, prefix);

    if (id != 'entity_new' && old_type != '' && (old_type == kind || !formsInitialized)) {
      return;
    }
    // Update the form type
    //console.log('Changing entity type for: ' + id + ': ' + old_type + ' ==> ' + kind);
    $('#' + id + ' form .form_type > .hidden input').val(kind);

    update_form_status('entity_' + id);

    // Update position input if needed
    var pattern = prefix + '.signatory_form .jq-entity-position .input:visible';
    var entityPositionValue = $('.tab-pane.active > form .signatory_form .jq-entity-position').data('position');
    if (entityPositionValue === undefined) {
      entityPositionValue = "";
    }
    var tabIndex = parseInt($(prefix + '.signatory_form:visible').find('input').first().attr('tabindex')) + 5; // tabindex of signatory first name
    var labelHtml = '<label class="string optional control-label" for="' + id + '_position">' + $(pattern + ' label').html() + '</label>';
    var inputHtml = get_position_html(kind, id, '', tabIndex, entityPositionValue);
    $(pattern).html(labelHtml + inputHtml);
  }

  $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    var target = $(e.target).attr("href"); // activated tab

    switch (target) {
      case '#individual':
        $.fn.autoScroll.focus_unless_error('#individual_form_first_name');
        update_form_status('individual_form');
        FF.AnalyticsTracker.profileIndividual(gon.offeringId);
        break;
      case '#entity':
        $.fn.autoScroll.scrollUp('#entity-new');
        update_form_status();
        FF.AnalyticsTracker.profileEntity(gon.offeringId);
        break;
      case '#entity-new':
        $.fn.autoScroll.scrollUp('#entity-types');
        FF.AnalyticsTracker.profileEntity(gon.offeringId);
        break;
      default:
        //console.log(target);
        //console.log($(e.target).data('type'));
        $.fn.autoScroll.focus_unless_error(target + '_name');
        var form = target.substring(1);
        var id = form.substring(form.lastIndexOf('-') + 1);
        if (id !== undefined) {
          if (id == form) {
            var kind = $(e.target).data('type');
            handle_change_entity_type(id, kind);
            FF.AnalyticsTracker.profileEntityType(gon.offeringId, kind);
          } else {
            handle_select_entity(id, $(e.target).data('type'));
          }
        }
        break;
    }
  });

  $('button[type=submit]').on('click', function () {
    FF.AnalyticsTracker.profileSubmit(gon.offeringId);
  });

  $('a').filter(function (index) {
    return $(this).text() === 'Finish Later';
  }).on('click', function () {
    FF.AnalyticsTracker.profileLater(gon.offeringId);
  });

  // auto inputs masking
  if ($('input[data-masked-date="true"]').length > 0) {
    $('input[data-masked-date="true"]').mask('99/99/9999', {placeholder: 'mm/dd/yyyy', autoclear: false})
  }

  (function(exports) {
    exports.respond_to_country_change_on_form = respond_to_country_change_on_form;
    exports.respond_to_country_change_on_entity_form = respond_to_country_change_on_entity_form;
    exports.respond_to_citizenship_change = respond_to_citizenship_change;
  }(window));

});
