$.scope('#offering-invest-investment-documents', function(ignore) {
  var $submitButton = $("button[type=submit]", '#investment-documents-form');

  $('#reviewed-information-checkbox').change(function() {
    $('.error[data-error-for="#reviewed-information-checkbox"]').addClass('hidden');
    $submitButton.prop('disabled', false);
  });

  $('#investment-documents-form').unbind('submit');
  $('#investment-documents-form').submit(function(e) {

    e.preventDefault();
    var success_fn = function(data, status, xhr) {
      $('#sign-documents-modal .modal-body').html(data);
      var height = $(window).height() - 170; // eyeballing it
      $('#sign-documents-modal iframe').css('height', height);
      $('#sign-documents-modal').modal();
    };

    var error_fn = function(data, status, xhr) {
      alert("There was an error loading the documents for this offering");
      $('#spinner').addClass('hidden');
      $submitButton.prop('disabled', false);
    };

    var complete_fn = function(data, status, xhr) {
      $('#spinner').addClass('hidden');
      $submitButton.prop('disabled', false);
    };

    var render_errors = function() {
      $('#review .error.hidden').removeClass('hidden');
      var errors = $('#errors');
      errors.parent().addClass('errors');
      errors.html('<p>Please review the errors below</p>');
      $.fn.autoScroll.focus_unless_error("#reviewed-information-checkbox");
    };

    FF.AnalyticsTracker.reviewSubmit(gon.offering_id);

    if($('#reviewed-information-checkbox').prop('checked')) {
      $('#spinner').removeClass('hidden');
      $('#errors').addClass('hidden');
      $.ajax({
        type: "POST",
        data: {reviewed_information: "on"},
        url: gon.reviewed_info_endpoint,
        success: success_fn,
        error: error_fn,
        complete: complete_fn
      });
    } else {
      render_errors();
    }
  });

  $('a').filter(function(ignore) { return $(this).text() === 'Finish Later'; }).on('click', function() {
    FF.AnalyticsTracker.reviewLater(gon.offering_id);
  });
});
