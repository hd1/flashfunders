$.scope('#browse-offerings', function(page) {
  'use strict';

  function compare(field, inverse) {
    return function(a, b) {
      var blankA = $.isEmptyObject($(a).data());
      var blankB = $.isEmptyObject($(b).data());
      if (blankA && !blankB) { return 1; }
      if (!blankA && blankB) { return -1; }
      var pendingA = $(a).data('closed-pending') == 1;
      var pendingB = $(b).data('closed-pending') == 1;
      if (pendingA && !pendingB) { return 1; }
      if (!pendingA && pendingB) { return -1; }
      // If neither or both are pending, apply requested sort
      var result = parseInt($(a).data(field)) - parseInt($(b).data(field));
      return inverse ? -1 * result : result;
    };
  }

  function resortTiles(which, order) {
    var comparatorFn;

    switch (order) {
      case 'newest':
        comparatorFn = compare('days-left', true);
        break;
      case 'closing_soon':
        comparatorFn = compare('days-left', false);
        break;
      case 'money_raised':
        comparatorFn = compare('amount-raised', true);
        break;
      default:
        comparatorFn = compare('pct-complete', true);
        break;
    }
    $('.currently-raising.' + which + ' .offering-card').sortElements({ comparator: comparatorFn });
    FF.AnalyticsTracker.browseSort(order);
  }

  $('#browse_sort_reg_cf_offering').on('change', function() {
    resortTiles('reg_cf', $(this).val());
  });

  $('#browse_sort_reg_d_offering').on('change', function() {
    resortTiles('reg_d', $(this).val());
  });

  $(document).ready(function() {
    resortTiles('reg_cf', 'pct_complete');
    resortTiles('reg_d', 'pct_complete');
  });
});
