$.scope('#investment-funds', function (page) {
  'use strict';

  var $topDivider = $('.workspace-header-divider');
  var active_form = null;
  var pageLoaded = false;
  var routingAjaxEndpoint = '/api/lookup_routing_number';

  var getActiveForm = function () {
    var forms = $('form:visible');
    if (forms.length == 1) {
      return forms.first();
    } else {
      return undefined;
    }
  };

  var clearTopError = function () {
    $topDivider.removeClass('errors');
    $('#errors').remove();
  };

  var addTopError = function (msg) {
    clearTopError();
    $topDivider.addClass('errors').append('<div id="errors" ><p>' + msg + '</p></div>');
  };

  var displayErrors = function () {
    addTopError('Please select one of the methods below to continue');
  };

  var selectACH = function ($method, $accountForm, $label, $legend, $desc, $auth, $routing) {
    $method.val('ach');
    $legend.text(gon.achRefundTitle);
    $desc.html(gon.achRefundDesc);
    $accountForm.removeClass('hidden');
    $routing.blur();
    $auth.show();
    $label.text('Routing Number');
    $.fn.autoScroll.focus_unless_error('#ach-info form #investor_fund_form_bank_account_holder');
    if (pageLoaded) FF.AnalyticsTracker.fundAch(gon.offeringId);
  };

  var selectWire = function ($method, $accountForm, $label, $legend, $desc, $auth, $routing) {
    $method.val('wire');
    $legend.text(gon.wireRefundTitle);
    $desc.text(gon.wireRefundDesc);
    $accountForm.removeClass('hidden');
    setAccountNumberLabel('');
    $auth.hide();
    $label.text('Routing Number / Swift Code');
    $.fn.autoScroll.focus_unless_error('#wire-info form #investor_fund_form_bank_account_holder');
    if (pageLoaded) FF.AnalyticsTracker.fundWire(gon.offeringId);
    $.fn.removeErrors($routing);
  };

  var handlePaymentSelection = function (target) {
    var form = getActiveForm();
    var $method = $('#investor_fund_form_funding_method');
    var $accountForm = $('#link-refund-account-form');
    var $label = $("label[for='investor_fund_form_bank_account_routing']");
    var $legend = $accountForm.find('legend');
    var $desc = $accountForm.find('#description');
    var $auth = $accountForm.find('#authorization');
    var $routing = $('#investor_fund_form_bank_account_routing');

    switch (target) {
      case '#wire-info':
      case 'wire':
        selectWire($method, $accountForm, $label, $legend, $desc, $auth, $routing);
        break;
      case '#ach-info':
      case 'ach':
        selectACH($method, $accountForm, $label, $legend, $desc, $auth, $routing);
        break;
    }
    $(form).areYouSure();
    if (active_form != form && pageLoaded) $(form).addClass('dirty');
    active_form = form;
  };

  var setAccountNumberLabel = function (bankName) {
    var text = 'Account Number';

    if (bankName !== '') {
      text += ' - ' + bankName;
    }
    $('#investor_fund_form_bank_name').val(bankName);
    $('label[for=investor_fund_form_bank_account_number]').text(text);

  };

  var handleRoutingNumberFound = function (data) {
    setAccountNumberLabel(data.name);
  };

  var handleRoutingNumberNotFound = function () {
    var $field = $('#investor_fund_form_bank_account_routing');
    var $error = $field.parent().find('.error');
    var msg = 'Routing number not found';
    if ($error.length === 0) {
      $field.after('<span class="error">' + msg + '</span>');
    } else {
      $error.text(msg).removeClass('invisible');
    }
    setAccountNumberLabel('');
  };

  var lookupRoutingNumber = function (routingNumber) {
    $.getJSON(routingAjaxEndpoint,
      {routing_number: routingNumber},
      function (data) {
        if (data.status == 'found') {
          handleRoutingNumberFound(data);
        } else {
          handleRoutingNumberNotFound();
        }
      });
  };

  $('#investor_fund_form_bank_account_routing').on('blur', function (ignore) {
    var routingNumber = $(this).val().trim();
    var method = $('#investor_fund_form_funding_method').val();
    if (method === 'ach' && routingNumber !== '') {
      lookupRoutingNumber(routingNumber);
    }
  });

  $('button[type="submit"]').click(function () {
    var active_form = getActiveForm();
    if (active_form !== undefined) {
      FF.AnalyticsTracker.fundSubmit(gon.offeringId);
      active_form.submit();
    } else {
      displayErrors();
    }
  });

  $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    var target = $(e.target).attr("href"); // activated tab
    handlePaymentSelection(target);
  });

  if ($('a[href=#ach-info]').length === 0) {
    handlePaymentSelection(gon.fundingMethod);
  } else if (gon.fundingMethod === 'wire') {
    $('a[href=#wire-info]').click();
  } else if (gon.fundingMethod === 'ach') {
    $('a[href=#ach-info]').click();
  }

  $('a').filter(function (ignore) {
    return $(this).text() === 'Finish Later';
  }).on('click', function () {
    FF.AnalyticsTracker.fundLater(gon.offeringId);
  });

  pageLoaded = true;
});
