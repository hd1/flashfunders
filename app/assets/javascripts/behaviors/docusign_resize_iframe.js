$.scope('#sign-documents-modal', function () {
  'use strict';

  function resizeDocusignIframe() {
    var adjustment = 180;
    $('iframe#docusign').height(Math.max($(window).height() - adjustment, 400));
  }

  $(window).resize(resizeDocusignIframe);

  resizeDocusignIframe();
});

$.scope('#sign-documents', function () {
  'use strict';

  function resizeDocusignIframe() {
    var adjustment = 180;
    $('iframe#docusign').height(Math.max($(window).height() - adjustment, 400));
  }

  $(window).resize(resizeDocusignIframe);

  resizeDocusignIframe();
});
