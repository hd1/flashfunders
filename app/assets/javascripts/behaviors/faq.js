$.scope('#faq-page', function (page) {
  var $window =  $(window),
    $categories = $('.faq-nav'),
    categoriesTop = $categories.offset().top,
    categoriesNavHeight = $categories.outerHeight(),
    categoriesPaddingTop = parseFloat($categories.css("padding-top")),
    bottomPoint = $('footer').offset().top;

  var stickyCategoriesNav = function() {
    var stopPointOffset = bottomPoint - (categoriesNavHeight + categoriesPaddingTop);

    if ($window.scrollTop() > stopPointOffset) {
      $categories.removeClass("pinned");
      $categories.offset({top: stopPointOffset});
    } else {
      $categories.addClass("pinned");
      $categories.css("position", "");
      $categories.css("top", "");
    }
  };

  var scrollTo = function(target) {
    if (target !== undefined) {
      $('html,body').animate({scrollTop: $(target).offset().top}, 250);
    }
  };

  var calculateCategoriesNav = function() {
    if (im.lessThan('desktop')) {
      $('.secondary-bg .span8').addClass('span12');
      $('.secondary-bg .span12').removeClass('span8');
    } else {
      $('.secondary-bg .span12').addClass('span8');
      $('.secondary-bg .span8').removeClass('span12');
      $categories.find('li').removeClass('active');
      $categories.find('li:first').addClass('active');
    }
  };

  var scrollTo = function(target) {
    var href = $.attr(target, 'href');
    $('html, body').animate({
        scrollTop: $(href).offset().top
    }, 250, function () {
        window.location.hash = href;
    });
    return false;
  };

  $('html,body').scrollspy({target: '#sticky-categories'});

  $('.faq-nav a').on('click', function(){
    scrollTo(this);
  });

  $('.questions-list ul li a').on('click', function() {
    $('.questions-list ul li a').removeClass("active");
    $(this).addClass("active");
    scrollTo(this);
  });

  $window.scroll(function() {
    if (im.greaterThan('desktop')) {
      var hasPassCategoriesTop = $window.scrollTop() > categoriesTop;

      $categories.toggleClass("pinned", hasPassCategoriesTop);
      if (hasPassCategoriesTop) { stickyCategoriesNav(); }
    }
  });

  $window.resize(function() {
    calculateCategoriesNav();
  });

  $(document).ready(function() {
    calculateCategoriesNav();
  });
});
