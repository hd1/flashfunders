$(function () {

  $('#new-session-modal').on('shown.bs.modal', function(){
    FF.AnalyticsTracker.modalShown("Sign In");
  });

  $('#new-registration-modal').on('shown.bs.modal', function(){
    FF.AnalyticsTracker.modalShown("Register");
  });
  $('.flash-notice').delay(5000).fadeOut();
  $('.flash-alert').delay(5000).fadeOut();

  // TODO: Move to appropriate behavior file
  $('#ownership-percentage-offered').ownershipPercentageUpdater();

  // ********************************
  // Track external links
  // ********************************
  $('body')
    .on('click', 'a[href^="//"],a[href^="http"]', function (e) {
      var href = this.href;

      if (!this.hostname.match(window.location.hostname)) {
        FF.AnalyticsTracker.outboundLink(href);
        if (this.target === undefined || this.target !== '_blank') {
          // if opening in this window, give the trackers time to work before following the link
          e.preventDefault();
          setTimeout(function () {
            location.href = href;
          }, 400);
        }
      }
    });

  // ********************************
  // Modals
  // ********************************
  $('.modal')
    .has('.returnable-form')
      .modalFormReturnTo()
      .end()
    .centerModal()
    .soloModal()
    .autoShowModal();

  // **********************************
  // sign-in / sign-up continue banners
  // **********************************
  $('.continue-banner-hide')
    .on('click', function (event) {
      $.each($('.authenticate-before-continue'), function(index, element) {
        $(element).addClass("hidden");
      });
  });

  $('.continue-banner-show')
    .on('click', function (event) {
      $.each($('.authenticate-before-continue'), function(index, element) {
        $(element).removeClass("hidden");
      });
    });

  // ********************************
  // Popovers
  // ********************************
  //
  if(im.lessThan('desktop')) {
    $(document).on('show.bs.popover', function(e) {
      var scrim = $('<div class="popover-scrim"></div>');
      scrim.on('click', function() {
        var popover = $('.popover:visible');
        popover.prev('[data-info-popover]').trigger('click');
        $(this).remove();
      });
      $('body').append(scrim);
    });
  }
  $('[data-info-popover]').each(function() {
      new FF.Popover(this);
  });


  // ********************************
  // Forms
  // ********************************
  PolyfillPlaceholder();

  $('form').submit(function(){
    var btn = $('button[type=submit]', this);
    if (btn.length == 0) {
      btn = $('button[type=submit]');
    }
    $(btn).prop('disabled', true);
    var spinner = $('#spinner', this);
    if (spinner.length == 0) {
      spinner = $('#spinner');
    }
    $(spinner).removeClass('hidden');
  });

  $('form').errorRemover();

  $('select').customizeSelects();

  $('.country-selector').stateChooser({ajaxEndpoint: '/api/regions'});

  $('input[type="radio"]:not(".custom-radio")').customBinaryRadioButtons();

  $([
    'form[action*="/investor/"]',
    'form[action*="/invest/"]',
    'form[action$="/invest"]'
  ].join(', ')).submit(function(e) { $(e.currentTarget).find('input[type=submit]').prop('disabled', true).addClass('disabled'); });

  $('.autoscroll').autoScroll();
  $('.btn-payment-ajax').paymentButtons();
  $("#idle-session-modal").userSession({ 'url': '/api/users/session_info' });

  // ********************************
  // Images
  // ********************************
  $('.expandable').expandElements();

});

