$.scope('#style-guide', function () {
  'use strict';

  function displayFontInfo($el, attrs) {
    var result = [],
      styleProps = $el.css(attrs),
      name = $el[0].nodeName;

    if (name === 'DIV') {
      name = $el.attr('class').split(' ')[0].toUpperCase();
    }
    $.each(styleProps, function (prop, val) {
      result.push(prop + ": " + val);
    });
    $el.html("<div class='tile'>" + name + "<div class='text'>" + result.join('<br>') + '</div></div>');
  }

  function renderInfo($el, displayVal, val, attr) {
    var name = '$' + $el.attr('class').split(' ')[0];

    $el.css('height', 'inherit');
    $el.html("<div class='tile'/><div class='text'>" + name + '<br>' + displayVal + '</div>');
    $el.children('.tile').css(attr, val);
    $el.css(attr, 'white');
  }

  function rgb2hex(rgb) {
    if (rgb === null) {
      return '#unknown';
    }
    if (rgb.search("rgb") === -1) {
      return rgb;
    }
    rgb = rgb.match(/^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*([\d\.]+))?\)$/);
    function hex(x) {
      return ("0" + parseInt(x).toString(16)).slice(-2);
    }
    var alpha = '';
    if (rgb[4] !== undefined) {
      alpha = ', ' + parseFloat(rgb[4]).toFixed(2).toString();
    }
    return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]) + alpha;
  }

  function displayColorInfo($el, attr) {
    var rgbcolor = $el.css(attr);
    var color = rgb2hex(rgbcolor);

    renderInfo($el, color, rgbcolor, attr);
  }

  function displayStyleInfo($el, attr) {
    var val = $el.css(attr);

    renderInfo($el, val, val, attr);
  }

  function annotateFonts() {
    $('#typography .style-info').each(function(ignore, el){
      displayFontInfo($(el), ['font-size', 'font-weight', 'line-height']);
    });
  }

  $(window).bind('resize', function(ignore) {
    $(window).resize(function() {
      clearTimeout(window.resizeEvt);
      window.resizeEvt = setTimeout(function() {
        annotateFonts();
      }, 250);
    });
  });

  $(document).ready(function() {
    annotateFonts();
    $('#colors .style-info').each(function(ignore, el){
      displayColorInfo($(el), 'background-color');
    });
    $('#gradients .style-info').each(function(ignore, el){
      displayStyleInfo($(el), 'background');
    });
    $('#spacing .style-info').each(function(ignore, el){
      displayStyleInfo($(el), 'height');
    });
  });

});
