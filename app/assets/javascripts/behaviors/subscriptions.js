function simpleSubscription(form) {
  form
    .on('submit', function () {
      // Prevent double clicks
      $('input[type=submit]', this).attr('disabled', true);
    })
    .on('ajax:error', function (xhr, data, status) {
      $('.errors', this).html("Provide a valid email address.");
      $('input[type=submit]', this).attr('disabled', false);
    })
    .on('ajax:success', function (xhr, data, status) {
      $('.subscription-header').addClass('hidden');
      $('.subscription-input', form).addClass("hidden");
      $('.subscription-success', form).removeClass("hidden");
      showHideSubscriptions(form.attr('id'));
    });
}

function showHideSubscriptions(id) {
  FF.resetCookies(); // Make sure cookies get reloaded
  var $panel = $('.subscription-section');
  var subscribed = FF.readCookie("_subscribed") === 'true';

  if (subscribed || FF.readCookie('_dismissed') === 'true') {
    if (id !== 'email-capture-banner') {
      $('#email-capture-banner').hide();
    }
  }
  if (subscribed) {
    if (id !== 'home_new_subscription' &&
        id !== 'browse_new_subscription' &&
        id !== 'email_new_subscription' &&
        id !== '/subscribe') {
      $panel.hide();
    }
  } else {
    $panel.show();
  }
}

function dismissEmailCapture() {
  $('#email_capture_dismiss')
    .on('ajax:success', function (xhr, data, status) {
      showHideSubscriptions($(this).attr('id'));
    });
}

function showNewsletterModal() {
  $('#newsletter-modal').modal('show');
}

function updateSourcePage() {
  $("input#subscription_source_page").attr('value', location.pathname + location.search);
}

var minutesAgo = function () {
  var secondsAgo = (new Date().getTime() - (FF.readCookie("_first_visit") * 1000)) / 1000;
  return minutesAgo = secondsAgo / 60;
};

$.scope('#what_is_ff_new_subscription', function () {
  simpleSubscription(this);
  FF.AnalyticsTracker.subscriptionWhatIsFFPage();
});

$.scope('#email_new_subscription', function () {
  simpleSubscription(this);
  FF.AnalyticsTracker.subscriptionEmail();
});

$.scope('#home_new_subscription', function () {
  simpleSubscription(this);
  FF.AnalyticsTracker.subscriptionHomePage();
});

$.scope('#browse_new_subscription', function () {
  simpleSubscription(this);
  FF.AnalyticsTracker.subscriptionBrowse();
});

$.scope('#email-capture-banner', function () {
  simpleSubscription(this);
  dismissEmailCapture();
});

$("#newsletter-modal").on('shown.bs.modal', function () {
  simpleSubscription($('#newsletter-modal-form'));
});

$(document).ready(function () {
  showHideSubscriptions(document.location.pathname);
  updateSourcePage();
  if (minutesAgo() > 30) {
    showNewsletterModal();
  }
});
