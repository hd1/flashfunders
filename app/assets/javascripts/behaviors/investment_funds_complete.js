$.scope('#investment-funds-complete', function (page) {
  'use strict';

  $('.edit-btn').click(function (e) {
    e.stopPropagation();

    var id = $(e.target).data('offering-id');
    if ($(e.target).hasClass('edit-payment')) {
      FF.AnalyticsTracker.successEditPayment(id);
    } else if ($(e.target).hasClass('edit-verification')) {
      FF.AnalyticsTracker.successEditAccreditation(id);
    }
  });

  $('.btn-payment-ajax').click(function (e) {
    var id = $(e.target).data('offering-id');
    FF.AnalyticsTracker.successFundsSent(id);
  });

  $('a').filter(function (index) {
    return $(this).text() === 'Download Docs';
  }).click(function (e) {
    var id = $(e.target).data('offering-id');
    FF.AnalyticsTracker.successDownloadDocs(id);
  });

});
