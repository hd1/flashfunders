$.scope('#invest_accreditation_new', function() {
  'use strict';
  var active_form = gon.form_class;
  var active_form_selected = false;
  var pageLoaded = false;
  var trackerFn = null;

  $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    var target = $(e.target).attr("href"); // activated tab
    var form = null;
    switch (target) {
      case '#income':
        $.fn.autoScroll.scrollUp('#third-party-or-docs');
        trackerFn = FF.AnalyticsTracker.verifyIndividualIncome;
        break;
      case '#net-worth':
        $.fn.autoScroll.scrollUp('#net-worth-third-party-or-docs');
        trackerFn = FF.AnalyticsTracker.verifyIndividualNetWorth;
        break;
      case '#income-third-party':
        $.fn.autoScroll.focus_unless_error("#third_party_income_form_role");
        form = 'third_party_income_form';
        trackerFn = FF.AnalyticsTracker.verifyIndividualThirdParty;
        break;
      case '#net-worth-third-party':
        $.fn.autoScroll.focus_unless_error("#third_party_net_worth_form_role");
        form = 'third_party_net_worth_form';
        trackerFn = FF.AnalyticsTracker.verifyIndividualThirdParty;
        break;
      case '#net-worth-doc':
        $.fn.autoScroll.scrollUp("#net-worth-joint-or-individual");
        trackerFn = FF.AnalyticsTracker.verifyIndividualDocs;
        break;
      case '#income-doc':
        $.fn.autoScroll.scrollUp("#income-joint-or-individual");
        trackerFn = FF.AnalyticsTracker.verifyIndividualDocs;
        break;
      case '#individual-net-worth':
      case '#joint-net-worth':
      case '#individual-income':
      case '#joint-income':
        $.fn.autoScroll.scrollUp(target + " form");
        form = target.substring(1).replace(/-/g, '_') + '_form';
        if (target.indexOf('joint') > 0) {
          trackerFn = FF.AnalyticsTracker.verifyIndividualDocsJoint;
        } else {
          trackerFn = FF.AnalyticsTracker.verifyIndividualDocsIndividual;
        }
        break;
    }
    if (active_form_selected && form !== active_form) {
      $('form').removeClass('dirty');
      $('#new_' + form).addClass('dirty');
    }
    if (pageLoaded && trackerFn !== null) {
      trackerFn(gon.offering_id);
    }
  });

  function openForm(income_or_net_worth, doc_or_third_party, joint_or_individual) {
    $('a[href=#' + income_or_net_worth + ']').click();
    $('a[href=#' + income_or_net_worth + "-" + doc_or_third_party + ']').click();
    if (doc_or_third_party === 'doc') {
      $('a[href=#' + joint_or_individual + "-" + income_or_net_worth + ']').click();
    }
  }

  $( function(){
    switch (active_form) {
      case 'individual_income_form':
        openForm('income', 'doc', 'individual');
        break;
      case 'joint_income_form':
        openForm('income', 'doc', 'joint');
        break;
      case 'individual_net_worth_form':
        openForm('net-worth', 'doc', 'individual');
        break;
      case 'joint_net_worth_form':
        openForm('net-worth', 'doc', 'joint');
        break;
      case 'third_party_income_form':
        openForm('income', 'third-party', '');
        break;
      case 'third_party_net_worth_form':
        openForm('net-worth', 'third-party', '');
        break;
    }
    active_form_selected = true;
    var form_name = '#new_' + active_form;
    $(form_name).areYouSure();
    pageLoaded = true;
  });

  (function($) {

    function removeCheckboxErrors(e) {
      $(e.currentTarget).closest('.input').removeClass('field_with_errors').parent().next().find('.error').removeClass('error');
    }

    $.fn.checkboxErrorRemover = function () {
      $(this).find('input[type="checkbox"]').on('change', removeCheckboxErrors);
    };
  })(jQuery);

  $('form').checkboxErrorRemover();

  $('input').keydown(function(event) {
    if(event.keyCode === 13){
      var form = this.closest('form').id;
      if (active_form_selected && form !== active_form) {
        $('form').removeClass('dirty');
      }
    }
  });

  $('.file-chooser-btn').on('click', function() { $('.file-key-error').remove(); });

  $('button[type=submit]').on('click', function() {
    FF.AnalyticsTracker.verifySubmit(gon.offering_id);
  });

  $('a').filter(function(index) { return $(this).text() === 'Finish Later'; }).on('click', function() {
    FF.AnalyticsTracker.verifyLater(gon.offering_id);
  });

});
