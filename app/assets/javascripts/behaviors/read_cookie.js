(function(){
  var cookies;

  var initCookie = function() {
    var rawCookies = document.cookie.split('; '),
      cookiesHash = {};

    for(var i = rawCookies.length - 1; i >= 0; i--){
      var cookieItem = rawCookies[i].split('=');
      var cookieName = cookieItem[0];
      var cookieValue = cookieItem[1];

      cookiesHash[cookieName] = cookieValue;
    }

    return cookiesHash;
  };

  var readCookie = function(name) {
    if (! cookies) {
      cookies = initCookie();
    }
    return cookies[name];
  };

  FF.resetCookies = function() { cookies = null; };
  FF.readCookie = readCookie;
})();
