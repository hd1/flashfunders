$.scope('#edit_stakeholders', function (page) {
  'use strict';

  $(document).on('click', 'button[data-target=".content-creation"]', function(e) {
    var checks = $('.notable-investor input[type="checkbox"]');
    if(checks.length == 1) {
      var small = $('.notable-investor small');
      small.fadeIn();
      setTimeout(function() {
        small.removeAttr('style');
      }, 20000);
    }
  });

  $(document).on('click', '.notable-investor', function(e) {
    var checks = $('.notable-investor input[type="checkbox"]');
    var this_check = $(this).find('input[type="checkbox"]');
    var was_checked = this_check.is(":checked");
    $.each(checks, function(idx, elem) {
      $(elem).prop('checked', false);
    });
    if(!was_checked) {
      this_check.prop('checked', true);
    }
    return false;
  });
});

