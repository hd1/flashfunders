$.scope('#issuer-application', function (ignore) {
  'use strict';

  function showHidePartner() {
    var $lead = $('#issuer_application_form_lead_source');
    var $partner = $('.issuer_application_form_preferred_partner');
    if ($lead.val() === 'option8') {
      $partner.removeClass('hidden');
    } else {
      $partner.addClass('hidden');
    }
  }

  $('#issuer_application_form_lead_source').change(function() {
    showHidePartner();
  });

  $(function() {
    showHidePartner();
  });
});
