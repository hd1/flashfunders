$.scope('#pitch-editor', function (page) {
  'use strict';

  var $saveBtn = $('.save-form', page),
      $form = $('.content-editor-form', page);

  function callbackAddTemplate(template) {
    $form.rankable('resetButtons');
    var textarea = template[0].getElementsByTagName('textarea')[0];

    if (textarea) {
      CKEDITOR.remove(textarea.name);
      CKEDITOR.replace(textarea.name);
    }
  }

  function callbackRemoveTemplate(template) {
    $form.rankable('resetButtons');
  }

  $form
    .unsavedFormChanges()
    .rankable()
    .contentEditor({
      onAddTemplate: callbackAddTemplate,
      onRemoveTemplate: callbackRemoveTemplate
    });

  $saveBtn
    .progressButton({onComplete: function() { $form.submit(); }});

});
