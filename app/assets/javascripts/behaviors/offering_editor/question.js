$.scope('#questions-editor', function(page) {

  var $saveBtn = $('.save-form', page),
      $form = $('.content-editor-form', page);

  function updateRankable(template) {
    $form.rankable('resetButtons');
  }

  $form
    .unsavedFormChanges()
    .rankable()
    .contentEditor({
      onAddTemplate: updateRankable,
      onRemoveTemplate: updateRankable
    });

  $saveBtn.click(function(event) {
    event.preventDefault();
    $form.submit();
  });

});


