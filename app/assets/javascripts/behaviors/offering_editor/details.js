$.scope('#company-details', function(page) {
  'use strict';
  var $saveBtn = $('.save-form', page)
    , $form = $('.content-editor-form', page);

  $form
    .unsavedFormChanges()
    .contentEditor();

  $saveBtn.progressButton({onComplete: function() { $form.submit(); }});

  var $input = $('.vanity-path-controls input');
  $input.focus(function() { $('.vanity-path-controls').addClass('focused'); });
  $input.blur(function() { $('.vanity-path-controls').removeClass('focused'); });

  $('.uploadable', page).uploadable({displayType: 'image'});
  $('.uploadable-image-header', page).uploadable({displayType: 'image', validateRatio: [945, 450], maxFileSize: 300});
  $('.uploadable-browse-image', page).uploadable({displayType: 'image', validateRatio: [740, 330], maxFileSize: 300});
  $('.uploadable-square-image', page).uploadable({displayType: 'image', validateRatio: [40, 40]});

  $('#offering_tagline_browse').charCount({
    allowed: 100,
    warning: 0,
    counterText: 'Chars left: '
  });

});
