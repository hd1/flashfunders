$.scope('#members-editor', function(page) {
  'use strict';
  var $saveBtn = $('.save-form', page)
    , $form = $('.content-editor-form', page);

  $form
    .unsavedFormChanges()
    .rankable()
    .contentEditor({
      onAddTemplate: function(template) {
        // enable/disable rank buttons
        $form.rankable('resetButtons');

        // add upload event to added template
        $('.uploadable', template).uploadable({displayType: 'image'});
      },
      onRemoveTemplate: function(template) {
        $form.rankable('resetButtons');
      }
    });

  $saveBtn.progressButton({onComplete: function() { $form.submit(); }});

  $('.uploadable', page).uploadable({displayType: 'image'});
});

