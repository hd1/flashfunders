$.scope('#investment-dashboard', function (page) {

  'use strict';

  function resend_verification_email(e) {
    var btn = $(e.target);
    $.ajax({
      type: 'POST',
      url: btn.data('url'),
      data: {},
      dataType: 'json',
      success: function (data) {
        btn.text('Sent');
        btn.prop('disabled', true);
      },
      failure: function () {
        console.log('ajax call failed');
      }
    });
  }

  var confirmCancel = function (path) {
    $('#cancel-investment-dialog').dialog({
      dialogClass: 'confirm-dialog',
      modal: true,
      draggable: false,
      resizable: false,
      appendTo: '#investment-dashboard',
      buttons: [{
        text: 'Cancel',
        class: 'btn btn-medium',
        click: function () {
          $(this).dialog("close");
        }
      },
      {
        text: 'Yes',
        type: 'submit',
        class: 'btn btn-medium btn-primary',
        click: function () {
          $('form', this).submit();
        }
      }],
      open: function(event, ui) {
        $('form', this).attr('action', path);
      }
    });
  };

  $('.resend-btn.resend-verification').click(function (e) {
    e.stopPropagation();

    resend_verification_email(e);
    var id = $(e.target).data('offering-id');
    FF.AnalyticsTracker.dashResendAccreditation(id);
  });

  $('.edit-btn').click(function (e) {
    e.stopPropagation();

    var id = $(e.target).data('offering-id');
    if ($(e.target).hasClass('edit-payment')) {
      FF.AnalyticsTracker.dashEditPayment(id);
    } else if ($(e.target).hasClass('edit-verification')) {
      FF.AnalyticsTracker.dashEditAccreditation(id);
    }
  });

  $('.btn-payment-ajax').click(function (e) {
    var id = $(e.target).data('offering-id');
    FF.AnalyticsTracker.dashFundsSent(id);
  });

  $('a').filter(function (index) {
    return $(this).text() === 'Download Docs';
  }).click(function (e) {
    var id = $(e.target).data('offering-id');
    FF.AnalyticsTracker.dashDownloadDocs(id);
  });

  $(document).ready(function() {
    FF.confirmCancel = confirmCancel;
  });

});
