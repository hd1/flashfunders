$.scope('#offering-invest-accreditation-verified', function(ignore) {

  $('button[type=submit]').on('click', function() {
    FF.AnalyticsTracker.verifySubmit(gon.offering_id);
  });

  $('a').filter(function(ignore) { return $(this).text() === 'Finish Later'; }).on('click', function() {
    FF.AnalyticsTracker.verifyLater(gon.offering_id);
  });
});
