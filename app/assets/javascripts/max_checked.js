(function($) {

  function disableOrEnableCheckboxes($el, maxChecked) {
    if ($el.filter(':checked').length > maxChecked - 1) {
      $el.filter(':not(:checked)').prop('disabled', 'disabled');
    } else {
      $el.filter(':disabled').prop('disabled', false);
    }
  }

  $.fn.maxChecked = function(maxChecked) {
    var $el = this;

    disableOrEnableCheckboxes($el, maxChecked);

    $(this).change(function(e) {
      disableOrEnableCheckboxes($el, maxChecked);
    });
    return this;
  };
})(jQuery);