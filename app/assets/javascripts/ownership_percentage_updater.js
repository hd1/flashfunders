(function ($) {
  function template(ownershipPercentage) {
    return "You're offering <div class='side-info--info h1'>" + ownershipPercentage + "%</div>";
  }

  function setPercentageString($el, $preMoneyField, $campaignGoalField) {
    var preMoneyValue = Number($preMoneyField.val());
    var campaignGoalValue = Number($campaignGoalField.val());

    var ownershipPercentage = String(((campaignGoalValue / (campaignGoalValue + preMoneyValue)) * 100)).substring(0, 5);

    if (preMoneyValue && campaignGoalValue) {
      $el.html(template(ownershipPercentage));
    } else {
      $el.html('');
    }
  };

  $.fn.ownershipPercentageUpdater = function () {
    var $el = this;
    var $preMoneyField = $(this.data('pre-money-valuation-selector'));
    var $campaignGoalField = $(this.data('campaign-goal-selector'));

    $preMoneyField.keyup(function () {
      setPercentageString($el, $preMoneyField, $campaignGoalField);
    });

    $campaignGoalField.keyup(function () {
      setPercentageString($el, $preMoneyField, $campaignGoalField);
    });

    setPercentageString($el, $preMoneyField, $campaignGoalField);

    return this;
  };
})(jQuery);
