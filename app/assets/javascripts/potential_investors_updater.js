(function ($) {
  function template(potentialInvestors) {
    return 'Potential # of Investors <div class="side-info--info h1">' + potentialInvestors + '</div>';
  }

  function setInvestorString($el, $minimumInvestmentAmountField, $campaignGoalField) {
    var minimumInvestmentAmountValue = Number($minimumInvestmentAmountField.val());
    var campaignGoalValue = Number($campaignGoalField.val());

    var potentialInvestors = String(Math.floor((campaignGoalValue / minimumInvestmentAmountValue)));

    if (minimumInvestmentAmountValue && campaignGoalValue) {
      $el.html(template(potentialInvestors));
    } else {
      $el.text('');
    }
  };

  $.fn.potentialInvestorsUpdater = function () {
    var $el = this;
    var $minimumInvestmentAmountField = $(this.data('minimum-investment-amount-selector'));
    var $campaignGoalField = $(this.data('campaign-goal-selector'));

    $minimumInvestmentAmountField.keyup(function () {
      setInvestorString($el, $minimumInvestmentAmountField, $campaignGoalField);
    });

    $campaignGoalField.keyup(function () {
      setInvestorString($el, $minimumInvestmentAmountField, $campaignGoalField);
    });

    setInvestorString($el, $minimumInvestmentAmountField, $campaignGoalField);

    return this;
  };
})(jQuery);
