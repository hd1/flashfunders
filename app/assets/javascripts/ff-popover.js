(function(exports) {

  var Popover = function (element) {
    var $popoverTarget = $(element);

    var trigger = 'hover';

    if(im.lessThan('desktop')) {
      trigger = 'click';
    }

    $popoverTarget.popover({
      placement: 'top',
      trigger: trigger,
      html: true,
      delay: { show: 50, hide: 400 }
    });
  };

  var originalLeave = $.fn.popover.Constructor.prototype.leave;
  $.fn.popover.Constructor.prototype.leave = function(obj){
    var self = obj instanceof this.constructor ?
      obj : $(obj.currentTarget)[this.type](this.getDelegateOptions()).data('bs.' + this.type);
    var container, timeout;

    originalLeave.call(this, obj);

    if(obj.currentTarget) {
      container = $(obj.currentTarget).siblings('.popover');
      timeout = self.timeout;
      container.one('mouseenter', function(){
        //We entered the actual popover – call off the dogs
        clearTimeout(timeout);
        //Let's monitor popover content instead
        container.one('mouseleave', function(){
          $.fn.popover.Constructor.prototype.leave.call(self, self);
        });
      });
    }
  };

  exports.FF.Popover = Popover;

})(window);

