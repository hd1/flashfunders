$(document).ready(function(){
  $('.modal').not('#sign-documents-modal, #idle-session-modal').on('hide.bs.modal', function (e) {
    if (!($(this).hasClass('in'))) return;
    e.preventDefault();
    $(this)
      .removeClass('in')
      .attr('aria-hidden', true)
      .off('click.dismiss.bs.modal')
      .fadeOut("slow");
    $('body').removeClass('modal-open');
    $('.modal-backdrop').remove();
    $(this).data('bs.modal').isShown = false;
  });
});
