(function (exports) {
  var InvestmentFormView = Backbone.View.extend({
    initialize: function () {
      this.$containerEl = this.options.$containerEl;
      this.authenticity_token_param_name = $('meta[name=csrf-param]').attr('content');
      this.authenticity_token_value = $('meta[name=csrf-token]').attr('content');
      this.canSubmit = false;

      this.$el.on('keyup', '#investment_unrounded_investment_amount',
        _.bind(this._handleProposedInvestmentChanged, this));

      this.$el.on('keydown', '#investment_unrounded_investment_amount',
        _.bind(this._blockSubmitOnKeyDown, this));

      this.$el.on('focus', '#investment_unrounded_investment_amount', function () {
        $('button[type=submit]').prop('disabled', false);
      });

      $('#new_investment').on("submit", _.bind(this._delayFormSubmission, this));
    },

    render: function () {
      var initial_amt = this.model.get('unrounded_investment_amount');
      this.$el.html(this.template(_.extend({
        authenticity_token_value: this.authenticity_token_value,
        authenticity_token_param_name: this.authenticity_token_param_name,
        form_method: this.options.formMethod,
        form_action: this.options.formAction,
        presented_investment_amount: this._presentAmount(initial_amt),
        maximum_investment_amount: this.model.get('offering_maximum_investment')
      }, this.model.toJSON(), this.errorMessages())));
      this._handleInvestmentChange(initial_amt);
      if (this._isSpvOffering()) {
        $('#notes_direct', this.$el).addClass('hidden');
        $('#notes_spv', this.$el).removeClass('hidden');
      } else {
        $('#notes_direct', this.$el).removeClass('hidden');
        $('#notes_spv', this.$el).addClass('hidden');
      }
      this._addErrors();
      this.$containerEl.html(this.$el);
    },

    errorMessages: function () {
      if (!this.errors) {
        this.errors = {};
        function errorMessageOrDefault(field) {
          var errorHash = this.model.get('errors');
          return errorHash && errorHash[field] ? errorHash[field][0] : null;
        }

        this.errors['unrounded_investment_amount_error_message'] = _.bind(errorMessageOrDefault, this)('unrounded_investment_amount');
      }
      return this.errors;
    },

    _addErrors: function () {
      var errorMessages = this.errorMessages();
      var fieldsNeedingErrorClass = _.keys(errorMessages);

      var compactedFieldsNeedingErrorClass = _.reject(fieldsNeedingErrorClass, function (key) {
        return errorMessages[key] === null;
      });

      if (compactedFieldsNeedingErrorClass.length) {
        this.$('.input.investment_unrounded_investment_amount').addClass('field_with_errors');
      }
    },

    _blockSubmitOnKeyDown: function (e) {
      if (e.which >= 48 && e.which <= 57) {
        this.canSubmit = false;
      }
    },

    _handleProposedInvestmentChanged: function (e) {
      if (e.which != 13) {
        // Block submit if enter key is hit multiple times.
        this._handleInvestmentChange($(e.target).val());
        this.canSubmit = true;
        $('button[type=submit]').prop('disabled', false);
        $('*').css('cursor', '');
        var spinner = $('#spinner', this);
        if (spinner.length == 0) {
          spinner = $('#spinner');
        }
        $(spinner).addClass('hidden');
      }
    },

    _handleInvestmentChange: function (amount) {
      var data = this._performCalcs(amount);
      this._updateForm(data.projectedCost, data.projectedShares, data.projectedOwnershipPct);
    },

    _getCalcArgs: function (val, model) {
      return {
        'amount': val,
        'total': model.get('offering_maximum_investment'),
        'modelDirectSpv': this._isSpvOffering(),
        'spvTotal': model.get('spv_total_investment'),
        'goal': model.get('spv_min'),
        'threshold': model.get('direct_investment_threshold'),
        'setupFee': model.get('spv_fee'),
        'sharePrice': model.get('share_price'),
        'sharesOffered': model.get('shares_offered'),
        'valuation': model.get('pre_money_valuation')
      };
    },

    _isSpvOffering: function () {
      return this.model.get('has_spv') === 'true';
    },

    _isUnderDirectThreshold: function (investmentAmount) {
      return Number(investmentAmount) < Number(this.model.get('direct_investment_threshold'));
    },

    _isSpv: function (investmentAmount) {
      return this._isSpvOffering() && this._isUnderDirectThreshold(investmentAmount);
    },

    _updateForm: function (cost, shares, ownership) {
      this._updateFormFields(cost, shares, ownership);
      this._updateWidgetCopy(cost, shares, ownership);
    },

    _formatForPage: function (val, precision) {
      return (val === undefined || !$.isNumeric(val) || val == 0 ? '--' : accounting.formatNumber(val, precision));
    },

    _updateFormFields: function (investmentAmount, numberOfShares, percentOwnership) {
      this.$('#investment_investment_amount').val(investmentAmount);
      this.$('#investment_investment_shares').val(numberOfShares);
      this.$('#investment_investment_percent_ownership').val(percentOwnership);
    },

    _presentAmount: function (rawInvestmentAmount) {
      if (isNaN(rawInvestmentAmount)) {
        return rawInvestmentAmount;
      }
      var investmentAmount = parseFloat(rawInvestmentAmount);
      if (investmentAmount == 0) {
        return "";
      }
      return investmentAmount > 0 ? (Math.floor(investmentAmount * 100) / 100) : rawInvestmentAmount;
    },

    _delayFormSubmission: function (e) {
      $('*').css('cursor', 'wait');
      var $input = $('#investment_unrounded_investment_amount');
      // prevent further updates to amount, and force the update
      $input.prop('readonly', true);
      if (this.canSubmit) {
        FF.AnalyticsTracker.investAmount(this.model.get('offering_id'));
        return true;
      }
      $input.prop('readonly', false);
      $('*').css('cursor', '');
      return false;
    },

  });

  var InvestmentStockFormView = InvestmentFormView.extend({
    template: JST['investor_investments/templates/form_stock'],

    _updateWidgetCopy: function (investmentAmount, numberOfShares, ownershipPercentage) {

      var ownershipPercentagePrecision = ownershipPercentage == 0 ? 0 : 3,
        $widget = this.$('.equity-ownership-widget');

      $widget.html(
        JST['investor_investments/templates/equity_ownership_widget']({
          number_of_shares: this._formatForPage(numberOfShares, 0),
          ownership_percentage: this._formatForPage(ownershipPercentage, ownershipPercentagePrecision),
          investment_amount: this._formatForPage(investmentAmount, 2)
        })
      );

      // Rebind popover
      this.$('[data-info-popover]').each(function () {
        new FF.Popover(this);
      });

      if (this._isSpvOffering()) {
        if (this._isUnderDirectThreshold(investmentAmount)) {
          // SPV Investment
          $('#share_count', $widget).addClass('hidden');
          $('#share_count_caption', $widget).addClass('hidden');
          $('#popover_direct', $widget).addClass('hidden');
          $('#popover_spv', $widget).removeClass('hidden');
        } else {
          // Direct Investment
          $('#share_count', $widget).removeClass('hidden');
          $('#share_count_caption', $widget).removeClass('hidden');
          $('#popover_direct', $widget).removeClass('hidden');
          $('#popover_spv', $widget).addClass('hidden');
        }
      }
    },

    _performCalcs: function (investmentAmount) {
      return new FF.UnitsBasedCalculator(this._getCalcArgs(investmentAmount, this.model));
    }

  });

  var InvestmentNotesFormView = InvestmentFormView.extend({
    template: JST['investor_investments/templates/form_notes'],

    _updateWidgetCopy: function (investmentAmount, ignore, investmentPercentConvertibleSpv) {
      var investmentPercentOfOfferingPrecision = investmentPercentConvertibleSpv === 0 ? 0 : 3,
        $widget = this.$('.convertible-note-ownership-widget');
      $widget.html(
        JST['investor_investments/templates/convertible_note_ownership_widget_copy']({
          other_security_type_title_plural: this.model.get('other_security_type_title_plural'),
          ownership_percentage: this._formatForPage(investmentPercentConvertibleSpv, investmentPercentOfOfferingPrecision),
          investment_amount: this._formatForPage(investmentAmount, 2),
          is_spv: this._isSpv(investmentAmount)
        })
      );

      // Rebind popover
      this.$('[data-info-popover]').each(function () {
        new FF.Popover(this);
      });

      if (this._isSpvOffering()) {
        if (this._isUnderDirectThreshold(investmentAmount)) {
          // SPV Investment
          $('#share_count', $widget).addClass('hidden');
          $('#share_count_caption', $widget).addClass('hidden');
          $('#popover_direct', $widget).addClass('hidden');
          $('#popover_spv', $widget).removeClass('hidden');
        } else {
          // Direct Investment
          $('#share_count', $widget).removeClass('hidden');
          $('#share_count_caption', $widget).removeClass('hidden');
          $('#popover_direct', $widget).removeClass('hidden');
          $('#popover_spv', $widget).addClass('hidden');
        }
      }
    },

    _performCalcs: function (investmentAmount) {
      return new FF.ConvertibleCalculator(this._getCalcArgs(investmentAmount, this.model));
    }
  });

  exports.FF.InvestmentStockFormView = InvestmentStockFormView;
  exports.FF.InvestmentNotesFormView = InvestmentNotesFormView;
  FFCalculators(exports.FF);

})(window);

