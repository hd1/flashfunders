function FFCalculators(exports) {
  'use strict';

  function round(value, decimals) {
    return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);
  }

  function truncate(value, decimals) {
    return Number(Math.floor(value + 'e' + decimals) + 'e-' + decimals);
  }

  function extendPrototype(source, destination) {
    for (var property in source)
      if (source.hasOwnProperty(property)) {
        destination[property] = source[property];
      }
    return destination;
  }

  function parseNumber(val) {
    if (typeof val === 'string') {
      val = val.replace(/[^0-9\.]+/g, "");
    }
    return Number(val);
  }

  function InvestmentCalculator(context) {
    this.investmentAmount = parseNumber(context.amount);
    this.spvMinimumGoal = parseNumber(context.goal);
    this.maxTotalInvestment = parseNumber(context.total);
    this.spvTotalInvestment = parseNumber(context.spvTotal);
    this.offeringDirectInvestmentThreshold = parseNumber(context.threshold);
    this.offeringModelDirectSpv = context.modelDirectSpv;
    this.offeringSetupFee = parseNumber(context.setupFee);
    this.preMoneyValuation = parseNumber(context.valuation);
  }

  InvestmentCalculator.prototype = {
    spvDilution: function () {
      var denominator = Math.max(this.spvMinimumGoal, this.spvTotalInvestment + this.investmentAmount);
      return denominator == 0 ? 0 : 1 - this.offeringSetupFee / denominator;
    },

    isSpv: function () {
      return this.offeringModelDirectSpv && this.investmentAmount < this.offeringDirectInvestmentThreshold;
    },

    postMoneyValuation: function () {
      // How much is the company worth if they raise their full amount?
      return this.preMoneyValuation + this.maxTotalInvestment;
    },

    calcResults: function() {
      return {
        projectedCost: this.projectedCost(),
        projectedShares: this.projectedShares(),
        projectedOwnershipPct: this.projectedPercentOwnership()
      };
    }
  };

  function ConvertibleCalculator(context) {
    InvestmentCalculator.call(this, context);

    return this.calcResults();
  }

  ConvertibleCalculator.prototype = extendPrototype(InvestmentCalculator.prototype, {
    projectedShares: function () {
      return 0;
    },

    projectedCost: function () {
      return this.validValues() ? round(this.investmentAmount, 2) : 0;
    },

    projectedPercentOwnership: function () {
      var postMoneyVal = this.postMoneyValuation();

      if (postMoneyVal === 0.0) return 0;

      var percentOwnership = this.investmentAmount / this.postMoneyValuation() * 100.0;
      if (this.isSpv()) {
        percentOwnership *= this.spvDilution();
      }
      return round(percentOwnership, 3);
    },

    validValues: function () {
      return this.investmentAmount > 0;
    }

  });

  function UnitsBasedCalculator(context) {
    InvestmentCalculator.call(this, context);
    this.sharePrice = Number(context.sharePrice);

    return this.calcResults();
  }

  UnitsBasedCalculator.prototype = extendPrototype(InvestmentCalculator.prototype, {
    projectedShares: function () {
      return this.validValues() ? Math.ceil(this.investmentAmount / this.sharePrice) : 0;
    },

    projectedCost: function () {
      if (!this.validValues()) return 0;
      if (this.isSpv()) return truncate(this.investmentAmount, 2);
      return truncate(this.projectedShares() * this.sharePrice, 2);
    },

    projectedPercentOwnership: function () {
      var result = 0;
      var postMoneyVal = this.postMoneyValuation();

      if (postMoneyVal === 0.0) return 0;

      if (this.isSpv())
        result = this.spvDilution() * this.investmentAmount / postMoneyVal * 100.0;
      else
        result = (this.projectedShares() * this.sharePrice) / postMoneyVal * 100.0;

      return round(result, 3);
    },

    validValues: function () {
      return this.investmentAmount > 0 && this.sharePrice > 0;
    }

  });

  function InvestCalculator(context) {
    switch (context.calcType) {
      case 'units_based':
      case 'revenue_note':
        return new UnitsBasedCalculator(context);
      case 'convertible':
        return new ConvertibleCalculator(context);
      default: return {};
    }
  }

  function RegCCalculator(context) {
    this.incomeAmount = parseNumber(context.incomeAmount);
    this.netWorth = parseNumber(context.netWorth);
    this.prevRegCExtTotal = parseNumber(context.prevRegCExtTotal);
    this.prevRegCTotal = parseNumber(context.prevRegCTotal);
    this.regCCutoff = parseNumber(context.regCCutoff);
    this.sharePrice = parseNumber(context.sharePrice);

    if (isNaN(this.sharePrice) || this.sharePrice == 0) {
      this.sharePrice = 0.01; // Hack for convertible offerings
    }

    return this.doCalc();
  }

  RegCCalculator.prototype = {
    doCalc: function () {
      var legalMax = 100000.0;
      var basis = Math.min(this.incomeAmount, this.netWorth);
      var mult = basis < 100000.0 ? 0.05 : 0.10;
      var limit = Math.min(legalMax, Math.max(basis * mult, 2000.0));
      var uncapped = limit - this.prevRegCExtTotal - this.prevRegCTotal;
      var maxShares = Math.floor(Math.min(Math.max(uncapped, 0), this.regCCutoff) / this.sharePrice);
      return {
        limit: Math.floor(limit),
        available: Math.floor(maxShares * this.sharePrice),
        over_limit: Math.floor(Math.abs(Math.min(uncapped, 0)))
      };
    }
  };

  exports.ConvertibleCalculator = ConvertibleCalculator;
  exports.UnitsBasedCalculator = UnitsBasedCalculator;
  exports.InvestmentCalculator = InvestCalculator;
  exports.RegCCalculator = RegCCalculator;
}

if (typeof FF === 'undefined') {
  FF = {};
  FFCalculators(FF);
}
