function process_experience(element) {
  var $alert_confirm = $('#investor_profile_form_experience_confirm').closest('.alert_confirm');

  if (element.find("option:selected").val() === 'None') {
    $alert_confirm.removeClass('hidden');
  } else if (!$alert_confirm.hasClass('hidden')) {
    $alert_confirm.addClass('hidden');
    $alert_confirm.find($("input:checkbox")).attr('checked', false);
  }
}

function process_percentage(element) {
  var option = element.find("option:selected").val(),
    $alert_confirm = $('#investor_profile_form_percentage_confirm').closest('.alert_confirm');

  if (option === 'Between 0% and 10%') {
    $alert_confirm.removeClass('hidden');
    $('label[for="investor_profile_form_percentage_confirm"]').html('<b>Yes</b>, by selecting 0-10%, I am confirming that I am willing to accept the risks of making investments on FlashFunders.');
  } else if (option === 'Between 80% and 100%') {
    $alert_confirm.removeClass('hidden');
    $('label[for="investor_profile_form_percentage_confirm"]').html('<b>Yes</b>, by selecting 80-100%, I am confirming that I am willing to accept the risks of making investments on FlashFunders.');
  } else if (!$alert_confirm.hasClass('hidden')) {
    $alert_confirm.addClass('hidden');
    $alert_confirm.find($("input:checkbox")).attr('checked', false);
  }
}

function error_box(element) {
  var elem = $(element.target);
  elem.parents('.box-error').removeClass("box-error");
}

$(document).ready(function () {
  var $experience = $('#investor_profile_form_experience_select'),
    $percentage = $('#investor_profile_form_percentage_select');

  if ($experience.find("option:selected").val() === 'None') {
    $('#investor_profile_form_experience_confirm').closest('.alert_confirm').removeClass('hidden');
    process_experience($experience);
  }
  if ($percentage.find("option:selected").val() !== '') {
    $('#investor_profile_form_percentage_confirm').closest('.alert_confirm').removeClass('hidden');
    process_percentage($percentage);
  }

  $('.box-error input').click(error_box);

  $('.profile-checkbox').click(function(e) {
    var obj = $(e.target);
    obj.find('input[type="checkbox"]').click();
  });


  $experience.change(function () { process_experience($(this)); });
  $percentage.change(function () { process_percentage($(this)); });
});
