$.scope('#investment-dashboard', function () {

  var toggle_fn = function () {
    var wire_container = $(this).closest('.wire-container');
    $(".full-wire-info", wire_container).toggle();
    $(".light-wire-info", wire_container).toggle();
  };

  $('.toggle-link').click(toggle_fn);

});