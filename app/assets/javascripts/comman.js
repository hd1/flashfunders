$(document).ready(function() {
  
	//Open links in new window
	$("a[rel=external]").each(function(){
		this.target = "_blank";
	});

	//default validate action
	$('form').attr('novalidate',true);

	$("a.print").click(function(){
		window.print();
		return false;
	});
});