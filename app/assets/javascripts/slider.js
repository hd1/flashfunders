$(function () {
  'use strict';
  var sliderActive = false;
  var slider;
  var slider2Active = false;
  var slider2;

  function createSlider() {
    slider = $('.slides').bxSlider({
      mode: 'fade',
      adaptiveHeight: false,
      swipeThreshold: 40,
      controls: false,
      auto: false,
      pause: 6000,
      autoHover: true,
      pagerCustom: '#bx-pager'
    });
    return true;
  }
  function create2Slider() {
    slider2 = $('.slides2').bxSlider({
      mode: 'fade',
      adaptiveHeight: false,
      swipeThreshold: 40,
      controls: false,
      auto: false,
      pause: 6000,
      autoHover: true,
      pagerCustom: '#bx-pager'
    });
    return true;
  }

  function isValid(slider) {
    return slider !== undefined && slider.length;
  }

  //create slider if page is wide
  $(document).ready(function(){
    if (im.greaterThan('tablet') || ($('#wif-slides').length != 0)) {
      sliderActive = createSlider();
      slider2Active = create2Slider();
    }
  });
  //create/destroy slider based on width
  $(window).resize(function () {
      //if wide and no slider, create slider
    if (im.greaterThan('tablet') && !sliderActive && !slider2Active) {
      sliderActive = isValid(slider) && slider.reloadSlider();
      slider2Active = isValid(slider2) && slider2.reloadSlider();
    } else if (im.lessThan('tablet') && sliderActive == true && slider2Active == true){
        if (isValid(slider)) slider.destroySlider();
        sliderActive = false;
        if (isValid(slider2)) slider2.destroySlider();
        slider2Active = false;
    }
    //else if wide and active slider, or narrow and no slider, do nothing
  });

  $(document).ready(function(){
    if (!$('.static-team').length) {
      $('.bxslider').bxSlider();
    } else {
      $('.bxslider').bxSlider({
        auto: true,
        autoControls: true
      });
    };
  });

  $('.next')
    .on('click', function () {
      if (slider !== undefined) slider.goToNextSlide();
      if (slider2 !== undefined) slider2.goToNextSlide();
      return false;
    });

  $('.prev')
    .on('click', function () {
      if (slider !== undefined) slider.goToPrevSlide();
      if (slider2 !== undefined) slider2.goToPrevSlide();
      return false;
    });
});
