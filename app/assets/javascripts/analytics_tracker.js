(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

(function (exports) {
  'use strict';

  function gaSendEvent(category, event, label) {
    ga('send', 'event', category, event, label);
  }

  exports.FF.AnalyticsTracker = {
    'modalShown': function (label) {
      gaSendEvent('modal', 'shown', label);
    },

    'subscriptionLandingPage': function () {
      gaSendEvent('Subscription', 'SubscriptionLandingPage');
    },

    'subscriptionWhatIsFFPage': function () {
      gaSendEvent('Subscription', 'SubscriptionWhatIsFFPage');
    },

    'subscriptionHomePage': function () {
      gaSendEvent('Subscription', 'SubscriptionHomePage');
    },

    'subscriptionBrowse': function () {
      gaSendEvent('Subscription', 'SubscriptionBrowse');
    },

    'subscriptionEmail': function () {
      gaSendEvent('Subscription', 'SubscriptionEmail');
    },

    'invest': function (offeringId) {
      gaSendEvent('OfferingDetail', 'Invest', offeringId);
    },

    'pitch': function (offeringId) {
      gaSendEvent('OfferingDetail', 'Pitch', offeringId);
    },

    'team': function (offeringId) {
      gaSendEvent('OfferingDetail', 'Team', offeringId);
    },

    'documents': function (offeringId) {
      gaSendEvent('OfferingDetail', 'Documents', offeringId);
    },

    'questions': function (offeringId) {
      gaSendEvent('OfferingDetail', 'Questions', offeringId);
    },

    'fundraising': function (offeringId) {
      gaSendEvent('OfferingDetail', 'Fundraising', offeringId);
    },

    'signedIn': function (email) {
      gaSendEvent('SignIn', 'SignIn', email);
    },

    'signInFailed': function (email) {
      gaSendEvent('SignIn', 'SignInFailed', email);
    },

    'registered': function (email) {
      gaSendEvent('CreateAccount', 'AccountSuccess', email);
    },

    'registerFailed': function (email) {
      gaSendEvent('CreateAccount', 'AccountFailed', email);
    },

    'videoStart': function (offeringId) {
      gaSendEvent('video', 'start', offeringId);
    },

    'videoFinish': function (offeringId) {
      gaSendEvent('video', 'finish', offeringId);
    },

    'outboundLink': function (link) {
      gaSendEvent('external', 'click', link, 0, true);
    },

    'followStart': function (offeringId) {
      gaSendEvent('OfferingDetail', 'FollowStart', offeringId);
    },

    'unfollow': function (offeringId) {
      gaSendEvent('OfferingDetail', 'Unfollow', offeringId);
    },

    'followCreate': function (offeringId) {
      gaSendEvent('OfferingDetail', 'FollowCreate', offeringId);
    },

    'askAQuestionStart': function (offeringId) {
      gaSendEvent('OfferingDetail', 'AskAQuestionStart', offeringId);
    },

    'askAQuestionCreate': function (offeringId) {
      gaSendEvent('OfferingDetail', 'AskAQuestionCreate', offeringId);
    },

    'investAmount': function (offeringId) {
      gaSendEvent('Investment', 'CalculatorInvest', offeringId);
    },

    'profileIndividual': function (offeringId) {
      gaSendEvent('Investment', 'ProfileInvestIndividual', offeringId);
    },

    'profileEntity': function (offeringId) {
      gaSendEvent('Investment', 'ProfileInvestEntity', offeringId);
    },

    'profileEntityType': function (offeringId, type) {
      if (type === 'llc') {
        type = 'LLC';
      } else {
        type = type && (type[0].toUpperCase() + type.slice(1));
      }
      gaSendEvent('Investment', 'Profile' + type, offeringId);
    },

    'profileLater': function (offeringId) {
      gaSendEvent('Investment', 'ProfileFinishLater', offeringId);
    },

    'profileSubmit': function (offeringId) {
      gaSendEvent('Investment', 'ProfileSubmit', offeringId);
    },

    'verifyIndividualIncome': function (offeringId) {
      gaSendEvent('Investment', 'AccreditationIndividualIncome', offeringId);
    },

    'verifyIndividualNetWorth': function (offeringId) {
      gaSendEvent('Investment', 'AccreditationIndividualNetWorth', offeringId);
    },

    'verifyIndividualThirdParty': function (offeringId) {
      gaSendEvent('Investment', 'AccreditationIndividualThirdParty', offeringId);
    },

    'verifyIndividualDocs': function (offeringId) {
      gaSendEvent('Investment', 'AccreditationIndividualDocs', offeringId);
    },

    'verifyIndividualDocsIndividual': function (offeringId) {
      gaSendEvent('Investment', 'AccreditationIndividualDocsIndividual', offeringId);
    },

    'verifyIndividualDocsJoint': function (offeringId) {
      gaSendEvent('Investment', 'AccreditationIndividualDocsJoint', offeringId);
    },

    'verifyEntityOffline': function (offeringId) {
      gaSendEvent('Investment', 'AccreditationEntityOffline', offeringId);
    },

    'verifyEntityThirdParty': function (offeringId) {
      gaSendEvent('Investment', 'AccreditationEntityThirdParty', offeringId);
    },

    'verifyLater': function (offeringId) {
      gaSendEvent('Investment', 'AccreditationFinishLater', offeringId);
    },

    'verifySubmit': function (offeringId) {
      gaSendEvent('Investment', 'AccreditationSubmit', offeringId);
    },

    'reviewLater': function (offeringId) {
      gaSendEvent('Investment', 'ReviewFinishLater', offeringId);
    },

    'reviewSubmit': function (offeringId) {
      gaSendEvent('Investment', 'ReviewSubmit', offeringId);
    },

    'signLater': function (offeringId) {
      gaSendEvent('Investment', 'SignCloseModal', offeringId);
    },

    'signSubmit': function (offeringId) {
      gaSendEvent('Investment', 'SignSuccess', offeringId);
    },

    'fundWire': function (offeringId) {
      gaSendEvent('Investment', 'FundWire', offeringId);
    },

    'fundCheck': function (offeringId) {
      gaSendEvent('Investment', 'FundCheck', offeringId);
    },

    'fundAch': function (offeringId) {
      gaSendEvent('Investment', 'FundAch', offeringId);
    },

    'fundLater': function (offeringId) {
      gaSendEvent('Investment', 'FundFinishLater', offeringId);
    },

    'fundSubmit': function (offeringId) {
      gaSendEvent('Investment', 'FundSubmit', offeringId);
    },

    'dashEditAccreditation': function (offeringId) {
      gaSendEvent('Dashboard', 'EditAccreditation', offeringId);
    },

    'dashResendAccreditation': function (offeringId) {
      gaSendEvent('Dashboard', 'ResendAccreditation', offeringId);
    },

    'dashDownloadDocs': function (offeringId) {
      gaSendEvent('Dashboard', 'DownloadDocs', offeringId);
    },

    'dashEditPayment': function (offeringId) {
      gaSendEvent('Dashboard', 'EditPayment', offeringId);
    },

    'dashFundsSent': function (offeringId) {
      gaSendEvent('Dashboard', 'FundsSent', offeringId);
    },

    'successEditAccreditation': function (offeringId) {
      gaSendEvent('SuccessPage', 'EditAccreditation', offeringId);
    },

    'successDownloadDocs': function (offeringId) {
      gaSendEvent('SuccessPage', 'DownloadDocs', offeringId);
    },

    'successEditPayment': function (offeringId) {
      gaSendEvent('SuccessPage', 'EditPayment', offeringId);
    },

    'successFundsSent': function (offeringId) {
      gaSendEvent('SuccessPage', 'FundsSent', offeringId);
    },

    'browseSort': function (criterion) {
      gaSendEvent('Browse', 'Sort', criterion);
    }

  };

}(window));
