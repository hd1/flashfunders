(function ($) {

  var CustomBinaryRadioButtons = function (name, radioButtonJqueryElements) {
    var $el = $('<div data-custom-radio-group></div>');

    var $lastRadioButtonParent = _.last(radioButtonJqueryElements).closest('.radio').children('label');

    $lastRadioButtonParent.after($el);

    _.each(radioButtonJqueryElements, function ($button) {
      var value = $button.val();
      var tabindex = $button.attr('tabindex');
      var $buttonParent = $button.closest('.radio').children('label');
      var labelText = $buttonParent.text();
      var changeFn = $button.data('changehandler');
      var changeFnData = $button.data('data');

      $button.removeAttr('tabindex');

      var $customRadioButton = $('<span class="custom-radio-input" data-custom-radio-input-name="'
        + name + '" data-custom-radio-input-value="' + value + '" tabindex="' + tabindex + '">' + labelText + '</span>');

      if($button.prop('checked')) {
        $customRadioButton.addClass('active');
      }

      $el.append($customRadioButton);
      $buttonParent.addClass('replaced-by-custom-radio-button');

      $customRadioButton.on('click', function (e) {
        $($el.find('.active')).removeClass('active');
        var $target = $(e.target);
        $target.addClass('active');

        _.each(radioButtonJqueryElements, function ($radioButton) {
          $radioButton.removeAttr('checked');
        });

        $button.click();
        executeFnByName(changeFn, window, changeFnData);
      });

      $customRadioButton.on('keydown', function (e) {
         if (parseInt(e.which) == '32') { // spacebar
             $(this).click();
             return false;
         }
      });
    });
  };

  function verifyPresenceOfTwoRadioButtonsPerName($radioButtons) {
    var argumentError = new Error('Must have exactly 2 radio buttons with the same name to create customBinaryRadioButtons');

    if ($radioButtons.size() % 2 !== 0) {
      throw  argumentError;
    }

    var nameCounts = {};
    _.each($radioButtons, function (radioButton) {
      if (nameCounts[$(radioButton).attr('name')]) {
        nameCounts[$(radioButton).attr('name')]++;
      } else {
        nameCounts[$(radioButton).attr('name')] = 1;
      }
    });


    var uniqueCountsOfNamePairs = _.uniq(_.values(nameCounts));
    var hasEvenNumberOfNamePairs = (uniqueCountsOfNamePairs[0] === 2 && uniqueCountsOfNamePairs.length === 1);
    var hasNoNamePairs = uniqueCountsOfNamePairs.length === 0;
    if (!(hasEvenNumberOfNamePairs || hasNoNamePairs)) {
      throw argumentError;
    }
  }

  $.fn.customBinaryRadioButtons = function () {
    var $radioButtons;

    if (this.is('input')) {
      $radioButtons = this;
    } else {
      $radioButtons = this.find('input[type="radio"]');
    }



    var radioButtonPairs = {};

    _.each($radioButtons, function (radioButton) {
      var $radioButton = $(radioButton);

      if (radioButtonPairs[$radioButton.attr('name')]) {
        radioButtonPairs[$radioButton.attr('name')].push($radioButton);
      } else {
        radioButtonPairs[$radioButton.attr('name')] = [$radioButton];
      }
    });

    _.each(radioButtonPairs, function (radioButtons, name) {
      new CustomBinaryRadioButtons(name, radioButtons);
    });

    return this;
  };
})(jQuery);
