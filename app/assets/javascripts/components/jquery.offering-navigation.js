(function ($, window) {
  'use strict';
  var pluginName = "offeringNavigation";

  function Plugin(page, options) {
    this.page = page;
    this.options = $.extend({}, $.fn[pluginName].defaults, options);

    this.init();
  }

  Plugin.prototype = {
    init: function () {
      this.FOOTER_TOP_BUFFER = 45;
      this.$window = $(window);
      this.$secNav = $('.pitch-nav', this.page);
      this.pinnedTop = this.$secNav.outerHeight();
      this.pinStartingHere = 0; //this._wholeRightPanel().offset().top;
      this.$offeringHeadOriginalSize = $('.offering-head', this.page).outerHeight();

      this._generateTertiaryNav(this);

      // honor location in page load hash
      this._showTab();

      this.$window
        .on('scroll', $.proxy(this._positionScrollToTop, this))
        .on('scroll', $.proxy(this._positionNav, this))
        .on('scroll', $.proxy(this._pinSecondaryNav, this))
        .on('resize', $.proxy(this._positionScrollToTop, this))
        .on('resize', $.proxy(this._positionNav, this))
        .on('hashchange', $.proxy(this._hashChange, this));
      $('a[data-toggle="tab"]')
        .on('show.bs.tab', $.proxy(this._togglePin, this))
        .on('click', $.proxy(this._toggleClick, this));

      $('.scroll-to-top').add('.logo').on('click', $.proxy(this._scrollToTop, this));

      $('.nav-item a', '#tertiary-nav-container').on('click', $.proxy(this._tertiaryNavClick, this));

      $('.pitch-nav .container .left-arrow').on( 'click', $.proxy(this._scrollNav, this, "-=50"));
      $('.pitch-nav .container .right-arrow').on('click', $.proxy(this._scrollNav, this, "+=50"));
      $('.pitch-nav .container ul').on('scroll', $.proxy(this._toggleArrows, this));
    },

    _toggleArrows: function() {
      var pitchNav = $('.pitch-nav .container'),
        list = $('ul', pitchNav),
        left = $('.left-arrow', pitchNav),
        right = $('.right-arrow', pitchNav),
        scrollLeft = list.scrollLeft(),
        listScrollWidth = list[0].scrollWidth;

      if(scrollLeft > 0) {
        left.show();

        if(scrollLeft >= (listScrollWidth - list.outerWidth())) {
          right.hide();
        } else {
          right.show();
        }
      } else {
        left.hide();
      }

    },

    _scrollNav: function(val) {
      $('.pitch-nav .container ul').animate({scrollLeft: val}, 100);
    },

    _firstTabHref: function () {
      return $('li a', this.$secNav).first().attr('href');
    },

    _generateTertiaryNav: function (plugin) {
      var container = '#tertiary-nav-container',
        $list = $('ul.nav', container);
      $('.element-title', '#navigable-pitch').each(function (unused, hdr) {
        var $hdr = $(hdr),
          txt = $hdr.text(),
          id = txt.replace(/[^\w]/g, '-');
        $list.append(plugin._navItem(id, txt));
        $hdr.attr('id', id);
      });

      $('html,body').scrollspy({ target: container, offset: 100 });

      $('ul.nav li', container)
        .removeClass('active')
        .first()
        .addClass('active');
    },

    _hashChange: function () {
      this._showTab();              // respond to location change events
    },

    _headSizeDifference: function () {
      return $('.offering-head').outerHeight() - this.$offeringHeadOriginalSize;
    },

    _navItem: function (id, txt) {
      return '<li class="nav-item"><a target="_self" href="#' + id + '">' + txt + '</a></li>';
    },

    _pinNav: function ($pinnedNav, offsetTop, offsetBottom) {
      if($(window).width() < 1000) {
        // We're on tablet or mobile.
        return;
      }
      var actualScrollTop = this.$window.scrollTop() + this.pinnedTop + 25 - offsetTop,
        bottomOfRightPanel,
        bottomPoint,
        parentWidth = $pinnedNav.parent().outerWidth(),
        scrolledPastDocumentHeight;

      if (actualScrollTop > this.pinStartingHere + this._headSizeDifference()) {
        $pinnedNav.addClass('pinned');

        bottomOfRightPanel = actualScrollTop + $pinnedNav.outerHeight() + offsetTop + offsetBottom;
        bottomPoint = $('footer').position().top - this.FOOTER_TOP_BUFFER;
        scrolledPastDocumentHeight = (this.$window.height() + this.$window.scrollTop()) > $(document).height();

        if (!scrolledPastDocumentHeight) {
          if (bottomPoint <= bottomOfRightPanel) {
            $pinnedNav.css('top', (bottomPoint - bottomOfRightPanel + this.pinnedTop));
          } else {
            $pinnedNav.css('top', this.pinnedTop + 25);
          }
          $pinnedNav.css('width', parentWidth);
        }
      } else {
        $pinnedNav.removeClass('pinned');
      }
    },

    _pinSecondaryNav: function () {
      if($(window).width() < 1000) {
        // We're on tablet or mobile.
        return;
      }
      this.navHeight = $('.offering-head').offset().top + $('.offering-head').outerHeight();
      var topScrollHeight = $(document).scrollTop();

      if (topScrollHeight >= this.navHeight) {
        this.$secNav.addClass('pinned');
        this.$secNav.next().css('margin-top', this.$secNav.outerHeight());
      } else {
        this.$secNav.removeClass('pinned');
        this.$secNav.next().css('margin-top', '0');
      }
    },

    _positionNav: function () {
      if($(window).width() < 1000) {
        // We're on tablet or mobile.
        this.$secNav.removeClass('pinned');
        return;
      }
      var rightPanel = this._wholeRightPanel(),
        elementToPin;
      if (rightPanel.length) {
        elementToPin = this._selectNavElement();
        if (elementToPin !== null) {
          this._pinNav(elementToPin.elem, elementToPin.offsetTop, elementToPin.offsetBottom);
          $('.offering-body').css('min-height', rightPanel.outerHeight() + 30);
        }
      }
    },

    _positionScrollToTop: function () {
      if($(window).width() > 1000) {
        var currentScrollPosition = $(document).scrollTop(),
          $button = $('.scroll-to-top', this.page),
          footerTop = $('footer').offset().top - $(document).scrollTop(),
          newBottom = $(window).height() - footerTop + ($button.height());

        if (currentScrollPosition === 0) {
          $('.scroll-to-top').hide();
        } else {
          $('.scroll-to-top').show();
          if (newBottom < 30) {newBottom = 30;}
          if (newBottom > $(window).height() - ($button.height() * 1.33)) {newBottom = $(window).height() - ($button.height() * 1.33);}
          $button.css('bottom', newBottom);
        }
      }
    },

    _scrollToTop: function () {
      $('html,body').animate({scrollTop: $('body').offset().top}, 300);
    },

    _selectNavElement: function () {
      var rightPanel = this._wholeRightPanel(),
        $tertiaryNav = $('.tertiary-navigation');

      if($tertiaryNav.is(':visible')) {
        // Room for tertiary nav only
        if ((this.$window.height() - this.pinnedTop) > $tertiaryNav.outerHeight()) {
          this._unpin(rightPanel);
          return {'elem': $tertiaryNav, 'offsetTop': 70, 'offsetBottom': 15, 'offsetLeft': 25};
        }
      }      // Room for nothing, just let the page scroll as God intended
      this._unpin($tertiaryNav);
      this._unpin(rightPanel);
      return null;
    },

    _showTab: function () {
      var secNavLink = window.location.hash.replace('#', '');
      if (secNavLink.length === 0) {
        secNavLink = this._firstTabHref();
      } else {
        secNavLink = '#nav-' + secNavLink;
      }
      $('a[href="' + secNavLink + '"]').tab('show');
    },

    _tertiaryNavClick: function (e) {
      var navItemId = e.target.attributes.getNamedItem('href').value;

      $('html,body').animate({
        scrollTop: $(navItemId).offset().top - 85
      }, 210);
      return false;
    },

    _toggleClick: function (e) {
      var elemHref = e.target.attributes.getNamedItem('href').value;
      if (elemHref === this._firstTabHref()) {
        history.pushState("", document.title, window.location.pathname + window.location.search);
        this._showTab();
      } else {
        window.location.hash = elemHref.replace('#nav-', '');
      }
    },

    _togglePin: function () {
      if ((this.$secNav.hasClass('pinned'))) {
        $('html body').scrollTop($('.tab-box').offset().top - 70);
      }
      this._positionNav();
    },

    _unpin: function (elem) {
      elem
        .css('top', '')
        .css('left', '')
        .css('position', '');
    },

    _wholeRightPanel: function () {
      return $('.right-panel');
    }

  };

  $.fn[pluginName] = function (options) {
    return this.each(function () {
      if (!$.data(this, pluginName)) {
        $.data(this, pluginName, new Plugin(this, options));
      }
    });
  };

}(jQuery, window));
