// This plugin is designed for the situation where a user is doing an ajax form submit via Rails
// UJS, but he does not have a current Devise session. When he submits the form, and gets back a
// 401 / Unauthorized response, this plugin will redirect him to whichever path he passes into this
// plugin as a string, or to the /users/sign_in page, if he passes no arguments. We handle this
// with JS, and not on the Rails side, because Warden, by default, intercepts xhr requests, and
// hands back a 401 response, so that these requests never get to our app. 
(function ($, window) {

  var pluginName = "redirectUnauthorizedAjax";

  function Plugin(element, options) {
    this.$form = $(element);
    this.options = $.extend({}, $.fn[pluginName].defaults, options);
    this.init();
  }

  Plugin.prototype = {
    init: function() {
      this.$form
      .on('ajax:error', $.proxy(this, '_redirectUnauthorized'));
    },

    // ----------------------------------------
    // Event Callbacks
    // ----------------------------------------

    _redirectUnauthorized: function(event, xhr, status, error) {
      if (error == 'Unauthorized' || error == 'Unprocessable Entity') {
        window.location = this.options.redirectPath;
      }
    }
  };

  $.fn[pluginName] = function(options) {
    return this.each(function() {
      if (!$.data(this, pluginName)) {
        $.data(this, pluginName, new Plugin(this, options));
      }
    });
  };

  $.fn[pluginName].defaults = {
    redirectPath: '/users/sign_in'
  };
})(jQuery, window);