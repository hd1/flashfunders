(function ($, window) {
  'use strict';
  var pluginName = "offeringCarouselSize";

  function Plugin(page, options) {
    this.page = page;
    this.options = $.extend({}, $.fn[pluginName].defaults, options);
    this.init();
  }

  Plugin.prototype = {
    init: function () {
      // Large pictures may not have been loaded yet, so naturalHeight and naturalWidth are not available.
      // The imagesLoaded library will set up events so our function won't get called until all are ready.
      $('#navigable-pitch').imagesLoaded(this._sizeCarouselContainers);
    },

    _sizeCarouselContainers: function () {
      // Scale all the carousel containers on the page after all the images have been loaded
      var MAX_HEIGHT = 730;

      $('.carousel').each(function () {
        var $carouselInner = $('.carousel-inner', this)
          , containerHeight = 0, containerWidth = $carouselInner.width()
          , tallestImage = 0, img_properties = [];

        $('img', $carouselInner).each(function () {
          var imageHeight = this.naturalHeight
            , imageWidth = this.naturalWidth
            , imageRatio = parseFloat(imageHeight / imageWidth);

          tallestImage = Math.max(tallestImage, containerWidth * imageRatio);
        });
        containerHeight = Math.min(MAX_HEIGHT, tallestImage);
        $carouselInner.css({
          'height': containerHeight
        });

        $('img', $carouselInner).each(function () {
          var imageHeight = this.naturalHeight
            , imageWidth = this.naturalWidth
            , marginVertical = '0px'
            , imageRatio = parseFloat(imageHeight / imageWidth);

          if (imageHeight > imageWidth) { // portrait
            imageHeight = containerHeight;
            imageWidth = imageHeight / imageRatio;
          } else {  // landscape
            imageWidth = containerWidth;
            imageHeight = imageWidth * imageRatio;
            marginVertical = (containerHeight / 2) - (imageHeight / 2) + 'px';
          }

          img_properties.push({
            img: this, height: imageHeight, width: imageWidth, vertical: marginVertical
          });
        });

        $.each(img_properties, function (index, obj) {
          var $carouselImage = $('img[src="' + $(obj.img).attr('src') + '"]', $carouselInner);
          $carouselImage.css({
            'max-height': obj.height, 'width': obj.width
          });
          $carouselImage.parent().css({
            'top': obj.vertical
          });
        });
      });
    }
  };

  $.fn[pluginName] = function (options) {
    return this.each(function () {
      if (!$.data(this, pluginName)) {
        $.data(this, pluginName, new Plugin(this, options));
      }
    });
  };

}(jQuery, window));
