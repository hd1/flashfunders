;(function ($, window, document) {
  "use strict";

  var pluginName = "userSession";

  function Plugin(element, options) {
    this.element = $(element);
    this.options = options;
    this.url = this.options.url;
    this.timers = [];

    this.warningInterval = 300;
    this.countdownTimer = -1;

    this._name = pluginName;
    this.init();
  }

  Plugin.prototype = {
    init: function() {
      this.setLastEvent();
      this.getSessionInfo();
      this.element.on('hide.bs.modal', $.proxy(this.modalClicked, this));
    },

    getSessionInfo: function() {
      $.each(this.timers, function(i, timerId) { clearTimeout(timerId); });
      this.timers = [];
      this.sentLastEvent = true;
      var secondsSinceLastEvent = parseInt((this.getTime() - this.lastEvent) / 1000);

      $.ajax({
        type: "get",
        url: this.url,
        data: { return_to: window.location.href, last_event: secondsSinceLastEvent },
        success: $.proxy(this.processSessionInfo, this),
        error: function () {
          document.location.replace('/users/log_me_out');
        }
      });
    },

    processSessionInfo: function(data) {
      this.warningInterval = data.warning_time;
      this.endTime = this.getTime() + (data.time_left * 1000);

      if (this.timeLeft() <= this.warningInterval) {
        if (this.sentLastEvent) {
          this.showModal();
        }
      } else {
        this.hideModal();
      }

      this.timers.push(setTimeout($.proxy(this.getSessionInfo, this), this.nextCheck() * 1000));
    },

    timeLeft: function() {
      return (this.endTime - this.getTime()) / 1000;
    },

    nextCheck: function() {
      if (this.timeLeft() < this.warningInterval) {
        if (this.sentLastEvent) {
          return (this.timeLeft() % 60);
        } else {
          // Check immediately if we are in modal mode but an event was detected before we showed the modal
          return 0;
        }
      } else {
        return this.timeLeft() - this.warningInterval + 1;
      }
    },

    activateEvents: function() {
      $(window).on('mousedown click scroll keypress', $.proxy(this.setLastEvent, this));
    },

    deactivateEvents: function() {
      $(window).off('mousedown click scroll keypress', this.setLastEvent);
    },

    setLastEvent: function() {
      this.sentLastEvent = false;
      this.lastEvent = this.getTime();
    },

    getTime: function() {
      return (new Date()).getTime();
    },

    // Modal functions
    showModal: function() {
      if (! this.element.hasClass('in')) {
        this.deactivateEvents();
        this.startTimer(this.element.find('[data-time-string]')[0]);
        this.element.modal('show');
      }
    },

    hideModal: function() {
      this.activateEvents();
      if (this.element.hasClass('in')) {
        this.element.modal('hide');
      }
    },

    modalClicked: function() {
      this.setLastEvent();
      this.getSessionInfo();
    },

    startTimer: function(display) {
      var self = this
        , minutes
        , seconds;

      clearInterval(self.countdownTimer);
      var displayTime = function () {
        var timer = self.timeLeft();
        timer = timer < 0 ? 0 : timer;
        minutes = self._addLeadingZero(parseInt(timer / 60, 10));
        seconds = self._addLeadingZero(parseInt(timer % 60, 10));

        display.textContent = minutes + " min " + seconds + " sec";
      };
      displayTime();
      self.countdownTimer = setInterval(displayTime, 1000);
    },

    _addLeadingZero: function(n) { return (n < 10) ? ("0" + n) : n; }
  };

  $.fn[pluginName] = function (options) {
    return this.each(function () {
      if (!$.data(this, "plugin_" + pluginName)) {
        $.data(this, "plugin_" + pluginName, new Plugin(this, options));
      }
    });
  };

})(jQuery, window, document);
