// Batch File Uploader
//
// A wrapper around the FileUpload plugin, providing additional callbacks
// for batch-level events.
//
// Options:
//   onProgressCheck - Called during file uploads returning a completion percentage.
//   onSuccess       - Called when a file is successfully uploaded.
//   onBatchSuccess  - Called when all files in the fileList have been uploaded successfully.
//   onFailure       - Called when a file has failed to be successfully uploaded.
//   onBatchFailure  - Called if any file in the file list has failed to upload successfully.
//
(function ($) {

  var pluginName = "batchUploader";

  function Plugin(element, options) {
    this.$form = $(element);
    this.options = $.extend({}, $.fn[pluginName].defaults, options);

    this.init();
  }

  Plugin.prototype = {
    init: function() {
      this._setCounter(0);

      this.$form
        .on('fileuploadadd', $.proxy(this, '_callbackAddFile'))
        .on('fileuploadfail', $.proxy(this, '_callbackUploadFailed'))
        .on('fileuploaddone', $.proxy(this, '_callbackUploadDone'))
        .on('fileuploadprogressall', $.proxy(this, '_callbackUploadProcessAll'))
        .on('fileuploadalways', $.proxy(this, '_callbackUploadAlways'));
    },

    _callbackAddFile: function(e, data) {
      // only set the count on the initial file upload
      if (this.batchCount == 0) {
        this.batchCount = data.originalFiles.length;
      }
      this._markStarted();
    },

    _callbackUploadFailed: function(e, data) {
      this._markFailure();
      this._triggerFailure(e, data);
    },

    _callbackUploadDone: function(e, data) {
      this._markSuccess();
      this._triggerSuccess(e, data);
    },

    _callbackUploadProcessAll: function(e, data) {
      this._triggerProgressCheck(e, data);
    },

    _callbackUploadAlways: function(e, data) {
      if (this.processedCount == this.batchCount) {
        if (this.failureCount > 0) {
          this._triggerBatchFailure();
        } else {
          this._triggerBatchSuccess();
        }
        // reset counters
        this._setCounter(0);
      }
    },

    _markStarted: function() {
      this.startedCount++;
    },

    _setCounter: function(count) {
      this.batchCount = count;
      this.successCount = 0;
      this.failureCount = 0;
      this.processedCount = 0;
      this.startedCount = 0;
    },

    _markSuccess: function() {
      this.successCount++;
      this.processedCount++;
    },

    _markFailure: function() {
      this.failureCount++;
      this.processedCount++;
    },

    _triggerProgressCheck: function(event, data) {
      var percentDone = parseInt(data.loaded / data.total * 100, 10);
      this.options.onProgressCheck(percentDone);
    },

    _triggerSuccess: function(event, data) {
      var key = $('Key', data.result).text();
      console.log("Successful Upload: " + key);
      this.options.onSuccess(data.files[0], key);
    },

    _triggerFailure: function(event, data) {
      console.log("Failed Upload: " + data.errorThrown);
      this.options.onFailure(data.files[0]);
    },

    _triggerBatchSuccess: function() {
      console.log("Batch Succeeded: " + this.batchCount);
      this.options.onBatchSuccess();
    },

    _triggerBatchFailure: function() {
      console.log("Batch Failed: " + this.failureCount + " failed uploads.");
      this.options.onBatchFailure();
    }
  };

  $.fn[pluginName] = function(options) {
    return this.each(function() {
      $.data(this, pluginName, new Plugin(this, options));
    });
  };

  $.fn[pluginName].defaults = {
    onProgressCheck: function(percentDone) {},
    onSuccess: function(file, key) {},
    onFailure: function(file) {},
    onBatchSuccess: function() {},
    onBatchFailure: function() {}
  };

})(jQuery);

