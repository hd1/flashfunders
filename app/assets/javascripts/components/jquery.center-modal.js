(function ($, window) {

  $.fn.centerModal = function() {

    function setToCenter(modal) {
      var dialog = $('.modal-dialog', modal),
          initModalHeight = dialog.outerHeight(),
          userScreenHeight = $(window).outerHeight();

      if (initModalHeight > userScreenHeight) {
        dialog.css('overflow', 'auto');
      } else {
        dialog.css('margin-top', (userScreenHeight / 2) - (initModalHeight / 2));
      }
    }

    this.each( function() {
      $(this).on('shown.bs.modal', function(e) {
        setToCenter(this);
      });
    });

    return this;
  };

})(jQuery, window);

