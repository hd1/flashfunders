(function ($) {

  $.fn.soloModal = function() {

    function hideAllModals() {
      $('.modal.in').each(function(){
        $(this).modal('hide');
      });
    }

    this.each( function() {
      $(this).on('show.bs.modal', function(e) {
        hideAllModals();
      });
    });

    return this;
  };

})(jQuery);

