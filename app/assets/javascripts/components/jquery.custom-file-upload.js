// Custom File Uploader
//
// A wrapper around the FileUpload plugin to provide global
// application-level features to the file upload process. Currently
// this includes:
//
//   1. Safe file names devoid of special characters
//   2. Option to enumerate file names
//
// Options:
//   enumerate:  True/False depending on whether or not to prefix the
//               filename with a sequence number. Defaulted to false.
//
(function ($) {
  var pluginName = "customFileUpload";

  function Plugin(form, options) {
    this.$form = $(form);
    this.options = $.extend({}, $.fn[pluginName].defaults, options);

    this.init();
  }

  Plugin.prototype = {
    init: function () {
      this.fileCount = 0;

      this.$form.fileupload({
        dataType: 'xml',
        add: $.proxy(this, '_callbackAddFile'),
        fail: $.proxy(this, '_callbackUploadFailed')
      });
    },

    add: function(options) {
      this.$form.fileupload('add', options);
    },

    _callbackAddFile: function(e, data) {
      this.fileCount++;
      this._setFileKey(data.files[0].name);

      data.submit();
    },

    _callbackUploadFailed: function(e, data) {
      Honeybadger.notify("File Upload Error", {
        context: {
          page: document.URL,
          type: e.type,
          files: data.files,
          xhr: data.jqXHR
        }
      });
    },

    _setFileKey: function(fileName) {
      var name = this._sanitizeFileName(fileName)
        , keyInput = $('input[name=key]', this.$form);

      // update the filename within the path
      var newKey = keyInput.val().replace(/(.*\/)(.*)/, "$1" + name);
      keyInput.val(newKey);
    },

    _sanitizeFileName: function(name) {
      // replace spaces with dashes, and only allow alpha-numerics
      var validName = name.replace(/[\s]/g, '-').replace(/[^a-z0-9_\-\.]/gi, '');

      if (this.options.enumerate === true) {
        validName  = this.fileCount + "_" + validName;
      }
      return validName;
    }
  };

  $.fn[pluginName] = function (methodOrOptions) {
    var instance = $.data(this, pluginName);

    // call a public method on an instance
    if ( instance && $.isFunction(Plugin.prototype[methodOrOptions]) ) {
      return instance[methodOrOptions].apply(instance, Array.prototype.slice.call( arguments, 1 ));
    }

    // initialize the plugin
    else if ( typeof methodOrOptions === 'object' || !methodOrOptions ) {
      $.data(this, pluginName, new Plugin(this, methodOrOptions));
      return $(this);
    }

    // attempting to call a method prior to initializing
    else if ( !instance ) {
      $.error( 'Plugin must be initialised before using method: ' + methodOrOptions );
    }

    // invalid method called
    else {
      $.error( 'Method ' + methodOrOptions + 'does not exist.');
    }
  };

  $.fn[pluginName].defaults = {
    enumerate: false
  };

}(jQuery));
