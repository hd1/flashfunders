(function($) {

  var splice = Array.prototype.splice;

  /**
   * @method scope
   * @param {String} selector CSS selector. Only ID selectors can be used here.
   * @param {Function} callback Callback function. Will be executed on DOM ready event.
   */
  $.extend({
    scope: function(selector, callback) {
      var args = arguments;

      $(function($) {
        var id = selector.split("#")[1]
          , root
          , $root;

        // warn to use only IDs as root selectors for scope
        if (typeof id === "undefined") {
          throw "Use ID selector. Unsupported selector: " + selector;
        }

        // fast check
        if (root = document.getElementById(id)) {
          $root = $(root);
          callback.apply($root, [$root].concat(splice.call(args, 2)));
        }
      });
    }
  });

})(jQuery);

