(function ($) {
  $.fn.paymentButtons = function () {
    'use strict';

    function turn_button_on(btn) {
      $(btn).text($(btn).data('msg-on'));
      $(btn).addClass('active');
    }

    function turn_button_off(btn) {
      $(btn).text($(btn).data('msg-off'));
      $(btn).removeClass('active');
    }

    function toggle_button(btn) {
      if ($(btn).hasClass('active')) {
        turn_button_off(btn);
      } else {
        turn_button_on(btn);
      }
    }

    function confirm_payment(e) {
      var btn = $(e.target);
      var confirming_payment = !btn.hasClass('active');
      var kind = btn.data('kind');
      var data = {kind: kind, yes_or_no: confirming_payment};
      $.ajax({
        type: 'POST',
        url: btn.data('url'),
        data: data,
        dataType: 'json',
        success: function (data) {
          console.log(data);
          toggle_button(e.target);
        },
        failure: function () {
          console.log('ajax call failed');
        }
      });
    }

    $('.btn-payment-ajax[data-sent="true"]').each(function () {
      turn_button_on($(this));
    });

    $('.btn-payment-ajax').on('click', confirm_payment);
  };
})(jQuery);
