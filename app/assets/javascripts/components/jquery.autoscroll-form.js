(function ($) {
  $.fn.autoScroll = function () {
    'use strict';

    // Animated scrolling closest preceding 'anchor' for fields class 'autoscroll'
    function scroll_to(target) {
      if (target !== undefined) {
        $('html,body').animate({scrollTop: $(target).offset().top}, 250);
      }
    }

    function find_anchor(field, searchUp) {
      var anchor;
      for (var parent = $(field); parent.length > 0; parent = $(parent).parent()) {
        // Is the container our anchor?
        if ($(parent).hasClass('autoscroll-anchor')) {
          return parent;
        }
        // Do we have a preceding sibling to scroll to?
        anchor = searchUp ? $(parent).prev('.autoscroll-anchor') : $(parent).next('.autoscroll-anchor');
        if (anchor.length) {
          return anchor;
        }
      }
      return undefined;
    }

    function _scroll_up(field) {
      scroll_to(find_anchor(field, true));
    }

    function _scroll_down(field) {
      scroll_to(find_anchor(field, false));
    }

    function handle_focus(field) {
      if ($(field).attr('click') != 'true') {
        _scroll_up(field);
      }
    }

    // Focus if there's no error on the field because focusing
    // would clear the error.
    $.fn.autoScroll.focus_unless_error = function(field) {
      var error = '.' + field.substring(1) + ' .error';
      if ($(error).length) { return; }
      $(field).focus();
    };

    $.fn.autoScroll.scrollUp = function(field) {
      _scroll_up(field);
    };

    $.fn.autoScroll.scrollDown = function(field) {
      _scroll_down(field);
    };

    // The mouseover methods are needed so that we don't autoscroll
    // out from under a user click. We only autoscroll when a user
    // tabs into a field.
    $('.autoscroll').on('focus', function (e) {
      handle_focus(e.target);
    }).mouseover(function () {
      $(this).attr('click', 'true');
    }).mouseout(function () {
      $(this).removeAttr('click');
    });

  };
})(jQuery);
