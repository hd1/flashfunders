(function ($) {
  $.fn.expandElements = function () {
    'use strict';
    var $expandableScrim = $('<div class="expandable-scrim" style="position: absolute; top: 0; right: 0; bottom: 0; left: 0; z-index: 1;"></div>'),
      $modal, $originalElement, $expandedElement, originalTop;

    $expandableScrim.prependTo('.expandable');

    function handleExpandoKeystrokes(e) {
      // Left arrow
      if (e.which === 37) {
        e.preventDefault();
        $('.carousel', $modal).carousel('prev');
        // Right arrow
      } else if (e.which === 39) {
        e.preventDefault();
        $('.carousel', $modal).carousel('next');
        // Escape
      } else if (e.which === 27) {
        closeExpando();
      }
    }

    function closeExpando() {
      $(document)
        .trigger('expando.hide')
        .unbind('keydown', handleExpandoKeystrokes);
      $modal.remove();
      $expandedElement.remove();
      $('.modal-close-icon').remove();
      $('.scrim').removeClass('show').css('background-color', 'transparent');
      $('html, body').css({
        'overflow': 'auto',
        'height': 'auto'
      });
      $('html body').scrollTop(originalTop);
    }

    function sizeModal() {
      var winHeight    = $(window).height(),
          winWidth     = $(window).width(),
          usableHeight = winHeight > 120 ? winHeight - 120 : winHeight,
          usableWidth  = winWidth > 120 ? winWidth - 120 : winWidth,
          windowRatio  = usableHeight / usableWidth,
          modalWidth   = 0, modalHeight = 0,
          greatestWidth = 0, greatestHeight = 0,
          img_properties = []
        ;

      // Iterate over the original images because the size of the images that were copied
      // into the modal may not be available in the browser before they've rendered.
      $('img', $originalElement).each(function () {
        var imageHeight, imageWidth,
          imageRatio = this.naturalHeight / this.naturalWidth;

        if (windowRatio < imageRatio) {
          imageHeight = usableHeight;
          imageWidth  = usableHeight / imageRatio;
          modalHeight = Math.max(usableHeight, modalHeight);
          modalWidth  = Math.max(imageWidth, modalWidth);
        } else {
          imageHeight = usableWidth * imageRatio;
          imageWidth  = usableWidth;
          modalHeight = Math.max(imageHeight, modalHeight);
          modalWidth  = Math.max(usableWidth, modalWidth);
        }
        greatestWidth = Math.max(greatestWidth, imageWidth);
        greatestHeight = Math.max(greatestHeight, imageHeight);

        img_properties.push({img: this, height: imageHeight, width: imageWidth });
      });
      $.each(img_properties, function (index, obj) {
        var $modalImage = $('img[src="' + $(obj.img).attr('src') + '"]', $modal);

        $modalImage.css({
          'max-height': obj.height,
          'height':     obj.height,
          'max-width':  obj.width,
          'width':      obj.width,
          'margin':     ((greatestHeight - obj.height) / 2).toString() + 'px ' + ((greatestWidth - obj.width) / 2) + 'px'
        });
      });

      $modal.css({
        'margin-left': -(modalWidth - 2) / 2,
        'margin-top':  -(modalHeight - 2) / 2,
        'left':        '50%',
        'top':         '50%'
      });
    }

    function resetModal() {
      $modal = $('<div class="modal-container" style="position: fixed; z-index: 999;"></div>');
    }

    function resetClone(element) {
      $(element).find('.carousel-inner').removeAttr('style');
      $('img', element).each(function () {
        $(this).removeAttr('style');
        $(this).parent().removeAttr('style');
      });
    }

    function openExpando() {
      var originalElementId, newId;

      resetModal();
      originalTop = $(window).scrollTop();
      if ($(this).hasClass('expandable')) {
        $originalElement = $(this);
      } else {
        $originalElement = $(this).parents('.expandable');
      }

      $(document).bind('keydown', handleExpandoKeystrokes);

      $('.scrim').addClass('show').css('background-color', 'rgba(255, 255, 255, 0.97)').css('z-index', '100');
      $('body').append($modal);
      $('html, body').css({
        'overflow': 'hidden',
        'height': '100%'
      });

      $originalElement.clone().appendTo($modal);
      resetClone($modal);
      $modal.find('.expand-link').remove();
      $modal.append($('<div class="modal-close-icon"></div>'));

      //BOOTSTRAP CAROUSEL SPECIFIC CHANGES
      originalElementId = $originalElement.attr('id');
      $expandedElement = $('.modal-container #' + originalElementId);
      newId = (originalElementId + '-expanded');
      $expandedElement.attr('id', newId);
      $expandedElement.find('[data-target=' + '#' + originalElementId + ']').attr('data-target', '#' + newId);
      $expandedElement.find('.carousel-control').attr('href', '#' + newId);

      //RESIZES MODAL TO DYNAMICALLY CENTER ON BROWSER WINDOW
      $expandedElement.css('margin', '0');

      //CLOSES MODAL
      $('.scrim, .modal-close-icon').click(function () {
        closeExpando();
      });

      //CLOSES MODAL ON EXPANDO IMAGE CLICK (NOT CAROUSEL)
      $('body > div.modal-container > div.embeddable-object.expandable > div.expandable-scrim').click(function () {
        closeExpando();
      });

      sizeModal();
      $(window).bind('resize', sizeModal);

      $(document).trigger('expando.show');
    }

    //OPENS MODAL & CLONES EXPANDABLE ELEMENT WHEN CLICKING ON EXPAND ICON
    $('.expand-link, .expandable-scrim').bind('click', openExpando);
  };
})(jQuery);
