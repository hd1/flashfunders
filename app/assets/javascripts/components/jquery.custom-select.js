(function ($) {

  var placeholder = $('<option value="">Select One</option>');

  function buildCheckbox(name, $option, tabindex) {
    var checked = $option.attr('selected') ? ' checked ' : ' ';
    var myVal = $option.val();

    return '<li class="checkbox"><div class="multi-checkbox-container"><input type="checkbox" data-parent="' + name + '" value="' + myVal + '" id="' + myVal + '"' + checked + 'class="multi-checkbox" tabindex="' + tabindex + '"><span>' + $option.text() + '</span></div></li>';
  }

  function convertMultipleSelectToCheckboxes($elem) {
    var $options = $elem.find('option');
    var name = $elem.attr('id');
    var tabindex = $elem.attr('tabindex');
    var wrapper = "<ul class='styled-multi-select " + $elem.attr('class') + "'> ";

    $options.each(function() {
      wrapper += buildCheckbox(name, $(this), ++tabindex);
    });
    $elem.wrap( wrapper + "</ul>");
    $elem.css('display', 'none');
  }

  function customizeSelects(selects) {
    $.each(selects, function (idx, obj) {
      var $elem = $(obj);
      if ($elem.hasClass('no-placeholder')) {
        $elem.wrap("<div class='styled-select'> </div>");
      } else if ($elem.attr('multiple') !== undefined) {
        convertMultipleSelectToCheckboxes($elem);
      } else {
        $elem.prepend(placeholder.clone())
          .wrap("<div class='styled-select'> </div>")
          .change(function (ignore) {
            if ($(this).find("option:selected").val() === placeholder.val()) {
              $(this).css('color', '#8f9ca3');
            } else {
              $(this).css('color', '#093954');
            }
          });
        if (!$elem.find('option[selected]').length) {
          $elem.val(placeholder.val()).change();
        }
      }
    });
  }

  function checkParentOption($elem) {
    var $parent = $('#' + $elem.data('parent'));
    var $option = $parent.find('option[value="' + $elem.val() + '"]');

    if ($elem.prop('checked')) {
      $option.attr('selected', 'selected');
    } else {
      $option.removeAttr('selected');
    }
  }

  function propagateClick($elem, pattern) {
    $elem.find(pattern).click();
  }

  function propagateFocus(e, onFlag) {
    var $elem = $(e.target);
    if ($elem.hasClass('multi-checkbox')) {
      var $parent = $elem.parent().parent();

      if (onFlag) {
        $parent.addClass('focus');
      } else {
        $parent.removeClass('focus');
      }
    }
  }

  $.fn.customizeSelects = function() {
    var selects = this;

    if (!this.is('select')) {
      selects = this.find('select');
    }
    customizeSelects(selects);
    $('.multi-checkbox').click(function() { checkParentOption($(this)); });

    var $checkboxes = $('li.checkbox');
    $checkboxes.click(function(e) {
      if ($(e.target).prop('tagName') !== 'INPUT') {
        propagateClick($(this), 'input[type="checkbox"]');
      }
    });
    $checkboxes.focusin(function(e) { propagateFocus(e, true); });
    $checkboxes.focusout(function(e) { propagateFocus(e, false); });
  };

}(jQuery));
