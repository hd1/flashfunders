(function ($, window) {

  $.fn.modalFormReturnTo = function() {

    function attachReturnTo(modal, targetLink) {
      var targetPath = targetLink.data('returnTo');

      modal.data('returnTo', targetPath);
    }

    function detachReturnTo(modal) {
      modal.removeData('returnTo');

      $('[data-inherit-return-to=true]', modal).each( function() {
        $(this).removeData('returnTo');
      });
    }

    function showHandler(event) {
      var modal = $(this)
        , targetLink = $(event.relatedTarget);

      attachReturnTo(modal, targetLink);
    }

    function hideHandler() {
      var modal = $(this);

      detachReturnTo(modal);
    }

    function linkReturnToHandler(event) {
      var link = $(this)
        , modal = link.closest('.modal')
        , returnPath = modal.data('returnTo');

      link.data('returnTo', returnPath);
    }

    function formSubmitHandler(event) {
      var form = $(event.currentTarget)
        , formPath = form.attr('action')
        , returnPath = form.closest('.modal').data('returnTo');

      if (returnPath != undefined) {
        formPath = formPath + "?return_to=" + encodeURIComponent(returnPath);
        form.attr('action', formPath);
      }
    }

    this.each( function() {
      $(this)
        .on('show.bs.modal', showHandler)
        .on('hide.bs.modal', hideHandler)
        .on('submit', '.returnable-form', formSubmitHandler)
        .on('click', '[data-inherit-return-to=true]', linkReturnToHandler);
    });

    return this;
  };

})(jQuery, window);


