//
// Rankable
//
// Provides functionality to move DOM elements up/down
// in relation to each other. Rankable items on the
// same level as each other are considered as a group
// when enabling/disabling the rank buttons.
//
// Usage:
//
//   <ul>
//     <li class='rankable'>
//       Dog
//       <a href='#' class='up-rank'>Up</a>
//       <a href='#' class='down-rank'>Down</a>
//     </li>
//     <li class='rankable'>
//       Cat
//       <a href='#' class='up-rank'>Up</a>
//       <a href='#' class='down-rank'>Down</a>
//     </li>
//     <li class='rankable'>
//       Bird
//       <a href='#' class='up-rank'>Up</a>
//       <a href='#' class='down-rank'>Down</a>
//     </li>
//   </ul>
//
//   <script>
//     $('ul').rankable();
//   </script>
//
(function ($, window) {

  var pluginName = "rankable";

  function Plugin(element) {
    this.$element = $(element);
    this.init();
  }

  Plugin.prototype = {
    init: function() {
      this.resetButtons();

      this.$element
        .on('click', '.up-rank', $.proxy(this, "_callbackMoveUp"))
        .on('click', '.down-rank', $.proxy(this, "_callbackMoveDown"));
    },

    resetButtons: function() {
      var $upRanks = $('.up-rank', this.$element),
          $downRanks = $('.down-rank', this.$element);

      $upRanks
        .removeClass('disabled')
        .each( $.proxy(this, "_disableHighestUpRanks") );

      $downRanks
        .removeClass('disabled')
        .each( $.proxy(this, "_disableLowestDownRanks") );
    },

    _callbackMoveUp: function(event) {
      event.preventDefault();

      this._moveUp( $(event.currentTarget).closest('.rankable') );
    },

    _callbackMoveDown: function(event) {
      event.preventDefault();

      this._moveDown( $(event.currentTarget).closest('.rankable') );
    },

    _moveUp: function($target) {
      var $prev = $target.prevAll('.rankable:first');

      if ($prev.length) {
        this._animateMoveUp($target, $prev);
      }
    },

    _moveDown: function($target) {
      var $next = $target.nextAll('.rankable:first');

      if ($next.length) {
        this._animateMoveDown($target, $next);
      }
    },

    _animateMoveUp: function($target, $prev) {
      this._fadeOutIn( $target,
        function() { $target.after($prev); },
        function() { this.resetButtons(); });
    },

    _animateMoveDown: function($target, $next) {
      this._fadeOutIn( $target,
        function() { $target.before($next); },
        function() { this.resetButtons(); });
    },

    _fadeOutIn: function($target, hiddenCallback, shownCallback) {
      var self = this;

      $target.fadeOut( function() {
        $.proxy(hiddenCallback, self)();

        $target.fadeIn(function() {
          $.proxy(shownCallback, self)();
        });
      });
    },

    _disableHighestUpRanks: function(index, element) {
      var $element = $(element),
          $prevElements = $element.closest('.rankable').prevAll('.rankable:visible');

      if ($prevElements.length == 0) {
        $element.addClass('disabled');
      }
    },

    _disableLowestDownRanks: function(index, element) {
      var $element = $(element),
          $nextElements = $element.closest('.rankable').nextAll('.rankable:visible');

      if ($nextElements.length == 0) {
        $element.addClass('disabled');
      }
    }
  };

  $.fn[pluginName] = function (methodOrOptions) {
    return this.each(function () {
      var instance = $.data(this, pluginName);

      // call a public method on an instance
      if ( instance && $.isFunction(Plugin.prototype[methodOrOptions]) ) {
        instance[methodOrOptions].apply(instance, Array.prototype.slice.call( arguments, 1 ));
      }

      // initialize the plugin
      else if ( typeof methodOrOptions === 'object' || !methodOrOptions ) {
        $.data(this, pluginName, new Plugin(this, methodOrOptions));
      }

      // attempting to call a method prior to initializing
      else if ( !instance ) {
        $.error( 'Plugin must be initialised before using method: ' + methodOrOptions );
      }

      // invalid method called
      else {
        $.error( 'Method ' + methodOrOptions + 'does not exist.');
      }
    });
  };
})(jQuery, window);
