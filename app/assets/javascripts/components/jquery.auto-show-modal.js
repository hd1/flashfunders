(function ($) {

  $.fn.autoShowModal = function() {

    function shouldOpen(modalName) {
      var queryString = window.location.search.substring(1);

      if (queryString != undefined) {
        var matcher = new RegExp("m=" + modalName + "([&#].*)?$");

        return matcher.test(queryString);
      }

      return false;
    }

    this.each( function() {
      var modal = $(this);

      if (shouldOpen( modal.attr('id') )) {
        modal.modal('show');
      }
    });

    return this;
  };

})(jQuery);

