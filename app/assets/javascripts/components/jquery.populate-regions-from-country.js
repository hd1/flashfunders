// Country / State selector
//
// Given a selector for a country and a state dropdown, and a path for an ajax call
// to get a list of states for each country, this populates the state dropdown with
// the appropriate list anytime the country is changed.
//
// Usage:
//
//   <select id="state-picker"></select>
//   <select id="country-picker"></select>
//
//   <script>
//     $('#country-picker').stateChooser(stateSelector: '#state-picker', ajaxEndpoint: '/investor_entities/states');
//   </script>
//
(function ($, window) {
  var pluginName = "stateChooser";
  var countryDataHash = null;
  var data = null;
  var ajaxCalled = false;

  /*
   This was working great but if you have multiple country-selectors on a page, the ajaxEndpoint was
   getting invoked multiple times and we end up with multiple copies of what is normally the same
   data. To avoid this, we move the countryData variable out of the instance and put a flag to let
    us know whether the ajax call has been initiated so we only do it once.
   */

  function Plugin(countrySelect, ajaxEndpoint) {
    this.$countrySelect = $(countrySelect);
    this.ajaxEndpoint = ajaxEndpoint;
    this.$stateSelect = $(this.$countrySelect.data('stateselector'));
    this.changeHandler = this.$countrySelect.data('changehandler');
    this.changeTag = this.$countrySelect.data('changetag');
    this.data = this.$countrySelect.data('data');

    this.init();
  }

  Plugin.prototype = {
    init: function () {
      this.countryData();
      this.$countrySelect.change($.proxy(this.countryChanged, this));
    },

    countryData: function() {
      if (countryDataHash == null) {
        countryDataHash = this.getLocal();
        if (countryDataHash == null) {
          this.fetchFromAjax();
        }
      }
      return countryDataHash;
    },

    setLocal: function(data) {
      lscache.set('countryData', data, 60*24*7);
    },

    getLocal: function() {
      return lscache.get('countryData');
    },

    fetchFromAjax: function() {
      if (!ajaxCalled) {
        ajaxCalled = true;
        $.getJSON(this.ajaxEndpoint, $.proxy(function (data) {
          countryDataHash = data;
          this.setLocal(countryDataHash);
        }, this));
      }
    },

    countryChanged: function () {
      this.$stateSelect.attr('disabled', 'disabled');
      this.populateStates();
      executeFnByName(this.changeHandler, window, this.currentCountry(), this.changeTag, this.data);
    },

    populateStates: function () {
      var data = this.countryData();
      if (data == null) return; // No country data loaded yet... Keep state (region) selector disabled

      var items = [], selected = false, that = this, regions = data[this.currentCountry()];
      $.each(regions, function (index, state) {
        selected = selected || that.selectedState(state.code);
        items.push("<option " + that.selectedAttr(that.selectedState(state.code)) + "value='" + state.code + "'>" + state.name + "</option>");
      });

      if (this.data !== undefined && (this.data['region_optional'] || items.length == 0  ) )
        items.unshift('<option selected val="">- Not Applicable -</option>');

      if (items.length > 1) {
        var defaultLabel = items.length == 0 ? '' : 'Select One';
        items.unshift("<option " + this.selectedAttr(!selected) + "val=''>" + defaultLabel + "</option>");
      }
      this.$stateSelect.html(items.join(''));

      if (items.length <= 1) return; // Keep drop down disabled if there are no regions or only one region

      this.$stateSelect.attr('disabled', false);
    },

    currentState: function () {
      return this.$stateSelect.val();
    },

    currentCountry: function () {
      return this.$countrySelect.val();
    },

    selectedState: function (stateCode) {
      return this.currentState() == stateCode;
    },

    selectedAttr: function (selected) {
      if (selected) {
        return "selected=selected ";
      } else {
        return "";
      }
    }
  };

  $.fn[pluginName] = function (options) {
    return this.each(function () {
      if (!$.data(this, pluginName)) {
        $.data(this, pluginName, new Plugin(this, options.ajaxEndpoint));
      }
    });
  };


})(jQuery, window);
