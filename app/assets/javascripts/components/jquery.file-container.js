// File Container
//
// Adds functionality to a button to trigger a file selection
// dialog. Upon selecting a file, visual file representation
// will appear. Subsequent clicks will continue to add new
// file inputs.
//
// Usage:
//
//   <div class="file-container">
//     <button>Choose File</button>
//   </div>
//
//   <script>
//     $('.file-container').fileContainer();
//   </script>
//
(function ($) {

  var pluginName = "fileContainer";

  function Plugin(container, options) {
    this.$container = $(container);
    this.options = $.extend({}, $.fn[pluginName].defaults, options);

    this.init();
  }

  Plugin.prototype = {
    init: function () {
      this.fileList = [];
      this.uploadedFileList = [];
      this._setupFileChooser();
      var self = this;
      $('.existing-file-list span').each(function () {
        self._addFileToUploaded($(this).text());
      });
    },

    _keyToFileName: function (key) {
      var filename = key.substring(key.lastIndexOf('/') + 1);
      // Strip off leading numeric prefix
      return filename.substring(filename.indexOf('_') + 1);
    },

    add: function (file) {
      if (file.size > 50000000) {
        this._showError('The file you are trying to upload exceeds the 50MB limit.');
      } else if (file.size === 0) {
        this._showError('The file you are trying to upload is empty.');
      } else {
        this._addFileToList(file);
      }
    },

    remove: function (file) {
      typeof(file) == 'string' ?
        this._removeFromList(this.uploadedFileList, file) :
        this._removeFromList(this.fileList, file);
    },

    _showError: function (errMsg) {
      var errorSpan = $('<span />').addClass('error file-key-error push-down-small').html(errMsg);
      errorSpan.appendTo(this.$container.find('.file-list'));
    },

    _addFileToList: function (file) {
      this.fileList.push(file);
      this._addFileToListInternal(file, file.name);
    },

    _addFileToUploaded: function (fileKey) {
      var filename = this._keyToFileName(fileKey);
      this.uploadedFileList.push(fileKey);
      this._addFileToListInternal(fileKey, filename);
    },

    _removeFromList: function (list, item) {
      var index = list.indexOf(item);
      return (index > -1) ? list.splice(index, 1) : list;
    },

    _addFileToListInternal: function (file, filename) {
      var self = this
        , fileItem = $('<span />').addClass('file-list-item').html(this._truncateName(filename))
        , removeItem = $('<a />').addClass('remove-file').html('x');

      fileItem.appendTo(this.$container.find('.file-list'));
      removeItem.appendTo(fileItem);

      removeItem.click(function () {
        self.remove(file, filename);
        $(this).parent().remove();
      });

    },

    _setupFileChooser: function () {
      var self = this
        , $fileChooser = $('<input>')
          .attr({type: 'file', class: 'hidden file-chooser', name: 'file-upload-chooser'})
          .appendTo(self.$container);

      $fileChooser.change(function () {
        self.add(this.files[0]);
        self._resetFileInput(this);
      });

      $('button', self.$container).click(function (e) {
        e.preventDefault();
        $('input[type="file"]', self.$container).click();
      });
    },

    _resetFileInput: function (input) {
      var $input = $(input);
      $input.replaceWith($input.clone(true));
    },

    _truncateName: function (name) {
      var truncatedName = name;

      if (name.length > 30) {
        var fileName = name.substring(0, 25) + '...';
        var fileType = name.substring(name.lastIndexOf('.') + 1, name.length);

        truncatedName = fileName + fileType;
      }

      return truncatedName;
    }
  };

  $.fn[pluginName] = function (options) {
    return this.each(function () {
      if (!$.data(this, pluginName)) {
        $.data(this, pluginName, new Plugin(this, options));
      }
    });
  };

  $.fn[pluginName].defaults = {};

})(jQuery);

