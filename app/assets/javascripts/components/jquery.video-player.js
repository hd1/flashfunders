(function ($, window) {

  var pluginName = "videoPlayer";

  function Plugin(container, options) {
    this.$container = $(container);
    this.options = $.extend({}, $.fn[pluginName].defaults, options);

    this.init();
  }

  Plugin.prototype = {

    init: function () {
      var $video = $('video', this.$container);

      // init
      this.video = $video[0];
      this.$closeLink = $('.close-video', this.$container);
      this.$videoProgress = $('.video-progress', this.$container);

      // init video controls
      this.$playButton = $('.video-control.play');
      this.$pauseButton = $('.video-control.pause');
      this.$volumeButton = $('.video-control.volume');
      this.$fullScreenButton = $('.video-control.expand');

      // init video progress
      this.progressInterval = null;
      this.$videoProgress = $('.video-progress', this.$container);

      // init volume control
      this.$volumeSlider = $('.video-volume-control', this.$container);

      // setup events
      $video.click( $.proxy(this._togglePlay, this) );
      this.$closeLink.click( $.proxy(this.hide, this) );
      this.$playButton.click( $.proxy(this.play, this) );
      this.$pauseButton.click( $.proxy(this.pause, this) );
      this.$volumeButton.click( $.proxy(this._toggleMute, this) );
      this.$fullScreenButton.click( $.proxy(this.fullScreen, this) );
      this.video.addEventListener("ended", $.proxy(this.finish, this), true);
      this._setupProgressBar();
      this._setupVolumeSlider();
      this._setupVolumeButton();
      this._setupVideoHover();
      this._checkAndDisableFullScreen();
    },

    show: function() {
      this.$container
        .removeClass('video-stopped')
        .addClass('video-playing');

      $('.video-wrapper', this.$container).find('img').hide();

      this._resizePlayer();
      this.enableShortcuts();

      if(this.desktop()) {
        this.$closeLink.show();
      }

      $('.progress-marker.total-duration')
        .html(this._convertTime(this.video.duration));

      this.play();
    },

    hide: function() {
      var self = this;

      this.pause();

      this.$closeLink.hide();
      $('.company-video').removeClass('playing');
      $('.video-wrapper img').show();
      $('.video-controls', '.company-video').hide();

      self.$container.removeClass('video-playing');
      self.$container.addClass('video-stopped');
      self.options.onHide();

      this.disableShortcuts();
    },

    mobile: function() {
      return (im.lessThan('tablet'));
    },

    tablet: function() {
      return (im.greaterThan('tablet') && im.lessThan('desktop'));
    },

    desktop: function() {
      return im.greaterThan('desktop');
    },

    play: function() {
      this.video.play();
      this._startProgress();

      this.$playButton.removeClass('active');
      this.$pauseButton.addClass('active');
      this.options.onPlay();
    },

    pause: function() {
      this.video.pause();
      this._stopProgress();

      this.$playButton.addClass('active');
      this.$pauseButton.removeClass('active');
    },

    finish: function() {
      this.options.onFinish();
    },

    setVolume: function(level) {
      this.video.volume = level;
    },

    mute: function(value) {
      if (value) {
        this.setVolume(0);
        this.$volumeButton.addClass('mute');
        $('.ui-slider-range', this.$volumeSlider).css('height','0');
        $('.ui-slider-handle', this.$volumeSlider).css('bottom','0');

      } else {
        this.setVolume(1);
        this.$volumeButton.removeClass('mute');
        $('.ui-slider-range', this.$volumeSlider).css('height','100%');
        $('.ui-slider-handle', this.$volumeSlider).css('bottom','100%');
      }
    },

    skipTo: function(position) {
      this.video.currentTime = position;
      this._updateProgressBar();
    },

    _fullScreenFunction: function() {
      if (this.video.requestFullScreen) {
        return this.video.requestFullScreen;
      } else if (this.video.msRequestFullscreen) {
        return this.video.msRequestFullscreen;
      } else if (this.video.mozRequestFullScreen) {
        return this.video.mozRequestFullScreen;
      } else if (this.video.webkitRequestFullscreen) {
        return this.video.webkitRequestFullscreen;
      } else {
        return false;
      }
    },

    fullScreen: function() {
      var fsFunc = this._fullScreenFunction();
      if (fsFunc) {
        $.proxy(fsFunc, this.video)();
      }
    },

    _checkAndDisableFullScreen: function() {
      var fsFunc = this._fullScreenFunction();
      if (!fsFunc) {
        $('.video-control.expand').remove();
        $('.video-control.volume').addClass('border-right');
      }
    },

    enableShortcuts: function() {
      $(document).bind('keydown', $.proxy(this._keyStrokeHandler, this));
    },

    disableShortcuts: function() {
      $(document).unbind('keydown', $.proxy(this._keyStrokeHandler, this));
    },

    _timeElapsed: function() {
      return this.video.currentTime;
    },

    _timeRemaining: function() {
      return this.video.duration - this.video.currentTime;
    },

    _percentComplete: function() {
      var percent = this.video.currentTime / this.video.duration * 100;
      return percent.toString() + '%';
    },

    _togglePlay: function() {
      if (this.video.paused) {
        this.play();
      } else {
        this.pause();
      }
    },

    _toggleMute: function() {
      this.mute( !this.$volumeButton.hasClass('mute') );
    },

    _startProgress: function() {
      this.progressInterval = window.setInterval(
        this._updateProgressBar.bind(this), 250
      );
    },

    _stopProgress: function() {
      window.clearInterval(this.progressInterval);
    },

    _updateProgressBar: function() {
      var slider = $('.ui-slider-handle', this.$videoProgress),
          sliderRange = $('.ui-slider-range', this.$videoProgress),
          currentTime = $('.progress-marker.current-time');

      slider.css('left', this._percentComplete());
      sliderRange.css('width', this._percentComplete());

      currentTime.html(this._convertTime(this.video.currentTime));
    },

    _showVolumeSlider: function() {
      this.$volumeSlider
        .show();
    },

    _hideVolumeSlider: function() {
      this.$volumeSlider.hide();
    },

    _resizePlayer: function() {
      $('.company-video').addClass('playing');
    },

    _convertTime: function(seconds) {
      var min = Math.floor(seconds / 60),
          sec = Math.floor(seconds - (min * 60)),
          paddedSec = sec < 10 ? "0" + sec : sec;

      return min + ":" + paddedSec;
    },

    _keyStrokeHandler: function(event) {
      switch( event.which ) {
        case 27: // escape
          this.hide();
          break;
        case 32: // spacebar
          event.preventDefault();
          this._togglePlay();
          break;
      }
    },

    _setupProgressBar: function() {
      var self = this;

      $('#video_progress_slider').slider({
        orientation: "horizontal",
        range: "min",
        min: 0,
        max: 100,
        value: 0,
        slide: function (event, element) {
          var position = element.value * self.video.duration / 100;
          self.skipTo(position);
        }
      });
    },

    _setupVolumeSlider: function() {
      var self = this;

      $("#volume_slider").slider({
        orientation: "vertical",
        range: "min",
        min: 0,
        max: 100,
        value: 100,
        slide: function (event, element) {
          var level = element.value / 100;
          self.setVolume(level);
        }
      });
    },

    _showVideoControls: function() {
      if(this.desktop()) {
        $('.video-controls', '.company-video.playing').fadeIn();
      }
    },

    _hideVideoControls: function() {
      if(this.desktop()) {
        $('.video-controls', '.company-video').fadeOut('slow');
      }
    },

    _setupVideoHover: function() {
      var self = this;
      $(this.$container).hover( function() { self._showVideoControls(); }, function() { self._hideVideoControls(); } );
    },

    _setupVolumeButton: function() {
      var self = this;

      this.$volumeButton.hover( function() { self._showVolumeSlider(); }, function() { self._hideVolumeSlider(); });
    }
  };

  $.fn[pluginName] = function (methodOrOptions) {
    return this.each(function () {
      var instance = $.data(this, pluginName);

      // call a public method on an instance
      if ( instance && $.isFunction(Plugin.prototype[methodOrOptions]) ) {
        instance[methodOrOptions].apply(instance, Array.prototype.slice.call( arguments, 1 ));
      }

      // initialize the plugin
      else if ( typeof methodOrOptions === 'object' || !methodOrOptions ) {
        $.data(this, pluginName, new Plugin(this, methodOrOptions));
      }

      // attempting to call a method prior to initializing
      else if ( !instance ) {
        $.error( 'Plugin must be initialised before using method: ' + methodOrOptions );
      }

      // invalid method called
      else {
        $.error( 'Method ' + methodOrOptions + 'does not exist.');
      }
    });
  };

  $.fn[pluginName].defaults = {
    onHide: function() {},
    onPlay: function() {},
    onFinish: function() {}
  };

})(jQuery, window);
