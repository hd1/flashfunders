(function ($, window) {
  "use strict";
  var pluginName = "topTrafficSources";

  function Plugin(container, options) {
    this.top_container = $(container);
    var containerData = this.top_container.data();
    this.date_format = "YYYY-MM-DD";
    this.date_format_for_display = "MMM D, YYYY";
    if(containerData.endDate !== undefined) {
      this.offering_end_date = containerData.endDate;
    } else {
      this.offering_end_date = moment().format(this.date_format);
    }
    if(containerData.offeringStartDate !== undefined) {
      this.offering_start_date = containerData.offeringStartDate;
    } else {
      this.offering_start_date = "";
    }

    this.container = $('.stats-container', container);
    this.item_container = $('.items', this.container);
    this.select = $('select', this.top_container);
    this.options = $.extend({}, $.fn[pluginName].defaults, options);
    this.empty_container = $('.empty', this.top_container);

    this.cached_data = {
      all_time: {
        name: "All Time",
        data: [],
        timeframe: {}
      },
      last_30_days: {
        name: "Last 30 Days",
        data: [],
        timeframe: {}
      },
      last_7_days: {
        name: "Last 7 Days",
        data: [],
        timeframe: {}
      },
      today: {
        name: "Last 24 Hours",
        data: [],
        timeframe: {}
      }
    };
    this.init();
  }

  Plugin.prototype = {

    init: function () {
      this.set_timeframe_selections();
      this.get_all_top_traffic_data();
    },

    spinner: function() {
      return $('<div class="spinner-container"><div class="la-ball-clip-rotate la-dark"><div></div></div></div>');
    },

    set_timeframe_selections: function() {
      var $this = this;
      $this.select.html('');
      $("option", $this.select).remove();
      $.each(this.cached_data, function(k, v) {
        var option = $('<option>');
        option.attr('value', k);
        if(k === 'all_time') {
          option.attr('selected', 'true');
        }
        option.text(v.name);
        $this.select.append(option);
      });

      $this.select.on('change', function() { $this.generate_and_render_referrers(); } );
    },

    get_all_top_traffic_data: function() {
      var $this = this;
      $this.item_container.html("");
      $this.item_container.append(this.spinner());

      $this.get_top_traffic_for_timeframe("all_time", function() {
        $this.generate_and_render_referrers();
        $this.get_top_traffic_for_timeframe("last_30_days", function() {
          $this.get_top_traffic_for_timeframe("last_7_days", function() {
            $this.get_top_traffic_for_timeframe("today");
          });
        });
      });
    },

    get_top_traffic_for_timeframe: function(key_timeframe, callback) {
      var $this = this;
      var data = {
        data_set: "referral_info"
      };
      var timeframe = this.get_timeframe(key_timeframe);
      if(timeframe !== undefined) {
        data.start_date = timeframe.start_date;
        data.end_date = timeframe.end_date;
      }

      $.ajax({
        url: "/api/issuer/analytics/get_data",
        data: data,
        success: function(data) {
          $this.store_cache(key_timeframe, data, timeframe);
          if (typeof callback === "function") {
            callback.call();
          }
        },
        error: function() {
          $this.store_cache(key_timeframe, {}, timeframe);
        }
      });
    },

    store_cache: function(key_timeframe, data, timeframe) {
      this.cached_data[key_timeframe].data = data;
      this.cached_data[key_timeframe].timeframe = timeframe;
    },

    generate_and_render_referrers: function() {
      var $this = this;
      var key_timeframe = $this.select.val();
      var data = $this.cached_data[key_timeframe].data;
      if(data.data === undefined) {
        $this.renderEmpty();
        return;
      }
      var items = $.map(data.data, function(item) {
        return $this.get_template_for(item);
      });
      if(items.length === 0) {
        $this.renderEmpty();
      } else {
        $this.render(items, data.total, $this.cached_data[key_timeframe].timeframe);
      }
    },

    format_number_for_display: function(numStr) {
      var num = Number(numStr);
      return num.toLocaleString('en');
    },

    get_template_for: function(item) {
      var ref = item[0],
        count_val = this.format_number_for_display(item[1]),
        share_val = item[2],
        perc = Math.round(share_val * 100, 2);

      var item_wrapper = $('<div class="span4 item"></div>');

      var top_row = $('<div class="row-fluid"></div>');
      var referrer = $('<div class="span8 referrer">' + ref + '</div>');
      var count = $('<div class="span2 count">' + count_val + '</div>');
      var share = $('<div class="span2 share">' + perc + '%</div>');

      top_row.append(referrer);
      top_row.append(count);
      top_row.append(share);

      var bottom_row = $('<div class="row-fluid"></div>');
      var slider_container = $('<div class="slider span12"></div>');
      var slider_bar = $('<div class="slider-value" style="width: ' + perc + '%"></div>');

      slider_container.append(slider_bar);
      bottom_row.append(slider_container);

      item_wrapper.append(top_row);
      item_wrapper.append(bottom_row);

      return item_wrapper;
    },

    get_timeframe: function(key_timeframe) {
      var end_date;
      if(moment(this.offering_end_date).isBefore(moment())) {
        end_date = moment(this.offering_end_date);
      } else {
        end_date = moment();
      }
      var start_date;

      switch(key_timeframe) {
        case 'last_30_days':
          start_date = moment(end_date).subtract(30, 'days').format(this.date_format);
          break;

        case 'last_7_days':
          start_date = moment(end_date).subtract(7, 'days').format(this.date_format);
          break;

        case 'today':
          start_date = moment(end_date).subtract(24, 'hours').format(this.date_format);
          break;

        default:
          start_date = moment(this.offering_start_date).format(this.date_format);
          break;
      }
      return {start_date: start_date, end_date: end_date.format(this.date_format)};
    },

    renderEmpty: function() {
      this.container.hide();
      this.empty_container.show();
    },

    render: function(items, total, timeframe) {
      var $this = this;
      $this.container.show();
      $this.empty_container.hide();
      $this.item_container.html('');
      $('.totals span.number', $this.container).html(total.toLocaleString('en'));
      if(timeframe !== undefined) {
        var text = moment(timeframe.start_date).format(this.date_format_for_display) + " - " + moment(timeframe.end_date).format(this.date_format_for_display);
        if(moment(this.offering_end_date).isBefore(moment())) {
          text += " (Offering end)";
        }
        $('.timeframe span', $this.container).html(text);
      } else {
        $('.timeframe span', $this.container).html("");

      }
      $.each(items, function(key, elem) {
        $this.item_container.append(elem);
      });
    }
  };

  $.fn[pluginName] = function (methodOrOptions) {
    return this.each(function () {
      var instance = $.data(this, pluginName);

      // call a public method on an instance
      if ( instance && $.isFunction(Plugin.prototype[methodOrOptions]) ) {
        instance[methodOrOptions].apply(instance, Array.prototype.slice.call( arguments, 1 ));
      }

      // initialize the plugin
      else if ( typeof methodOrOptions === 'object' || !methodOrOptions ) {
        $.data(this, pluginName, new Plugin(this, methodOrOptions));
      }

      // attempting to call a method prior to initializing
      else if ( !instance ) {
        $.error( 'Plugin must be initialised before using method: ' + methodOrOptions );
      }

      // invalid method called
      else {
        $.error( 'Method ' + methodOrOptions + 'does not exist.');
      }
    });
  };

}(jQuery, window));

$.scope('#top-traffic', function (page) {
  'use strict';
  page.topTrafficSources(page);
});
