(function ($, window) {

  var pluginName = "batchUploaderForm";

  function Plugin(element, options) {
    this.$form = $(element);
    this.$submitButton = $('#' + this.$form.data('submit-btn'));
    this.options = $.extend({}, $.fn[pluginName].defaults, options);
    this.submitText = this.$submitButton.text();

    this.init();
  }

  Plugin.prototype = {
    init: function () {
      var self = this;

      $('.file-picker').fileContainer();

      self.setInProgress(false);

      $(window).on('beforeunload', function () {
        if (self.uploadInProgress) {
          return "Your documents are being uploaded.  This may take a few minutes to complete.";
        }
      });

      this.$form.submit(function (e) {
        e.preventDefault();
        self.uploadAndSubmit();
      });
    },

    uploadAndSubmit: function () {
      var self = this,
          $uploadForm = $('#file-upload-form'),
          $fileContainer = $('.file-picker', this.$form).data('fileContainer');

      if ($fileContainer != undefined && $fileContainer.fileList.length > 0) {
        self.disableSubmitBtn(true);
        self.disablePage(true);
        self.setInProgress(true);

        $uploadForm
          .customFileUpload({enumerate: true})
          .batchUploader({
            onProgressCheck: function (percentDone) {
              self.setUploadStatus(percentDone);
            },
            onSuccess: function (file, key) {
              self.addKeyInput(key);
              $fileContainer.remove(file);
            },
            onBatchSuccess: function () {
              //Submit the non-jQuery version of the form so we don't trigger the callback
              self.reportPreviouslyUploadedFiles($fileContainer);
              self.setInProgress(false);
              self.$form.get(0).submit();
            },
            onBatchFailure: function () {
              self.options.onBatchFailure();
              self.cancelUploadStatus();
              self.disableSubmitBtn(false);
              self.disablePage(false);
              self.setInProgress(false);
            }
          });

        $uploadForm.customFileUpload('add', {files: $fileContainer.fileList});

      } else {
        //Submit the non-jQuery version of the form so we don't trigger the callback
        self.reportPreviouslyUploadedFiles($fileContainer);
        self.$form.get(0).submit();
      }
    },

    reportPreviouslyUploadedFiles: function($fileContainer) {
      for (var i = 0; i < $fileContainer.uploadedFileList.length; i++) {
        this.addKeyInput($fileContainer.uploadedFileList[i]);
      }
    },

    setInProgress: function(status) {
      this.uploadInProgress = status;
    },

    disableSubmitBtn: function (disable) {
      this.$submitButton.attr('disabled', disable);
    },

    disablePage: function (disable) {
      if (disable) {
        $('#scrim').addClass('show');
      } else {
        $('#scrim').removeClass('show');
      }
    },

    setUploadStatus: function (percent) {
      var btnText = 'Uploading...' + percent + '%' ;

      this.$submitButton
          .text(btnText)
          .addClass('btn-progress');
    },

    cancelUploadStatus: function () {
      this.$submitButton
          .text(this.submitText)
          .removeClass('btn-progress');
    },

    addKeyInput: function (key) {
      $('.file-key-input', this.$form)
          .clone(true)
          .val(key)
          .removeClass('file-key-input')
          .prop('disabled', false)
          .appendTo(this.$form);
    }
  };

  $.fn[pluginName] = function (options) {
    return this.each(function () {
      $.data(this, pluginName, new Plugin(this, options));
    });
  };

  $.fn[pluginName].defaults = {
    onBatchFailure: function() {}
  };

})(jQuery, window);
