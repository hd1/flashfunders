/**
 * Created by seth on 7/8/15.
 */

/*
  Enable components to accept a javascript function by name and then execute it
 */
function executeFnByName(functionName, context /*, args */) {
  if (functionName === undefined) { return; }
  var args = [].slice.call(arguments).splice(2);
  var namespaces = functionName.split(".");
  var func = namespaces.pop();
  for(var i = 0; i < namespaces.length; i++) {
    context = context[namespaces[i]];
  }
  if (context[func] !== undefined) {
    return context[func].apply(this, args);
  }
}
