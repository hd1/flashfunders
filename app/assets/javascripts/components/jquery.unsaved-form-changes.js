// This plugin watches for changes to input, textarea and select fields on a form
// If it sees changes, it adds the class .changed-input to the form. If the user 
// attempts to navigate away from the page, he gets an "unsaved changes" warning.
// When the user clicks the submit button, the .changed-input class is removed from 
// the form.
(function ($, window) {

  var pluginName = "unsavedFormChanges";

  function Plugin(element) {
    this.$form = $(element);
    this.init();
  }

  Plugin.prototype = {
    init: function() {

      $(window)
        .on('beforeunload', $.proxy(this, '__checkForUnsavedChanges'));

      this.$form
        .on('submit', $.proxy(this, '__removeChangedInputClass'))
        .on('change keyup keydown', 'input, textarea, select', $.proxy(this, '__addChangedInputClass'));
    },

    // ----------------------------------------
    // Event Callbacks
    // ----------------------------------------

    __checkForUnsavedChanges: function(event) {
      if ($('.changed-input').length) {
        return 'You haven\'t saved your changes.';
      }
    },

    __removeChangedInputClass: function(event) {
      this.$form.removeClass('changed-input');
    },

    __addChangedInputClass: function(event) {
      this.$form.addClass('changed-input');
    }

  };

  $.fn[pluginName] = function() {
    return this.each(function() {
      $.data(this, pluginName, new Plugin(this));
    });
  };

})(jQuery, window);


