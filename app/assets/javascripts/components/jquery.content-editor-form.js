(function ($, window) {
  'use strict';
  var pluginName = "contentEditor";

  function Plugin(element, options) {
    this.$form = $(element);
    this.options = $.extend({}, $.fn[pluginName].defaults, options);

    this.$fileUploadForm = $('#' + this.options.fileUploadFormId);

    this.init();
  }

  Plugin.prototype = {
    init: function () {
      this.$fileUploadForm
        .customFileUpload({
          enumerate: true
        })
        .bind('fileuploaddone', $.proxy(this, '_callbackUploadSuccess'));

      this.$form
        .on('submit', $.proxy(this, '_callbackSubmit'))
        .on('click', '.add-image', $.proxy(this, '_callbackAddImage'))
        .on('click', '.add-template', $.proxy(this, '_callbackAddTemplate'))
        .on('click', '.remove-element', $.proxy(this, '_callbackRemoveElement'))
        .on('change', 'input:file', $.proxy(this, '_callbackImageChosen'));
    },

    // ----------------------------------------
    // Event Callbacks
    // ----------------------------------------
    _callbackSubmit: function (event) {
      this._updateRanking();
    },

    _callbackAddImage: function (event) {
      event.preventDefault();

      this._openFileChooser(event.currentTarget);
    },

    _callbackAddTemplate: function (event) {
      event.preventDefault();

      this._addTemplate(event.currentTarget);
    },

    _callbackRemoveElement: function (event) {
      event.preventDefault();

      this._removeTemplate(event.currentTarget);
    },

    _callbackImageChosen: function (event) {
      var input = event.target
        , file = input.files[0]
        , $button = $(input).prev('button');

      this._addImageTemplate($button, file);
      this._resetFileChooser(input);
    },

    _callbackUploadSuccess: function (event, data) {
      var $template = data.context
        , key = $('Key', data.result).text();

      if ($template) {
        this._updateFileKey(key, $template);
      } else {
        this._callbackPitchImageUploadFailed(data);
        $('html,body').animate({ scrollTop: 0}, 'fast');
        $('.flash-alert').text("Sorry, something went wrong while uploading your image. Please reload the page and try again.").show();
      }
    },
    // ----------------------------------------
    // local routines
    // ----------------------------------------
    _addTemplate: function (button) {
      var $button = $(button)
        , target = $button.data('target')
        , $template = $(this._getTemplate(button));

      $button.closest(target).before($template);

      this.options.onAddTemplate($template);
    },

    _addImageTemplate: function (button, file) {
      var $button = $(button)
        , target = $button.data('target')
        , $template = $(this._getTemplate(button));

      $button.closest(target).before($template);

      // Enforce max file size
      if (file.size < this.options.maxImageSize) {
        this._displayImagePreview(file, $template);
        this._uploadFile(file, $template);

        this.options.onAddTemplate($template);
      } else {
        alert('Your file is too large. Please choose a file smaller than ' + this.options.maxImageSize / 1024 + 'kb.');
      }
    },

    _removeTemplate: function (button) {
      var self = this
        , $button = $(button)
        , $target = $button.data('target')
        , $template = $button.closest($target);

      $template.fadeOut(function () {
        self.options.onRemoveTemplate($template);
      });

      $('.destroy', $template).val(true);
    },

    _openFileChooser: function (button) {
      $(button)
        .next('input:file')
        .click();
    },

    _resetFileChooser: function (input) {
      var $input = $(input)
        , $clone = $input.clone(true);

      $input.replaceWith($clone);
    },

    _getTemplate: function (element) {
      var $element = $(element)
        , field_id = $element.data('id')
        , template = $element.data('template');

      return this._replacePlaceholders(template, field_id);
    },

    _displayImagePreview: function (file, $template) {
      var reader = new FileReader();

      reader.onload = function (e) {
        var localPath = e.target.result
          , $image = $('<img>').attr('src', localPath);

        $('.photo', $template).append($image);
      };
      reader.readAsDataURL(file);
    },

    _uploadFile: function (file, $template) {
      this.$fileUploadForm.customFileUpload('add', {
        files: file,
        context: $template
      });
    },

    _updateFileKey: function (key, $template) {
      $('.photo input:hidden', $template).val(key);
    },

    _replacePlaceholders: function (template, placeholder) {
      var time = new Date().getTime()
        , regexp = new RegExp(placeholder, 'g');

      return template.replace(regexp, time);
    },

    _updateRanking: function () {
      $.each(this._getRankInputGroups(), function () {
        var inputs = this;

        // set the position value of each rank input
        $.each(inputs, function (index, input) {
          input.val(index);
        });
      });
    },

    _getRankInputGroups: function () {
      var groups = [];

      // clear previous groupings (if any)
      $('.template-group').removeData('groupId');

      // group all nested rank inputs together
      $('input.rank')
        .map(function (index, element) {
          return {container: $(this).closest('.template-group'), input: $(this)};
        })
        .each(function () {
          var groupId = this.container.data('groupId');

          if (groupId === undefined) {
            groupId = groups.length;
            groups[groupId] = [];

            this.container.data('groupId', groupId);
          }

          groups[groupId].push(this.input);
        });

      return groups;
    },

    _callbackPitchImageUploadFailed: function(data) {
      Honeybadger.notify("Pitch Image Upload Error", {
        context: {
          page: document.URL,
          type: 'pitch image upload failed',
          key: $('Key', data.result).text(),
          data: data
        }
      });
    }
  };

  $.fn[pluginName] = function (options) {
    return this.each(function () {
      $.data(this, pluginName, new Plugin(this, options));
    });
  };

  $.fn[pluginName].defaults = {
    fileUploadFormId: 'file-upload-form',
    onAddTemplate: function (template) {},
    onRemoveTemplate: function (template) {},
    maxImageSize: 50 * 1024 * 1024
  };

}(jQuery, window));
