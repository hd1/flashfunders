(function ($) {
  $.fn.socialSharingPopup = function (options) {
    var settings = $.extend({
      width: 640,
      height: 440
    }, options);

    $.each(this, function(index, link) {
      var $link = $(link);
      var shareUrl = $link.attr('href');
      var width = settings.width;
      var height = settings.height;

      $link.on('click', function (e) {
        e.preventDefault();

        var pos = FF.CalculatePopupPosition(window.outerWidth, window.outerHeight, width, height, window.screenX, window.screenY);
        var popupSettings = 'toolbar=no, menubar=no, scrollbars=no, resizable=no, location=no, directories=no, status=no';
        window.open(shareUrl, '', 'width=' + width + ', height=' + height + ', left=' + pos.left + ', top=' + pos.top + popupSettings);
      });
    });
  };
})(jQuery);
