(function ($, window) {
  'use strict';
  var pluginName = "progressButton";

  function Plugin(element, options) {
    this.$btn = $(element);
    this.options = $.extend({}, $.fn[pluginName].defaults, options);
    this.$fileUploadForm = $('#' + this.options.fileUploadFormId);
    this._uploadCount = 0;

    this.init();
  }

  Plugin.prototype = {
    init: function () {
      this.$fileUploadForm
        //.bind('fileuploadfail', $.proxy(this, '_callbackUploadFail'))
        .bind('fileuploadadd', $.proxy(this, '_callbackUploadAdd'))
        .bind('fileuploadalways', $.proxy(this, '_callbackUploadAlways'))
        .bind('fileuploadprogressall', $.proxy(this, '_callbackUploadProgress'));

      this.$btn.click($.proxy(this, '_callbackButtonClick'));
    },

    // ----------------------------------------
    // Event Callbacks
    // ----------------------------------------

    _callbackButtonClick: function(event) {
      event.preventDefault();

      if (this._uploadCount > 0) {
        this.$btn
          .text('Uploading ...')
          .addClass('btn-progress')
          .attr('disabled', true);
      } else {
        this.options.onComplete();
      }
    },

    _callbackUploadAdd: function () {
      this._uploadCount += 1;
    },

    _callbackUploadAlways: function () {
      this._uploadCount -= 1;
      if (this._uploadCount < 1 && this.$btn.hasClass('btn-progress')) {
        this.$btn.text('Upload complete');
        this.options.onComplete();
      }
    },

    _callbackUploadProgress: function (event, data) {
      var percentage = parseInt(data.loaded / data.total * 100, 10);
      if (this.$btn.hasClass('btn-progress')) {
        this.$btn.text(percentage + '% ' + 'Uploading ...');
      }
    }
  };

  $.fn[pluginName] = function (options) {
    return this.each(function () {
      $.data(this, pluginName, new Plugin(this, options));
    });
  };

  $.fn[pluginName].defaults = {
    fileUploadFormId: 'file-upload-form',
    onComplete: function() {}
  };

}(jQuery, window));
