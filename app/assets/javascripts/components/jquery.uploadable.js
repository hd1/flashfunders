// Uploadable
//
// Provides functionality to trigger a file upload
// using a carrierwave direct form.
//
// Options:
//
//   displayType:      'filename' or 'image'
//                     Determines the style of output provided once the
//                     file has been successfully uploaded. Filename
//                     will append the file name string. Image will
//                     append an image tag for the uploaded source.
//                     Defaults to 'filename'.
//
//   addBtnClass:      Class name indicating a new file to be uploaded.
//                     Defaults to 'btn-add-file'.
//
//   updateBtnClass:   Class name indicating an existing file will be
//                     updated.
//                     Defaults to 'btn-update-file'.
//
//   fileContentClass: Class name indicating the displayed content of
//                     the new/update area.
//                     Defaults to 'file-content'.
//
//   fileUploadFormId: The ID reference to a file upload form. This form
//                     is used to to handle the file upload via AJAX.
//                     Defaults to 'file-upload-form'
//
//   validateRatio:    [minimum width, minimum height]
//                     For images, will validate that ratio is aspect of width x height.
//
//   maxFileSize:      Maximum file size (in kilobytes).
//
// Usage:
//
//   <input type='file' name='file-chooser' />
//   <input type='hidden' name='key' class='file-key' />
//
//   <div class='btn-add-file uploadable'>
//     <div class='btn-overlay'>Update</div>
//     <span class='file-content'>Click to Add</span>
//   </div>
//
//   <script>
//     $('.uploadable').uploadable();
//   </script>
//
(function ($, window) {

  var pluginName = "uploadable";

  function Plugin(element, options) {
    this.$element = $(element);
    this.options = $.extend({}, $.fn[pluginName].defaults, options);

    this.init();
  }

  Plugin.prototype = {
    init: function() {
      this.$fileChooser = this.$element.prevAll('input:file').first();
      this.$fileKey = this.$element.prevAll('input.file-key').first();
      this.$uploadForm = $('#' + this.options.fileUploadFormId);
      this.$contentType = $('#' + this.options.contentTypeInputId);

      this.$uploadForm
        .customFileUpload()
        .on('fileuploaddone', $.proxy(this, '_callbackUploadSuccess'));

      this.$element
        .on('click', $.proxy(this, '_callbackAddFile'));

      this.$fileChooser
        .on('change', $.proxy(this, '_callbackFileChosen'));
    },

    _callbackAddFile: function(event) {
      event.preventDefault();

      this._openFileChooser();
    },

    _callbackFileChosen: function(event) {
      var input = event.target
        , file = input.files[0];

      if(file.size == 0){
        this.showFileError(input, 'File cannot be empty');
      } else if(this.validFileSize(file)) {
        this.showFileError(input, "File cannot be over " + this.options.maxFileSize + "kb.");
      } else {
        this._setFileContentType(file);
        this._showInProgress(true);
        this._uploadAndDisplayFile(file);
      }
      this._resetFileChooser();
    },

    showFileError: function(input, message) {
      var invalid_file = $(input).parent().append("<span class='error'>" + message + "</span>");
      $('.error', invalid_file).delay(5000).fadeOut();
    },

    validFileSize: function(file) {
      return (this.options.maxFileSize > 0 && file.size > (this.options.maxFileSize * 1024));
    },

    _setFileContentType: function(file) {
      this.$contentType.val(file.type);
    },

    _callbackUploadSuccess: function(event, data) {
      var context = data.context
        , key = $('Key', data.result).text();

      // need to check if the file uploaded is meant to be
      // handled by this plugin.
      if ( context != undefined && $(context).is(this.$element) ) {
        this._showInProgress(false);
        this._setFileKey(key);
      }
    },

    _openFileChooser: function(button) {
      this.$fileChooser.click();
    },

    _setFileKey: function(key) {
      this.$fileKey.val(key);
    },

    _uploadAndDisplayFile: function(file) {
      var self = this;

      $.when(self._isValidImage(file)).done(function(result) {
        self.$element.closest('fieldset').children(".upload-image-error").remove();

        if (result.isValid || result.isValid == 'no-validation') {
          self._uploadFile(file);
          self._displayFile(file);
        } else {
          var $warning = $('<div class="upload-image-error">' + result.errorMessage + '</div>')
            , contentSelector = '.' + self.options.fileContentClass;

          $(contentSelector , self.$element).closest('fieldset').prepend($warning);
        }
      });
    },

    _isValidImage: function(file) {
      if (!this.options.validateRatio) {
        return { isValid: 'no-validation' };
      }

      var reader = new FileReader()
        , deferred = $.Deferred()
        , self = this;

      reader.onload = function (e) {
        $('<img>').on('load', function() {
          deferred.resolve(self._validateImage(this));
        }).attr('src', e.target.result);
      };

      reader.readAsDataURL(file);
      return deferred.promise();
    },

    _validateImage: function(image) {
      var rect = { width: 0, height: 0, isValid: false, errorMessage: '' };

      rect.width = this.options.validateRatio[0];
      rect.height = this.options.validateRatio[1];

      switch (true) {
        case (!rect.width || !rect.height):
          rect.isValid = 'no-validation';
          break;
        default:
          var ratio = this._findSimplestFormRatio(rect.width, rect.height);
          rect.isValid = (image.width * rect.height == image.height * rect.width) && (image.width >= rect.width);
          rect.errorMessage = 'Your image file should have an aspect ratio of ' + ratio[0] + ':' + ratio[1] + ' and be at least ' + rect.width + 'px wide.';
      }

      return rect;
    },

    _findSimplestFormRatio: function(numerator, denominator) {
      var gcd = function(a, b) { return (!b) ? a : gcd(b, a % b); }; // Greatest Common Divider
      var gc_denominator = gcd(numerator, denominator);
      return [numerator / gc_denominator, denominator / gc_denominator];
    },

    _uploadFile: function(file) {
      this.$uploadForm.customFileUpload('add', {
        files: file,
        context: this.$element
      });
    },

    _displayFile: function(file) {
      switch (this.options.displayType) {
        case 'image':
          this._updateImage(file);
          break;
        case 'filename':
          this._updateFileName(file);
          break;
      }
    },

    _updateImage: function(file) {
      var self = this
        , reader = new FileReader();

      reader.onload = function (e) {
        var localPath = e.target.result,
            $image = $('<img>').attr('src', localPath);

        self._replaceContent($image);
        self._setAsUpdated();
      };
      reader.readAsDataURL(file);
    },

    _updateFileName: function(file) {
      var $filename = $('<span>').text(file.name);

      this._replaceContent($filename);
      this._setAsUpdated();
    },

    _replaceContent: function(content) {
      var contentSelector = '.' + this.options.fileContentClass;

      $(contentSelector , this.$element).html(content);
    },

    _resetFileChooser: function() {
      var $clone = this.$fileChooser.clone(true);

      this.$fileChooser.replaceWith($clone);
      this.$fileChooser = $clone;
    },

    _showInProgress: function(isShown) {
      // show/hide in-progress element
    },

    _setAsUpdated: function() {
      if (!this.$element.hasClass( this.options.updateBtnClass )) {
        this.$element.addClass( this.options.updateBtnClass );
      }
    }
  };

  $.fn[pluginName] = function(options) {
    return this.each(function() {
      if (!$.data(this, pluginName)) {
        $.data(this, pluginName, new Plugin(this, options));
      }
    });
  };

  $.fn[pluginName].defaults = {
    displayType: 'filename',
    addBtnClass: 'btn-upload-file',
    updateBtnClass: 'btn-update-file',
    fileContentClass: 'file-content',
    fileUploadFormId: 'file-upload-form',
    contentTypeInputId: 'document_uploader_content_type',
    maxFileSize: 51200
  };

})(jQuery, window);
