(function($) {

  function removeErrors($element) {
    var input = $element.closest('.input');
    input.removeClass('field_with_errors');

     // Remove standard, nested error elements
    input.find('.error').addClass('invisible');
    // Remove non-nested error elements that have the remove-error-for property set
    var new_target = input.find('[data-remove-error-for]').data('removeErrorFor');
    var select = $('[data-remove-error-for=' + new_target + ']').first();
    var errored = select.closest('.field_with_errors');
    errored.removeClass('field_with_errors');
    $('[data-error-for=' + input.find('[data-remove-error-for]').data('removeErrorFor') + ']').addClass('invisible');
  }

  function handleRemoveErrorEvent(e) {
    removeErrors($(e.currentTarget));
  }

  $.fn.removeErrors = removeErrors;

  $.fn.errorRemover = function () {
    var $el = $(this);

    $el.find('input').on('focus', handleRemoveErrorEvent);
    $el.find('input[type="checkbox"], input[type="radio"]').on('change', handleRemoveErrorEvent);
    $el.find('select').on('focus', handleRemoveErrorEvent);
    $el.find('textarea').on('focus', handleRemoveErrorEvent);

    return this;
  };

})(jQuery);
