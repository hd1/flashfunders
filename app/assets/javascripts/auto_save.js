(function ($) {

  function AutoSaver(el) {
    this.el = el;
    this.$el = $(el);
    this.url = this.$el.attr('action');

    this.bindEvents();
  }

  _.extend(AutoSaver.prototype, {
    bindEvents: function () {
      this.$el.find('input[type="text"], input[type="email"], input[type="url"], input[type="number"], textarea').blur($.proxy(this._saveFields, this));
      this.$el.find('select').blur($.proxy(this._saveFields, this));
      this.$el.find('input[type="checkbox"]').change($.proxy(this._saveFields, this));
    },

    _saveFields: function () {
      $.ajax(this.url, {
          data: this.$el.serialize(),
          type: 'POST',
          dataType: 'json'
      });
    }
  });

  $.fn.autoSave = function () {
    $(this).each(function () {
      new AutoSaver(this);
    });
    return this;
  };


})(jQuery);