/* Here we are latching on an event ... in this case, the dialog open event */

CKEDITOR.on('dialogDefinition', function(ev) {
    try {
      var dialogName = ev.data.name;
      /* Gets the contents of the opened dialog */
      var dialogDefinition = ev.data.definition;
      /* Make sure that the dialog opened is the link plugin, otherwise do nothing */
      if(dialogName === 'link') {
        /* Getting the contents of the Target tab */
        var informationTab = dialogDefinition.getContents('target');

        /* Getting the contents of the dropdown field "Target" so we can set it */
        var targetField = informationTab.get('linkTargetType');

        /* Now that we have the field, we just set the default to _blank
        A good modification would be to check the value of the URL field
        and if the field does not start with "mailto:" or a relative path,
        then set the value to "_blank" */

        targetField['default'] = '_blank';
      }

    } catch(exception) {
      alert('Error ' + ev.message);
    }
});
