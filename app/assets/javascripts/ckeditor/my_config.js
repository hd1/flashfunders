if (typeof(CKEDITOR) != 'undefined') {
  CKEDITOR.editorConfig = function (config) {
    config.uiColor = "#AAAAAA";
    config.extraPlugins = 'divarea,sourcedialog';
    config.toolbar = [
      ['Bold', 'Italic', 'Underline'],
      ['NumberedList', 'BulletedList', 'Format', 'HorizontalRule'],
      ['Link', 'Unlink'],
      ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
      ['Undo', 'Redo'],
      ['Sourcedialog']
    ];
  };
} else {
  console.log("ckeditor not loaded");
}
