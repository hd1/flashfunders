$(function() {
  var $pressMenus = $('.press-nav ul li');
  var pressDelay = 4000, pressTimeoutId;

  function handle_caret() {
    if ($('.user-menu .nav-offering:first').hasClass('current-offering')) {
      $('ul.user-menu').addClass('first-menu');
    } else {
      $('ul.user-menu').removeClass('first-menu');
    }
  }

  $('.nav-offering').on('click', function() {
    $('.user-menu li').removeClass('current-offering');
    $(this).addClass('current-offering');
    handle_caret();
  });

  $pressMenus.unbind('click');
  $pressMenus.on('click', function(e) {
    e.preventDefault();

    if (e.originalEvent.isTrusted) {
      clearTimeout(pressTimeoutId);
    }

    $('a', this).tab('show');
  });

  function rotatePress(delay) {
    pressTimeoutId = setTimeout(function() {
      var currentIndex = $pressMenus.index($($pressMenus.selector + '.active'));

      $pressMenus[++currentIndex % $pressMenus.length].click();
      rotatePress(delay);
    }, delay);
  }

  handle_caret();
  rotatePress(pressDelay);
});
