(function (exports) {
  var VideoPlayer = Backbone.View.extend({
    el: '#video-player-container',
    template: JST['video_player/templates/video_iframe'],

    events: {
      'click .play-button': '_replacePhotoWithVideo',
    },

    _replacePhotoWithVideo: function () {
      this.$el.find('#cover-media').html(
        this.template(this.options)
      );
    }
  });

  exports.FF.VideoPlayer = VideoPlayer;

})(window);