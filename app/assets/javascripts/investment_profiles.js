$(function () {
  var $topDivider = $('.workspace-header-divider');

  function getForm() {
    var $activeFormSection = $('#non-existent-id');
    if ($('#individual.active').length > 0) {
      $activeFormSection = $('#individual');
    } else if ($('#entity.active').length > 0) {
      if($('#entity-new').hasClass('active') > 0) {
        $activeFormSection = $('#entity-new .active');
      } else {
        $activeFormSection = $('#entity .tab-pane.active div.active').first();
      }
    }
    return $activeFormSection.find('form');
  }

  function clearTopError() {
    $topDivider.removeClass('errors');
    $('#errors').remove();
  }

  function addTopError(msg) {
    clearTopError();
    $topDivider.addClass('errors').append('<div id="errors" ><p>' + msg + '</p></div>');
  }

  function displayErrors() {
    if($('.selections li.active').length == 0) {
      addTopError('Please select one of the methods below to continue');
    } else {
      addTopError('Please select an entity below to continue');
    }
  }

  function submitForm() {
    var $form = getForm();
    if($form.length > 0) {
      $form.submit();
    } else {
      displayErrors();
    }
  }

  $('#submit-button').click( function(e){
    e.preventDefault();

    submitForm();
  });
});
