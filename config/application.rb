require File.expand_path('../boot', __FILE__)

# Pick the frameworks you want:
require "active_record/railtie"
require "action_controller/railtie"
require "action_mailer/railtie"
require "sprockets/railtie"
# require "rails/test_unit/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(:default, Rails.env)

module Flashfunders
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'
    config.active_record.raise_in_transactional_callbacks = true

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    config.i18n.load_path += Dir[Rails.root.join('config', 'locales', 'emails', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de

    config.time_zone = 'Pacific Time (US & Canada)'
    config.active_record.default_timezone = :local

    config.autoload_paths += Dir[Rails.root.join('lib')]
    config.active_record.schema_format = :sql

    config.i18n.enforce_available_locales = true

    config.require_email_confirmation = true

    config.exceptions_app = self.routes

    config.force_ssl = true

    config.facebook_app_id = ENV.fetch('FB_APP_ID')

    config.cache_store = :redis_store, ENV.fetch('REDISTOGO_URL'), { expires_in: 5.minutes, raise_errors: false }

    config.encryption = ActiveSupport::OrderedOptions.new.tap do |options|
      options.ssn_key = ENV.fetch('SSN_ENCRYPT_KEY')
      options.spouse_ssn_key = ENV.fetch('SPOUSE_SSN_ENCRYPT_KEY')
      options.bank_account_key = ENV.fetch('BANK_ACCOUNT_ENCRYPT_KEY')
      options.bank_account_holder_key = ENV.fetch('BANK_ACCOUNT_HOLDER_ENCRYPT_KEY')
    end

    require 'multipart_sanitizer'
    config.middleware.insert_before Rack::Runtime, MultipartSanitizer
  end
end
