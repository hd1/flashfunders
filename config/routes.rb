Flashfunders::Application.routes.draw do
  concern :showable_new do
    get :show, on: :member, action: :new
  end

  concern :showable_edit do
    get :show, on: :member, action: :edit
  end

  root to: 'home#show'

  get 'converted_show', to: 'home#converted_show'

  devise_for :users, controllers: { sessions: 'sessions', registrations: 'registrations', confirmations: 'confirmations', passwords: 'passwords', omniauth_callbacks: 'omniauth' }

  devise_scope :user do
    put '/users/confirm', to: 'confirmations#confirm', as: :user_confirm
    get '/users/log_me_out', to: 'sessions#log_me_out', as: :user_log_me_out
    get '/users/logout', to: 'sessions#logout', as: :user_logout

    namespace :api, defaults: { format: :json } do
      get '/users/session_info', to: '/sessions#session_info', as: :user_session_info
    end
  end

  get 'user/email_consent', to: 'email_consent#new'
  post 'user/email_consent', to: 'email_consent#create'
  get 'sign/:investment_uuid', to: 'docusign_bypass#sign'
  get 'issuer_sign/:investment_uuid', to: 'docusign_bypass#issuer_sign'
  get 'sign_success', to: 'docusign_bypass#success', as: :sign_success
  get 'sign_redirect', to: 'docusign_bypass#redirect', as: :sign_redirect
  get 'investment_agreement/:investment_uuid', to: 'docusign_bypass#agreement', as: :investment_agreement

  get '/impersonate/user/:user_id', to: 'impersonations#create', as: :impersonate
  delete '/impersonate/user/revert', to: 'impersonations#destroy', as: :revert_impersonate

  resource :issuer_applications, only: [:new, :create], concerns: [:showable_new] do
    get 'success'
    get 'recreate'
  end

  resource :dashboard, only: :show do
    resources :investments, only: [:index], controller: 'investments' do
      member do
        get :download_docs
        put :cancel_investment
      end
    end
    resources :conversations, only: [:index, :show, :create], controller: 'messages'
  end

  resource :subscriptions, only: [:create]
  get 'subscribe', to: 'subscriptions#new', as: :subscribe
  post 'dismiss', to: 'subscriptions#dismiss', as: :dismiss

  namespace :issuer do
    namespace :editor do
      resources :details, only: [:edit, :update], concerns: [:showable_edit]
      resources :pitches, only: [:edit, :update], concerns: [:showable_edit]
      resources :questions, only: [:edit, :update], concerns: [:showable_edit]
      resources :documents, only: [:edit, :update], concerns: [:showable_edit]
      resources :stakeholders, only: [:edit, :update], concerns: [:showable_edit]
    end
    resources :dashboard, controller: :dashboard, only: :show do
      get 'active_investments'
      get 'investment_activity', action: 'export_csv'
    end
    get 'change_context', controller: 'issuer'
  end

  resources :offering, controller: :offerings, only: :show do
    resource :investment_flow, controller: 'investment_flow/investments', except: [:destroy, :show] do
      resource :fund, controller: 'investment_flow/funds', only: [:new, :create, :edit, :update], concerns: [:showable_new] do
        get '/complete', action: 'complete'
      end
    end

    resource :investment, controller: 'invest/investments', except: [:destroy, :show] do
      get 'recreate'
      post '/update_payment', action: 'update_payment'
      post '/resend_verification', action: 'resend_verification'
    end
    namespace :invest do
      namespace :accreditation do
        resource :new, controller: :new, only: [:new, :create, :edit, :update], concerns: [:showable_new]
        resource :verified, controller: :verified, only: [:new, :create], concerns: [:showable_new]
      end
      resource :investment_profile, only: [:new, :create, :edit], concerns: [:showable_new]
      resource :investment_documents, only: [:show] do
        post '/accept', action: 'accept'
        get '/docusign_handler', action: 'docusign_handler'
        get '/docusign_redirect', action: 'docusign_redirect'
      end

      resource :fund, only: [:new, :create, :edit, :update], concerns: [:showable_new] do
        get '/complete', action: 'complete'
      end

    end

    resources :messages, controller: :offering_messages, only: [:create]
    resources :comment_forms, controller: :offering_comment_forms, only: [:create, :destroy]
    resource :follower, controller: :offering_followers, only: [:update, :destroy] do
      get 'follow', as: :redirect
    end
  end

  resources :third_party_qualifications, only: [:edit, :update], concerns: [:showable_edit] do
    get :decline, on: :member
  end

  resource :browse, only: :show
  resources :contact_forms, only: :create

  get '/terms', to: 'static#tos', as: :terms
  get '/terms_of_use', to: 'static#tos', as: nil
  get '/privacy', to: 'static#privacy', as: :privacy
  get '/privacy_policy', to: 'static#privacy', as: nil
  get '/email_disclaimer', to: 'static#email_disclaimer'
  get '/email_disclaimer_to_investor', to: 'static#email_disclaimer_to_investor'
  get '/email_disclaimer_to_issuer', to: 'static#email_disclaimer_to_issuer'
  get '/cip_notice', to: 'static#cip_notice'
  get '/unsubscribe', to: 'static#unsubscribe', as: :unsubscribe
  get '/contact', to: 'static#contact', as: :contact
  get '/get_started', to: 'static#get_started', as: :get_started
  get '/faq', to: 'static#faq_investor', as: :faq
  get '/team', to: 'static#team', as: :team
  get '/press', to: 'static#press', as: :press
  get '/electronic_consent', to: 'static#econsent', as: :econsent
  get '/sitemap', to: 'static#sitemap', as: :site_map

  if Rails.env.production?
    get '/blog', to: redirect('http://blog.flashfunders.com'), as: :blog
  else
    get '/blog', to: redirect('http://blog.flashdough.com'), as: :blog
  end
  get '/embedded_footer', to: 'iframeable#footer'
  get '/embedded_header', to: 'iframeable#header'

  get '/flashfundersinsider', to: redirect("#{ENV['HUBSPOT_LANDING_DOMAIN']}/flashfundersinsider"), as: :digital_marketing_landing_page

  get '/what_is_flashfunders', to: 'static#investors', as: :what_is_flashfunders

  get '/investors', to: 'static#investors', as: :investors
  get '/investor_hub', to: 'static#investors', as: :investor_hub
  get '/investor_faq', to: 'static#faq_investor', as: :investor_faq
  get '/faq/investor', to: 'static#faq_investor', as: :faq_investor

  get '/startups', to: 'static#startups', as: :startups
  get '/startup_hub', to: 'static#startups', as: :startup_hub
  get '/startup_faq', to: 'static#faq_startup', as: :startup_faq
  get '/faq/startup', to: 'static#faq_startup', as: :faq_startup

  # obsoleted paths redirection
  get '/how_to_invest', to: redirect('faq/investor#how_to_invest_flow')
  get '/raise_capital', to: redirect('startups')
  get '/documents/Investors', to: redirect('faq/investor#general')
  get '/startup_requirements', to: redirect('faq/startup#general-what-are-the-requirements-to-raise-capital-on-flashfunders')
  get '/investor_terms', to: redirect('faq/investor#investor_requirements_and_rights')
  get '/investor_benefits', to: redirect('faq/investor#benefits_risks')
  get '/startup_terms', to: redirect('faq/startup#fundraising_details')
  get '/investor_how_to_invest', to: redirect('faq/investor#how_to_invest_flow')

  namespace :api, defaults: { format: :json } do
    resources :offerings, only: :show

    namespace :issuer do
      resources :offerings, only: :index
      namespace :analytics do
        get '/get_data', to: '/issuer/analytics#get_data'
      end
    end

    get '/regions', to: '/static#regions', as: :regions
    get '/lookup_routing_number', to: '/static#lookup_routing_number', as: :lookup_routing_number
  end

  get '/robots.txt', to: 'home#robots'

  unless Rails.env.production?
    get '/style_guide', to: 'static#style_guide', as: :style_guide
    get '/honeybadger', to: 'honeybadger#blow_up'
  end

  get '/404', to: 'errors#not_found'
  get '/500', to: 'errors#internal_error'

  # This is a catch-all route - leave it at the bottom of the routes file so it
  # doesn't clobber any legitimate routes
  get '*path', to: 'offerings#vanity_show', constraints: OfferingsVanityConstraints.new, as: 'offering_vanity'
end
