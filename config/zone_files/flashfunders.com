$ORIGIN flashfunders.com.
@ 600 AWS ALIAS Z2FDTNDATAQYW2 d5uf17hsmekwm.cloudfront.net.
@ 3600 IN MX 10 aspmx2.googlemail.com.
@ 3600 IN MX 5 alt2.aspmx.l.google.com.
@ 3600 IN MX 5 alt1.aspmx.l.google.com.
@ 3600 IN MX 10 aspmx3.googlemail.com.
@ 3600 IN MX 1 aspmx.l.google.com.
@ 172800 IN NS ns-1362.awsdns-42.org.
@ 172800 IN NS ns-446.awsdns-55.com.
@ 172800 IN NS ns-558.awsdns-05.net.
@ 172800 IN NS ns-1756.awsdns-27.co.uk.
@ 900 IN SOA ns-1362.awsdns-42.org. awsdns-hostmaster.amazon.com. 1 7200 900 1209600 86400
@ 3600 IN TXT "v=spf1" "include:_spf.google.com" "include:mail.zendesk.com" "include:_spf.zdsys.com" "sendgrid.net" "ip4:208.81.212.0/22" "~all"
@ 3600 IN TXT "ALIAS" "for" "akita-2718.herokussl.com"
google._domainkey 3600 IN TXT "v=DKIM1"
smtpapi._domainkey 3600 IN TXT "k=rsa"
_jabber._tcp 3600 IN SRV 20 0 5269 xmpp-server2.l.google.com.
_jabber._tcp 3600 IN SRV 20 0 5269 xmpp-server3.l.google.com.
_jabber._tcp 3600 IN SRV 5 0 5269 xmpp-server.l.google.com.
_jabber._tcp 3600 IN SRV 20 0 5269 xmpp-server1.l.google.com.
_jabber._tcp 3600 IN SRV 20 0 5269 xmpp-server4.l.google.com.
assets 3600 IN CNAME d3h9mfivrbq3mr.cloudfront.net.
assets0 3600 IN CNAME d3h9mfivrbq3mr.cloudfront.net.
assets1 3600 IN CNAME d3h9mfivrbq3mr.cloudfront.net.
assets2 3600 IN CNAME d3h9mfivrbq3mr.cloudfront.net.
assets3 3600 IN CNAME d3h9mfivrbq3mr.cloudfront.net.
blog 3600 IN CNAME 1646512.group12.sites.hubspot.net.
bradmin 3600 IN CNAME iwate-6765.herokussl.com.
calendar 3600 IN CNAME ghs.googlehosted.com.
docs 3600 IN CNAME ghs.googlehosted.com.
email 3600 IN CNAME sendgrid.net.
smtpapi._domainkey.email 3600 IN TXT "k=rsa"
o1.email 3600 IN A 167.89.14.66
mail 3600 IN CNAME ghs.googlehosted.com.
sites 3600 IN CNAME ghs.googlehosted.com.
www 3600 IN CNAME akita-2718.herokussl.com.
