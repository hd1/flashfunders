# This calculator was used to make the config vars referenced below:
# http://manuel.manuelles.nl/sidekiq-heroku-redis-calc/
require 'sidekiq'

if Rails.env.development?
  require 'sidekiq/testing'
  Sidekiq::Testing.inline!
end

class SidekiqErrorHandler
  def call(worker_class, job, queue, redis_pool)
    # fallback to perform synchronoushly when redis is down.
    # This happens before pushing job to redis.
    begin
      Sidekiq.redis {|conn| conn.ping}
      yield
    rescue
      worker_class.new.perform(*(job['args']))
      false
    end
  end
end

Sidekiq.configure_client do |config|
  config.redis = { :size => 1 }
  config.client_middleware { |chain| chain.add SidekiqErrorHandler } unless Rails.env.test?
end

Sidekiq.configure_server do |config|
  config.redis = { :size => 46 }
end
