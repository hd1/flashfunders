# Be sure to restart your server when you modify this file.

# Configure sensitive parameters which will be filtered from the log file.
if Rails.env.production? || Rails.env.review?
  Rails.application.config.filter_parameters += [:address1, :address2, :address_1,
    :address_2, :amount, :answer, :banc_box_escrow_id, :bank_account_holder,
    :bank_account_type, :bank_account_routing, :bank_account_number, :bio, :body,
    :check_bank_name, :bank_name, :check_payable_to, :city, :company_name, :confirmation_token,
    :corporate_name, :date_of_birth, :daytime_phone, :description, :ein, :email,
    :employment_data, :password, :ssn, :envelope_email, :escrow_data, :experience,
    :external_investor_count, :first_name, :form_path, :full_name, :id_number,
    :id_type, :income, :issued_date, :last_name, :liquid_net_worth, :location,
    :message, :middle_initial, :mobile_phone, :mode, :name, :net_worth, :option_pool,
    :passport_image, :percent_ownership, :phone_number, :pledged, :position,
    :question, :registration_name, :reset_password_token, :routing_number, :state,
    :tax_id_number, :understand_risk, :wire_beneficiary_name, :wire_instructions,
    :zip_code]
end
