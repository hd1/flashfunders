Accreditor.user_class = 'User'
Accreditor.entity_class = 'InvestorEntity'
Accreditor.document_class = 'Accreditation::Document'
Accreditor.document_foreign_key = 'accreditation_qualification_id'
