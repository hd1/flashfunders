require 'fintech_clearing/fintech_clearing'

FintechClearing::Config.configure do |config|
  config.api_url = ENV['FINTECH_CLEARING_API_URL']
  config.api_keys = {
    default: ENV['FINTECH_CLEARING_API_KEY'],
  }
  config.basic_auth = {
    email: ENV['FINTECH_CLEARING_USERNAME'],
    password: ENV['FINTECH_CLEARING_PASSWORD']
  }

  if Rails.env.test?
    config.api_url = 'https://api.sandbox.fintechclearing.com/'
    config.api_keys[:default] ||= 'FAKE-FINTECH-CLEARING-API-KEY'

    config.basic_auth[:email] = 'fifth@ff.com'
    config.basic_auth[:password] = 'password'
  end

  config.logger = Rails.logger
end
