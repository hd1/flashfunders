FundAmerica::Config.configure do |config|
  config.api_url = ENV['FUND_AMERICA_API_URL']
  config.api_keys = {
      default: ENV['FUND_AMERICA_API_KEY'],
      spv: ENV['FUND_AMERICA_SPV_API_KEY'],
      reg_c: ENV['FUND_AMERICA_CF_API_KEY']
  }

  if Rails.env.test?
    config.api_keys[:default] ||= 'FAKE-FUND-AMERICA-API-KEY'
    config.api_keys[:spv] ||= 'FAKE-FUND-AMERICA-SPV-API-KEY'
    config.api_keys[:reg_c] ||= 'FAKE-FUND-AMERICA-REG-C-API-KEY'
  end

  config.logger = Rails.logger
end
