uri = URI.parse ENV['REDISTOGO_URL']
if uri.scheme == 'redis'
  $redis = Redis.new(url: uri.to_s)
else
  $redis = Redis.new(host: uri.host, port: uri.port)
end
