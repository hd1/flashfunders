Time::DATE_FORMATS[:short_date] = Date::DATE_FORMATS[:short_date] = '%m/%d/%Y'
Time::DATE_FORMATS[:long_date] =  Date::DATE_FORMATS[:long_date] = '%B %e, %Y'
Time::DATE_FORMATS[:full_date] = Date::DATE_FORMATS[:full_date] = '%A, %B %e, %Y'
Time::DATE_FORMATS[:abbreviated_full_date] = Date::DATE_FORMATS[:abbreviated_full_date] = '%a, %b %-d %Y'
Time::DATE_FORMATS[:today_message] = '%l:%M%P'
Time::DATE_FORMATS[:message] = '%b %d at %l:%M%P'
Time::DATE_FORMATS[:short_date_time] = '%Y-%m-%d %T'
Time::DATE_FORMATS[:long_day_datetime] = '%a, %b %e, %Y %r %Z'

