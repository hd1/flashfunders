require 'socket'

IDology[:username]=ENV['IDOLOGY_USERNAME']
IDology[:password]=ENV['IDOLOGY_PASSWORD']

unless Rails.env.test?
  begin
    proximo_uri = URI.parse(ENV['PROXIMO_URL'])
    ip = IPSocket::getaddress(proximo_uri.host)

    IDology::Subject.http_proxy ip, 80, proximo_uri.user, proximo_uri.password
  rescue URI::InvalidURIError
    puts 'No proxy used for IDology requests'
  end
end
