CarrierWave.configure do |config|
  if Rails.env.test? || Rails.env.development?
    config.storage = :file
    config.fog_public = false
  else
    config.storage = :fog
    config.fog_public = true
  end

  config.max_file_size = 50.megabytes

  if Rails.env.test?
    config.fog_credentials = {
      :provider               => 'AWS',
      :aws_access_key_id      => 'fake key',
      :aws_secret_access_key  => 'fake secret key',
      :region                 => 'us-west-1',
    }
    config.fog_directory  = "flashfunders-test"
  else
    config.fog_credentials = {
      :provider               => 'AWS',
      :aws_access_key_id      => ENV['AWS_ACCESS_KEY_ID'],
      :aws_secret_access_key  => ENV['AWS_SECRET_ACCESS_KEY'],
      :region                 => ENV['AWS_REGION'],
    }
    config.fog_directory  = ENV['FOG_DIRECTORY']
  end

  config.fog_attributes = {'x-amz-server-side-encryption' => 'AES256'}
  config.validate_unique_filename = false
  config.use_action_status = true
  config.upload_expiration = 1.week

  config.asset_host = proc do |file|
    host = ActionController::Base.asset_host

    if host =~ /%d/
      host % (Zlib.crc32(file.path) % 4)
    else
      host
    end
  end
end

