Hubspot.configure({ hapikey: ENV['HUBSPOT_API_KEY'],
                    portal_id: ENV['HUBSPOT_KEY'] })
Flashfunders::Application.config.deal_owner = ENV['HUBSPOT_DEAL_OWNER']
