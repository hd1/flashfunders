class EmailSubjectLine
  def self.delivering_email(mail)
    prefix_email_subject mail
  end

  def self.previewing_email(mail)
    prefix_email_subject mail 
  end

  private

  def self.prefix_email_subject(mail)
    mail.subject = "[#{Rails.env.upcase}] #{mail.subject}" unless Rails.env.production?
  end
end

ActionMailer::Base.register_interceptor(EmailSubjectLine)
ActionMailer::Base.register_preview_interceptor(EmailSubjectLine)
