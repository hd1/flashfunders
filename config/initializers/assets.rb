Flashfunders::Application.config.assets.precompile << /\.(?:svg|eot|woff|ttf)\z/
Flashfunders::Application.config.assets.precompile += %w( ckeditor/my_config.js ckeditor/my_styles.js vendor/ckeditor/my_contents.scss)
Flashfunders::Application.config.assets.precompile += %w(iframe/if_footer.scss iframe/if_header.scss iframe/if_header.js)
