if defined?(MailSafe::Config)
  if Rails.env == 'production'
    msg = 'MailSafe must not be active in Production'
    Rails.logger.fatal msg
    raise msg
  end
  MailSafe::Config.internal_address_definition = /.*@flashfunders\.com/i
  MailSafe::Config.replacement_address = 'engineering+mail_safe@flashfunders.com'
end
