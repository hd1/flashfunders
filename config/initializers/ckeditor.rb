Ckeditor.setup do |config|
  config.assets_languages = ['en']
  config.assets_plugins = ['dialog', 'link', 'magicline']
end
