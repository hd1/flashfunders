def log_in_with_credentials(email, password)
  visit(new_user_session_path)

  fill_in 'user_email', with: email
  fill_in 'user_password', with: password

  click_button I18n.t('sign_in.form_section.commit')
end
