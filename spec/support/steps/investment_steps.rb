def fill_in_investment_amount(amount)
  visit new_offering_investment_path(offering)

  fill_in_throttled_keyup_input \
    I18n.translate('investor_investments.form_section.investment_amount.label'), with: amount
end

def submit_investment(amount)
  fill_in_investment_amount(amount)

  click_on (I18n.translate('investor_investments.form_section.commit'))

  sleep(1) # due to a delay in the form submission
end

def invest_as_individual(skip_submit=false)
  click_on I18n.t('investment_profile.new.individual.title')

  fill_in_individual_entity('Colorado', true, false, skip_submit)
end

def fill_in_individual_entity(region, us_citizen, reg_c=false, skip_submit=false)
  fill_in I18n.translate('investor_information.form_section.fieldset_1.first_name.label'), with: 'Jone'
  fill_in I18n.translate('investor_information.form_section.fieldset_1.middle_initial.label'), with: 'T'
  fill_in I18n.translate('investor_information.form_section.fieldset_1.last_name.label'), with: 'Denver'
  fill_in I18n.translate('investor_information.form_section.fieldset_1.date_of_birth.label', popover: '').strip, with: '01/05/1955'
  fill_in I18n.translate('investor_information.form_section.fieldset_1.social_security_number.label', popover: '').strip, with: '555.55-5555'

  within('.location-information') do
    fill_in I18n.translate('investor_information.form_section.fieldset_2.address_1.label'), with: '123 Street'
    fill_in I18n.translate('investor_information.form_section.fieldset_2.address_2.label'), with: 'Apt 204'
    fill_in I18n.translate('investor_information.form_section.fieldset_2.city.label'), with: 'San One'

    select region

    fill_in I18n.translate('investor_information.form_section.fieldset_2.zip_code.label'), with: '12345'
    fill_in I18n.translate('investor_information.form_section.fieldset_2.phone.daytime_label'), with: '123.555-5555'
  end

  select 'Passport'
  fill_in I18n.translate('investor_information.form_section.fieldset_3.id_number.label'), with: '123'
  fill_in I18n.translate('investor_information.form_section.fieldset_3.location.label'), with: 'Denver'
  fill_in I18n.translate('investor_information.form_section.fieldset_3.issued_date.label'), with: '01/05/1955'
  fill_in I18n.translate('investor_information.form_section.fieldset_3.expiration_date.label'), with: '01/01/2020'
  check_radio_button('individual_form[signatory_us_citizen]', us_citizen)

  within('.employment-information') do
    select 'Employed'
    fill_in I18n.t('investor_information.form_section.fieldset_4.employment_role'), with: 'Boss'
    fill_in I18n.t('investor_information.form_section.fieldset_4.employer_name'), with: 'F'
    check_radio_button('individual_form[signatory_employer_industry]', true)
  end

  agree_to_investment_experience(reg_c)

  click_on I18n.t('investment_profile.new.buttons.submit') unless skip_submit

end

def agree_to_investment_experience(reg_c=false)
  kind = reg_c ? 'reg_c' : 'reg_d'
  within('.suitability-information') do
    check I18n.t("investment_profile.new.suitability.fieldset_1.#{kind}.high_risk_agree")
    check I18n.t("investment_profile.new.suitability.fieldset_1.#{kind}.experience_agree")
    check I18n.t("investment_profile.new.suitability.fieldset_1.#{kind}.loss_risk_agree")
    check I18n.t("investment_profile.new.suitability.fieldset_1.#{kind}.time_horizon_agree")
    if reg_c
      check I18n.t('investment_profile.new.suitability.fieldset_1.reg_c.cancel_agree')
      check I18n.t('investment_profile.new.suitability.fieldset_1.reg_c.responsible_agree')
    end
  end
end

def submit_third_party_net_worth(role, email)
  click_on I18n.t('investor_accreditation.new.verify_net_worth.name')
  page.find('a[href="#net-worth-third-party"] .title').click

  select_from_dropdown role, from: 'third_party_net_worth_form_role'
  fill_in I18n.t('investor_accreditation.new.verify_net_worth.third_party_verification.form.fieldset.email'), with: email

  click_on I18n.t('investor_accreditation.buttons.submit')
  expect(page).to have_case_insensitive_content(I18n.t('investment_documents.title'))
end

def agree_to_and_sign_documents
  # click_on I18n.t('investment_documents.buttons.commit')
  #
  # HACK: Instead of continuing to next page, we want to
  # avoid the Docusign iframe/API. Manually pushing the
  # state forward instead and simulating the continuation
  # to the next step.
  Investment.first.tap do |investment|
    investment.sign_documents!
  end

  visit dashboard_investments_path(offering.id)
  click_on I18n.t('investor_investments.invest.resume')
end

def fill_in_link_refund_account
  fill_in_refund_account(I18n.t('investor_transfer_investments.link_refund_account.routing_number_label'), 'Some dude', '1234', '999999999', 'Checking')
end

def fill_in_wire_refund_account(name, routing, account, type)
  fill_in_refund_account(I18n.t('link_refund_account.routing_number_wire_label'), name, routing, account, type)
end

def fill_in_ach_refund_account(name, routing, account, type)
  fill_in_refund_account(I18n.t('link_refund_account.routing_number_ach_label'), name, routing, account, type)
  check I18n.t('fund_flow.authorization.amount', amount: '')
end

def fill_in_refund_account(routing_label, name, routing, account, type)
  within('#link-refund-account-form') do
    fill_in I18n.t('investor_transfer_investments.link_refund_account.name_on_account_label'), with: name
    fill_in routing_label, with: routing
    fill_in I18n.t('investor_transfer_investments.link_refund_account.account_number_label'), with: account
    select type, from: I18n.t('investor_transfer_investments.link_refund_account.account_type_label')
  end
end

def choose_to_send_check
  click_on I18n.t('fund.check.title')
  fill_in_link_refund_account
  click_button I18n.t('fund.buttons.commit')
end
