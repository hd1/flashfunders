module ObjectCreationMethods
  include CarrierWaveDirect::Test::Helpers

  def new_user(overrides={})
    defaults = {
      registration_name: 'John Username',
      email: 'user@example.com',
      password: 'thepassword',
      password_confirmation: 'thepassword',
      agreed_to_tos: true,
      email_consent_at: 1.day.ago,
      user_type: :user_type_investor
    }

    User.new do |user|
      if overrides.has_key?(:us_citizen) || overrides.has_key?(:country)
        user.personal_federal_identification_information = PersonalFederalIdentificationInformation.new
      end
      apply(user, defaults, overrides)
    end
  end

  def create_user(overrides={})
    new_user(overrides).tap do |user|
      user.save!
      user.confirm!
    end
  end

  def create_temporary_user(overrides={})
    new_user(overrides).tap do |user|
      user.encrypted_password=''
      user.save!
    end
  end

  def create_unconfirmed_user(overrides={})
    new_user(overrides).tap do |user|
      user.save!
    end
  end

  def pitch_html
    <<-HTML
<section>
  <h1>The top threats:</h1>
  <ol>
    <li>Dogs</li>
    <li>Yarn</li>
    <li>CatNip</li>
  </ol>
</section>
    HTML
  end

  def create_complete_closed_campaign(email = 'user2@example.com')
    user = create_user(email: email, password: 'thepassword', password_confirmation: 'thepassword')

    offering = OfferingCreator.new.create(user_id: user.id)
    pitch_section = Pitch::Section.create!(offering_id: offering.id, title: 'Title')
    pitch_section.text_areas.create!(html: true, body: pitch_html)
    company = offering.company

    offering.update_attributes!(
      visibility: Offering.visibilities[:visibility_public],
      rank: 1,
      elevator_pitch: 'Do not ride the elevator in an earthquake.',
      tagline: 'We will make you shake',
      ends_at: Time.zone.parse('2013-12-12'),
      issuer_user_id: user.id
    )

    create_campaign_document(name: 'Executive Summary', documentable: offering)
    create_deal_document(name: 'Term Sheet', documentable: offering.reg_d_direct_security)

    company.update_attributes!(
      name: 'Earthquakes R Us',
      website_url: 'http://earthquakes.example.com/shake',
      twitter_url: 'twitter.com/earthquakes',
      facebook_url: 'facebook.com/earthquakes',
      location: 'Irvine, CA'
    )

    offering.update_attributes!(
      pre_money_valuation: 3000000,
      fully_diluted_shares: 1000,
    )
    offering.securities.first.update_attributes(
        type: 'Securities::FspStock',
        minimum_total_investment: 2000000,
        minimum_investment_amount: 3000,
        maximum_total_investment: 40000000,
        maximum_total_investment_display: '$40 million',
        shares_offered: 500,
    )

    offering
  end

  def create_complete_open_campaign(email = 'user@example.com')
    user = create_user(email: email, password: 'thepassword', password_confirmation: 'thepassword')

    offering = OfferingCreator.new.create(user_id: user.id)
    pitch_section = Pitch::Section.create!(offering_id: offering.id, title: 'The top threats: Dogs Yarn CatNip')
    pitch_section.text_areas.create!(html: true, body: pitch_html)
    company = offering.company

    offering.update_attributes!(
      visibility: Offering.visibilities[:visibility_public],
      rank: 1,
      elevator_pitch: 'Watch me ride this elevator, all the way to the top floor.',
      tagline: "It's the facebook of cosmetic surgery",
      ends_at: 45.days.from_now,
      issuer_user_id: user.id,
      vanity_path: "foobiz#{offering.id}"
    )

    create_campaign_document(name: 'Executive Summary', documentable: offering)

    create_deal_document(name: 'Term Sheet', documentable: offering.reg_d_direct_security)

    company.update_attributes!(
      name: 'FooBiz',
      website_url: 'http://www.subdomain.example.com/foobiz',
      twitter_url: 'twitter.com/foobiz',
      facebook_url: 'facebook.com/foobizinc',
      location: 'Santa Monica, CA'
    )

    offering.update_attributes!(
      pre_money_valuation: 50000000,
      pre_money_valuation_display: '$50 million',
      fully_diluted_shares: 100,
      share_price: 100,
      investable: true
    )

    offering.securities.first.update_attributes(
        type: 'Securities::FspStock',
        minimum_total_investment: 10000000,
        minimum_total_investment_display: '$10 million',
        minimum_investment_amount: 5000,
        minimum_investment_amount_display: '$5 thousand',
        maximum_total_investment: 40000000,
        maximum_total_investment_display: '$40 million',
        shares_offered: 50,
    )
    offering
  end

  def new_company(overrides={})
    defaults = {
      offering_id: -> { create_offering.id }
    }

    Company.new { |company| apply(company, defaults, overrides) }
  end

  def create_company(overrides={})
    new_company(overrides).tap(&:save!)
  end

  def new_offering(overrides={})
    defaults = {
      minimum_total_investment: 200000
    }

    Offering.new { |offering| apply(offering, defaults, overrides) }
  end

  def create_offering(overrides={})
    new_offering(overrides).tap(&:save!)
  end

  def new_campaign_document(overrides={})
    defaults = {
      name: 'Cool Document',
      key: sample_key(DocumentUploader.new),
      documentable: Securities::Security.create,
    }

    CampaignDocument.new { |campaign_document| apply(campaign_document, defaults, overrides) }
  end

  def create_campaign_document(overrides={})
    new_campaign_document(overrides).tap(&:save!)
  end

  def new_investment(overrides={})
    Investment.new { |investment| apply(investment, {}, overrides) }
  end

  def create_investment(overrides={})
    new_investment(overrides).tap do |investment|
      investment.save
    end
  end

  def new_corporate_federal_identification_information(overrides = {})
    defaults = {
      corporate_name: 'Corp Inc, LLC. DBA McDonalds',
      ein: '123456789',
      state_id: State.find_by_name('South Dakota').code,
      company_id: -> { create_company.id }
    }

    CorporateFederalIdentificationInformation.new { |info| apply(info, defaults, overrides) }
  end

  def create_corporate_federal_identification_information(overrides={})
    new_corporate_federal_identification_information(overrides).tap(&:save!)
  end

  def new_deal_document(overrides = {})
    defaults = {
      name: 'Cool Document',
      description: 'Cool Description',
      key: sample_key(DocumentUploader.new),
      documentable: Securities::Security.create,
    }

    DealDocument.new { |deal_doc| apply(deal_doc, defaults, overrides) }
  end

  def create_deal_document(overrides = {})
    new_deal_document(overrides).tap(&:save!)
  end

  def apply(object, defaults, overrides)
    options = defaults.merge(overrides)
    options.each do |method, value_or_proc|
      object.send("#{method}=", value_or_proc.is_a?(Proc) ? value_or_proc.call : value_or_proc)
    end
  end
end
