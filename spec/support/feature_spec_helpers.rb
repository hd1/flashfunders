module FeatureSpecHelpers
  include Warden::Test::Helpers

  def sign_in_user(user)
    login_as(user, scope: :user)
  end

  def fill_in_throttled_keyup_input(locator, options={})
    with = options.delete(:with)
    delay = options.delete(:delay) || 0.5
    field = find(:fillable_field, locator, options)
    page.execute_script("$('input##{field[:id]}').val(#{with.to_json}).trigger('keyup');")
    sleep delay if delay > 0
  end

  def step(message)
    if block_given?
      yield
    else
      pending message
    end
  end

  def attach_file_to_hidden_field(locator, path)
    original_ignore_hidden_elements = Capybara.ignore_hidden_elements
    Capybara.ignore_hidden_elements = false

    hidden_file_field = page.find_field(locator)

    raise 'The hidden file input element must have an id' unless hidden_file_field['id']

    page.execute_script("$('##{hidden_file_field['id']}').removeClass('hidden')")
    page.attach_file(locator, path)
    page.execute_script("$('##{hidden_file_field['id']}').addClass('hidden')")
    Capybara.ignore_hidden_elements = original_ignore_hidden_elements
  end

  def run_escrow_updater
    EscrowUpdater.new.fetch_and_process_all_escrows
  end

  def dropdown_contents(id)
    page.evaluate_script("$(\"##{id} option:selected('selected')\").text()")
  end

  def select_from_dropdown(item_text, options)
    select item_text, options
  end

  def create_account(name, email, pass, modal=false, submit=true)
    fill_in 'user_registration_name', with: name, match: :first
    fill_in 'user_email', with: email, match: :first
    fill_in 'user_password', with: pass, match: :first
    fill_in 'user_password_confirmation', with: pass, match: :first

    if submit
      if modal
        click_on I18n.t('sign_up.form_section.commit')
      else
        within '.panel' do
          click_on I18n.t('sign_up.form_section.commit')
        end
      end
    end
  end

  def create_account_modal(name, email, pass, submit=true)
    create_account(name, email, pass, true, submit)
  end

  def submit_email_consent
    simulate_checkbox_click('email_consent_form_email_consent')
    click_on I18n.t('email_consent.submit')
  end

  def confirm_account(pass, us_citizen, country)
    fill_in 'user_password', with: pass
    fill_in 'user_password_confirmation', with: pass

    check_radio_button('user[us_citizen]', us_citizen)

    select country, from: 'user_country'

    click_on I18n.t('sign_up.form_section.commit')
  end

  def check_radio_button(field, flag)
    pattern = ".custom-radio-input[data-custom-radio-input-name='#{field}'][data-custom-radio-input-value=#{flag}]"
    button = page.find(:css, pattern)
    button.click
  end

  def simulate_checkbox_click(field)
    page.execute_script("$('##{field}').click()")
  end

  def confirm_email_and_sign_in(email, pass)
    confirm_email(email)
    sign_in(email, pass)
  end

  def confirm_email(email)
    confirm_user(User.find_by_email(email))
  end

  def sign_in(email, pass)
    fill_in 'user_email', with: email, match: :first
    fill_in 'user_password', with: pass, match: :first

    click_button I18n.t('sign_in.form_section.commit'), match: :first
  end

  def fill_in_reg_c_investment(income, net_worth, external_investments, amount)
    fill_in_throttled_keyup_input 'income', with: income, delay: 0
    fill_in_throttled_keyup_input 'net_worth', with: net_worth, delay: 0
    fill_in_throttled_keyup_input 'external', with: external_investments, delay: 0
    fill_in_throttled_keyup_input 'investment_amount', with: amount, delay: 0
  end

  def save_investment
    click_button I18n.t('investment_flow.invest.flow_selector.amount_calculator.buttons.save')
  end

  def select_reg_c
    page.find('a[href="#reg-c-panel"]').click
  end

  def select_reg_d
    page.find('a[href="#reg-d-panel"]').click
  end

  def select_verify_entity_offline
    page.find('a[href="#entity-offline"]').click
  end

  def confirm_user(user)
    user.send(:generate_confirmation_token!)
    visit user_confirmation_path(confirmation_token: user.instance_variable_get('@raw_confirmation_token'))
  end
end
