module ControllerSpecHelpers
  def set_request_ip(ip_address)
    @request.env['REMOTE_ADDR'] = ip_address
  end
end