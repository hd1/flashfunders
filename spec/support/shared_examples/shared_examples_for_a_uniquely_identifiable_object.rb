shared_examples_for 'it is uniquely identifiable' do

  let(:some_uuid) { '71ba265e-5ae8-4b1c-9d24-02d8f3d85b2d' }

  context 'uuid' do
    it 'saves a generated uuid before object is created' do
      expect(SecureRandom).to receive(:uuid).and_return(some_uuid)
      object = described_class.create!(valid_params)
      expect(object.uuid).to eq(some_uuid)
    end

    it 'does not regenerate uuid when object is updated' do
      expect(SecureRandom).to receive(:uuid).and_return(some_uuid)
      object = described_class.create!(valid_params)

      expect { object.update_attributes!(id: 4333) }.to_not change{ object.uuid }
    end
  end

end
