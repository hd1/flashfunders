shared_examples_for 'a campaign controller with campaign authorization' do
  context 'when not logged in as the campaign owner' do
    let(:non_campaign_owner) { create_user(email: 'non_owner@example.com') }

    before do
      sign_in :user, non_campaign_owner
    end

    describe 'GET edit' do
      it 'renders a 404 page' do
        get :edit, offering_id: 42

        expect(response.status).to eq(404)
      end
    end

    describe 'PATCH update' do
      it 'renders a 404 page' do
        patch :update, offering_id: 42

        expect(response.status).to eq(404)
      end
    end
  end
end