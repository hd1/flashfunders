require 'rspec/expectations'

RSpec::Matchers.define :have_case_insensitive_content do |expected|
  match do |actual|
    actual.text.downcase.match(Regexp.quote(expected.downcase))
  end

  failure_message do |actual|
    "expected \"#{expected.downcase}\" to appear in \"#{actual.text}\""
  end

  failure_message_when_negated do |actual|
    "expected \"#{expected.downcase}\" to not appear in \"#{actual.text}\""
  end
end