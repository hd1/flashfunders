require 'rails_helper'

feature 'Investor can invest in a campaign', :warden_stub do

  def find_value_for_definition(list_class_name, definition)
    list_xpath_prefix = "//dl[contains(concat(' ',normalize-space(@class),' '),' #{list_class_name} ')]"
    page.find(:xpath, "#{list_xpath_prefix}//dt[text()='#{definition}']/../dd").text()
  end

  def find_value_for_definition_with_popover
    page.find(:xpath, "//dt[text()[2]]/../dd").text()
  end

  let(:user) do
    create_user(email: 'user@example.com', password: 'thepassword', password_confirmation: 'thepassword')
  end
  let!(:investor_entity) { FactoryGirl.create(:investor_entity) }
  let(:offering) do
    PersonalFederalIdentificationInformation.new({
                                                   investor_entity_id: investor_entity.id,
                                                   first_name: 'Guy',
                                                   middle_initial: '',
                                                   last_name: 'Duke',
                                                   address1: '123 Street',
                                                   address2: '#555',
                                                   city: 'Boulder',
                                                   state_id: 'CO',
                                                   zip_code: '80304',
                                                   daytime_phone: '1234567890',
                                                   ssn: '555-55-5555',
                                                   date_of_birth: '01/01/1980',
                                                   us_citizen: true
                                                 })
    offering = OfferingCreator.new.create(user_id: user.id)
    offering.update_attributes(
      ends_at: 60.days.from_now,
      tagline: 'The disappearing, reappearing ink.',
      visibility: :visibility_public,
      fully_diluted_shares: 10000,
      share_price: 2.035,
      pre_money_valuation: (10000 * 2.035),
      investable: true)
    offering.securities.first.update_attributes(
        type: 'Securities::FspStock',
        shares_offered: 5000,
        maximum_total_investment: (5000 * 2.035),
        minimum_total_investment: 1000000,
        minimum_total_investment_display: '$1 million',
        minimum_investment_amount: 5000,
    )
    Company.where(offering_id: offering.id).first.update_attributes(website_url: 'http://foobar.com', name: 'Acme')
    offering.reload
  end
  let(:investor_signing_status) { 'completed' }

  before do
    RoutingNumber.create! routing_number: '012345678'

    allow_any_instance_of(DocusignRest::Client).to receive(:get_recipient_view).and_return({ 'url' => 'docusign.foo.bar' })
    allow_any_instance_of(DocusignRest::Client).to receive(:create_envelope_from_template).and_return({ 'envelopeId' => 'envelope_id' })
    allow_any_instance_of(DocusignRest::Client).to receive(:get_envelope_recipients).and_return(
      { 'signers' => [
        {
          'roleName' => 'issuer',
          'status' => 'sent',
        },
        {
          'roleName' => 'investor',
          'status' => investor_signing_status,
        }
      ] })
    allow(Offering).to receive(:find).and_return(offering)
  end

  context 'logged out user (Use Case: #3)' do
    before do
      allow(InvestorMailer).to receive_message_chain(:checking_in, :deliver_later)
    end

    it 'can enter an investment', :js do
      step 'by clicking Invest on the campaign page' do
        visit offering_path(offering)

        within '.offering-head' do
          click_on I18n.translate('investor_investments.invest.start')
        end
      end

      step 'investor cannot invest an invalid amount' do
        fill_in_throttled_keyup_input I18n.translate('investor_investments.form_section.investment_amount.label'), with: 0

        click_on (I18n.translate('investor_investments.form_section.commit'))

        expect(page).to have_content('Must be larger than $5000.00')
      end

      step 'investor can invest a valid amount' do
        fill_in_throttled_keyup_input I18n.translate('investor_investments.form_section.investment_amount.label'), with: 5003

        within '.equity-ownership-widget' do
          expect(page).to have_content('16.393% ' + I18n.t('investor_investments.form_section.investment_details.part_2_stock') +
                                         ' $5,004.06')
        end

        click_on (I18n.translate('investor_investments.form_section.commit'))

        expect(current_path).to eq(new_user_registration_path)
      end

      step 'signs up user that has no account' do
        create_account('John Q User', 'new_user@example.com', 'secretpassword')
        submit_email_consent

        expect(current_path).to eq(new_offering_invest_investment_profile_path(offering))
      end

      step 'user confirms their account by clicking on the link in the email' do
        pending 'Confirmations temporarily suspended'
        confirm_user(User.find_by_email('new_user@example.com'))

        expect(current_path).to eq(new_user_session_path)
      end

      step 'and signs in to continue their investment' do
        fill_in 'user_email', with: 'new_user@example.com'
        fill_in 'user_password', with: 'secretpassword'

        click_on I18n.t('sign_in.form_section.commit')

        expect(current_path).to eq(new_offering_invest_investment_profile_path(offering))
      end
    end
  end

  context 'logged out user has completed the investment already' do
    let(:entity) { InvestorEntity.create(user_id: user.id) }
    let(:investment) { Investment.create(user_id: user.id, offering_id: offering.id, security: offering.securities.first, shares: 1222, investor_entity: entity) }

    before do
      investment.begin!
      investment.select_entity!
      investment.verify_accreditation!
      Accreditation::StatusUpdater.new.update_investment(investment)
      investment.sign_documents!
      investment.select_funding_method!
      investment.transfer_funds!
    end

    it 'can enter an investment', :js do
      step 'by clicking Invest on the campaign page' do
        visit offering_path(offering)

        within '.offering-head' do
          click_on I18n.translate('investor_investments.invest.start')
        end
      end

      step 'investor can invest a valid amount' do
        fill_in_throttled_keyup_input I18n.translate('investor_investments.form_section.investment_amount.label'), with: 5003

        within '.equity-ownership-widget' do
          expect(page).to have_content('16.393% ' + I18n.t('investor_investments.form_section.investment_details.part_2_stock') +
                                         ' $5,004.06')
        end

        click_on (I18n.translate('investor_investments.form_section.commit'))

        sleep(0.5) # due to a delay in the form submission

        expect(current_path).to eq(new_user_registration_path)
      end

      log_in_with_credentials('user@example.com', 'thepassword')

      expect(current_path).to eq(dashboard_investments_path)
      expect(find_value_for_definition('investment-details', I18n.t('investment_dashboard.my_investments_tab.investment_details.shares_owned.term'))).to eq('1,222')
    end
  end

  context 'For an SPV offering' do
    before do
      offering.update_attributes(spv_minimum_goal: 25000)
      offering.securities.create(is_spv: true, type: 'Securities::FspStock', minimum_total_investment: 25000, minimum_investment_amount: 25000)
      sign_in_user(user)
      visit offering_path(offering)

      within '.offering-head' do
        click_on I18n.translate('investor_investments.invest.start')
      end
    end
    after do
      offering.securities.where(is_spv: true).delete_all
    end
    it 'on the campaign page', :js do
      step 'the new investment page displays the correct text for an SPV offering' do
        expect(page).to have_content((I18n.translate('investor_investments.workspace_title') + ' Acme'))
        expect(page).to have_content("Acme #{I18n.translate('investor_investments.part_1')} 5,000 #{I18n.translate('investor_investments.shares')} ($2.035/#{I18n.translate('investor_investments.share')}) #{I18n.translate('investor_investments.part_2_stock', stock_offered: 'FlashSeed Preferred')}")
        expect(page).to have_content("Investments under $25,000.00 will be made into a FlashFund (LLC) that holds FlashSeed Preferred stock in Acme.")
      end
    end
  end

  context 'logged in user in the javascript part of the flow' do
    before { sign_in_user(user) }
    it 'on the campaign page', :js do
      step 'by clicking Invest on the campaign page' do
        visit offering_path(offering)

        within '.offering-head' do
          click_on I18n.translate('investor_investments.invest.start')
        end
      end

      step 'the new investment page displays the correct company for a direct campaign' do
        expect(page).to have_content((I18n.translate('investor_investments.workspace_title') + ' Acme'))
        expect(page).to have_content("Acme #{I18n.translate('investor_investments.part_1')} 5,000 #{I18n.translate('investor_investments.shares')} ($2.035/#{I18n.translate('investor_investments.share')}) #{I18n.translate('investor_investments.part_2_stock', stock_offered: 'FlashSeed Preferred')}")
        expect(page).not_to have_content("Investments under")
        expect(page).to have_content(I18n.translate('investor_investments.raised').upcase)
        expect(page).to have_content(I18n.translate('investor_investments.total_equity_offered').upcase)
        expect(page).to have_content(I18n.translate('investor_investments.pre_money_valuation').upcase)

        expect(page).to have_content(I18n.translate('investor_investments.form_section.investment_amount.label'))

        expect(page).to have_content(I18n.translate('investor_investments.form_section.investment_details.title'))
        within '.equity-ownership-widget' do
          expect(page).to have_content(' --% ' + I18n.t('investor_investments.form_section.investment_details.part_2_stock') +
                                         ' $--')
        end
      end

      step 'and by trying to prematurely visit later stages of the investment path, the investor finds themselves on the new investment page' do

        visit edit_offering_investment_path(offering)
        expect(page).to have_content(I18n.translate('investor_investments.form_section.investment_amount.label'))
        expect(page.current_path).to eq(new_offering_investment_path(offering))

        visit offering_invest_investment_documents_path(offering)
        expect(page).to have_content(I18n.translate('investor_investments.form_section.investment_amount.label'))
        expect(page.current_path).to eq(new_offering_investment_path(offering))

        visit new_offering_invest_fund_path(offering)
        expect(page).to have_content(I18n.translate('investor_investments.form_section.investment_amount.label'))
        expect(page.current_path).to eq(new_offering_investment_path(offering))

        visit new_offering_invest_fund_path(offering)
        expect(page).to have_content(I18n.translate('investor_investments.form_section.investment_amount.label'))
        expect(page.current_path).to eq(new_offering_investment_path(offering))
      end

      step 'investor cannot invest an invalid amount' do
        fill_in_throttled_keyup_input I18n.translate('investor_investments.form_section.investment_amount.label'), with: 5

        click_on (I18n.translate('investor_investments.form_section.commit'))

        expect(page).to have_content('Must be larger than $5000.00')

        fill_in_throttled_keyup_input I18n.translate('investor_investments.form_section.investment_amount.label'), with: '49$$$$$$$99-&-;-^-%---any old crap you like'

        click_on (I18n.translate('investor_investments.form_section.commit'))

        expect(page).to have_content('Must be larger than $5000.00')
        expect(find_field(I18n.translate('investor_investments.form_section.investment_amount.label')).value).to eq('49$$$$$$$99-&-;-^-%---any old crap you like')

        fill_in_throttled_keyup_input I18n.translate('investor_investments.form_section.investment_amount.label'), with: ''

        click_on (I18n.translate('investor_investments.form_section.commit'))
        expect(page).to have_content('Provide investment amount in ####.## format.')
        expect(page).to have_content('--% ' + I18n.t('investor_investments.form_section.investment_details.part_2_stock') +
                                       ' $--')

        expect(find_field(I18n.translate('investor_investments.form_section.investment_amount.label')).value).to eq('')
      end

    end
  end

  context 'logged in user in the non-javascript part of the flow' do
    background do
      sign_in_user(user)
      investor_entity = InvestorEntity::Individual.create!(user_id: user.id + 100, identification_status_id: :verified)
      investor_entity.investments.create!(user_id: user.id + 100, offering_id: offering.id, security_id: offering.securities.first.id, shares: 10000, amount: 200000, percent_ownership: 26.387, state: 'escrowed')
      Accreditor::IndividualIncomeQualification.create!(user_id: user.id + 100, investor_entity_id: investor_entity.id, expires_at: offering.ends_at + 10.days, consent_to_income: true)
      create_user(id: user.id + 100, email: 'authorized_user@example.com', authorized_to_invest_status: User.authorized_to_invest_statuses[:allowed])

      @investment = Investment.create!(
        id: 8433,
        user_id: user.id,
        offering_id: offering.id,
        security: offering.securities.first,
        shares: 2459,
        amount: 5004.06,
        percent_ownership: 16.393,
        investor_entity_id: investor_entity.id,
        accreditation_status_id: :pending,
        funding_method: :check)
      @investment.begin!
    end

    it 'allows a user to wire transfer money into the escrow', js: true do

      @investment.select_entity!
      @investment.verify_accreditation!

      Accreditation::StatusUpdater.new.update_investment(@investment)
      @investment.sign_documents!
      @investment.select_funding_method!

      step 'and funds have been transferred' do
        @investment.transfer_funds!

        visit dashboard_investments_path

        expect(page).to_not have_content I18n.t('investor_investments.invest.resume')
      end

      step 'investor can no longer visit the transfer investments page' do
        visit new_offering_invest_fund_path(offering)
        expect(page.current_path).to eq(dashboard_investments_path)
      end

      step 'by clicking Invest on the campaign page' do
        visit offering_path(offering)

        within '.offering-head' do
          click_on I18n.translate('investor_investments.invest.complete')
        end
        expect(page.current_path).to eq(dashboard_investments_path)
      end
    end

    it 'having made a valid investment, the user', js: true, percy: true do

      step 'view investment on dashboard' do
        visit dashboard_investments_path

        step 'in the details section' do
          expect(page).to have_case_insensitive_content('Acme')

          campaign_list_class = 'campaign-details'
          expect(find_value_for_definition(campaign_list_class, I18n.t('investment_dashboard.my_investments_tab.offering_details.amount_raised.term'))).to eq('$200,000')
          expect(find_value_for_definition(campaign_list_class, I18n.t('investment_dashboard.my_investments_tab.offering_details.offering_goal.term'))).to eq('$1 million')
          expect(find_value_for_definition(campaign_list_class, I18n.t('investment_dashboard.my_investments_tab.offering_details.days_left.term'))).to eq('60 days')

          investment_list_class = 'investment-details'
          expect(find_value_for_definition(investment_list_class, I18n.t('investment_dashboard.my_investments_tab.investment_details.investment_amount.term'))).to eq('$5,004.06')
          expect(find_value_for_definition(investment_list_class, I18n.t('investment_dashboard.my_investments_tab.investment_details.shares_owned.term'))).to eq('2,459')
          expect(find_value_for_definition_with_popover).to eq('16.393%')
        end

        step 'in the status section, report useful information' do
          expect(page).to have_case_insensitive_content I18n.t('investment_dashboard.my_investments_tab.statuses.incomplete.title')
          expect(page).to have_content I18n.t('investment_dashboard.my_investments_tab.statuses.incomplete.description')

          click_on I18n.t('investor_investments.invest.resume')
          expect(page.current_path).to eq(new_offering_invest_investment_profile_path(offering))
        end
      end

      step 'is redirected to the investment show page when trying to access later investment stages, or a new investment' do
        visit new_offering_investment_path(offering)
        expect(page.current_path).to eq(new_offering_invest_investment_profile_path(offering))

        visit edit_offering_investment_path(offering)
        expect(page).to have_case_insensitive_content((I18n.translate('investor_investments.workspace_title') + ' Acme'))
        expect(page.current_path).to eq(edit_offering_investment_path(offering))

        visit new_offering_invest_fund_path(offering)
        expect(page.current_path).to eq(new_offering_invest_investment_profile_path(offering))
      end

      step 'investor chooses and submits an investment method' do
        expect(page).to have_case_insensitive_content(I18n.t('investment_profile.new.title'))

        click_on I18n.t('investment_profile.new.individual.title')

        invest_as_individual(true)

        Percy::Capybara.snapshot(page, name: 'reg_d_only_individual_profile')
        click_on I18n.t('investment_profile.new.buttons.submit')
      end

      step 'investor chooses and submits an accreditation type' do
        expect(page).to have_case_insensitive_content(I18n.t('investor_accreditation.new.title'))

        click_on I18n.t('investor_accreditation.new.verify_net_worth.name')
        page.find('a[href="#net-worth-third-party"] .title').click

        select_from_dropdown "Licensed Attorney", from: 'third_party_net_worth_form_role'
        fill_in I18n.t('investor_accreditation.new.verify_net_worth.third_party_verification.form.fieldset.email'), with: 'foo@example.com'
        click_on I18n.t('investor_accreditation.buttons.submit')
      end

      step 'investor can see their investment details' do
        expect(page).to have_case_insensitive_content(I18n.translate('investment_documents.title'))

        within '#amount' do
          expect(page).to have_content '$5,004.06'
        end
      end

      step 'investor declines documents' do
        @investment.reload.decline_documents!
        visit dashboard_investments_path
        expect(page).to have_case_insensitive_content I18n.translate('investment_dashboard.my_investments_tab.statuses.incomplete.title')
      end

      step 'Investor signs docs and can no longer visit the new, edit, or agree, sign, docusign_handler and docusign_redirect to documents routes' do
        @investment.sign_documents!
        visit new_offering_investment_path(offering)
        expect(page.current_path).to eq(offering_invest_fund_path(offering))

        visit edit_offering_investment_path(offering)
        expect(page.current_path).to eq(offering_invest_fund_path(offering))

        visit offering_invest_investment_documents_path(offering)
        expect(page.current_path).to eq(offering_invest_fund_path(offering))

        visit docusign_handler_offering_invest_investment_documents_path(offering)
        expect(page.current_path).to eq(offering_invest_fund_path(offering))

        visit docusign_redirect_offering_invest_investment_documents_path(offering)
        expect(page.current_path).to eq(offering_invest_fund_path(offering))

      end

      step 'and can no longer visit anything other than the dashboard' do
        @investment.select_funding_method!
        visit new_offering_investment_path(offering)
        expect(page.current_path).to eq(dashboard_investments_path)

        visit edit_offering_investment_path(offering)
        expect(page.current_path).to eq(dashboard_investments_path)

        visit offering_invest_investment_documents_path(offering)
        expect(page.current_path).to eq(dashboard_investments_path)

      end

      step 'investor logs out and enters in new investment info' do
        find('li.user-icon').click()
        click_on I18n.t('nav.sign_out')

        expect(page.current_path).to eq root_path

        visit new_offering_investment_path(offering)

        expect(page).to have_content I18n.t('investor_investments.form_section.investment_amount.label')
        fill_in_throttled_keyup_input I18n.t('investor_investments.form_section.investment_amount.label'), with: 5003
        within '.equity-ownership-widget' do
          expect(page).to have_content('16.393% ' + I18n.t('investor_investments.form_section.investment_details.part_2_stock') +
                                         ' $5,004.06')
        end
        click_on (I18n.translate('investor_investments.form_section.commit'))

        sleep(0.5) # due to a delay in the form submission

        log_in_with_credentials('user@example.com', 'thepassword')
      end

      step 'dashboard no longer provides links to investment workflow when funding is happening' do
        visit dashboard_investments_path
        Percy::Capybara.snapshot(page, name: 'investment_dashboard')
        expect(page).to_not have_content I18n.t('investor_investments.invest.resume')
      end

      step 'dashboard shows investment status as waiting on wire' do
        expect(page).to have_content I18n.t('investment_dashboard.my_investments_tab.statuses.payment_pending.wire', amount: '$5,004.06')
      end

      step 'by clicking Invest on the campaign page' do
        visit offering_path(offering)

        within '.offering-head' do
          click_on I18n.translate('investor_investments.invest.complete')
        end

        expect(page.current_path).to eq(dashboard_investments_path)

      end

      step 'pending transfer and accreditation is not complete' do
        visit dashboard_investments_path

        expect(page).to have_case_insensitive_content I18n.t('investment_dashboard.my_investments_tab.statuses.unaccredited.title')
        expect(page).to have_content I18n.t("investment_dashboard.my_investments_tab.statuses.payment_pending.#{@investment.funding_method}", amount: InvestmentPresenter.new(@investment).formatted_amount)
      end

      step 'accreditation is complete' do
        qualification = Accreditor::Qualification.where(user_id: user.id).first
        qualification.update_attributes expires_at: 1.year.from_now, verified_at: Date.today
        Accreditation::StatusUpdater.new.update_investment @investment
        @investment.verified!

        visit dashboard_investments_path
        expect(page).to have_content I18n.t('investment_dashboard.my_investments_tab.statuses.accredited.message')
      end

      step 'campaign escrow end date is in the past' do
        @investment.update_attributes state: 'intended'
        offering.update_attributes ends_at: 1.week.ago, escrow_end_date: 1.day.ago

        visit dashboard_investments_path
        expect(page).to_not have_case_insensitive_content I18n.t('investment_dashboard.my_investments_tab.statuses.incomplete.title')
        expect(page).to_not have_case_insensitive_content I18n.t('investor_investments.invest.resume')
      end
    end
  end

  context 'campaign has closed' do
    before do
      offering.update_attributes({ ends_at: 1.day.ago })
      sign_in_user(user)
      Investment.create!(id: 8433, user_id: user.id, offering_id: offering.id, security: offering.securities.first)
    end

    shared_examples 'offering closed correctly' do |closed_status, days_left|
      it 'displays correct days left' do
        offering.update_attribute(:closed_status, closed_status)

        Timecop.travel do
          visit dashboard_investments_path
          expect(page).to have_case_insensitive_content days_left
        end
      end
    end

    it_should_behave_like 'offering closed correctly', :successful, I18n.t('browse.discovery.campaign_swatch.offering.end_date.funded')
    it_should_behave_like 'offering closed correctly', :pending, I18n.t('browse.discovery.campaign_swatch.pending')
  end

  context 'partially registered user (Use Case: #7, #8)' do
    before do
      allow(InvestorMailer).to receive_message_chain(:checking_in, :deliver_later)
    end

    let(:partially_registered_user) { create_temporary_user(email: 'joe@partially.com') }

    it 'can enter an investment', :js do
      pending 'Reinstate when we reenable confirmations'
      step 'by clicking Invest on the campaign page' do
        visit offering_path(offering)

        within '.offering-head' do
          click_on I18n.translate('investor_investments.invest.start')
        end
      end

      step 'investor can invest a valid amount' do
        fill_in_throttled_keyup_input I18n.translate('investor_investments.form_section.investment_amount.label'), with: 5003

        within '.equity-ownership-widget' do
          expect(page).to have_content('16.393% ' + I18n.t('investor_investments.form_section.investment_details.part_2_stock') +
                                         ' $5,004.06')
        end

        click_on (I18n.translate('investor_investments.form_section.commit'))

        expect(current_path).to eq(new_user_registration_path)
      end

      step 'tries to sign in' do
        within '.panel' do
          click_on I18n.t('sign_up.have_account_prompt')
        end

        fill_in 'user_email', with: partially_registered_user.email
        fill_in 'user_password', with: 'secretpassword'

        within '.panel' do
          click_on I18n.t('sign_in.form_section.commit')
        end

        expect(current_path).to eq(new_user_confirmation_path)
        expect(page).to have_content(I18n.t('devise.confirmations.user.invest_investments.partially_registered'))
      end

      step 'user confirms their account by clicking on the link in the email' do
        confirm_user(partially_registered_user)

        expect(current_path).to eq(user_confirmation_path)
      end

      step 'sets up their password and continue their investment' do
        confirm_account('secretpassword', true, 'United Kingdom')

        expect(current_path).to eq(new_offering_invest_investment_profile_path(offering))
      end
    end
  end

  context 'unconfirmed user (Use Case: #14)' do
    before do
      allow(InvestorMailer).to receive_message_chain(:checking_in, :deliver_later)
    end

    it 'registers, invests, confirms, continues investment', :js do
      step 'user registers' do
        visit new_user_registration_path

        create_account('Joe User', 'joe@unconfirmed.com', 'secretpassword')
        submit_email_consent

        expect(current_path).to eq(root_path)
      end

      step 'clicks Invest on the campaign page' do
        visit offering_path(offering)

        within '.offering-head' do
          click_on I18n.translate('investor_investments.invest.start')
        end
      end

      step 'investor can invest a valid amount' do
        fill_in_throttled_keyup_input I18n.translate('investor_investments.form_section.investment_amount.label'), with: 5003

        within '.equity-ownership-widget' do
          expect(page).to have_content('16.393% ' + I18n.t('investor_investments.form_section.investment_details.part_2_stock') +
                                         ' $5,004.06')
        end

        click_on (I18n.translate('investor_investments.form_section.commit'))

        expect(current_path).to eq(new_offering_invest_investment_profile_path(offering))
      end

      step 'tries to sign in' do
        pending 'Confirmations temporarily suspended'
        within '.panel' do
          click_on I18n.t('sign_up.have_account_prompt')
        end

        fill_in 'user_email', with: User.last.email
        fill_in 'user_password', with: 'secretpassword'

        within '.panel' do
          click_on I18n.t('sign_in.form_section.commit')
        end

        expect(current_path).to eq(new_user_confirmation_path)
        expect(page).to have_content(I18n.t('devise.confirmations.user.invest_investments.fully_registered'))
      end

      step 'user confirms their account by clicking on the link in the email' do
        pending 'Confirmations temporarily suspended'
        confirm_user(User.last)

        expect(current_path).to eq(new_user_session_path)
      end

      step 'signs in and continues the investment' do
        pending 'Confirmations temporarily suspended'
        fill_in 'user_email', with: User.last.email
        fill_in 'user_password', with: 'secretpassword'

        within '.panel' do
          click_on I18n.t('sign_in.form_section.commit')
        end

        expect(current_path).to eq(new_offering_invest_investment_profile_path(offering))
      end

    end
  end

end
