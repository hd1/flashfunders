require 'rails_helper'

feature 'Visit the browse page as a logged-in user' do
  let!(:user) { create_user(email: 'foo@example.com', password: 'password', password_confirmation: 'password') }

  before do
    create_complete_open_campaign
    create_complete_closed_campaign
    sign_in_user(user)
  end

  scenario 'who should not see the subscription form', js: true do
    visit browse_path
    expect(page).to_not have_selector('.subscription-container')
  end

end

feature 'Visit the browse page to see an offering with various ends_at values' do
  let!(:campaign) { create_complete_open_campaign }
  let!(:entity) { FactoryGirl.create(:investor_entity_individual) }
  let!(:investment) { FactoryGirl.create(:investment, :escrowed, investor_entity_id: entity.id, offering: campaign, security_id: campaign.reg_d_direct_security.id, amount: campaign.reg_d_direct_security.minimum_total_investment.to_f, funding_method: 'wire')}

  before do
    investment.user.allowed!
  end

  scenario 'offering is closed successfully', js: true do
    campaign.visibility_public!
    campaign.update_attribute(:investable, true)

    campaign.update_attribute(:ends_at, DateTime.yesterday.midnight.advance(hours: 24, seconds: -1))
    campaign.update_attribute(:closed_status, :successful)
    Timecop.freeze do
      visit browse_path
      expect(page).to have_case_insensitive_content('Successfully Funded')
    end
  end

  scenario 'offering is closed unsuccessfully', js: true do
    campaign.update_attribute(:ends_at, 1.day.ago)
    campaign.update_attribute(:closed_status, :unsuccessful)
    Timecop.freeze do
      visit browse_path
      expect(page).not_to have_case_insensitive_content('FooBiz')
    end
  end

  scenario 'offering is closed pending', js: true do
    campaign.update_attribute(:ends_at, 1.day.ago)
    campaign.update_attribute(:closed_status, :pending)
    Timecop.freeze do
      visit browse_path
      expect(page).to have_case_insensitive_content("FooBiz")
    end
  end

  scenario 'offering is still open with a day to go', js: true do
    campaign.update_attribute(:ends_at, 1.days.from_now)
    Timecop.freeze do
      visit browse_path
      expect(page).to have_case_insensitive_content("1 Day Left")
    end
  end

  scenario 'offering is still open with less than a day to go', js: true do
    campaign.update_attribute(:ends_at, 2.seconds.from_now)
    Timecop.freeze do
      visit browse_path
      expect(page).to have_case_insensitive_content("0 Days Left")
    end
  end
end
