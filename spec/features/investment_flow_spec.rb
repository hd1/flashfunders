require 'rails_helper'
include ActionView::Helpers::NumberHelper

feature 'Investment Flow', js: true do
  context 'With a Reg C Enabled Offering' do
    let(:offering) {
      offering = create_complete_open_campaign
      FactoryGirl.create(:cf_stock, offering: offering)
      offering
    }
    let(:email) { 'reg_c_investor@example.com' }
    let(:pass) { 'secretpassword' }

    context 'An an unregistered user', js: true do
      before do
        logout
      end

      scenario 'I can sign up and start an investment', percy: true do
        step 'View offering page and start investment' do
          visit offering_path(offering)

          expect(current_path).to eq(offering_path(offering))

          within '.company-raised' do
            click_on I18n.t('investor_investments.invest.start')
          end

          expect(current_path).to eq(new_user_registration_path)
        end

        step 'I sign up for a new account' do
          create_account('Reg C Investor', email, pass)
          submit_email_consent

          # expect(current_path).to eq(new_user_confirmation_path)
          #
          # confirm_email(email)
          #
          # expect(current_path).to eq(new_user_session_path)
          #
          # sign_in(email, pass)

          expect(current_path).to eq(new_offering_investment_flow_path(offering))
        end

        step 'I select the Reg C tab and see correct amount feedback' do
          select_reg_c
          expect(page).to have_case_insensitive_content(I18n.t('investment_flow.invest.flow_selector.reg_c_calculator.title'))

          fill_in_reg_c_investment(150000, 1000000, 0, 50000) # too much
          check_amount_error('You can invest up to $15,000. Please update your investment amount.')

          fill_in_throttled_keyup_input 'investment_amount', with: 49, delay: 0 # too little
          check_amount_error('You must invest at least $50')

          fill_in_throttled_keyup_input 'investment_amount', with: 10000, delay: 0 # just right

          check_amount_ok
          Percy::Capybara.snapshot(page, name: 'reg_c_amount')
        end

        step 'I select the Reg D tab and see correct amount feedback' do
          select_reg_d

          fill_in_throttled_keyup_input 'investment_amount', with: offering.reg_d_direct_security.maximum_total_investment + 1 # too much
          check_amount_error("You can invest up to #{number_to_currency(offering.reg_d_direct_security.maximum_total_investment, precision: 0)}. Please update your investment amount.")

          fill_in_throttled_keyup_input 'investment_amount', with: 19999, delay: 0 # too little
          check_amount_error('You must invest at least $20,000')

          fill_in_throttled_keyup_input 'investment_amount', with: 20000, delay: 0 # just right

          check_amount_ok
          Percy::Capybara.snapshot(page, name: 'reg_d_amount')
        end

      end
    end

    context 'As a confirmed user', js: true do
      let(:user) { create_user(email: 'verifying_user@qa.com', us_citizen: true, country: 'US') }
      let(:bank_name) { 'Bank of Vinnie' }
      let(:routing_number) { '012345678' }

      context 'As a domestic user' do
        before do
          sign_in_user(user)
          RoutingNumber.create! routing_number: routing_number, name: bank_name
        end

        context 'starting a Reg C investment' do
          scenario 'I visit the amount page', percy: true do
            step 'choose the Reg C tab and complete the form' do
              visit new_offering_investment_flow_path(offering)

              select_reg_c
              expect(page).to have_case_insensitive_content(I18n.t('investment_flow.invest.flow_selector.reg_c_calculator.title'))

              fill_in_reg_c_investment(150000, 1000000, 0, 14799)
              check_amount_ok

              save_investment
              expect(current_path).to eq(new_offering_invest_investment_profile_path(offering))
            end

            step 'Edit the amount' do
              select_amount

              expect(current_path).to eq(edit_offering_investment_flow_path(offering))
            end

            step 'Do not change the amount' do
              select_profile

              expect(current_path).to eq(new_offering_invest_investment_profile_path(offering))
              Percy::Capybara.snapshot(page, name: 'reg_c_profile')
            end

            step 'Complete Profile (Individual Entity)' do
              expect(page).to_not have_case_insensitive_content(I18n.t('investment_profile.new.individual.title'))
              expect(page).to_not have_case_insensitive_content(I18n.t('investment_profile.new.entity.title'))

              fill_in_individual_entity('California', true, true)

              expect(current_path).to eq(offering_invest_investment_documents_path(offering))
              Percy::Capybara.snapshot(page, name: 'reg_c_terms_of_investment')
            end

            step 'Sign Documents' do
              expect(page).to have_case_insensitive_content(I18n.translate('investment_documents.title'))

              within '#amount' do
                expect(page).to have_content '$14,800.00'
              end

              agree_to_and_sign_documents
              expect(current_path).to eq(offering_investment_flow_fund_path(offering))
            end

            step 'Select Funding Method (ACH)' do
              refund_account_description = ActionView::Base.full_sanitizer.sanitize(I18n.t('fund_flow.ach.link_refund_account.description', more_link: '')).strip

              expect(page).to have_case_insensitive_content(I18n.t('fund_flow.ach.title'))
              expect(page).to have_case_insensitive_content(I18n.t('fund_flow.wire.title'))

              click_on I18n.t('fund_flow.ach.title')

              expect(page).to have_case_insensitive_content(I18n.t('fund_flow.ach.link_refund_account.title'))
              expect(page).to have_case_insensitive_content(refund_account_description)
              investment = Investment.last
              expect(page).to have_case_insensitive_content(I18n.t('fund_flow.authorization.text', escrow_provider_long: investment.security.escrow_provider.title(long: true), escrow_provider_short: investment.security.escrow_provider.title))

              fill_in_ach_refund_account(user.registration_name, routing_number, '999999999', 'Checking')

              wait_for_ajax # need to give the api call time to complete

              expect(page).to have_case_insensitive_content("#{I18n.t('link_refund_account.account_number_label')} - #{bank_name}")
              Percy::Capybara.snapshot(page, name: 'reg_c_ach')
            end

            step 'Change to Wire' do
              click_on I18n.t('fund_flow.wire.title')

              expect(page).to have_case_insensitive_content(I18n.t('fund_flow.wire.link_refund_account.title'))
              expect(page).to have_case_insensitive_content(I18n.t('fund_flow.wire.link_refund_account.description'))
              investment = Investment.last
              expect(page).to_not have_case_insensitive_content(I18n.t('fund_flow.authorization.text', escrow_provider_long: investment.security.escrow_provider.title(long: true), escrow_provider_short: investment.security.escrow_provider.title))

              fill_in_wire_refund_account(user.registration_name, routing_number, '999999999', 'Checking')

              expect(page).to have_case_insensitive_content(I18n.t('link_refund_account.account_number_label'))
              expect(page).to_not have_case_insensitive_content("#{I18n.t('link_refund_account.account_number_label')} - #{bank_name}")
              Percy::Capybara.snapshot(page, name: 'reg_c_wire')
            end

            step 'Submit investment with ach' do
              click_on I18n.t('fund_flow.ach.title')
              click_on I18n.t('fund_flow.buttons.commit')

              expect(current_path).to eq(complete_offering_investment_flow_fund_path(offering))
              Percy::Capybara.snapshot(page, name: 'reg_c_ach_success')
              expect(current_path).to eq(complete_offering_investment_flow_fund_path(offering))
            end
          end
        end
      end

      context 'As an international user' do
        before do
          user.us_citizen = false
          user.country = 'Nigeria'
          user.save
          sign_in_user(user)
        end

        scenario 'Make sure tabs behave properly' do
          step 'I begin my Reg D investment' do
            visit new_offering_investment_flow_path(offering)

            select_reg_d

            fill_in_throttled_keyup_input 'investment_amount', with: offering.reg_d_direct_security.maximum_total_investment + 1, delay: 0 # too much
            check_amount_error("You can invest up to #{number_to_currency(offering.reg_d_direct_security.maximum_total_investment, precision: 0)}. Please update your investment amount.")

            fill_in_throttled_keyup_input 'investment_amount', with: 19999, delay: 0 # too little
            check_amount_error('You must invest at least $20,000')

            fill_in_throttled_keyup_input 'investment_amount', with: 20000, delay: 0 # just right
            check_amount_ok
          end

          step 'I switch to the Reg C tab and see correct amount feedback' do
            select_reg_c

            expect(page).to have_case_insensitive_content(I18n.t('investment_flow.invest.flow_selector.reg_c_calculator.title'))

            fill_in_reg_c_investment(150000, 1000000, 0, 50000) # too much
            check_amount_error('You can invest up to $15,000. Please update your investment amount.')

            fill_in_throttled_keyup_input 'investment_amount', with: 49, delay: 0 # too little
            check_amount_error('You must invest at least $50')

            fill_in_throttled_keyup_input 'investment_amount', with: 10000, delay: 0 # just right
            check_amount_ok
          end
        end
      end
    end

  end
end

def check_amount_error(msg)
  expect(page).to have_css('.invest-limit.limit-error')
  expect(page).to have_content(msg)
  expect(page).to have_button(I18n.t('investment_flow.invest.flow_selector.amount_calculator.buttons.save'), disabled: true)
end

def check_amount_ok
  expect(page).to_not have_css('.invest-limit.limit-error')
  expect(page).to have_button(I18n.t('investment_flow.invest.flow_selector.amount_calculator.buttons.save'), disabled: false)
end

def select_amount
  within '#progressbar' do
    click_on 'Amount'
  end
end

def select_profile
  within '#progressbar' do
    click_on 'Profile'
  end
end

def select_verify
  within '#progressbar' do
    click_on 'Verify'
  end
end

def select_sign_docs
  within '#progressbar' do
    click_on 'Sign Docs'
  end
end

def select_fund
  within '#progressbar' do
    click_on 'Fund'
  end
end
