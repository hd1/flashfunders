require 'rails_helper'

feature 'Offering Page' do
  before(:each) do
    # TODO: This should not be necessary, but some of the assertions fail with the new offering page
    Capybara.use_default_driver
    offering.user.update_attribute(:authorized_to_invest_status, User.authorized_to_invest_statuses[:allowed])
  end

  let!(:offering) do
    offering = create_complete_open_campaign
    cf_security = FactoryGirl.create(:cf_stock, offering: offering)
    create_deal_document(name: 'Term Sheet', documentable: cf_security)
    create_deal_document(name: 'LLC Agreement', documentable: cf_security)
    investor_entity = InvestorEntity.create!(user_id: offering.user_id, identification_status_id: :verified)
    investor_entity.investments.create!(user_id: offering.user_id, offering_id: offering.id, security: offering.reg_d_direct_security, shares: 100000, amount: 2000000, percent_ownership: 26.387, state: 'escrowed')
    offering.team_members.create!(full_name: "Reginald D. Member", position: "CEO and Founder", bio: "Renaissance man. The man with the\nplan.", facebook_url: "http://facebook.com/reginaldmember", linkedin_url: "http://linkedin.com/reginaldpro", twitter_url: "http://twitter.com/therealreginaldmember")
    offering.investors.create!(full_name: "Burt Q. Investorman", position: "Investor", bio: "I like to swim in my pool of money!")
    offering.reload
  end

  scenario 'Browsing for and viewing a campaign' do
    visit browse_path

    expect(page).to have_case_insensitive_content 'Startups Currently Fundraising'

    step 'verifying the campaign swatch' do
      within '.currently-raising.reg_d .offering-card', text: offering.company.name do
        expect(page).to have_case_insensitive_content offering.tagline
        expect(page).to_not have_case_insensitive_content '45 days left'
      end
    end

    step 'Viewing a campaign offering' do
      visit offering_path(offering)
    end

    step 'campaign has open graph meta tags' do
      expect(page).to have_selector :xpath, '//head/meta[@property="twitter:image"]', visible: false
    end

    step 'campaign does not have a video' do
      expect(page).not_to have_css('.play-btn')
    end

    step 'verifying basic info' do
      expect(page).to have_case_insensitive_content 'FooBiz'
      expect(page).to have_case_insensitive_content "It's the facebook of cosmetic surgery"
      expect(page).to have_case_insensitive_content 'www.subdomain.example.com'
      expect(page).to have_case_insensitive_content 'Santa Monica, CA'

    end

    step 'verifying investment info' do
      unless offering.reg_c_enabled?
        expect(page).to have_case_insensitive_content I18n.t('browse.discovery.campaign_swatch.offering.minimum_total_investment_display') + ' $10 million'
        expect(page).to have_case_insensitive_content I18n.t('show_campaign.offering_information.max_equity_offered.title') + ' 33.33%'
        expect(page).to have_case_insensitive_content I18n.t('show_campaign.offering_information.pre_money_valuation') + ' $50 million'
        expect(page).to have_case_insensitive_content I18n.t('show_campaign.offering_information.security_offered.title') + ' ' + I18n.t('show_campaign.offering_information.security_offered.value_seed')
        expect(page).to have_link I18n.t('show_campaign.intro_to_founder')
      end
      expect(page).to have_case_insensitive_content I18n.t('show_campaign.offering_information.raised').upcase + ' $2,000,000'
      expect(page).to have_case_insensitive_content I18n.t('show_campaign.offering_information.days_left') + ' 45'
      expect(page).to have_case_insensitive_content I18n.t('show_campaign.offering_information.max_equity_offered.title') + ' 33.33%'
      expect(page).to have_case_insensitive_content I18n.t('show_campaign.offering_information.pre_money_valuation') + ' $50 million'
      expect(page).to have_case_insensitive_content I18n.t('show_campaign.offering_information.security_offered.title') + ' ' + I18n.t('show_campaign.offering_information.security_offered.value_seed')
      expect(page).to have_css "a[href$='#{new_offering_investment_path(offering.id)}']"
    end

    step 'looking at the pitch' do
       expect(page).to have_case_insensitive_content 'The top threats: Dogs Yarn CatNip'
    end

    step 'looking at the team' do
      within '.pitch-nav' do
        click_on 'Team'
      end

      expect(page).to have_css "img[src*='']"
      expect(page).to have_case_insensitive_content 'Reginald D. Member'
      expect(page).to have_case_insensitive_content 'CEO and Founder'
      expect(page).to have_case_insensitive_content 'Renaissance man. The man with the plan.'
      expect(page.html).to include('<p><p>Renaissance man. The man with the <br />plan.</p></p>')

      expect(page).to have_xpath '//a[@href="http://facebook.com/reginaldmember"]'
      expect(page).to have_xpath '//a[@href="http://linkedin.com/reginaldpro"]'
      expect(page).to have_xpath '//a[@href="http://twitter.com/therealreginaldmember"]'
    end

    step 'looking at the investors/advisors' do
      expect(page).to have_css "img[src*='']"
      expect(page).to have_case_insensitive_content 'Burt Q. Investorman'
      expect(page).to have_case_insensitive_content 'Investor'
      expect(page).to have_case_insensitive_content 'I like to swim in my pool of money!'
    end

    step 'looking at the investment document' do
      expect(page).to have_case_insensitive_content 'Executive Summary'
      r =  Regexp.new(offering.campaign_documents.first.document_new.path)
      expect(page).to have_css("a", text: /r/)
      expect(page).to have_case_insensitive_content 'Term Sheet'
      r =  Regexp.new(offering.reg_d_direct_security.deal_documents.first.document_new.path)
      expect(page).to have_css("a", text: /r/)
    end
  end

  scenario 'Viewing an uninvestable offering' do
    offering.update_attribute(:investable, false)

    visit offering_path(offering)

    expect(page).to_not have_css "a[href$='#{new_offering_investment_path(offering.id)}']"
  end

  scenario 'Viewing an offering with preview visibility' do
    offering.update_attribute(:investable, true)
    offering.visibility_preview!
    offering.update_attribute(:uuid, 'XXXX')

    visit browse_path

    expect(page).not_to have_case_insensitive_content I18n.t('browse.discovery.open_title')
    expect(page).not_to have_css "a[href$='#{new_offering_investment_path(offering.id)}']"

    visit offering_path(offering)
    expect(page).to have_case_insensitive_content("page not found")

    visit offering_path(offering, :preview => offering.uuid)
    expect(page).to have_case_insensitive_content("FooBiz")
  end

  scenario 'Viewing an SPV offering' do
    offering.visibility_public!
    offering.update_attribute(:investable, true)
    offering.other_security.update_attributes(type: 'Securities::FspStock', is_spv: true)

    visit offering_path(offering)
    click_on 'Fundraising'

    expect(page).to have_case_insensitive_content 'LLC Agreement'
    r =  Regexp.new(offering.other_security.deal_documents.first.document_new.path)
    expect(page).to have_css("a", text: /r/)
  end

  scenario 'Viewing an expiring offering' do
    offering.visibility_public!
    offering.update_attribute(:investable, true)

    offering.update_attribute(:ends_at, DateTime.yesterday.midnight.advance(hours: 24, seconds: -1))
    offering.update_attribute(:closed_status, :successful)
    Timecop.freeze do
      visit offering_path(offering)
      expect(page).to have_case_insensitive_content("Successfully Funded")
    end

    offering.update_attribute(:ends_at, 2.seconds.from_now)
    Timecop.freeze do
      visit offering_path(offering)
      expect(page).to have_case_insensitive_content("Days Left 0")
    end

    offering.update_attribute(:ends_at, 2.seconds.ago)
    offering.update_attribute(:closed_status, :pending)
    Timecop.freeze do
      visit offering_path(offering)
      expect(page).to have_case_insensitive_content("Pending")
    end

    offering.update_attribute(:ends_at, 2.seconds.ago)
    offering.update_attribute(:closed_status, :successful)
    Timecop.freeze do
      visit offering_path(offering)
      expect(page).to have_case_insensitive_content("Funded")
    end

    offering.update_attribute(:ends_at, 2.seconds.ago)
    offering.update_attribute(:closed_status, :unsuccessful)
    Timecop.freeze do
      visit offering_path(offering)
      expect(page).to have_case_insensitive_content("Closed")
    end
  end

  scenario 'viewing an offering in various states of completion' do
    offering.update_attribute(:ends_at, 2.days.ago)
    offering.update_attribute(:closed_status, :successful)
    offering.reg_d_direct_security.update_attribute(:external_investment_amount, 12000000)

    visit offering_path(offering)
    expect(page).to have_case_insensitive_content("Successfully Funded")

    offering.update_attribute(:ends_at, 2.days.ago)
    offering.update_attribute(:closed_status, :pending)

    visit offering_path(offering)
    expect(page).to have_case_insensitive_content("Pending Closing Since")
  end
end
