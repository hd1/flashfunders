require 'rails_helper'

feature 'View the homepage as a logged-out user' do
  before do
    create_complete_open_campaign
    create_funded_startups
  end

  scenario 'who can see the flashfunders homepage' do
    visit root_path

    expect(page).to have_content(I18n.t('home.title'))
    expect(page).to have_link(I18n.t('home.start_invest'), href: browse_path)

    expect(page).to have_case_insensitive_content 'FooBiz'
    expect(page).to_not have_case_insensitive_content 'Earthquakes R Us'
    expect(page).to_not have_selector :xpath, '//head/meta[@property="twitter:image"]', visible: false
    expect(page).to have_selector :xpath, '//head/meta[@property="og:url"]', visible: false
  end

  scenario 'who cannot subscribe to our newsletter with a bad email' do
    visit root_path
    within '#home_new_subscription' do
      fill_in 'subscription[email]', with: 'bad_email'
      click_on 'Subscribe'
    end
    # TODO: uncomment when error state is implemented.
    # expect(page).to have_content 'Provide a valid email address.'
  end

  scenario 'who can subscribe to our newsletter with a good email' do
    visit root_path
    within '#home_new_subscription' do
      fill_in 'subscription[email]', with: 'valid@email.com'
      click_on 'Subscribe'
    end
    expect(page).to have_content I18n.t('subscription.insider_success')
  end

  scenario 'who does not see the sign-in continue banner' do
    visit root_path
    click_on 'Sign In'
    expect(page).to_not have_case_insensitive_content 'Sign In or Create Account to continue.'
  end
end

feature 'Visit the homepage as a logged-in user' do

  let!(:user) { create_user(email: 'foo@example.com', password: 'password', password_confirmation: 'password') }

  before do
    create_complete_open_campaign
    create_funded_startups
    sign_in_user(user)
  end

  scenario 'who should not see the subscription form' do
    visit root_path
    expect(page).to_not have_selector('.subscription-container')
  end
end

feature 'Visit the home page to see an offering with various ends_at values' do
  let!(:campaign) { create_complete_open_campaign }
  before { create_funded_startups }

  scenario 'offering is still open with a day to go', js: true do
    campaign.update_attribute(:ends_at, 1.days.from_now)
    Timecop.freeze do
      visit root_path
      expect(page).to have_case_insensitive_content('1 Day Left')
    end
  end

  scenario 'offering is still open with less than a day to go', js: true do
    campaign.update_attribute(:ends_at, 2.seconds.from_now)
    Timecop.freeze do
      visit root_path
      expect(page).to have_case_insensitive_content('0 Days Left')
    end
  end

end


def create_funded_startups
  funded_startups = ['The Influential Network', 'MobileSpike', 'Cartogram']

  funded_startups.each do |company_name|
    offering = create_complete_closed_campaign(email: "#{company_name.parameterize('_')}@offering.com")
    offering.company.update_attribute(:name, company_name)
  end
end
