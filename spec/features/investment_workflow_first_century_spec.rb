require 'rails_helper'

feature 'User invests in a first_century offering', :js do
  let!(:offering) { FactoryGirl.create(:offering, :first_century, escrow_end_date: 10.days.from_now) }
  let!(:user) { FactoryGirl.create(:user, email: 'investor@example.com', password: 'password', confirmed_at: 1.day.ago) }
  let!(:routing_number) { RoutingNumber.create! routing_number: '123456789' }

  before do
    allow(SecureRandom).to receive(:uuid).and_return('586c0441-a0f1-49c6-b2e0-a9d4d65d8376')
  end

  scenario 'as a previous investor' do
    step 'login to account' do
      log_in_with_credentials('investor@example.com', 'password')
    end

    step 'choose an offering' do
      visit offering_path(offering)

      within '.offering-head' do
        click_on I18n.t('investor_investments.invest.start')
      end
    end

    step 'invest in an offering' do
      submit_investment(8000)
    end

    step 'choose investment method' do
      invest_as_individual
    end

    step 'provide accreditation information' do
      submit_third_party_net_worth('Licensed Attorney', 'lawyer@example.com')
    end

    step 'agree to documents' do
      agree_to_and_sign_documents
    end

    step 'get check instructions' do
      choose_to_send_check
    end

    step 'view wire transfer success page' do
      expect(page).to have_case_insensitive_content I18n.t('investor_transfer_investments.success.title')
      expect(page).to have_case_insensitive_content I18n.t("investment_dashboard.my_investments_tab.statuses.pending.title")
      expect(page).to have_case_insensitive_content I18n.t('investment_dashboard.my_investments_tab.check.view_title')
      expect(page).to have_link I18n.t('investment_documents.edit_button')

      click_on I18n.t('investment_dashboard.my_investments_tab.tab_name')
    end

    step 'view successful investment' do
      expect(page).to have_css('.investment', count: 1)
      expect(page).to have_content(offering.company.name)
    end
  end

end
