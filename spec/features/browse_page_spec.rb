require 'rails_helper'

feature 'Offering Page' do
  let!(:campaign) do
    create_complete_open_campaign.tap do |new_campaign|
      new_campaign.update_attributes(vanity_path: vanity_url)
    end
  end

  before(:each) do
    # TODO: This should not be necessary, but some of the assertions fail with the new offering page
    Capybara.use_default_driver
  end

  context 'with a vanity URL' do
    let(:vanity_url) { 'foo-biz-path' }
    scenario 'Browsing for and viewing a campaign' do
      visit browse_path
      step 'See the vanity URL' do
        within '.currently-raising.reg_d .offering-card', text: 'FooBiz' do
          expect(page).to have_link(campaign.company.name, href: '/foo-biz-path')
          click_link(campaign.company.name)
        end
      end
      step 'See the offerings pitch tab' do
        expect(page).to have_css('#nav-pitch.active')
      end
    end
  end

  context 'without a vanity URL' do
    let(:vanity_url) { '' }
    scenario 'Browsing for and viewing a campaign' do
      visit browse_path
      step 'See the ID-based URL' do
        within '.currently-raising.reg_d .offering-card', text: 'FooBiz' do
          expect(page).to have_link(campaign.company.name, href: "/offering/#{campaign.id}")
          click_link(campaign.company.name)
        end
      end
      step 'See the offerings pitch tab' do
        expect(page).to have_css('#nav-pitch.active')
      end
    end
  end

end
