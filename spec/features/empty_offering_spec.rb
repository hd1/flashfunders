require 'rails_helper'

feature 'Offering Page' do
  let!(:campaign) { create(:empty_offering) }

  before do
    FactoryGirl.create(:cf_stock, offering: campaign)
  end

  scenario 'Browsing for and viewing an empty campaign' do
    visit browse_path
    expect(page).to have_case_insensitive_content "Startups Currently Fundraising"

    step 'verifying the campaign swatch' do
      within '.currently-raising.reg_cf .offering-card', text: 'N/A' do
        expect(page).to have_case_insensitive_content "N/A"
      end
    end

    step 'Viewing a campaign offering' do
      visit offering_path(campaign)
    end

    step 'verifying basic info' do
      expect(page).to have_case_insensitive_content 'N/A'
    end
  end
end
