require 'rails_helper'

feature 'Logged Out User', :js do
  before do
    allow(InvestorMailer).to receive_message_chain(:checking_in, :deliver_later)
  end

  scenario 'As a user I can sign up, confirm my account, and sign in' do
    visit new_user_registration_path

    step 'creating account' do
      create_account('John Q User', 'user@example.com', 'secretpassword', false, false)

      expect(page).to_not have_content 'user@example.com'

      expect do
        within '.panel' do
          click_on I18n.t('sign_up.form_section.commit')
        end
        submit_email_consent

        expect(current_path).to eq(root_path)
      end.to change(Devise::Async::Backend::Sidekiq.jobs, :size).by(1)
    end

    step 'confirming email' do
      pending 'Confirmations temporarily suspended'
      confirm_user(User.find_by_email('user@example.com'))

      expect(current_path).to eq(new_user_session_path)
      expect(page).to have_case_insensitive_content(I18n.t('devise.confirmations.confirmed'))
    end

    step 'signing in' do
      click_on I18n.t('nav.main_links.sign_in')
      click_button I18n.t('sign_in.form_section.commit')
      expect(page).to have_content(I18n.t('devise.failure.invalid'))

      fill_in 'user_email', with: 'user@example.com', match: :first
      fill_in 'user_password', with: 'secretpassword', match: :first

      click_button I18n.t('sign_in.form_section.commit'), match: :first

      expect(current_path).to eq(get_started_path)
    end
  end

  scenario 'Trying to confirm an email with a bad token' do
    pending 'Confirmations temporarily suspended'
    create_user(email: 'user@example.com')

    visit user_confirmation_path(token: '123')

    expect(current_path).to eq(user_confirmation_path)
    expect(page).to have_content(I18n.t('recoverable.resend_confirmation.title'))
  end

  context 'Using the Registration page' do
    scenario 'Signing up with existing email address complains but changing to a new email address redirects to the new_user_confirmation_path after success' do
      pending 'Confirmations temporarily suspended'
      create_user(email: 'user@example.com')

      visit new_user_registration_path

      create_account('John Username', 'user@example.com', 'secretpassword')
      expect(page).to have_content('has already been taken')

      fill_in 'user_email', with: 'new_user@example.com', match: :first
      fill_in 'user_password', with: 'secretpassword', match: :first
      fill_in 'user_password_confirmation', with: 'secretpassword', match: :first

      within '.panel' do
        click_on I18n.t('sign_up.form_section.commit')
      end

      expect(current_path).to eq(new_user_confirmation_path)
    end
  end

  context 'Using the Registration modal dialog' do
    scenario 'Signing up' do

      step 'with existing email address complains' do
        create_user(email: 'user2@example.com')

        visit root_path

        within('.primary-nav.js-nav') do
          click_on I18n.t('nav.main_links.sign_up')
        end

        create_account_modal('John Username', 'user2@example.com', 'secretpassword')

        expect(page).to have_content('has already been taken')
      end

      step 'changing to a new email address shows the confirmation modal (Use Case: #4)' do
        fill_in 'user_email', with: 'new_user3@example.com', match: :first
        fill_in 'user_password', with: 'secretpassword', match: :first
        fill_in 'user_password_confirmation', with: 'secretpassword', match: :first

        click_on I18n.t('sign_up.form_section.commit')

        expect(current_path).to eq(root_path)
        # expect(page).to have_css('#confirmation-modal', visible: true)
        # expect(page).to have_css('#new-registration-modal', visible: false)
        # within('#confirmation-modal') do
        #   expect(page).to have_content(I18n.t('devise.registrations.signed_up_but_unconfirmed'))
        # end
      end
    end

    scenario 'As an unregistered user I can sign up and see the confirmation modal (Use Case: #9)' do
      pending 'Confirmations temporarily suspended'
      create_temporary_user(email: 'new_user25@temporary.com')
      visit root_path
      within('.primary-nav.js-nav') do
        click_on I18n.t('nav.main_links.sign_up')
      end

      create_account_modal('John Username', 'new_user25@temporary.com', 'secretpassword')

      expect(page).to have_css('#confirmation-modal', visible: true)
      expect(page).to have_css('#new-registration-modal', visible: false)
      within('#confirmation-modal') do
        expect(page).to have_content(I18n.t('devise.registrations.signed_up_but_unconfirmed'))
      end
    end

  end
end
