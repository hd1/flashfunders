require 'rails_helper'

feature 'Logging in' do
  let!(:user) { create_user(email: 'user@example.com', password: 'thepassword', password_confirmation: 'thepassword') }
  let(:partially_registered_user) { create_temporary_user(email: 'user@partial.com') }
  let(:unconfirmed_user) { create_unconfirmed_user(email: 'user@unconfirmed.com', password: 'password', password_confirmation: 'password') }

  after { Warden.test_reset! }

  context 'from the home controller' do
    it 'takes the user to the home page', js: true do
      visit('/')
      expect(page).to_not have_content('Email')
      expect(page).to_not have_content('Password')

      click_on I18n.t('nav.main_links.sign_in')

      fill_in 'user_email', with: 'user@example.com', match: :first
      fill_in 'user_password', with: 'thepassword', match: :first

      click_button I18n.t('nav.main_links.sign_in'), match: :first

      expect(current_path).to eq(root_path)
    end
  end

  context 'when trying to hit a page other than the home page' do
    it 'takes the user to the page they were trying to hit after they log in' do
      visit(dashboard_conversations_path)

      fill_in 'user_email', with: 'user@example.com', match: :first
      fill_in 'user_password', with: 'thepassword', match: :first

      click_button I18n.t('nav.main_links.sign_in'), match: :first

      expect(current_path).to eq(dashboard_conversations_path)
    end
  end

  context 'when user goes directly to sign in path' do
    it 'takes the user to the home page after they login' do
      visit(new_user_session_path)
      fill_in 'user_email', with: 'user@example.com', match: :first
      fill_in 'user_password', with: 'thepassword', match: :first

      click_button I18n.t('nav.main_links.sign_in'), match: :first

      expect(current_path).to eq(root_path)
    end
  end

  context 'a partially registered user tries to sign in (Use Case: #10)' do
    context 'via the sign in page' do
      it 'and is redirected to the user registration page' do
        visit new_user_session_path

        fill_in 'user_email', with: partially_registered_user.email
        fill_in 'user_password', with: 'thepassword'

        click_button I18n.t('nav.main_links.sign_in')

        expect(current_path).to eq(new_user_registration_path)

        user_registration_name = find_by_id('user_registration_name').value
        user_registration_email = find_by_id('user_email').value
        expect(user_registration_name).to eq(partially_registered_user.registration_name)
        expect(user_registration_email).to eq(partially_registered_user.email)
      end
    end

    context 'via the sign in modal' do
      it 'and is redirected to the confirmation page' do
        visit root_path

        click_link I18n.t('nav.main_links.sign_in')

        expect(page).to have_css('#new-session-modal', visible: true)

        fill_in 'user_email', with: partially_registered_user.email
        fill_in 'user_password', with: 'thepassword'

        click_button I18n.t('nav.main_links.sign_in')

        expect(current_path).to eq(root_path)
        expect(page).to have_css('#new-session-modal', visible: false)
        expect(page).to have_css('#new-registration-modal', visible: true)

        user_registration_name = find_by_id('user_registration_name').value
        user_registration_email = find_by_id('user_email').value
        expect(user_registration_name).to eq(partially_registered_user.registration_name)
        expect(user_registration_email).to eq(partially_registered_user.email)
      end
    end
  end

  context 'an unconfirmed user tries to sign in (Use Case: #15)' do
    context 'via the sign in page' do
      it 'and is redirected to the confirmation page' do
        visit new_user_session_path

        fill_in 'user_email', with: unconfirmed_user.email
        fill_in 'user_password', with: unconfirmed_user.password

        click_button I18n.t('nav.main_links.sign_in')

        expect(current_path).to eq(new_user_confirmation_path)
        expect(page).to have_content(I18n.t('devise.confirmations.user.fully_registered'))
      end
    end

    context 'via the sign in modal' do
      it 'and is redirected to the confirmation page' do
        visit root_path

        click_link I18n.t('nav.main_links.sign_in')

        expect(page).to have_css('#new-session-modal', visible: true)

        fill_in 'user_email', with: unconfirmed_user.email
        fill_in 'user_password', with: unconfirmed_user.password

        click_button I18n.t('nav.main_links.sign_in')

        expect(current_path).to eq(root_path)
        expect(page).to have_css('#new-session-modal', visible: false)
        expect(page).to have_css('#confirmation-modal', visible: true)
        within('#confirmation-modal') do
          expect(page).to have_content(I18n.t('devise.confirmations.user.fully_registered'))
        end
      end
    end
  end

end
