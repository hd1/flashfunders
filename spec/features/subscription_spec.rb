require 'rails_helper'

feature 'Visitor subscribes to newsletter', js: true do

  scenario 'with valid email address' do
    visit subscribe_path

    within '#email_new_subscription' do
      fill_in 'subscription_email', with: 'foo@example.com'
      click_on 'Subscribe'
    end

    expect(page).to have_content(I18n.t('subscription.insider_success'))
  end

  scenario 'with invalid email' do
    visit subscribe_path

    within '#email_new_subscription' do
      fill_in 'subscription_email', with: 'invalid'
      click_on 'Subscribe'
    end

    expect(page).to have_content(I18n.t('subscription.error.email_not_valid'))
  end

end
