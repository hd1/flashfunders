require 'rails_helper'

feature 'Email service redirects to unsubscribe' do
  it 'and the page be there' do
    visit unsubscribe_path
    expect(page).to have_case_insensitive_content(I18n.t('unsubscribe.title'))

    click_on I18n.t('unsubscribe.return_link')
    expect(current_path).to eq(root_path)
  end
end
