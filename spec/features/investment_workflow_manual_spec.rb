require 'rails_helper'

feature 'User invests in a manual offering', :js do
  let!(:offering) {
    o = FactoryGirl.create(:offering, escrow_end_date: 10.days.from_now)
    o.securities.each { |s| s.update_attribute :escrow_provider, FactoryGirl.create(:escrow_provider_manual)}
    o
  }
  let!(:user) { FactoryGirl.create(:user, email: 'investor@example.com', password: 'password', confirmed_at: 2.years.ago) }

  before do
    offering.reg_d_securities.first.update_attribute(:minimum_investment_amount, 25000)
    RoutingNumber.create! routing_number: '123456789'
  end

  scenario 'as a previous investor' do
    step 'login to account' do
      log_in_with_credentials('investor@example.com', 'password')
    end

    step 'choose an offering' do
      visit offering_path(offering)

      within '.offering-head' do
        click_on I18n.t('investor_investments.invest.start')
      end
    end

    step 'invest in an offering' do
      submit_investment(8000)
    end

    step 'choose investment method' do
      invest_as_individual
    end

    step 'provide accreditation information' do
      submit_third_party_net_worth('Licensed Attorney', 'lawyer@example.com')
    end

    step 'agree to documents' do

      expect(page.current_path).to eq(offering_invest_investment_documents_path(offering))

      within('#amount') do
        expect(page).to have_content('$8,000.00')
      end

      within('#verify') do
        expect(page).to have_content("Licensed Attorney")
        expect(page).to have_content('lawyer@example.com')
      end

      agree_to_and_sign_documents
    end

    step 'link refund account' do
      expect(page).to have_content(I18n.t('fund.success.notice.manual'))

      fill_in_link_refund_account
      click_button I18n.t('fund.buttons.commit')

    end

    step 'view sucessful investment' do
      expect(page).to have_case_insensitive_content(I18n.t('investor_transfer_investments.success.title'))

      click_on I18n.t('investment_dashboard.my_investments_tab.tab_name')

      expect(page).to have_css('.investment', count: 1)
      expect(page).to have_content(offering.company.name)
    end
  end

end
