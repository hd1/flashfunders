require 'rails_helper'

feature 'View the terms of use' do
  before do
    create_complete_open_campaign
    create_complete_closed_campaign
  end

  scenario 'a logged out user can see the terms of use' do
    visit terms_path

    expect(page).to have_case_insensitive_content 'TERMS OF USE'
  end

  scenario 'a logged out user can see the privacy policy' do
    visit privacy_path

    expect(page).to have_case_insensitive_content 'PRIVACY POLICY'
  end

  scenario 'a logged out user can see the email disclaimer' do
    visit email_disclaimer_path

    expect(page).to have_case_insensitive_content 'IMPORTANT NOTICE TO RECIPIENTS'
  end

  scenario 'everyone can visit the /faq' do
    visit faq_path

    expect(page).to have_case_insensitive_content(I18n.t('faq_investor.title'))
  end

  scenario 'everyone can visit the /team' do
    visit team_path

    expect(page.body).to include(I18n.t('team.subtitle'))
  end
end
