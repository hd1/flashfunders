require 'rails_helper'

feature 'Issuer Application', :js do
  let(:confirmed_user) { create_user(email: 'joe@entrepreneur.com', registration_name: 'Joe Entrepreneur', user_type: :user_type_entrepreneur) }
  let(:partially_registered_user) { create_temporary_user(email: 'joe+partial@entrepreneur.com', registration_name: 'Joe Entrepreneur', user_type: :user_type_entrepreneur, confirmed_at: Time.now.to_s) }
  let(:unconfirmed_user) { create_unconfirmed_user(email: 'unconfirmed@entrepreneur.com', registration_name: 'Unconfirmed Entrepreneur') }

  scenario 'An unregistered user' do
    step 'creates an account' do
      visit new_user_registration_path

      create_account('John Q User', 'user@example.com', 'secretpassword')
      submit_email_consent
      expect(current_path).to eq(root_path)
    end

    step 'confirms their account' do
      pending 'Confirmations temporarily suspended'
      confirm_user(User.last)

      expect(current_path).to eq(new_user_session_path)
    end

    step 'signs in' do
      fill_in 'user_email', with: 'user@example.com'
      fill_in 'user_password', with: 'secretpassword'

      click_on I18n.t('sign_in.form_section.commit')

      expect(current_path).to eq(get_started_path)
    end

    step 'fills out an issuer application' do
      visit new_issuer_applications_path

      fill_out_issuer_application

      expect(current_path).to eq(success_issuer_applications_path)
      check_user_type('user@example.com')
    end

    step 'and signs out' do
      visit destroy_user_session_path
    end
  end

  scenario 'An unregistered user' do

    step 'incorrectly fills out an app' do
      email = 'joeentrepreneur.com'

      visit new_issuer_applications_path

      expect(User.find_by_email(email)).to be(nil)

      fill_out_issuer_application('New Issuer', email)

      expect(current_path).to eq(issuer_applications_path)
      expect(User.find_by_email(email)).to be(nil)
    end

    step 'correctly fills out an app' do
      email = 'joe+unregistered@entrepreneur.com'

      visit new_issuer_applications_path

      expect(User.find_by_email(email)).to be(nil)

      fill_out_issuer_application('New Issuer', email)
      expect(current_path).to eq(success_issuer_applications_path)
      expect(User.find_by_email(email)).to_not be(nil)
    end
  end

  scenario 'A partially registered user (Use Case: #6)' do
    step 'fills out an app' do
      visit new_issuer_applications_path
      fill_out_issuer_application(partially_registered_user.registration_name, partially_registered_user.email)
    end

    # TODO: Replace completes their registration step with this one when we reenable confirmations
    # step 'should be asked to confirm their account' do
    #   expect(current_path).to eq(new_user_confirmation_path)
    #   expect(page).to have_content(I18n.t('devise.confirmations.user.issuer_applications.partially_registered'))
    #
    #   expect(find_application(partially_registered_user.id, 'Foo Inc.')).to be(nil)
    #   partially_registered_user.reload
    #   expect(partially_registered_user.context['recreate_params']).to_not be(nil)
    # end

    # step 'confirms their account' do
    #   confirm_user(partially_registered_user)
    #
    #   confirm_account('secretpassword', true, 'United States')
    # end

    step 'completes their registration' do
      expect(current_path).to eq(new_user_registration_path)
      fill_in 'user_password', with: 'secretpassword', match: :first
      fill_in 'user_password_confirmation', with: 'secretpassword', match: :first

      within '.panel' do
        click_on I18n.t('sign_up.form_section.commit')
      end
    end

    step 'should see success page' do
      expect(current_path).to eq(success_issuer_applications_path)
      expect(find_application(partially_registered_user.id, 'Foo Inc.')).to_not be(nil) # application should be created
    end

  end

  scenario 'An unconfirmed user (Use Case: #13)' do
    step 'fills out an issuer application' do
      visit new_issuer_applications_path

      fill_out_issuer_application(unconfirmed_user.registration_name, unconfirmed_user.email)

      expect(current_path).to eq(new_user_confirmation_path)
      expect(page).to have_content(I18n.t('devise.confirmations.user.issuer_applications.fully_registered'))
    end

    step 'confirms their account' do
      confirm_user(unconfirmed_user)

      expect(current_path).to eq(new_user_session_path)
    end

    step 'signs in and application is submitted' do

      fill_in 'user_email', with: unconfirmed_user.email
      fill_in 'user_password', with: 'thepassword'

      within '.panel' do
        click_on I18n.t('sign_in.form_section.commit')
      end

      expect(current_path).to eq(success_issuer_applications_path)
    end

    step 'and signs out' do
      visit destroy_user_session_path
    end
  end

  scenario 'A confirmed user who is not signed in' do
    step 'fills out application' do
      visit new_issuer_applications_path
      fill_out_issuer_application(confirmed_user.registration_name, confirmed_user.email)

      expect(current_path).to eq(new_user_session_path)
      expect(find_application(confirmed_user.id, 'Foo Inc.')).to be(nil) # application shouldn't be created yet
    end

    step 'and signs in' do
      fill_in 'user_email', with: confirmed_user.email
      fill_in 'user_password', with: confirmed_user.password
      within '.panel' do
        click_on I18n.t('sign_in.form_section.commit')
      end
    end

    step 'should see success page' do
      expect(current_path).to eq(success_issuer_applications_path)
      expect(find_application(confirmed_user.id, 'Foo Inc.')).to_not be(nil) # application should be created
    end
  end

end

def find_application(user_id, company_name)
  IssuerApplication.find_by(user_id: user_id, company_name: company_name)
end

def fill_out_issuer_application(name=nil, email=nil)
  # Founder Info
  if name.present? && email.present?
    # Founder Info
    within '#new_issuer_application_form' do
      fill_in I18n.t('issuer_application.new.founder.name'), with: name
      fill_in I18n.t('issuer_application.new.founder.email'), with: email
    end
  end
  fill_in I18n.t('issuer_application.new.founder.phone'), with: '123-456-7890'
  # Company Info
  fill_in I18n.t('issuer_application.new.company.name'), with: 'Foo Inc.'
  fill_in I18n.t('issuer_application.new.company.website'), with: 'http://foo-inc.com/'
  select IssuerApplication::ENTITY_TYPES.first.first, from: I18n.t('issuer_application.new.company.entity')
  # Campaign Info
  fill_in I18n.t('issuer_application.new.details.raise_amount'), with: '10'
  find(:css, '.issuer_application_form_company_criteria .checkbox:nth-child(2)').click
  find(:css, '.issuer_application_form_company_criteria .checkbox:nth-child(4)').click
  select IssuerApplication::NEW_MONEY_COMMITTED_RANGES.first.first, from: I18n.t('issuer_application.new.details.new_money_committed')
  select IssuerApplication::NUM_USERS.first.first, from: I18n.t('issuer_application.new.details.num_users')
  select IssuerApplication::PROMOTION_BUDGET.first.first, from: I18n.t('issuer_application.new.details.budget')
  select IssuerApplication::LEAD_SOURCES.first.first, from: I18n.t('issuer_application.new.details.lead_source')

  click_on I18n.t('issuer_application.new.button')
end

def check_user_type(email)
  user = User.find_by_email(email)
  expect(user).to_not be(nil)
  expect(user.user_type).to eq('user_type_entrepreneur')
end
