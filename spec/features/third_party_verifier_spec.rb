require 'rails_helper'

feature 'Third party completes request for investor verification' do
  before(:each) do
    # TODO: This should not be necessary, but some of the assertions fail with the javascript driver
    Capybara.use_default_driver
  end

  let!(:user) { create_user }
  let!(:role) { Accreditor::Qualification::VALID_ROLE_TYPES.first }
  let!(:investor_entity) { FactoryGirl.create(:investor_entity) }
  let!(:personal_information) { PersonalFederalIdentificationInformation.new({ investor_entity_id: investor_entity.id, first_name: 'Ian', last_name: 'Investor' }) }
  let!(:qualification) { Accreditor::ThirdPartyIncomeQualification.create(role: role, user_id: user.id, email: 'larry@example.com', investor_entity_id: investor_entity.id) }

  context 'As an individual' do
    let!(:investor_entity) { InvestorEntity::Individual.create!(user_id: user.id) }

    scenario 'by confirming the investor accreditation' do
      fill_in_and_submit_form

      expect(page).to have_case_insensitive_content I18n.t('investor_accreditation.on_completion.success.title')
    end

    scenario 'by declining to sign' do
      decline_to_sign

      expect(page).to have_case_insensitive_content I18n.t('investor_accreditation.on_completion.decline.title')
    end

    scenario 'by submitting incomplete information' do
      submit_incomplete_form

      expect(page).to have_case_insensitive_content I18n.t('third_party_verification.error.top_message')
    end

    scenario 'when the form has already been submitted' do
      fill_in_and_submit_form
      visit edit_third_party_qualification_path(qualification)

      expect(page).to have_case_insensitive_content I18n.t('investor_accreditation.on_completion.success.title')
    end

    scenario 'when logged in as the same investor being verified' do
      sign_in_user(user)

      visit edit_third_party_qualification_path(qualification)

      expect(field_labeled(I18n.t('third_party_verification.about_you.name'), disabled: true)[:disabled]).to eq "disabled"
      expect(field_labeled(I18n.t('third_party_verification.about_you.role'), disabled: true)[:disabled]).to eq "disabled"
      expect(field_labeled(I18n.t('third_party_verification.about_you.firm'), disabled: true)[:disabled]).to eq "disabled"
      expect(field_labeled(I18n.t('third_party_verification.about_you.license'), disabled: true)[:disabled]).to eq "disabled"
      expect(field_labeled(I18n.t('third_party_verification.about_you.license_state'), disabled: true)[:disabled]).to eq "disabled"
      expect(field_labeled(I18n.t('third_party_verification.contact_information.phone'), disabled: true)[:disabled]).to eq "disabled"
      expect(field_labeled(I18n.t('third_party_verification.contact_information.email'), disabled: true)[:disabled]).to eq "disabled"
      expect(field_labeled(I18n.t('third_party_verification.review_statement.signature'), disabled: true)[:disabled]).to eq "disabled"
    end
  end

  context 'As an LLC' do
    let!(:investor_entity) { InvestorEntity::LLC.create!(user_id: user.id) }

    scenario 'by confirming the investor accreditation' do
      fill_in_and_submit_llc_form

      expect(page).to have_case_insensitive_content I18n.t('investor_accreditation.on_completion.success.title')
    end

    scenario 'by not checking a criteria option' do
      fill_in_and_submit_form

      expect(page).to have_case_insensitive_content I18n.t('third_party_verification.error.top_message')
    end
  end

  def fill_in_and_submit_form
    fill_in_form

    click_button I18n.t('third_party_verification.review_statement.submit')
  end

  def fill_in_and_submit_llc_form
    fill_in_form
    check I18n.t('third_party_verification.criteria.total_assets')

    click_button I18n.t('third_party_verification.review_statement.submit')
  end

  def fill_in_form
    visit edit_third_party_qualification_path(qualification)

    fill_in I18n.t('third_party_verification.about_you.name'), with: 'Larry Lawyer', match: :first
    fill_in I18n.t('third_party_verification.about_you.firm'), with: 'Big Firm, LLC.'
    fill_in I18n.t('third_party_verification.about_you.license'), with: 'License123'
    fill_in I18n.t('third_party_verification.contact_information.phone'), with: '555-555-5555'
    fill_in I18n.t('third_party_verification.review_statement.signature'), with: 'Larry Lawyer'

    select role, from: I18n.t('third_party_verification.about_you.role')
    select 'Alaska', from: I18n.t('third_party_verification.about_you.license_state')
  end

  def submit_incomplete_form
    visit edit_third_party_qualification_path(qualification)

    click_button I18n.t('third_party_verification.review_statement.submit')
  end

  def decline_to_sign
    visit edit_third_party_qualification_path(qualification)

    click_on I18n.t('third_party_verification.review_statement.decline')
  end
end
