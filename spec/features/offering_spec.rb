require 'rails_helper'

feature 'Offering', js: true do
  let!(:offering) { create_complete_open_campaign }
  let!(:user) { create_user(email: 'foo@example.com', password: 'password', password_confirmation: 'password') }

  feature 'harmful html tags' do
    scenario 'should not be displayed when the text_area content is harmful' do
      offering.pitch_sections.last.text_areas.last.update_attributes( body: harmful_tags, html: false )
      visit offering_path(offering)
      expect(page).to_not have_content(harmful_tags)
    end

    scenario 'should not be displayed when the text_area content type is html' do
      offering.pitch_sections.last.text_areas.last.update_attributes( body: harmful_tags + 'Some content', html: true )
      visit offering_path(offering)
      expect(page).to_not have_content(harmful_tags)
      expect(page).to have_content('Some content')
    end
  end

  feature 'Security Type' do
    scenario 'should display FlashSeed Preferred where security type is FlashSeed Preferred' do
      sign_in_user(user)
      visit offering_path(offering)
      click_link("Fundraising")
      expect(page).to have_content("FlashSeed Preferred")
    end

    scenario 'should display Offering#security_type_custom where security type is custom' do
      offering.reg_d_direct_security.update_attributes(type: 'Securities::CustomStock', security_title: 'Preferred Stock')

      sign_in_user(user)
      visit offering_path(offering)
      click_link("Fundraising")
      expect(page).to have_content("Preferred Stock")
    end
  end

  scenario 'Logged-out user who visits Offering#show with modal params should see the ask_question_modal' do
    visit offering_path(offering, m: 'intro-to-founder-modal')
    expect(page).to have_css('#intro-to-founder-modal')
  end

  scenario 'Logged-in user who visits Offering#show with modal params should see the ask_question_modal' do
    sign_in_user(user)
    visit offering_path(offering, m: 'intro-to-founder-modal')
    expect(page).to have_css('#intro-to-founder-modal')
  end

  def harmful_tags
    '<applet></applet><style></style><link></link><iframe></iframe><object></object><embed></embed><form></form>'
  end

end
