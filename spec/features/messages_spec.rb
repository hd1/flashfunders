require 'rails_helper'

feature 'User views messages', :stub_warden, :js do

  let!(:issuer) do
    user = create_user(email: 'issuer@example.com', registration_name: 'Fred Issuer', password: 'thepassword', password_confirmation: 'thepassword')
    PersonalFederalIdentificationInformation.create(user_id: user.id, first_name: 'Fred', last_name: 'Issuer')
    user
  end

  let!(:investor_1) do
    user_2 = create_user(email: 'investor_1@example.com', registration_name: 'Joe Investor', password: 'thepassword', password_confirmation: 'thepassword')
    PersonalFederalIdentificationInformation.create(user_id: user_2.id, first_name: 'Joe', last_name: 'Investor')
    user_2
  end

  let!(:investor_2) do
    user_3 = create_user(email: 'investor_2@example.com', registration_name: 'Jane Investor', password: 'thepassword', password_confirmation: 'thepassword')
    PersonalFederalIdentificationInformation.create(user_id: user_3.id, first_name: 'Jane', last_name: 'Investor')
    user_3
  end

  let!(:offering) { Offering.create(public_contact_name: 'Fake Name', issuer_user_id: issuer.id) }
  let!(:company) { Company.create name: 'Fake Company', offering_id: offering.id }
  let(:comment) { 'A comment about the startup' }

  context 'as an Issuer' do
    before do
      sign_in_user(issuer)
    end

    scenario 'with no messages' do
      visit dashboard_conversations_path

      # expect(page).to have_case_insensitive_content(company.name)
      expect(page).to have_case_insensitive_content(I18n.t('investment_dashboard.messages_tab.index.empty'))
    end

    scenario 'with messages' do
      messages = {
        first:  OfferingMessage.create!(offering_id: offering.id, user_id: investor_1.id, body: 'XOXO McSweeneys drinking vinegar.', created_at: 2.minutes.ago),
        second: OfferingMessage.create!(offering_id: offering.id, user_id: investor_2.id, body: 'Second set of text', created_at: 2.hours.ago),
        third:  OfferingMessage.create!(offering_id: offering.id, user_id: investor_2.id, body: 'Third set of text', created_at: 2.hours.ago)
      }

      messages.each { |key, message| messages[key] = MessagePresenter.new(message, current_offering: offering) }

      visit dashboard_conversations_path(offering_id: offering.id)

      expect(page).to have_case_insensitive_content(messages[:first].body)
      expect(page).to have_case_insensitive_content(investor_1.full_name)
      expect(page).to_not have_case_insensitive_content(messages[:second].body)
      expect(page).to have_case_insensitive_content(messages[:third].body)
      expect(page).to have_case_insensitive_content(investor_2.full_name)

      step 'opens a message' do

        click_on(messages[:third].body)

        expect(page).to have_case_insensitive_content("Conversation with #{investor_2.registration_name}")
        expect(page).to have_case_insensitive_content(messages[:second].body)
        expect(page).to have_case_insensitive_content(messages[:third].body)
      end
    end

    scenario 'sending a message' do
      OfferingMessage.create! sent_by_user: true, offering_id: offering.id, user_id: investor_1.id,
                              body:         'roof party craft beer mlkshk fap.', created_at: 2.minutes.ago

      visit dashboard_conversation_path(offering.id, user_id: investor_1.uuid)

      fill_in 'offering_message_body', with: comment

      click_on 'Send'

      expect(page).to have_case_insensitive_content(I18n.t('offering_message.confirm'))
      expect(page).to have_case_insensitive_content(comment)
    end

    scenario 'sending a blank message' do
      OfferingMessage.create! sent_by_user: true, offering_id: offering.id, user_id: investor_1.id,
                              body:         'roof party craft beer mlkshk fap.', created_at: 2.minutes.ago

      visit dashboard_conversation_path(offering.id, user_id: investor_1.uuid)
      fill_in 'offering_message_body', with: ''
      click_on 'Send'
      expect(page).to have_case_insensitive_content(I18n.t('offering_message.error.blank_message'))
    end

  end

  context 'as an Investor' do
    before do
      sign_in_user(investor_1)
    end

    scenario 'with no messages' do
      visit dashboard_conversations_path

      # expect(page).to have_case_insensitive_content(company.name)
      expect(page).to have_case_insensitive_content(I18n.t('investment_dashboard.messages_tab.index.empty'))
    end

    scenario 'with messages' do
      messages = {
        first:  OfferingMessage.create!(offering_id: offering.id, user_id: investor_1.id, body: 'XOXO McSweeneys drinking vinegar.', created_at: 2.minutes.ago),
        second: OfferingMessage.create!(offering_id: offering.id, user_id: investor_2.id, body: 'Second set of text', created_at: 2.hours.ago),
        third:  OfferingMessage.create!(offering_id: offering.id, user_id: investor_2.id, body: 'Third set of text', created_at: 2.hours.ago)
      }

      messages.each { |key, message| messages[key] = MessagePresenter.new(message, current_offering: offering) }

      visit dashboard_conversations_path(offering_id: offering.id)
      expect(page).to have_case_insensitive_content(messages[:first].body)
      expect(page).to have_case_insensitive_content(offering.public_contact_name)
      expect(page).to_not have_case_insensitive_content(messages[:second].body)
      expect(page).to_not have_case_insensitive_content(messages[:third].body)

      step 'opens a message' do

        click_on(messages[:first].body)

        expect(page).to have_case_insensitive_content("Conversation with #{offering.public_contact_name}")
        expect(page).to have_case_insensitive_content(messages[:first].body)
      end
    end

    scenario 'sending a message' do
      OfferingMessage.create! sent_by_user: true, offering_id: offering.id, user_id: investor_1.id,
                              body:         'roof party craft beer mlkshk fap.', created_at: 2.minutes.ago

      visit dashboard_conversation_path(offering.id, user_id: investor_1.uuid)

      fill_in 'offering_message_body', with: comment

      click_on 'Send'

      expect(page).to have_case_insensitive_content(I18n.t('offering_message.confirm'))
      expect(page).to have_case_insensitive_content(comment)
    end

    scenario 'sending a blank message' do
      OfferingMessage.create! sent_by_user: true, offering_id: offering.id, user_id: investor_1.id,
                              body:         'roof party craft beer mlkshk fap.', created_at: 2.minutes.ago

      visit dashboard_conversation_path(offering.id, user_id: investor_1.uuid)
      fill_in 'offering_message_body', with: ''
      click_on 'Send'
      expect(page).to have_case_insensitive_content(I18n.t('offering_message.error.blank_message'))
    end
  end
end
