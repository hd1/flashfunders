require 'rails_helper'

feature 'User invests in a fund_america offering', :js do
  let!(:offering) { FactoryGirl.create(:offering, :fund_america, escrow_end_date: 10.days.from_now) }
  let!(:user) { FactoryGirl.create(:user, email: 'investor@example.com', password: 'password', confirmed_at: 1.year.ago) }

  before do
    RoutingNumber.create! routing_number: '123456789'
    allow(SecureRandom).to receive(:uuid).and_return('586c0441-a0f1-49c6-b2e0-a9d4d65d8376')
  end

  scenario 'as a previous investor' do
    step 'login to account' do
      log_in_with_credentials('investor@example.com', 'password')
    end

    step 'choose an offering' do
      visit offering_path(offering)

      within '.offering-head' do
        click_on I18n.t('investor_investments.invest.start')
      end
    end

    step 'invest in an offering' do
      submit_investment(8000)
    end

    step 'choose investment method' do
      invest_as_individual
    end

    step 'provide accreditation information' do
      submit_third_party_net_worth('Licensed Attorney', 'lawyer@example.com')
    end

    VCR.use_cassette('successful_fund_america_investment') do
      step 'agree to documents' do
        agree_to_and_sign_documents
      end

      step 'provide funds transfer preferences' do
        expect(page.current_path).to eq(offering_invest_fund_path(offering))
        choose_to_send_check
      end

      step 'view successful investment' do
        expect(page).to have_case_insensitive_content(I18n.t('investor_transfer_investments.success.title'))

        click_on I18n.t('investment_dashboard.my_investments_tab.tab_name')

        expect(page).to have_css('.investment', count: 1)
        expect(page).to have_content(offering.company.name)
      end
    end
  end

end
