require 'rails_helper'

feature 'Investor verifies accreditation', :stub_warden do

  let(:upload_file_path) { File.expand_path('spec/fixtures/avatar.jpg', Rails.root) }
  let(:user) { create_user(email: 'user@example.com', password: 'thepassword', password_confirmation: 'thepassword') }
  let(:security) { FactoryGirl.create(:fsp_stock) }
  let(:security_2) { FactoryGirl.create(:fsp_stock) }
  let(:offering) { FactoryGirl.create(:offering, user: user, securities: [security], escrow_end_date: 10.days.from_now)}
  let(:offering_2) { FactoryGirl.create(:offering, user: user, securities: [security_2], escrow_end_date: 10.days.from_now)}
  let(:investment_1) { Investment.create! user_id: user.id, offering_id: offering.id,  security: security, state: 'entity_selected', investor_entity: investor_entity }
  let(:investment_2) { Investment.create! user_id: user.id, offering_id: offering_2.id, security: security_2, state: 'entity_selected', investor_entity: investor_entity }

  before do
    sign_in_user(user)
  end

  after do
    Accreditation::Document.destroy_all
  end

  context 'as an individual' do
    let(:investor_entity) { FactoryGirl.create(:investor_entity_individual, :with_pfii, user_id: user.id, country: 'US') }
    before { investment_1 }


    scenario 'by individual income', js: true, percy: true do
      step 'visit the accreditation page' do
        visit new_offering_invest_accreditation_new_path(offering)

        expect(page).to have_case_insensitive_content(I18n.t('investor_accreditation.new.title'))
      end

      step 'choose to provide documents for individual income' do
        click_on I18n.t('investor_accreditation.new.verify_income.name')

        within('#income') do
          find('div.title', text: I18n.t('investor_accreditation.new.verify_income.document_verification.title').upcase).click
          find('span.title', text: I18n.t('investor_accreditation.new.verify_income.document_verification.individual.title')).click
        end
      end

      step 'submit empty form' do
        click_on I18n.t('investor_accreditation.buttons.submit')

        expect(page).to have_content(I18n.t('workspace_header.errors.general_error_message'))
      end

      step 'fill in and submit form' do
        check I18n.t('investor_accreditation.new.verify_income.document_verification.individual.form.consent_to_income.label')

        within('#individual-income') do
          # HACK couldn't get attach_file to work with an invisible file input
          page.execute_script %Q{
          $('#individual-income .file-chooser').removeClass('hidden');
          }
          attach_file('file-upload-chooser', upload_file_path)
        end

        Percy::Capybara.snapshot(page, name: 'income_individual')
        # TODO: Prevent file uploads to AWS
        #       click_on I18n.t('investor_accreditation.buttons.submit')
      end
    end

    scenario 'by individual net worth', js: true, percy: true do
      step 'visit the accreditation page' do
        visit new_offering_invest_accreditation_new_path(offering)

        expect(page).to have_case_insensitive_content(I18n.t('investor_accreditation.new.title'))
      end

      step 'choose to provide documents for individual net worth' do
        click_on I18n.t('investor_accreditation.new.verify_net_worth.name')

        within('#net-worth') do
          find('div.title', text: I18n.t('investor_accreditation.new.verify_net_worth.document_verification.title').upcase).click
          find('span.title', text: I18n.t('investor_accreditation.new.verify_net_worth.document_verification.individual.title')).click
        end
      end

      step 'submit empty form' do
        click_on I18n.t('investor_accreditation.buttons.submit')

        expect(page).to have_content(I18n.t('workspace_header.errors.general_error_message'))
      end

      step 'fill in and submit form' do
        check I18n.t('investor_accreditation.new.verify_net_worth.document_verification.individual.form.authorization.checkbox')

        within('#individual-net-worth') do
          # HACK couldn't get attach_file to work with an invisible file input
          page.execute_script %Q{
          $('#individual-net-worth .file-chooser').removeClass('hidden');
          }
          attach_file('file-upload-chooser', upload_file_path)
        end

        Percy::Capybara.snapshot(page, name: 'net_worth_individual')
        # TODO: Prevent file uploads to AWS
        #       click_on I18n.t('investor_accreditation.buttons.submit')
      end
    end

    scenario 'by joint income', js: true, percy: true do
      step 'visit the accreditation page' do
        visit new_offering_invest_accreditation_new_path(offering)

        expect(page).to have_case_insensitive_content(I18n.t('investor_accreditation.new.title'))
      end

      step 'choose to provide documents for joint income' do
        click_on I18n.t('investor_accreditation.new.verify_income.name')

        within('#income') do
          find('div.title', text: I18n.t('investor_accreditation.new.verify_income.document_verification.title').upcase).click
          find('span.title', text: I18n.t('investor_accreditation.new.verify_income.document_verification.joint.title')).click
        end
      end

      step 'submit empty form' do
        click_on I18n.t('investor_accreditation.buttons.submit')

        expect(page).to have_content(I18n.t('workspace_header.errors.general_error_message'))
      end

      step 'fill in and submit form' do
        check I18n.t('investor_accreditation.new.verify_income.document_verification.joint.form.consent_to_income')
        fill_in I18n.t('investor_accreditation.new.verify_income.document_verification.joint.form.spouse.name'), with: 'Spousey Spouserson'
        fill_in I18n.t('investor_accreditation.new.verify_income.document_verification.joint.form.spouse.email'), with: 'foo@example.com'

        within('#joint-income') do
          # HACK couldn't get attach_file to work with an invisible file input
          page.execute_script %Q{
          $('#joint-income .file-chooser').removeClass('hidden');
          }
          attach_file('file-upload-chooser', upload_file_path)
        end

        Percy::Capybara.snapshot(page, name: 'income_spouse')
        # TODO: Prevent file uploads to AWS
        #       click_on I18n.t('investor_accreditation.buttons.submit')
      end
    end

    scenario 'by joint net worth', js: true, percy: true do
      step 'visit the accreditation page' do
        visit new_offering_invest_accreditation_new_path(offering)

        expect(page).to have_case_insensitive_content(I18n.t('investor_accreditation.new.title'))
      end

      step 'choose to provide documents for individual net worth' do
        click_on I18n.t('investor_accreditation.new.verify_net_worth.name')

        within('#net-worth') do
          find('div.title', text: I18n.t('investor_accreditation.new.verify_net_worth.document_verification.title').upcase).click
          find('span.title', text: I18n.t('investor_accreditation.new.verify_net_worth.document_verification.joint.title')).click
        end
      end

      step 'submit empty form' do
        click_on I18n.t('investor_accreditation.buttons.submit')

        expect(page).to have_content(I18n.t('workspace_header.errors.general_error_message'))
      end

      step 'fill in and submit form' do
        check I18n.t('investor_accreditation.new.verify_net_worth.document_verification.joint.form.authorization.checkbox')
        fill_in I18n.t('investor_accreditation.new.verify_net_worth.document_verification.joint.form.spouse.name'), with: 'Spousey Spouserson'
        fill_in I18n.t('investor_accreditation.new.verify_net_worth.document_verification.joint.form.spouse.email'), with: 'foo@example.com'
        fill_in I18n.t('investor_accreditation.new.verify_net_worth.document_verification.joint.form.spouse.ssn'), with: '123456789'

        within('#joint-net-worth') do
          # HACK couldn't get attach_file to work with an invisible file input
          page.execute_script %Q{
          $('#joint-net-worth .file-chooser').removeClass('hidden');
          }
          attach_file('file-upload-chooser', upload_file_path)
        end

        Percy::Capybara.snapshot(page, name: 'net_worth_spouse')
        # TODO: Prevent file uploads to AWS
        #       click_on I18n.t('investor_accreditation.buttons.submit')
      end
    end

    context 'with a repeat investor' do
      before { investment_2 }
      scenario 'requests third party to verify income', js: true, percy: true do
        step 'visit the accreditation page' do
          visit new_offering_invest_accreditation_new_path(offering)
          expect(page).to have_case_insensitive_content(I18n.t('investor_accreditation.new.title'))
        end

        step 'choose third party income verification' do
          page.find('a[href="#income"] .title').click

          within('#income') do
            page.find('div.title', text: I18n.translate('investor_accreditation.new.verify_income.third_party_verification.title').upcase).click
          end
        end

        step 'submit empty form' do
          click_on I18n.t('investor_accreditation.buttons.submit')

          expect(page).to have_content(I18n.t('workspace_header.errors.general_error_message'))
        end

        step 'fill in and submit form' do
          within('#income-third-party') do
            select_from_dropdown 'Licensed Attorney', from: "third_party_income_form_role"
            fill_in I18n.t('investor_accreditation.new.verify_income.third_party_verification.form.fieldset.email'), with: 'foo@example.com'
          end

          Percy::Capybara.snapshot(page, name: 'income_third_party')
          click_button I18n.t('investor_accreditation.buttons.submit')

          expect(page).to have_case_insensitive_content(I18n.t('investment_documents.title'))
        end

        step 'accreditation is pending on repeat investment' do
          visit new_offering_invest_accreditation_new_path(offering_2)
          expect(page).to have_case_insensitive_content(I18n.t('investor_accreditation.new.verification_method'))
        end
      end

      scenario 'by individual income, for a user who has verified accreditation in the past', js: true do
        verify_accreditation_for(InvestorEntity.last.id, Date.today - 100)

        step 'visit the accreditation page' do
          visit new_offering_invest_accreditation_new_path(offering)

          expect(page).to have_case_insensitive_content(I18n.t('investor_accreditation.new.previously_verified_subtitle'))
        end
      end
    end

    scenario 'requests third party to verify net worth', js: true, percy: true do
      step 'visit the accreditation page' do
        visit new_offering_invest_accreditation_new_path(offering)

        expect(page).to have_case_insensitive_content(I18n.t('investor_accreditation.new.title'))
      end

      step 'choose third party income verification' do
        page.find('a[href="#net-worth"] .subtitle', text: I18n.translate('investor_accreditation.new.verify_net_worth.description')).click

        within('#net-worth') do
          page.find('div.title', text: I18n.translate('investor_accreditation.new.verify_net_worth.third_party_verification.title').upcase).click
        end
      end

      step 'submit empty form' do
        click_on I18n.t('investor_accreditation.buttons.submit')

        expect(page).to have_content(I18n.t('workspace_header.errors.general_error_message'))
      end

      step 'fill in and submit form' do
        within('#net-worth-third-party') do
          select_from_dropdown 'Licensed Attorney', from: "third_party_net_worth_form_role"
          fill_in I18n.t('investor_accreditation.new.verify_net_worth.third_party_verification.form.fieldset.email'), with: 'foo@example.com'
        end

        Percy::Capybara.snapshot(page, name: 'net_worth_third_party')
        click_button I18n.t('investor_accreditation.buttons.submit')

        expect(page).to have_case_insensitive_content(I18n.t('investment_documents.title'))
      end
    end
  end

  context 'as a trust' do
    let(:investor_entity) { FactoryGirl.create(:investor_entity_trust, :with_pfii, user_id: user.id, country: 'US') }
    before { investment_1 }
    scenario 'requests third party to verify entity', js: true do

      step 'visit the accreditation page' do
        visit new_offering_invest_accreditation_new_path(offering)

        expect(page).to have_case_insensitive_content(I18n.t('investor_accreditation.new_entity.title'))
      end

      step 'submit without opening a form' do
        click_on I18n.t('investor_accreditation.buttons.submit')

        expect(page).to have_content(I18n.t('investor_accreditation.error.no_option_selected'))
      end

      step 'submit empty form' do
        find('div.title', text: I18n.t('investor_accreditation.new_entity.third_party_to_verify.title').upcase).click

        click_on I18n.t('investor_accreditation.buttons.submit')

        expect(page).to have_content(I18n.t('workspace_header.errors.general_error_message'))
      end

      step 'fill in and submit form' do
        select_from_dropdown 'Licensed Attorney', from: "third_party_entity_form_role"
        fill_in I18n.t('investor_accreditation.new_entity.third_party_to_verify.form.fieldset.email'), with: 'foo@example.com'

        click_button I18n.t('investor_accreditation.buttons.submit')

        expect(page).to have_case_insensitive_content(I18n.t('investment_documents.title'))
      end
    end

    scenario 'requests to be contacted offline', js: true, percy: true do
      step 'visit the accreditation page' do
        visit new_offering_invest_accreditation_new_path(offering)
      end

      step 'request to be contacted' do
        find('div.title', text: I18n.t('investor_accreditation.new_entity.offline.title').upcase).click
        click_on I18n.t('investor_accreditation.buttons.submit')
      end

      expect(page).to have_case_insensitive_content(I18n.t('investment_documents.title'))
      Percy::Capybara.snapshot(page, name: 'reg_d_terms_of_investment')
    end

    scenario 'by a trust, for a trust that has verified accreditation in the past', js: true do
      verify_accreditation_for(InvestorEntity.last.id, Date.today - 100)

      step 'visit the accreditation page' do
        visit new_offering_invest_accreditation_new_path(offering)

        expect(page).to have_case_insensitive_content(I18n.t('investor_accreditation.new_entity.previously_verified_subtitle'))
      end
    end
  end

end
