require 'rails_helper'

feature 'Investor can fill out investment profile form', :stub_warden, js: true do

  let(:issuer_user) { create_user }
  let(:issuer_pfii) { FactoryGirl.create(:personal_federal_identification_information, user: issuer_user, first_name: 'Issuer', last_name: 'Dude') }

  let(:investor_user) { create_user(email: "investor_user@email.com", registration_name: "Joe Investor") }
  let(:individual_investor_entity) { FactoryGirl.create(:investor_entity_individual) }
  let(:trust_investor_entity) { FactoryGirl.create(:investor_entity_trust) }
  let(:individual_pfii) { FactoryGirl.create(:personal_federal_identification_information, investor_entity: individual_investor_entity, first_name: 'Guy', last_name: 'Duke') }
  let(:trust_pfii) { FactoryGirl.create(:personal_federal_identification_information, investor_entity: trust_investor_entity, first_name: 'Guy', last_name: 'Duke') }

  let!(:offering_1) { OfferingCreator.new.create(user_id: issuer_user.id, escrow_end_date: 10.days.from_now, ends_at: 11.days.from_now) }
  let!(:offering_2) { OfferingCreator.new.create(user_id: issuer_user.id, escrow_end_date: 10.days.from_now, ends_at: 11.days.from_now) }

  let!(:investment_1) { Investment.create! user: investor_user, offering: offering_1, security: offering_1.reg_d_direct_security, state: 'intended' }
  let!(:investment_2) { Investment.create! user: investor_user, offering: offering_2, security: offering_2.reg_d_direct_security, state: 'intended' }

  let(:valid_email) { 'foo@example.com' }
  let(:valid_role) { 'Licensed Attorney' }

  before do
    sign_in_user(investor_user)
  end

  scenario 'on the investment profile page' do
    step 'You can see the Individual and Entity options' do
      visit new_offering_invest_investment_profile_path(offering_1)

      expect(page).to have_case_insensitive_content(I18n.translate('investment_profile.new.individual.title'))
      expect(page).to have_case_insensitive_content(I18n.translate('investment_profile.new.entity.title'))
    end

    step 'When you do not select an option' do
      click_button I18n.t('investment_profile.new.buttons.submit')

      expect(page).to have_case_insensitive_content(I18n.translate('investment_profile.new.error.no_method_selected'))
    end

    step 'When you do not select an entity' do
      click_link I18n.t('investment_profile.new.entity.title')
      click_button I18n.t('investment_profile.new.buttons.submit')

      expect(page).to have_case_insensitive_content(I18n.translate('investment_profile.new.error.no_entity_selected'))
    end
  end

  scenario 'validates country and region dropdowns for individual' do
    visit new_offering_invest_investment_profile_path(offering_1)

    step 'verify defaults for individual' do

      click_link I18n.t('investment_profile.new.individual.title')
      check_us_citizen 'individual_form', true

      # Did we initialize our dropdowns properly?
      expect(page).to have_field 'individual_form_signatory_country', with: 'US'
      expect(page).to have_xpath '//select[@id="individual_form_signatory_state_id"]/option[@value="CA"]'

      check_passport_photo page, false
    end

    step 'ensure that country and region dropdowns synchronize properly when selecting non-USA country' do
      select_from_dropdown 'United Kingdom', from: 'individual_form_signatory_country'
      check_us_citizen 'individual_form', false

      # Make sure the country was selected correctly
      expect(page).to have_field 'individual_form_signatory_country', with: 'GB'

      # Make sure the state dropdown was updated properly
      expect(page).to have_xpath '//select[@id="individual_form_signatory_state_id"]/option[@value="CGV"]'

      check_non_us_placeholders page, 'individual_form'

      check_passport_photo page, true
    end

    step 'ensure that dropdowns synchronize properly when selecting USA again' do
      select_from_dropdown 'United States', from: 'individual_form_signatory_country'
      check_us_citizen 'individual_form', true

      # Make sure the country was selected correctly
      expect(page).to have_field 'individual_form_signatory_country', with: 'US'

      # Make sure the state dropdown was updated properly
      expect(page).to have_xpath '//select[@id="individual_form_signatory_state_id"]/option[@value="WY"]'

      check_us_placeholders page, 'individual_form'

      check_passport_photo page, false
    end

    step 'ensure that state dropdown is correct when there are no subregions' do
      select_from_dropdown 'American Samoa', from: 'individual_form_signatory_country'

      # Make sure the country was selected correctly
      expect(page).to have_field 'individual_form_signatory_country', with: 'AS'

      # Make sure the state dropdown is disabled
      expect(page).to have_xpath('//*[@id="individual_form_signatory_state_id" and @disabled="disabled"]')
    end
  end

  scenario 'validates country and region dropdowns for trust' do
    visit new_offering_invest_investment_profile_path(offering_1)

    step 'verify defaults for trust' do

      click_link I18n.t('investment_profile.new.entity.title')
      click_link 'Trust'

      # Did we initialize our dropdowns properly?
      expect(page).to have_field 'entity_new_formation_country', with: 'US'
      expect(page).to have_field 'entity_new_country', with: 'US'
      expect(page).to have_field 'entity_new_signatory_country', with: 'US'
      expect(page).to have_xpath '//select[@id="entity_new_formation_state_id"]/option[@value="CA"]'
      expect(page).to have_xpath '//select[@id="entity_new_state_id"]/option[@value="CA"]'
      expect(page).to have_xpath '//select[@id="entity_new_signatory_state_id"]/option[@value="CA"]'

      check_passport_photo page, false
    end

    step 'ensure entity fields respond properly to country of formation changes' do
      select_from_dropdown 'United Kingdom', from: 'entity_new_formation_country'

      # state dropdown should have not applicable as an option
      expect(page).to have_xpath('//select[@id="entity_new_formation_state_id"]/option[. = "- Not Applicable -"]')
      # formatting placeholder for tax id number should be removed
      expect(page).to have_xpath('//*[@id="entity_new_tax_id_number" and @placeholder=""]')
    end

    step 'ensure that country and region dropdowns synchronize properly when selecting non-USA country' do
      select_from_dropdown 'United Kingdom', from: 'entity_new_signatory_country'
      check_us_citizen 'entity_new', false

      # Make sure the country was selected correctly
      expect(page).to have_field 'entity_new_signatory_country', with: 'GB'

      # Make sure the state dropdown was updated properly
      expect(page).to have_xpath '//select[@id="entity_new_signatory_state_id"]/option[@value="CGV"]'

      check_non_us_placeholders page, 'entity_new'

      check_passport_photo page, true
    end

    step 'ensure that dropdowns synchronize properly when selecting USA again' do
      select_from_dropdown 'United States', from: 'entity_new_signatory_country'
      check_us_citizen 'entity_new', true

      # Make sure the country was selected correctly
      expect(page).to have_field 'entity_new_signatory_country', with: 'US'

      # Make sure the state dropdown was updated properly
      expect(page).to have_xpath '//select[@id="entity_new_signatory_state_id"]/option[@value="WY"]'

      check_us_placeholders page, 'entity_new'

      check_passport_photo page, false
    end

    step 'ensure that state dropdown is correct when there are no subregions' do
      select_from_dropdown 'American Samoa', from: 'entity_new_signatory_country'

      # Make sure the country was selected correctly
      expect(page).to have_field 'entity_new_signatory_country', with: 'AS'

      # Make sure the state dropdown is disabled
      expect(page).to have_xpath('//*[@id="entity_new_signatory_state_id" and @disabled="disabled"]')
    end
  end

  scenario 'saving individual info' do
    visit new_offering_invest_investment_profile_path(offering_1)

    step 'you can submit when you select individual' do

      click_link I18n.t('investment_profile.new.individual.title')

      expect(find_field(I18n.t('investor_information.form_section.fieldset_1.first_name.label')).value).to eq 'Joe'
      expect(find_field(I18n.t('investor_information.form_section.fieldset_1.last_name.label')).value).to eq 'Investor'

      fill_in_individual_form

      click_button I18n.t('investment_profile.new.buttons.submit')
      expect(page.current_path).to eq(new_offering_invest_accreditation_new_path(offering_1))
    end

    step 'and see that it is saved' do
      expect(InvestorEntity::Individual.last).to be
    end

    step 'you verify the accreditation for the entity and make another investment' do
      verify_accreditation_for(InvestorEntity::Individual.last.id)

      visit new_offering_invest_investment_profile_path(offering_2)

      click_link I18n.t('investment_profile.new.individual.title')

      click_button I18n.t('investment_profile.new.buttons.submit')

      expect(page.current_path).to eq(new_offering_invest_accreditation_verified_path(offering_2))
    end
  end

  context 'Reg S Investment' do
    before do
      investment_1.update_attributes(calc_data: { reg_s_investor: true }, security_id: offering_1.reg_d_direct_security.id)
      investment_2.update_attributes(calc_data: { reg_s_investor: true }, security_id: offering_2.reg_d_direct_security.id)
    end
    scenario 'saving international individual info' do
      visit new_offering_invest_investment_profile_path(offering_1)

      step 'you can submit when you select individual' do

        click_link I18n.t('investment_profile.new.individual.title')

        expect(find_field(I18n.t('investor_information.form_section.fieldset_1.first_name.label')).value).to eq 'Joe'
        expect(find_field(I18n.t('investor_information.form_section.fieldset_1.last_name.label')).value).to eq 'Investor'

        fill_in_international_individual_form

        click_button I18n.t('investment_profile.new.buttons.submit')

        expect(page.current_path).to eq(offering_invest_investment_documents_path(offering_1))
      end

      step 'and see that it is saved' do
        expect(InvestorEntity::Individual.last).to be
      end

      step 'you verify the accreditation for the entity and make another investment' do
        verify_accreditation_for(InvestorEntity::Individual.last.id)

        visit new_offering_invest_investment_profile_path(offering_2)

        click_link I18n.t('investment_profile.new.individual.title')

        click_button I18n.t('investment_profile.new.buttons.submit')

        expect(page.current_path).to eq(offering_invest_investment_documents_path(offering_2))
      end
    end
  end

  scenario 'submitting invalid trust info' do
    visit new_offering_invest_investment_profile_path(offering_1)

    step 'you wind up back on the page' do

      click_link I18n.t('investment_profile.new.entity.title')
      click_link 'Trust'
      click_button I18n.t('investment_profile.new.buttons.submit')

      expect(page.current_path).to eq(offering_invest_investment_profile_path(offering_1))
    end

    step 'and the trust form should be open' do
      expect(page).to have_content(I18n.t('investment_profile.new.entity.form.trust.information.name'))
    end

    step 'you see errors on the page' do
      expect(page).to have_content('Provide a nine digit tax ID.')
    end
  end

  scenario 'saving trust info', percy: true do
    visit new_offering_invest_investment_profile_path(offering_1)

    step 'you can submit valid trust info' do

      click_link I18n.t('investment_profile.new.entity.title')
      click_link 'Trust'

      Percy::Capybara.snapshot(page, name: 'reg_d_trust_entity')

      within('.signatory_form') do
        expect(find_field(I18n.t('investor_information.form_section.fieldset_1.first_name.label')).value).to eq 'Joe'
        expect(find_field(I18n.t('investor_information.form_section.fieldset_1.last_name.label')).value).to eq 'Investor'
      end

      within('#entity-info') do
        fill_in I18n.t('investment_profile.new.entity.form.trust.information.name'), with: 'Name'
        fill_in I18n.t('investment_profile.new.entity.form.trust.information.tax_id'), with: '123456789'
        fill_in I18n.t('investment_profile.new.entity.form.trust.information.formation_date'), with: '12/12/2012'
        select_from_dropdown 'United States', from: 'entity_new_formation_country'
        select_from_dropdown 'California', from: 'entity_new_formation_state_id'
      end
      within('#contact-info') do
        fill_in I18n.t('investment_profile.new.entity.form.trust.contact.line_1'), with: '123 Main'
        fill_in I18n.t('investment_profile.new.entity.form.trust.contact.city'), with: 'Santa Monica'
        select_from_dropdown 'California', from: 'entity_new_state_id'
        fill_in I18n.t('investment_profile.new.entity.form.trust.contact.zip'), with: '12345'
        fill_in I18n.t('investment_profile.new.entity.form.trust.contact.phone'), with: '1234567891'
      end

      fill_in_signatory_information('entity_new', 'trust')

      click_button I18n.t('investment_profile.new.buttons.submit')

      expect(page.current_path).to eq(new_offering_invest_accreditation_new_path(offering_1))
      Percy::Capybara.snapshot(page, name: 'reg_d_accreditation')
    end

    step 'and see that it is saved' do
      trust = InvestorEntity::Trust.last
      expect(trust.state_id).to eq('CA')
    end
  end

  scenario 'submitting invalid llc info', js: true do
    visit new_offering_invest_investment_profile_path(offering_1)

    step 'you wind up back on the page' do

      click_link I18n.t('investment_profile.new.entity.title')
      click_link 'LLC'

      click_button I18n.t('investment_profile.new.buttons.submit')

      expect(page.current_path).to eq(offering_invest_investment_profile_path(offering_1))
    end

    step 'and the llc form should be open' do
      expect(page).to have_content(I18n.t('investment_profile.new.entity.form.llc.information.name'))
    end

    step 'you see errors on the page' do
      expect(page).to have_content('Provide a nine digit tax ID.')
    end
  end

  scenario 'saving llc info' do
    create_llc_for(offering_1)

    step 'and see that it is saved' do
      llc = InvestorEntity::LLC.last
      expect(llc.state_id).to eq('CA')
    end
  end

  scenario 'submitting invalid partnership info' do
    visit new_offering_invest_investment_profile_path(offering_1)

    step 'you wind up back on the page' do

      click_link I18n.t('investment_profile.new.entity.title')
      click_link 'Partnership'

      click_button I18n.t('investment_profile.new.buttons.submit')

      expect(page.current_path).to eq(offering_invest_investment_profile_path(offering_1))
    end

    step 'and the partnership form should be open' do
      expect(page).to have_content(I18n.t('investment_profile.new.entity.form.partnership.information.name'))
    end

    step 'you see errors on the page' do
      expect(page).to have_content('Provide a nine digit tax ID.')
    end
  end

  scenario 'saving partnership info' do
    visit new_offering_invest_investment_profile_path(offering_1)

    step 'you can submit valid partnership info' do

      click_link I18n.t('investment_profile.new.entity.title')
      click_link 'Partnership'

      within('#entity-info') do
        fill_in I18n.t('investment_profile.new.entity.form.partnership.information.name'), with: 'Name'
        fill_in I18n.t('investment_profile.new.entity.form.partnership.information.tax_id'), with: '123456789'
        select_from_dropdown 'United States', from: 'entity_new_formation_country'
        select_from_dropdown 'California', from: 'entity_new_formation_state_id'
        fill_in I18n.t('investment_profile.new.entity.form.partnership.information.formation_date'), with: '01/01/2014'
      end
      within('#contact-info') do
        fill_in I18n.t('investment_profile.new.entity.form.partnership.contact.line_1'), with: '123 Main'
        fill_in I18n.t('investment_profile.new.entity.form.partnership.contact.city'), with: 'Santa Monica'
        select_from_dropdown 'California', from: 'entity_new_state_id'
        fill_in I18n.t('investment_profile.new.entity.form.partnership.contact.zip'), with: '12345'
        fill_in I18n.t('investment_profile.new.entity.form.partnership.contact.phone'), with: '1234567891'
      end

      fill_in_signatory_information('entity_new', 'partnership')

      click_button I18n.t('investment_profile.new.buttons.submit')
      expect(page.current_path).to eq(new_offering_invest_accreditation_new_path(offering_1))
    end

    step 'and see that it is saved' do
      partnership = InvestorEntity::Partnership.last
      expect(partnership.state_id).to eq('CA')
    end
  end

  scenario 'submitting invalid corporation info' do
    visit new_offering_invest_investment_profile_path(offering_1)

    step 'you wind up back on the page' do

      click_link I18n.t('investment_profile.new.entity.title')
      click_link 'Corporation'

      click_button I18n.t('investment_profile.new.buttons.submit')

      expect(page.current_path).to eq(offering_invest_investment_profile_path(offering_1))
    end

    step 'and the corporation form should be open' do
      expect(page).to have_content(I18n.t('investment_profile.new.entity.form.corporation.information.name'))
    end

    step 'you see errors on the page' do
      expect(page).to have_content('Provide a nine digit tax ID.')
    end
  end

  scenario 'saving corporation info' do
    visit new_offering_invest_investment_profile_path(offering_1)

    step 'you can submit valid corporation info' do

      click_link I18n.t('investment_profile.new.entity.title')
      click_link 'Corporation'

      within('#entity-info') do
        fill_in I18n.t('investment_profile.new.entity.form.corporation.information.name'), with: 'Name'
        fill_in I18n.t('investment_profile.new.entity.form.corporation.information.tax_id'), with: '*****2345'
        select_from_dropdown 'United States', from: 'entity_new_formation_country'
        select_from_dropdown 'California', from: 'entity_new_formation_state_id'
        fill_in I18n.t('investment_profile.new.entity.form.corporation.information.formation_date'), with: '01/01/2014'
      end

      click_button I18n.t('investment_profile.new.buttons.submit')
      expect(page).to have_content('Provide a nine digit tax ID.') #not a server error

      within('#entity-info') do
        fill_in I18n.t('investment_profile.new.entity.form.corporation.information.name'), with: 'Name'
        fill_in I18n.t('investment_profile.new.entity.form.corporation.information.tax_id'), with: '123456789'
        select_from_dropdown 'United States', from: 'entity_new_formation_country'
        select_from_dropdown 'California', from: 'entity_new_formation_state_id'
        fill_in I18n.t('investment_profile.new.entity.form.corporation.information.formation_date'), with: '01/01/2014'
      end

      within('#contact-info') do
        fill_in I18n.t('investment_profile.new.entity.form.corporation.contact.line_1'), with: '123 Main'
        fill_in I18n.t('investment_profile.new.entity.form.corporation.contact.city'), with: 'Santa Monica'
        select_from_dropdown 'California', from: 'entity_new_state_id'
        fill_in I18n.t('investment_profile.new.entity.form.corporation.contact.zip'), with: '12345'
        fill_in I18n.t('investment_profile.new.entity.form.corporation.contact.phone'), with: '1234567891'
      end

      fill_in_signatory_information('entity_new', 'corporation')

      click_button I18n.t('investment_profile.new.buttons.submit')

      expect(page.current_path).to eq(new_offering_invest_accreditation_new_path(offering_1))
    end

    step 'and see that it is saved' do
      corporation = InvestorEntity::Corporation.last
      expect(corporation.state_id).to eq('CA')
    end
  end

  scenario 'using an existing entity' do
    create_llc_for(offering_1)

    step 'do a second investment using that entity' do
      visit new_offering_invest_investment_profile_path(offering_2)
      click_link I18n.t('investment_profile.new.entity.title')

      entity_id = InvestorEntity.last.id
      expect(page).to have_css("a[href='#entity-#{entity_id}']")
      click_on 'TestCompany'

      expect(find_field('Tax ID Number').value).to eq '*****6789'
      expect(page).to have_css(".tab.active > a[href='#entity_#{entity_id}']")
      click_button I18n.t('investment_profile.new.buttons.submit')
      expect(page.current_path).to eq(new_offering_invest_accreditation_new_path(offering_2))
    end
  end

  scenario 'using an existing entity with errors' do
    create_llc_for(offering_1)

    step 'do a second investment using that entity but with errors' do
      visit new_offering_invest_investment_profile_path(offering_2)
      click_link I18n.t('investment_profile.new.entity.title')

      entity_id = InvestorEntity.last.id
      expect(page).to have_css("a[href='#entity-#{entity_id}']")
      click_on 'TestCompany'
      expect(find_field('Tax ID Number').value).to eq '*****6789'
      fill_in "Tax ID Number", with: "123"

      click_button I18n.t('investment_profile.new.buttons.submit')
      expect(page.current_path).to eq(offering_invest_investment_profile_path(offering_2))
      expect(find_field('Tax ID Number').value).to eq '123'
      expect(page).to have_content('Provide a nine digit tax ID.')
    end

  end

  scenario 'creating an LLC when you have an existing entity' do
    create_llc_for(offering_1)

    step 'create another entity' do
      visit new_offering_invest_investment_profile_path(offering_2)
      click_link I18n.t('investment_profile.new.entity.title')

      click_on 'New Entity'
      submit_llc_info

      expect(page.current_path).to eq(new_offering_invest_accreditation_new_path(offering_2))
      expect(InvestorEntity.count).to eq(2)
    end
  end

  scenario 'creating an LLC with invalid info with an existing entity' do
    create_llc_for(offering_1)

    step 'create another entity with bad data' do
      visit new_offering_invest_investment_profile_path(offering_2)
      click_link I18n.t('investment_profile.new.entity.title')

      click_on 'New Entity'
      click_link 'LLC'
      click_button I18n.t('investment_profile.new.buttons.submit')

      expect(page.current_path).to eq(offering_invest_investment_profile_path(offering_2))
      expect(InvestorEntity.count).to eq(1)
    end
  end

  context 'check validations' do

    scenario 'testing validations for individuals' do
      visit new_offering_invest_investment_profile_path(offering_1)

      # :first_name, :last_name, are required but prepopulated
      required_us_fields = [
          :signatory_date_of_birth, :signatory_ssn, :signatory_address1, :signatory_city, :signatory_state_id,
          :signatory_zip_code, :signatory_daytime_phone, :signatory_id_type, :signatory_id_number, :signatory_location,
          :signatory_issued_date, :signatory_expiration_date
      ]

      skip_international_fields = [:signatory_ssn, :signatory_zip_code, :signatory_id_type]

      step 'test for required fields' do

        click_link I18n.t('investment_profile.new.individual.title')

        click_button I18n.t('investment_profile.new.buttons.submit')

        # Check for expected errors
        required_us_fields.each { |f| expect_field_has_error(:individual_form, f) }
      end

      step 'test for international requirements' do

        select 'United Kingdom', from: :individual_form_signatory_country

        click_button I18n.t('investment_profile.new.buttons.submit')

        required_us_fields.each { |f| expect_field_has_error(:individual_form, f) unless skip_international_fields.include? f }
      end

      step 'test for consistency requirements' do

        fill_in :individual_form_signatory_ssn, with: 123

        skip_international_fields += [:signatory_ssn]

        click_button I18n.t('investment_profile.new.buttons.submit')

        required_us_fields.each { |f| expect_field_has_error(:individual_form, f) unless skip_international_fields.include? f }
      end
    end

    scenario 'testing validations for international entities' do
      visit new_offering_invest_investment_profile_path(offering_1)

      # :signatory_first_name, :signatory_last_name, are required by prepopulated
      required_us_fields = [:name, :tax_id_number, :state_id, :formation_state_id,
                            :signatory_date_of_birth, :signatory_ssn, :signatory_address1,
                            :signatory_city, :signatory_state_id, :signatory_zip_code, :signatory_daytime_phone,
                            :signatory_id_type, :signatory_id_number, :signatory_location, :signatory_issued_date, :signatory_expiration_date]

      skip_international_fields = [:signatory_ssn, :signatory_zip_code, :signatory_id_type, :signatory_state_id]

      step 'test for required fields' do

        click_link I18n.t('investment_profile.new.entity.title')
        click_link 'Trust'
        click_button I18n.t('investment_profile.new.buttons.submit')

        required_us_fields.each { |f| expect_field_has_error('entity_new', f) }
      end

      step 'test for international requirements' do

        select 'Guam', from: 'entity_new_signatory_country'

        click_button I18n.t('investment_profile.new.buttons.submit')

        required_us_fields.each { |f| expect_field_has_error('entity_new', f) unless skip_international_fields.include? f }
      end

      step 'test for consistency requirements' do

        select 'Guam', from: 'entity_new_country'
        fill_in 'entity_new_tax_id_number', with: '123'
        fill_in 'entity_new_signatory_ssn', with: '123'
        fill_in 'entity_new_signatory_daytime_phone', with: '5555'

        skip_international_fields += [:tax_id_number, :signatory_daytime_phone, :state_id]

        click_button I18n.t('investment_profile.new.buttons.submit')

        required_us_fields.each { |f| expect_field_has_error('entity_new', f) unless skip_international_fields.include? f }
      end
    end

  end

  def expect_field_has_error(form, field)
    expect(page).to have_css(".#{form}_#{field} .error")
  end

  def create_llc_for(offering)
    visit new_offering_invest_investment_profile_path(offering)

    step 'create an entity' do

      click_link I18n.t('investment_profile.new.entity.title')
      submit_llc_info
      expect(page.current_path).to eq(new_offering_invest_accreditation_new_path(offering))
    end
  end

  def submit_llc_info
    click_link 'LLC'

    within('#entity-info') do
      fill_in I18n.t('investment_profile.new.entity.form.llc.information.name'), with: 'TestCompany'
      fill_in I18n.t('investment_profile.new.entity.form.llc.information.tax_id'), with: '123456789'
      select_from_dropdown 'United States', from: 'entity_new_formation_country'
      select_from_dropdown 'California', from: 'entity_new_formation_state_id'
      fill_in I18n.t('investment_profile.new.entity.form.llc.information.formation_date'), with: '01/01/2014'
    end

    within('#contact-info') do
      fill_in I18n.t('investment_profile.new.entity.form.llc.contact.line_1'), with: '123 Main'
      fill_in I18n.t('investment_profile.new.entity.form.llc.contact.city'), with: 'Santa Monica'
      select_from_dropdown 'California', from: 'entity_new_state_id'
      fill_in I18n.t('investment_profile.new.entity.form.llc.contact.zip'), with: '12345'
      fill_in I18n.t('investment_profile.new.entity.form.llc.contact.phone'), with: '1234567891'
    end

    fill_in_signatory_information('entity_new', 'llc')

    click_button I18n.t('investment_profile.new.buttons.submit')
  end

  def fill_in_individual_form
    fill_in I18n.translate('investor_information.form_section.fieldset_1.first_name.label'), with: 'Jone'
    fill_in I18n.translate('investor_information.form_section.fieldset_1.middle_initial.label'), with: 'T'
    fill_in I18n.translate('investor_information.form_section.fieldset_1.last_name.label'), with: 'Denver'
    fill_in I18n.translate('investor_information.form_section.fieldset_1.date_of_birth.label', popover: '').strip, with: '01/05/1955'
    fill_in I18n.translate('investor_information.form_section.fieldset_1.social_security_number.label', popover: '').strip, with: '555-55-5555'

    within('.location-information') do
      fill_in I18n.translate('investor_information.form_section.fieldset_2.address_1.label'), with: '123 Street'
      fill_in I18n.translate('investor_information.form_section.fieldset_2.address_2.label'), with: 'Apt 204'
      fill_in I18n.translate('investor_information.form_section.fieldset_2.city.label'), with: 'San One'

      select 'Colorado'

      fill_in I18n.translate('investor_information.form_section.fieldset_2.zip_code.label'), with: '12345'
      fill_in I18n.translate('investor_information.form_section.fieldset_2.phone.daytime_label'), with: '123.555-5555'
    end

    select 'Passport'
    fill_in I18n.translate('investor_information.form_section.fieldset_3.id_number.label'), with: '123'
    fill_in I18n.translate('investor_information.form_section.fieldset_3.location.label'), with: 'Denver'
    fill_in I18n.translate('investor_information.form_section.fieldset_3.issued_date.label'), with: '01/05/1955'
    fill_in I18n.translate('investor_information.form_section.fieldset_3.expiration_date.label'), with: '01/01/2020'
    check_us_citizen 'individual_form', true

    within('.employment-information') do
      select 'Employed'
      fill_in I18n.t('investor_information.form_section.fieldset_4.employment_role'), with: 'Boss'
      fill_in I18n.t('investor_information.form_section.fieldset_4.employer_name'), with: 'F'
      check_employer_industry 'individual_form', true
    end

    agree_to_investment_experience
  end

  def check_us_citizen(form, flag)
    check_radio_button "#{form}[signatory_us_citizen]", flag
  end

  def check_employer_industry(form, flag)
    check_radio_button "#{form}[signatory_employer_industry]", flag
  end

  def fill_in_international_individual_form
    fill_in_individual_form

    select_from_dropdown 'United Kingdom', from: 'individual_form_signatory_country'
    select_from_dropdown 'Belfast', from: 'individual_form_signatory_state_id'
    select_from_dropdown 'Driver\'s License', from: 'individual_form_signatory_id_type'

    check_us_citizen 'individual_form', false
    check_employer_industry 'individual_form', false

  end

  def fill_in_signatory_information(form, kind)
    within("##{form} .signatory_form") do
      fill_in I18n.translate('investor_information.form_section.fieldset_1.first_name.label'), with: 'Jone'
      fill_in I18n.translate('investor_information.form_section.fieldset_1.middle_initial.label'), with: 'T'
      fill_in I18n.translate('investor_information.form_section.fieldset_1.last_name.label'), with: 'Denver'
      fill_in I18n.translate('investor_information.form_section.fieldset_1.date_of_birth.label', popover: '').strip, with: '01/05/1955'
      fill_in I18n.translate('investor_information.form_section.fieldset_1.social_security_number.label', popover: '').strip, with: '555-55-5555'

      case kind
        when 'llc', 'corporation'
          fill_in I18n.t("investment_profile.new.entity.form.#{kind}.personal.position"), with: 'Cook'
        when 'partnership'
          select 'General Partner'
        else
          # fill_in "#{form}_position", with: 'Trustee'
      end
      check I18n.translate("investment_profile.new.entity.form.#{kind}.personal.confirmed_authorization")
    end

    within('.location-information') do
      fill_in I18n.translate('investor_information.form_section.fieldset_2.address_1.label'), with: '123 Street'
      fill_in I18n.translate('investor_information.form_section.fieldset_2.address_2.label'), with: 'Apt 204'
      fill_in I18n.translate('investor_information.form_section.fieldset_2.city.label'), with: 'San One'
      select_from_dropdown 'United States', from: "#{form}_signatory_country"
      select 'Colorado'
      fill_in I18n.translate('investor_information.form_section.fieldset_2.zip_code.label'), with: '12345'
      fill_in I18n.translate('investor_information.form_section.fieldset_2.phone.daytime_label'), with: '123-456-8990'
    end

    select 'Passport'
    fill_in I18n.translate('investor_information.form_section.fieldset_3.id_number.label'), with: '123'
    fill_in I18n.translate('investor_information.form_section.fieldset_3.location.label'), with: 'Denver'
    fill_in I18n.translate('investor_information.form_section.fieldset_3.issued_date.label'), with: '01/05/1955'
    fill_in I18n.translate('investor_information.form_section.fieldset_3.expiration_date.label'), with: '01/01/2020'
    check_us_citizen "#{form}", true

    within('.employment-information') do
      select 'Employed'
      fill_in I18n.t('investor_information.form_section.fieldset_4.employment_role'), with: 'Boss'
      fill_in I18n.t('investor_information.form_section.fieldset_4.employer_name'), with: 'F'
      check_employer_industry form, true
    end

    agree_to_investment_experience
  end

  def check_us_placeholders(page, form)
    expect(page).to have_xpath "//*[@id='#{form}_signatory_ssn' and @placeholder='###-##-####']"
    expect(page).to have_xpath "//*[@id='#{form}_signatory_daytime_phone' and @placeholder='###-###-####']"
  end

  def check_non_us_placeholders(page, form)
    expect(page).to have_xpath "//*[@id='#{form}_signatory_ssn' and @placeholder='']"
    expect(page).to have_xpath "//*[@id='#{form}_signatory_daytime_phone' and @placeholder='']"
  end

  def check_passport_photo(page, required)
    text = I18n.t('investment_profile.new.international.passport_html')
    text = text[0..text.index('. ')] # We only look for the first sentence
    case
      when required
        expect(page).to have_case_insensitive_content text
      else
        expect(page).to_not have_case_insensitive_content text
    end
  end
end
