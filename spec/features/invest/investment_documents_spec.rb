require 'rails_helper'

shared_examples "an investor_entity" do

  before do
    investment.investor_entity = investor_entity
    investment.save
    investment.update_attribute(:state, "accreditation_verified")
    log_in_with_credentials('investor@example.com', 'password')
  end

  context "Using third party income verification" do
    let!(:qualification) do
      Accreditor::ThirdPartyIncomeQualification.create(investor_entity_id: investor_entity.id, email: "cpa@email.com", role: "Certified Public Accountant", user_id: user.id)
    end

    before do
      Accreditation::StatusUpdater.new.update_investment investment

      visit offering_invest_investment_documents_path(offering)
    end

    it "displays the email and role of the third party" do
      within("#verify") do
        expect(page).to have_content("cpa@email.com")
        expect(page).to have_content("Certified Public Accountant")
      end

    end

    it "displays pending if the email has been sent" do
      qualification.emailed_at = 1.days.ago
      qualification.save
      visit offering_invest_investment_documents_path(offering)

      expect(page).to have_case_insensitive_content(I18n.t("investment_documents.sections.verify.verification_pending_status"))
    end

    it "displays expired if so" do
      qualification.expires_at = 2.days.ago
      qualification.save
      visit offering_invest_investment_documents_path(offering)

      expect(page).to have_case_insensitive_content(I18n.t("investment_documents.sections.verify.verification_expired_status"))
    end

    it "displays declined if so" do
      qualification.declined_at = 1.days.ago
      qualification.save
      visit offering_invest_investment_documents_path(offering)

      expect(page).to have_case_insensitive_content(I18n.t("investment_documents.sections.verify.verification_declined_status"))
    end

    it "displays verified if so" do
      qualification.verified_at = 1.days.ago
      qualification.save
      visit offering_invest_investment_documents_path(offering)

      expect(page).to have_case_insensitive_content(I18n.t("investment_documents.sections.verify.verification_verified_status"))
    end
  end

  context "Using Documents" do
    let!(:qualification) { Accreditor::IndividualIncomeQualification.create(investor_entity_id: investor_entity.id, user_id: user.id, data: {}) }

    before do
      investor_entity.accreditor_qualifications = [qualification]
      doc1                                      = Accreditation::Document.create(accreditation_qualification_id: qualification.id)
      doc1.update_column(:document, "123-123-123-123/document.pdf")
      doc2 = Accreditation::Document.create(accreditation_qualification_id: qualification.id)
      doc2.update_column(:document, "123-123-123-123/document_campaign.pdf")

      Accreditation::StatusUpdater.new.update_investment investment

      visit offering_invest_investment_documents_path(offering)
    end

    it "displays the type of verification" do
      within("#verify") do
        expect(page).to have_content("Individual Income")
        expect(page).to have_content("document.pdf")
        expect(page).to have_content("document_campaign.pdf")
      end

    end

    it "displays pending by default" do
      expect(page).to have_case_insensitive_content(I18n.t("investment_documents.sections.verify.verification_pending_status"))
    end

  end

end

feature 'User reaches Review and Sign page', :js do
  let!(:trust_entity) { FactoryGirl.create(:investor_entity_trust, :with_pfii, user_id: user.id) }
  let!(:individual_entity) { FactoryGirl.create(:investor_entity_individual, :with_pfii, user_id: user.id) }
  let!(:llc_entity) { FactoryGirl.create(:investor_entity_llc, :with_pfii, user_id: user.id) }
  let!(:offering) { FactoryGirl.create(:offering, escrow_end_date: 10.days.from_now) }
  let!(:investment) { FactoryGirl.build(:investment, offering: offering, security: offering.reg_d_direct_security, user: user) }


  context 'with a confirmed email' do
    let!(:user) { FactoryGirl.create(:user, email: 'investor@example.com', password: 'password', confirmed_at: 1.day.ago) }

    context "As an Entity" do
      it_should_behave_like "an investor_entity" do
        let(:investor_entity) { trust_entity }
      end
    end

    context "As an Individual" do
      it_should_behave_like "an investor_entity" do
        let(:investor_entity) { individual_entity }
      end

      context "Individual-specific behaviour" do
        let!(:qualification) { Accreditor::ThirdPartyIncomeQualification.create(investor_entity_id: individual_entity.id, email: "cpa@email.com", role: "Certified Public Accountant", user_id: user.id) }

        before do
          investment.investor_entity = individual_entity
          investment.save
          investment.update_attribute(:state, "accreditation_verified")
          individual_entity.accreditor_qualifications = [qualification]

          log_in_with_credentials('investor@example.com', 'password')
          Accreditation::StatusUpdater.new.update_investment investment

          visit offering_invest_investment_documents_path(offering)
        end

        it "should display the Individual's PFII" do
          expect(page).to have_content(individual_entity.full_name)
          expect(page).to_not have_content(I18n.t("investment_documents.sections.profile.signatory_name_title"))
        end
      end
    end

    context "As an LLC" do
      it_should_behave_like "an investor_entity" do
        let(:investor_entity) { llc_entity }
      end
    end

  end

end