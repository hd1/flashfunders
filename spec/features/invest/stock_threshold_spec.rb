require 'rails_helper'

feature 'Investor invests in stock offering', :stub_warden do
  before do
    user.update_attribute(:authorized_to_invest_status, User.authorized_to_invest_statuses[:allowed])
  end

  let!(:user) { FactoryGirl.create(:user) }
  let!(:offering) { o = FactoryGirl.create(:fsp_only_offering,
                     user: user,
                     ends_at: 60.days.from_now,
                     tagline: 'The disappearing, reappearing ink.',
                     visibility: :visibility_public,
                     fully_diluted_shares: 10000,
                     share_price: 2.035,
                     pre_money_valuation: (10000 * 2.035))
    o.securities.first.update_attributes(
        maximum_total_investment: (5000 * 2.035),
        minimum_investment_amount: 100,
        shares_offered: 5000,
    )
    o
  }
  let!(:investor_entity) { FactoryGirl.create(:investor_entity) }
  let!(:investment) { FactoryGirl.create(:investment, investor_entity: investor_entity, security: offering.reg_d_direct_security, offering: offering, user: user) }


  context 'invests in model_direct offering' do
    scenario 'no investment amount yet entered', js: true do
      visit new_offering_investment_path(offering)
      expect(page).to have_content('--% of the Company for $--')
      expect(page).not_to have_content('Investments above $25,000.00 are direct, without any fees or carry.')
    end

    scenario 'amount >= minimum_investment_amount', js: true do
      fill_in_investment_amount('100')
      expect(page).to have_case_insensitive_content('You will own: 0.333% of the Company for $101.75')
    end
  end

  context 'invests in model_direct_spv offering' do
    before do
      offering.update_attributes(
        spv_minimum_goal: 100000,
        setup_fee_dollar: 8000)
      FactoryGirl.create(:fsp_stock, is_spv: true, minimum_total_investment: 100000, minimum_investment_amount: 25000, offering: offering)
      offering.reload
    end

    context 'with no starting investments' do
      scenario 'no investment amount yet entered', js: true do
        visit new_offering_investment_path(offering)
        expect(page).not_to have_content('This amount may be rounded for even stock shares.')
        expect(page).to have_content('Investments above $25,000.00 will be made directly into Acme Co. without any fees or carry.')
        expect(page).to have_case_insensitive_content('You will own: --% of the Company for $--')
        expect(page).not_to have_content('-- Shares and --%')
      end

      scenario 'investor enters an amount < the direct investment threshold', js: true  do
        fill_in_investment_amount('24999')
        expect(page).to have_case_insensitive_content('You will own: 75.345% of the Company for $24,999.00')
      end

      scenario 'investor enters an amount >= the direct investment threshold', js: true  do
        fill_in_investment_amount('25000')
        expect(page).to have_case_insensitive_content('You will own: 81.907% of the Company for $25,002.01')
      end
    end

    context 'with spv_min < direct threshold' do
      before do
        offering.update_attributes(
          spv_minimum_goal: 11000)
        offering.reload
      end

      scenario 'invests < spv_min', js: true do
        fill_in_investment_amount('500')
        expect(page).to have_case_insensitive_content('You will own: 0.447% of the Company for $500.00')
      end

      scenario 'invests > spv_min and < threshold', js: true do
        fill_in_investment_amount('15000')
        expect(page).to have_case_insensitive_content('You will own: 22.932% of the Company for $15,000.00')
      end
    end

    context 'starting with 11 SPV investments (totaling $110K + $20K = $130K) > SPV min ($100K)' do
      before do
        investor_entity = InvestorEntity.create!(user_id: user.id, identification_status_id: :verified)
        11.times do
          FactoryGirl.create(:investment, :escrowed, offering_id: offering.id, security: offering.other_security, amount: 10000, user_id: user.id, investor_entity_id: investor_entity.id)
        end
      end

      scenario 'spv (below direct threshold)', js: true  do
        fill_in_investment_amount('20000')
        expect(page).to have_case_insensitive_content('You will own: 61.488% of the Company for $20,000.00')
        # Hide Direct Popover
        expect(page).to have_css('span#popover_direct.hidden', visible: false)
        # Show SPV Popover
        expect(page).to have_css('span#popover_spv')
        expect(page).not_to have_css('span#popover_spv.hidden')
      end

      scenario 'above direct threshold', js: true  do
        fill_in_investment_amount('30000')
        expect(page).to have_case_insensitive_content('You will own: 98.287% of the Company for $30,002.00')
        # Show Direct Popover
        expect(page).to have_css('span#popover_direct')
        expect(page).not_to have_css('span#popover_direct.hidden')
        # Hide SPV Popover
        expect(page).to have_css('span#popover_spv.hidden', visible: false)
      end
    end

    context 'SPV total investments totalling $23K + $22K = $45K > SPV min of $40K' do
      before do
        offering.update_attributes(spv_minimum_goal: 40000)
        offering.other_security.update_attributes(minimum_total_investment: 40000)
        offering.reload
        investor_entity = InvestorEntity.create!(user_id: user.id, identification_status_id: :verified)
        FactoryGirl.create(:investment, :escrowed, offering_id: offering.id, security: offering.other_security, amount: 23000, user_id: user.id, investor_entity_id: investor_entity.id)
      end

      scenario 'investment COMMITTED and below direct threshold', js: true do
        fill_in_investment_amount('22000')
        expect(page).to have_case_insensitive_content('You will own: 59.259% of the Company for $22,000.00')
      end
    end

    context 'SPV total investments NOT totalling $23K + $22K = $45K > SPV min of $40K' do
      before do
        offering.update_attributes(spv_minimum_goal: 40000)
        offering.reload
        FactoryGirl.create(:investment, offering_id: offering.id, amount: 23000, user_id: user.id)
      end

      scenario 'investment NOT COMMITTED and below direct threshold', js: true do
        fill_in_investment_amount('22000')
        expect(page).to have_case_insensitive_content('You will own: 57.658% of the Company for $22,000.00')
      end
    end
  end
end
