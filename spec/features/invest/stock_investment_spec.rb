require 'rails_helper'

feature 'Investor invests in stock offering', :stub_warden do
  before do
    user.update_attribute(:authorized_to_invest_status, User.authorized_to_invest_statuses[:allowed])
  end

  let!(:user) { FactoryGirl.create(:user) }
  let(:security) {
    FactoryGirl.create(:security,
                       type: 'Securities::FspLlc',
                       shares_offered: 5000,
                       maximum_total_investment: (5000 * 2.0359827654),
                       minimum_total_investment: 1_000_000,
                       minimum_investment_amount: 100,
    )
  }
  let!(:offering) { FactoryGirl.create(:offering,
                     user: user,
                     securities: [security],
                     ends_at: 60.days.from_now,
                     tagline: 'The disappearing, reappearing ink.',
                     visibility: :visibility_public,
                     fully_diluted_shares: 10000,
                     pre_money_valuation: (10000 * 2.0359827654),
                     share_price: 2.0359827654) }
  let!(:investor_entity) { FactoryGirl.create(:investor_entity) }
  let!(:investment) { FactoryGirl.create(:investment, investor_entity: investor_entity, offering: offering, security: security, user: user) }


  context 'invests in model_direct offering' do
    scenario 'no investment amount yet entered', js: true do
      visit new_offering_investment_path(offering)
      expect(page).to have_content('--% of the Company for $--')
      expect(page).not_to have_content('Investments above $25,000.00 are direct, without any fees or carry.')
    end

    scenario 'amount >= minimum_investment_amount', js: true do
      fill_in_investment_amount('100')
      expect(page).to have_case_insensitive_content('You will own: 0.333% of the Company for $101.79')
    end
  end

  context 'invests in model_direct_spv offering' do
    let!(:spv_security) { FactoryGirl.create(:fsp_stock,
                                             is_spv: true,
                                             offering: offering,
                                             minimum_total_investment: 100000,
                                             maximum_total_investment: 0,
                                             minimum_investment_amount: 25000)
    }

    before do
      offering.update_attributes(
        spv_minimum_goal: 100000,
        setup_fee_dollar: 8000)
      offering.reload
    end

    context 'with no starting investments' do
      scenario 'no investment amount yet entered', js: true do
        visit new_offering_investment_path(offering)

        expect(page).not_to have_content('This amount may be rounded for even stock shares.')
        expect(page).to have_content('Investments above $25,000.00 will be made directly into Acme Co. without any fees or carry.')
        expect(page).to have_case_insensitive_content('You will own: --% of the Company for $--')
        expect(page).not_to have_content('-- Shares and --%')
      end

      scenario 'investor enters an amount < the direct investment threshold', js: true  do
        fill_in_investment_amount('24999')
        expect(page).to have_case_insensitive_content('You will own: 75.309% of the Company for $24,999.00')
      end

      scenario 'investor enters an amount >= the direct investment threshold', js: true  do
        fill_in_investment_amount('25000')
        expect(page).to have_case_insensitive_content('You will own: 81.867% of the Company for $25,001.86')
      end
    end

    context 'with spv_min < direct threshold' do
      before do
        offering.update_attributes(
          spv_minimum_goal: 11000)
        offering.reload
      end

      scenario 'invests > spv_min and < threshold', js: true do
        fill_in_investment_amount('15000')
        expect(page).to have_case_insensitive_content('You will own: 22.921% of the Company for $15,000.00')
      end
    end

    context 'starting with 11 SPV investments (totaling $110K + $20K = $130K) > SPV min ($100K)' do
      before do
        investor_entity = InvestorEntity.create!(user_id: user.id, identification_status_id: :verified)
        11.times do
          FactoryGirl.create(:investment, :escrowed, offering_id: offering.id, security: spv_security, amount: 10000, user_id: user.id, investor_entity_id: investor_entity.id)
        end
      end

      scenario 'spv (below direct threshold)', js: true  do
        fill_in_investment_amount('20000')
        expect(page).to have_case_insensitive_content('You will own: 61.458% of the Company for $20,000.00')
      end

      scenario 'above direct threshold', js: true  do
        fill_in_investment_amount('30000')
        expect(page).to have_case_insensitive_content('You will own: 98.233% of the Company for $30,000.20')
      end
    end

    context 'SPV total investments totalling $23K + $22K = $45K > SPV min of $40K' do
      before do
        offering.update_attributes(spv_minimum_goal: 40000)
        offering.reload
        investor_entity = InvestorEntity.create!(user_id: user.id, identification_status_id: :verified)
        FactoryGirl.create(:investment, :escrowed, offering_id: offering.id, security: spv_security, amount: 23000, user_id: user.id, investor_entity_id: investor_entity.id)
      end

      scenario 'investment COMMITTED and below direct threshold', js: true do
        fill_in_investment_amount('22000')
        expect(page).to have_case_insensitive_content('You will own: 59.231% of the Company for $22,000.00')
      end
    end

    context 'SPV total investments NOT totalling $23K + $22K = $45K > SPV min of $40K' do
      before do
        offering.update_attributes(spv_minimum_goal: 40000)
        offering.reload
        FactoryGirl.create(:investment, offering_id: offering.id, amount: 23000, user_id: user.id)
      end

      scenario 'investment NOT COMMITTED and below direct threshold', js: true do
        fill_in_investment_amount('22000')
        expect(page).to have_case_insensitive_content('You will own: 57.630% of the Company for $22,000.00')
      end
    end
  end
end
