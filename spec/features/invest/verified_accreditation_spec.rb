require 'rails_helper'

feature 'Investor already has a verified accreditation', :stub_warden do

  let!(:user) do
    user = create_user(email: 'user@example.com', password: 'thepassword', password_confirmation: 'thepassword')
    FactoryGirl.create(:personal_federal_identification_information, user_id: user.id, first_name: 'Guy', last_name: 'Duke')
    user
  end

  let!(:offering) { FactoryGirl.create(:fsp_only_offering, user_id: user.id) }
  let(:entity) { FactoryGirl.create(:investor_entity_individual, user_id: user.id, personal_federal_identification_information: user.personal_federal_identification_information)}
  let!(:investment) { Investment.create! user_id: user.id, offering_id: offering.id, security: offering.reg_d_direct_security, state: 'entity_selected', investor_entity_id: entity.id }
  let(:start_date) { Date.current - 20 }
  let(:end_date) { Date.current + 20 }
  let!(:qualification) { Accreditor::Qualification.create! type: Accreditor::IndividualIncomeQualification.to_s, data: {}, user_id: user.id, verified_at: start_date, expires_at: end_date, investor_entity_id: entity.id}

  before do
    sign_in_user(user)

    Accreditation::StatusUpdater.new.update_investment investment
  end

  scenario 'by individual income' do

    step 'visit the already accredited page' do
      visit new_offering_invest_accreditation_verified_path(offering)

      expect(page).to have_case_insensitive_content(I18n.t('investor_accreditation.already_accredited.individual_title'))
    end

    step 'submit empty form' do
      click_on I18n.t('investor_accreditation.success.links.continue')

      expect(page).to have_content(I18n.t('workspace_header.errors.general_error_message'))
    end

    step 'submit checked form' do
      check I18n.t('investor_accreditation.already_accredited.below_the_fold.promise_individual')
      click_on I18n.t('investor_accreditation.success.links.continue')

      expect(current_path).to eq(offering_invest_investment_documents_path(offering))
    end
  end

  scenario 'by third party as an individual', js: true do
    qualification.update_attribute :type, Accreditor::ThirdPartyIncomeQualification.to_s

    step 'visit the already accredited page' do
      visit new_offering_invest_accreditation_verified_path(offering)

      expect(page).to have_case_insensitive_content(I18n.t('investor_accreditation.already_accredited.individual_title'))
    end

    step 'submit form' do
      click_on I18n.t('investor_accreditation.success.links.continue')

      expect(current_path).to eq(offering_invest_investment_documents_path(offering))
    end
  end

  scenario 'with a qualification that runs out in fewer than 10 days', js: true do
    investment.current_accreditor_span.update_attribute :end_date, Date.current + 5

    step 'visit the already accredited page' do
      visit new_offering_invest_accreditation_verified_path(offering)

      expect(page).to have_case_insensitive_content(I18n.t('investor_accreditation.already_accredited.low_time_warning', days_left: 5))
    end
  end

  scenario 'by third party as an entity', js: true do
    qualification.update_attribute :type, Accreditor::ThirdPartyEntityQualification.to_s
    entity.update_attribute(:type, InvestorEntity::LLC.to_s)

    step 'visit the already accredited page' do
      visit new_offering_invest_accreditation_verified_path(offering)

      expect(page).to have_case_insensitive_content(I18n.t('investor_accreditation.already_accredited.group_title', entity_type: 'LLC'))
    end

    step 'submit form' do
      click_on I18n.t('investor_accreditation.success.links.continue')

      expect(current_path).to eq(offering_invest_investment_documents_path(offering))
    end
  end

  scenario 'offline as an entity', js: true do
    qualification.update_attribute :type, Accreditor::OfflineEntityQualification.to_s
    entity.update_attribute(:type, InvestorEntity::LLC.to_s)

    step 'visit the already accredited page' do
      visit new_offering_invest_accreditation_verified_path(offering)

      check I18n.t('investor_accreditation.already_accredited.below_the_fold.promise', entity_type: 'LLC')

      expect(page).to have_case_insensitive_content(I18n.t('investor_accreditation.already_accredited.group_title', entity_type: 'LLC'))
    end

    step 'submit form' do
      click_on I18n.t('investor_accreditation.success.links.continue')

      expect(current_path).to eq(offering_invest_investment_documents_path(offering))
    end
  end
end
