require 'rails_helper'

feature 'Investor invests in SPV offering', :stub_warden do
  let!(:user) { FactoryGirl.create(:user, confirmed_at: 1.day.ago) }
  let!(:offering) { FactoryGirl.create(:offering,
                                       user: user,
                                       ends_at: 60.days.from_now,
                                       tagline: 'Raymondo Mondo Taco',
                                       visibility: :visibility_public,
                                       # shares_offered: 100000,
                                       # share_price: 1,
                                       # maximum_total_investment: 2000000,
                                       # minimum_total_investment: 1000000,
                                       # minimum_investment_amount: 100,
                                       # security_type_id: 2,
                                       # offering_model: :model_direct_spv,
                                       fully_diluted_shares: 10000) }
  let!(:qualification) { Accreditor::ThirdPartyIncomeQualification.new(user_id: user.id, email: "cpa@email.com", role: "Certified Public Accountant") }
  let!(:entity) { FactoryGirl.create(:investor_entity_individual, :with_pfii, accreditor_qualifications: [qualification]) }
  let!(:investment) { FactoryGirl.create(:investment, user: user,
    offering: offering, state: 'accreditation_verified',
    security: offering.other_security,
    investor_entity: entity,
    percent_ownership: 55) }

  before do
    sign_in_user(user)

    Accreditation::StatusUpdater.new.update_investment investment
  end

  context 'investor sees right terms for an SPV offering' do
    before do
      investment.update_attributes(amount: 10999.99)
    end
    scenario 'should see FlashFund terms', js: true do
      visit offering_invest_investment_documents_path(offering)
      expect(page).to have_content('55.000%')
    end
  end

  context 'investor see right terms for a direct offering' do
    before do
      investment.update_attributes(amount: 11000)
    end
    scenario 'should see FlashSeed Preferred terms', js: true do
      visit offering_invest_investment_documents_path(offering)
      expect(page).to have_content('55.000%')
    end
  end
end
