require 'rails_helper'

feature 'Investor invests in stock offering', :stub_warden do
  let!(:user) { FactoryGirl.create(:user) }
  let!(:offering) { FactoryGirl.create(:fsp_only_offering, user: user, fully_diluted_shares: 10000, share_price: 2.035) }

  before do
    RoutingNumber.create! routing_number: '012345678'
    offering.update_attributes(
        ends_at: 60.days.from_now,
        tagline: 'The disappearing, reappearing ink.',
        visibility: :visibility_public,
        fully_diluted_shares: 10000,
        share_price: 2.035,
        pre_money_valuation: (10000 * 2.035)
    )

    offering.reg_d_direct_security.update_attributes(
        shares_offered: 5000,
        maximum_total_investment: (5000 * 2.035),
        minimum_total_investment: 1000000,
        minimum_investment_amount: 5000
    )
    offering.reload
    user.confirm!
  end

  scenario 'by entering an investment amount', :js, percy: true do
    fill_in_investment_amount(5002)

    within '.equity-ownership-widget' do
      expect(page).to have_content('16.387% ' + I18n.t('investor_investments.form_section.investment_details.part_2_stock') +
                                     ' $5,002.03')
    end

    Percy::Capybara.snapshot(page, name: 'reg_d_only_amount')
  end

  context 'as a logged in investor' do
    before { sign_in_user(user) }

    scenario 'by submitting an investment amount', js: true do
      submit_investment(5010)

      expect(current_path).to eql(new_offering_invest_investment_profile_path(offering))
    end

    scenario 'by submitting an invalid investment amount', js: true do
      submit_investment(0)

      expect(page).to have_content('Must be larger than $')
      expect(page).to have_content('--% ' + I18n.t('investor_investments.form_section.investment_details.part_2_stock') +
                                   ' $--')
    end
  end

  context 'as an anonymous user' do
    scenario 'by submitting an investment amount', js: true do
      submit_investment(5010)
      expect(current_path).to eq(new_user_registration_path)

      log_in_with_credentials(user.email, 'password')
      expect(current_path).to eql(new_offering_invest_investment_profile_path(offering))
    end

    scenario 'by submitting an invalid investment amount', js: true do
      submit_investment(0)

      expect(current_path).to eq(offering_investment_path(offering))
      expect(page).to have_content('Must be larger than $5000.00')
    end
  end

  context 'for a custom stock offering' do
    scenario 'viewing the page' do
      offering.securities.first.update_attributes(
          type: 'Securities::CustomStock',
          security_title: 'Awesome Stock!',
          security_title_plural: 'Awesome Stocks!'
      )

      visit new_offering_investment_path(offering)

      expect(page).to have_content("of #{offering.security_type_custom}")

      offering.update_attributes(security_type_id: :security_fsp)
    end
  end
end

feature 'Investor invests in convertible note offering', :stub_warden do
  let!(:user) { FactoryGirl.create(:user) }
  let!(:offering) { FactoryGirl.create(:convertible_offering,
    user: user,
    ends_at: 60.days.from_now,
    tagline: 'The disappearing, reappearing ink.',
    visibility: :visibility_public,
    pre_money_valuation: 0,
  ) }

  before do
    RoutingNumber.create! routing_number: '012345678'
    offering.securities.first.update_attributes(
        maximum_total_investment: 2000000,
        minimum_total_investment: 1000000,
        minimum_investment_amount: 5000,
        discount_rate: '35',
        interest_rate: '12',
        valuation_cap: 0,
        security_title_plural: 'bitcoins'
    )
    user.confirm!
  end

  scenario 'by entering an investment amount', :js do
    fill_in_investment_amount(5002)

    within '.convertible-note-ownership-widget' do
      expect(page).to have_content("$5,002.00 of #{offering.securities.first.security_title_plural} and 0.250% #{I18n.t('investor_investments.form_section.investment_details.part_2_notes')}")
    end
  end

  context 'as a logged in investor' do
    before { sign_in_user(user) }

    scenario 'by submitting an investment amount', js: true do
      submit_investment(5010)

      expect(current_path).to eql(new_offering_invest_investment_profile_path(offering))
    end

    scenario 'by submitting an invalid investment amount', js: true do
      submit_investment(0)

      expect(page).to have_content('Must be larger than $')
      expect(page).to have_case_insensitive_content("You will own: $-- of #{offering.securities.first.security_title_plural} and --% #{I18n.t('investor_investments.form_section.investment_details.part_2_notes')}")
    end
  end

  context 'as an anonymous user' do
    scenario 'by submitting an investment amount', js: true do
      submit_investment(5010)
      expect(current_path).to eq(new_user_registration_path)

      log_in_with_credentials(user.email, 'password')
      expect(current_path).to eql(new_offering_invest_investment_profile_path(offering))
    end

    scenario 'by submitting an invalid investment amount', js: true do
      submit_investment(0)

      expect(current_path).to eq(offering_investment_path(offering))
      expect(page).to have_content('Must be larger than $5000.00')
    end
  end
end
