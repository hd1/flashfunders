require 'rails_helper'

feature 'Investor invests in convertible note offering', :stub_warden do
  before { user.update_attribute(:authorized_to_invest_status, User.authorized_to_invest_statuses[:allowed]) }

  let!(:user) { FactoryGirl.create(:user) }
  let!(:offering) { FactoryGirl.create(:offering,
                     user: user,
                     ends_at: 60.days.from_now,
                     tagline: 'The disappearing, reappearing ink.',
                     visibility: :visibility_public,
                     securities: [
                         FactoryGirl.create(:convertible,
                                             type: Securities::CustomConvertible,
                                             maximum_total_investment: 2_000_000,
                                             minimum_total_investment: 1_000_000,
                                             minimum_investment_amount: 100,
                                             discount_rate: '35',
                                             security_title_plural: 'bitcoins',
                                             security_title: 'bitcoin',
                                             valuation_cap: '0'
                         )],
                     setup_fee_dollar: 8000,
                     spv_minimum_goal: 100000,
  ) }
  let!(:investor_entity) { FactoryGirl.create(:investor_entity, user_id: user.id) }

  context 'invests in model_direct offering' do
    scenario 'no investment amount yet entered', js: true do
      visit new_offering_investment_path(offering)
      expect(page).to have_content('--% of the Company')
      expect(page).not_to have_content('Investments above $25,000.00 are direct, without any fees or carry.')
      expect(page).to have_content('bitcoins')
    end

    scenario 'amount >= minimum_investment_amount', js: true do
      fill_in_investment_amount('100')
      expect(page).to have_case_insensitive_content('You will own: $100.00 of bitcoins and 0.005% of the Company')
    end
  end

  context 'invests in model_direct_spv offering' do
    let!(:spv_security) { FactoryGirl.create(:convertible,
                                             offering: offering,
                                             type: 'Securities::CustomConvertible',
                                             is_spv: true,
                                             minimum_total_investment: 100000,
                                             minimum_investment_amount: 25000,
                                             discount_rate: '35',
                                             security_title_plural: 'bitcoins',
                                             security_title: 'bitcoin',
                                             valuation_cap: '0'
    )
    }

    before do
      offering.reload
    end

    context 'with no starting investments', js: true do
      scenario 'no investment amount yet entered' do
        visit new_offering_investment_path(offering)
        expect(page).not_to have_content('This amount may be rounded for even stock shares.')
        expect(page).to have_content('Investments above $25,000.00 will be made directly into Acme Co. without any fees or carry.')
        expect(page).to have_content('--% of the Company')
        expect(page).not_to have_content('-- Shares and --%')
      end

      scenario 'investor enters an amount < the direct investment threshold' do
        fill_in_investment_amount('24999')
        expect(page).to have_case_insensitive_content('You will own: 1.150% of the Company')
      end

      scenario 'investor enters an amount = the direct investment threshold' do
        fill_in_investment_amount('25000')
        expect(page).to have_case_insensitive_content('You will own: $25,000.00 of bitcoins and 1.250% of the Company')
      end

      scenario 'investor enters an amount > the direct investment threshold' do
        fill_in_investment_amount('250000')
        expect(page).to have_case_insensitive_content('You will own: $250,000.00 of bitcoins and 12.500% of the Company')
      end
    end

    context 'starting with 11 SPV investments (totaling $110K + $20K = $130K) > SPV min ($100K)' do
      before do
        investor_entity = InvestorEntity.create!(user_id: user.id, identification_status_id: :verified)
        11.times do
          FactoryGirl.create(:investment, :escrowed, offering_id: offering.id, security: spv_security, amount: 10000, user_id: user.id, investor_entity_id: investor_entity.id)
        end
      end

      scenario 'below direct threshold', js: true  do
        fill_in_investment_amount('20000')
        expect(page).to have_case_insensitive_content('You will own: 0.938% of the Company')
      end

      scenario 'above direct threshold', js: true  do
        fill_in_investment_amount('100000')
        expect(page).to have_case_insensitive_content('You will own: $100,000.00 of bitcoins and 5.000% of the Company')
      end
    end

    context 'SPV total investments totalling $23K + $22K = $45K > SPV min of $40K' do
      before do
        spv_security.update_attributes(minimum_total_investment: 40000)
        offering.reload
        investor_entity = InvestorEntity.create!(user_id: user.id, identification_status_id: :verified)
        FactoryGirl.create(:investment, :escrowed, offering_id: offering.id, security: spv_security, amount: 23000, user_id: user.id, investor_entity_id: investor_entity.id)
      end

      scenario 'investment COMMITTED and below direct threshold', js: true do
        fill_in_investment_amount('22000')
        expect(page).to have_case_insensitive_content('You will own: 0.904% of the Company')
      end
    end

    context 'SPV total investments NOT totalling $23K + $22K = $45K > SPV min of $40K' do
      before do
        spv_security.update_attributes(minimum_total_investment: 40000)
        offering.reload
        FactoryGirl.create(:investment, offering_id: offering.id, security: spv_security, amount: 23000, user_id: user.id)
      end

      scenario 'investment NOT COMMITTED and below direct threshold', js: true do
        fill_in_investment_amount('22000')
        expect(page).to have_case_insensitive_content('You will own: 0.880% of the Company')
      end
    end
  end
end
