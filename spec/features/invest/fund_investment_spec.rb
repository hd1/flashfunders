require 'rails_helper'

feature 'Investor transfers funds', :js do
  let!(:user) { FactoryGirl.create(:user) }
  let!(:entity) { FactoryGirl.create(:investor_entity_individual, :with_pfii) }
  let!(:offering) { FactoryGirl.create(:fsp_only_offering, user: user) }
  let!(:investment) { FactoryGirl.create(:investment, user: user, offering: offering, security: offering.reg_d_direct_security, investor_entity_id: entity.id, state: 'signed_documents') }

  before do
    sign_in_user(user)

    offering.reg_d_direct_security.update_attributes(fund_america_id: 'iS2dFdP3QteQAp7J9RvrzQ')
    InvestmentEvent::SignDocumentsEvent.create! investment_id: investment.id
    Bank::InvestorAccount.create! user_id: user.id, account_number: '1234', account_type: 'Checking'
    Bank::InternalAccount.create! user_id: user.id, account_number: '1234', account_type: 'Checking'
  end

  context 'for a Reg-C enabled offering' do
    before do
      FactoryGirl.create(:cf_stock, offering: offering)
      offering.save
      offering.reload
      allow(RoutingNumber).to receive(:find_by_routing_number).and_return(double(RoutingNumber, name: 'Some bank name'))
    end

    context 'with an International Reg-C investment' do
      before do
        investment.update_attributes(calc_data: { reg_s_investor: true }, security_id: offering.reg_c_security.id)
      end

      scenario 'by choosing to wire money' do
        VCR.use_cassette('transfer_by_wire_selection') do
          choose_wire(false)

          expect(page.current_path).to eq(complete_offering_investment_flow_fund_path(offering_id: offering.id))

          expect(page).to have_case_insensitive_content I18n.t('investor_transfer_investments.success.title')
          expect(page).to have_case_insensitive_content I18n.t('investor_transfer_investments.success.confirmation.wire', amount_due: dollar_amount(investment.amount))
          expect(page).to have_case_insensitive_content I18n.t('investment_dashboard.my_investments_tab.statuses.pending.title')
          expect(page).to have_case_insensitive_content I18n.t('investment_dashboard.my_investments_tab.wire.view_title')
          expect(page).to have_case_insensitive_content I18n.t('investor_transfer_investments.instructions.domestic_title')
          expect(page).to have_case_insensitive_content I18n.t('investor_transfer_investments.instructions.international_title')
        end
      end
    end

    context 'with an International Reg-D investment' do
      before do
        investment.update_attributes(calc_data: { reg_s_investor: true }, security_id: offering.reg_d_direct_security.id)
        allow_any_instance_of(Address).to receive(:address1).and_return('911')
      end

      scenario 'by choosing to wire money', percy: true do
        VCR.use_cassette('transfer_by_wire_selection') do
          choose_wire(false, skip_submit: true)

          Percy::Capybara.snapshot(page, name: 'reg_d_wire')
          submit_fund

          expect(page.current_path).to eq(complete_offering_investment_flow_fund_path(offering_id: offering.id))
          Percy::Capybara.snapshot(page, name: 'reg_d_wire_success')

          expect(page).to have_case_insensitive_content I18n.t('investor_transfer_investments.success.title')
          expect(page).to have_case_insensitive_content I18n.t('investor_transfer_investments.success.confirmation.reg_d_wire', amount_due: dollar_amount(investment.amount))
          expect(page).to have_case_insensitive_content I18n.t('investment_dashboard.my_investments_tab.statuses.pending.title')
          expect(page).to have_case_insensitive_content I18n.t('investment_dashboard.my_investments_tab.wire.view_title')
          expect(page).to have_case_insensitive_content I18n.t('investor_transfer_investments.instructions.domestic_title')
          expect(page).to have_case_insensitive_content I18n.t('investor_transfer_investments.instructions.international_title')
        end
      end
    end

    context 'with a Reg-C investment' do
      before do
        investment.update_attributes(calc_data: { reg_s_investor: false }, security_id: offering.reg_c_security.id)
      end

      context 'with an amount over $1,000' do
        scenario 'by choosing to wire money' do
          VCR.use_cassette('transfer_by_wire_selection') do
            choose_wire

            expect(page.current_path).to eq(complete_offering_investment_flow_fund_path(offering_id: offering.id))

            expect(page).to have_case_insensitive_content I18n.t('investor_transfer_investments.success.title')
            expect(page).to have_case_insensitive_content I18n.t('investor_transfer_investments.success.confirmation.wire', amount_due: dollar_amount(investment.amount))
            expect(page).to have_case_insensitive_content I18n.t('investment_dashboard.my_investments_tab.statuses.pending.title')
            expect(page).to have_case_insensitive_content I18n.t('investment_dashboard.my_investments_tab.wire.view_title')
            expect(page).to have_case_insensitive_content I18n.t('investor_transfer_investments.instructions.domestic_title')
            expect(page).to have_case_insensitive_content I18n.t('investor_transfer_investments.instructions.international_title')
          end
        end

        scenario 'by choosing ACH' do
          VCR.use_cassette('transfer_by_ach_selection', match_requests_on: [:method, :uri]) do
            choose_ach

            expect(page.current_path).to eq(complete_offering_investment_flow_fund_path(offering_id: offering.id))

            expect(page).to have_case_insensitive_content I18n.t('investor_transfer_investments.success.title')
            expect(page).to have_case_insensitive_content I18n.t('investor_transfer_investments.success.confirmation.ach', amount_due: dollar_amount(investment.amount))
            expect(page).to have_case_insensitive_content I18n.t('investment_dashboard.my_investments_tab.statuses.pending.title')
            expect(page).to_not have_case_insensitive_content I18n.t('investor_transfer_investments.instructions.international_title')
            expect(page).to_not have_link I18n.t('investment_documents.edit_button')
          end
        end
      end

      context 'with an amount under $1,000' do
        before do
          investment.update_attributes(amount: 100)
        end

        scenario 'by choosing ACH' do
          VCR.use_cassette('transfer_by_ach_selection', match_requests_on: [:method, :uri]) do
            choose_ach(false)

            expect(page.current_path).to eq(complete_offering_investment_flow_fund_path(offering_id: offering.id))

            expect(page).to have_case_insensitive_content I18n.t('investor_transfer_investments.success.title')
            expect(page).to have_case_insensitive_content I18n.t('investor_transfer_investments.success.confirmation.ach', amount_due: dollar_amount(investment.amount))
            expect(page).to have_case_insensitive_content I18n.t('investment_dashboard.my_investments_tab.statuses.pending.title')
            expect(page).to_not have_case_insensitive_content I18n.t('investor_transfer_investments.instructions.international_title')
            expect(page).to_not have_link I18n.t('investment_documents.edit_button')
          end
        end
      end

    end

    context 'with a Reg-D investment' do
      before do
        investment.update_attributes(calc_data: { reg_s_investor: false }, security_id: offering.reg_d_direct_security.id)
      end

      scenario 'by choosing to wire money' do
        VCR.use_cassette('transfer_by_wire_selection') do
          choose_wire

          expect(page.current_path).to eq(complete_offering_investment_flow_fund_path(offering_id: offering.id))

          expect(page).to have_case_insensitive_content I18n.t('investor_transfer_investments.success.title')
          expect(page).to have_case_insensitive_content I18n.t('investor_transfer_investments.success.confirmation.reg_d_wire', amount_due: dollar_amount(investment.amount))
          expect(page).to have_case_insensitive_content I18n.t('investment_dashboard.my_investments_tab.statuses.pending.title')
          expect(page).to have_case_insensitive_content I18n.t('investment_dashboard.my_investments_tab.wire.view_title')
          expect(page).to have_case_insensitive_content I18n.t('investor_transfer_investments.instructions.domestic_title')
          expect(page).to have_case_insensitive_content I18n.t('investor_transfer_investments.instructions.international_title')
        end
      end

      scenario 'by choosing ACH' do
        VCR.use_cassette('transfer_by_ach_selection', match_requests_on: [:method, :uri]) do
          choose_ach

          expect(page.current_path).to eq(complete_offering_investment_flow_fund_path(offering_id: offering.id))

          expect(page).to have_case_insensitive_content I18n.t('investor_transfer_investments.success.title')
          expect(page).to have_case_insensitive_content I18n.t('investor_transfer_investments.success.confirmation.reg_d_ach', amount_due: dollar_amount(investment.amount))
          expect(page).to have_case_insensitive_content I18n.t('investment_dashboard.my_investments_tab.statuses.pending.title')
          expect(page).to_not have_case_insensitive_content I18n.t('investor_transfer_investments.instructions.international_title')
          expect(page).to_not have_link I18n.t('investment_documents.edit_button')
        end
      end

    end
  end

  context 'for a fund america offering' do
    scenario 'by choosing to wire money' do
      VCR.use_cassette('transfer_by_wire_selection') do
        choose_to_wire_funds

        expect(page.current_path).to eq(complete_offering_invest_fund_path(offering_id: offering.id))

        expect(page).to have_case_insensitive_content I18n.t('investor_transfer_investments.success.title')
        expect(page).to have_case_insensitive_content I18n.t('investment_dashboard.my_investments_tab.statuses.pending.title')
        expect(page).to have_case_insensitive_content I18n.t('investment_dashboard.my_investments_tab.wire.view_title')
        expect(page).to have_case_insensitive_content I18n.t('investor_transfer_investments.instructions.domestic_title')
        expect(page).to have_case_insensitive_content I18n.t('investor_transfer_investments.instructions.international_title')
      end
    end

    scenario 'by choosing to send a check' do
      VCR.use_cassette('transfer_by_check_selection', match_requests_on: [:method, :uri]) do
        choose_to_send_check

        expect(page).to have_case_insensitive_content I18n.t('investor_transfer_investments.success.title')
        expect(page).to have_case_insensitive_content I18n.t('investment_dashboard.my_investments_tab.statuses.pending.title')
        expect(page).to have_case_insensitive_content I18n.t('investment_dashboard.my_investments_tab.check.view_title')
        expect(page).to_not have_case_insensitive_content I18n.t('investor_transfer_investments.instructions.international_title')
        expect(page).to have_link I18n.t('investment_documents.edit_button')
      end
    end
  end

  context 'for a first century offering' do
    before do
      ep = FactoryGirl.create(:escrow_provider_first_century)
      offering.securities.update_all("escrow_provider_id = #{ep.id}")
      visit new_offering_invest_fund_path(offering)
    end

    scenario 'by choosing to wire money' do
      choose_to_wire_funds

      expect(page).to have_case_insensitive_content I18n.t('investment_dashboard.my_investments_tab.wire.view_title')
      expect(page).to have_link I18n.t('investment_documents.edit_button')
      expect(page).to_not have_case_insensitive_content I18n.t('investor_transfer_investments.instructions.international_title')
      expect(page).to_not have_case_insensitive_content I18n.t('investor_transfer_investments.instructions.domestic_title')
    end
  end

  context 'for a manual offering' do
    before do
      ep = FactoryGirl.create(:escrow_provider_manual)
      offering.securities.update_all("escrow_provider_id = #{ep.id}")
    end

    scenario 'just link the refund account and continue' do
      visit new_offering_invest_fund_path(offering)

      fill_in_link_refund_account
      click_button I18n.t('fund.buttons.commit')

      expect(page).to have_case_insensitive_content I18n.t('investor_transfer_investments.success.title')
      expect(page).to_not have_case_insensitive_content I18n.t('investor_transfer_investments.instructions.international_title')
      expect(page).to_not have_case_insensitive_content I18n.t('investor_transfer_investments.instructions.domestic_title')
    end
  end

  def choose_to_wire_funds
    visit new_offering_invest_fund_path(offering)

    click_on(I18n.t('fund.wire.title'))
    fill_in_link_refund_account
    click_button I18n.t('fund.buttons.commit')
  end

  def choose_to_send_check
    visit new_offering_invest_fund_path(offering)

    click_on(I18n.t('fund.check.title'))
    fill_in_link_refund_account
    click_button I18n.t('fund.buttons.commit')
  end

  def choose_wire(do_select=true, opts={})
    visit new_offering_invest_fund_path(offering)

    click_on(I18n.t('fund_flow.wire.title')) if do_select
    fill_in_wire_refund_account('Some dude', '1234', '999999999', 'Checking')
    submit_fund unless opts[:skip_submit]
  end

  def choose_ach(do_select=true, opts={})
    visit new_offering_invest_fund_path(offering)

    click_on(I18n.t('fund_flow.ach.title')) if do_select
    fill_in_ach_refund_account('Some dude', '123456789', '999999999', 'Checking')
    submit_fund unless opts[:skip_submit]
  end

  def submit_fund
    click_button I18n.t('fund.buttons.commit')
  end

  def dollar_amount(amount)
    Currency.new(amount).to_s(precision: 2)
  end

end
