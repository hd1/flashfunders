require 'rails_helper'

feature 'Visitor updates the offering', js: true do

  let!(:company) { Company.create!(name: "Testco") }
  let!(:offering) { Offering.create!(issuer_user_id: issuer_user.id, company: company) }
  let!(:issuer_user) { create_user(email: 'issuer@example.com', password: 'password', password_confirmation: 'password') }
  let!(:regular_user) { create_user(email: 'user@example.com', password: 'password', password_confirmation: 'password') }

  let(:company_name) { 'Testco' }
  let(:public_contact_name) { 'Mary Contact' }
  let(:website_url) { 'http://www.example.com/companyco' }
  let(:tagline) { 'The fastest feature specs around!' }
  let(:location) { 'Santa Monica, California' }

  scenario 'by filling out the basic information form' do
    sign_in_user(issuer_user)
    visit edit_issuer_editor_detail_path(offering)

    fill_in 'Company Name', with: company_name
    fill_in 'Contact Person Name', with: public_contact_name
    fill_in 'Website', with: website_url
    fill_in 'Tagline', with: tagline
    fill_in 'Location', with: location

    click_on 'Save Changes'

    visit edit_issuer_editor_detail_path(offering)

    expect(page).to have_field('Company Name', with: company_name)
    expect(page).to have_field('Contact Person Name', with: public_contact_name)
    expect(page).to have_field('Tagline', with: tagline)
    expect(page).to have_field('Website', with: website_url)
    expect(page).to have_field('Location', with: location)
  end

  scenario 'by incorrectly filling out the website url' do
    sign_in_user(issuer_user)
    visit edit_issuer_editor_detail_path(offering)

    fill_in 'Company Name', with: company_name
    fill_in 'Contact Person Name', with: public_contact_name
    fill_in 'Website', with: "www.testco.com"
    fill_in 'Tagline', with: tagline
    fill_in 'Location', with: location

    click_on 'Save Changes'

    expect(page).to have_content('Provide URL in http://example.com format.')
  end

  # This is a test for the case where a user makes a change on the form, leaves, and is
  # prompted with a dialog asking whether he wants to save his changes. Since we cannot
  # capture that dialog in a jasmine test, we are going partway and just looking for the
  # class we add that causes the dialog to pop.
  scenario 'by leaving the page with unsaved changes' do
    sign_in_user(issuer_user)
    visit edit_issuer_editor_detail_path(offering)

    expect(page).not_to have_selector('.changed-input')

    fill_in 'Company Name', with: company_name

    expect(page).to have_selector('.changed-input')
  end
end
