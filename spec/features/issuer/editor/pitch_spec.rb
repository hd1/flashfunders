require 'rails_helper'

feature 'Visitor updates the offering pitch', js: true do

  let!(:company) { Company.create!(name: "Testco.") }
  let!(:offering) { Offering.create!(issuer_user_id: issuer_user.id, company: company) }
  let!(:issuer_user) { create_user(email: 'issuer@example.com', password: 'password', password_confirmation: 'password') }
  let!(:regular_user) { create_user(email: 'user@example.com', password: 'password', password_confirmation: 'password') }

  scenario 'by adding sections', js: true do
    sign_in_user(issuer_user)
    visit edit_issuer_editor_pitch_path(offering)

    add_new_section(title: 'Title X')
    add_text_content(0, content: 'This is a description')
    add_new_section(title: 'Title Y')
    add_text_content(1, content: 'This is also a description')

    click_on 'Save Changes'

    expect(page).to have_field('Section Title', with: 'Title X')
    expect(page).to have_selector('textarea.ckeditor', visible: false, text: 'This is a description')

    expect(page).to have_field('Section Title', with: 'Title Y')
    expect(page).to have_selector('textarea.ckeditor', visible: false, text: 'This is also a description')
  end

  scenario 'by adding a valid video url' do
    sign_in_user(issuer_user)
    visit edit_issuer_editor_pitch_path(offering)

    add_new_section(title: 'Video Content')
    add_video_url(0, content: 'https://www.vimeo.com')

    click_on 'Save Changes'

    expect(page).to have_field('Section Title', with: 'Video Content')
    expect(page).to have_field('Video URL', with: 'https://www.vimeo.com')
  end

  scenario 'by adding an invalid video url' do
    sign_in_user(issuer_user)
    visit edit_issuer_editor_pitch_path(offering)

    add_new_section(title: 'Video Content')
    add_video_url(0, content: 'http://www.vimeo.com')

    click_on 'Save Changes'

    expect(page).not_to have_field('Video URL', with: 'https://www.vimeo.com')
    expect(page).to have_content("Provide a valid URL in these schemes: https")
  end

  scenario 'by sorting sections' do
    sign_in_user(issuer_user)
    visit edit_issuer_editor_pitch_path(offering)

    add_new_section(title: 'Title X')
    add_new_section(title: 'Title Y')

    first(:css, '.down-rank').click

    sleep(1)  # wait for js to move items

    click_on 'Save Changes'

    within all('.section').first do
      expect(page).to have_field('Section Title', with: 'Title Y')
    end
  end

  scenario 'by deleting section' do
    sign_in_user(issuer_user)
    visit edit_issuer_editor_pitch_path(offering)

    add_new_section(title: 'Title X')
    add_new_section(title: 'Title Y')
    add_text_content(0, content: 'This is a description', html: false)

    click_on 'Save Changes'

    find('.remove-element[data-target=".section"]', match: :first).click

    click_on 'Save Changes'

    expect(page).to have_field('Section Title', with: 'Title Y')
    expect(page).to_not have_field('Section Title', with: 'Title X')
    expect(page).to_not have_content('This is a description')
  end

  scenario 'with invalid data' do
    sign_in_user(issuer_user)
    visit edit_issuer_editor_pitch_path(offering)

    add_new_section(title: '')
    add_text_content(0, content: '')

    add_new_section(title: '')
    add_text_content(1, content: '')

    click_on 'Save Changes'

    expect(page).to have_selector('div.field_with_errors', count: 4)
  end

  scenario 'without being the original issuer' do
    sign_in_user(regular_user)
    visit edit_issuer_editor_pitch_path(offering)
    expect(page).to have_content("404 WE'RE SORRY PAGE NOT FOUND")
  end

  # This is a test for the case where a user makes a change on the form, leaves, and is
  # prompted with a dialog asking whether he wants to save his changes. Since we cannot
  # capture that dialog in a jasmine test, we are going partway and just looking for the
  # class we add that causes the dialog to pop.
  scenario 'by leaving the page with unsaved changes' do
    sign_in_user(issuer_user)
    visit edit_issuer_editor_pitch_path(offering)

    expect(page).not_to have_selector('.changed-input')

    add_new_section(title: 'Title X')

    expect(page).to have_selector('.changed-input')
  end

  def add_new_section(content)
    click_on 'Add Pitch Section'
    populate_section(content)
  end

  def add_text_content(section_index, content = {})
    within all('.section')[section_index] do
      click_on 'Add Text'

      within all('.content-area').last do
        fill_in_ckeditor "Text", with: content[:content]
      end
    end
  end

  def add_video_url(section_index, content = {})
    within all('.section')[section_index] do
      click_on 'Add Video'

      within all('.content-area').last do
        fill_in 'Video URL', with: content[:content]
      end
    end
  end

  def populate_section(content)
    within all('.section').last do
      fill_in 'Section Title', with: content[:title]
    end
  end

  # Used to fill ckeditor fields
  # param [String] locator label text for the textarea or textarea id
  def fill_in_ckeditor(locator, params = {})
    # Find out ckeditor id at runtime using its label
    locator = find('label', text: locator)[:for] if page.has_css?('label', text: locator)
    # Fill the editor content
    page.execute_script <<-SCRIPT
        var ckeditor = CKEDITOR.instances.#{locator}
        ckeditor.setData('#{params[:with]}')
        ckeditor.focus()
        ckeditor.updateElement()
    SCRIPT
  end

end
