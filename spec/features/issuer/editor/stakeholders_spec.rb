require 'rails_helper'

feature 'User updates the stakeholders page', js: true do

  let!(:company) { Company.create!(name: "Testco.") }
  let!(:offering) { Offering.create!(issuer_user_id: issuer_user.id, company: company) }
  let!(:issuer_user) { create_user(email: 'issuer@example.com', password: 'password', password_confirmation: 'password') }
  let!(:regular_user) { create_user(email: 'user@example.com', password: 'password', password_confirmation: 'password') }

  scenario 'by adding data to the first team member' do
    sign_in_user(issuer_user)
    visit edit_issuer_editor_stakeholder_path(offering)

    click_button "Add Team Member"

    within('.team-members-container') do
      fill_in "Name", with: "Joe Schmo"
      fill_in "Position", with: "Head of Something"
      fill_in "Bio", with: "This is a very long piece of text"
      fill_in "LinkedIn", with: "http://www.linkedin.com/?p=param"
      fill_in "Twitter", with: "http://www.twitter.com?p=param"
      fill_in "Website", with: "http://www.testco.com"
      fill_in "Facebook", with: "http://www.facebook.com?p=param"
    end

    click_on 'Save Changes'

    expect(page).to have_field('stakeholders[team_members_attributes][0][full_name]', with: 'Joe Schmo')
  end

  scenario 'but cannot add an invalid linkedin_url' do
    sign_in_user(issuer_user)
    visit edit_issuer_editor_stakeholder_path(offering)

    click_button "Add Team Member"

    within('.team-members-container') do
      fill_in "Name", with: "Joe Schmo"
      fill_in "Position", with: "Head of Something"
      fill_in "Bio", with: "This is a very long piece of text"
      fill_in "LinkedIn", with: "www.linkedin.com/?p=param"
    end

    click_on 'Save Changes'

    expect(page).to have_content('Provide URL in http://example.com format.')
  end

  scenario 'user adds another stakeholder' do

    sign_in_user(issuer_user)

    visit edit_issuer_editor_stakeholder_path(offering)

    click_button "Add Team Member"
    click_button "Add Team Member" # Second team member since first is no longer present

    selector_root = page.all("input").entries.map{|f| $1 if f.path =~ /id\=\'(stakeholders_team_members_attributes_\d\d+)_full_name\'/ }.compact.last

    fill_in "#{selector_root}_full_name", with: "Second Full Name"
    fill_in "#{selector_root}_position", with: "Peon"
    fill_in "#{selector_root}_bio", with: "Bio 2"
    fill_in "#{selector_root}_linkedin_url", with: "http://www.linkedin.com"
    fill_in "#{selector_root}_twitter_url", with: "http://www.twitter.com"
    fill_in "#{selector_root}_website", with: "http://www.test.com"
    fill_in "#{selector_root}_facebook_url", with: "http://www.facebook.com"

    click_on 'Save Changes'
    expect(page).to have_field("stakeholders_team_members_attributes_1_full_name", with: 'Second Full Name')
    expect(page).to have_field("stakeholders_team_members_attributes_1_position", with: 'Peon')
    expect(page).to have_field("stakeholders_team_members_attributes_1_bio", with: 'Bio 2')
    expect(page).to have_field("stakeholders_team_members_attributes_1_linkedin_url", with: 'http://www.linkedin.com')
    expect(page).to have_field("stakeholders_team_members_attributes_1_twitter_url", with: 'http://www.twitter.com')
    expect(page).to have_field("stakeholders_team_members_attributes_1_website", with: 'http://www.test.com')
    expect(page).to have_field("stakeholders_team_members_attributes_1_facebook_url", with: 'http://www.facebook.com')

  end

  scenario 'by adding data to the first investor' do
    sign_in_user(issuer_user)
    visit edit_issuer_editor_stakeholder_path(offering)

    click_button "Add Person"

    within('.investors-container') do

      fill_in "Name", with: "Joe Schmo"
      fill_in "Position", with: "Head of Something"
      fill_in "Bio", with: "This is a very long piece of text"
      fill_in "LinkedIn", with: "http://www.linkedin.com/?p=param"
      fill_in "Twitter", with: "http://www.twitter.com?p=param"
      fill_in "Website", with: "http://www.testco.com"
      fill_in "Facebook", with: "http://www.facebook.com?p=param"
      find("label.notable-investor").click
    end

    click_on 'Save Changes'

    expect(page).to have_field('stakeholders[investors_attributes][0][full_name]', with: 'Joe Schmo')

    expect(offering.reload.notable_investor_id).to eq(Stakeholder.last.id)
  end

  # This is a test for the case where a user makes a change on the form, leaves, and is
  # prompted with a dialog asking whether he wants to save his changes. Since we cannot
  # capture that dialog in a jasmine test, we are going partway and just looking for the
  # class we add that causes the dialog to pop.
  scenario 'by leaving the page with unsaved changes' do
    sign_in_user(issuer_user)
    visit edit_issuer_editor_stakeholder_path(offering)

    expect(page).not_to have_selector('.changed-input')
    click_button "Add Person"

    within('.investors-container') do
      fill_in "Name", with: "Joe Schmos"
    end

    expect(page).to have_selector('.changed-input')
  end
end
