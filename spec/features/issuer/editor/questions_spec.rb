require 'rails_helper'

feature 'Visitor updates the offering FAQ', js: true do

  let!(:company) { Company.create!(name: "Testco.") }
  let!(:offering) { Offering.create!(issuer_user_id: issuer_user.id, company: company) }
  let!(:issuer_user) { create_user(email: 'issuer@example.com', password: 'password', password_confirmation: 'password') }
  let!(:regular_user) { create_user(email: 'user@example.com', password: 'password', password_confirmation: 'password') }

  scenario 'by adding sections' do
    sign_in_user(issuer_user)
    visit edit_issuer_editor_question_path(offering)

    add_new_question(question: 'Question A', answer: 'Answer A')
    add_new_question(question: 'Question B', answer: 'Answer B')

    click_on 'Save Changes'

    expect(page).to have_field('Question', with: 'Question A')
    expect(page).to have_field('Answer', with: 'Answer A')

    expect(page).to have_field('Question', with: 'Question B')
    expect(page).to have_field('Answer', with: 'Answer B')
  end

  scenario 'by sorting sections' do
    sign_in_user(issuer_user)
    visit edit_issuer_editor_question_path(offering)

    add_new_question(question: 'Question A', answer: 'Answer A')
    add_new_question(question: 'Question B', answer: 'Answer B')

    first(:css, '.down-rank').click

    sleep(1)  # wait for js to move items

    click_on 'Save Changes'

    within all('.section').first do
      expect(page).to have_field('Question', with: 'Question B')
      expect(page).to have_field('Answer', with: 'Answer B')
    end
  end

  scenario 'by deleting section' do
    sign_in_user(issuer_user)
    visit edit_issuer_editor_question_path(offering)

    add_new_question(question: 'Question A', answer: 'Answer A')
    add_new_question(question: 'Question B', answer: 'Answer B')

    click_on 'Save Changes'

    first(:css, '.remove-element').click

    click_on 'Save Changes'

    expect(page).to have_field('Question', with: 'Question B')
    expect(page).to have_field('Answer', with: 'Answer B')
    expect(page).to_not have_field('Question', with: 'Question A')
    expect(page).to_not have_field('Answer', with: 'Answer A')
  end

  scenario 'with invalid data' do
    sign_in_user(issuer_user)
    visit edit_issuer_editor_question_path(offering)

    add_new_question(question: '', answer: '')
    add_new_question(question: '', answer: '')

    click_on 'Save Changes'

    expect(page).to have_selector('div.field_with_errors', count: 4)
  end

  scenario 'without being the original issuer' do
    sign_in_user(regular_user)
    visit edit_issuer_editor_pitch_path(offering)
    expect(page).to have_content("404 WE'RE SORRY PAGE NOT FOUND")
  end

  # This is a test for the case where a user makes a change on the form, leaves, and is
  # prompted with a dialog asking whether he wants to save his changes. Since we cannot
  # capture that dialog in a jasmine test, we are going partway and just looking for the
  # class we add that causes the dialog to pop.
  scenario 'by leaving the page with unsaved changes' do
    sign_in_user(issuer_user)
    visit edit_issuer_editor_question_path(offering)

    expect(page).not_to have_selector('.changed-input')

    add_new_question(question: 'Question B', answer: 'Answer B')

    expect(page).to have_selector('.changed-input')
  end

  def add_new_question(content)
    click_on 'Add Question'
    populate_section(content)
  end

  def populate_section(content)
    within all('.section').last do
      fill_in 'Question', with: content[:question]
      fill_in 'Answer', with: content[:answer]
    end
  end

end
