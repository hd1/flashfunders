require 'rails_helper'

feature 'Visitor updates the offering pitch', js: true do

  let!(:company) { Company.create!(name: "Testco.") }
  let!(:offering) { Offering.create!(issuer_user_id: issuer_user.id, company: company) }
  let!(:issuer_user) { create_user(email: 'issuer@example.com', password: 'password', password_confirmation: 'password') }
  let!(:regular_user) { create_user(email: 'user@example.com', password: 'password', password_confirmation: 'password') }

  scenario 'by trying to save a document' do
    sign_in_user(issuer_user)
    visit edit_issuer_editor_pitch_path(offering)
    click_on 'Documents'
    first('.btn-dashed').click  
    click_on 'Save Changes'
    expect(page).to have_content('The following information is missing or invalid:')
  end

end
