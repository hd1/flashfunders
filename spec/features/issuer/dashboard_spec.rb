require 'rails_helper'
require 'issuer_analytics'

feature 'Issuer Dashboard', :stub_warden do
  let!(:user) { FactoryGirl.create(:user, confirmed_at: 1.day.ago) }
  let!(:investor) { create_user(email: 'user@example.com', password: 'thepassword', password_confirmation: 'thepassword', authorized_to_invest_status: :allowed) }
  let!(:reg_d_security) { FactoryGirl.create(:fsp_stock,
                                                    shares_offered: 100000,
                                                    maximum_total_investment: 2000000,
                                                    minimum_total_investment: 1000000,
                                                    minimum_investment_amount: 100,)
  }

  let!(:other_security) { FactoryGirl.create(:fsp_stock,
                                             is_spv: true,
                                             shares_offered: 100000,
                                             maximum_total_investment: 2000000,
                                             minimum_total_investment: 1000000,
                                             minimum_investment_amount: 100,)
  }

  let!(:offering) { FactoryGirl.create(:offering,
                                       user:                        user,
                                       securities:                  [reg_d_security, other_security],
                                       ends_at:                     60.days.from_now,
                                       share_price: 1,
                                       tagline:                     'Raymondo Mondo Taco',
                                       visibility:                  :visibility_public,
                                       fully_diluted_shares:        10000) }
  let!(:qualification) { Accreditor::ThirdPartyIncomeQualification.new(user_id: user.id, email: "cpa@email.com", role: "Certified Public Accountant") }
  let!(:entity) { FactoryGirl.create(:investor_entity_individual, :with_pfii, accreditor_qualifications: [qualification]) }

  before do
    sign_in_user(user)
    allow(Google::Auth).to receive(:get_application_default).and_return(nil)
    ENV['GOOGLE_PROFILE_ID'] = 'XXXXXXXX' # Use dummy profile id
    VCR.insert_cassette("google_analytics_page_visit_info", record: :none, match_requests_on: [:method])
    VCR.insert_cassette("google_analytics_referral_info", record: :none, match_requests_on: [:method], allow_playback_repeats: true )
  end

  after do
    2.times { VCR.eject_cassette }
  end

  context 'With no investments' do

    it 'should not show Investment Activity' do
      visit issuer_dashboard_path(offering)

      expect(page).to have_case_insensitive_content(I18n.t('issuer-dashboard.investments.none'))
    end
  end

  context 'With < 10 investments, all escrowed' do
    before do
      5.times do |i|
        FactoryGirl.create(:investment,
                           updated_at:      Date.parse("12/#{i+1}/2015"),
                           docs_signed_at:  Date.parse("11/#{i+1}/2015"),
                           escrowed_at:     Date.parse("12/#{i+1}/2015"),
                           amount:          (i+1) * 10000,
                           user:            investor,
                           offering:        offering,
                           security:        reg_d_security,
                           state:           'escrowed',
                           investor_entity: entity).save
      end
    end

    scenario 'check correct display' do
      step 'should not show pagination' do
        visit issuer_dashboard_path(offering)

        expect(page).to_not have_case_insensitive_content('next')
      end

      step 'should show the correct total' do
        expect(page).to have_content('$150,000')
      end
    end

  end

  context 'With 20 investments' do
    before do
      20.times do |i|
        FactoryGirl.create(:investment,
                           updated_at:      Date.parse("12/#{i+1}/2015"),
                           amount:          (i+1) * 10000,
                           user:            investor,
                           offering:        offering,
                           investor_entity: entity).save
      end
    end

    scenario 'user should see...' do
      step 'first 10 are displayed ordered by Active descending' do
        visit issuer_dashboard_path(offering)

        within("#issuer-investments") do
          expect(page).to have_content('12/20')
          expect(page).to_not have_content('12/01')
          expect(page).to have_content('1 - 10 of 20')
        end
      end

      step 'clicking on Active shows first 10 ordered by Active ascending' do
        click_on 'active'

        within("#issuer-investments") do
          expect(page).to_not have_content('12/20')
          expect(page).to have_content('12/01')
        end
      end

      step 'clicking on amount sorts by amount descending' do
        click_on 'amount'

        within("#issuer-investments") do
          expect(page).to_not have_content('$10,000.00')
          expect(page).to have_content('$200,000.00')
        end
      end

      step 'test pagination' do
        visit issuer_dashboard_path(offering)

        expect(page).to have_content('1 - 10 of 20')

        click_on 'Next'

        expect(page).to have_content('11 - 20 of 20')
      end
    end
  end

  context 'With 10 investments' do
    before do
      10.times do |i|
        FactoryGirl.create(:investment,
                           updated_at:      Date.parse("12/#{i+1}/2015"),
                           amount:          (i+1) * 10000,
                           user:            investor,
                           offering:        offering,
                           security:        reg_d_security,
                           investor_entity: entity).save
      end
    end

    context 'and a never viewed dashboard' do
      before do
        offering.dashboard_last_viewed_at = nil
        offering.save
      end

      scenario 'issuer views dashboard' do
        step 'On first visit, all should be highlighted' do
          visit issuer_dashboard_path(offering)

          expect(all('tr.highlight').count).to eq(10)
        end

        step 'On next visit, none should be highlighted' do
          visit issuer_dashboard_path(offering)

          expect(all('tr.highlight').count).to eq(0)
        end
      end
    end

    context 'and a recently viewed dashboard' do
      before do
        offering.dashboard_last_viewed_at = Time.parse('12/5/2015 12:25')
        offering.save
      end

      it 'should highlight the most recent five' do
        visit issuer_dashboard_path(offering)

        expect(all('tr.highlight').count).to eq(5)
      end
    end
  end

  context "Top Referrer Info" do

    before do
      visit issuer_dashboard_path(offering)
    end

    it "should display the section for top referrers" do
      expect(page).to have_content(I18n.t("issuer-dashboard.top-traffic.title"))
      expect(page).to_not have_content(I18n.t('issuer-dashboard.top-traffic.no-traffic.title'))
      within('#top-traffic .items') do

        # Values from the VCR response
        ["hs_email", "(direct)", "linkedin.com", "Likeminder Updates 2015", "digital.nyc", "lqdwifi.com"].each do |source|
          expect(page).to have_content(source)
        end
      end

      within('#top-traffic .totals') do
        # Value from VCR response
        expect(page).to have_content("146 " + I18n.t('issuer-dashboard.top-traffic.total-page-views'))
      end

    end

    context "when there is no data" do
      before do
        ia = Object.new
        allow(IssuerAnalytics).to receive(:new).and_return(ia)
        allow(ia).to receive(:referral_info).and_return({})
        allow(ia).to receive(:page_visit_info).and_return({})
        $redis.flushall # make sure we don't use cached data
        visit issuer_dashboard_path(offering)

      end

      it "displays empty view when there is no data" do
        expect(page).to have_content(I18n.t('issuer-dashboard.top-traffic.no-traffic.title'))
      end
    end

  end

end
