require 'rails_helper'

feature 'User Forgot Password', :js do
  let(:partially_registered_user) { create_temporary_user(email: 'user@partial.com') }
  let(:unconfirmed_user) { create_unconfirmed_user(email: 'user@unconfirmed.com') }
  let(:confirmed_user) { create_user(email: 'user@confirmed.com') }

  context 'As an unregistered user' do
    it 'directs the user to the correct page' do
      visit new_user_password_path

      fill_in 'user_email', with: 'user@unregistered.com'

      click_on I18n.t('recoverable.forgot_password.commit')

      expect(current_path).to eq(user_password_path)
      expect(page).to have_content('not found')
    end
  end

  context 'As a partially registered user' do
    it 'directs the user to the correct page' do
      visit new_user_password_path

      fill_in 'user_email', with: partially_registered_user.email

      click_on I18n.t('recoverable.forgot_password.commit')

      expect(current_path).to eq(new_user_confirmation_path)
      expect(page).to have_content(I18n.t('devise.confirmations.user.partially_registered'))
    end
  end

  context 'As an unconfirmed user' do
    it 'directs the user to the correct page' do
      visit new_user_password_path

      fill_in 'user_email', with: unconfirmed_user.email

      click_on I18n.t('recoverable.forgot_password.commit')

      expect(current_path).to eq(new_user_session_path)
      expect(page).to have_content(I18n.t('devise.passwords.send_instructions'))
    end
  end

  context 'As a confirmed user' do
    it 'directs the user to the correct page' do
      visit new_user_password_path

      fill_in 'user_email', with: confirmed_user.email

      click_on I18n.t('recoverable.forgot_password.commit')

      expect(current_path).to eq(new_user_session_path)
      expect(page).to have_content(I18n.t('devise.passwords.send_instructions'))
    end
  end
end