require 'rails_helper'

describe StoreAccessorBoolean::ActiveRecordExtensions do

  before do
    class StoreAccessorBooleanTest < ActiveRecord::Base
      ActiveRecord::Base.connection.create_table(:store_accessor_boolean_tests, :force => true) do |t|
        t.hstore :data
        t.hstore :more_data
      end

      extend StoreAccessorBoolean::ActiveRecordExtensions
      store_accessor_boolean :data, :is_a_thing
      store_accessor :more_data, :is_another_thing
    end
  end

  describe "#store_accessor_boolean" do
    it 'provides the interrogative method' do
      book = StoreAccessorBooleanTest.new(is_a_thing: true, is_another_thing: true)

      expect(book).to respond_to(:is_a_thing?)
      expect(book.is_a_thing?).to be_truthy

      expect(book).not_to respond_to(:is_another_thing?)
    end
  end
end
