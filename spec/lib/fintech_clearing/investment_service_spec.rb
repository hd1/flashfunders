require 'rails_helper'

module FintechClearing
  describe InvestmentService do
    let(:default_investment_attrs) do
      {
        investment: {
          uuid: '',
          offering_uuid: 'd9175bf5-59f8-4016-acbf-94de6a65432f',
          amount: '12399.0',
          funding_method: 'funding_method_ach',
        }
      }
    end

    describe '#create' do
      it 'should successfully create an investment' do
        VCR.use_cassette('offering_open_for_investment') do
          investment = InvestmentService.new.create(default_investment_attrs)

          expect(investment.uuid).to be_present
          expect(investment.amount.to_s).to eq default_investment_attrs[:investment][:amount]
          expect(investment.funding_method).to eq default_investment_attrs[:investment][:funding_method]
        end
      end
    end

    describe '#update' do
      let(:updated_investment_attrs) do
        {
          investment: {
            bank_routing_number: '111111111',
            bank_account_type: 'bank_account_type_savings',
            investor_first_name: 'Patryk',
            investor_last_name: 'Kopec',
            investor_phone: '1112223333',
            investor_email: 'patryk+15@flashfunders.com',
            investor_address1: 'Thousandstreet',
            investor_address2: '',
            investor_city: 'Santa Monica',
            investor_state: 'CA',
            investor_country: 'US',
            investor_postal_code: '12345"'
          }
        }
      end

      before do
        VCR.use_cassette('offering_open_for_investment') do
          @investment = InvestmentService.new.create(default_investment_attrs)
        end
      end

      it 'should successfully update an investment' do
        updated_attrs = updated_investment_attrs
          .deep_merge(default_investment_attrs)
          .deep_merge(investment: {uuid: @investment.uuid})

        VCR.use_cassette('offering_open_for_investment') do
          investment = InvestmentService.new.update(updated_attrs)

          expect(investment.uuid).to eq @investment.uuid
          expect(investment.investor_first_name).to eq updated_investment_attrs[:investment][:investor_first_name]
          expect(investment.investor_last_name).to eq updated_investment_attrs[:investment][:investor_last_name]
        end
      end

    end
  end
end
