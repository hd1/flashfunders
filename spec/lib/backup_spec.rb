require 'rails_helper'

describe Backup do
  subject { Backup.new(double(Fog::Storage)).operational? }

  let(:bucket_dump) { [
      { "Owner"=>{},
        "Key"=>"pgbackups/production/2014-07-10-19-20-03-+0000.dump",
        "LastModified"=> 2.days.ago.to_datetime,
        "ETag"=>"c1a105bc356b95c5d63a070decf9ffa3",
        "Size"=>300,
        "StorageClass"=>"STANDARD"
      },
      { "Owner"=>{},
        "Key"=>"pgbackups/production/2014-07-11-19-20-03-+0000.dump",
        "LastModified"=> 2.weeks.ago.to_datetime,
        "ETag"=>"c1a105bc356b95c5d63a070decf9ffa3",
        "Size"=>415354,
        "StorageClass"=>"STANDARD"
      },
      { "Owner"=>{},
        "Key"=>"pgbackups/production/2014-07-11-20-20-03-+0000.dump",
        "LastModified"=> 3.weeks.ago.to_datetime,
        "ETag"=>"c1a105bc356b95c5d63a070decf9ffa3",
        "Size"=>415354,
        "StorageClass"=>"STANDARD"
      }  ] }
  before do
    allow_any_instance_of(Backup).to receive(:bucket_contents).and_return(bucket_dump)
  end

  context 'when a recent backup is present and is large enough' do
    it 'reports operational backups' do
      bucket_dump[1]["LastModified"] = 23.hours.ago.to_datetime
      expect(subject).to be_truthy
    end
  end

  context 'when a recent backup is not present' do
    it 'reports too-old backups' do
      expect(subject).to be_falsy
    end
  end

  context 'when a recent backup is present but is too small' do
    it 'reports too-small backups' do
      bucket_dump[0]["LastModified"] = 23.hours.ago.to_datetime
      expect(subject).to be_falsy
    end
  end

  context 'when no backups are found' do
    it 'reports no operational backups' do
      bucket_dump.each { |b| b["Key"] = 'x'}
      expect(subject).to be_falsy
    end
  end
end
