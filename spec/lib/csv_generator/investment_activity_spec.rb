require 'rails_helper'

describe CSVGenerator::InvestmentActivity do
  let(:investment_presenter) { InvestmentPresenter.new(FactoryGirl.create(:investment)) }
  let(:expected_columns) {
    [
      :formatted_updated_at,
      :investor_name,
      :investor_email,
      :formatted_amount,
      :created_at,
      :has_signed_docs?,
      :escrowed?
    ]
  }

  before do
    expected_columns.each do |column|
      allow(investment_presenter).to receive(column)
    end
  end

  it 'returns an investment activity row' do
    CSVGenerator::InvestmentActivity.get_row(investment_presenter)

    expected_columns.each do |column|
      expect(investment_presenter).to have_received(column)
    end
  end
end
