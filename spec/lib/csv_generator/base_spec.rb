require 'rails_helper'

describe CSVGenerator::Base do
  let(:file_suffix) { "_#{Time.zone.now.strftime('%F')}.csv" }

  after do
    CSVGenerator.send(:remove_const, :ComplexNum) if defined? CSVGenerator::ComplexNum
  end

  describe '#initialize' do
    let(:columns) { %w(a b c d e) }
    let(:resources) { [1, 2, 3, 4, 5] }

    before { class CSVGenerator::SomeReport; end }

    it 'cannot initialize a csv generator for an undefined report' do
      expect {
        CSVGenerator::Base.new(:undefined_report, columns, resources)
      }.to raise_error(NameError)
    end

    it 'can initialize a csv generator for a defined report' do
      expect {
        CSVGenerator::Base.new(:some_report, columns, resources)
      }.to_not raise_error
    end

    describe '#set_filename' do
      before do
        class CSVGenerator::SomeReport
          def self.filename(opts); opts[:custom] end
        end
      end

      it 'sets a default filename' do
        filename = CSVGenerator::Base.new(:some_report, columns, resources).filename
        expect(filename).to eq("report" + file_suffix)
      end

      it 'can set a custom filename' do
        filename = CSVGenerator::Base.new(:some_report, columns, resources, custom: 'custom').filename
        expect(filename).to eq('custom' + file_suffix)
      end
    end
  end

  describe '#generate' do
    let(:columns) { %w(real imaginary conjugate) }
    let(:resources) { 5.times.map(&:i) }

    context 'undefined #get_row' do
      shared_examples_for 'undefined get_row' do
        it 'returns nil data' do
          csv_data = CSVGenerator::Base.new(:complex_num, columns, resources).generate

          expect(csv_data).to be_nil
        end
      end

      context 'undefined #permitted_resources' do
        before do
          class CSVGenerator::ComplexNum
          end
        end

        it_behaves_like 'undefined get_row'
      end

      context 'defined #permitted_resources' do
        before do
          class CSVGenerator::ComplexNum
            def self.permitted_resources(complex)
              [Complex]
            end
          end
        end

        it_behaves_like 'undefined get_row'
      end
    end

    context 'defined #get_row' do
      context 'undefined #permitted_resources' do
        before do
          class CSVGenerator::ComplexNum
            def self.get_row(complex)
              [complex.real, complex.imaginary, complex.conjugate]
            end
          end
        end

        it 'only returns columns' do
          csv_data = CSVGenerator::Base.new(:complex_num, columns, resources).generate

          expect(csv_data).to eq(columns.to_csv)
        end
      end

      context 'defined #permitted_resources' do
        before do
          class CSVGenerator::ComplexNum
            def self.get_row(c)
              [c.real, c.imaginary, c.conjugate]
            end

            def self.permitted_resources
              [Complex]
            end
          end
        end

        it 'returns full data' do
          csv_data = CSVGenerator::Base.new(:complex_num, columns, resources).generate
          csv_rows = resources.map{ |r| [r.real, r.imaginary, r.conjugate].to_csv }.join

          expect(csv_data).to eq(columns.to_csv + csv_rows)
        end
      end

    end
  end

  describe '#safe_resources' do
    let(:columns) { %w(a b c d e) }
    let(:resources) { ['1', [2], {a: 3}, 4.i, 5.i] }

    before do
      class CSVGenerator::ComplexNum
        def self.permitted_resources
          [Complex]
        end
      end
    end

    it 'only returns permitted_resources resources' do
      generator = CSVGenerator::Base.new(:complex_num, columns, resources)
      expect(generator.send(:safe_resources)).to eq([4.i, 5.i])
    end
  end

end
