require 'rails_helper'

describe Serializable do

  before do
    allow(Serializable).to receive(:lock)
    allow(Serializable).to receive(:unlock)
  end

  shared_examples_for 'serializable' do
    it 'uses the actual key to lock/unlock' do

      expect(Serializable).to receive(:lock).with(val)
      expect(Serializable).to receive(:unlock).with(val)
      expect(self).to receive(:puts).with(key)

      if trigger_exception
        expect { test_serialization(key, true) }.to raise_error
      else
        test_serialization(key)
      end
    end
  end

  let (:trigger_exception) { false }

  context 'with a numeric key' do
    let(:key) { 123 }
    let(:val) { 123 }

    it_behaves_like 'serializable'
  end

  context 'with a non-numeric key' do
    let(:key) { 'Key_#123' }
    let(:val) { 315226332344976342 }

    it_behaves_like 'serializable'
  end

  context 'with an error' do
    let(:key) { 'Key_#123' }
    let(:val) { 315226332344976342 }
    let(:trigger_exception) { true }

    it_behaves_like 'serializable'
  end

  def test_serialization(key, raise_error=false)
    Serializable.with_serialization(key) do
      puts(key)
      raise 'some error' if raise_error
    end
  end

end
