require 'rails_helper'

describe "Check review for new ENV vars" do

  before do
    allow_any_instance_of(CheckEnvironmentVariables).to receive(:add_and_fetch_remote).and_return(nil)
    allow_any_instance_of(CheckEnvironmentVariables).to receive(:remote_environment_variable_keys).and_return([])
    allow_any_instance_of(CheckEnvironmentVariables).to receive(:diff).and_return("diff --git a/README.rdoc b/README.rdoc\nindex 5d5fcd4..7fd7575 100644\n--- a/README.rdoc\n+++ b/README.rdoc\n@@ -1,2 +0,0 @@\n-ENV['BRAD'] = 'beaten at ping pong' ENV['SYS'] = 'here'\n-\n")
    @c = CheckEnvironmentVariables.new('review')
  end

  it "should find local vars that have not been added to review" do
    expect(@c.unmatched_keys.sort).to eq ['BRAD', 'SYS']
    expect(@c.unmatched_keys?).to eq(true)
  end
end
