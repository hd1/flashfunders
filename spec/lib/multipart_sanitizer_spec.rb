require 'rails_helper'

describe MultipartSanitizer do
  def env_for url, opts={}
    Rack::MockRequest.env_for(url, opts)
  end

  let(:app) { ->(env) { [200, env, "app"] } }

  let :sanitizer do
    MultipartSanitizer.new(app)
  end

  context '#when content-type header is multipart/form-data' do
    before do
      mock_env = env_for('https://www.flashfunders.com/sites/all/libraries/elfinder/php/connector.minimal.php')
      mock_env["CONTENT_TYPE"] = "multipart/form-data; boundary=(UploadBoundary)"
      @status, @env, _  = sanitizer.call(mock_env)
    end

    it "should remove empty data in content-type header" do
      expect(@env['CONTENT_TYPE']).to eql("multipart/form-data")
    end
  end

end
