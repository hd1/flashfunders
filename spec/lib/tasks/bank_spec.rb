require 'rails_helper'
require 'rake'

describe 'bank escrow rake tasks' do
  before :all do
    Rake.application.rake_require 'tasks/bank'
    Rake::Task.define_task(:environment)
  end

  describe 'bank:update_escrows' do
    let :run_rake_task do
      Rake::Task['bank:update_escrows'].reenable
      Rake.application.invoke_task 'bank:update_escrows'
    end

    it 'updates the state escrow of the investment to escrowed' do

      escrow_updater = double(EscrowUpdater)
      expect(EscrowUpdater).to receive(:new).and_return(escrow_updater)

      expect(escrow_updater).to receive(:fetch_and_process_all_escrows)

      run_rake_task
    end
  end
end