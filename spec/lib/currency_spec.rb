require 'rails_helper'

describe Currency do

  context 'when currency options are provided to string conversion' do
    let(:options) { {precision: 0} }
    let(:currency) { Currency.new(BigDecimal("123000.99")) }

    it 'is passed through to number_to_currency method' do
      expect(currency.to_s(options)).to eql("$123,001")
      expect(currency.to_str(options)).to eql("$123,001")
    end
  end

  context 'when a parsable string is provided' do
    let(:currency) { Currency.new("$1,000.00") }

    it 'parses string value' do
      expect(currency.amount).to eql(1000.00)
    end
  end

  context 'when a non-parsable string is provided' do
    let(:currency) { Currency.new("$asdf") }

    it 'sets amount to zero' do
      expect(currency.amount).to eql(0)
    end
  end

  describe '.parse' do
    it 'returns a Currency object' do
      currency = Currency.parse("0")
      expect(currency.amount).to eql(BigDecimal(0))

      currency = Currency.parse("$1")
      expect(currency.amount).to eql(BigDecimal(1))

      currency = Currency.parse("$1000")
      expect(currency.amount).to eql(BigDecimal(1000))

      currency = Currency.parse("$1,000,000")
      expect(currency.amount).to eql(BigDecimal(1000000))

      currency = Currency.parse("$1,000.99")
      expect(currency.amount).to eql(BigDecimal("1000.99"))
    end

    it 'raises a TypeError' do
      expect{ Currency.parse("$") }.to raise_error(TypeError)
      expect{ Currency.parse("abc") }.to raise_error(TypeError)
      expect{ Currency.parse("$$100") }.to raise_error(TypeError)
      expect{ Currency.parse("1.000.00") }.to raise_error(TypeError)
      expect{ Currency.parse("$1000.abc") }.to raise_error(TypeError)
    end
  end
end

