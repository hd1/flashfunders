require 'rails_helper'

describe InvestorMailer, type: :mailer do
  let(:user) { double(User, id: 1, full_name: nil, registration_name: 'John Doe', email: 'jdoe@example.com', personal_federal_identification_information: nil, user_type: :user_type_investor) }
  let(:mailer) { InvestorMailer.checking_in(user.id) }

  before do
    allow(User).to receive(:find).and_return(user)
    allow(user).to receive_message_chain(:offering_messages, :count).and_return(0)
    allow(user).to receive_message_chain(:investments, :count).and_return(0)
    allow(user).to receive(:user_type_investor?).and_return(true)
  end

  before(:each) do
    ActionMailer::Base.deliveries = []
  end

  context 'User registered as investor and does not have any investments or messages' do

    describe '#checking_in' do
      it 'has the correct mail attributes' do
        expect(mailer.subject).to eq I18n.t('investor_mailer.checking_in.subject')
        expect(mailer.to).to include user.email
        expect(mailer.from).to include I18n.t('contacts.investors')
        expect(mailer.header.to_s).to match(/X-SMTPAPI.*InvestorMailer-checking_in/)
      end

      it 'has all significant links in email body' do
        expect(mailer.body).to have_content(I18n.t('investor_mailer.checking_in.title'))
        expect(mailer.body).to match browse_url
        expect(mailer.body).to have_content(I18n.t('contacts.investors'))
      end

      it 'can be delivered' do
        mailer.deliver_later
        expect(ActionMailer::Base.deliveries).to_not be_empty
      end
    end
  end

  context 'User registered as investor and sent a message' do

    before do
      allow(user).to receive_message_chain(:offering_messages, :count).and_return(1)
    end

    describe '#checking_in' do
      it 'will not be delivered' do
        mailer.deliver_now
        expect(ActionMailer::Base.deliveries).to be_empty
      end
    end
  end

  context 'User registered as investor and made an investments' do

    before do
      allow(user).to receive_message_chain(:investments, :count).and_return(1)
    end

    describe '#checking_in' do
      it 'will not be delivered' do
        mailer.deliver_now
        expect(ActionMailer::Base.deliveries).to be_empty
      end
    end
  end

  context 'User did not register as an investor' do

    before do
      allow(user).to receive(:user_type_investor?).and_return(false)
    end

    describe '#checking_in' do
      it 'will not be delivered' do
        mailer.deliver_now
        expect(ActionMailer::Base.deliveries).to be_empty
      end
    end
  end

end
