require 'rails_helper'

describe AccreditationMailer do

  let(:uuid) { SecureRandom.uuid }
  let(:email) { 'test@example.com' }
  let(:investor_name) { 'John Doe' }
  let(:investor_email) { 'johndoe@example.com' }
  let(:options) { {investor_name: investor_name, investor_email: investor_email} }
  let(:qualification) { double(Accreditor::Qualification, email: email, uuid: uuid) }
  let(:subject_line) { I18n.t('accreditation_mailer.third_party.person_verification.subject') }
  let(:entity_name) { 'John Doe, LLC' }
  let(:options) { {investor_name: investor_name, investor_email: investor_email, entity_name: entity_name} }


  describe '#third_party_person_verification_email' do
    let(:mailer) { AccreditationMailer.third_party_person_verification_email(qualification, options) }

    it 'should have the correct mail attributes' do
      expect(mailer.subject).to eq subject_line
      expect(mailer.to).to eq [email]
      expect(mailer.cc).to eq [investor_email]
      expect(mailer.reply_to).to eq [investor_email]
      expect(mailer.from).to eq [I18n.t('contacts.accreditation')]
      expect(mailer.header.to_s).to match(/X-SMTPAPI.*AccreditationMailer-third_party_person_verification_email/)
    end

    it 'can be delivered' do
      mailer.deliver_now
      expect(ActionMailer::Base.deliveries).to_not be_empty
    end
  end

  describe '#third_party_entity_verification_email' do
    let(:mailer) { AccreditationMailer.third_party_entity_verification_email(qualification, options) }

    it 'should have the correct mail attributes' do
      expect(mailer.subject).to eq subject_line
      expect(mailer.to).to eq [email]
      expect(mailer.cc).to eq [investor_email]
      expect(mailer.reply_to).to eq [investor_email]
      expect(mailer.from).to eq [I18n.t('contacts.accreditation')]
      expect(mailer.header.to_s).to match(/X-SMTPAPI.*AccreditationMailer-third_party_entity_verification_email/)
    end

    it 'can be delivered' do
      mailer.deliver_now
      expect(ActionMailer::Base.deliveries).to_not be_empty
    end
  end

end
