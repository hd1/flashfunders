require 'rails_helper'

describe InvestmentMailer do
  describe '#funds_received' do
    let(:security_1) { Securities::Security.new(minimum_total_investment: 100000, is_spv: false) }
    let(:security_2) { Securities::Security.new(minimum_total_investment: 200000, is_spv: true) }
    let(:subject_line) { I18n.t('investment_mailer.funds_received.subject') }
    let(:user) { double(User, email: "bigbux@example.com", full_name: "Stanley Bigbux" ) }
    let(:facebook_presenter) { double(FacebookPresenter, offering_url: "www.facebook-for-cats.com") }
    let(:twitter_presenter) { double(TwitterPresenter, offering_url: "www.twitter-is-fun.com") }
    let(:linkedin_presenter) { double(LinkedInPresenter, offering_url: "www.twitter-is-fun.com") }
    let(:social_presenter) { OpenStruct.new(facebook: facebook_presenter, twitter: twitter_presenter, linkedin: linkedin_presenter) }
    let(:company) { double(Company, name: "FakeCo.") }
    let(:offering) { FactoryGirl.create(:offering, id: 33, tagline: "Not a scam!",
                            securities: [security_1], ends_at: Date.current + 1) }
    let(:offering2) { FactoryGirl.create(:offering, id: 34, tagline: "Not a scam!",
                             securities: [security_2], ends_at: Date.current + 2) }
    let(:investment) { FactoryGirl.create(:investment, offering_id: offering.id, user_id: 1, security_id: security_1.id, amount: '10000', accreditation_status_id: 2) }
    let(:investment2) { FactoryGirl.create(:investment, offering_id: offering2.id, user_id: 1, security_id: security_2.id, amount: '10000', accreditation_status_id: 2) }

    subject(:email)  { InvestmentMailer.funds_received(investment.id) }
    subject(:email2) { InvestmentMailer.funds_received(investment2.id) }

    before do
      allow_any_instance_of(SecurityPresenters::Security).to receive_messages(accredited_balance: 150000, formatted_accredited_balance: "$150,000")
      allow(SocialPresenterFactory).to receive(:build).and_return(social_presenter)
      allow(Offering).to receive(:find).and_return(offering)
      allow(User).to receive(:find).and_return(user)
      allow(Offering).to receive(:find).and_return(offering)
      allow_any_instance_of(Investment).to receive(:user).and_return(user)
      allow_any_instance_of(Offering).to receive(:company).and_return(company)
      ActionMailer::Base.deliveries.clear
    end

    it 'delivers correct mail' do
      expect(email.subject).to eq subject_line
      expect(email.to).to eq [user.email]
      expect(email.from).to eq [I18n.t('contacts.investors')]

      expect(email.header.to_s).to match(/X-SMTPAPI.*InvestmentMailer-funds_received/)
    end

    it 'has the correct body' do
      expect(email.body).to have_content(I18n.t('investment_mailer.funds_received.goal_raised_html',
                                           company_name: company.name,
                                           offering_raised: '$150,000',
                                           offering_goal: '$100,000',
                                           days_left: 1))

      expect(email.body).to have_content(I18n.t('investment_mailer.funds_received.received_direct_html', company_name: company.name))
      expect(email.body).to have_content(I18n.t('investment_mailer.funds_received.date'))
      expect(email.body).to have_content(I18n.t('investment_mailer.funds_received.amount'))
      expect(email.body).to have_content(I18n.t('investment_mailer.funds_received.contact_html', email: I18n.t('contacts.investors')))
      expect(email2.body).to have_content(I18n.t('investment_mailer.funds_received.received_flashfund_html', company_name: company.name))
    end

    context 'For an spv investment' do
      it 'should display the correct company name' do
        expect(email2.body).to have_content(I18n.t('investment_mailer.funds_received.received_flashfund_html', company_name: company.name))
      end
    end

    context 'For a direct investment' do
      it 'should display the correct company name' do
        expect(email.body).to have_content(I18n.t('investment_mailer.funds_received.received_direct_html', company_name: company.name))
      end
    end

    context 'For a funded offering' do
      it 'should display the correct body' do
        expect(email.body).to have_content(I18n.t('investment_mailer.funds_received.goal_raised_html',
          company_name: company.name, days_left: 1, offering_raised: '$150,000', offering_goal: '$100,000'))
      end
    end

    context 'For an offering below the minimum' do
      before { allow(Offering).to receive(:find).and_return(offering2) }

      it 'should display the correct body' do
        expect(email2.body).to have_content(I18n.t('investment_mailer.funds_received.goal_not_raised_html',
          company_name: company.name, days_left: 2))
      end
    end
  end

  describe '#reminds_to_complete', type: :mailer do
    let(:subject_line) { I18n.t('investment_mailer.reminds_to_complete.subject') }
    let(:investment) { FactoryGirl.create(:investment, :intended) }
    subject(:email)  { InvestmentMailer.reminds_to_complete(investment.id) }

    it 'delivers correct email' do
      expect(email.subject).to eq subject_line
      expect(email.to).to eq [investment.user.email]
      expect(email.from).to eq [I18n.t('contacts.investors')]
      expect(email.header.to_s).to match(/X-SMTPAPI.*InvestmentMailer-reminds_to_complete/)
    end

    it 'has the correct body html part' do
      expect(email.html_part.body).to have_content(I18n.t('investment_mailer.reminds_to_complete.snazzy.overlay'))
      expect(email.html_part.body).to have_content(I18n.t('contacts.investors'))
      expect(email.html_part.body).to have_content(I18n.t('investment_mailer.reminds_to_complete.snazzy.p1', contact_investor: I18n.t('contacts.investors')))
      expect(email.html_part.body).to have_content(I18n.t('investment_mailer.reminds_to_complete.snazzy.p2'))
    end

    it 'has three buttons with correct links' do
      expect(email.html_part.body).to have_content(I18n.t('investment_mailer.reminds_to_complete.snazzy.btn1'))
      expect(email.html_part.body).to have_content(I18n.t('investment_mailer.reminds_to_complete.snazzy.btn2'))
      expect(email.html_part.body).to have_content(I18n.t('investment_mailer.reminds_to_complete.snazzy.btn3'))

      expect(email.html_part.body).to match(I18n.t('investment_mailer.reminds_to_complete.snazzy.link1'))
      expect(email.html_part.body).to match(I18n.t('investment_mailer.reminds_to_complete.snazzy.link2'))
      expect(email.html_part.body).to match(I18n.t('investment_mailer.reminds_to_complete.snazzy.link3'))
    end

    it 'only delivers the email if the investment state is intended' do
      investment.update_attributes(state: 'entity_selected')
      ActionMailer::Base.deliveries.clear
      email.deliver_now
      expect(ActionMailer::Base.deliveries).to be_empty
    end

    it 'can be delivered' do
      expect { InvestmentMailer.delay.reminds_to_complete(investment.id) }.to change(Sidekiq::Extensions::DelayedMailer.jobs, :size).by(1)
    end
  end

  describe '#funding_method_selected' do
    let(:investment) { FactoryGirl.create(:international_investment, security: FactoryGirl.create(:security), funding_method: Investment.funding_methods[:wire]) }
    subject(:email) { InvestmentMailer.funding_method_selected(investment.id) }
    let(:email_body) { email.body.raw_source }

    it 'delivers correct email' do
      expect(email.to).to eq [investment.user.email]
      expect(email.from).to eq [I18n.t('contacts.investors')]
      expect(email.header.to_s).to match(/X-SMTPAPI.*InvestmentMailer-funding_method_selected/)
    end

    context 'Wire instructions' do
      before { investment.update_attribute(:funding_method, Investment.funding_methods[:wire]) }

      it 'displays the international instructions' do
        expect(email_body).to have_content(I18n.t('investment_mailer.instructions.international_title'))
        expect(email_body).to have_content(I18n.t('investment_mailer.instructions.domestic_title'))
      end
    end

    context 'Ach' do
      before { investment.update_attribute(:funding_method, Investment.funding_methods[:ach]) }

      it 'does not display any instructions' do
        expect(email_body).to_not have_content(I18n.t('investment_mailer.instructions.international_title'))
        expect(email_body).to_not have_content(I18n.t('investment_mailer.instructions.domestic_title'))
      end

      it 'shows the ach title' do
        expect(email_body).to have_content(I18n.t('investment_mailer.funding_method_selected.ach.title'))
      end
    end

    context 'Check instructions' do
      before { investment.update_attribute(:funding_method, Investment.funding_methods[:check]) }

      it 'does not display the international instructions' do
        expect(email_body).to_not have_content(I18n.t('investment_mailer.instructions.international_title'))
        expect(email_body).to have_content(I18n.t('investment_mailer.funding_method_selected.check.title', company_name: investment.offering.company.name))
      end
    end

    context 'Reg C investment' do
      before { investment.security.reg_cf! }

      it 'displays the cancellation date' do
        expect(email_body).to have_content(I18n.t('investment_mailer.funding_method_selected.reg_c_details.cancellation'))
        expect(email_body).to_not have_content(I18n.t('investment_mailer.funding_method_selected.offering_end_date'))
      end
    end

    context 'Reg D investment' do
      before { investment.security.reg_d! }

      it 'displays the offering end date' do
        expect(email_body).to have_content(I18n.t('investment_mailer.funding_method_selected.offering_end_date'))
        expect(email_body).to_not have_content(I18n.t('investment_mailer.funding_method_selected.reg_c_details.cancellation'))
      end
    end
  end
end
