class InvestmentPreview < ActionMailer::Preview
  def funds_received_email_reg_c_or_reg_s
    investment = Investment.where(state: 'escrowed').detect { |i| i.is_reg_c? && i.offering.photo_swatch_url }
    InvestmentMailer.funds_received(investment.id)
  end

  def funds_received_email_reg_d
    investment = Investment.where(state: 'escrowed').detect { |i| !i.is_reg_c? && !i.verified? && i.offering.photo_swatch_url }
    InvestmentMailer.funds_received(investment.id)
  end

  def reminds_to_complete_email
    investment = Investment.where(state: 'intended').last
    InvestmentMailer.reminds_to_complete(investment.id)
  end

  def funding_method_selected_ach_email
    # Need to keep test for calc_data for now for data consistency
    investment = Investment.ach.where(state: 'funding_method_selected').order(updated_at: 'desc').detect { |i| i.is_reg_c? && i.offering.photo_swatch_url }
    if investment.blank?
      investment = Investment.ach.where(state: 'funding_method_selected').order(updated_at: 'desc').detect { |i| i.is_reg_c? }
    end
    raise 'Could not find an appropriate investment' if investment.blank?
    # Just for showing, not actually save the record
    investment.calc_data = mock_ach_calc_data if investment.calc_data.nil?
    InvestmentMailer.funding_method_selected(investment.id)
  end

  def funding_method_selected_wire_email
    # Need to keep test for calc_data for now for data consistency
    investment = Investment.wire.where(state: 'funding_method_selected').order(updated_at: 'desc').detect { |i| i.is_reg_c? && i.offering.photo_swatch_url }
    if investment.blank?
      investment = Investment.wire.where(state: 'funding_method_selected').order(updated_at: 'desc').detect { |i| i.is_reg_c? }
    end
    raise 'Could not find an appropriate investment' if investment.blank?
    # Just for showing, not actually save the record
    investment.calc_data = mock_wire_calc_data if investment.calc_data.nil?
    InvestmentMailer.funding_method_selected(investment.id)
  end

  def funding_method_selected_check_email
    investment = Investment.where(funding_method: Investment.funding_methods[:check]).detect { |i| i.offering.photo_swatch_url }
    InvestmentMailer.funding_method_selected(investment.id)
  end


  def offering_target_reached_email
    offering = find_successful_or_active_offering
    if offering
      investment = Investment.where(offering_id: offering.id).first
      InvestmentMailer.offering_target_reached(investment.id)
    end
  end

  def counter_signed_email
    offering = find_successful_or_active_offering
    if offering
      investment = Investment.where(offering_id: offering.id).first
      InvestmentMailer.counter_signed(investment.id)
    end
  end

  private

  def find_successful_or_active_offering
    offering = nil
    raised_amount = 0
    Offering.where(investable: true).each do |o|
      offering_presenter = OfferingPresenter.new(o)
      raised = offering_presenter.cf_amount_raised
      if offering_presenter.cf_minimum_reached?
        offering = o
        break
      elsif raised >= raised_amount
        offering = o
        raised_amount = raised
      end
    end
    offering
  end

  def mock_wire_calc_data
    {
      "reg_c_income"=>"200000.0",
      "reg_selector"=>"reg-c",
      "reg_c_external"=>"0.0",
      "reg_s_investor"=>"",
      "reg_c_net_worth"=>"200000.0",
      "reg_c_calc_output"=>"{\"limit\"=>20000, \"available\"=>14999, \"over_limit\"=>0}"
    }
  end

  def mock_ach_calc_data
    {
      "reg_c_income"=>"0.0",
      "reg_selector"=>"reg-c",
      "reg_c_external"=>"0.0",
      "reg_s_investor"=>"false",
      "reg_c_net_worth"=>"0.0",
      "reg_c_calc_output"=>"{\"limit\"=>2000, \"available\"=>1444, \"over_limit\"=>0}"
    }
  end
end
