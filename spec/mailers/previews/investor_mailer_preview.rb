class InvestorPreview < ActionMailer::Preview
  def checking_in_email

    user = User.where('users.created_at < ?', 2.weeks.ago).where('length(registration_name) > ?', 10).includes(:investments).where(investments: { user_id: nil }).first
    InvestorMailer.checking_in user.id, true
  end
end
