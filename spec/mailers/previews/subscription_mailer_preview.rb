class SubscriptionPreview < ActionMailer::Preview
  def welcome_email
    SubscriptionMailer.welcome_email('foo@example.com')
  end
end
