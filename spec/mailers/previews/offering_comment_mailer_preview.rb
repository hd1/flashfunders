class OfferingCommentPreview < ActionMailer::Preview
  def reply_comment_from_issuer
    reply_comment = OfferingComment.where.not(parent_id: nil).last
    parent_comment = OfferingComment.find(reply_comment.parent_id)
    offering_id = parent_comment.offering.id
    comment_user_id = parent_comment.user.id

    OfferingCommentMailer.reply_comment_from_issuer(offering_id, comment_user_id, reply_comment.id)
  end

  def comment_from_user
    comment = OfferingComment.where(parent_id: nil).last
    offering_id = comment.offering.id
    comment_user_id = comment.user.id

    OfferingCommentMailer.comment_from_user(offering_id, comment_user_id, comment.id)
  end
end
