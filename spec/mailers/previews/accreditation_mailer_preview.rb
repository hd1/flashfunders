class AccreditationPreview < ActionMailer::Preview
  def third_party_person_verification_email
    name = 'John Doe'
    email = 'jdoe@example.com'

    qualification = Accreditor::ThirdPartyIncomeQualification.new(
      uuid: SecureRandom.uuid,
      email: 'foo@example.com',
    )

    AccreditationMailer.third_party_person_verification_email(qualification, investor_name: name, investor_email: email)
  end

  def third_party_entity_verification_email
    name = 'John Doe'
    email = 'jdoe@example.com'
    entity_name = 'John Doe, LLC'

    qualification = Accreditor::ThirdPartyEntityQualification.new(
      uuid: SecureRandom.uuid,
      email: 'foo@example.com',
    )

    AccreditationMailer.third_party_entity_verification_email(qualification, entity_name: entity_name, investor_name: name, investor_email: email)
  end

end
