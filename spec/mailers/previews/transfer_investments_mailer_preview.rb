class TransferInvestmentsMailerPreview < ActionMailer::Preview
  def send_mail
    error = FundAmerica::ServiceError.new('unable to create investment: {"investment"=>{"equity_share_count"=>["must be greater than 0"]}}')

    TransferInvestmentsMailer.send_email(Investment.first.id, error.to_s)
  end

end

