class OfferingPreview < ActionMailer::Preview
  def message_from_user_email
    offering = Offering.last
    user = User.last
    message = OfferingMessage.new(body: "Why\n\n <script>alert('hello there!');</script>is the sky blue?", id: 1)

    OfferingMailer.message_from_user(offering.id, user.id, message)
  end

  def message_from_issuer_email
    offering = Offering.last
    user = User.last
    message = OfferingMessage.new(body: "Air\npollution", id: 1, user: user)

    OfferingMailer.message_from_issuer(offering.id, user.id, message)
  end
end

