class ContactPreview < ActionMailer::Preview
  def contact_us_from_registered_user_email
    user = User.new(email: 'user@example.com', registration_name: 'Test User', id: 23)
    message = 'Why is the sky blue?'

    ContactMailer.contact_us('startup', user.registration_name, user.email, message, 'contact', user.id)
  end

  def contact_us_from_unregistered_user_email
    user = User.new(email: 'user@example.com', registration_name: 'Test User', id: 23)
    message = 'Why is the sky blue?'

    ContactMailer.contact_us('startup', user.registration_name, user.email, message, 'contact', nil)
  end

end

