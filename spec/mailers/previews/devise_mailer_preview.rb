class DevisePreview < ActionMailer::Preview
  def welcome
    user = User.where.not(registration_name: blank?).first
    DeviseMailer.welcome_instructions(user, "faketoken", {})
  end

  def reset_password_instructions
    user = User.last
    DeviseMailer.reset_password_instructions(user, 'faketoken', {})
  end

  def unlock_instructions
    user = User.last
    DeviseMailer.unlock_instructions(user, 'faketoken', {})
  end

  def completed_issuer_application
    user = User.where.not(registration_name: blank?).first
    user.user_type = :user_type_entrepreneur
    DeviseMailer.welcome_instructions(user, 'faketoken', { template_name: 'completed_issuer_application', subject: 'Your application has been received.' })
  end

end
