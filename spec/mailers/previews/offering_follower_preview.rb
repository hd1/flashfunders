class OfferingFollowerPreview < ActionMailer::Preview
  def welcome_follower
    fake_follower = OfferingFollower.order(created_at: :desc)
      .first_or_create(user_id: User.last.id, offering_id: Offering.last.id)

    OfferingFollowerMailer.welcome_follower(fake_follower.id)
  end

  def notify_issuer_reply
    offering = Offering.where.not(vanity_path: nil).where.not(vanity_path: '').first
    reply = OfferingComment.where.not(parent_id: nil).first_or_create do |comment|
      comment.parent_id = OfferingComment.where(offering_id: offering.id).first.id
      comment.body = 'reply'
      comment.user_id = User.where.not(id: parent.user.id).detect{|user| user.full_name}.id
    end

    OfferingFollowerMailer.notify_issuer_reply(reply.parent.offering.id, reply.id)
  end
end
