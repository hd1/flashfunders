require 'rails_helper'

describe ContactMailer do

  describe '#contact_us' do
    let(:user) { double(User, id: 1, full_name: nil, registration_name: 'John Doe', email: 'jdoe@example.com', personal_federal_identification_information: nil) }
    let(:message) { 'Test\nmessage' }

    let(:mailer) { ContactMailer.contact_us('startup', user.registration_name, user.email, message, 'contact', user.id) }

    before do
      allow(User).to receive(:find).and_return(user)
    end

    it 'should have the correct mail attributes' do
      expect(mailer.subject).to include 'New email from a user'
      expect(mailer.to).to include I18n.t('contacts.startups')
      expect(mailer.from).to include user.email
      expect(mailer.body).to have_content(message)
      expect(mailer.header.to_s).to match(/X-SMTPAPI.*ContactMailer-contact_us/)
    end

    it 'can be delivered' do
      mailer.deliver_now
      expect(ActionMailer::Base.deliveries).to_not be_empty
    end
  end

end

