require 'rails_helper'

describe OfferingMailer do

  describe '#message_from_user' do
    let(:user) { User.new(id: 1, registration_name: 'John Doe', email: 'jdoe@example.com') }
    let(:issuer) { double(User, id: 456, full_name: 'Araya Stark', uuid: 'xx') }
    let(:company) { double(Company, name: 'My Company') }
    let(:offering) { double(Offering, id: 1, company: company, public_email: 'email@mycompany.com', issuer_user: issuer) }
    let(:offering_message) { double(OfferingMessage, id: 1, body: "Test\nmessage", email: 'mock@flashfunders.com', name: 'mock name') }

    let(:mailer) { OfferingMailer.message_from_user(123, 456, offering_message) }

    before do
      allow(Offering).to receive(:find).and_return(offering)
      allow(User).to receive(:find).and_return(user)
    end

    it 'should have the correct mail attributes' do
      expect(mailer.subject).to include 'My Company'
      expect(mailer.to).to include 'email@mycompany.com'
      expect(mailer.from).to include I18n.t('contacts.no_reply')
      expect(mailer.body).to have_content('John Doe')
      expect(mailer.header.to_s).to match(/X-SMTPAPI.*OfferingMailer-message_from_user/)
    end

    it 'when user has a full name it is included in the message' do
      allow(user).to receive(:personal_federal_identification_information).and_return("something")
      allow(user).to receive(:full_name).and_return("Sam Smith")

      expect(mailer.body).to have_content('Sam Smith')
      expect(mailer.body).to have_content('Araya Stark')
    end

    it 'should not have body message' do
      expect(mailer.body).to_not match("Test<br />message")
    end


    it 'should have the offering name in the from' do
      expect(mailer.header[:from].to_s).to include(user.registration_name)
    end

    it 'can be delivered' do
      mailer.deliver_now
      expect(ActionMailer::Base.deliveries).to_not be_empty
    end
  end

  describe '#message_from_issuer' do
    let!(:user) do
      user = create_user(email: 'user@example.com', password: 'thepassword', password_confirmation: 'thepassword')
      FactoryGirl.create(:personal_federal_identification_information, user_id: user.id, first_name: 'Guy', last_name: 'Duke')
      user
    end

    let(:company) { double(Company, name: 'My Company') }
    let(:offering) { double(Offering, id: 1, company: company, public_email: 'email@mycompany.com', public_contact_name: 'Larry Doe') }
    let(:offering_message) { double(OfferingMessage, id: 1, body: "This\nis a <b>message</b>", user_id: 456, email: 'mock@flashfunders.com', name: 'mock name') }
    let(:mailer) { OfferingMailer.message_from_issuer(123, 456, offering_message) }
    let(:issuer) { double(User, id: 456, registration_name: 'mock name', uuid: 'xx') }

    before do
      allow(Offering).to receive(:find).and_return(offering)
      allow(User).to receive(:find).and_return(user)
      allow(offering_message).to receive(:user).and_return(issuer)
    end

    it 'should have the correct mail attributes' do
      expect(mailer.subject).to eq(I18n.t('offering_mailer.from_issuer.subject', company: company.name))
      expect(mailer.to).to include user.email
      expect(mailer.from).to include(I18n.t('contacts.no_reply'))
      expect(mailer.header.to_s).to match(/X-SMTPAPI.*OfferingMailer-message_from_issuer/)
    end

    it 'should have the offering name in the from' do
      expect(mailer.header[:from].to_s).to include(offering.public_contact_name)
    end

    it 'inserts an html break in the reply' do
      expect(mailer.body).to match(I18n.t('offering_mailer.from_issuer.title', issuer_name: offering.public_contact_name, company_name: company.name))
    end

    it 'can be delivered' do
      mailer.deliver_now
      expect(ActionMailer::Base.deliveries).to_not be_empty
    end
  end
end
