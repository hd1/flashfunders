require 'rails_helper'

describe OfferingCommentMailer do
  describe '#comment_from_user' do
    let(:user) { User.new(id: 1, registration_name: 'John Doe', email: 'jdoe@example.com') }
    let(:issuer_user) { User.new(id: 2, registration_name: 'Issuer Name', email: 'issuer@mycompany.com') }
    let(:company) { double(Company, name: 'My Company') }
    let(:offering) { double(Offering, id: 1, company: company, public_email: 'issuer@mycompany.com',
                     public_contact_name: 'Issuer Name', vanity_path: 'mycompany', issuer_user_id: 2) }
    let(:offering_presenter) { OfferingPresenter.new(offering) }
    let(:offering_comment) { double(OfferingComment, id:1, body: "Test\ncomment", offering_id: 1, user_id: 1, parent_id: nil) }

    let(:mailer) { OfferingCommentMailer.comment_from_user(offering.id, user.id, offering_comment.id) }

    before do
      allow(Offering).to receive(:find).and_return(offering)
      allow(User).to receive(:find).and_return(user)
      allow(OfferingComment).to receive(:find_by).and_return(offering_comment)
      allow(offering).to receive_message_chain(:issuer_user, :email).and_return(issuer_user.email)
    end

    it 'should have the correct mail attributes' do
      expect(mailer.subject).to have_content(I18n.t('offering_comment_mailer.comment_from_user.subject'))
      expect(mailer.to).to include 'issuer@mycompany.com'
      expect(mailer.from).to include 'no-reply@flashfunders.com'
      expect(mailer.body).to have_content(I18n.t('offering_comment_mailer.comment_from_user.body', user_name: 'John Doe'))
      expect(mailer.header.to_s).to match(/X-SMTPAPI.*OfferingCommentMailer-comment_from_user/)
    end

    it 'should have the correct offering vanity url' do
      expected_path = offering_vanity_path(path: offering_presenter.vanity_path, comment_id: offering_comment.id, anchor: 'comment')
      expected_url = URI.join(root_url, expected_path).to_s

      expect(mailer.body).to match(Regexp.escape(expected_url))
    end

    it 'should fallback to offering-id offering url' do
      allow(offering).to receive(:vanity_path).and_return('')
      expected_path = offering_path(id: offering_presenter.offering_id, comment_id: offering_comment.id, anchor: 'comment')
      expected_url = URI.join(root_url, expected_path).to_s

      expect(mailer.body).to match(Regexp.escape(expected_url))
    end

    it 'should have the user name in the from' do
      expect(mailer.header[:from].to_s).to include(user.registration_name)
    end

    it 'can be delivered' do
      mailer.deliver_now
      expect(ActionMailer::Base.deliveries).to_not be_empty
    end
  end

  describe '#reply_comment_from_issuer' do
    let(:user) { User.new(id: 1, registration_name: 'John Doe', email: 'jdoe@example.com') }
    let(:issuer_user) { User.new(id: 2, registration_name: 'Issuer Name', email: 'issuer@mycompany.com') }
    let(:company) { double(Company, name: 'My Company') }
    let(:offering) { double(Offering, id: 1, company: company, public_email: 'issuer@mycompany.com',
                     public_contact_name: 'Issuer Name', vanity_path: 'mycompany', issuer_user_id: 2) }
    let(:offering_presenter) { OfferingPresenter.new(offering) }
    let(:comment) { double(OfferingComment, id:1, body: "Test\ncomment", offering_id: 1, user_id: 1, parent_id: nil) }
    let(:reply_comment) { double(OfferingComment, id:2, body: "Test\nreply comment", offering_id: 1, user_id: 2, parent_id: 1) }

    let(:mailer) { OfferingCommentMailer.reply_comment_from_issuer(offering.id, user.id, reply_comment.id) }

    before do
      allow(Offering).to receive(:find).and_return(offering)
      allow(User).to receive(:find).and_return(user)
      allow(OfferingComment).to receive(:find_by).and_return(reply_comment)
      allow(offering).to receive_message_chain(:issuer_user, :id).and_return(issuer_user.id)
    end

    it 'should have the correct mail attributes' do
      expect(mailer.subject).to have_content(I18n.t('offering_comment_mailer.reply_comment_from_issuer.subject', issuer_name: offering.public_contact_name))
      expect(mailer.to).to include 'jdoe@example.com'
      expect(mailer.from).to include 'no-reply@flashfunders.com'
      expect(mailer.body).to have_content(I18n.t('offering_comment_mailer.reply_comment_from_issuer.body', issuer_name: 'Issuer Name', company_name: 'My Company'))
      expect(mailer.header.to_s).to match(/X-SMTPAPI.*OfferingCommentMailer-reply_comment_from_issuer/)
    end

    it 'should have the correct offering vanity url' do
      expected_path = offering_vanity_path(path: offering_presenter.vanity_path, comment_id: reply_comment.id, anchor: 'comment')
      expected_url = URI.join(root_url, expected_path).to_s

      expect(mailer.body).to match(Regexp.escape(expected_url))
    end

    it 'should fallback to offering-id offering url' do
      allow(offering).to receive(:vanity_path).and_return('')
      expected_path = offering_path(id: offering_presenter.offering_id, comment_id: reply_comment.id, anchor: 'comment')
      expected_url = URI.join(root_url, expected_path).to_s

      expect(mailer.body).to match(Regexp.escape(expected_url))
    end

    it 'should have the user name in the from' do
      expect(mailer.header[:from].to_s).to include(issuer_user.registration_name)
    end

    it 'can be delivered' do
      mailer.deliver_now
      expect(ActionMailer::Base.deliveries).to_not be_empty
    end
  end
end
