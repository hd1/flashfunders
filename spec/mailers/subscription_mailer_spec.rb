require 'rails_helper'

describe SubscriptionMailer do
  let(:mailer) { SubscriptionMailer.welcome_email('foo@example.com') }

  describe '#welcome_email' do
    it 'has the correct mail attributes' do
      expect(mailer.subject).to eq I18n.t('subscription_mailer.welcome.subject')
      expect(mailer.to).to include 'foo@example.com'
      expect(mailer.from).to include I18n.t('contacts.investors')
      expect(mailer.header.to_s).to match(/X-SMTPAPI.*SubscriptionMailer-welcome_email/)
    end

    it "has all significant links in email body" do
      expect(mailer.html_part.body).to have_content I18n.t('subscription_mailer.welcome.snazzy.btn')
      expect(mailer.text_part.body).to have_content 'utm_campaign=bedc136961-6_18_2015'
    end

    it 'can be delivered' do
      mailer.deliver_now
      expect(ActionMailer::Base.deliveries).to_not be_empty
    end
  end
end
