require 'rails_helper'

describe OfferingFollowerMailer do
  let(:offering) { FactoryGirl.create(:offering, issuer_user_id: issuer.id) }

  let(:investor) { FactoryGirl.create(:user) }
  let(:issuer) { FactoryGirl.create(:user) }
  let(:someone) { FactoryGirl.create(:user) }

  let(:comment) { FactoryGirl.create(:offering_comment, user_id: investor.id, offering_id: offering.id, body: 'yada yada yada')}
  let!(:reply) { FactoryGirl.create(:offering_comment, user_id: issuer.id, parent_id: comment.id, body: 'yada what?')}

  before do
    @investor = OfferingFollower.create(offering_id: offering.id, user_id: investor.id, mode: :investor)
    @issuer = OfferingFollower.create(offering_id: offering.id, user_id: issuer.id, mode: :follower)
    @someone = OfferingFollower.create(offering_id: offering.id, user_id: someone.id, mode: :follower)
  end

  describe '#welcome_follower' do
    let(:mailer) { OfferingFollowerMailer.welcome_follower(@investor.id) }

    it 'has the correct mail attributes' do
      expect(mailer.subject).to eq I18n.t('offering_follow_mailer.welcome_follower.subject', company_name: offering.company.name)
      expect(mailer.to).to include investor.email
      expect(mailer.from).to include I18n.t('contacts.no_reply')
      expect(mailer.body).to have_content("You Are Now A Part of the #{offering.company.name} Community")
      expect(mailer.body).to match(browse_url)
    end
  end

  describe '#notify_issuer_reply' do
    let(:mailer) { OfferingFollowerMailer.notify_issuer_reply(offering.id, reply.id) }

    it 'has the correct mail attributes' do
      expect(mailer.subject).to eq I18n.t('offering_follow_mailer.notify_issuer_reply.subject', company_name: offering.company.name)
      expect(mailer.to).to_not include investor.email
      expect(mailer.to).to_not include issuer.email
      expect(mailer.to).to be_blank
      expect(mailer.bcc).to include someone.email
      expect(mailer.from).to include I18n.t('contacts.no_reply')
      expect(mailer.body).to have_content(comment.body)
      expect(mailer.body).to have_content(reply.body)
    end
  end
end
