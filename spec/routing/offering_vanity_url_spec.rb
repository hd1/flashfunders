require 'rails_helper'

describe 'Offering vanity url' do
  context 'when attempting to visit a legit vanity url' do
    let!(:offering) { Offering.create!(vanity_path: 'im-so-vain') }
    it 'should take you to that offering page' do
      expect(get: 'im-so-vain').to route_to(
        controller: 'offerings',
        action: 'vanity_show',
        path: 'im-so-vain',
      )
    end
  end
  context 'when attempting to visit a non-existant vanity url' do
    it 'should 404' do
      expect(get: 'this-is-not-a-real-vanity-url').not_to be_routable
    end
  end
  context 'when attempting to visity an existing url' do
    it "should get there, as per norm, ensuring we haven't clobbered anything" do
      expect(get: 'privacy').to route_to(
        controller: 'static',
        action: 'privacy'
      )
    end
  end
end
