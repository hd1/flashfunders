require 'rails_helper'

feature 'static pages' do
  scenario "static content", percy: true do
    [root_path,
     startups_path,
     investors_path,
     faq_investor_path,
     faq_startup_path,
     team_path,
     terms_path,
     privacy_path
    ].each do |path|
      visit path
      Percy::Capybara.snapshot(page)
    end
  end
end

feature 'offering tabs', js: true do
  let!(:offering) { create_complete_open_campaign }

  scenario "visit offering secondary navigation", percy: true do
    visit offering_path(offering)

    OfferingPresenter.new(offering).secondary_nav.each do |tab|
      click_link(tab.capitalize)
      Percy::Capybara.snapshot(page, name: "offering-#{tab}")
    end
  end
end

feature 'browse page', js: true do
  before do
    5.times do |i|
      instance_variable_set("@offering#{i+1}", create_complete_open_campaign("foo#{i}@foomail.com"))
    end
    @entity = FactoryGirl.create(:investor_entity_individual)
    FactoryGirl.create(:investment, :escrowed, investor_entity_id: @entity.id, offering: @offering5, security_id: @offering5.reg_d_direct_security.id, amount: @offering5.reg_d_direct_security.minimum_total_investment.to_f, funding_method: 'wire')
  end

  scenario "various offerings' states", percy: true do
    @offering1.update_attribute(:ends_at, 2.hours.from_now)
    @offering2.update_attribute(:ends_at, 1.days.from_now)

    @offering3.update_attribute(:ends_at, 1.day.ago)
    @offering3.update_attribute(:closed_status, :pending)

    @offering4.update_attribute(:ends_at, 1.day.ago)
    @offering4.update_attribute(:closed_status, :unsuccessful)

    @offering5.visibility_public!
    @offering5.update_attribute(:investable, true)
    @offering5.update_attribute(:ends_at, DateTime.yesterday.midnight.advance(hours: 24, seconds: -1))
    @offering5.update_attribute(:closed_status, :successful)
    Timecop.freeze do
      visit browse_path
      Percy::Capybara.snapshot(page)
    end
  end

end

feature 'emails' do
  before do
    User.create!(email: 'jane@entrepreneur.com', registration_name: 'Jane Entrepreneur', user_type: :user_type_entrepreneur, needs_password_set: false, password: 'password')
    investor = User.create!(email: 'jane@investor.com', registration_name: 'Jane Investor', user_type: :user_type_investor, needs_password_set: false, password: 'password')
    User.create!(email: 'jane+no_password@investor.com', registration_name: 'Jane Investor', user_type: :user_type_investor, needs_password_set: true, password: 'password')
    User.create!(email: 'john+started_investment@investor.com', registration_name: 'John Investor', user_type: :user_type_investor, needs_password_set: false, password: 'password', created_at: 3.weeks.ago)
    offering = create_complete_open_campaign('john@entrepreneur.com')
    FactoryGirl.create(:cf_stock, offering: offering)
    offering.reload
    entity = FactoryGirl.create(:investor_entity_individual)
    Investment.create!(user_id: investor.id, offering_id: offering.id, amount: 1500, security_id: offering.reg_c_security.id, investor_entity_id: entity.id, state: 'intended', reg_selector: 'reg-c')
    Investment.create!(user_id: investor.id, offering_id: offering.id, amount: 1500, security_id: offering.reg_c_security.id, investor_entity_id: entity.id, funding_method: 'wire', state: 'funding_method_selected', reg_selector: 'reg-c')
    Investment.create!(user_id: investor.id, offering_id: offering.id, amount: 1500, security_id: offering.reg_c_security.id, investor_entity_id: entity.id, funding_method: 'wire', state: 'funding_method_selected', calc_data: nil)
    Investment.create!(user_id: investor.id, offering_id: offering.id, amount: 1500, security_id: offering.reg_c_security.id, investor_entity_id: entity.id, funding_method: 'ach', state: 'funding_method_selected', reg_selector: 'reg-c')
    Investment.create!(user_id: investor.id, offering_id: offering.id, amount: 25000, security_id: offering.reg_d_direct_security.id, investor_entity_id: entity.id, funding_method: 'check', state: 'funding_method_selected', reg_selector: 'reg-d')
    Investment.create!(user_id: investor.id, offering_id: offering.id, amount: 1500, security_id: offering.reg_c_security.id, investor_entity_id: entity.id, funding_method: 'ach', state: 'escrowed', reg_selector: 'reg-c')
    Investment.create!(user_id: investor.id, offering_id: offering.id, amount: 25000, security_id: offering.reg_d_direct_security.id, investor_entity_id: entity.id, funding_method: 'wire', state: 'escrowed', reg_selector: 'reg-d')
    Investment.create!(user_id: investor.id, offering_id: offering.id, amount: 1500, security_id: offering.reg_c_security.id, investor_entity_id: entity.id, funding_method: 'ach', state: 'counter_signed', reg_selector: 'reg-c')
    comment = FactoryGirl.create(:offering_comment, user_id: investor.id, offering_id: offering.id, body: 'yada yada yada')
    FactoryGirl.create(:offering_comment, user_id: offering.issuer_user_id, offering_id: offering.id, parent_id: comment.id, body: 'blah blah blah')

    allow_any_instance_of(Address).to receive(:address1).and_return('999')
    allow_any_instance_of(Offering).to receive(:escrow_end_date).and_return(Date.new(2015, 11, 11))
    allow_any_instance_of(Offering).to receive(:ends_at).and_return(Time.zone.at(1471139445))
    allow(DateTime).to receive(:current).and_return(DateTime.new(2016,9,2))
    allow_any_instance_of(Offering).to receive(:photo_swatch_url).and_return(('mailer/header_photo3.png'))
  end

  # Excluding Docusign email since it's internal and just has a subject and no body
  let (:exclude) { %w(docusign) }

  scenario 'snapshot each mailer preview', percy: true do
    ActionMailer::Preview.all.each do |preview|
      category = preview.to_s.underscore.gsub(/_preview/, '')
      unless exclude.include?(category)
        preview.emails.each do |email|
          path = "/rails/mailers/#{category}/#{email}"
          visit path
          within_frame('messageBody') do
            Percy::Capybara.snapshot(page)
          end
        end
      end
    end
  end
end
