require 'rails_helper'

describe ThirdPartyVerificationHelper do

  describe '#accreditation_statement' do
    let(:investor_entity) { InvestorEntity.new }
    let(:qualification) { Accreditor::Qualification.new }

    subject { helper.accreditation_statement(investor_entity, qualification) }

    it 'returns the copy' do
      copy = I18n.t('third_party_verification.review_statement.statement.other_entity')
      expect(subject).to eql(copy)
    end

    context 'when entity is an individual' do
      let(:investor_entity) { InvestorEntity::Individual.new }

      context 'when qualification is for income' do
        let(:qualification) { Accreditor::ThirdPartyIncomeQualification.new }

        it 'returns the copy' do
          copy = I18n.t('third_party_verification.review_statement.statement.personal_income')
          expect(subject).to eql(copy)
        end
      end

      context 'when qualification is for net worth' do
        let(:qualification) { Accreditor::ThirdPartyNetWorthQualification.new }

        it 'returns the copy' do
          copy = I18n.t('third_party_verification.review_statement.statement.personal_net_worth')
          expect(subject).to eql(copy)
        end
      end
    end

    context 'when entity is a trust' do
      let(:investor_entity) { InvestorEntity::Trust.new }

      it 'returns the copy' do
        copy = I18n.t('third_party_verification.review_statement.statement.trust')
        expect(subject).to eql(copy)
      end
    end

  end

  describe '#non_individual_entity?' do
    subject { helper.non_individual_entity?(investor_entity) }

    it 'should be false for an individual' do
      expect(helper.non_individual_entity?(InvestorEntity::Individual.new)).to be_falsy
    end

    it 'should be true for a trust' do
      expect(helper.non_individual_entity?(InvestorEntity::Trust.new)).to be_truthy
    end

  end

  describe '#trust_entity?' do
    subject { helper.trust_entity?(investor_entity) }

    it 'should be false for an LLC' do
      expect(helper.trust_entity?(InvestorEntity::LLC.new)).to be_falsy
    end

    it 'should be true for a trust' do
      expect(helper.non_individual_entity?(InvestorEntity::Trust.new)).to be_truthy
    end

  end
end
