require 'rails_helper'

describe OfferingHelper do
  describe '#follower_css' do
    let(:follower) { double(OfferingFollower) }

    context 'when follower is following' do
      before { allow(follower).to receive(:following?).and_return(true) }

      it 'returns \'followed\' string' do
        expect( helper.follower_css(follower) ).to eql('followed')
      end
    end

    context 'when follower is not following' do
      before { allow(follower).to receive(:following?).and_return(false) }

      it 'returns nil' do
        expect( helper.follower_css(follower) ).to be_nil
      end
    end
  end

  describe '#offering_vanity_path' do
    subject { helper.offering_short_path(offering_presenter) }
    let(:offering_presenter) { double(OfferingPresenter, offering_id:123, vanity_path:vanity_path) }

    context 'when the offering has a vanity path' do
      let(:vanity_path) { "test-vanity" }
      it { should eq("/test-vanity") }
    end
    context 'when the offering has no vanity path' do
      let(:vanity_path) { nil }
      it { should eq("/offering/123") }
    end
  end

  describe '#comment_tab_path' do
    let(:offering) { double(Offering, vanity_path: 'test-vanity') }

    context 'when the offering has a vanity path' do
      it 'returns a vanity path with anchor comment' do
        expect( helper.comment_tab_path(offering) ).to eql('/test-vanity#comment')
      end
    end
  end

  describe '#is_issuer_user?' do
    let(:user) { FactoryGirl.create(:user, email: 'issuer_user@example.com') }
    let(:offering) { FactoryGirl.create(:offering, issuer_user: user) }

    context 'when the user is the issuer of offering' do
      it 'is issuer user' do
        expect( helper.is_issuer_user?(offering, user) ).to eql(true)
      end
    end

    context 'when the user is not the issuer of offering' do
      before { offering.update_attributes(issuer_user_id: nil) }
      it 'is not issuer user' do
        expect( helper.is_issuer_user?(offering, user) ).to eql(false)
      end
    end
  end

  describe '#can_reply?' do
    let(:user) { FactoryGirl.create(:user, email: 'issuer_user@example.com') }
    let(:commenter) { FactoryGirl.create(:user, email: 'commenter@example.com') }
    let(:offering) { FactoryGirl.create(:offering, issuer_user: user) }
    let(:comment) { OfferingComment.create(user_id: commenter.id) }


    context 'when the user is an issuer and not the owner of the comment' do
      it 'can reply comment' do
        expect( helper.can_reply?(offering, user, comment) ).to eql(true)
      end
    end

    context 'when the user is an issuer and the owner of the comment' do
      before { comment.update_attributes(user_id: user.id) }
      it 'can not reply comment' do
        expect( helper.can_reply?(offering, user, comment) ).to eql(false)
      end
    end
  end

  describe '#can_message?' do
    let(:user) { FactoryGirl.create(:user, email: 'issuer_user@example.com') }
    let(:commenter) { FactoryGirl.create(:user, email: 'commenter@example.com') }
    let(:offering) { FactoryGirl.create(:offering, issuer_user: user) }
    let(:comment) { OfferingComment.create(user_id: commenter.id) }


    context 'when the user is an issuer and not the owner of the comment' do
      it 'can message to the owner' do
        expect( helper.can_message?(offering, user, comment) ).to eql(true)
      end
    end

    context 'when the user is an issuer and the owner is the owner of the comment' do
      before { comment.update_attributes(user_id: user.id) }
      it 'can not message to the owner' do
        expect( helper.can_message?(offering, user, comment) ).to eql(false)
      end
    end

    context 'when the user is not an issuer' do
      it 'can not message to the owner' do
        expect( helper.can_message?(offering, commenter, comment) ).to eql(false)
      end
    end
  end

  describe '#can_delete?' do
    let(:user) { FactoryGirl.create(:user, email: 'issuer_user@example.com') }
    let(:commenter) { FactoryGirl.create(:user, email: 'commenter@example.com') }
    let(:offering) { FactoryGirl.create(:offering, issuer_user: user) }
    let(:comment) { OfferingComment.create(user_id: user.id) }


    context 'when the user is an issuer and the owner of the comment' do
      it 'can delete the comment' do
        expect( helper.can_delete?(offering, user, comment) ).to eql(true)
      end
    end

    context 'when the user is an issuer and not the owner of the comment' do
      before { comment.update_attributes(user_id: user.id) }
      it 'can delete the comment' do
        expect( helper.can_delete?(offering, user, comment) ).to eql(true)
      end
    end

    context 'when the user is not an issuer and the owner of the comment' do
      before { comment.update_attributes(user_id: commenter.id) }
      it 'can delete the comment' do
        expect( helper.can_delete?(offering, commenter, comment) ).to eql(true)
      end
    end

    context 'when the user is not an issuer and not the owner of the comment' do
      it 'can not delete the comment' do
        expect( helper.can_delete?(offering, commenter, comment) ).to eql(false)
      end
    end
  end

  describe '#total_visible_comments' do
    let(:offering) { FactoryGirl.create(:offering) }
    let!(:comment) { FactoryGirl.create(:offering_comment, offering_id: offering.id) }
    let!(:second_comment) { FactoryGirl.create(:offering_comment , offering_id: offering.id) }

    context 'when all of comment is visible' do
      it 'returns total of visible comments' do
        offering_comments = OfferingComment.visible.where(offering_id: offering.id)
        expect( helper.total_visible_comments(offering_comments) ).to eql(2)
      end
    end

    context 'when some of comment is not visible' do
      before { second_comment.update_attributes(offering_id: nil)}
      it 'returns total of visible comments' do
        offering_comments = OfferingComment.visible.where(offering_id: offering.id)
        expect( helper.total_visible_comments(offering_comments) ).to eql(1)
      end
    end
  end
end
