require 'rails_helper'

describe HeaderHelper do
  describe '#header_menus' do
    def regex(str)
      Regexp.new(Regexp.escape(str))
    end

    def looks_right?(menu_item, key)
      !!((menu_item =~ all_links[key][:link]) && (menu_item =~ all_links[key][:class]))
    end

    let(:user) { User.new(id: 1, user_type: user_type) }
    let(:user_type) { nil }
    let(:offering_id) { 100 }

    let(:all_links) do
      {
          browse:          { link: regex(helper.browse_path), class: regex('header-browse') },
          raise:           { link: regex(helper.startups_path), class: regex('header-raise') },
          investors:       { link: regex(helper.investors_path), class: regex('header-investors') },
          my_investments:  { link: regex(helper.dashboard_investments_path), class: regex('header-my-investments') },
          dashboard:       { link: regex(issuer_dashboard_path(id: offering_id)), class: regex('header-dashboard') },
          offering_editor: { link: regex(edit_issuer_editor_detail_path(id: offering_id)), class: regex('header-offering-editor') },
          messages:        { link: regex(dashboard_conversations_path), class: regex('header-messages') },
          faq:             { link: regex(faq_investor_path), class: regex('header-faq') },
          blog:            { link: regex(blog_path), class: regex('header-blog') },
      }
    end

    subject { helper.header_menus.split('</li>') }

    context 'not logged in' do
      before do
        allow(helper).to receive(:current_user).and_return(nil)
      end

      it 'should return the generic links' do
        expect(subject.size).to eq(5)

        expect(looks_right?(subject[0], :browse)).to eq(true)
        expect(looks_right?(subject[1], :raise)).to eq(true)
        expect(looks_right?(subject[2], :investors)).to eq(true)
        expect(looks_right?(subject[3], :faq)).to eq(true)
        expect(looks_right?(subject[4], :blog)).to eq(true)
      end
    end


    context 'logged in' do

      before do
        allow(helper).to receive(:current_user).and_return(user)
      end

      context 'With a current offering' do

        before { allow(helper).to receive(:get_offering_context).and_return(offering_id) }

        it 'should return offering links' do
          expect(subject.size).to eq(6)

          expect(looks_right?(subject[0], :dashboard)).to eq(true)
          expect(looks_right?(subject[1], :offering_editor)).to eq(true)
          expect(looks_right?(subject[2], :browse)).to eq(true)
          expect(looks_right?(subject[3], :my_investments)).to eq(true)
          expect(looks_right?(subject[4], :blog)).to eq(true)
          expect(looks_right?(subject[5], :messages)).to eq(true)
        end
      end

      context 'With no offerings' do
        context 'As a user type entrepreneur' do
          let(:user_type) { :user_type_entrepreneur }

          it 'should return entrepreneur links' do
            expect(subject.size).to eq(5)

            expect(looks_right?(subject[0], :browse)).to eq(true)
            expect(looks_right?(subject[1], :raise)).to eq(true)
            expect(looks_right?(subject[2], :my_investments)).to eq(true)
            expect(looks_right?(subject[3], :blog)).to eq(true)
            expect(looks_right?(subject[4], :messages)).to eq(true)
          end
        end

        context 'As a user type investor' do
          let(:user_type) { :user_type_investor }

          it 'should return investor links' do
            expect(subject.size).to eq(4)

            expect(looks_right?(subject[0], :browse)).to eq(true)
            expect(looks_right?(subject[1], :my_investments)).to eq(true)
            expect(looks_right?(subject[2], :blog)).to eq(true)
            expect(looks_right?(subject[3], :messages)).to eq(true)
          end
        end

        context 'With no user type' do
          let(:user_type) { nil }

          it 'should return the generic links' do
            expect(subject.size).to eq(4)

            expect(looks_right?(subject[0], :browse)).to eq(true)
            expect(looks_right?(subject[1], :my_investments)).to eq(true)
            expect(looks_right?(subject[2], :blog)).to eq(true)
            expect(looks_right?(subject[3], :messages)).to eq(true)
          end
        end
      end
    end
  end

end
