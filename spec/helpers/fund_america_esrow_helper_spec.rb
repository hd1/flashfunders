require 'rails_helper'

describe FundAmericaEscrowHelper do

  let(:investor_user) { FactoryGirl.create(:user, email: 'john.investor@example.com') }

  let(:security) { FactoryGirl.create(:security,
                                      fund_america_id: 'yLzpAj3qTsWGChgvml4uZw',
                                      is_spv: true) }

  let(:offering) { FactoryGirl.create(:offering, securities: [security],
                                      share_price: BigDecimal.new('500.00')) }

  let(:investment) { FactoryGirl.create(:investment,
                                        security: security,
                                        offering: offering,
                                        amount: BigDecimal.new(5000),
                                        shares: 10,
                                        investor_entity: investor_entity,
                                        user: investor_user,
                                        fund_america_id: nil,
                                        funding_method: 'wire',
                                        investor_account_id: 1) }

  let(:investor_entity) { FactoryGirl.create(:investor_entity,
                                             personal_federal_identification_information: personal_federal_identification_information,
                                             fund_america_id: '',
                                             fund_america_ach_authorization_id: ''
  ) }

  let(:personal_federal_identification_information) { FactoryGirl.create(:personal_federal_identification_information,
                                                                         city: 'Las Vegas',
                                                                         country: 'US',
                                                                         date_of_birth: Date.parse('1980-01-01'),
                                                                         first_name: 'John',
                                                                         last_name: 'Investor',
                                                                         daytime_phone: '17025551212',
                                                                         zip_code: '89123',
                                                                         state_id: 'NV',
                                                                         address1: '555 Some St',
                                                                         ssn: '000000000'
  ) }

  let!(:investment_parameters) { Hash.new(
    id: '',
    account_number: '123123123',
    account_type: 'checking',
    name_on_account: 'Bongo Bob',
    routing_number: '011000015',
    entity_id: nil,
    check_type: 'personal',
    email: 'john.investor@example.com',
    literal: 'John User',
    ip_address: '192.168.0.1',
    user_agent: 'ruby'
  ) }

  let!(:person_parameters) { Hash.new }

  let!(:ach_parameters) { Hash.new }

  let!(:entity_parameters) { Hash.new }

  let!(:fund_america_investment) { FundAmerica::Investment.new({ id: '1' }) }

  let(:investment_service) { FundAmerica::InvestmentService.new }
  let(:entity_service) { FundAmerica::EntityService.new }
  let(:ach_authorization_service) { FundAmerica::AchAuthorizationService.new }

  before do
    allow(FundAmericaInvestmentParameterBuilder).to receive(:build).and_return(investment_parameters)
    allow(FundAmericaInvestmentParameterBuilder).to receive(:entity_params_for_person).and_return(person_parameters)
    allow(FundAmericaInvestmentParameterBuilder).to receive(:ach_authorization_params).and_return(ach_parameters)

    allow(FundAmerica::InvestmentService).to receive(:new).and_return(investment_service)
    allow(investment_service).to receive(:create).and_return(fund_america_investment)
    allow(investment_service).to receive(:update).and_return(fund_america_investment)

    allow(FundAmerica::EntityService).to receive(:new).and_return(entity_service)
    allow(entity_service).to receive(:create).and_return(double(FundAmerica::Entity, id: 1))
    allow(entity_service).to receive(:update).and_return(double(FundAmerica::Entity, id: 1))

    allow(FundAmerica::AchAuthorizationService).to receive(:new).and_return(ach_authorization_service)
    allow(ach_authorization_service).to receive(:create).and_return(double(FundAmerica::AchAuthorization, id: 1))
  end

  context 'with a non RegC offering' do
    before do
      investment.update_attribute('funding_method', 'wire')
    end

    describe 'create_fund_america_investment' do
      it 'creates when fund america id did not exist' do
        FundAmericaEscrowHelper.fund_america_update_investment(investment.id)
        expect(investment_service).to have_received(:create).with(investment_parameters, { api: :spv })
        expect(investment_service).to_not receive(:update)
        expect(entity_service).to_not receive(:create)
        expect(ach_authorization_service).to_not receive(:create)
        expect(investor_entity.fund_america_id).to eq('')
        expect(investor_entity.fund_america_ach_authorization_id).to eq('')
      end
    end

    describe 'update_fund_america_investment' do
      before do
        investment.update_attribute('fund_america_id', '2')
      end

      it 'updates when fund america id does exist' do
        FundAmericaEscrowHelper.fund_america_update_investment(investment.id)
        expect(investment_service).to have_received(:update).with(investment_parameters, { api: :spv })
        expect(entity_service).to_not receive(:update)
        expect(ach_authorization_service).to_not receive(:update)
        expect(investor_entity.fund_america_id).to eq('')
        expect(investor_entity.fund_america_ach_authorization_id).to eq('')
      end
    end
  end

  context 'with a RegC offering' do
    before do
      FactoryGirl.create(:cf_stock, offering: offering)
      security.update_attributes(is_spv: false, type: 'Securities::FspStock')
      investment.update_attribute('funding_method', 'ach')
      investment.reload
      offering.reload
    end

    describe 'create_fund_america_investment_and_entity_and_ach_authorization' do
      it 'creates when fund america id did not exist' do
        FundAmericaEscrowHelper.fund_america_update_investment(investment.id)
        expect(investment_service).to have_received(:create).with(investment_parameters, {})
        expect(entity_service).to have_received(:create).with(entity_parameters, {})
        expect(ach_authorization_service).to have_received(:create).with(ach_parameters, {})
        expect(investor_entity.reload.fund_america_id).to eq('1')
        expect(investor_entity.fund_america_ach_authorization_id).to eq('1')
      end
    end

    describe 'update_fund_america_investment_and_entity_and_ach_authorization' do
      before do
        investment.update_attribute('fund_america_id', '2')
      end

      it 'updates when fund america id does exist' do
        FundAmericaEscrowHelper.fund_america_update_investment(investment.id)
        expect(investment_service).to have_received(:update).with(investment_parameters, {})
        expect(entity_service).to have_received(:create).with(entity_parameters, {})
        expect(ach_authorization_service).to have_received(:create).with(ach_parameters, {})
        expect(investor_entity.reload.fund_america_id).to eq('1')
        expect(investor_entity.fund_america_ach_authorization_id).to eq('1')
      end
    end
  end
end
