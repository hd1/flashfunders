require 'rails_helper'

describe DashboardHelper do

  let(:message_data) {
    [# offering_id,user_id,sent_by_user,unread
      [74, 1721, true, false],
      [74, 1721, false, true],
      [74, 1213, true, false],
      [74, 5, true, false],
      [74, 1985, true, true],
      [74, 1985, true, true],
      [74, 1837, true, false],
      [74, 1837, true, true],
      [74, 1837, false, false],
      [74, 1837, false, false],
      [74, 914, true, true],
      [74, 1985, true, true],
      [74, 2213, true, true],
      [74, 914, true, true],
      [74, 2221, false, false],
      [74, 2207, true, true],
      [74, 2225, true, false],
      [74, 2221, false, true],
      [74, 2221, true, false],
      [74, 2221, true, false],
      [74, 2225, false, false],
      [74, 2232, true, true],
      [74, 2263, false, true],
      [74, 2225, true, true],
      [74, 2245, true, true],
      [74, 2237, true, true],
      [74, 2253, true, true],
      [74, 2237, true, true],
      [74, 2260, false, true],
      [74, 2253, true, true],
      [74, 2253, true, true],
      [74, 2256, true, true],
      [74, 1985, true, true],
      [74, 1985, true, true],
      [74, 2253, true, true],
      [74, 2237, true, true],
      [74, 2258, true, true],
      [74, 2260, true, false],
      [74, 2263, true, false],
      [74, 2276, true, true],
      [74, 1213, true, true],
      [74, 2024, true, true],
      [174, 2188, false, true]]
  }
  let(:user_data) {
    [# id, email, registration_name
      [1721, 'someone@flashfunders.com', 'Some One'],
      [1213, 'someoneelse@flashfunders.com', 'Someone Else'],
      [5, 'brendan+i2@flashfunders.com', 'Brendan Kao'],
      [1985, 'rj+bobbyinvestor@flashfunders.com', 'Flitways'],
      [1837, 'another@flashfunders.com', 'Another Person'],
      [914, 'christina@flashfunders.com', 'Christina'],
      [2213, 'rjcicc+jonesy@gmail.com', 'rj jonea'],
      [2221, 'seth+cartogram@flashfunders.com', 'Seth Goldman'],
      [2207, 'christina+17@flashfunders.com', 'Christina'],
      [2225, 'seth+cartogram2@flashfunders.com', 'Seth Goldman'],
      [2232, 'seth+cartogram3@flashfunders.com', 'Seth Goldman'],
      [2263, 'brad+testing123@flashfunders.com', 'Brad'],
      [2245, 'seth+cartogram5@flashfunders.com', 'Seth Goldman'],
      [2237, 'brad+eggs@flashfunders.com', 'Brad'],
      [2253, 'brad+intro@flashfunders.com', 'Brad Bennett'],
      [2260, 'reyes+investors@flashfunders.com', 'Jon Reyes'],
      [2256, 'brad+intro-page@flashfunders.com', 'Brad'],
      [2258, 'rj+testingflow@flashfunders.com', 'rj ciccaglione '],
      [2276, 'brad+eggs123@flashfunders.com', 'Brad'],
      [2024, 'seth@flashfunders.com', 'Seth Goldman'],
      [2188, 'reyes@flashfunders.com', 'Jon Reyes']]
  }

  before do
    # create data
    user_data.each do |row|
      create_user(id: row[0], email: row[1], registration_name: row[2])
    end
    message_data.each do |row|
      OfferingMessage.create!(offering_id: row[0], user_id: row[1], sent_by_user: row[2], unread: row[3], body: 'placeholder')
    end
    Company.create(id:74, name: 'Test Company', offering_id: 74)
    @offering = Offering.create!(id: 74, user_id: 2188)
  end

  let(:current_user) { User.find(2188) }

  describe '#offering_title_tabs' do
    before do
      allow(controller).to receive(:change_context_path).and_return(issuer_dashboard_path(@offering.id))
    end

    let(:get_offering_context) { @offering.id }
    let(:tab_info) { {tab_code_name: 'Test Company', name: 'Test Company (25)', dashboard_path: '/issuer/dashboard/74', current_offering: 'current-offering'} }

    it 'should have the correct tab info' do
      expect(offering_title_tabs).to eql([tab_info])
    end
  end

  describe '#total_unread_for_offering' do
    it 'should have the correct unread total for the offering' do
      expect(OfferingMessage.total_unread_for_offering(@offering.id)).to eq(25)
    end
  end

  describe '#total_unread_for_user' do
    it 'should have the correct unread total for the user' do
      expect(OfferingMessage.total_unread_for_user(current_user.id, @offering.id)).to eq(26)
    end
  end

end
