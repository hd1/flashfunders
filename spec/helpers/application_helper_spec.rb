require 'rails_helper'

describe ApplicationHelper do

  describe "#formatted_message" do
    it "returns a simple string" do
      expect(helper.formatted_message("simple string")).to eq("simple string")
    end

    it "returns a formatted string with a line break" do
      expect(helper.formatted_message("simple string\nwith break")).to eq("simple string<br />with break")
    end

    it "strips out any html" do
      expect(helper.formatted_message("simple <b>string</b>")).to eq("simple &lt;b&gt;string&lt;/b&gt;")
    end

    it "strips out any html but still leaves line breaks" do
      expect(helper.formatted_message("simple\n<b>string</b>")).to eq("simple<br />&lt;b&gt;string&lt;/b&gt;")
    end

  end

  describe "#page_meta_title" do
    context "on the home page" do
      it 'should return the default title' do
        expect(helper.page_meta_title).to eq(I18n.t('meta.title.default'))
      end
    end

    context "on the browse page" do
      it 'should return the title for the browse page' do
        helper.request.path = '/browse'
        expect(helper.page_meta_title).to eq(I18n.t('meta.title.browse'))
      end
    end

    context "on the My Investments page" do
      it 'should return the default title' do
        helper.request.path = '/investments'
        expect(helper.page_meta_title).to eq(I18n.t('meta.title.default'))
      end
    end
  end

  describe "#page_meta_description" do
    context "on the home page" do
      before do
        allow(helper).to receive(:controller_name).and_return('home')
      end

      it 'should return true for browse_page?' do
        expect(helper.browse_page?).to be_falsy
        expect(helper.home_page?).to be_truthy
      end

      it 'should return the default description' do
        expect(helper.page_meta_description[:content]).to eq(I18n.t('meta.description.default'))
      end
    end

    context "on the browse page" do
      before do
        allow(helper).to receive(:controller_name).and_return('browse')
      end

      it 'should return true for browse_page?' do
        expect(helper.home_page?).to be_falsy
        expect(helper.browse_page?).to be_truthy
      end

      it 'should return the description for the browse page' do
        helper.request.path = '/browse'
        expect(helper.page_meta_description[:content]).to eq(I18n.t('meta.description.browse'))
      end
    end

    context "on the My Investments page" do
      it 'should return the default description' do
        helper.request.path = '/investments'
        expect(helper.page_meta_description[:content]).to eq(I18n.t('meta.description.default'))
      end
    end
  end

end
