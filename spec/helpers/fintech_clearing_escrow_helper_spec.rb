require 'rails_helper'

describe FintechClearingEscrowHelper do
  let(:investor_user) { FactoryGirl.create(:user, email: 'john.investor@example.com') }
  let(:security) { FactoryGirl.create(:security, fund_america_id: 'd9175bf5-59f8-4016-acbf-94de6a65432f') }
  let(:offering) { FactoryGirl.create(:offering, securities: [security]) }
  let(:investment) { FactoryGirl.create(:investment,
    security: security,
    offering: offering,
    amount: BigDecimal.new(5000),
    user: investor_user,
    fund_america_id: nil,
    funding_method: 'wire',
    investor_account_id: 1) }

  let!(:investment_parameters) {
    { investment: {
        uuid: '',
        offering_uuid: 'd9175bf5-59f8-4016-acbf-94de6a65432f',
        amount: '12399.0',
        funding_method: 'funding_method_ach',
        bank_routing_number: '111111111',
        bank_account_type: 'bank_account_type_savings',
        bank_account_number: '1234567890',
        bank_account_holder: 'Lanister',
        investor_first_name: 'Patryk',
        investor_last_name: 'Kopec',
        investor_phone: '1112223333',
        investor_email: 'patryk+15@flashfunders.com',
        investor_address1: 'Thousandstreet',
        investor_address2: '',
        investor_city: 'Santa Monica',
        investor_state: 'CA',
        investor_country: 'US',
        investor_postal_code: '12345',
        investor_date_of_birth: '1979',
        investor_ssn: '61653687'
      }
    }
  }

  let!(:fintech_clearing_investment) { FintechClearing::Investment.new(
    double(HTTParty::Response, parsed_response: '{"uuid":"501ecd90-d78b-4fbf-a602-988c11f7a647"}')) }

  let(:investment_service) { FintechClearing::InvestmentService.new }

  before do
    allow(FintechClearingInvestmentParameterBuilder).to receive(:build).and_return(investment_parameters)
    allow(FintechClearing::InvestmentService).to receive(:new).and_return(investment_service)
    allow(investment_service).to receive(:create).and_return(fintech_clearing_investment)
    allow(investment_service).to receive(:update).and_return(fintech_clearing_investment)
  end

  describe 'create_fintech_clearing_investment' do
    it 'creates when fintech investment id did not exist' do
      FintechClearingEscrowHelper.update_investment(investment.id)
      expect(investment_service).to have_received(:create).with(investment_parameters)
      expect(investment_service).to_not receive(:update)
    end
  end

  describe 'update_fintech_clearing_investment' do
    before do
      investment.update_attribute('fund_america_id', '501ecd90-d78b-4fbf-a602-988c11f7a647')
    end

    it 'updates when fintech investment id does exist' do
      FintechClearingEscrowHelper.update_investment(investment.id)
      expect(investment_service).to_not have_received(:create).with(investment_parameters)
      expect(investment_service).to have_received(:update).with(investment_parameters)
    end
  end

end
