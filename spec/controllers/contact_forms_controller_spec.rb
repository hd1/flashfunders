require 'rails_helper'

describe ContactFormsController do
  let(:params) { { contact_form: {
    user_type: "startup",
    name:      "Harry Potter",
    email:     "harry@hatemuggles.com",
    message:   "I like turtles",
    page_name: "some page"
    } }
  }

  let(:args) { params[:contact_form].merge({ user_id: nil }).values }

  let(:mailer_obj) { double(ContactMailer) }


  it 'should send a message' do
    expect(ContactForm).to receive(:new).with(params[:contact_form]).and_call_original
    expect(ContactMailer).to receive(:contact_us).with(*args).and_return(mailer_obj)
    expect(mailer_obj).to receive(:deliver_later)
    post :create, params.merge(format: :js)
  end
end