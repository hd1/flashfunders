require 'rails_helper'

describe SessionsController do

  before { @request.env['devise.mapping'] = Devise.mappings[:user] }

  describe 'GET #new' do
    context 'when return_to is set in the query string' do
      let(:params) { {return_to: '/some_path'} }

      before { allow(controller).to receive(:store_location_for) }

      it 'stores the location provided' do
        get :new, params

        expect(controller).to have_received(:store_location_for).with(:user, '/some_path')
      end
    end

    context 'when return_to is not set in the query string' do
      before { allow(controller).to receive(:store_location_for) }

      it 'does not store the location provided' do
        get :new

        expect(controller).to_not have_received(:store_location_for)
      end
    end
  end

  describe 'POST #create' do
    let(:warden) { double(Warden) }
    let(:params) { {user: {email: 'foo@example.com', password: 'password'}} }

    before do
      allow(warden).to receive(:authenticate)
      allow(warden).to receive(:authenticate?)
      allow(controller).to receive(:warden).and_return(warden)
    end

    it 'authenticates user' do
      post :create, params

      expect(warden).to have_received(:authenticate).at_least(1)
    end

    context 'when user is authenticated' do
      let!(:user) { create_user }

      before do
        allow(controller).to receive(:sign_in)
        allow(warden).to receive(:authenticate).and_return(user)
      end

      it 'signs in user' do
        post :create, params

        expect(controller).to have_received(:sign_in)
      end

      it 'sets a flash message' do
        post :create, params

        expect(flash.notice).to be_present
      end

      it 'sets analytics_sign_in_email flash message' do
        post :create, params
        expect(flash[:analytics_sign_in_email][:success]).to eql(user.email)
        expect(flash[:analytics_sign_in_email][:failed]).to be_nil
      end

      it 'redirects to the \'after sign in path\'' do
        allow(controller).to receive(:after_sign_in_path_for).and_return('/some_path.html')
        post :create, params

        expect(response).to redirect_to('/some_path.html')
      end

      context 'when XHR request' do
        it 'renders the :create js template' do
          xhr :post, :create, params

          expect(response).to render_template(:create)
        end
      end

      context 'when startup cookie gets set' do
        let!(:offering) { FactoryGirl.create(:offering, issuer_user: user) }

        it 'user is an issuer' do
          post :create, params

          expect(cookies['_startup']).to eql(offering.id)
        end

        it 'user is not an issuer' do
          offering.update_attributes(issuer_user_id: nil)
          post :create, params

          expect(cookies['_startup']).to eql(nil)
        end
      end
    end

    context 'when user fails authentication' do
      before { allow(warden).to receive(:authenticate) }

      it 'sets a flash error' do
        post :create, params

        expect(flash.alert).to be_present
      end

      it 'redirects to the new user session page' do
        post :create, params

        expect(response).to render_template(:new)
      end

      context 'when XHR request' do
        it 'renders the :create js template' do
          xhr :post, :create, params

          expect(response).to render_template(:create)
        end
      end
    end

    context 'when user is locked out due to too many attempts' do
      let!(:user) { create_user(locked_at: 2.minutes.ago) }
      let(:params) { {user: {email: 'user@example.com', password: 'thepassword'}} }

      it 'sets a flash error' do
        post :create, params

        expect(flash.alert).to eq "Your account has been temporarily locked due to too many failed attempts to log in."
      end

      it 'redirects to the new user session page' do
        post :create, params

        expect(response).to render_template(:new)
      end

      context 'when XHR request' do
        it 'renders the :create js template' do
          xhr :post, :create, params

          expect(response).to render_template(:create)
        end
      end
    end

    context 'when return_to is set in the query string' do
      let(:params) { {return_to: '/some_path'} }

      before { allow(controller).to receive(:store_location_for) }

      it 'stores the location provided' do
        post :create, params

        expect(controller).to have_received(:store_location_for).with(:user, '/some_path')
      end
    end

    context 'when return_to is not set in the query string' do
      before { allow(controller).to receive(:store_location_for) }

      it 'does not store the location provided' do
        post :create

        expect(controller).to_not have_received(:store_location_for)
      end
    end
  end

  describe '#session_info' do
    context 'when no one is signed in' do
      it 'should be unauthorized' do
        get :session_info
        expect(response).to be_unauthorized
      end
    end

    context 'when a user is signed in' do
      # let(:last_request_ago) { 8 }
      let(:last_request_time) { (Time.now - 8.minutes).to_i }

      before do
        @request.env["devise.mapping"] = Devise.mappings[:user]
        user = FactoryGirl.create(:user)
        sign_in user
        controller.user_session['last_request_at'] = last_request_time
      end

      it 'should not update the last_request_at for the user' do
        # this is not actually testing anything because warden doesn't run during this test
        get :session_info

        warden.session(:user)
        expect(response).to be_success

        expect(controller.user_session['last_request_at']).to eq(last_request_time)
      end

      it 'should update the last_request_at for the user if reset_timer is passed' do
        # This should be a test, but warden doesn't actually run during controller specs so we can't test it
      end

      it 'should update the last_request_at if last_event is sent' do
        get :session_info, last_event: 10

        expect(response).to be_success
        expect(controller.user_session['last_request_at']).to_not eq(last_request_time)
      end

      it 'should default to 5 minutes if the warning time env var is not present' do
        set_env('USER_TIMEOUT_WARNING', nil) do
          get :session_info
          hash = JSON.parse(response.body)
          expect(hash['warning_time']).to eq(5 * 60)
        end
      end

      it 'should use the env var for warning time' do
        set_env('USER_TIMEOUT_WARNING', '123') do
          get :session_info
          hash = JSON.parse(response.body)
          expect(hash['warning_time']).to eq(ENV['USER_TIMEOUT_WARNING'].to_i * 60)
        end
      end

      it 'should return the total session time' do
        get :session_info
        hash = JSON.parse(response.body)
        expect(hash['total_session_time']).to eq(Devise.timeout_in)
      end

      it 'should return the time left' do
        Timecop.freeze do
          get :session_info, last_event: 45
          hash = JSON.parse(response.body)
          expect(hash['time_left']).to eq(Devise.timeout_in - 45)
        end
      end

      private

      def set_env(var, val)
        original = ENV[var]
        val.nil? ? ENV.delete(var) : ENV[var] = val

        yield

        ENV[var] = original
      end
    end
  end

end
