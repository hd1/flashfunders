require 'rails_helper'

describe Invest::InvestmentsController do

  context 'for an investment in a stock offering' do

    let(:security_double) { mock_model(Securities::FspStock, is_spv?: false) }
    let(:offering_double) { mock_model(Offering, id: 78, ended?: false, reg_c_enabled?: false, securities: [security_double]) }
    let(:escrow_provider_double) { mock_model(EscrowProvider, id: 78)}

    let(:investment_double) { mock_model(Investment, id: 32, offering_id: 78, state: "started") }
    let(:investor_investment_form_double) { double(InvestorInvestmentForm, create: nil, update: true, investment_amount: 43, investment_id: 27, errors: {}) }

    let(:offering_presenter_double) do
      double(OfferingPresenter,
             share_price: 27,
             fully_diluted_shares: 56,
             shares_offered: 78)
    end

    let(:user) { create_user }

    before do
      allow(controller).to receive(:offering_presenter).and_return(offering_presenter_double)
      allow(Offering).to receive(:find).and_return(offering_double)
      allow(investment_double).to receive(:offering).and_return(offering_double)
      allow(offering_double).to receive(:_read_attribute).with("id").and_return(offering_double.id)
      allow(InvestorInvestmentForm).to receive(:new).and_return(investor_investment_form_double)
      allow(investor_investment_form_double).to receive(:create).and_return(investment_double)
      allow(investment_double).to receive(:started?)
      allow(investment_double).to receive(:intended?).and_return(false)
      allow(investment_double).to receive(:resumable?).and_return(false)
      allow(investment_double).to receive(:signed_documents?).and_return(false)
      allow(investment_double).to receive(:entity_selected?).and_return(false)
      allow(investment_double).to receive(:linked_refund_account?).and_return(false)
      allow(investment_double).to receive(:accreditation_verified?).and_return(false)
      allow(investment_double).to receive(:declined_documents?).and_return(false)
      allow(investment_double).to receive(:wire_selected?).and_return(false)
      allow(investment_double).to receive(:check_selected?).and_return(false)
      allow(investment_double).to receive(:refunded?).and_return(false)
      allow(investment_double).to receive(:resumable?).and_return(true)
      allow(offering_double).to receive(:escrow_provider).and_return(escrow_provider_double)
      allow(EscrowProvider).to receive(:find).and_return(escrow_provider_double)
    end

    describe 'GET new' do
      let(:offering_follower_double) { double(OfferingFollower, follow_as!: nil) }

      before do
        sign_in :user, user
        allow(OfferingFollower).to receive(:find_or_initialize_by).and_return(offering_follower_double)
        allow(Investment).to receive_message_chain(:where, :where, :not, :first_or_initialize).and_return(investment_double)
      end

      it 'is successful' do
        get :new, offering_id: 78

        expect(response.code).to eq('200')
        expect(offering_follower_double).to have_received(:follow_as!).with(:investor)
      end
    end

    describe 'POST create' do
      let(:investor_investment_calculator_double) do
        double(InvestorInvestmentCalculator,
               projected_cost: 700.54,
               projected_shares: 48,
               projected_percent_ownership: 79.345)
      end

      let(:auditor_double) { double(Auditor, write: nil) }
      let(:offering_follower_double) { double(OfferingFollower, follow_as!: nil) }

      before do
        allow(controller).to receive(:investor_investment_form_validator).and_return(investor_investment_form_validator_double)
        allow(controller).to receive(:investor_investment_calculator).and_return(investor_investment_calculator_double)
        allow(controller).to receive(:auditor).and_return(auditor_double)
        allow(controller).to receive(:authenticate_user!)
        allow(OfferingFollower).to receive(:find_or_initialize_by).and_return(offering_follower_double)
        allow(Investment).to receive_message_chain(:where, :where, :not, :first_or_initialize).and_return(investment_double)
      end

      context 'when the form is valid' do
        let(:investor_investment_form_validator_double) { double(InvestorInvestmentFormValidator, valid?: true) }

        before { sign_in :user, user }

        it 'authenticates the user' do
          post :create, offering_id: 78, investment: {foo: 'bar'}

          expect(controller).to have_received(:authenticate_user!)
          expect(offering_follower_double).to have_received(:follow_as!).with(:investor)
        end

        it 'saves the investment form and redirects to the investment documents path' do
          post :create, offering_id: 78, investment: {investment_amount: 6782, unrounded_investment_amount: 6785}

          expect(investor_investment_form_double).to have_received(:create)
        end

        it 'audits the investment' do
          current_time = Time.now
          allow(Time).to receive(:now).and_return(current_time)

          set_request_ip('1.2.3.4')

          post :create, offering_id: 78, id: 993, investment: {investment_amount: 6782, unrounded_investment_amount: 6785, investment_shares: 27, investment_percent_ownership: 28.734}

          expect(auditor_double).to have_received(:write).with(
                                        {
                                            desired_investment_amount: '6785',
                                            displayed_investment_amount: '6782',
                                            actual_investment_amount: '700.54',
                                            displayed_investment_shares: '27',
                                            actual_investment_shares: '48',
                                            displayed_investment_percent_ownership: '28.734',
                                            actual_investment_percent_ownership: '79.345',
                                        }, user.id, '1.2.3.4', '/offering/78/investment', current_time.utc, nil
                                    )
        end
      end

      context 'when the form is invalid' do
        let(:investor_investment_form_validator_double) { double(InvestorInvestmentFormValidator, valid?: false) }

        it 'renders new' do
          post :create, offering_id: 78, investment: {investment_amount: nil, unrounded_investment_amount: nil, offering_id: nil}

          expect(response).to render_template('investment_stock_form')
          expect(assigns(:investor_investment)).to eq(investor_investment_form_double)
          expect(investor_investment_form_double).to_not have_received(:create)
        end
      end

      context 'after the user has completed their profile' do
        let(:investor_investment_form_validator_double) { double(InvestorInvestmentFormValidator) }

        before { allow(investment_double).to receive(:state).and_return("accreditation_verified") }

        it 'this action is off limits' do
          post :create, offering_id: offering_double.id

          expect(response).to redirect_to(offering_invest_investment_documents_path(offering_double))
        end
      end
    end

    describe 'PATCH update' do

      let(:investor_investment_calculator_double) do
        double(InvestorInvestmentCalculator,
               projected_cost: 700.54,
               projected_shares: 48,
               projected_percent_ownership: 79.345)
      end

      let(:auditor_double) { double(Auditor, write: nil) }
      let!(:investor_entity) {FactoryGirl.create(:investor_entity) }
      let!(:offering) { FactoryGirl.create(:offering, id: 78) }
      let!(:investment) { FactoryGirl.create(:investment, user: user, investor_entity: investor_entity, offering: offering, state: :accreditation_verified) }

      before do
        sign_in :user, user

        allow(controller).to receive(:investor_investment_form_validator).and_return(investor_investment_form_validator_double)
        allow(controller).to receive(:investor_investment_calculator).and_return(investor_investment_calculator_double)
        allow(controller).to receive(:auditor).and_return(auditor_double)
      end

      let(:investor_investment_form_validator_double) { double(InvestorInvestmentFormValidator, valid?: true) }

      it 'initializes an investor investment form with an investment id' do
        patch :update, offering_id: offering.id, id: investment.id, investment: {investment_amount: 6782.36}

        expect(InvestorInvestmentForm).to have_received(:new).with(
                                              hash_including(:investment_id),
                                              investor_investment_form_validator_double)
      end

      context 'when the form is valid' do
        let(:investor_investment_form_validator_double) { double(InvestorInvestmentFormValidator, valid?: true) }

        it 'saves the investment form and redirects to the investment documents path' do
          patch :update, offering_id: 78, id: 993, investment: {investment_amount: 6782, unrounded_investment_amount: 6785}

          expect(response).to redirect_to offering_invest_investment_documents_path(offering_id: 78)
          expect(investor_investment_form_double).to have_received(:update)
        end

        it 'audits the investment' do
          current_time = Time.now
          allow(Time).to receive(:now).and_return(current_time)

          set_request_ip('1.2.3.4')

          patch :update, offering_id: 78, id: 993, investment: {investment_amount: 6782, unrounded_investment_amount: 6785, investment_shares: 27, investment_percent_ownership: 28.734}

          expect(auditor_double).to have_received(:write).with(
                                        {
                                            desired_investment_amount: '6785',
                                            displayed_investment_amount: '6782',
                                            actual_investment_amount: '700.54',
                                            displayed_investment_shares: '27',
                                            actual_investment_shares: '48',
                                            displayed_investment_percent_ownership: '28.734',
                                            actual_investment_percent_ownership: '79.345',
                                        }, user.id, '1.2.3.4', '/offering/78/investment', current_time.utc, nil
                                    )
        end
      end

      context 'when the form is invalid' do
        let(:investor_investment_form_validator_double) { double(InvestorInvestmentFormValidator, valid?: false) }

        it 'renders new' do
          patch :update, offering_id: 78, id: 993, investment: {investment_amount: nil, unrounded_investment_amount: nil, offering_id: nil}

          expect(response).to render_template('investment_stock_form')
          expect(assigns(:investor_investment)).to eq(investor_investment_form_double)
          expect(investor_investment_form_double).to_not have_received(:update)
        end
      end


      context 'but they have declined the docusign docs' do
        let(:investor_investment_form_validator_double) { double(InvestorInvestmentFormValidator, valid?: true) }

        before do
          allow(investment_double).to receive_messages(signed_documents?: false,
                            declined_documents?: true, state: "declined_documents")
        end

        it 'this action is ON limits' do
          patch :update, offering_id: 78, id: 993, investment: {investment_amount: 6782, unrounded_investment_amount: 6785}

          expect(investor_investment_form_double).to have_received(:update)
          expect(response).to redirect_to offering_invest_investment_documents_path(offering_id: 78)
        end
      end
    end

    describe 'GET #recreate' do
      let(:investor_investment_calculator_double) do
        double(InvestorInvestmentCalculator,
               projected_cost: 700.54,
               projected_shares: 48,
               projected_percent_ownership: 79.345)
      end
      let(:auditor_double) { double(Auditor, write: nil) }
      let(:investor_investment_form_validator_double) { double(InvestorInvestmentFormValidator, valid?: true) }
      let!(:existing_offering) { FactoryGirl.create(:offering) }
      let!(:existing_investment) { FactoryGirl.create(:investment, offering: existing_offering, user: user) }
      let(:new_investment) { FactoryGirl.build(:investment, offering: offering_double) }

      before do
        sign_in :user, user
        session[:recreate_params] = { investment: { investment_amount: 433.47, unrounded_investment_amount: 434, investment_shares: 386, investment_percent_ownership: 0.097 } }.stringify_keys
        allow(investor_investment_form_double).to receive(:valid?).and_return(false)

        allow(controller).to receive(:investor_investment_form_validator).and_return(investor_investment_form_validator_double)
        allow(controller).to receive(:investor_investment_calculator).and_return(investor_investment_calculator_double)
        allow(controller).to receive(:auditor).and_return(auditor_double)
        allow(controller).to receive(:investment).and_return(new_investment)
        allow(investment_double).to receive(:offering).and_return(offering_double)
      end

      after do
        session[:recreate_params] = nil
      end

      it 'sets the params to the stored session investment params' do
        get :recreate, offering_id: 78
        expect(@controller.params['offering_id']).to eq('78')
        expect(@controller.params['investment']).to eq({ investment_amount: 433.47, unrounded_investment_amount: 434, investment_shares: 386, investment_percent_ownership: 0.097 }.stringify_keys)
      end

      it 'clears the session after calling recreate' do
        get :recreate, offering_id: 78

        expect(@controller.session[:recreate_params]).to be_nil
      end

      it 'delegates to create' do
        expect(@controller).to receive(:create).and_call_original

        get :recreate, offering_id: 78
      end

      context "existing offering" do
        before do
          allow(Offering).to receive(:find).and_return(existing_offering)
          allow(controller).to receive(:investment).and_return(existing_investment)
        end
        it 'redirects without calling create if an investment exists' do
          expect(@controller).to_not receive(:create)

          get :recreate, offering_id: existing_offering.id

          expect(response).to redirect_to new_offering_investment_path(existing_offering)
        end
      end
    end
  end

  context 'for an investment in a convertible note offering' do
    let(:security_double) { mock_model(Securities::FspConvertible, is_spv?: false) }
    let(:offering_double) { mock_model(Offering, id: 78, ended?: false, securities: [security_double], reg_c_enabled?: false) }
    let(:escrow_provider_double) { mock_model(EscrowProvider, id: 78)}

    let(:investment_double) { mock_model(Investment, id: 32, state: 'started') }
    let(:investor_investment_form_double) { double(InvestorInvestmentForm, create: nil, update: true, investment_amount: 43, investment_id: 27, errors: {}) }

    let(:offering_presenter_double) { double(OfferingPresenter) }

    let(:user) { create_user }

    before do
      allow(controller).to receive(:offering_presenter).and_return(offering_presenter_double)
      allow(Offering).to receive(:find).and_return(offering_double)
      allow(investment_double).to receive(:offering).and_return(offering_double)
      allow(investment_double).to receive(:offering_id).and_return(offering_double.id)
      allow(offering_double).to receive(:_read_attribute).with("id").and_return(offering_double.id)
      allow(InvestorInvestmentForm).to receive(:new).and_return(investor_investment_form_double)
      allow(investor_investment_form_double).to receive(:create).and_return(investment_double)
      allow(investment_double).to receive(:started?)
      allow(investment_double).to receive(:intended?).and_return(false)
      allow(investment_double).to receive(:resumable?).and_return(false)
      allow(investment_double).to receive(:signed_documents?).and_return(false)
      allow(investment_double).to receive(:entity_selected?).and_return(false)
      allow(investment_double).to receive(:linked_refund_account?).and_return(false)
      allow(investment_double).to receive(:accreditation_verified?).and_return(false)
      allow(investment_double).to receive(:declined_documents?).and_return(false)
      allow(investment_double).to receive(:wire_selected?).and_return(false)
      allow(investment_double).to receive(:check_selected?).and_return(false)
      allow(investment_double).to receive(:refunded?).and_return(false)
      allow(investment_double).to receive(:resumable?).and_return(true)
      allow(offering_double).to receive(:escrow_provider).and_return(escrow_provider_double)
      allow(EscrowProvider).to receive(:find).and_return(escrow_provider_double)
    end

    describe 'GET new' do
      let(:offering_follower_double) { double(OfferingFollower, follow_as!: nil) }

      before do
        sign_in :user, user
        allow(OfferingFollower).to receive(:find_or_initialize_by).and_return(offering_follower_double)
        allow(Investment).to receive_message_chain(:where, :where, :not, :first_or_initialize).and_return(investment_double)
      end

      it 'is successful' do
        get :new, offering_id: 78

        expect(response.code).to eq('200')
        expect(offering_follower_double).to have_received(:follow_as!).with(:investor)
      end
    end

    describe 'POST create' do
      let(:investor_investment_calculator_double) do
        double(InvestorInvestmentCalculator,
               projected_cost: 700.54,
               projected_percent_ownership: 79.345)
      end

      let(:auditor_double) { double(Auditor, write: nil) }
      let(:offering_follower_double) { double(OfferingFollower, follow_as!: nil) }

      before do
        allow(controller).to receive(:investor_investment_form_validator).and_return(investor_investment_form_validator_double)
        allow(controller).to receive(:investor_investment_calculator).and_return(investor_investment_calculator_double)
        allow(controller).to receive(:auditor).and_return(auditor_double)
        allow(controller).to receive(:authenticate_user!)
        allow(investment_double).to receive(:offering).and_return(offering_double)
        allow(OfferingFollower).to receive(:find_or_initialize_by).and_return(offering_follower_double)
        allow(Investment).to receive_message_chain(:where, :where, :not, :first_or_initialize).and_return(investment_double)
      end

      context 'when the form is valid' do
        let(:investor_investment_form_validator_double) { double(InvestorInvestmentFormValidator, valid?: true) }
        let(:investor_investment_calculator_double) do
          double(InvestorInvestmentCalculator,
                 projected_cost: 700.54,
                 projected_shares: 0,
                 projected_percent_ownership: 79.345)
        end
        before { sign_in :user, user }

        it 'authenticates the user' do
          post :create, offering_id: 78, investment: {foo: 'bar'}

          expect(controller).to have_received(:authenticate_user!)
          expect(offering_follower_double).to have_received(:follow_as!).with(:investor)
        end

        it 'saves the investment form and redirects to the investment documents path' do
          post :create, offering_id: 78, investment: {investment_amount: 6782, unrounded_investment_amount: 6785}

          expect(investor_investment_form_double).to have_received(:create)
        end

        it 'audits the investment' do
          current_time = Time.now
          allow(Time).to receive(:now).and_return(current_time)

          set_request_ip('1.2.3.4')

          post :create, offering_id: 78, id: 993, investment: {investment_amount: 6782, unrounded_investment_amount: 6785, investment_percent_ownership: 28.734}

          expect(auditor_double).to have_received(:write).with(
                                        {
                                            desired_investment_amount: '6785',
                                            displayed_investment_amount: '6782',
                                            actual_investment_amount: '700.54',
                                            displayed_investment_shares: nil,
                                            actual_investment_shares: '0',
                                            displayed_investment_percent_ownership: '28.734',
                                            actual_investment_percent_ownership: '79.345',
                                        }, user.id, '1.2.3.4', '/offering/78/investment', current_time.utc, nil
                                    )
        end
      end

      context 'when the form is invalid' do
        let(:investor_investment_form_validator_double) { double(InvestorInvestmentFormValidator, valid?: false) }
        let(:investor_investment_calculator_double) do
          double(InvestorInvestmentCalculator,
                 projected_cost: 700.54,
                 projected_shares: 0,
                 projected_percent_ownership: 79.345)
        end

        it 'renders new' do
          post :create, offering_id: 78, investment: {investment_amount: nil, unrounded_investment_amount: nil, offering_id: nil}

          expect(response).to render_template('investment_notes_form')
          expect(assigns(:investor_investment)).to eq(investor_investment_form_double)
          expect(investor_investment_form_double).to_not have_received(:create)
        end
      end

      context 'after the user has completed their profile' do
        let(:investor_investment_form_validator_double) { double(InvestorInvestmentFormValidator) }

        before { allow(investment_double).to receive(:state).and_return("accreditation_verified") }

        it 'this action is off limits' do
          post :create, offering_id: offering_double.id

          expect(response).to redirect_to(offering_invest_investment_documents_path(offering_double))
        end
      end
    end

    describe 'PATCH update' do

      let(:investor_investment_calculator_double) do
        double(InvestorInvestmentCalculator,
               projected_cost: 700.54,
               projected_shares: 0,
               projected_percent_ownership: 79.345)
      end

      let(:auditor_double) { double(Auditor, write: nil) }

      before do
        sign_in :user, user

        allow(controller).to receive(:investor_investment_form_validator).and_return(investor_investment_form_validator_double)
        allow(controller).to receive(:investor_investment_calculator).and_return(investor_investment_calculator_double)
        allow(controller).to receive(:auditor).and_return(auditor_double)
        allow(Investment).to receive_message_chain(:where, :where, :not, :first_or_initialize).and_return(investment_double)
        allow(investment_double).to receive(:state).and_return('accreditation_verified')
        allow(investment_double).to receive(:offering_id).and_return(offering_double.id)
      end

      let(:investor_investment_form_validator_double) { double(InvestorInvestmentFormValidator, valid?: true) }

      it 'intializes an investor investment form with an investment id' do
        patch :update, offering_id: 78, id: 32, investment: {investment_amount: 6782.36}

        expect(InvestorInvestmentForm).to have_received(:new).with(
                                              hash_including(:investment_id),
                                              investor_investment_form_validator_double)
      end

      context 'when the form is valid' do
        let(:investor_investment_form_validator_double) { double(InvestorInvestmentFormValidator, valid?: true) }

        it 'saves the investment form and redirects to the investment documents path' do
          patch :update, offering_id: 78, id: 993, investment: {investment_amount: 6782, unrounded_investment_amount: 6785}

          expect(response).to redirect_to offering_invest_investment_documents_path(offering_id: 78)
          expect(investor_investment_form_double).to have_received(:update)
        end

        it 'audits the investment' do
          current_time = Time.now
          allow(Time).to receive(:now).and_return(current_time)

          set_request_ip('1.2.3.4')

          patch :update, offering_id: 78, id: 993, investment: {investment_amount: 6782, unrounded_investment_amount: 6785, investment_percent_ownership: 28.734}

          expect(auditor_double).to have_received(:write).with(
                                        {
                                            desired_investment_amount: '6785',
                                            displayed_investment_amount: '6782',
                                            actual_investment_amount: '700.54',
                                            displayed_investment_shares: nil,
                                            actual_investment_shares: '0',
                                            displayed_investment_percent_ownership: '28.734',
                                            actual_investment_percent_ownership: '79.345',
                                        }, user.id, '1.2.3.4', '/offering/78/investment', current_time.utc, nil
                                    )
        end
      end

      context 'when the form is invalid' do
        let(:investor_investment_form_validator_double) { double(InvestorInvestmentFormValidator, valid?: false) }

        it 'renders new' do
          patch :update, offering_id: 78, id: 993, investment: {investment_amount: nil, unrounded_investment_amount: nil, offering_id: nil}

          expect(response).to render_template('investment_notes_form')
          expect(assigns(:investor_investment)).to eq(investor_investment_form_double)
          expect(investor_investment_form_double).to_not have_received(:update)
        end
      end

      context 'but they have declined the docusign docs' do
        let(:investor_investment_form_validator_double) { double(InvestorInvestmentFormValidator, valid?: true) }
        before do
          allow(investment_double).to receive_messages(signed_documents?: false, declined_documents?: true, state: 'declined_documents')
        end

        it 'this action is ON limits' do
          patch :update, offering_id: 78, id: 993, investment: {investment_amount: 6782, unrounded_investment_amount: 6785}

          expect(response).to redirect_to offering_invest_investment_documents_path(offering_id: 78)
          expect(investor_investment_form_double).to have_received(:update)
        end
      end
    end

  end

  context 'for an investment in an llc offering' do

    let(:security_double) { mock_model(Securities::FspLlc, is_spv?: false) }
    let(:offering_double) { mock_model(Offering, id: 78, ended?: false, securities: [security_double], reg_c_enabled?: false) }
    let(:escrow_provider_double) { mock_model(EscrowProvider, id: 42)}

    let(:investment_double) { mock_model(Investment, id: 32, offering_id: 78, state: "started") }
    let(:investor_investment_form_double) { double(InvestorInvestmentForm, create: nil, update: true, investment_amount: 43, investment_id: 27, errors: {}) }

    let(:offering_presenter_double) do
      double(OfferingPresenter,
             share_price: 27,
             fully_diluted_shares: 56,
             shares_offered: 78)
    end

    let(:user) { create_user }

    before do
      allow(controller).to receive(:offering_presenter).and_return(offering_presenter_double)
      allow(Offering).to receive(:find).and_return(offering_double)
      allow(investment_double).to receive(:offering).and_return(offering_double)
      allow(offering_double).to receive(:_read_attribute).with("id").and_return(offering_double.id)
      allow(InvestorInvestmentForm).to receive(:new).and_return(investor_investment_form_double)
      allow(investor_investment_form_double).to receive(:create).and_return(investment_double)
      allow(investment_double).to receive(:started?)
      allow(investment_double).to receive(:intended?).and_return(false)
      allow(investment_double).to receive(:resumable?).and_return(false)
      allow(investment_double).to receive(:signed_documents?).and_return(false)
      allow(investment_double).to receive(:entity_selected?).and_return(false)
      allow(investment_double).to receive(:linked_refund_account?).and_return(false)
      allow(investment_double).to receive(:accreditation_verified?).and_return(false)
      allow(investment_double).to receive(:declined_documents?).and_return(false)
      allow(investment_double).to receive(:wire_selected?).and_return(false)
      allow(investment_double).to receive(:check_selected?).and_return(false)
      allow(investment_double).to receive(:refunded?).and_return(false)
      allow(investment_double).to receive(:resumable?).and_return(true)
      allow(offering_double).to receive(:escrow_provider).and_return(escrow_provider_double)
      allow(EscrowProvider).to receive(:find).and_return(escrow_provider_double)
    end

    describe 'POST create' do
      let(:investor_investment_calculator_double) do
        double(InvestorInvestmentCalculator,
               projected_cost: 700.54,
               projected_shares: 48,
               projected_percent_ownership: 79.345)
      end

      let(:auditor_double) { double(Auditor, write: nil) }
      let(:offering_follower_double) { double(OfferingFollower, follow_as!: nil) }

      before do
        allow(controller).to receive(:investor_investment_form_validator).and_return(investor_investment_form_validator_double)
        allow(controller).to receive(:investor_investment_calculator).and_return(investor_investment_calculator_double)
        allow(controller).to receive(:auditor).and_return(auditor_double)
        allow(controller).to receive(:authenticate_user!)
        allow(OfferingFollower).to receive(:find_or_initialize_by).and_return(offering_follower_double)
        allow(Investment).to receive_message_chain(:where, :where, :not, :first_or_initialize).and_return(investment_double)
      end

      context 'when the form is invalid' do
        let(:investor_investment_form_validator_double) { double(InvestorInvestmentFormValidator, valid?: false) }

        it 'renders new' do
          post :create, offering_id: 78, investment: {investment_amount: nil, unrounded_investment_amount: nil, offering_id: nil}

          expect(response).to render_template('investment_stock_form')
          expect(assigns(:investor_investment)).to eq(investor_investment_form_double)
          expect(investor_investment_form_double).to_not have_received(:create)
        end
      end

    end

    describe 'PATCH update' do

      let(:investor_investment_calculator_double) do
        double(InvestorInvestmentCalculator,
               projected_cost: 700.54,
               projected_shares: 48,
               projected_percent_ownership: 79.345)
      end

      let(:auditor_double) { double(Auditor, write: nil) }
      let!(:investor_entity) {FactoryGirl.create(:investor_entity) }
      let!(:offering) { FactoryGirl.create(:offering, id: 78) }
      let!(:investment) { FactoryGirl.create(:investment, user: user, investor_entity: investor_entity, offering: offering, state: :accreditation_verified) }

      before do
        sign_in :user, user

        allow(controller).to receive(:investor_investment_form_validator).and_return(investor_investment_form_validator_double)
        allow(controller).to receive(:investor_investment_calculator).and_return(investor_investment_calculator_double)
        allow(controller).to receive(:auditor).and_return(auditor_double)
      end

      let(:investor_investment_form_validator_double) { double(InvestorInvestmentFormValidator, valid?: true) }

      context 'when the form is invalid' do
        let(:investor_investment_form_validator_double) { double(InvestorInvestmentFormValidator, valid?: false) }

        it 'renders new' do
          patch :update, offering_id: 78, id: 993, investment: {investment_amount: nil, unrounded_investment_amount: nil, offering_id: nil}

          expect(response).to render_template('investment_stock_form')
          expect(assigns(:investor_investment)).to eq(investor_investment_form_double)
          expect(investor_investment_form_double).to_not have_received(:update)
        end
      end
    end
  end
end
