require 'rails_helper'

module Invest
  describe InvestmentProfilesController do

    let(:user) { create_user }
    let(:security) { FactoryGirl.create(:fsp_stock) }
    let(:investment) do
      Investment.create(amount: 10000.00,
                     id: 43,
                     shares: 27,
                     state: 'intended',
                     offering_id: 42,
                     percent_ownership: 26.745,
                     security_id: security.id,
                     user_id: user.id)
    end
    let(:offering_double) { mock_model(Offering, id: 42, ended?: false, escrow_end_date: 10.days.from_now, reg_c_enabled?: false) }
    let(:escrow_provider_double) { mock_model(EscrowProvider, id: 42)}
    let(:entity) { InvestorEntity.create(user_id: user.id) }

    before do
      sign_in :user, user
      allow(Investment).to receive_message_chain(:where, :where, :not, :first_or_initialize).and_return(investment)
      allow(Investment).to receive_message_chain(:where, :pluck).with(:investor_entity_id).and_return([entity.id])
      allow(investment).to receive(:resumable?).and_return(true)
      allow(Offering).to receive(:find).and_return(offering_double)
      allow(investment).to receive(:offering).and_return(offering_double)
      allow(offering_double).to receive(:escrow_provider).and_return(escrow_provider_double)
      allow(EscrowProvider).to receive(:find).and_return(escrow_provider_double)
    end

    describe 'GET new' do
    end

    describe 'POST #create' do
      let(:form_params) { {name: 'trust',
                           form_type: 'trust',
                           tax_id_number: '123456789',
                           formation_date: '12/12/2012',
                           address_1: '123 Main st.',
                           address_2: '',
                           city: 'Anytown',
                           country: 'US',
                           state_id: 'AK',
                           formation_state_id: 'AL',
                           zip_code: '12345',
                           phone_number: '1234567890',
                           position: 'Chief'} }
      let(:form_double) { double(EntityForm, save: true, attributes: form_params) }

      before do
        allow(EntityForm).to receive(:new).and_return(form_double)
        investment.state = 'entity_selected'
      end

      context 'when user is an investor' do
        let(:user) { create_user }

        before do
          investment.state = 'intended'
        end

        context 'on success' do
          let!(:orphan_entity) { InvestorEntity.create(user_id: user.id) }
          let!(:qualification) { Accreditor::IndividualIncomeQualification.create!(user_id: user.id, investor_entity_id: orphan_entity.id, data: Hash.new) }
          before do
            allow(form_double).to receive(:save).and_return(true)
            allow(form_double).to receive(:entity).and_return(entity)
            allow(entity).to receive_message_chain(:accreditation, :status, :approved?).and_return(true)
            allow(investment).to receive(:select_entity!).and_call_original
            allow(investment).to receive(:is_reg_d?).and_return(true)
            allow(investment).to receive(:is_reg_s?).and_return(false)
            allow(investment).to receive(:is_reg_c?).and_return(false)
          end

          it 'updates the state of the investment and verifies orphans are removed' do
            expect(InvestorEntity.count).to eq(2)
            expect(Accreditor::Qualification.count).to eq(1)

            post :create, offering_id: 42, entity_new: form_params

            expect(investment).to have_received(:select_entity!)
            expect(InvestorEntity.count).to eq(1)
            expect(Accreditor::Qualification.count).to eq(0)
          end

          it 'updates the entity of the investment' do
            post :create, offering_id: 42, entity_new: form_params

            expect(investment.investor_entity).to eq(entity)
          end

          it 'updates the accreditation status' do
            post :create, offering_id: 42, entity_new: form_params

            expect(investment).to be_verified
          end

          it 'redirects to the accreditation page' do
            post :create, offering_id: 42, entity_new: form_params

            expect(response).to redirect_to(new_offering_invest_accreditation_new_path(offering_double))
          end

          context 'auditing form submissions' do
            let(:accreditation_form_auditor_double) { double(Auditor, write: true) }
            let(:stateful_params) { form_params.merge('form_class' => form_double.class.to_s) }

            before do
              allow(controller).to receive(:auditor).and_return(accreditation_form_auditor_double)
            end

            it 'write the information to the audit log' do
              allow_any_instance_of(Time).to receive(:utc).and_return('footime')

              set_request_ip('1.2.3.4')

              post :create, offering_id: 42, entity_new: form_params

              expect(accreditation_form_auditor_double).to have_received(:write).
                                                               with(hash_including(stateful_params), user.id, '1.2.3.4', '/offering/42/invest/investment_profile', 'footime', nil)
            end
          end
        end
        context 'on failure' do
          before { allow(form_double).to receive(:save).and_return(false) }

          it 're-renders new' do
            post :create, offering_id: 42, entity_new: form_params

            expect(response).to render_template('new')
          end
        end
      end
    end

    describe '#idology_locate' do
      let(:individual_investor_entity) { FactoryGirl.create(:investor_entity_individual, :with_pfii) }

      before do
        individual_investor_entity.create_personal_federal_identification_information(
          first_name: 'John',
          last_name: 'Smith',
          address1: '222333 Peachtree Place',
          address2: 'Mars',
          city: 'Atlanta',
          state_id: 'GA',
          zip_code: '30318',
          date_of_birth: Date.new(1975,2,1),
          daytime_phone: '333-333-4444',
          ssn: '112223333')
      end

      it 'can locate entity from IDology' do
        VCR.use_cassette('idology_locate') do
          expect(individual_investor_entity).to be_pending

          controller.send(:idology_locate, individual_investor_entity)

          expect(individual_investor_entity).to be_verified
          expect(individual_investor_entity.idology_id).to eq('1424589644')
          expect(user.reload).to be_allowed
        end
      end

      it 'cannot locate entity from IDology' do
        individual_investor_entity.personal_federal_identification_information
          .update_attributes(first_name: "Jon", last_name: "Snow")

        VCR.use_cassette('idology_locate_not_found') do
          expect(individual_investor_entity).to be_pending

          controller.send(:idology_locate, individual_investor_entity)

          expect(individual_investor_entity).to be_pending
          expect(individual_investor_entity.idology_id).to eq('1424602768')
          expect(user.reload).to be_pending
        end
      end

      it 'raises idoloy error' do
        allow(Honeybadger).to receive(:notify)
        individual_investor_entity.personal_federal_identification_information
          .update_attribute(:state_id, "invalid state")

        VCR.use_cassette('idology_locate_errors') do
          controller.send(:idology_locate, individual_investor_entity)
          expect(Honeybadger).to have_received(:notify).with(IDology::Error)
          expect(individual_investor_entity.idology_id).to be(nil)
        end
      end
    end
  end
end
