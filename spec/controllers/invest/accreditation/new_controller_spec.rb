require 'rails_helper'

module Invest
  module Accreditation
    describe NewController do

      let(:user) { create_user }
      let(:entity_name) { 'John Doe, LLC' }
      let(:investor_entity) { InvestorEntity.new name: entity_name, country: 'US' }
      let(:security) { FactoryGirl.create(:fsp_stock) }
      let(:investment) do
        Investment.create(amount: 10000.00,
                       id: 43,
                       shares: 27,
                       offering_id: 42,
                       percent_ownership: 26.745,
                       user_id: user.id,
                       security_id: security.id,
                       investor_entity: investor_entity)
      end
      let(:offering_double) { mock_model(Offering, id: 42, ended?: false, reg_c_enabled?: false) }
      let(:escrow_provider_double) { mock_model(EscrowProvider, id: 42)}

      before do
        sign_in :user, user
        allow(controller).to receive(:current_user).and_return(user)
        allow(controller).to receive(:investor_entity).and_return(investor_entity)
        allow(Investment).to receive_message_chain(:where, :where, :not, :first_or_initialize).and_return(investment)
        allow(Offering).to receive(:find).and_return(offering_double)
        allow(investment).to receive(:offering).and_return(offering_double)
        allow(offering_double).to receive(:escrow_closed?).and_return(false)
        allow(offering_double).to receive(:escrow_end_date).and_return(10.days.from_now)
        allow(controller).to receive(:offering).and_return(offering_double)
        allow(offering_double).to receive(:escrow_provider).and_return(escrow_provider_double)
        allow(EscrowProvider).to receive(:find).and_return(escrow_provider_double)
      end

      describe 'GET new' do
        before do
          investment.state = 'entity_selected'
          investment.save
        end

        it 'renders the new accreditation verification page' do
          get :new, offering_id: 42

          expect(assigns(:uploader)).to be_a(AccreditationDocumentUploader)
          expect(response).to be_success
        end
      end

      describe 'POST #create' do
        let(:full_name) { 'John Doe' }
        let(:form_double) { double(AccreditationForm, save: true, attributes: form_params, docs: []) }
        let(:form_params) { { role: 'abc', email: 'foo@example.com', consent_to_income: true, credit_report_authorization: true, spouse_name: 'Spouse', spouse_email: 'spouse@spouse.ly', spouse_ssn: '123456789', file_keys: ['/upload/foo.png'] } }

        before do
          allow(user).to receive(:full_name).and_return(full_name)
          allow(AccreditationForm).to receive(:new).and_return(form_double)
          investment.state = 'entity_selected'
          investment.save
        end

        context 'submitting form params' do
          let(:param_key) { form_class.to_s.underscore }

          before do
            allow(form_class).to receive(:new).and_return(form_double)
            allow(form_double).to receive(:is_a?).and_return(false)
            allow(form_double).to receive(:is_a?).with(form_class).and_return(true)
          end

          context 'submitting third party income form' do
            let(:form_class) { ThirdPartyIncomeForm }

            it 'instantiates the appropriate accreditation form object' do
              post :create, offering_id: 42, param_key => form_params

              expect(form_class).to have_received(:new).at_least(1).times.with(
                                        form_params, user, investor_entity)
            end

          end

          context 'submitting third party net worth form' do
            let(:form_class) { ThirdPartyNetWorthForm }

            it 'instantiates the appropriate accreditation form object' do
              post :create, offering_id: 42, param_key => form_params

              expect(form_class).to have_received(:new).at_least(1).times.with(
                                        form_params, user, investor_entity)
            end

          end

          context 'submitting third party entity form' do
            let(:entity_name) { 'John Doe, LLC' }
            let(:form_class) { ThirdPartyEntityForm }
            let(:investor_entity) { InvestorEntity.new name: entity_name }

            before do
              allow(controller).to receive(:investor_entity).and_return(investor_entity)
            end

            it 'instantiates the appropriate accreditation form object' do
              post :create, offering_id: 42, param_key => form_params

              expect(form_class).to have_received(:new).with(form_params, user, investor_entity)
            end

            it 'sends an email' do
              post :create, offering_id: 42, third_party_income_form: form_params

            end
          end

          context 'submitting income doc form' do
            let(:form_class) { IndividualIncomeForm }

            it 'instantiates the appropriate accreditation form object' do
              post :create, offering_id: 42, param_key => form_params

              expect(form_class).to have_received(:new).at_least(1).times.with(
                                        form_params, user, investor_entity)
            end
          end

          context 'submitting income doc joint form' do
            let(:form_class) { JointIncomeForm }

            it 'instantiates the appropriate accreditation form object' do
              post :create, offering_id: 42, param_key => form_params

              expect(form_class).to have_received(:new).at_least(1).times.with(
                                        form_params, user, investor_entity)
            end
          end

          context 'submitting net worth doc form' do
            let(:form_class) { IndividualNetWorthForm }

            it 'instantiates the appropriate accreditation form object' do
              post :create, offering_id: 42, param_key => form_params

              expect(form_class).to have_received(:new).at_least(1).times.with(
                                        form_params, user, investor_entity)
            end
          end

          context 'submitting net worth doc joint form' do
            let(:form_class) { JointNetWorthForm }

            context 'auditing form submissions' do
              let(:accreditation_form_auditor_double) { double(AccreditationFormAuditor, write: true) }

              before do
                allow(controller).to receive(:auditor).and_return(accreditation_form_auditor_double)
              end

              it 'write the information to the audit log' do
                Timecop.freeze do

                  set_request_ip('1.2.3.4')

                  post :create, offering_id: 42, param_key => form_params

                  expect(accreditation_form_auditor_double).to have_received(:write).
                                                                   with(hash_including(form_params), user.id, '1.2.3.4', '/offering/42/invest/accreditation/new', Time.now.utc, nil)
                end
              end
            end

            it 'instantiates the appropriate accreditation form object' do
              post :create, offering_id: 42, param_key => form_params

              expect(form_class).to have_received(:new).at_least(1).times.with(
                                        form_params, user, investor_entity)
            end
          end

          context 'submitting offline entity form' do
            let(:form_class) { OfflineEntityForm }

            it 'instantiates the appropriate accreditation form object' do
              post :create, offering_id: 42, param_key => form_params

              expect(form_class).to have_received(:new).with(
                                        form_params, user, investor_entity)
            end
          end
        end

        context 'on success' do
          before { allow(form_double).to receive(:save).and_return(true) }

          it 'updates the state of the investment' do
            expect(investment).to receive(:verify_accreditation!)
            post :create, offering_id: 42, third_party_income_form: form_params
          end

          it 'updates accreditations' do
            expect_any_instance_of(::Accreditation::StatusUpdater).to receive(:update_investment).with(investment)

            post :create, offering_id: 42, third_party_income_form: form_params
          end

        end

        context 'on failure' do
          before do
            allow(form_double).to receive(:save).and_return(false)
            allow(investor_entity).to receive(:is_a?).and_return(false)
            allow(investor_entity).to receive(:is_a?).with(InvestorEntity::Individual).and_return(true)
          end

          it 're-renders new' do
            post :create, offering_id: 42, third_party_income_form: form_params

            expect(assigns(:uploader)).to be_a(AccreditationDocumentUploader)
            expect(response).to render_template('new')
          end

          it 'does not update accreditations' do
            expect(::Accreditor::SpanCreator).to_not receive(:populate_all)
            expect_any_instance_of(::Accreditation::StatusUpdater).to_not receive(:update!)

            post :create, offering_id: 42, third_party_income_form: form_params
          end

          context 'when entity verification form is invalid' do
            let(:form_class) { ThirdPartyEntityForm }
            before do
              allow(form_double).to receive(:is_a?).and_return(false)
              allow(form_double).to receive(:is_a?).with(ThirdPartyEntityForm).and_return(true)
              allow(investor_entity).to receive(:is_a?).and_return(true)
              allow(investor_entity).to receive(:is_a?).with(InvestorEntity::Individual).and_return(false)
            end

            it 're-renders new_entity' do
              post :create, offering_id: 42, third_party_entity_form: form_params

              expect(assigns(:uploader)).to be_a(AccreditationDocumentUploader)
              expect(response).to render_template('new_entity')
            end
          end
        end

        context "update" do
          let!(:qualification_tp) { Accreditor::ThirdPartyIncomeQualification.create(
            investor_entity_id: investor_entity.id,
            email: "accountant@example.com",
            role: "Certified Public Accountant",
            user_id: investment.user_id
          )}

          let(:form_params) { { role: 'Lawyer', email: 'foo@example.com'} }

          before do
            investment.state = 'accreditation_verified'
            investment.save
            ::Accreditor::SpanCreator.new(investor_entity.id).create_for_entity
            ::Accreditation::StatusUpdater.new.update_investment investment
            allow(ThirdPartyNetWorthForm).to receive(:new).and_call_original
            allow(ThirdPartyIncomeForm).to receive(:new).and_call_original
            allow(OfflineEntityForm).to receive(:new).and_call_original
          end

          it "saves the new accreditation method" do
            patch :update, offering_id: 42, third_party_net_worth_form: form_params
            expect(investment.reload.current_accreditor_qualification.class).to eq(Accreditor::ThirdPartyNetWorthQualification)
          end

          it "regenerates accreditations" do
            expect(investment).to receive(:regenerate_accreditations)
            patch :update, offering_id: 42, third_party_net_worth_form: form_params
          end


          context "before signing docs" do
            it "does not trigger the investor verification even if the method has changed" do
              expect(investment).to_not receive(:trigger_investor_verification)
              patch :update, offering_id: 42, third_party_income_form: {email: "lawyer@example.com",
                                                                        role: "Lawyer"}
            end
          end

          context "after signing docs" do
            before do
              investment.update_attribute(:state, "signed_documents")
              InvestmentEvent::SignDocumentsEvent.create(investment_id: investment.id)
            end

            it "re-triggers investor verification if the method has changed" do
              expect(investment).to receive(:trigger_investor_verification)
              patch :update, offering_id: 42, third_party_net_worth_form: form_params
            end

            it "does not re-trigger the investor verification if the method has not changed" do
              expect(investment).to_not receive(:trigger_investor_verification)
              patch :update, offering_id: 42, third_party_income_form: {email: "accountant@example.com",
                                                                        role: "Lawyer"}
            end

            it "doesn't explode if we use a non-third party qualification" do
              expect(investment).to_not receive(:trigger_investor_verification)
              patch :update, offering_id: 42, offline_entity_form: {request_contact: 1}
            end
          end

        end
      end


    end
  end
end
