require 'rails_helper'

describe Invest::InvestmentDocumentsController do

  let(:investment) do
    Investment.create(amount:            10000.00,
                      id:                43,
                      shares:            27,
                      percent_ownership: BigDecimal.new(26.745, 5),
                      user:              user,
                      offering_id:       42,
                      envelope_email:    'test@example.com',
                      envelope_id:       'envelope_id'
    )
  end
  let(:offering_double) { mock_model(Offering, id: 42,
                                     ended?: false, model_direct_spv?: false, escrow_closed?: false, reg_c_enabled?: false) }
  let(:escrow_provider_double) { mock_model(EscrowProvider, id: 42) }

  let(:investor_investment_summary_double) { double(InvestorInvestmentSummary) }

  let(:envelope_double) { double(DocusignInvestmentAgreementEnvelope, envelope_id: 'envelope_id', investor_signed_at: Time.now) }
  let(:param_double) { double(DocusignInvestorAgreementParameterBuilder, envelope_details: { envelope: 'detail' }, template_id: 'template_id', investor_name: 'Name', investor_email: 'Email') }

  let(:user) { create_user(email: 'user@confirmed.com') }
  let(:unconfirmed_user) { create_unconfirmed_user(email: 'user@unconfirmed.com') }
  let(:auditor_double) { double(Auditor, write: true) }
  let(:deal_document_double) { double(DealDocument, name: 'Direct') }
  let(:spv_deal_document_double) { double(DealDocument, name: 'SPV') }

  context 'An a confirmed user' do
    before do
      sign_in :user, user

      allow(controller).to receive(:investor_investment_summary).and_return(investor_investment_summary_double)
      allow(investment).to receive(:offering).and_return(offering_double)
      allow(Offering).to receive(:find).and_return(offering_double)
      allow(DocusignInvestorAgreementParameterBuilder).to receive(:new).and_return(param_double)
      allow(offering_double).to receive(:escrow_provider).and_return(escrow_provider_double)
      allow(EscrowProvider).to receive(:find).and_return(escrow_provider_double)
    end

    describe 'GET show' do
      context 'for a Direct Offering' do
        before do
          investment.state = 'accreditation_verified'
          allow(Investment).to receive_message_chain(:where, :where, :not, :first_or_initialize).and_return(investment)
          allow(Document).to receive_message_chain(:where, :where, :not).and_return([deal_document_double])
        end

        it 'renders the investment documents show page' do
          get :show, offering_id: 42

          expect(response).to render_template(:show)
        end

        it 'should show the investment documents page' do
          get :show, offering_id: 42

          expect(response.status).to eq(200)
          expect(assigns(:investment)).to eq(investment)
          expect(assigns(:documents)).to eq([deal_document_double])
          document = assigns(:documents).first
          expect(document.name).to eq('Direct')
        end
      end

      context 'for an SPV Offering' do
        let(:offering_double) { mock_model(Offering, id: 42, ended?: false, reg_c_enabled?: false,
                                           model_direct_spv?: true, direct_investment_threshold: 25000, escrow_closed?: false) }
        before do
          investment.state = 'accreditation_verified'
          allow(Investment).to receive_message_chain(:where, :where, :not, :first_or_initialize).and_return(investment)
          allow(Document).to receive_message_chain(:where, :where, :not).and_return([spv_deal_document_double])
        end

        it 'should show the investment documents page' do
          get :show, offering_id: 42

          expect(response.status).to eq(200)
          expect(assigns(:documents)).to eq([spv_deal_document_double])
          document = assigns(:documents).first
          expect(document.name).to eq('SPV')
        end
      end
    end

    describe 'POST accept' do

      before do
        investment.state = 'accreditation_verified'
        allow(investment).to receive(:update_attributes)
        allow(Investment).to receive_message_chain(:where, :where, :not, :first_or_initialize).and_return(investment)
        allow(controller).to receive(:show).and_return(true)
        allow(DocusignInvestmentAgreementEnvelope).to receive(:create).and_return(envelope_double)
        allow(DocusignInvestmentAgreementUrl).to receive(:get_embedded_url).and_return('docusign_url')
      end

      it 'renders the form again is the terms have not been accepted' do
        post :accept, offering_id: 42
        json = JSON.parse(response.body)
        expect(json).to eq({ "base" => [], "reviewed_information" => I18n.t('investment_documents.sections.review.error_message') })
      end

      it "gets the embedded url" do
        post :accept, offering_id: 42, reviewed_information: "on"
        expect(DocusignInvestmentAgreementUrl).to have_received(:get_embedded_url).with(envelope_double.envelope_id, param_double.investor_name, param_double.investor_email, docusign_redirect_offering_invest_investment_documents_url(offering_double.id))
        expect(assigns(:agreement_url)).to eq('docusign_url')
      end

      context 'When the envelope_email is nil and/or differs from the current user email' do
        it 'creates a new envelope' do
          post :accept, offering_id: 42, reviewed_information: "on"
          expect(DocusignInvestmentAgreementEnvelope).to have_received(:create)
        end

        it 'updates the investment envelope id' do
          post :accept, offering_id: 42, reviewed_information: "on"
          expect(investment).to have_received(:update_attributes).with(envelope_id: envelope_double.envelope_id, envelope_email: user.email, cached_envelope_details: param_double.envelope_details)
        end
      end

      context 'When the envelope already exists' do
        context "and the data hasn't changed" do
          before do
            investment.cached_envelope_details = param_double.envelope_details
          end

          it 'does not create a new envelope' do
            post :accept, offering_id: 42, reviewed_information: "on"
            expect(DocusignInvestmentAgreementEnvelope).to_not have_received(:create)
          end
        end

        context "and the data has changed" do
          before do
            investment.cached_envelope_details = { "foo" => "bar" }
          end

          it 'creates a new envelope' do
            post :accept, offering_id: 42, reviewed_information: "on"
            expect(DocusignInvestmentAgreementEnvelope).to have_received(:create)
          end

          it 'updated the cached envelope details' do
            post :accept, offering_id: 42, reviewed_information: "on"
            expect(investment).to have_received(:update_attributes).with(envelope_id: envelope_double.envelope_id, envelope_email: user.email, cached_envelope_details: param_double.envelope_details)
          end

        end
      end
    end

    describe 'GET docusign_redirect' do
      before do
        investment.state = 'accreditation_verified'
        allow(Investment).to receive_message_chain(:where, :where, :not, :first_or_initialize).and_return(investment)
      end

      it 'renders the sign page with embedded docusign' do
        get :docusign_redirect, offering_id: 42

        expect(response).to be_success
      end

      context 'and the documents have been signed' do
        it 'assigns the redirect to url to the accept investment documents path' do
          get :docusign_redirect, offering_id: 42, event: 'signing_complete'

          expect(assigns(:redirect_to_url)).to eq(docusign_handler_offering_invest_investment_documents_path(42))
        end
      end

      context 'and the documents have been declined' do
        it 'assigns the redirect to url to the accept investment documents path' do
          get :docusign_redirect, offering_id: 42, event: 'decline'

          expect(assigns(:redirect_to_url)).to eq(docusign_handler_offering_invest_investment_documents_path(42))
        end
      end

      context 'and the documents were signed already' do
        it 'assigns the redirect to url to the investment dashboard' do
          get :docusign_redirect, offering_id: 42, event: 'viewing_complete'

          expect(assigns(:redirect_to_url)).to eq(docusign_handler_offering_invest_investment_documents_path(42))
        end
      end

      context 'and the documents will be finished later' do
        it 'assigns the redirect to url to the investment dashboard' do
          get :docusign_redirect, offering_id: 42, event: 'other event'

          expect(assigns(:redirect_to_url)).to eq(dashboard_investments_path)
        end
      end
    end

    describe 'POST docusign_handler' do
      before do
        investment.state = 'accreditation_verified'
        allow(DocusignInvestmentAgreementEnvelope).to receive(:new).and_return(envelope_double)
        allow(Investment).to receive_message_chain(:where, :where, :not, :first_or_initialize).and_return(investment)
      end

      context 'when the docs are signed' do
        before do
          allow(envelope_double).to receive(:investor_signed?).and_return(true)
          allow(investment).to receive(:sign_documents!)

          allow(controller).to receive(:auditor).and_return(auditor_double)
        end

        it 'updates the state of the investment' do
          get :docusign_handler, offering_id: 42

          expect(investment).to have_received(:sign_documents!)
        end

        it 'updates the docs signed at time' do
          Timecop.freeze do
            get :docusign_handler, offering_id: 42

            expect(investment.docs_signed_at).to eq(envelope_double.investor_signed_at)
          end
        end

        it 'audits the acceptance of documents' do
          Timecop.freeze do
            set_request_ip('1.2.3.4')

            get :docusign_handler, offering_id: 42

            expect(auditor_double).to have_received(:write).with(
              {
                investment_amount:            '10000.0',
                investment_shares:            '27',
                investment_percent_ownership: '26.745'
              }, user.id, '1.2.3.4', '/offering/42/invest/investment_documents/docusign_handler', Time.current.utc, nil
            )
          end
        end

        context 'after the user has signed documents' do
          before do
            investment.state = 'signed_documents'
          end

          it 'this action is off limits' do
            get :docusign_handler, offering_id: offering_double.id

            expect(response).to redirect_to(offering_invest_fund_path(offering_double))
          end
        end
      end

      context 'when the docs are declined' do
        before do
          allow(envelope_double).to receive(:investor_signed?).and_return(false)
          allow(envelope_double).to receive(:investor_declined?).and_return(true)
          allow(investment).to receive(:decline_documents!)
        end

        it 'should change the investment state' do
          get :docusign_handler, offering_id: 42

          expect(investment).to have_received(:decline_documents!)
        end

        it 'should show the error page' do
          get :docusign_handler, offering_id: 42

          expect(response).to redirect_to(offering_invest_investment_documents_path(offering_double))
        end
      end
    end
  end

end

