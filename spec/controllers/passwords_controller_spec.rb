require 'rails_helper'

describe PasswordsController do
  before { @request.env['devise.mapping'] = Devise.mappings[:user] }

  let(:partially_registered_user) { create_temporary_user(email: 'user@partial.com') }
  let(:unconfirmed_user) { create_unconfirmed_user(email: 'user@unconfirmed.com') }
  let(:confirmed_user) { create_user(email: 'user@confirmed.com') }

  describe 'POST #create' do

    context 'as an unregistered user' do
      it 'should show new template' do
        post :create, user: { email: 'user@unregistered.com' }

        expect(response).to render_template(:new)
      end
    end

    context 'as a partially registered user (Use Case: #11)' do
      it 'should ask user to complete their account' do
        post :create, user: { email: partially_registered_user.email }

        expect(response).to redirect_to(new_user_confirmation_path(user: { email: partially_registered_user.email }))
      end
    end

    context 'as an unconfirmed user' do
      it 'should ask user to confirm their email' do
        post :create, user: { email: unconfirmed_user.email }

        expect(response).to redirect_to(new_user_session_path)
      end
    end

    context 'as a confirmed user' do
      it 'should send an email' do
        post :create, user: { email: confirmed_user.email }

        expect(response).to redirect_to(new_user_session_path)
      end
    end
  end
end
