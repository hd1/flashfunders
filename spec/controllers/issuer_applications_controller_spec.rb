require 'rails_helper'

describe IssuerApplicationsController do
  context 'With a signed in user' do
    let(:user) { create_user }

    before do
      sign_in user
    end

    describe '#new' do
      before { allow(controller).to receive(:store_location_for) }

      it 'renders the waiting list page' do
        get :new

        expect(response).to be_success
      end

      it 'sets the issuer application url in the session' do
        get :new

        expect(controller).to have_received(:store_location_for).with(:user, issuer_applications_path)
      end
    end

    describe '#create' do
      let(:issuer_application_form) { double(IssuerApplicationForm, save: true, valid?: true, email: user.email) }
      let(:issuer_application) { double(IssuerApplication, id: 34) }

      before do
        allow(IssuerApplicationForm).to receive(:new).and_return(issuer_application_form)
        allow(IssuerApplication).to receive_message_chain(:where, :last).and_return(issuer_application)
        allow(IssuerApplication).to receive_message_chain(:where, :exists?).and_return(false)
        allow(issuer_application_form).to receive(:errors).and_return([])
        allow(issuer_application_form).to receive(:user).and_return(user)
      end

      context 'the issuer application is valid' do
        it 'creates an IssuerApplication record' do
          post :create, issuer_application_form: { stubbed_params: '' }

          expect(IssuerApplicationForm).to have_received(:new).with(anything, user)
        end

        it 'redirects to success' do
          post :create, issuer_application_form: { stubbed_params: '' }

          expect(response).to redirect_to(action: :success)
        end
      end

      context 'the issuer application is invalid' do
        before { allow(issuer_application_form).to receive(:save).and_return(false) }

        it 'renders new' do
          post :create, issuer_application_form: { stubbed_params: true }

          expect(response).to render_template(:new)
        end
      end
    end
  end

  context 'With an unregistered issuer (Use Case: #2)' do
    let(:user) { User.new(id: 123, email: 'joe@entrepeneur.com') }

    describe '#create' do
      let(:issuer_application_form) { double(IssuerApplicationForm, save: true, valid?: true, email: user.email) }
      let(:issuer_application) { double(IssuerApplication, id: 34) }

      before do
        allow(IssuerApplicationForm).to receive(:new).and_return(issuer_application_form)
        allow(IssuerApplication).to receive_message_chain(:where, :last).and_return(issuer_application)
        allow(IssuerApplication).to receive_message_chain(:where, :exists?).and_return(false)
        allow(issuer_application_form).to receive(:errors).and_return([])
        allow(issuer_application_form).to receive(:user).and_return(user)
      end

      context 'the issuer application is valid' do
        it 'sends a welcome email to the new issuer' do
          expect {
            post :create, issuer_application_form: { stubbed_params: '' }
          }.to change(Devise::Async::Backend::Sidekiq.jobs, :size).by(1)
        end

        it 'redirects to success' do
          post :create, issuer_application_form: { stubbed_params: '' }

          expect(response).to redirect_to(action: :success)
        end
      end
    end
  end

  context 'With a partially registered issuer (Use Case #6)' do
    let(:user) { create_temporary_user(id: 123, email: 'joe+partial@entrepeneur.com') }

    describe '#create' do
      let(:issuer_application_form) { double(IssuerApplicationForm, save: true, valid?: true, email: user.email) }
      let(:issuer_application) { double(IssuerApplication, id: 34) }

      before do
        allow(IssuerApplicationForm).to receive(:new).and_return(issuer_application_form)
        allow(IssuerApplication).to receive_message_chain(:where, :last).and_return(issuer_application)
        allow(IssuerApplication).to receive_message_chain(:where, :exists?).and_return(false)
        allow(issuer_application_form).to receive(:errors).and_return([])
        allow(issuer_application_form).to receive(:user).and_return(user)
      end

      context 'the issuer application is valid' do
        it 'does not send a welcome email' do
          expect {
            post :create, issuer_application_form: { stubbed_params: '' }
          }.to_not change(Devise::Async::Backend::Sidekiq.jobs, :size)
        end

        it 'redirects to confirmation page' do
          post :create, issuer_application_form: { stubbed_params: '' }

          expect(response).to redirect_to(new_user_confirmation_path(email: user.email))
        end
      end
    end
  end

  describe '#recreate' do
    context 'the issuer has issuer application session' do
      before do
        session[:recreate_params] = { issuer_application_form: '' }
        allow(controller).to receive(:create)
        allow(controller).to receive(:render).and_return(:nothing)
      end

      it "recreates issuer application" do
        get :recreate

        expect(controller.params.keys).to include("issuer_application_form")
        expect(controller).to have_received(:create)
      end
    end

    context 'the issuer has no issuer application session' do
      it "redirects to new issuer application" do
        get :recreate

        expect(response).to redirect_to(action: :new)
      end
    end
  end

end