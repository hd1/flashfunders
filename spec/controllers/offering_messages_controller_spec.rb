require 'rails_helper'

describe OfferingMessagesController do

  describe 'with a signed_in user' do
    let(:user) { create_user }

    before { sign_in :user, user }

    describe 'POST #create' do
      let(:offering) { double(Offering, id: 123) }
      let(:auditor_double) { double(Auditor, write: nil) }
      let(:offering_message) { double(OfferingMessage) }

      before do
        allow(Offering).to receive_message_chain(:includes, :find).and_return(offering)
        allow(controller).to receive(:auditor).and_return(auditor_double)
      end

      it 'returns a response' do
        xhr :post, :create, { offering_id: 123 }

        expect(response).to be_successful
        expect(assigns(:offering)).to eql(offering)
      end

      it 'marks the user as a follower of the offering' do
        current_time = Time.now.utc
        allow(Time).to receive(:now).and_return(current_time)

        xhr :post, :create, { offering_id: 123 }

        expect(OfferingFollower.last.offering_id).to eq(123)
        expect(OfferingFollower.last.user_id).to eq(user.id)
        expect(OfferingFollower.last.mode).to eq("follower")
      end

      it 'sends a welcome follower email' do
        expect {
          xhr :post, :create, { offering_id: 123 }
        }.to change(Sidekiq::Extensions::DelayedMailer.jobs, :size).by(1)
      end
    end
  end

end
