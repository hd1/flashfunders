require 'rails_helper'

describe OfferingsController do
  let(:user) { create_user(email: 'owner@example.com') }
  let(:second_user) { create_user(email: 'issuer@example.com') }

  before do
    sign_in :user, user
  end

  context 'The campaign does not exist' do
    it 'renders the 404 page' do
      expect { get :show, id: 404 }.to raise_error(ActiveRecord::RecordNotFound)
    end
  end

  describe 'GET show' do
    let(:company_double) { double(Company, name: 'Foos Furry Farm', id: 301) }
    let(:security) { Securities::FspStock.new(minimum_total_investment: 0, minimum_total_investment_display: '$100K', is_spv: false)}
    let(:offering_attrs) { {id: 10, ended?: false, company: company_double,
                            securities: [security],
                            reg_d_direct_security: security,
                            other_security: nil,
                            uuid: '57c16f3c-8cac-4dde-8e67-2d94e0f91162',
                            visibility_restricted?: false, visibility_preview?: false, visibility_public?: false, vanity_path: 'test-vanity-path',
                            facebook_pixel_id: nil, google_tracking_id: nil, google_adwords_conversion_id: nil, google_adwords_conversion_label: nil } }
    let(:offering_double) { double(Offering, offering_attrs) }
    let(:team_member_double) { double(Stakeholder, full_name: 'Frank Oz') }
    let(:investor_advisor_double) { double(Stakeholder, full_name: 'Jim Hensen') }
    let(:campaign_document_double) { double(CampaignDocument, name: 'The Biz') }
    let(:deal_document_double) { double(DealDocument, name: 'Wheeling & Dealing') }
    let(:spv_deal_document_double) { double(DealDocument, name: 'Hybrid Power') }

    before do
      allow(Offering).to receive_message_chain(:includes, :find).and_return(offering_double)
      allow(offering_double).to receive(:team_members).and_return([team_member_double])
      allow(offering_double).to receive(:investors).and_return([investor_advisor_double])
      allow(offering_double).to receive(:company).and_return(company_double)
      allow(OfferingInvestments).to receive(:new).and_return(double(committed_security: Investment.none))
    end

    context 'when the campaign has restricted visibility' do
      context 'and the user owns the campaign' do
        let(:offering_double) { double(Offering, offering_attrs.merge(id: 1, tagline: 'Come for the petting, stay for the eating.',
                                                                      user_id: user.id, issuer_user_id: second_user.id,
                                                                      visibility_restricted?: true)) }

        it 'allows them to see it' do
          get :show, id: 1

          expect(response).to be_success
        end
      end

      context 'and the user does not own the campaign' do
        let(:offering_double) { double(Offering, offering_attrs.merge(id: 3, tagline: 'Come for the petting, stay for the eating.',
                                                                      user_id: second_user.id, issuer_user_id: second_user.id,
                                                                      visibility_restricted?: true)) }

        it 'renders the 404 page' do
          get :show, id: 1

          expect(response.status).to eq(404)
        end
      end

      context 'and the user does not own the campaign but the issuer_user does own the campaign' do
        let(:offering_double) { double(Offering, offering_attrs.merge(id: 5, tagline: 'Come for the petting, stay for the eating.',
                                                                      user_id: second_user.id, issuer_user_id: user.id,
                                                                      visibility_restricted?: true)) }

        it 'allows them to see it' do
          get :show, id: 1

          expect(response).to be_success
        end
      end

    end

    context 'when the campaign has preview visibility' do
      context 'and the user is not signed in' do
        let(:offering_double) { double(Offering, offering_attrs.merge(id: 5, tagline: 'Come for the petting, stay for the eating.',
                                                                      user_id: user.id, issuer_user_id: second_user.id,
                                                                      visibility_preview?: true,
                                                                      uuid: 'XXXX')) }

        before do
          sign_out :user
        end

        it 'allows them to see it' do
          get :show, id: 1, preview: 'XXXX'

          expect(response).to be_success
        end
      end
    end

    context 'when the campaign has public visibility' do
      let(:offering_double) { double(Offering, offering_attrs.merge(id: 1, visibility_public?: true, tagline: 'Come for the petting, stay for the eating.', tagline_browse: 'Going up Sir...Your floor Sir.')) }

      it 'instantiates a campaign presenter from the correct campaign and its associated company, offering, and escrow account' do
        get :show, id: 1

        offering_presenter = assigns(:offering_presenter)
        expect(offering_presenter.company_name).to eq('Foos Furry Farm')
        expect(offering_presenter.tagline).to eq('Come for the petting, stay for the eating.')
        expect(offering_presenter.tagline_browse).to eq('Going up Sir...Your floor Sir.')

        expect(offering_presenter.minimum_total_investment_display).to eq('$100K')
        expect(offering_presenter.formatted_accredited_balance).to eq('$0')
        expect(offering_presenter.number_of_investors).to eq(0)

      end

      it 'instantiates a collection of team members for the campaign' do
        get :show, id: 1

        expect(assigns(:team_information)).to be_a_kind_of(Enumerable)

        team_information = assigns(:team_information).first
        expect(team_information.full_name).to eq('Frank Oz')
      end

      it 'instantiates a collection of investors and advisors for the campaign' do
        get :show, id: 1

        expect(assigns(:investor_advisor_information)).to be_a_kind_of(Enumerable)

        investor_advisor_information = assigns(:investor_advisor_information).first
        expect(investor_advisor_information.full_name).to eq('Jim Hensen')
      end
    end

    context 'when the campaign has public visibility' do
      let(:offering_double) { double(Offering, offering_attrs.merge(id: 1, visibility_public?: true, tagline: 'Come for the petting, stay for the eating.', elevator_pitch: 'Going up Sir...Your floor Sir.')) }
      before do
        allow(Document).to receive_message_chain(:where, :where, :not).and_return([campaign_document_double])
      end
      it 'instantiates a collections of campaign documents' do
        get :show, id: 1

        campaign_document = assigns(:offering_documents).first
        expect(campaign_document.name).to eq('The Biz')
      end
    end

    context 'when the campaign has public visibility' do
      let(:offering_double) { double(Offering, offering_attrs.merge(id: 1, visibility_public?: true, tagline: 'Come for the petting, stay for the eating.', elevator_pitch: 'Going up Sir...Your floor Sir.')) }

      it 'instantiates an offering follower' do
        get :show, id: 42

        expect(assigns(:follower)).to be_a_kind_of(OfferingFollower)
      end
    end
  end

  describe '#vanity_show' do
    context 'with a valid vanity path' do
      let(:mock_offering) { mock_model(Offering, visibility_public?: true, vanity_path: 'im-so-vain') }
      let(:company_double) { double(Company, name: 'Foos Furry Farm', id: 301) }

      before do
        allow(Offering).to receive_message_chain(:includes, :find_by!).and_return(mock_offering)
        allow(Offering).to receive_message_chain(:includes, :find).and_return(mock_offering)
        allow(mock_offering).to receive(:company).and_return(company_double)
      end
      it 'shows the appropriate offering' do
        allow(controller).to receive(:show)
        get :vanity_show, { path: 'im-so-vain' }
        expect(assigns[:offering]).to eq(mock_offering)
        expect(controller).to have_received(:show)
      end
      it 'normalizes the vanity_path' do
        allow(controller).to receive(:show)
        get :vanity_show, { path: 'IM-SO-vain' }
        expect(Offering).to have_received(:includes)
      end
      it "it should have the appropriate context to call '#show'" do
        # make sure #show doesn't double render or something that would break vanity_show
        allow(controller).to receive(:offering).and_return(mock_offering.as_null_object)
        get :vanity_show, { path: 'im-so-vain' }
      end
    end
    context 'with a bogus vanity path' do
      it 'raises NotFound' do
        expect {
          get :vanity_show, { path: 'bogus-path' }
        }.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
    context 'with a valid vanity path to a visibility-restricted offering' do
      let(:mock_offering) { mock_model(Offering, visibility_restricted?: true, visibility_preview?: false, visibility_public?: false) }
      let(:company_double) { double(Company, name: 'Foos Furry Farm', id: 301) }
      before do
        allow(Offering).to receive_message_chain(:includes, :find).and_return(mock_offering)
        allow(Offering).to receive_message_chain(:includes, :find_by!).and_return(mock_offering)
        allow(mock_offering).to receive(:company).and_return(company_double)
      end
      it 'shows record not found' do
        allow(controller).to receive(:show)
        get :vanity_show, { path: 'im-so-vain' }
        expect(response.status).to eq(404)
      end
    end
    context 'with a valid vanity path to a visibility-preview offering' do
      let(:mock_offering) { mock_model(Offering, visibility_restricted?: false, visibility_preview?: true, visibility_public?: false, uuid: 'XXXX', vanity_path: 'im-so-vain') }
      let(:company_double) { double(Company, name: 'Foos Furry Farm', id: 301) }

      before do
        allow(Offering).to receive_message_chain(:includes, :find_by!).and_return(mock_offering)
        allow(Offering).to receive_message_chain(:includes, :find).and_return(mock_offering)
        allow(mock_offering).to receive(:company).and_return(company_double)
      end
      it 'shows the offering' do
        allow(controller).to receive(:show)
        get :vanity_show, { path: 'im-so-vain' }
        expect(controller).to have_received(:show)
      end
    end
  end
end
