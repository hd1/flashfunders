require 'rails_helper'

describe ConfirmationsController do
  before { @request.env['devise.mapping'] = Devise.mappings[:user] }

  describe '#new' do
    let (:user) { create_unconfirmed_user(email: 'joe_unconfirmed@example.com') }

    context 'when redirect_after_signup in session is true' do
      before do
        session[:redirect_after_signup] = true
        controller.store_location_for(:user, privacy_path)
      end

      it 'the confirmation email should redirect to the url' do
        get :new, user: { email: user.email, new: true }

        expect(User.find_by_email(user.email).context['path']).to eq(privacy_path)
      end
    end

    context 'when return_to is set in the query string' do
      it 'the confirmation email should return the user to the stored location' do
        get :new, { user: { email: user.email, new: true }, return_to: privacy_path }

        expect(User.find_by_email(user.email).context['path']).to eq(privacy_path)
      end
    end

  end

  describe '#create' do
    before do
      Devise::Async.enabled = false
    end

    after do
      Devise::Async.enabled = true
    end

    describe 'as an unknown user' do
      it 'should display an error message' do
        post :create, user: { email: 'anonymous@unknown.com' }

        expect(DeviseMailer).to_not receive(:confirmation_instructions)
        expect(response).to be_success
        expect(response).to render_template('devise/confirmations/new')
      end
    end

    describe 'with a defined user' do
      before do
        allow(resource).to receive(:send_on_create_confirmation_instructions).and_return(nil)
      end

      describe 'as an unconfirmed fully registered user' do
        let(:resource) { create_unconfirmed_user(email:                 'fully_registered_user@example.com',
                                                 registration_name:     'Registered User',
                                                 password:              'password',
                                                 password_confirmation: 'password',
                                                 us_citizen:            false,
                                                 country:               'UK',
                                                 confirmation_sent_at:  1.day.ago) }
        it 'should send a reconfirm email' do
          expect(DeviseMailer).to receive(:confirmation_instructions).and_call_original

          post :create, user: { email: resource.email }

          expect(response).to redirect_to(new_user_session_path)
          expect(flash[:notice]).to eq(I18n.t('devise.confirmations.send_instructions'))
        end

      end

      describe 'as an unconfirmed partially registered user' do
        let(:resource) { create_temporary_user(email:                'partially_registered_user@example.com',
                                               registration_name:    'Registered User',
                                               confirmation_sent_at: 1.day.ago) }
        it 'should send a reconfirm email' do
          expect(DeviseMailer).to receive(:confirmation_instructions).and_call_original

          post :create, user: { email: resource.email }

          expect(response).to redirect_to(new_user_session_path)
          expect(flash[:notice]).to eq(I18n.t('devise.confirmations.send_instructions'))
        end
      end

      describe 'as a confirmed user' do
        let(:resource) { create_temporary_user(email:                'confirmed_user@example.com',
                                               registration_name:    'Registered User',
                                               confirmation_sent_at: 1.day.ago) }

        it 'should redirect to sign in page' do
          resource.password = 'password'
          resource.confirm!
          post :create, user: { email: resource.email }

          expect(DeviseMailer).to_not receive(:confirmation_instructions)
          expect(response).to redirect_to(new_user_session_path)
          expect(flash[:notice]).to eq(I18n.t('devise.confirmations.user.already_confirmed'))
        end
      end
    end
  end

  # TODO: Restore these tests when we reenable email confirmation.
  # describe '#show' do
  #   describe 'with a valid confirmation token' do
  #
  #     let(:confirmation_token) { 'does not matter' }
  #
  #     before do
  #       allow(Devise.token_generator).to receive(:digest).with(instance_of(ConfirmationsController), :confirmation_token, confirmation_token).and_return(resource.confirmation_token)
  #     end
  #
  #     describe 'as an already confirmed fully registered user' do
  #       let(:resource) { create_user(email: 'new_confirmed_user@example.com',
  #                                    registration_name: 'Confirmed User',
  #                                    password: 'password',
  #                                    password_confirmation: 'password',
  #                                    confirmation_sent_at: 1.day.ago) }
  #
  #       it 'I should be redirected to the sign in page' do
  #         get :show, confirmation_token: confirmation_token
  #
  #         expect(response).to redirect_to(new_user_session_path(user: { email: resource.email }))
  #       end
  #     end
  #
  #     describe 'as a fully registered user' do
  #       let(:resource) { create_unconfirmed_user(email:                 'new_user@example.com',
  #                                                registration_name:     'Registered User',
  #                                                password:              'password',
  #                                                password_confirmation: 'password',
  #                                                confirmation_sent_at:  1.day.ago) }
  #
  #       it 'I should be confirmed and taken to the sign in page' do
  #         get :show, confirmation_token: confirmation_token
  #
  #         expect(response).to redirect_to(new_user_session_path)
  #         resource.reload
  #         expect(resource.confirmation_token).to be(nil)
  #         expect(resource.confirmed_at).to_not be(nil)
  #       end
  #     end
  #
  #     describe 'as a partially registered user' do
  #       let(:resource) { create_temporary_user(email:                'new_user@example.com',
  #                                              registration_name:    'Partially Registered User',
  #                                              confirmation_sent_at: 1.day.ago) }
  #
  #       render_views
  #
  #       it 'I should see the confirmation page' do
  #
  #         get :show, confirmation_token: confirmation_token
  #
  #         expect(response).to be_success
  #         expect(response).to render_template('devise/confirmations/show')
  #         page = Capybara::Node::Simple.new(response.body)
  #         expect(page.find_by_id('user_confirmation_token').value).to eq(confirmation_token)
  #       end
  #     end
  #
  #   end
  #
  #   describe 'with an expired confirmation token' do
  #
  #     before do
  #       allow(Devise.token_generator).to receive(:digest).and_return(resource.confirmation_token)
  #     end
  #
  #     describe 'as a fully registered user' do
  #       let(:resource) { create_unconfirmed_user(email:                 'new_user@example.com',
  #                                                registration_name:     'Registered User',
  #                                                password:              'password',
  #                                                password_confirmation: 'password',
  #                                                us_citizen:            true,
  #                                                country:               'US') }
  #
  #       it 'I should see the resend confirmation page' do
  #         resource.confirmation_sent_at = 1.year.ago
  #         resource.save!
  #
  #         get :show, confirmation_token: 'does not matter'
  #
  #         expect(response).to be_success
  #         expect(response).to render_template('devise/confirmations/new')
  #       end
  #     end
  #
  #     describe 'as a partially registered user' do
  #       let(:resource) { create_temporary_user(email:             'new_user@example.com',
  #                                              registration_name: 'Partially Registered User') }
  #
  #       it 'I should see the resend confirmation page' do
  #         resource.confirmation_sent_at = 1.year.ago
  #         resource.save!
  #
  #         get :show, confirmation_token: 'does not matter'
  #
  #         expect(response).to be_success
  #         expect(response).to render_template('devise/confirmations/new')
  #       end
  #     end
  #
  #   end
  #
  #   describe 'with an invalid confirmation token' do
  #     let(:resource) { create_user(email:                 'new_user@example.com',
  #                                  registration_name:     'Registered User',
  #                                  password:              'password',
  #                                  password_confirmation: 'password') }
  #
  #     it 'I should be able to resend the confirmation email' do
  #       get :show, confirmation_token: 'invalid token'
  #
  #       expect(response).to be_success
  #       expect(response).to render_template('devise/confirmations/new')
  #     end
  #   end
  #
  #   describe 'with a valid uuid' do
  #
  #     describe 'as a fully registered user' do
  #       let(:resource) { create_unconfirmed_user(email:                 'new_user@example.com',
  #                                                registration_name:     'Registered User',
  #                                                password:              'password',
  #                                                password_confirmation: 'password',
  #                                                confirmation_sent_at:  1.day.ago) }
  #
  #       it 'I should be confirmed and taken to the sign in page' do
  #         get :show, uuid: resource.uuid
  #
  #         expect(response).to redirect_to(new_user_confirmation_path(token_mismatch: true, email: resource.email))
  #         expect(resource.confirmed_at).to be(nil)
  #       end
  #     end
  #
  #     describe 'as a partially registered user' do
  #       let(:resource) { create_temporary_user(email:                'new_user@example.com',
  #                                              registration_name:    'Partially Registered User',
  #                                              confirmation_sent_at: 1.day.ago) }
  #
  #       render_views
  #
  #       it 'I should see the confirmation page' do
  #         get :show, uuid: resource.uuid
  #
  #         expect(response).to redirect_to(new_user_confirmation_path(token_mismatch: true, email: resource.email))
  #         expect(resource.confirmed_at).to be(nil)
  #       end
  #     end
  #
  #   end
  #
  #   describe 'with an invalid uuid' do
  #     let(:resource) { create_user(email:                 'new_user@example.com',
  #                                  registration_name:     'Registered User',
  #                                  password:              'password',
  #                                  password_confirmation: 'password') }
  #
  #     it 'I should be able to resend the confirmation email' do
  #       get :show, uuid: 1234
  #
  #       expect(response).to be_success
  #       expect(response).to render_template('devise/confirmations/new')
  #     end
  #   end
  # end

  describe '#confirm' do
    let(:resource) { create_temporary_user(email:             'new_user@example.com',
                                           registration_name: 'Partially Registered User') }

    describe 'with a confirmation token' do
      before do
        allow(Devise.token_generator).to receive(:digest).and_return(resource.confirmation_token)
      end

      describe 'with matching valid password and confirmation fields' do
        it 'I should be confirmed and logged in' do
          put :confirm, user: { confirmation_token: 'does not matter', password: 'valid_password', password_confirmation: 'valid_password', us_citizen: false, country: 'GB' }

          expect(response).to redirect_to(get_started_path)
          resource.reload
          expect(resource.confirmation_token).to be(nil)
          expect(resource.confirmed_at).to_not be(nil)
        end
      end

      describe 'with mismatched password and confirmation fields' do
        it 'I should see an error message' do
          put :confirm, confirmation_token: 'does not matter', password: 'password_one', password_confirmation: 'password_two'

          expect(response).to be_success
          expect(response).to render_template('devise/confirmations/show')
        end
      end

      describe 'with an invalid password' do
        it 'I should see an error message' do
          put :confirm, confirmation_token: 'does not matter', password: 'short', password_confirmation: 'short'

          expect(response).to be_success
          expect(response).to render_template('devise/confirmations/show')
        end
      end

      describe 'with an invalid confirmation token' do
        it 'I should be taken to the resend the confirmation page' do
          allow(Devise.token_generator).to receive(:digest).and_call_original
          put :confirm, confirmation_token: 'does matter', password: 'short', password_confirmation: 'short'

          expect(response).to be_success
          expect(response).to render_template('devise/confirmations/new')
        end
      end
    end

    describe 'with a uuid' do
      describe 'with matching valid password and confirmation fields' do
        it 'I should be confirmed and logged in' do
          put :confirm, user: { uuid: resource.uuid, password: 'valid_password', password_confirmation: 'valid_password', us_citizen: true, country: 'AS' }

          expect(response).to redirect_to(get_started_path)
          resource.reload
          expect(resource.confirmation_token).to be(nil)
          expect(resource.confirmed_at).to_not be(nil)
        end
      end

      describe 'with mismatched password and confirmation fields' do
        it 'I should see an error message' do
          put :confirm, uuid: resource.uuid, password: 'password_one', password_confirmation: 'password_two'

          expect(response).to be_success
          expect(response).to render_template('devise/confirmations/show')
        end
      end

      describe 'with an invalid password' do
        it 'I should see an error message' do
          put :confirm, uuid: resource.uuid, password: 'short', password_confirmation: 'short'

          expect(response).to be_success
          expect(response).to render_template('devise/confirmations/show')
        end
      end
    end
  end
end
