require 'rails_helper'

describe ImpersonationsController do

  let(:impersonator) { true }
  let(:user) { create_user(impersonator: impersonator) }
  let(:impersonated_user) { create_user(email: 'jdoe@example.com') }
  let!(:offering) { FactoryGirl.create(:offering, issuer_user: user) }

  describe 'GET create' do
    before do
      sign_in(:user, user)

      allow(controller).to receive(:sign_in)
      allow(controller).to receive(:audit_form!)
    end

    it 'sets the original user id in the session' do
      get :create, user_id: impersonated_user.id
      expect(session[:impersonator_id]).to eql(user.id)
    end

    it 'signs in as the impersonated user' do
      get :create, user_id: impersonated_user.id
      expect(controller).to have_received(:sign_in).with(impersonated_user)
    end

    it 'audits the impersonation' do
      get :create, user_id: impersonated_user.id
      expect(controller).to have_received(:audit_form!)
    end

    it 'redirects to the root path' do
      get :create, user_id: impersonated_user.id
      expect(response).to redirect_to(root_path)
    end

    it '_startup cookie gets set and user is an issuer' do
      get :create, user_id: user
      expect(cookies['_startup']).to eql(offering.id)
    end

    it '_startup cookies gets set and user is not an issuer' do
      offering.update_attributes(issuer_user_id: nil)
      get :create, user_id: impersonated_user.id
      expect(cookies['_startup']).to eql(nil)
    end

    context 'when user is not allowed to impersonate' do
      let(:impersonator) { false }

      it 'does not sign in as the impersonated user' do
        get :create, user_id: impersonated_user.id
        expect(controller).to_not have_received(:sign_in)
      end

      it 'redirects to the root path' do
        get :create, user_id: impersonated_user.id
        expect(response).to redirect_to(root_path)
      end
    end

    context 'when user is already impersonating' do
      let(:impersonated_user_2) { create_user(email: 'test@example.com') }

      before do
        sign_in :user, impersonated_user
        session[:impersonator_id] = user.id
        session[:impersonated_id] = impersonated_user.id
      end

      it 'signs in as the new impersonated user' do
        get :create, user_id: impersonated_user_2.id
        expect(controller).to have_received(:sign_in).with(impersonated_user_2)
      end

      it 'does not overwrite original user id in the session' do
        get :create, user_id: impersonated_user.id
        expect(session[:impersonator_id]).to eql(user.id)
        expect(session[:impersonated_id]).to eql(impersonated_user.id)
      end
    end
  end

  describe 'DELETE destroy' do
    before do
      sign_in :user, impersonated_user
      session[:impersonator_id] = user.id
      session[:impersonated_id] = impersonated_user.id

      allow(controller).to receive(:sign_in)
      allow(controller).to receive(:audit_form!)
    end

    it 'signs in as the original user' do
      delete :destroy
      expect(controller).to have_received(:sign_in).with(user)
    end

    it 'removes the original user ID from session' do
      delete :destroy
      expect(session[:impersonator_id]).to be_nil
      expect(session[:impersonated_id]).to be_nil
    end

    it 'audits the impersonation' do
      get :create, user_id: impersonated_user.id
      expect(controller).to have_received(:audit_form!)
    end

    it 'redirects to the root path' do
      delete :destroy
      expect(response).to redirect_to(root_path)
    end
  end

end

