require 'rails_helper'

module InvestmentFlow
  describe InvestmentsController do

    context '#new' do
      let(:user) { create_user }
      let(:offering) { FactoryGirl.create(:offering) }

      context 'For a Reg C Enabled offering' do
        before do
          FactoryGirl.create(:cf_stock, offering: offering)
        end

        it 'should require user to sign up' do
          get :new, offering_id: offering.id

          expect(response).to redirect_to(new_user_registration_path)
        end

        context 'with a signed in user' do
          before do
            sign_in(:user, user)
          end

          it 'should render new' do
            get :new, offering_id: offering.id

            expect(response).to be_success
            expect(response).to render_template(:new)
          end
        end
      end

      context 'For a non Reg C Enabled offering' do
        it 'should redirect to old flow' do
          get :new, offering_id: offering.id

          expect(response).to redirect_to(new_offering_investment_path)
        end
      end
    end

    context '#edit' do

    end

    context '#create' do

    end

    context '#audit_params' do

    end

  end
end
