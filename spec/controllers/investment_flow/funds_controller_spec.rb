require 'rails_helper'

module InvestmentFlow
  describe Invest::FundsController, :js do

    let(:investor_fund_form_auditor_double) { double(InvestorFundFormAuditor, write: true) }
    let(:escrow_provider) { FactoryGirl.create(:escrow_provider_fundamerica) }
    let(:security) { FactoryGirl.create(:security) }
    let!(:offering) { FactoryGirl.create(:offering, securities: [security]) }

    let(:user) { create_user(
      uuid: 'c87b221b-7c3b-4fa7-b805-8e5ff3d8e856',
      email: 'foo@example.com',
      current_sign_in_ip: '0.0.0.1')
    }
    let!(:investor_entity) { FactoryGirl.create(:investor_entity_individual, :with_pfii, user_id: user.id) }
    let!(:investment) { Investment.create(
      id: 123,
      security: security,
      offering_id: offering.id,
      amount: 12000,
      uuid: '44a61b3e-857b-45f5-8e38-c18aa29973cd',
      state: 'signed_documents',
      investor_entity_id: investor_entity.id,
      user_id: user.id
    )
    }
    let(:params) { {
      bank_account_number: '12345',
      bank_account_holder: 'Testy guy',
      bank_account_type: 'CHECKING',
      bank_account_routing: '111111111',
      passport_image: "some_image.png",
      funding_method: "wire" }
    }

    let!(:bank_account) { Bank::InvestorAccount.create(account_holder: params[:bank_account_holder], account_number: params[:bank_account_number], routing_number: params[:bank_account_routing], account_type: params[:bank_account_type], user_id: user.id) }

    before do
      allow(controller).to receive(:auditor).and_return(investor_fund_form_auditor_double)
      allow(controller).to receive(:send_payment_instructions_email).and_return(true)
      sign_in :user, user
    end

    context "creating" do
      context 'when the form is valid' do
        it 'updates the state of the investment and redirects to the complete page' do
          expect(controller).to receive(:send_payment_instructions_email).and_return(true)
          post :create, investor_fund_form: params, offering_id: offering.id
          investment.reload
          expect(investment.state).to eq("funding_method_selected")
          expect(response).to redirect_to(complete_offering_invest_fund_path(offering_id: offering.id))
        end

        it 'write the information to the audit log' do
          allow_any_instance_of(Time).to receive(:utc).and_return('footime')
          set_request_ip('1.2.3.4')

          post :create, investor_fund_form: params, offering_id: offering.id

          expect(investor_fund_form_auditor_double).to have_received(:write).
            with(a_hash_including(params.stringify_keys), user.id, '1.2.3.4', request.path, 'footime', nil)
        end
      end

      context 'when the form is invalid' do
        it 'renders the provide account page' do
          post :create, investor_fund_form: { foo: 'bar' }, offering_id: offering.id

          expect(response).to render_template('new')
        end

        it 'does not audit the form' do
          post :create, investor_fund_form: { foo: 'bar' }, offering_id: offering.id
          expect(investor_fund_form_auditor_double).to_not have_received(:write)
        end

        it 'does not send an email' do
          post :create, investor_fund_form: { foo: 'bar' }, offering_id: offering.id
          expect(controller).to_not receive(:send_payment_instructions_email)
        end
      end
    end

    context "updating" do
      before do
        investment.select_funding_method!
        investment.update_attributes(funding_method: 'check', investor_account_id: bank_account.id)
        params[:bank_account_number] = "**123"
      end
      context 'when the form is valid' do
        it 'does not update the state of the investment and redirects to the dashboard' do
          patch :update, investor_fund_form: params, offering_id: offering.id
          investment.reload
          expect(investment.state).to eq("funding_method_selected")
          expect(response).to redirect_to(dashboard_investments_path)
        end

        it 'sends an email if the funding method has changed' do
          expect(controller).to receive(:send_payment_instructions_email).and_return(true)
          patch :update, investor_fund_form: params, offering_id: offering.id
        end

        it "does not send an email if the funding method hasn't changed" do
          params[:funding_method] = 'check'
          expect(controller).to_not receive(:send_payment_instructions_email)
          patch :update, investor_fund_form: params, offering_id: offering.id
        end

        it 'write the information to the audit log' do
          allow_any_instance_of(Time).to receive(:utc).and_return('footime')
          set_request_ip('1.2.3.4')

          patch :update, investor_fund_form: params, offering_id: offering.id

          expect(investor_fund_form_auditor_double).to have_received(:write).
            with(a_hash_including(params.stringify_keys), user.id, '1.2.3.4', request.path, 'footime', nil)
        end
      end

      context 'when the form is invalid' do
        it 'renders the provide account page' do
          patch :update, investor_fund_form: { foo: 'bar' }, offering_id: offering.id

          expect(response).to render_template('new')
        end

        it 'does not audit the form' do
          patch :update, investor_fund_form: { foo: 'bar' }, offering_id: offering.id
          expect(investor_fund_form_auditor_double).to_not have_received(:write)
        end

        it 'does not send an email' do
          patch :update, investor_fund_form: { foo: 'bar' }, offering_id: offering.id
          expect(controller).to_not receive(:send_payment_instructions_email)
        end
      end
    end
  end

end
