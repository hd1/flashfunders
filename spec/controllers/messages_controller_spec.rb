require 'rails_helper'

describe MessagesController do
  let(:user) { create_user(email: 'user@confirmed.com') }
  let(:unconfirmed_user) { create_unconfirmed_user(email: 'user@unconfirmed.com') }

  let(:message_1) { OfferingMessage.new offering_id: 1, user_id: user.id, sent_by_user: true, created_at: Time.current + 1.minute, body: 'message' }
  let(:message_2) { OfferingMessage.new offering_id: 1, user_id: user.id, sent_by_user: false, created_at: Time.current + 2.minute, body: 'message' }
  let(:message_3) { OfferingMessage.new offering_id: 2, user_id: user.id, sent_by_user: true, created_at: Time.current + 3.minute, body: 'message' }
  let(:message_4) { OfferingMessage.new offering_id: 3, user_id: 123, sent_by_user: true, created_at: Time.current + 4.minute, body: 'message' }
  let(:message_5) { OfferingMessage.new offering_id: 3, user_id: 123, sent_by_user: false, created_at: Time.current + 5.minute, body: 'message' }
  let(:message_6) { OfferingMessage.new offering_id: 4, user_id: 123, sent_by_user: true, created_at: Time.current + 6.minute, body: 'message' }

  let(:auditor_double) { double(Auditor, write: nil) }

  context 'as a confirmed user' do
    before do
      sign_in :user, user
      allow(controller).to receive(:auditor).and_return(auditor_double)
    end

    describe '#index' do
      context 'when the user has an offering' do
        before do
          allow(Offering).to receive(:for_user).and_return([Offering.new(id: 3)])
          [message_4, message_3, message_1].map(&:save)
        end

        it 'should be successful' do
          get :index

          expect(response).to be_successful
        end

        it 'should sort the messages newest first' do
          get :index

          messages = [message_4, message_3, message_1].map { |message| MessagePresenter.new(message) }
          expect(assigns(:messages).map(&:id)).to eq(messages.map(&:id))
        end
      end

      context 'when no offering is found' do
        before do
          allow(Offering).to receive(:for_user).and_return([])
          [message_3, message_1].map(&:save)
        end

        it 'should be successful' do
          get :index

          expect(response).to be_successful
        end

        it 'should sort the messages newest first' do
          get :index

          messages = [message_3, message_1].map { |message| MessagePresenter.new(message) }
          expect(assigns(:messages).map(&:id)).to eq(messages.map(&:id))
        end
      end
    end

    describe '#show' do
      before do
        allow(User).to receive(:find).and_return(double(User))
      end

      context 'As an investor' do
        context 'Who is allowed to view these messages' do
          let!(:offering_messages) { [message_1.save!, message_2.save!, message_3.save!, message_4.save!, message_5.save!, message_6.save!] }

          it 'should be successful' do
            get :show, id: 1, user_id: user.uuid

            expect(response.status).to be 200
            expect(assigns(:message)).to be_a OfferingMessage
          end

          it 'should sort the messages newest first' do
            get :show, id: 1, user_id: user.uuid

            expect(assigns(:messages)).to eq([message_2, message_1])
          end

          it 'should mark the messages as read' do
            get :show, id: 1, user_id: user.uuid

            expect(message_1.reload.unread).to eq(true)
            expect(message_2.reload.unread).to eq(false)
          end

          it 'should show a 404 message for the wrong user' do
            get :show, id: 1, user_id: SecureRandom.uuid

            expect(response.status).to eq(404)
          end
        end

        context 'When not allowed to message' do
          it 'should 404' do
            get :show, id: 1, user_id: user.uuid

            expect(response.status).to eq(404)
          end
        end
      end

      context 'As an issuer' do
        context 'Who is allowed to view these messages' do
          let!(:offering_messages) { [message_1.save!, message_2.save!, message_3.save!, message_4.save!, message_5.save!, message_6.save!] }
          before do
            allow(Offering).to receive(:for_user).and_return([offering_1, offering_4])
            OfferingMessage.update_all(user_id: user.id)
          end

          let(:offering_1) { Offering.new id: 1 }
          let(:offering_4) { Offering.new id: 4 }

          context 'with two offerings' do
            before do
              request.cookies['_startup'] = '1'
            end

            it 'should be successful' do
              get :show, id: 4, user_id: user.uuid

              expect(response.cookies['_startup']).to eq('4')
              expect(response.status).to eq(200)
            end
          end
        end
      end
    end

    describe '#create' do
      let(:offering_id) { 1 }

      context 'as an investor' do
        context 'When allowed to message' do
          before { allow(controller).to receive(:conversation_exists?).and_return(true) }

          it 'should be successful' do
            post :create, { offering_message: { body: "Test body", offering_id: offering_id, user_id: user.uuid } }

            expect(response).to redirect_to(dashboard_conversation_path(offering_id, user_id: user.uuid))
          end

          it 'logs to the audit logs' do
            current_time = Time.now
            allow(Time).to receive(:now).and_return(current_time)
            message_body = "Test body"

            set_request_ip('1.2.3.4')
            post :create, { offering_message: { body: message_body, offering_id: offering_id, user_id: user.uuid } }

            expect(auditor_double).to have_received(:write).with({ offering_id: offering_id.to_s, user_id: user.id, body: message_body }.stringify_keys, user.id, '1.2.3.4', '/dashboard/conversations', current_time, nil)
          end

          it 'sends an email to the company' do
            post(:create, { offering_message: { body: "Test body", offering_id: offering_id, user_id: user.uuid } })

            expect(Sidekiq::Extensions::DelayedMailer.jobs.last['args'].first).to have_content(':message_from_user')
          end
        end

        context 'Communicating with an issuer who you have contacted' do
          before { OfferingMessage.create! offering_id: offering_id, user_id: user.id, body: 'message' }

          it 'should be successful' do
            post :create, { offering_message: { body: "Test body", offering_id: offering_id, user_id: user.uuid } }

            expect(response).to redirect_to(dashboard_conversation_path(offering_id, user_id: user.uuid))
          end
        end

        context 'Trying to communicate with a random issuer' do
          it 'should 404' do
            post :create, { offering_message: { body: "Test body", offering_id: offering_id, user_id: user.uuid } }

            expect(response.status).to eq(404)
          end
        end
      end

      context 'Trying to create a message when you are not the issuer or the investor' do
        it 'should 404' do
          post :create, { offering_message: { body: "Test body", offering_id: offering_id, user_id: SecureRandom.base64 } }

          expect(response.status).to eq(404)
        end
      end

      context 'as an issuer' do
        before do
          allow(Offering).to receive(:for_user).and_return([offering])
          Investment.create! offering_id: offering.id, user_id: user.id
        end

        let(:offering) { Offering.new id: 1 }
        let(:investor_id) { user.uuid }

        context 'When allowed to message' do
          it 'sends an email to the investor' do
            post(:create, { offering_message: { body: "Test body", offering_id: offering.id, user_id: investor_id } })

            expect(Sidekiq::Extensions::DelayedMailer.jobs.last['args'].first).to have_content(':message_from_issuer')
          end
        end

        context 'Communicating with an investor' do
          before { Investment.create! offering_id: offering.id, user_id: user.id }
          it 'should be successful' do
            post :create, { offering_message: { body: "Test body", offering_id: offering.id, user_id: investor_id } }

            expect(response).to redirect_to(dashboard_conversation_path(offering_id, user_id: user.uuid))
          end

        end

        context 'Communicating with a user who has messaged the issuer' do
          before { OfferingMessage.create! offering_id: offering.id, user_id: user.id, body: 'message' }
          it 'should be successful' do
            post :create, { offering_message: { body: "Test body", offering_id: offering.id, user_id: investor_id } }

            expect(response).to redirect_to(dashboard_conversation_path(offering_id, user_id: user.uuid))
          end
        end

        context 'Trying to communicate with a random user' do
          let(:random_user) { FactoryGirl.create(:user, uuid: SecureRandom.uuid) }

          before do
            OfferingMessage.create!(offering_id: offering.id, user_id: investor_id, body: 'message')
          end

          it 'should 404' do
            post :create, { offering_message: { body: "Test body", offering_id: offering.id, user_id: random_user.uuid } }

            expect(response.status).to eq(404)
          end
        end

      end
    end
  end
end
