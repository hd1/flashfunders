require 'rails_helper'
describe ApplicationController do
  controller do
    skip_before_filter :authenticate_user!, except: [:index]

    def index
      render text: 'made it'
    end

    def show
      render text: 'here!'
    end

    def update
      render text: 'updated!'
    end

    def destroy
      render text: 'BOOM!'
    end

    def create
      render text: 'Whizbang- have a resource!'
    end
  end

  describe 'authentication' do
    it 'redirects to the sign in page if user is not logged in' do
      get :index

      expect(response).to redirect_to new_user_session_path
    end

    it 'perform the requested action if the user is logged in' do
      sign_in(:user, create_user)

      get :index

      expect(response.body).to eql('made it')
    end
  end

  describe 'storing the url used for after sign in redirect' do
    before do
      sign_in(:user, create_user)
      allow(controller).to receive(:store_location_for)
    end

    it 'stores the requested url in the session' do
      get :show, id: 5

      expect(controller).to have_received(:store_location_for).with(:user, request.path)
    end

    it 'sets the redirect_after_signup key to false' do
      session[:redirect_after_signup] = true
      get :show, id: 5

      expect(session[:redirect_after_signup]).to be_falsy
    end

    it 'does not store the requested url in the session for a devise controller' do
      allow(controller).to receive(:devise_controller?).and_return(true)

      get :show, id: 5

      expect(controller).to_not have_received(:store_location_for)
    end

    it "does not store the requested url in the session for xhr's" do
      xhr :get, :show, id: 5

      expect(controller).to_not have_received(:store_location_for)
    end

    it 'non-get requests do not store requested url in the session' do
      session[:previous_url] = '/boom/bang/fizzle'

      post :create
      patch :update, id: 12
      put :update, id: 12
      delete :destroy, id: 12

      expect(controller).to_not have_received(:store_location_for)
    end
  end

  describe '#impersonating?' do
    it { expect(controller.impersonating?).to be_falsy }

    context 'when impersonation user ID is set in session' do
      before { session[:impersonator_id] = 123 }
      it { expect(controller.impersonating?).to be_truthy }
    end
  end

  describe '#impersonating_user' do
    let(:user) { double(User) }

    before do
      session[:impersonator_id] = 123
      allow(User).to receive(:find).and_return(user)
    end

    it 'returns the user doing the impersonation' do
      expect(controller.impersonating_user).to eql(user)
      expect(User).to have_received(:find).with(123)
    end

    context 'when not currently impersonating' do
      before do
        session[:impersonator_id] = nil
      end

      it 'returns nil' do
        expect(controller.impersonating_user).to be_nil
      end
    end
  end

  describe 'vistior with utm parameters' do
    it 'only gets utm parameters' do
      get :index, utm_source: 'source', utmfake: 'fake', utm_whatever_acceptable: 'other'
      expect(controller.send(:utm_params)).to eq({'utm_source' => 'source', 'utm_whatever_acceptable' => 'other'})
    end

    context 'get client id from google analytics cookie' do
      it 'can get client id' do
        cookies['_ga'] = 'GA1.1.0123456789.9876543210'
        client_id = controller.send(:get_ga_client_id)

        expect(client_id).to eq('0123456789.9876543210')
      end

      it 'cannot get client id' do
        %w(FAKEGA1.1.00000 FAKEGA1.1.1234567890.1234567890 GA1.1.12 GA1.1.1234567890.123456789).each do |ga|
          cookies['_ga'] = ga
          client_id = controller.send(:get_ga_client_id)

          expect(client_id).to_not be_present
        end
      end
    end

    context 'track_visitor' do
      before do
        allow(Utm).to receive(:track!)
        cookies['_ga'] = 'GA1.1.0123456789.9876543210'
      end

      context 'calls utm tracker correctly' do
        it 'does with client id and utm params' do
          get :index, utm_source: 'source', utmfake: 'fake'

          expect(Utm).to have_received(:track!).with('0123456789.9876543210', {utm_source: 'source'}, nil, nil)
        end

        it 'does with client id, page action and no utm params' do
          controller.send(:track_visitor, nil)

          expect(Utm).to have_received(:track!).with('0123456789.9876543210', {}, nil, nil)
        end
      end

      context 'not calling utm tracker' do
        it 'does not call without presence of client id' do
          cookies['_ga'] = 'Fake.GA1.1.0123456789.9876543210'
          get :index, utm_source: 'source', utmfake: 'fake'

          expect(Utm).to_not have_received(:track!)
        end
      end
    end
  end

  describe "#present_in_params?" do
    context 'all args present' do
      before do
        allow(controller).to receive(:params).and_return( { arg1: 'here', arg2: 'also here', arg3: 'me too' })
      end
      it 'should return true' do
        expect(controller.present_in_params?(:arg1, :arg2)).to be_truthy
      end
    end

    context 'some args missing' do
      before do
        allow(controller).to receive(:params).and_return( { arg1: 'here' } )
      end
      it 'should return false' do
        expect(controller.present_in_params?(:arg1, :arg2)).to be_falsy
      end
    end

    context 'all args present, but some have no value' do
      before do
        allow(controller).to receive(:params).and_return( { arg1: 'here', arg2: '' } )
      end
      it 'should return false' do
        expect(controller.present_in_params?(:arg1, :arg2)).to be_falsy
      end
    end
  end

end
