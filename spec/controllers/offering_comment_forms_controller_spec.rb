require 'rails_helper'

describe OfferingCommentFormsController do
  let(:commenter) { FactoryGirl.create(:user, email: 'commenter@example.com', registration_name: 'commenter') }
  let(:issuer_user) { FactoryGirl.create(:user, email: 'issuer_user@example.com', registration_name: 'issuer') }
  let(:offering) { FactoryGirl.create(:offering, issuer_user_id: issuer_user.id, id: 1) }

  let(:form_params) {
    {
      offering_id: offering.id,
      offering_comment_form: {
        body: "Nullam In Dui Mauris",
        offering_id: offering.id,
        user_id: commenter.id,
        return_to: "#{offering_path(offering)}#comment"
      }
    }
  }

  before { sign_in :user, commenter }

  describe "#create" do
    before do
      allow(controller).to receive(:send_email_to_issuer).and_call_original
      allow(controller).to receive(:send_email_to_user).and_call_original
      allow(controller).to receive(:send_email_to_followers).and_call_original
    end

    it "creates a comment successfully" do
      expect{
        post :create, form_params
      }.to change(OfferingComment, :count).by(1)
      expect(response).to redirect_to(form_params[:offering_comment_form][:return_to])
    end

    it "does not create an invalid comment" do
      expect{
        post :create, form_params.deep_merge(offering_comment_form: { body: ""})
      }.to_not change(OfferingComment, :count)
      expect(response).to redirect_to(form_params[:offering_comment_form][:return_to])
    end

    it "sends email to user when issuer replies" do
      comment = create_comment(form_params[:offering_comment_form].slice(:body, :offering_id, :user_id))

      expect {
        post :create, form_params.deep_merge(offering_comment_form: {parent_id: comment.id})
      }.to change(Sidekiq::Extensions::DelayedMailer.jobs, :size).by(1)
      expect(controller).to have_received(:send_email_to_user)
      expect(controller).to have_received(:send_email_to_followers)
      expect(controller).to_not have_received(:send_email_to_issuer)
    end

    it "sends email to issuer when user comments" do
      expect {
        post :create, form_params
      }.to change(Sidekiq::Extensions::DelayedMailer.jobs, :size).by(1)
      expect(controller).to have_received(:send_email_to_issuer)
      expect(controller).to_not have_received(:send_email_to_user)
      expect(controller).to_not have_received(:send_email_to_followers)
    end
  end

  describe "#destroy" do
    let(:mandatory_params) { {offering_id: offering.id, return_to: "#{offering_path(offering)}#comment"} }

    before do
      @user_comment = create_comment(form_params[:offering_comment_form].slice(:body, :offering_id, :user_id))

      params = form_params[:offering_comment_form].merge(parent_id: @user_comment.id, user_id: issuer_user.id)
      @issuer_reply = create_comment(params.slice(:body, :parent_id, :user_id))

      params = form_params[:offering_comment_form].merge(user_id: issuer_user.id)
      @issuer_comment = create_comment(params.slice(:body, :offering_id, :user_id))
    end

    context "comment owner" do
      it "can hide a comment" do
        delete :destroy, mandatory_params.merge(id: @user_comment.id)
        expect(@user_comment.reload).to be_comment_hidden_by_author
      end

      it "cannot hide somebody else's comments" do
        delete :destroy, mandatory_params.merge(id: @issuer_comment.id)
        expect(@issuer_comment.reload).to be_comment_visible
      end

      it "cannot hide issuer's reply" do
        delete :destroy, mandatory_params.merge(id: @issuer_reply.id, parent_id: @user_comment.id)
        expect(@issuer_reply.reload).to be_comment_visible
      end
    end

    context "offering issuer user" do
      before { sign_in :user, issuer_user }

      it "can hide a comment" do
        delete :destroy, mandatory_params.merge(id: @issuer_comment.id)
        expect(@issuer_comment.reload).to be_comment_hidden_by_author
        expect(@issuer_comment.reload.updated_by_user).to eq(issuer_user.id)
      end

      it "can hide whosever comments" do
        delete :destroy, mandatory_params.merge(id: @user_comment.id)
        expect(@user_comment.reload).to be_comment_hidden_by_author
        expect(@user_comment.reload.updated_by_user).to eq(issuer_user.id)
      end

      it "can hide a reply" do
        delete :destroy, mandatory_params.merge(id: @issuer_reply.id, parent_id: @user_comment.id)
        expect(@issuer_reply.reload).to be_comment_hidden_by_author
        expect(@issuer_reply.reload.updated_by_user).to eq(issuer_user.id)
      end
    end
  end

  describe "#send_email_to_followers" do
    let(:user_comment) { create_comment(form_params[:offering_comment_form].slice(:body, :offering_id, :user_id)) }
    let(:issuer_reply) {
      create_comment(form_params[:offering_comment_form].merge(parent_id: user_comment.id, user_id: issuer_user.id)
        .slice(:body, :parent_id, :user_id))
    }

    before do
      allow(controller).to receive(:params).and_return({offering_id: offering.id})
    end

    it "doesn't send email to followers because there is no any offering follower" do
      expect {
        controller.send(:send_email_to_followers, issuer_reply)
      }.to_not change(Sidekiq::Extensions::DelayedMailer.jobs, :size)
    end

    it "send email to offering's followers" do
      OfferingFollower.create(offering_id: offering.id, user_id: FactoryGirl.create(:user).id, mode: :follower)

      expect {
        controller.send(:send_email_to_followers, issuer_reply)
      }.to change(Sidekiq::Extensions::DelayedMailer.jobs, :size).by(1)
    end

  end

  private

  def create_comment(params)
    OfferingCommentForm.new(params).save
  end
end
