require 'rails_helper'

describe BrowseController do
  let(:open_offering_presenter_double) { double(OfferingPresenter, company_name: 'Open Fooland Emporium', formatted_percent_complete: 80, ended?: false)}
  let(:open_bf_offering_presenter_double) { double(OfferingPresenter, company_name: 'Better Funded Emporium', formatted_percent_complete: 90, ended?: false)}
  let(:pending_offering_presenter_double) { double(OfferingPresenter, company_name: 'Fooland Emporium', formatted_percent_complete: 50, ended?: false)}
  let(:closed_offering_presenter_double) { double(OfferingPresenter, company_name: 'Barisle Repository')}

  before do
    allow(controller).to receive(:find_open_campaigns).and_return([pending_offering_presenter_double, open_bf_offering_presenter_double, open_offering_presenter_double])
    allow(controller).to receive(:find_closed_campaigns).and_return([closed_offering_presenter_double])
  end

  describe 'GET show' do
    it 'does not require login' do
      get :show
      expect(response.code).to eq('200')
    end

    context 'when there are more than 5 open campaigns' do
      before do
        allow(controller).to receive(:find_open_campaigns).and_return([pending_offering_presenter_double, open_bf_offering_presenter_double, open_offering_presenter_double])
        allow(controller).to receive(:find_closed_campaigns).and_return([closed_offering_presenter_double])
      end

      it 'finds all the open, placeholder and closed campaigns' do
        get :show

        open_campaigns = assigns(:open_campaigns)[:reg_cf]
        placeholder_campaigns = assigns(:placeholder_campaigns)[:reg_cf]
        closed_campaigns = assigns(:closed_campaigns)[:reg_cf]

        expect(open_campaigns.first.company_name).to eq('Better Funded Emporium')
        expect(open_campaigns[1].company_name).to eq('Open Fooland Emporium')
        expect(open_campaigns.last.company_name).to eq('Fooland Emporium')
        expect(closed_campaigns.first.company_name).to eq('Barisle Repository')
        expect(placeholder_campaigns.length).to eq(0)
      end
    end

  end

end
