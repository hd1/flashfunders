require 'rails_helper'

describe ThirdPartyQualificationsController do

  let(:user) { create_user }
  let(:uuid) { SecureRandom.uuid }
  let(:qualification) { Accreditor::ThirdPartyQualification.new(uuid: uuid, license_state: 'CA') }
  let(:auditor) { double(Auditor, write: true) }

  before do
    sign_in :user, user

    allow(Accreditor::ThirdPartyQualification).to receive(:find_by_uuid!).and_return(qualification)
    allow(InvestorEntity).to receive(:find).and_return(InvestorEntity.new)

  end

  shared_examples_for 'a completed qualification action' do
    context 'when qualification is already submitted' do
      before { qualification.signed_by_third_party_at = Time.current }

      it 'renders the complete page' do
        request
        expect(response).to render_template(:complete)
      end
    end

    context 'when qualification is declined' do
      before { qualification.declined_by_third_party_at = Time.current }

      it 'renders the declined page' do
        request
        expect(response).to render_template(:declined)
      end
    end
  end

  describe 'GET #edit' do
    it 'displays the page' do
      get :edit, id: uuid

      expect(assigns(:qualification)).to be_present
      expect(assigns(:investor_entity)).to be_present
      expect(response).to render_template(:edit)
    end

    it_behaves_like 'a completed qualification action' do
      let(:request) { get :edit, id: uuid }
    end
  end

  describe 'PATCH #update' do
    let(:qualification_form_double) { double(ThirdPartyQualificationForm) }
    let(:params) {
      {
        id: uuid,
        third_party_qualification_form: {
          name: 'John Doe',
          role: 'Licensed Attorney' }
      }
    }

    before do
      allow(controller).to receive(:auditor).and_return(auditor)
      allow(qualification_form_double).to receive(:save).and_return(true)
      allow(ThirdPartyQualificationForm).to receive(:new).and_return(qualification_form_double)
    end

    it 'saves the form object' do
      patch :update, params

      expect(qualification_form_double).to have_received(:save)
    end

    it 'renders the :complete template' do
      patch :update, params

      expect(response).to render_template(:complete)
    end

    it 'sends form params to auditor' do
      patch :update, params

      expect(auditor).to have_received(:write).with(hash_including('license_state' => 'CA', 'user_id' => qualification.user_id), anything, anything, anything, anything)
    end

    context 'when unable to save form' do
      before { allow(qualification_form_double).to receive(:save).and_return(false) }

      it 'renders the :edit template' do
        patch :update, params

        expect(response).to render_template(:edit)
      end
    end

    it_behaves_like 'a completed qualification action' do
      let(:request) { patch :update, params }
    end
  end

  describe 'GET #decline' do
    before { allow(qualification).to receive(:update!) }

    it 'renders the :declined template' do
      get :decline, id: uuid

      expect(response).to render_template(:declined)
    end

    it 'sets the qualifcation record to "declined"' do
      Timecop.freeze do
        get :decline, id: uuid

        expect(qualification).to have_received(:update!).with(
          {declined_by_third_party_at: Time.current}
        )
      end
    end

    it_behaves_like 'a completed qualification action' do
      let(:request) { patch :update, id: uuid }
    end
  end

  describe '#disabled_form' do
    it 'returns false' do
      expect(controller.disabled_form?).to be_falsy
    end

    context 'when the current user is the same as user to be qualified/verified' do
      before { qualification.user_id = user.id }

      it 'returns true' do
        expect(controller.disabled_form?).to be_truthy
      end
    end
  end

end

