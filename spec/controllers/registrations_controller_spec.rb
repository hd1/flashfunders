require 'rails_helper'

describe RegistrationsController do
  before { @request.env['devise.mapping'] = Devise.mappings[:user] }

  describe 'GET #new' do
    context 'when return_to is set in the query string' do
      let(:params) { { return_to: '/some_path' } }

      before { allow(controller).to receive(:store_location_for) }

      it 'stores the location provided' do
        get :new, params

        expect(controller).to have_received(:store_location_for).with(:user, '/some_path')
      end
    end

    context 'when return_to is not set in the query string' do
      before { allow(controller).to receive(:store_location_for) }

      it 'does not store the location provided' do
        get :new

        expect(controller).to_not have_received(:store_location_for)
      end
    end
  end

  describe 'POST #create' do
    let(:registration_form_auditor_double) { double(RegistrationFormAuditor, write: true) }
    let(:time) { Time.now }
    let(:user_params) {
      { registration_name: 'John Fullname',
        email: 'user@example.com',
        password: 'thesecretpassword',
        password_confirmation: 'thesecretpassword',
        user_type: :user_type_investor,
        us_citizen: true,
        country: 'US'
      }
    }
    let(:temporary_user) { create_temporary_user(email: 'new_user@example.com',
                                                 registration_name: 'Partially Registered User') }

    before do
      allow(controller).to receive(:auditor).and_return(registration_form_auditor_double)
      allow(controller).to receive(:registered_at).and_return(time)
      allow(controller).to receive(:track_visitor)
      allow(InvestorMailer).to receive_message_chain(:checking_in, :deliver_later)
    end

    it 'redirects temporary users to the send confirmation screen' do
      pending 'Confirmations temporarily suspended'
      post :create, user: { email: temporary_user.email,
                            registration_name: 'Partially Registered User',
                            password: 'password',
                            password_confirmation: 'password',
                            us_citizen: false,
                            country: 'UK'}

      expect(temporary_user.has_no_password?).to be_truthy
      expect(response).to redirect_to(new_user_confirmation_path(user: { email: temporary_user.email, new: true }))
      temporary_user.reload
      expect(temporary_user.has_no_password?).to be_falsy
    end

    it 'write the information to the audit log' do
      post :create, user: user_params
      audit_params = {
        'registration_name' => 'John Fullname',
        'email' => 'user@example.com'
      }
      expect(registration_form_auditor_double).to have_received(:write).
        with(hash_including(audit_params), User.last.id, '0.0.0.0', user_registration_path, time)
    end

    it 'sets the analytics flash when successful' do
      post :create, user: user_params
      expect(flash[:analytics_sign_up_email][:success]).to eql('user@example.com')
      expect(flash[:analytics_sign_up_email][:failed]).to be_nil
    end

    it 'tracks visitor comming with utm params and then register successfully' do
      post :create, user: user_params
      expect(controller).to have_received(:track_visitor).with(User.first)
    end

    it 'creates the user with the correct info' do
      expect {
        post :create, user: user_params
      }.to change(User, :count).from(0).to(1)

      created_user = User.last

      expect(created_user.registration_name).to eq('John Fullname')
    end

    it 'should redirect to the confirmation page (Use Case: #4, #9)' do
      pending 'Confirmations temporarily suspended'
      controller.store_location_for(:user, privacy_url)

      post :create, user: user_params
      expect(response).to redirect_to(new_user_confirmation_path(user: { email: user_params[:email], new: true }))
    end

    context 'when XHR request' do
      it 'should redirect to the get started page' do
        controller.store_location_for(:user, privacy_url)

        xhr :post, :create, user: user_params
        expect(response).to render_template(:create)
      end
    end

    context 'when registration fails' do
      before { user_params[:email] = 'invalid.ru' }

      it 'does not set the analytics flash' do
        post :create, user: user_params
        expect(flash[:analytics_sign_up_email][:success]).to be_nil
        expect(flash[:analytics_sign_up_email][:failed]).to eql('invalid.ru')
      end

      it 'does not call the audit log' do
        post :create, user: user_params
        expect(registration_form_auditor_double).to_not have_received(:write)
      end
    end

  end

end
