require 'rails_helper'

describe SubscriptionsController do

  describe "GET new" do
    it "renders the show template" do
      get :new
      expect(response).to render_template(:new)
    end

    it 'assigns a new subscription object' do
      get :new
      expect(assigns(:subscription)).to be_present
    end
  end

  describe "POST 'create'" do
    before { allow(controller).to receive(:track_visitor) }

    it "returns http success with a valid email" do
      post :create, {subscription: {email: 'foo@example.com'}}
      expect(response).to be_success
    end

    it 'tracks visitor comming with utm params and then subscibe successfully' do
      post :create, {subscription: {email: 'foo@example.com'}}
      expect(controller).to have_received(:track_visitor).with(Subscription.first)
    end

    it "returns http failure when email is missing" do
      post :create, {subscription: {email: ''}}
      expect(response).to_not be_success
    end

    it "returns http failure with an email with invalid format" do
      post :create, {subscription: {email: 'bad_email'}}
      expect(response).to_not be_success
    end

  end

end
