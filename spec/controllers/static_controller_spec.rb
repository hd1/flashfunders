require 'rails_helper'
describe StaticController do
  before { allow(controller).to receive(:store_location_for) }

  it "does not store the requested url for the terms of use page" do
    get :tos

    expect(controller).to_not have_received(:store_location_for)
  end

  context '#regions' do

    let(:region_hash) { { regions_go: 'here' } }

    before do
      StaticController.class_variable_set('@@state_options', nil) # Make sure it's not cached already
    end

    after do
      StaticController.class_variable_set('@@state_options', nil) # This breaks other tests
    end

    it 'should return json' do
      allow(State).to receive(:get_regions).and_return(region_hash)
      get :regions
      expect(response.body).to eq(region_hash.to_json)
    end

    it 'should cache the result' do
      expect(State).to receive(:get_regions).and_return(region_hash)
      get :regions
      expect(response).to be_success

      expect(State).to_not receive(:get_regions)
      get :regions
      expect(response).to be_success
    end
  end
end

