require 'rails_helper'

describe EmailConsentController do
  let(:user) { FactoryGirl.create(:user, email: 'user@example.com', registration_name: 'user', agreed_to_tos: nil) }

  let(:auditor_double) { double(Auditor, write: nil) }

  before do
    controller.store_location_for(:user, privacy_path)
    allow(controller).to receive(:auditor).and_return(auditor_double)
    user.update_attributes(email_consent_at: nil)
    sign_in :user, user
  end

  describe "#create" do

    it "updates the user record if the form is valid" do
      set_request_ip('1.2.3.4')

      Timecop.freeze do
        post :create, { email_consent_form: { email_consent: '1' } }
        expect(user.reload.email_consent_at.to_i).to eq(Time.now.to_i)
        expect(user.reload.agreed_to_tos_datetime.to_i).to eq(Time.now.to_i)
        expect(user.reload.agreed_to_tos_ip).to eq('1.2.3.4')
        expect(response).to redirect_to(privacy_path)
      end
    end

    it "does not update the user record if the form is invalid" do
      set_request_ip('1.2.3.4')

      post :create, { email_consent_form: { email_consent: '0' } }
      expect(user.reload.email_consent_at).to eq(nil)
      expect(user.reload.agreed_to_tos_datetime.to_i).to eq(0)
      expect(user.reload.agreed_to_tos_ip).to eq(nil)
      expect(response).not_to redirect_to(user_email_consent_path)
    end

    it "redirects the user to the homepage if there is no stored location" do
      set_request_ip('1.2.3.4')
      controller.stored_location_for(:user)

      post :create, { email_consent_form: { email_consent: '1' } }

      expect(response).to redirect_to(root_path)
    end
  end

end
