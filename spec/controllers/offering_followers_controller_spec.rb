require 'rails_helper'

describe OfferingFollowersController do

  let(:user) { create_user }
  let(:follower) { double(OfferingFollower) }

  before do
    sign_in :user, user
    allow(controller).to receive(:follower).and_return(follower)
  end

  describe '#update' do
    before { allow(follower).to receive(:follow_as!) }

    it 'follows the offering' do
      xhr :put, :update, offering_id: 123

      expect(response).to be_successful
      expect(follower).to have_received(:follow_as!).with(:follower)
    end
  end

  describe '#destroy' do
    before {
      allow(follower).to receive(:unfollow!)
      allow(controller).to receive(:company_name).and_return('the company')
    }

    it 'follows the offering' do
      xhr :delete, :destroy, offering_id: 123

      expect(response).to be_successful
      expect(follower).to have_received(:unfollow!)
    end
  end

end
