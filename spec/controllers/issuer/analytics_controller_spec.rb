require 'rails_helper'
require 'issuer_analytics'

module Issuer

  describe AnalyticsController do
    let(:user) { create_user }

    shared_examples_for 'an analytics data api' do |api|

      describe 'user is not logged in' do
        it 'should return unauthorized (401)' do
          Issuer::AnalyticsController.new
          get :get_data, format: :json, data_set: api

          expect(response.status).to eq(401)
        end
      end

      context 'user is logged in' do
        before do
          sign_in :user, user
        end

        context 'with no offerings' do
          it 'should return nothing' do
            get :get_data, format: :json, data_set: api

            expect(response).to be_success
            expect(response.body).to be_empty
          end
        end

        context 'with an offering' do
          before do
            allow(::Google::Auth).to receive(:get_application_default).and_return(nil)
          end

          let(:offering) { Offering.new(id: 123, user_id: user.id, vanity_path: 'likeminder', escrow_end_date: 100.days.from_now) }

          it 'should return data' do
            offering.save
            VCR.use_cassette("google_analytics_#{api}", record: :none, match_requests_on: [:method]) do
              expect(controller).to receive(:cache!).and_call_original

              if api == :referral_info
                get :get_data, format: :json, data_set: api, start_date: '2016-01-01', offering_id: offering.id
              else
                get :get_data, format: :json, data_set: api, start_date: '2016-01-01', offering_id: offering.id, frequency: "daily"
              end

              expect(response).to be_success
              data = JSON.parse(response.body)
              expect(data['offering_id']).to eq(offering.id)
            end

            expect(controller).to_not receive(:get_fresh)
            if api == :referral_info
              get :get_data, format: :json, data_set: api, start_date: '2016-01-01', offering_id: offering.id
            else
              get :get_data, format: :json, data_set: api, start_date: '2016-01-01', offering_id: offering.id, frequency: "daily"
            end
            expect(response).to be_success

            data = JSON.parse(response.body)
            expect(data['offering_id']).to eq(offering.id)

          end
        end

      end
    end

    describe '#page_visit_info' do
      it_behaves_like 'an analytics data api', :page_visit_info
    end

    describe '#referral_info' do
      it_behaves_like 'an analytics data api', :referral_info
    end

    describe "setup_dates" do
      let(:offering) { FactoryGirl.create(:offering, escrow_end_date: 50.days.from_now, created_at: 50.days.ago, issuer_user: user) }


      before do
        allow(Google::Auth).to receive(:get_application_default).and_return(nil)
        ENV['GOOGLE_PROFILE_ID'] = 'XXXXXXXX' # Use dummy profile id
        sign_in :user, user
        VCR.insert_cassette("google_analytics_referral_info", record: :none, match_requests_on: [:method])
      end
      
      after do
        VCR.eject_cassette
      end

      it "uses the offering's created_at date as the start date and today's date as end date if nothing is provided" do
        get :get_data, format: :json, data_set: :referral_info, offering_id: offering.id
        expect(assigns(:end_date)).to eq(Date.today.to_s)
        expect(assigns(:start_date)).to eq(offering.created_at.to_date.to_s)
      end

      it "overrides end dates in the future" do
        get :get_data, format: :json, data_set: :referral_info, end_date: 100.days.from_now.to_date.to_s, offering_id: offering.id
        expect(assigns(:end_date)).to eq(Date.today.to_s)
      end

      it "overrides start date if before the offering created at date" do
        get :get_data, format: :json, data_set: :referral_info, start_date: 100.days.ago.to_date.to_s, offering_id: offering.id
        expect(assigns(:start_date)).to eq(offering.created_at.to_date.to_s)
      end

      it "overrides start date and end dates if start>end even if they're within the offering lifetime" do
        end_date = 47.days.ago.to_date.to_s
        get :get_data, format: :json, data_set: :referral_info, start_date: 45.days.ago.to_date.to_s, end_date: end_date, offering_id: offering.id
        expect(assigns(:start_date)).to eq(offering.created_at.to_date.to_s)
        expect(assigns(:end_date)).to eq(Date.today.to_s)
      end

      it "overrides both start and end date if the start_date is after the end date and the end date is before the offering lifetime" do
        start_date = 51.days.ago.to_date.to_s
        end_date = 55.days.ago.to_date.to_s
        get :get_data, format: :json, data_set: :referral_info, start_date: start_date, end_date: end_date, offering_id: offering.id
        expect(assigns(:start_date)).to eq(offering.created_at.to_date.to_s)
        expect(assigns(:end_date)).to eq(Date.today.to_s)
      end

      it "does not override good dates" do
        start_date = 45.days.ago.to_date.to_s
        end_date = Date.today.to_s
        get :get_data, format: :json, data_set: :referral_info, start_date: start_date, end_date: end_date, offering_id: offering.id
        expect(assigns(:start_date)).to eq(start_date)
        expect(assigns(:end_date)).to eq(end_date)
      end

      it "overrides the end_date if its after the escrow end date and escrow end date is > Today" do
        start_date = 45.days.ago.to_date.to_s
        end_date = Date.today.to_s
        offering.escrow_end_date = 10.days.ago
        offering.save
        get :get_data, format: :json, data_set: :referral_info, start_date: start_date, end_date: end_date, offering_id: offering.id
        expect(assigns(:start_date)).to eq(start_date)
        expect(assigns(:end_date)).to eq(offering.escrow_end_date.to_date.to_s)
      end
    end

  end
end
