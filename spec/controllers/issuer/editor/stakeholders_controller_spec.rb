require 'rails_helper'

module Issuer
  module Editor
    describe StakeholdersController do
      include CarrierWaveDirect::Test::Helpers

      let(:offering) { Offering.new(id: 123) }
      let(:user) { create_user }
      let(:auditor_double) { double(Auditor, write: nil) }

      before do
        sign_in :user, user
        allow(Offering).to receive_message_chain(:for_user, :includes, :find).and_return(offering)
        allow(controller).to receive(:auditor).and_return(auditor_double)
      end

      describe '#edit' do
        it 'renders the page' do
          get :edit, id: 123

          expect(response).to be_successful
          expect(assigns(:offering)).to eql(offering)
        end
      end

      describe '#update' do
        let(:stakeholder_params) {
          { "team_members_attributes" => [
              "id" => "123",
              "_destroy" => "false",
              "stakeholder_type_id" => "456",
              "full_name" => "John Doe",
              "rank" => "1",
              "position" => "CEO",
              "bio" => "This is a bio.",
              "linkedin_url" => "www.linkedin.com",
              "twitter_url" => "www.twitter.com",
              "facebook_url" => "www.facebook.com",
              "website" => "www.example.com",
              "avatar_photo_key" => sample_key(StakeholderUploader.new)
            ],
            "investors_attributes" => [
              "id" => "567",
              "_destroy" => "false",
              "stakeholder_type_id" => "890",
              "full_name" => "Jane Smith",
              "rank" => "1",
              "position" => "Advisor",
              "bio" => "This is a bio.",
              "linkedin_url" => "www.linkedin.com",
              "twitter_url" => "www.twitter.com",
              "facebook_url" => "www.facebook.com",
              "website" => "www.example.com",
              "avatar_photo_key" => sample_key(StakeholderUploader.new)
            ]
          }
        }

        before { allow(offering).to receive(:update_attributes).and_return(true) }

        it 'saves the offering attributes' do
           put :update, id: 123, stakeholders: stakeholder_params

           expect(offering).to have_received(:update_attributes).with(stakeholder_params)
        end

        it 'writes to the audit table' do
          Timecop.freeze do
            current_time = Time.now
            set_request_ip('1.2.3.4')
            put :update, id: 123, stakeholders: stakeholder_params
            expect(auditor_double).to have_received(:write).with(stakeholder_params.stringify_keys, user.id, '1.2.3.4', ('/issuer/editor/stakeholders/' + offering.id.to_s), current_time, nil)
          end
        end

        it "redirects to edit page" do
           put :update, id: 123, stakeholders: stakeholder_params

           expect(response).to redirect_to(edit_issuer_editor_stakeholder_path(123))
        end

        context 'when form is not saved' do
          before { allow(offering).to receive(:update_attributes).and_return(false) }

          it "renders the page" do
             put :update, id: 123, stakeholders: stakeholder_params

             expect(response).to render_template(:edit)
             expect(assigns(:offering)).to eql(offering)
          end
        end
      end
    end
  end
end


