require 'rails_helper'

module Issuer
  module Editor
    describe QuestionsController do

      let(:offering) { Offering.new(id: 123) }
      let(:user) { create_user }
      let(:auditor_double) { double(Auditor, write: nil) }

      before do
        allow(Offering).to receive_message_chain(:for_user, :includes, :find).and_return(offering)
        sign_in :user, user
        allow(controller).to receive(:auditor).and_return(auditor_double)
      end

      describe '#edit' do
        it 'renders the page' do
          get :edit, id: 123

          expect(response).to be_successful
          expect(assigns(:offering)).to eql(offering)
        end

        it 'does not initialize any records' do
          get :edit, id: 123

          expect(assigns(:offering).investor_faqs.size).to eql(0)
        end
      end

      describe '#update' do
        let(:faq_params) {
          { "investor_faqs_attributes" => [
              "id" => "123",
              "_destroy" => "false",
              "question" => "Why?",
              "answer" => "Just because.",
              "rank" => "1"
            ]
          }
        }

        before { allow(offering).to receive(:update_attributes).and_return(true) }

        it 'saves the offering attributes' do
           put :update, id: 123, faq: faq_params

           expect(offering).to have_received(:update_attributes).with(faq_params)
        end

        it 'writes to the audit table' do
          Timecop.freeze do
            current_time = Time.now
            set_request_ip('1.2.3.4')
            put :update, id: 123, faq: faq_params
            expect(auditor_double).to have_received(:write).with(faq_params.stringify_keys, user.id, '1.2.3.4', ('/issuer/editor/questions/' + offering.id.to_s), current_time, nil)
          end
        end

        it "redirects to edit page" do
           put :update, id: 123, pitch: faq_params

           expect(response).to redirect_to(edit_issuer_editor_question_path(123))
        end

        context 'when form is not saved' do
          before { allow(offering).to receive(:update_attributes).and_return(false) }

          it "renders the page" do
             put :update, id: 123, pitch: faq_params

             expect(response).to render_template(:edit)
             expect(assigns(:offering)).to eql(offering)
          end
        end
      end
    end
  end
end

