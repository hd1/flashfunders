require 'rails_helper'

module Issuer
  module Editor
    describe PitchesController do

      let(:offering) { Offering.new(id: 123) }
      let(:user) { create_user }
      let(:auditor_double) { double(Auditor, write: nil) }

      before do
        allow(Offering).to receive_message_chain(:for_user, :includes, :find).and_return(offering)
        sign_in :user, user
        allow(controller).to receive(:auditor).and_return(auditor_double)
      end

      describe '#edit' do
        it "renders the page" do
          get :edit, id: 123

          expect(response).to be_successful
          expect(assigns(:offering)).to eql(offering)
        end
      end

      describe '#update' do
        let(:pitch_params) {
          { "pitch_sections_attributes" => [
              "id" => "123",
              "_destroy" => "false",
              "title" => "foo",
              "rank" => "1",
              "content_items_attributes" => [
                "content_type" => "SomeClass",
                "_destroy" => "false",
                "rank" => "1",
                "content_attributes" => [
                  "id" => "123",
                  "body" => "This is a description",
                  "html" => "0",
                  "key" => "this/is/a/key",
                  "expandable" => "1",
                  "carousel_images_attributes" => [
                    "id" => "456",
                    "_destroy" => "false",
                    "key" => "this/is/a/key",
                    "rank" => "1"
                  ]
                ]
              ]
            ]
          }
        }

        before { allow(offering).to receive(:update_attributes).and_return(true) }

        it 'saves the offering attributes' do
           put :update, id: 123, pitch: pitch_params

           expect(offering).to have_received(:update_attributes).with(pitch_params)
        end

        it 'writes to the audit table' do
          Timecop.freeze do
            current_time = Time.now
            set_request_ip('1.2.3.4')
            put :update, id: 123, pitch: pitch_params
            expect(auditor_double).to have_received(:write).with(pitch_params.stringify_keys, user.id, '1.2.3.4', ('/issuer/editor/pitches/' + offering.id.to_s), current_time, nil)
          end
        end

        it "redirects to edit page" do
           put :update, id: 123, pitch: pitch_params

           expect(response).to redirect_to(edit_issuer_editor_pitch_path(123))
        end

        context 'when form is not saved' do
          before { allow(offering).to receive(:update_attributes).and_return(false) }

          it "renders the page" do
             put :update, id: 123, pitch: pitch_params

             expect(response).to render_template(:edit)
             expect(assigns(:offering)).to eql(offering)
          end
        end
      end

    end
  end
end


