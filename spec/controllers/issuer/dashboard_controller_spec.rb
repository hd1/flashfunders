require 'rails_helper'

module Issuer

  describe DashboardController do
    let(:offering) { Offering.new(id: 123) }
    let(:user) { create_user(email: 'user@confirmed.com') }
    let(:unconfirmed_user) { create_unconfirmed_user(email: 'user@unconfirmed.com') }

    context 'as a confirmed user' do
      before do
        sign_in :user, user
      end

      describe '#show' do
        context 'with a valid offering' do
          before do
            allow(Offering).to receive(:for_user).and_return([offering])
          end

          it 'should render the page' do
            get :show, id: 123

            expect(response).to be_successful
            expect(assigns(:offering)).to eql(offering)

            data = assigns(:investment_data)
            expect(data[:items]).to eq([])
            expect(data[:items].class).to eq(Kaminari::PaginatableArray)
            expect(data[:sort_by]).to eq('updated_at')
            expect(data[:sort_dir]).to eq('desc')
            expect(data[:page]).to eq(0)
            expect(data[:remaining]).to eq(-DashboardController::ITEMS_PER_PAGE)
            expect(data[:displayed]).to eq(0)
            expect(data[:sort_headers].count).to eq(4)
          end
        end

        context 'with an invalid offering' do
          it 'should not render the page and return 404' do
            get :show, id: 124

            expect(response.status).to eq(404)
          end
        end
      end
    end
  end
end
