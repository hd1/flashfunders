require 'rails_helper'

module Issuer

  describe IssuerController do

    let(:offering) { Offering.new(id: 123) }
    let(:user) { create_user }

    before do
      sign_in :user, user
    end

    describe '#change_context' do
      before do
        allow(Offering).to receive(:for_user).and_return([offering])
        cookies['_startup'] = 500
      end

      let(:target) { issuer_dashboard_path(123) }

      it 'should update the cookie and redirect' do
        get :change_context, id: 123, target: target

        expect(cookies['_startup'].to_i).to eq(123)
        expect(response.status).to eq(302)
        expect(response).to redirect_to(target)
      end
    end
  end
end
