require 'rails_helper'

describe FormAuditing do
  # set up fake class which includes FormAuditing
  before do
    class FakeClass
      include FormAuditing

      # methods needed to create objects for these tests
      def initialize(params)
        @params = params
      end

      def current_user
        @params[:current_user]
      end

      def impersonating_user
        @params[:impersonator]
      end

      def request
        ActiveSupport::OrderedOptions.new.tap do |o|
          o.ip = @params[:request_ip]
          o.path = @params[:request_path]
        end
      end

      # methods needed by FormAuditing
      private

      def auditor
        @params[:auditor]
      end

      def audit_params
        @params[:form_params]
      end
    end
  end

  after do
    Object.send :remove_const, :FakeClass
  end

  let(:auditor_double) { double(Auditor, write: nil) }
  let(:real_user) {User.new(id: 1)}
  let(:impersonator) {User.new(id: 2)}
  let(:form_params) { {"any_old_thing" => ["id" => "123"]} }
  let(:request_ip) { '1.2.3.4' }
  let(:request_path) { '/anyold/path' }

  describe '#audit_form!' do
    let(:object) { FakeClass.new({auditor: auditor_double, form_params: form_params,
                                  current_user: real_user, impersonator: impersonator,
                                  request_ip: request_ip, request_path: request_path}) }

    it 'writes to the audit table' do
      Timecop.freeze do
        current_time = Time.now
        object.audit_form!
        expect(auditor_double).to have_received(:write).with(form_params.stringify_keys, real_user.id,
                                                             request_ip, request_path,
                                                             current_time, impersonator.id)
      end
    end
  end

  describe '#audit_form_without_user!' do
    let(:object) { FakeClass.new({auditor: auditor_double, form_params: form_params,
                                  request_ip: request_ip, request_path: request_path}) }

    it 'writes to the audit table' do
      Timecop.freeze do
        current_time = Time.now
        object.audit_form_without_user!
        expect(auditor_double).to have_received(:write).with(form_params.stringify_keys, nil,
                                                             request_ip, request_path, current_time)
      end
    end
  end

  describe '#auditor' do
    it 'raises an error when not defined' do
      FakeClass.send(:remove_method, :auditor)
      object = FakeClass.new({auditor: auditor_double,
                              form_params: form_params,
                              request_ip: request_ip,
                              request_path: request_path})

      expect { object.audit_form_without_user! }.to raise_error(RuntimeError)
    end
  end

  describe '#audit_params' do
    it 'raises an error when not defined' do
      FakeClass.send(:remove_method, :audit_params)
      object = FakeClass.new({auditor: auditor_double,
                              form_params: form_params,
                              request_ip: request_ip,
                              request_path: request_path})

      expect { object.audit_form_without_user! }.to raise_error(RuntimeError)
    end
  end
end
