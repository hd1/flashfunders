require 'rails_helper'

describe InvestmentsController do
  let(:user) { create_user }

  before do
    sign_in :user, user
  end

  describe 'GET index' do
    it 'should not attempt to check the cip status' do
      get :index

      expect(response).to be_success
    end
  end

  describe 'GET download_docs' do
    let(:offering) { FactoryGirl.create(:offering) }
    let(:security) { offering.securities.first }
    let(:investment) { FactoryGirl.create(:investment, user_id: user.id, offering: offering, state: 'signed_documents', envelope_id: 'some_id', security: security) }
    let(:docusign_client) { double(DocusignRest) }

    before do
      allow(DocusignRest::Client).to receive(:new).and_return(docusign_client)
      allow(docusign_client).to receive(:get_combined_document_from_envelope).and_return(File.read('spec/fixtures/document_investment.pdf'))
    end

    it 'should render the PDF stream from docusign' do
      get :download_docs, id: investment.id
      expect(response.body.length).to eq(25701)
      expect(response.headers['Content-Disposition']).to have_content('inline')
      expect(response.headers['Content-Disposition']).to have_content("Investment Agreement Documents #{offering.corporate_name}.pdf")
    end

    it 'should get multiple PDFs combined if there is an omnibus signature doc' do
      security.update_attribute(:docusign_issuer_countersign_omnibus_id, 'x')
      investment.update_attribute(:state, 'counter_signed')

      get :download_docs, id: investment.id
      expect(response.body.length).to eq(26042)
      expect(response.headers['Content-Disposition']).to have_content('inline')
      expect(response.headers['Content-Disposition']).to have_content("Investment Agreement Documents #{offering.corporate_name}.pdf")
    end

  end

  describe 'PUT cancel_investment' do
    let(:offering) { FactoryGirl.create(:offering, :basics_equity, :reg_c) }
    let(:investment) { FactoryGirl.create(:investment, user_id: user.id, offering: offering, state: 'escrowed') }

    context 'Reg C investment' do
      before { investment.update_attribute(:security_id, offering.reg_c_security.id) }

      it 'can cancel an investment' do
        put :cancel_investment, id: investment.id
        expect(investment.reload).to be_cancelled
      end

      it "cannot cancel anyone else's investment" do
        investment.update_attribute(:user_id, 999)
        put :cancel_investment, id: investment.id
        expect(investment.reload).to_not be_cancelled
      end

      it "cannot cancel an investment's state is not pending transfer or escrowed" do
        investment.update_attribute(:state, 'signed_documents')
        put :cancel_investment, id: investment.id
        expect(investment.reload).to_not be_cancelled
      end

      it 'cannot cancel an investment where cooling off period has ended' do
        offering.update_attribute(:ends_at, 7.days.ago)
        put :cancel_investment, id: investment.id
        expect(investment.reload).to_not be_cancelled
      end
    end

    context 'Non Reg C investment' do
      before { investment.update_attribute(:security_id, offering.other_security.id) }

      it 'cannot cancel an investment' do
        put :cancel_investment, id: investment.id
        expect(investment.reload).to_not be_cancelled
      end
    end

  end
end
