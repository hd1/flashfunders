FactoryGirl.define do
  factory :personal_federal_identification_information, aliases: [:pfii] do
    first_name "Paulo"
    last_name "Delgado"
    middle_initial "A"
    date_of_birth "1976-10-31"
    ssn "654656544"
    address1 "123 Main St"
    city "Moo"
    zip_code "91919"
    daytime_phone "5555555555"
    state_id "CA"
    country "US"
    id_type "Driver's License"
    id_number "D123456789"
    location "Cali"
    issued_date "01/01/1980"
    expiration_date "01/01/2080"
    employer_industry "true" # hstore will always return a string
    us_citizen true
  end

  trait :international do
    first_name "Jean Paul"
    address1 "1 Rue Le Main"
    city "Paris"
    zip_code "FR12345"
    daytime_phone "+43 2 555-555-5555"
    state_id "F"
    country "FR"
    us_citizen false
  end
end
