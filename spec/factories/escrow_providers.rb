def build_escrow_provider_data(hash)
  id hash[:id]
  name hash[:name]
  provider_key hash[:provider_key]
  properties hash[:properties]
  default_instructions hash[:default_instructions]
  initialize_with { EscrowProvider.find_or_create_by(id: id) }
end

FactoryGirl.define do
  require Rails.root + 'db/escrow_provider_seed_data.rb'
  escrow_providers = escrow_provider_seed_data
  factory :escrow_provider_bancbox, class: 'EscrowProvider' do
    build_escrow_provider_data(escrow_providers[0])
  end

  factory :escrow_provider_manual, class: 'EscrowProvider' do
    build_escrow_provider_data(escrow_providers[1])
  end

  factory :escrow_provider_fundamerica, class: 'EscrowProvider' do
    build_escrow_provider_data(escrow_providers[2])
  end

  factory :escrow_provider_first_century, class: 'EscrowProvider' do
    build_escrow_provider_data(escrow_providers[3])
  end
end