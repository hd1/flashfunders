FactoryGirl.define do
  factory :offering_funding_detail_direct do
    check_payable_to 'Direct account name'
    check_memo 'Remember this'
    wire_instructions 'Send it quick'
    check_bank_name 'Main Street Bank'
    wire_beneficiary_name 'Willy Keepitsafe Direct'
    association :wire_bank_account, factory: :wire_bank_account, strategy: :build
    association :check_address, factory: :check_address, strategy: :build
    association :wire_bank_address, factory: :wire_bank_address, strategy: :build
    association :wire_beneficiary_address, factory: :wire_beneficiary_address, strategy: :build
  end
  factory :offering_funding_detail_spv do
    check_payable_to 'SPV account name'
    check_memo 'Who could forget'
    wire_instructions 'Send it now'
    check_bank_name 'Main Street Bank'
    wire_beneficiary_name 'Willy Keepitsafe SPV'
    association :wire_bank_account, factory: :wire_bank_account, strategy: :build
    association :check_address, factory: :check_address, strategy: :build
    association :wire_bank_address, factory: :wire_bank_address, strategy: :build
    association :wire_beneficiary_address, factory: :wire_beneficiary_address, strategy: :build
  end
  factory :offering_funding_detail_direct_international, class: 'OfferingFundingDetailDirectInternational' do
    check_payable_to 'International direct account name'
    check_memo 'Remember this cobber'
    wire_instructions 'Send it quicker than two shakes of a roo tail'
    check_bank_name 'MacQuarrie Bank'
    wire_beneficiary_name 'Bruce'
    association :wire_bank_account, factory: :wire_bank_account, strategy: :build
    association :check_address, factory: :check_address, strategy: :build
    association :wire_bank_address, factory: :wire_bank_address, strategy: :build
    association :wire_beneficiary_address, factory: :wire_beneficiary_address, strategy: :build
  end
  factory :offering_funding_detail_spv_international, class: 'OfferingFundingDetailSpvInternational' do
    check_payable_to 'International SPV account name'
    check_memo 'Oi - mate!'
    wire_instructions 'Chuck it ovah heah'
    check_bank_name 'East End Money Changers'
    wire_beneficiary_name 'Chav'
    association :wire_bank_account, factory: :wire_bank_account, strategy: :build
    association :check_address, factory: :check_address, strategy: :build
    association :wire_bank_address, factory: :wire_bank_address, strategy: :build
    association :wire_beneficiary_address, factory: :wire_beneficiary_address, strategy: :build
  end
end

FactoryGirl.define do

  factory :routing_number do
    name 'Big Bank Inc'
  end

  factory :bank_account, class: 'Bank::WireAccount', aliases: [:wire_bank_account]  do
    user_id nil
    bank_name 'Big Bucks'
    routing_number '123456789'
    account_number '4455667788'
    account_type 'checking'
    after(:build) do |bank_account, evaluator|
      create(:routing_number, routing_number: bank_account.routing_number)
    end
  end
end

FactoryGirl.define do
  factory :address, aliases: [:check_address, :wire_bank_address, :wire_beneficiary_address] do
    sequence(:address1) { |i| "#{i} Main Street" }
    address2 nil
    city 'Yankton'
    state 'SD'
    zip_code '57078'
    phone_number '555-444-1232'
  end
end
