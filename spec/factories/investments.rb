FactoryGirl.define do
  factory :investment do
    user
    offering
    state :started
    amount 10000
    shares 1000

    trait :escrowed do
      state         :escrowed
      escrowed_at   Time.current
    end

    trait :by_individual do
      association :investor_entity_individual
    end

    trait :intended do
      state :intended
    end

    factory :international_investment do
      association :investor_entity, factory: :international_investor_entity
      association :offering, :fund_america
      state :funding_method_selected
    end
  end
end
