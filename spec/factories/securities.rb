FactoryGirl.define do
  trait :basic_security do
    offering
    escrow_provider                   { FactoryGirl.create(:escrow_provider_fundamerica) }
    offering_funding_detail_domestic  { FactoryGirl.create(:offering_funding_detail_direct) }
    offering_funding_detail_international  { FactoryGirl.create(:offering_funding_detail_direct_international) }
    minimum_total_investment          750000.0
    minimum_total_investment_display  '$750K'
    minimum_investment_amount         1000
    minimum_investment_amount_display '$1000'
    maximum_total_investment          1500000.0
    maximum_total_investment_display  '$1.5 million'
  end

  factory :cf_stock, traits: [:basic_security], class: 'Securities::CfStock' do
    regulation                        Securities::Security.regulations[:reg_cf]
    minimum_total_investment          50_000
    minimum_total_investment_display  '$50K'
    minimum_investment_amount         50
    minimum_investment_amount_display '$50'
    maximum_total_investment          1000000.0
    maximum_total_investment_display  '$1 million'
  end

  factory :security, traits: [:basic_security], class: 'Securities::Security' do
    shares_offered                    1138088

    factory :fsp_stock, class: 'Securities::FspStock'
  end

  factory :convertible, traits: [:basic_security], class: 'Securities::Convertible' do
    discount_rate          35
    security_title                   'Bitcoin'
    security_title_plural            'Bitcoins'
    valuation_cap          5500000
    valuation_cap_display  '$5.5 Million'
    shares_offered                    nil

    factory :fsp_convertible, class: 'Securities::FspConvertible'
  end

end

