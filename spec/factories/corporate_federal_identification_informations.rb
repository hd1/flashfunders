FactoryGirl.define do
  factory :corporate_federal_identification_information do
    state_id 'CA'
    corporate_name 'ACME Corp.'
    ein '123456789'
  end
end
