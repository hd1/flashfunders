FactoryGirl.define do
  factory :issuer_application do
    first_name 'Offering'
    last_name 'Issuer'
    phone_number '333-444-3333'
    company_name 'My Great Company'
    website 'http://my_great_company.com'
    amount_needed 5000000
    pre_money_valuation 500
    previously_raised 0
    pledged 25
    entity_type 'S-Corp'
    state_id 'DE'
    message 'My company is the greatest!'
  end
end
