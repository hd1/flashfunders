FactoryGirl.define do
  factory :offering_comment do
    user { FactoryGirl.create(:user) }
    offering { FactoryGirl.create(:offering) }
    body 'body comment'
    display_state 1
    updated_by_user nil
    parent_id nil
  end
end
