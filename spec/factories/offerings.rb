FactoryGirl.define do
  trait :basics do
    user { FactoryGirl.create(:user, :with_pfii) }
    company
    visibility                        Offering.visibilities[:visibility_public]
    pre_money_valuation               4000000.0
    pre_money_valuation_display       '$4 million'
    investable                        true
    escrow_end_date                   Date.today + 110
    ends_at                           Time.current + 100.days
  end

  trait :basics_equity do
    fully_diluted_shares              3035604
    share_price                       1.318
  end

  trait :basics_spv do
    setup_fee_dollar                  0
    setup_fee_display                 '$0.00'
  end

  trait :convertible do
    fully_diluted_shares nil

    after(:build) do |offering|
      if offering.securities.empty?
        offering.securities = [build(:convertible, offering: offering, escrow_provider: FactoryGirl.create(:escrow_provider_first_century)) ]
      end
    end
  end

  factory :spv_convertible_offering, class: 'Offering', traits: [:basics, :convertible] do
    after(:build) do |offering|
      if offering.securities.size <= 1
        offering.securities.append(build(:convertible, offering: offering, is_spv: true, escrow_provider: FactoryGirl.create(:escrow_provider_first_century)))
      end
    end
  end

  factory :convertible_offering, class: 'Offering', traits: [:basics, :convertible]

  factory :offering, traits: [:basics, :basics_equity, :basics_spv] do
    after(:build) do |offering|
      if offering.securities.empty?
        offering.securities = [
            build(:fsp_stock, offering: offering),
            build(:fsp_stock, offering: offering, is_spv: true, minimum_total_investment: 0)
        ]
      end
    end

    trait :fsp_only do
      securities { [build(:fsp_stock)] }
    end

    factory :fsp_only_offering, traits: [:fsp_only]

    trait :fund_america do
      after(:build) do |offering|
        offering.securities = [build(:fsp_stock, offering: offering, fund_america_id: 'iS2dFdP3QteQAp7J9RvrzQ', escrow_provider: FactoryGirl.create(:escrow_provider_fundamerica)) ]
      end
    end

    trait :first_century do
      after(:build) do |offering|
        offering.securities = [build(:fsp_stock, offering: offering, escrow_provider: FactoryGirl.create(:escrow_provider_first_century)) ]
      end
    end

    trait :manual do
      after(:build) do |offering|
        offering.securities = [build(:fsp_stock, offering: offering, escrow_provider: FactoryGirl.create(:escrow_provider_manual)) ]
      end
    end

    trait :empty do
      user                              nil
      company                           nil
      pre_money_valuation               nil
      pre_money_valuation_display       nil
      fully_diluted_shares              nil
      escrow_end_date                   nil
      ends_at                           nil

    end

    trait :reg_c do
      after(:build) do |offering|
        offering.securities.append([build(:cf_stock)])
      end
    end

    trait :reg_c_only do
      securities { [build(:cf_stock)] }
    end

    factory :empty_offering, traits: [:empty]


  end

end
