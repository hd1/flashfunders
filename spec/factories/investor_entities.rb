FactoryGirl.define do
  factory :third_party_income_qualification, class: 'Accreditor::ThirdPartyIncomeQualification' do
    user
    data Hash.new
  end

  factory :investor_entity do
    name 'John Doe'
    tax_id_number 123456789
    address_1 '123 Main St.'
    city 'Santa Monica'
    state_id 'CA'
    country 'US'
    zip_code '90401'
    phone_number '3105551212'
    position 'Advisor'
    formation_state_id 'DE'
    formation_country 'US'

    factory :investor_entity_individual, class: 'InvestorEntity::Individual' do
      trait :with_pfii do
        personal_federal_identification_information
      end
    end

    factory :investor_entity_llc, class: 'InvestorEntity::Entity' do
      trait :with_pfii do
        personal_federal_identification_information
      end
    end

    factory :investor_entity_trust, class: 'InvestorEntity::Entity' do
      trait :with_pfii do
        personal_federal_identification_information
      end
    end

    factory :international_investor_entity, class: 'InvestorEntity::Individual' do
      country 'GB'
      state_id 'BNS'
      city 'Barnsley'
    end

  end

end

