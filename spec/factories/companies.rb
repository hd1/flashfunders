FactoryGirl.define do
  factory :company do
    name "Acme Co."
    corporate_federal_identification_information
  end
end
