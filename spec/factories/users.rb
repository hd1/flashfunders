FactoryGirl.define do
  factory :user do
    sequence(:email) { |i| "user_#{i}@flashfunders.com" }
    password 'password'
    agreed_to_tos true
    registration_name 'Jane Schmoe'
    user_type :user_type_investor
    confirmed_at 1.day.ago
    email_consent_at 1.day.ago

    trait :with_pfii do
      personal_federal_identification_information
    end
  end
end

