require 'rails_helper'

describe CityValidator do
  describe '#validate_each' do
    let(:model) { mock_model('SomeModel') }

    subject(:validator) { described_class.new(attributes: {foo: 'bar'}) }

    it 'allows names with a-z and spaces' do
      validator.validate_each(model, :name, "Quigley McDaniels")
      expect(model.errors).to be_empty
    end

    it 'adds errors that enumerate the failing characters' do
      validator.validate_each(model, :name, "Quigley Mc!!")
      expect(model.errors).to_not be_empty
      expect(model.errors[:name]).to match_array(['Only letters and spaces allowed'])
    end
  end
end
