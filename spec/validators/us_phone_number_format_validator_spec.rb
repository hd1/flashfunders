require 'rails_helper'

describe UsPhoneNumberFormatValidator do
  describe '#validate_each' do
    subject(:us_phone_number_validator) { UsPhoneNumberFormatValidator.new(attributes: {foo: 'bar'})}

    let(:model) {mock_model('SomeModel')}

    it 'allows valid phone numbers, ignoring symbols' do
      us_phone_number_validator.validate_each(model, :phone_number, '1234567890')
      expect(model.errors[:phone_number]).to be_blank

      us_phone_number_validator.validate_each(model, :phone_number, '(123) 456-7890')
      expect(model.errors[:phone_number]).to be_blank

      us_phone_number_validator.validate_each(model, :phone_number, '1 (123) 456-7890')
      expect(model.errors[:phone_number]).to be_blank

      us_phone_number_validator.validate_each(model, :phone_number, '123.456-7890')
      expect(model.errors[:phone_number]).to be_blank

      us_phone_number_validator.validate_each(model, :phone_number, '12*3456789#0')
      expect(model.errors[:phone_number]).to be_blank
    end

    it 'allows a blank phone number' do
      us_phone_number_validator.validate_each(model, :phone_number, '')
      expect(model.errors[:phone_number]).to be_blank

      us_phone_number_validator.validate_each(model, :phone_number, nil)
      expect(model.errors[:phone_number]).to be_blank
    end

    it 'adds errors if the number is invalid' do
      us_phone_number_validator.validate_each(model, :phone_number, 'ABC-123-4567')
      expect(model.errors[:phone_number]).to_not be_blank

      us_phone_number_validator.validate_each(model, :phone_number, '12-1233-121')
      expect(model.errors[:phone_number]).to_not be_blank

      us_phone_number_validator.validate_each(model, :phone_number, '123A-123-4567')
      expect(model.errors[:phone_number]).to_not be_blank

      us_phone_number_validator.validate_each(model, :phone_number, '4223-123-4567')
      expect(model.errors[:phone_number]).to_not be_blank
    end
  end
end