require 'rails_helper'

describe TaxIdFormatValidator do

  subject(:tax_id_validator) { TaxIdFormatValidator.new(attributes: {foo: 'bar'}) }

  let(:model) { mock_model('SomeModel')}

  describe '#validate_each' do
    it 'allows valid Tax IDs, including hyphens, periods or no extra formatting' do
      tax_id_validator.validate_each(model, :tax_id, '123-45-6789')
      expect(model.errors[:tax_id]).to be_blank

      tax_id_validator.validate_each(model, :tax_id, '123.45.6789')
      expect(model.errors[:tax_id]).to be_blank

      tax_id_validator.validate_each(model, :tax_id, '123-45-.6789')
      expect(model.errors[:tax_id]).to be_blank

      tax_id_validator.validate_each(model, :tax_id, '123456789')
      expect(model.errors[:tax_id]).to be_blank
    end

    it 'adds an error if tax ID is blank' do
      tax_id_validator.validate_each(model, :tax_id, nil)
      expect(model.errors[:tax_id]).to_not be_blank

      tax_id_validator.validate_each(model, :tax_id, '')
      expect(model.errors[:tax_id]).to_not be_blank
    end

    it 'adds an error if not enough digits for tax ID' do
      tax_id_validator.validate_each(model, :tax_id, '12349')

      expect(model.errors[:tax_id]).to_not be_blank
    end

    it 'adds an error if it is the correct format but contains any non-digits' do
      tax_id_validator.validate_each(model, :tax_id, '123-45A-6789')
      expect(model.errors[:tax_id]).to_not be_blank

      tax_id_validator.validate_each(model, :tax_id, '123A45A6789')
      expect(model.errors[:tax_id]).to_not be_blank
    end
  end
end
