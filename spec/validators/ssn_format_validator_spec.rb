require 'rails_helper'

describe SsnFormatValidator do

  subject(:ssn_validator) { SsnFormatValidator.new(attributes: {foo: 'bar'}) }

  let(:model) { mock_model('SomeModel')}

  describe '#validate_each' do
    it 'allows valid SSNs, including hyphens, periods or no extra formatting' do
      ssn_validator.validate_each(model, :ssn, '123-45-6789')
      expect(model.errors[:ssn]).to be_blank

      ssn_validator.validate_each(model, :ssn, '123.45.6789')
      expect(model.errors[:ssn]).to be_blank

      ssn_validator.validate_each(model, :ssn, '123-45-.6789')
      expect(model.errors[:ssn]).to be_blank

      ssn_validator.validate_each(model, :ssn, '123456789')
      expect(model.errors[:ssn]).to be_blank
    end

    it 'adds an error if ssn is blank' do
      ssn_validator.validate_each(model, :ssn, nil)
      expect(model.errors[:ssn]).to_not be_blank

      ssn_validator.validate_each(model, :ssn, '')
      expect(model.errors[:ssn]).to_not be_blank
    end

    it 'adds an error if not enough digits for SSN' do
      ssn_validator.validate_each(model, :ssn, '12349')

      expect(model.errors[:ssn]).to_not be_blank
    end

    it 'adds an error if it is the correct format but contains any non-digits' do
      ssn_validator.validate_each(model, :ssn, '123-45A-6789')
      expect(model.errors[:ssn]).to_not be_blank

      ssn_validator.validate_each(model, :ssn, '123A45A6789')
      expect(model.errors[:ssn]).to_not be_blank
    end
  end
end
