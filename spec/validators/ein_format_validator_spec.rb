require 'rails_helper'

describe EinFormatValidator do

  subject(:ein_validator) { EinFormatValidator.new(attributes: {foo: 'bar'}) }

  let(:model) { mock_model('SomeModel')}

  describe '#validate' do
    it "allows valid EIN's, without hyphens, which are 9 digits" do
      allow(model).to receive(:ein).and_return('123456789')
      ein_validator.validate(model)
      expect(model.errors[:ein]).to be_blank
    end

    it 'adds an error if there are any non-digits but the correct number of characters' do
      allow(model).to receive(:ein).and_return('12-345678')
      ein_validator.validate(model)
      expect(model.errors[:ein]).to_not be_blank
    end

    it 'adds an error if not enough digits for EIN' do
      allow(model).to receive(:ein).and_return('12349')
      ein_validator.validate(model)
      expect(model.errors[:ein]).to_not be_blank
    end
  end
end
