require 'rails_helper'

describe CastStringAttribute do
  describe '.boolean' do
    it 'raises unless the input is nil or a string' do
      expect { CastStringAttribute.boolean(123) }.to raise_error(CastStringAttribute::InputNotAString)
    end

    it 'casts to a boolean for "true" or "false"' do
      expect(CastStringAttribute.boolean('true')).to eq(true)
      expect(CastStringAttribute.boolean('false')).to eq(false)
    end

    it 'returns nil for nil' do
      expect(CastStringAttribute.boolean(nil)).to eq(nil)
    end

    it 'returns the original value for values not equal to "true" or "false"' do
      expect(CastStringAttribute.boolean('not a boolean')).to eq('not a boolean')
    end
  end

  describe '.integer' do
    it 'raises if the input is not a string' do
      expect { CastStringAttribute.integer(123) }.to raise_error(CastStringAttribute::InputNotAString)
    end

    it 'returns nil for empty string' do
      expect(CastStringAttribute.integer('')).to eq(nil)
    end

    it 'returns nil for nil' do
      expect(CastStringAttribute.integer(nil)).to eq(nil)
    end

    it 'casts to an integer for integer values' do
      expect(CastStringAttribute.integer('234')).to eq(234)
    end
  end

  describe '.array_of_integers' do
    it 'raises if the array does not contain all strings' do
      expect { CastStringAttribute.array_of_integers(["23", 123]) }.to raise_error(CastStringAttribute::InputNotAString)
    end

    it 'returns an empty array when passed nil' do
      expect(CastStringAttribute.array_of_integers(nil)).to eq([])
    end

    it 'returns an array of integers without nil values' do
      expect(CastStringAttribute.array_of_integers(['1', '', '2', nil, '3'])).to eq([1,2,3])
    end
  end
end