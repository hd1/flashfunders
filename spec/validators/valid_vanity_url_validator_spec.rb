require 'rails_helper'

describe ValidVanityUrlValidator do

  subject(:vanity_url_validator) { ValidVanityUrlValidator.new(attributes: {foo: 'bar'}) }

  let(:model) { mock_model('Offering')}

  describe '#validate_each' do
    it 'adds an error if the vanity url matches an existing rails route' do
      vanity_url_validator.validate_each(model, :vanity_url, 'users')
      expect(model.errors[:vanity_url]).to_not be_blank

      vanity_url_validator.validate_each(model, :vanity_url, 'investment_agreement')
      expect(model.errors[:vanity_url]).to_not be_blank
    end

    it 'allows for a vanity url that is unique and not an existing rails route' do
      vanity_url_validator.validate_each(model, :vanity_url, 'foobarfoo')
      expect(model.errors[:vanity_url]).to be_blank
    end

  end

end
