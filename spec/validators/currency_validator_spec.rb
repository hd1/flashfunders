require 'rails_helper'

class TestModel
  include ActiveModel::Validations

  def initialize(attributes = {})
    @attributes = attributes
  end

  def read_attribute_for_validation(key)
    @attributes[key]
  end
end

class TestWidget < TestModel
  validates :amount, currency: true
end

class TestWidgetWithMessage < TestModel
  validates :amount, currency: { message: 'Some other message.' }
end

class TestWidgetAllowsNil < TestModel
  validates :amount, currency: {allow_nil: true}
end

class TestWidgetAllowsNilFalse < TestModel
  validates :amount, currency: {allow_nil: false}
end


describe CurrencyValidator do

  describe 'validation' do
    context 'when value is parsable by Currency' do
      let(:currency) { double('Currency') }
      before { allow(Currency).to receive(:parse).and_return(currency) }

      it 'is valid' do
        expect(TestWidget.new(amount: 100)).to be_valid
      end
    end

    context 'when value is not parsable by Currency' do
      it 'is not valid' do
        expect(TestWidget.new(amount: 'blah')).to_not be_valid
      end
    end
  end

  describe 'error messages' do
    context 'when message is not defined' do
      subject { TestWidget.new(amount: 'invalid') }
      before { subject.valid? }

      it 'adds the default message' do
        expect(subject.errors[:amount]).to include("is invalid")
      end
    end

    context 'when a custom message is defined' do
      subject { TestWidgetWithMessage.new(amount: 'invalid') }
      before { subject.valid? }

      it 'adds the custom message' do
        expect(subject.errors[:amount]).to include("Some other message.")
      end
    end
  end

  describe "allowing nil currency" do
    it "should not be valid when :allow_nil option is missing" do
      expect(TestWidget.new(amount: nil)).to_not be_valid
    end

    it "should be valid when :allow_nil options is set to true" do
      expect(TestWidgetAllowsNil.new(amount: nil)).to be_valid
    end

    it "should not be valid when :allow_nil option is set to false" do
      expect(TestWidgetAllowsNilFalse.new(amount: nil)).to_not be_valid
    end
  end

end

