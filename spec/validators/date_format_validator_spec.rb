require 'rails_helper'

describe DateFormatValidator do
  subject(:date_format_validator) { DateFormatValidator.new({ attributes: { foo: :bar } }) }
  let(:model) { mock_model('ThingWithDate') }

  describe '#validate_each' do
    it 'can be valid' do
      date_format_validator.validate_each(model, :date_attribute, '01/02/1970')
      expect(model.errors).to be_empty
    end

    it 'is valid if nil' do
      date_format_validator.validate_each(model, :date_attribute, nil)
      expect(model.errors).to be_empty
    end

    it 'warns of an incorrect date string format' do
      date_format_validator.validate_each(model, :date_attribute, '1970/01/02')
      expect(model.errors[:date_attribute]).to include('Provide date in mm/dd/yyyy format.')
    end

    it 'warns of values that do not represent a legit date' do
      date_format_validator.validate_each(model, :date_attribute, '02/29/1970')
      expect(model.errors[:date_attribute]).to include('Is not an actual calendar date.')
    end
  end
end

