require 'rails_helper'

describe ZipCodeFormatValidator do

  subject(:zip_code_validator) { ZipCodeFormatValidator.new(attributes: {foo: 'bar'}) }

  let(:model) { mock_model('SomeModel')}

  describe '#validate_each' do
    it 'allows zip code of 5 numeric digits' do
      zip_code_validator.validate_each(model, :zip_code, '12345')

      expect(model.errors[:zip_code]).to be_blank
    end

    it 'allows a zip code of 5 numeric digits plus 4 additional ones' do
      zip_code_validator.validate_each(model, :zip_code, '12345-6789')

      expect(model.errors[:zip_code]).to be_blank
    end

    it 'adds an error if fewer than 5 digits' do

      zip_code_validator.validate_each(model, :zip_code, '1234')

      expect(model.errors[:zip_code]).to_not be_blank
    end

    it 'adds an error if fewer than 4 extended digits' do
      zip_code_validator.validate_each(model, :zip_code, '12345-123')

      expect(model.errors[:zip_code]).to_not be_blank
    end

    it 'adds an error if any non-numeric digits are present' do
      zip_code_validator.validate_each(model, :zip_code, '12A45')

      expect(model.errors[:zip_code]).to_not be_blank
    end
  end
end
