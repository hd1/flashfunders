require 'rails_helper'

describe WebUrlFormatValidator do
  describe '#validate_each' do

    subject(:web_url_format_validator) { WebUrlFormatValidator.new(attributes: {foo: 'bar'}) }
    let(:model) { mock_model('SomeModel')}

    it 'adds errors for invalid URLs' do
      web_url_format_validator.validate_each(model, :website_url, 'boom!')

      expect(model.errors[:website_url]).to include 'Provide URL in http://example.com format.'
    end

    it 'adds errors for non-http urls' do
      web_url_format_validator.validate_each(model, :website_url, 'http://broniestumblog.example.com')
      expect(model.errors[:website_url]).to_not include 'Provide a valid URL in these schemes: http, https.'

      web_url_format_validator.validate_each(model, :website_url, 'https://broniestumblog.example.com')
      expect(model.errors[:website_url]).to_not include 'Provide a valid URL in these schemes: http, https.'

      web_url_format_validator.validate_each(model, :website_url, 'ftp://broniestumblog.example.com')
      expect(model.errors[:website_url]).to include 'Provide a valid URL in these schemes: http, https.'
    end

    it 'does not add errors to blank URLs' do
      web_url_format_validator.validate_each(model, :website_url, '')
      expect(model.errors[:website_url]).to be_empty
    end

    it 'adds an error if url is not complete' do
      web_url_format_validator.validate_each(model, :website_url, 'http:')

      expect(model.errors[:website_url]).to include 'Missing URL domain'
    end

    it 'does not add errors for valid urls' do
      web_url_format_validator.validate_each(model, :website_url, 'http://broniestumblog.example.com')
      expect(model.errors[:website_url]).to be_empty

      web_url_format_validator.validate_each(model, :website_url, 'https://broniestumblog.example.com')
      expect(model.errors[:website_url]).to be_empty
    end
  end
end
