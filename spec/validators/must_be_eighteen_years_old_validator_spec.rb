require 'rails_helper'

describe MustBeEighteenYearsOldValidator do
  describe '#validate_each' do
    subject(:validator) { MustBeEighteenYearsOldValidator.new(attributes: {foo: 'barney'}) }
    let(:model) { mock_model('SomethingWithADate') }

    it 'can be valid' do
      valid_birthdate = (Date.today - 18.years).strftime("%m/%d/%Y")

      validator.validate_each(model, :date_of_birth, valid_birthdate)
      expect(model.errors[:date_of_birth]).to eq([])
    end

    it 'is valid if nil' do
      validator.validate_each(model, :date_of_birth, nil)
      expect(model.errors).to be_empty
    end

    it 'is valid if the date is not in the format MM/DD/YYYY' do
      validator.validate_each(model, :date_of_birth, '2012-01-24')
      expect(model.errors).to be_empty
    end

    it 'is invalid if date is less than 18 years ago' do
      under_18_birthdate = (Date.today - 15.years).strftime("%m/%d/%Y")

      validator.validate_each(model, :date_of_birth, under_18_birthdate)
      expect(model.errors[:date_of_birth]).to include('Must be 18 years old.')
    end
  end
end