describe('FF.NumberRounder', function () {

  describe('#roundToThousandths', function () {
    it('works on numbers that do not have decimals', function () {
      expect(new FF.NumberRounder(8).roundToThousandths()).toBeCloseTo(8.000,3);
    });

    it('rounds up correctly', function () {
      expect(new FF.NumberRounder(8.5278).roundToThousandths()).toEqual(8.528);
      expect(new FF.NumberRounder(8.5275).roundToThousandths()).toEqual(8.528);
    });

    it('rounds down correctly', function () {
      expect(new FF.NumberRounder(8.5274).roundToThousandths()).toEqual(8.527);
    });
  });
});