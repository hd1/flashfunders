describe('FF.VideoPlayer', function () {
  var player, element, media;

  beforeEach(function () {
    element = $('<div id="video-player-container"/>');
    media = $('<div id="cover-media"/>').append($('<img>')).append($('<div class="play-button"/>'));
    element.append(media);
    $('#jasmine_content').append(element);
  });

  afterEach(function () {
    $('#jasmine_content').html('');
  });

  it('replaces the image with the video when the user clicks play', function () {
    player = new FF.VideoPlayer(
      {
        videoURL: '//player.example.com/video/26514212',
        videoHeight: 440,
        videoWidth: 1000
      });

    expect(element.find('img').length).toBeGreaterThan(0);

    element.find('.play-button').click();

    expect(element.find('img').length).toEqual(0);

    var videoFrame = element.find('iframe');
    expect(videoFrame.attr('src')).toEqual('//player.example.com/video/26514212?&autoplay=1');
    expect(videoFrame.attr('height')).toEqual('440');
    expect(videoFrame.attr('width')).toEqual('1000');
  });
});