describe('$', function () {
  describe('maxCheckedInputs', function () {
    it('disables the other checkboxes when the max number is selected', function () {
      var $html = $('<input id="a" type="checkbox" checked="checked" class="foo"/><input id="b" type="checkbox" class="foo"/><input id="c" type="checkbox" name="checky" class="foo"/>');

      $('#jasmine_content').html($html);

      $("input:checkbox").maxChecked(2);

      expect($('input:checked').size()).toEqual(1);

      $('#b').click();

      expect($('input:checked').size()).toEqual(2);

      expect($('#c').prop('disabled')).toEqual(true);

      $('#b').click();

      expect($('#c').prop('disabled')).toEqual(false);

      $('#c').click();

      expect($('#b').prop('disabled')).toEqual(true);
    });

    it('disables the other checkboxes when the max number is selected initially', function(){
      var $html = $('<input id="a" type="checkbox" checked="checked" class="foo"/><input id="b" checked="checked" type="checkbox" class="foo"/><input id="c" type="checkbox" name="checky" class="foo"/>');

      $('#jasmine_content').html($html);

      $("input:checkbox").maxChecked(2);

      expect($('#c').prop('disabled')).toEqual(true);
    });

    it('returns itself for chaining', function () {
      var $target = $('#jasmine_content');
      expect($target.maxChecked(3)).toEqual($target);
    });
  });


});

