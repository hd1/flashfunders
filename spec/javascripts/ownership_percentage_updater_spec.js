describe('$', function () {
  describe('ownershipPercentageUpdater', function () {
    it('returns itself for chaining', function () {
      var $target = $('#jasmine_content');
      expect($target.ownershipPercentageUpdater()).toEqual($target);
    });

    it('updates the target with ownership percentage from the fields specified', function () {
      var html = '<span id="ownership_percentage_display" data-pre-money-valuation-selector="#pre-money" data-campaign-goal-selector="#goal"/>' +
        '<input type="text" name="pre-money" id="pre-money"/><input type="text" name="goal" id="goal"/>';

      $('#jasmine_content').html(html);

      $('#ownership_percentage_display').ownershipPercentageUpdater();

      expect($('#ownership_percentage_display').html()).toEqual('');

      $('#pre-money').val(12345).trigger('keyup');

      expect($('#ownership_percentage_display').html()).toEqual('');

      $('#goal').val(4354).trigger('keyup');

      expect($('#ownership_percentage_display').html()).toEqual("You're offering <div class=\"side-info--info h1\">26.07%</div>");

      $('#goal').val('').trigger('keyup');

      expect($('#ownership_percentage_display').html()).toEqual('');
    });

    it('renders the offering percent immediately if values are present', function () {
      var html = '<span id="ownership_percentage_display" data-pre-money-valuation-selector="#pre-money" data-campaign-goal-selector="#goal"/>' +
        '<input value="12345" type="text" name="pre-money" id="pre-money"/><input value="4354" type="text" name="goal" id="goal"/>';

      $('#jasmine_content').html(html);

      $('#ownership_percentage_display').ownershipPercentageUpdater();

      expect($('#ownership_percentage_display').html()).toEqual("You're offering <div class=\"side-info--info h1\">26.07%</div>");
    });
  });
});