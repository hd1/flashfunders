describe('$', function () {
  describe('socialSharingPopup', function () {

    beforeEach(function () {
      $('#jasmine_content').html($('<div id="social_things">' +
        '<script>var ga = function(){};</script>' +
        '<a id="one_social_thing" class="social-share" href="https://example.com/url_with_sharing_params?share=me" target="_blank">Share</a>' +
        '<a id="two_social_thing" class="social-share" href="https://example.com/wat_is_happen?" target="_blank">Share</a>' +
        '</div>'));
      $('.social-share').socialSharingPopup();

      spyOn(window, 'open');
    });

    it('opens a new share window when a social-share is clicked', function () {
      spyOn(FF, 'CalculatePopupPosition').and.returnValue({ left: 434, top: 372 });

      var popupSettings = 'toolbar=no, menubar=no, scrollbars=no, resizable=no, location=no, directories=no, status=no';

      $('#one_social_thing').click();
      expect(window.open.calls.mostRecent().args).toEqual(['https://example.com/url_with_sharing_params?share=me', '', 'width=640, height=440, left=434, top=372' + popupSettings]);

      $('#two_social_thing').click();
      expect(window.open.calls.mostRecent().args).toEqual(['https://example.com/wat_is_happen?', '', 'width=640, height=440, left=434, top=372' + popupSettings]);
    });

    it('prevents default on the anchor', function () {
      var event = jQuery.Event('click');
      spyOn(event, 'preventDefault');

      $('#one_social_thing').trigger(event);
      expect(event.preventDefault).toHaveBeenCalled();
    });
  });
});