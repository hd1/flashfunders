function triggerKeydownOnContainer(key, $container) {
    var event = jQuery.Event('keydown');
    var keyCode;

    switch(key) {
        case 'tab':         keyCode = 9; break;
        case 'return':      keyCode = 13; break;
        case 'escape':      keyCode = 27; break;
        case 'spacebar':    keyCode = 32; break;
        case 'up arrow':    keyCode = 38; break;
        case 'down arrow':  keyCode = 40; break;
    }

    event.which = keyCode;

    $container.trigger(event);
}

describe('$', function () {
  describe('customBinaryRadioButtons', function () {
    var $yesBox, $noBox;

    beforeEach(function () {
      $yesBox = $('<span class="radio"><label><input tabindex="2" type="radio" name="foo_field" value="yes"/>Yes</label></span>');
      $noBox = $('<span class="radio"><label><input tabindex="2" type="radio" name="foo_field" value="no"/>No</label></span>');
    });

    it('returns itself for chaining', function () {
      var $element = $('#jasmine_content');
      expect($element.customBinaryRadioButtons()).toEqual($element);
    });

    it('changes the selection on the input when the custom button is clicked', function () {
      $('#jasmine_content').append($yesBox);
      $('#jasmine_content').append($noBox);

      $('#jasmine_content').customBinaryRadioButtons();

      expect($('input').is(':checked')).toEqual(false);

      $('[data-custom-radio-input-value="yes"]').click();

      expect($('input[value="yes"]').is(':checked')).toEqual(true);

      $('[data-custom-radio-input-value="no"]').click();

      expect($('input[value="no"]').is(':checked')).toEqual(true);
      expect($('input[value="yes"]').is(':checked')).toEqual(false);

      $('[data-custom-radio-input-value="yes"]').click();

      expect($('input[value="yes"]').is(':checked')).toEqual(true);
      expect($('input[value="no"]').is(':checked')).toEqual(false);

    });

    it('adds the class active to the radio button that is clicked, and removes it from the other', function () {
      $('#jasmine_content').append($yesBox);
      $('#jasmine_content').append($noBox);

      $('#jasmine_content').customBinaryRadioButtons();


      expect($('[data-custom-radio-group] [data-custom-radio-input-name="foo_field"][data-custom-radio-input-value="yes"]').size()).toEqual(1);
      expect($('[data-custom-radio-group] [data-custom-radio-input-name="foo_field"][data-custom-radio-input-value="no"]').size()).toEqual(1);

      expect($('[data-custom-radio-group] .active').size()).toEqual(0);

      $('[data-custom-radio-input-value="yes"]').click();

      expect($('[data-custom-radio-input-value="yes"]').hasClass('active')).toEqual(true);

      $('[data-custom-radio-input-value="no"]').click();

      expect($('[data-custom-radio-input-value="yes"]').hasClass('active')).toEqual(false);
      expect($('[data-custom-radio-input-value="no"]').hasClass('active')).toEqual(true);
    });

    it('uses the tabindex from the origin input and then removes from original', function () {
        $('#jasmine_content').append('<div tabindex="1">Foo</div>').append($yesBox);
        $('#jasmine_content').append($noBox).append('<div tabindex="3">Bar</div>');

        $('#jasmine_content').customBinaryRadioButtons();

        expect($('.custom-radio-input').attr('tabindex')).toEqual('2');
        $('input[type="radio"]').each(function () {
            expect($(this).attr('tabindex')).not.toBeDefined();
        })
    });

    it('selects the radio choice when focused and hitting spacebar', function () {
        $('#jasmine_content').append('<div tabindex="1">Foo</div>').append($yesBox);
        $('#jasmine_content').append($noBox).append('<div tabindex="3">Bar</div>');

        $('#jasmine_content').customBinaryRadioButtons();

        var $target = $('.custom-radio-input:first');
        $target.focus();
        triggerKeydownOnContainer('spacebar', $target);
        expect($target.hasClass('active')).toEqual(true);
    });

    it('adds the custom buttons after the parent of the last input item, and hides original inputs', function () {
      $('#jasmine_content').append($yesBox);
      $('#jasmine_content').append($noBox);

      $('#jasmine_content').customBinaryRadioButtons();

      expect($('.radio label').hasClass('replaced-by-custom-radio-button')).toEqual(true);
      expect($('.radio label + div[data-custom-radio-group]').size()).toEqual(1);
    });

    it('also works if called on a collection of radio buttons', function () {
      $('#jasmine_content').append($yesBox);
      $('#jasmine_content').append($noBox);

      $('#jasmine_content input[type="radio"]').customBinaryRadioButtons();

      expect($('[data-custom-radio-group] [data-custom-radio-input-name="foo_field"][data-custom-radio-input-value="yes"]').size()).toEqual(1);
      expect($('[data-custom-radio-group] [data-custom-radio-input-name="foo_field"][data-custom-radio-input-value="no"]').size()).toEqual(1);
      expect($('.radio label').hasClass('replaced-by-custom-radio-button')).toEqual(true);
    });

    it('sets the text of the custom button to equal the label text of the original item', function () {
      $yesBox = $('<span class="radio"><label><input tabindex="2" type="radio" name="foo_field" value="true"/>Yes</label></span>');
      $noBox = $('<span class="radio"><label><input tabindex="2" type="radio" name="foo_field" value="false"/>No</label></span>');

      $('#jasmine_content').append($yesBox);
      $('#jasmine_content').append($noBox);

      $('#jasmine_content').customBinaryRadioButtons();

      var $customYes = $('#jasmine_content').find('span.custom-radio-input[data-custom-radio-input-value="true"]');
      expect($customYes.text()).toEqual('Yes');

      var $customNo = $('#jasmine_content').find('span.custom-radio-input[data-custom-radio-input-value="false"]');
      expect($customNo.text()).toEqual('No');
    });

    it('sets the custom button as "active" if the input it replaces is checked', function () {
      $yesBox = $('<span class="radio"><label><input tabindex="2" type="radio" name="foo_field" value="true"/>Yes</label></span>');
      $noBox = $('<span class="radio"><label><input tabindex="2" type="radio" name="foo_field" value="false" checked="checked"/>No</label></span>');

      $('#jasmine_content').append($yesBox);
      $('#jasmine_content').append($noBox);

      $('#jasmine_content').customBinaryRadioButtons();

      var $customNo = $('#jasmine_content').find('span.custom-radio-input[data-custom-radio-input-value="false"]');
      expect($customNo.hasClass('active')).toEqual(true);
    });
  });
});

