describe('$', function () {
  var $rightRatioFilelists, $fileLists;

  var getFileBlob = function (url, cb) {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url);
    xhr.responseType = "arraybuffer";
    xhr.addEventListener('load', function() {
      cb(xhr.response, xhr.getResponseHeader('Content-Type'));
    });
    xhr.send();
  };

  var blobToFile = function (response, contentType, name) {
    var arrayBuffer = new Uint8Array(response);
    var blob = new Blob([arrayBuffer], { type: contentType });

    blob.lastModifiedDate = new Date();
    blob.name = name;
    return blob;
  };

  var getFileObject = function(filePathOrUrl, cb) {
    getFileBlob(filePathOrUrl, function (response, contentType) {
      cb(blobToFile(response, contentType, 'filename.extension'));
    });
  };

  describe('uploadable', function () {
    var event = jQuery.Event('change'), $imageContent, $fileChooser, $imageFieldset;

    beforeEach(function() {
      $imageContent = $("<input type='file' name='file-chooser' />" +
        "<input type='hidden' name='key' class='file-key' />" +
        "<div id='clickme' class='btn-add-file uploadable'>" +
          "<div class='btn-overlay'>Update</div>" +
          "<div class='file-content'></div>" +
        "</div>");

      $imageFieldset = $("<fieldset></fieldset>").append($imageContent);
      $('#jasmine_content').append($imageFieldset);
      $fileChooser = $imageContent.prevAll('input:file').first();
      event.target = $fileChooser;
    });

    describe('valid ratio image', function(){
      beforeEach(function(done) {
        getFileObject('spec/fixtures/browse_image.jpg', function(fileObject) {
          $rightRatioFilelists = [fileObject];
          done();
        });
      });

      beforeEach(function(){
        $('.uploadable').uploadable({displayType: 'image', validateRatio: [740,330]});
        event.target.files = $rightRatioFilelists;
        $fileChooser.trigger(event);
      });

      // Waiting for $fileChooser.trigger(event)
      beforeEach(function(done){
        setTimeout(function() {
          done();
        }, 100);
      });

      it('uploads a valid ratio image and remove error message if any', function() {
        var errorMessage = $imageFieldset.children('.upload-image-error').text();
        expect(errorMessage).not.toBe('Your image file should have an aspect ratio of 74:33 and be at least 740px wide.');
      });
    });

    describe('invalid ratio image without validation', function(){
      beforeEach(function(done) {
        getFileObject('assets/logo.png', function(fileObject) {
          $fileLists = [fileObject];
          done();
        });
      });

      beforeEach(function(){
        $('.uploadable').uploadable({displayType: 'image'});
        event.target.files = $fileLists;
        $fileChooser.trigger(event);
      });

      it('uploads a non-browse image', function() {
        var errorMessage = $imageFieldset.children('.upload-image-error').text();
        expect(errorMessage).not.toBe('Your image file should have an aspect ratio of 74:33 and be at least 740px wide.');
      });
    });

    describe('invalid ratio image with validation', function(){
      beforeEach(function(done) {
        getFileObject('assets/logo.png', function(fileObject) {
          $fileLists = [fileObject];
          done();
        });
      });

      beforeEach(function(){
        $('.uploadable').uploadable({displayType: 'image', validateRatio: [770,330]});
        event.target.files = $fileLists;

        $fileChooser.trigger(event);
      });

      // Waiting for $fileChooser.trigger(event)
      beforeEach(function(done){
        setTimeout(function() {
          done();
        }, 100);
      });

      it('uploads a non-browse image', function() {
        var errorMessage = $imageFieldset.children('.upload-image-error').text();
        expect(errorMessage).toBe('Your image file should have an aspect ratio of 7:3 and be at least 770px wide.');
      });
    });

    describe('invalid minimum width image and with validation', function(){
      beforeEach(function(done) {
        getFileObject('assets/ico-twitter.png', function(fileObject) {
          $fileLists = [fileObject];
          done();
        });
      });

      beforeEach(function(){
        $('.uploadable').uploadable({displayType: 'image', validateRatio: [40,40]});
        event.target.files = $fileLists;

        $fileChooser.trigger(event);
      });

      // Waiting for $fileChooser.trigger(event)
      beforeEach(function(done){
        setTimeout(function() {
          done();
        }, 100);
      });

      it('uploads a non-browse image', function() {
        var errorMessage = $imageFieldset.children('.upload-image-error').text();
        expect(errorMessage).toBe('Your image file should have an aspect ratio of 1:1 and be at least 40px wide.');
      });
    });

  });
});
