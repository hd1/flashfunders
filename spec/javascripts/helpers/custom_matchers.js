CustomMatchers = {
  toBeInDom: function() {
    return {
      compare: function (actual, expected) {
        var result = actual.closest('html').size() !== 0;

        return {
          pass: result
        };
      }
    };
  }
};