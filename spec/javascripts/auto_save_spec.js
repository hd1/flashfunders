describe('$', function () {
  describe('autoSave', function () {

    it('returns the jQuery object for chaining', function () {
      var $jasmineContent = $('#jasmine_content');

      expect($jasmineContent.autoSave()).toEqual($jasmineContent);
    });

    describe('posting the form data to the correct url', function () {
      it('when a text field is blurred', function () {
        var $form = $('<form action="/save/me">' +
          '<input type="text" name="some_name" />' +
          '</form>');

        $('#jasmine_content').append($form);

        spyOn($, 'ajax');

        $('form').autoSave();

        $('[name="some_name"]').val('hello');
        $('[name="some_name"]').trigger('blur');

        expect($.ajax).toHaveBeenCalledWith('/save/me', {
          data: 'some_name=hello',
          dataType: 'json',
          type: 'POST'
        });
      });

      it('when a email field is blurred', function () {
        var $form = $('<form action="/save/me">' +
          '<input type="email" name="some_name" />' +
          '</form>');

        $('#jasmine_content').append($form);

        spyOn($, 'ajax');

        $('form').autoSave();

        $('[name="some_name"]').val('hello');
        $('[name="some_name"]').trigger('blur');

        expect($.ajax).toHaveBeenCalledWith('/save/me', {
          data: 'some_name=hello',
          dataType: 'json',
          type: 'POST'
        });
      });

      it('when a url field is blurred', function () {
        var $form = $('<form action="/save/me">' +
          '<input type="url" name="some_name" />' +
          '</form>');

        $('#jasmine_content').append($form);

        spyOn($, 'ajax');

        $('form').autoSave();

        $('[name="some_name"]').val('hello');
        $('[name="some_name"]').trigger('blur');

        expect($.ajax).toHaveBeenCalledWith('/save/me', {
          data: 'some_name=hello',
          dataType: 'json',
          type: 'POST'
        });
      });

      it('when a texarea is blurred', function () {
        var $form = $('<form action="/save/me">' +
          '<textarea name="some_name">hello</textarea>' +
          '</form>');

        $('#jasmine_content').append($form);

        spyOn($, 'ajax');

        $('form').autoSave();

        $('[name="some_name"]').trigger('blur');

        expect($.ajax).toHaveBeenCalledWith('/save/me', {
          data: 'some_name=hello',
          dataType: 'json',
          type: 'POST'
        });
      });

      it('when a number type is blurred', function () {
        var $form = $('<form action="/save/me">' +
          '<input type="number" name="some_number" />' +
          '</form>');

        $('#jasmine_content').append($form);

        spyOn($, 'ajax');

        $('form').autoSave();

        $('[name="some_number"]').val('123');
        $('[name="some_number"]').trigger('blur');

        expect($.ajax).toHaveBeenCalledWith('/save/me', {
          data: 'some_number=123',
          dataType: 'json',
          type: 'POST'
        });
      });

      it('when a checkbox is checked', function () {
        var $form = $('<form action="/save/me">' +
          '<input type="checkbox" name="some_name" value="hello"/>' +
          '</form>');

        $('#jasmine_content').append($form);

        spyOn($, 'ajax');

        $('form').autoSave();

        $('[name="some_name"]').click();
        $('[name="some_name"]').trigger('change');

        expect($.ajax).toHaveBeenCalledWith('/save/me', {
          data: 'some_name=hello',
          dataType: 'json',
          type: 'POST'
        });
      });

      it('when a select box is blurred', function () {
        var $form = $('<form action="/save/me">' +
          '<select name="some_name">' +
          '<option value="hello" selected/>' +
          '</input>' +
          '</form>');

        $('#jasmine_content').append($form);

        spyOn($, 'ajax');

        $('form').autoSave();

        $('[name="some_name"]').trigger('blur');

        expect($.ajax).toHaveBeenCalledWith('/save/me', {
          data: 'some_name=hello',
          dataType: 'json',
          type: 'POST'
        });
      });
    });
  });
});