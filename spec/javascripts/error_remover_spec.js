describe('$', function () {
  var $textFieldWithErrors, $checkboxFieldWithErrors, $radioButtonFieldWithErrors, $selectWithErrors;

  beforeEach(function () {
    $textFieldWithErrors = $('<form><div class="input field_with_errors"><input type="text" name="gar_field"/><span class="error">You did it wrong</span></div></form>');
    $checkboxFieldWithErrors = $('<form><div class="input"><input type="checkbox" name="gar_field"/><span class="error">You did it wrong</span></div></form>');
    $radioButtonFieldWithErrors = $('<form><div class="input"><input type="radio" name="gar_field"/><span class="error">You did it wrong</span></div></form>');
    $selectWithErrors = $('<form><div class="input"><select><option>hello</option></select><span class="error">You did it wrong</span></div></form>');
  });

  describe('#errorRemover', function () {
    it('returns itself', function () {
      $('#jasmine_content').append($textFieldWithErrors);
      expect($textFieldWithErrors.errorRemover()).toEqual($textFieldWithErrors);
    });

    it('removes the error class and span from a focused input field', function () {
      $('#jasmine_content').append($textFieldWithErrors);

      $('form').errorRemover();

      $('input[name="gar_field"]').trigger('focus');

      expect($textFieldWithErrors.find('div').hasClass('field_with_errors')).toEqual(false);
      expect($textFieldWithErrors.find('span.error').hasClass('invisible')).toEqual(true);
    });

    it('removes the error class and span for checkboxes when changed', function () {
      $('#jasmine_content').append($checkboxFieldWithErrors);

      $('form').errorRemover();

      $('input[name="gar_field"]').trigger('change');

      expect($checkboxFieldWithErrors.find('span.error').hasClass('invisible')).toEqual(true);
    });

    it('removes the error class and span for radio buttons when changed', function () {
      $('#jasmine_content').append($radioButtonFieldWithErrors);

      $('form').errorRemover();

      $('input[name="gar_field"]').trigger('change');

      expect($radioButtonFieldWithErrors.find('span.error').hasClass('invisible')).toEqual(true);
    });

    it('removes the error class and span for custom select boxes when clicked', function () {
      $('#jasmine_content').append($selectWithErrors);

      $('form').errorRemover();

      $('select', $selectWithErrors).triggerHandler('focus');

      expect($selectWithErrors.find('span.error').hasClass('invisible')).toEqual(true);
    });
  });
});
