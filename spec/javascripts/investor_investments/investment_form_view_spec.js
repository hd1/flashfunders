describe('FF.InvestmentStockFormView', function () {
  var view;
  describe('render', function () {
    function createView() {
      return new FF.InvestmentStockFormView({
        $containerEl: $('#jasmine_content'),
        updateDelay: 0,
        model: new Backbone.Model({
          offering_id: 34,
          minimum_investment_amount: 3,
          investment_amount: 327,
          investment_shares: 3,
          investment_percent_ownership: 87.645,
          offering_model: "model_direct",
          direct_investment_threshold: "",
          formatted_direct_investment_threshold: "",
          company_name: "",
          errors: {},
          form_action: '/foo/bar'
        })
      });
    }

    it('renders the form', function () {
      view = createView();

      view.render();

      expect(view.$('#investment_unrounded_investment_amount')).toBeInDom();
    });

    it('puts the authenticity token in the form', function () {
      var tokenName = $('<meta content="authenticity_token" name="csrf-param">');
      var tokenVal = $('<meta content="lionstigersbearsohmy" name="csrf-token">');

      $('head').append(tokenName);
      $('head').append(tokenVal);

      view = createView();
      view.render();

      expect(view.$('input[name=authenticity_token]').val()).toEqual('lionstigersbearsohmy')
    });

    describe('error messages', function () {
      describe('when present', function () {
        it('are displayed below the input and the input container gets classed correctly', function () {
          var error_message = 'Provide investment amount in 1000.00 format.';
          view = createView();
          view.model.set('errors', {'unrounded_investment_amount': [error_message]});
          view.render();

          expect(view.$('.input.investment_unrounded_investment_amount').hasClass('field_with_errors')).toBeTruthy();
          expect(view.$('span.error').html()).toEqual(error_message);
        });
      });

      describe('when empty', function () {
        it('does not add the field_with_errors class', function () {
          view = createView();
          view.render();

          expect(view.$('.input.investment_unrounded_investment_amount').hasClass('field_with_errors')).toEqual(false);
          expect(view.$('span.error').html()).toEqual('');
        });
      })
    });
  });
});