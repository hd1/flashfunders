describe('CalculatePopupPosition', function () {
  it('calculates the left and top position', function () {
    var result = FF.CalculatePopupPosition(600, 400, 300, 200, 0, 0);
    expect(result.top).toEqual(100);
    expect(result.left).toEqual(150);
  });

  it('floors numbers that are decimals', function () {
    var result = FF.CalculatePopupPosition(600, 400, 301, 201, 0, 0);
    expect(result.top).toEqual(99);
    expect(result.left).toEqual(149);
  });

  it('adds the x and y offset to popup position', function () {
    var result = FF.CalculatePopupPosition(600, 400, 301, 201, 34, 35);
    expect(result.top).toEqual(134);
    expect(result.left).toEqual(183);
  });
});