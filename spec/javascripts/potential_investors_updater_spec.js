describe('$', function () {
  describe('potentialInvestorsUpdater', function () {
    it('returns itself for chaining', function () {
      var $target = $('#jasmine_content');
      expect($target.potentialInvestorsUpdater()).toEqual($target);
    });

    it('updates the target with potential number of investors from the fields specified', function () {
      var html = '<span id="potential-investors" data-minimum-investment-amount-selector="#minimum-investment" data-campaign-goal-selector="#goal"/>' +
        '<input type="text" id="minimum-investment"/><input type="text" id="goal"/>';

      $('#jasmine_content').html(html);

      $('#potential-investors').potentialInvestorsUpdater();

      expect($('#potential-investors').html()).toEqual('');

      $('#minimum-investment').val(123).trigger('keyup');

      expect($('#potential-investors').html()).toEqual('');

      $('#goal').val(4354).trigger('keyup');

      expect($('#potential-investors').html()).toEqual('Potential # of Investors <div class="side-info--info h1">35</div>');

      $('#goal').val('').trigger('keyup');

      expect($('#potential-investors').html()).toEqual('');
    });

    it('renders the number of investors immediately if values are present', function () {
      var html = '<span id="potential-investors" data-minimum-investment-amount-selector="#minimum-investment" data-campaign-goal-selector="#goal"/>' +
        '<input type="text" id="minimum-investment" value="123"/><input type="text" id="goal" value="4354"/>';

      $('#jasmine_content').html(html);

      $('#potential-investors').potentialInvestorsUpdater();

      expect($('#potential-investors').html()).toEqual('Potential # of Investors <div class="side-info--info h1">35</div>');
    });
  });
});