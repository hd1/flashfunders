require "codeclimate-test-reporter"
CodeClimate::TestReporter.start

ENV["RAILS_ENV"] ||= 'test'
require File.expand_path("../../config/environment", __FILE__)
require 'rspec/rails'
require 'capybara/rails'
require 'sidekiq/testing'

Dir[Rails.root.join("spec/support/**/*.rb")].each { |f| require f }
Dir[Rails.root.join("spec/shared/**/*.rb")].each { |f| require f }

ActiveRecord::Migration.maintain_test_schema!

RSpec.configure do |config|

  config.infer_spec_type_from_file_location!
  config.order = "random"
  config.include ObjectCreationMethods
  config.include FeatureSpecHelpers, type: :feature
  config.include ControllerSpecHelpers, type: :controller
  config.include Devise::TestHelpers, type: :controller
  config.include FactoryGirl::Syntax::Methods
  config.include WaitForAjax, type: :feature

  config.before(:each) do |example|
    $redis.flushall
    if example.metadata[:type] == :feature
      Capybara.current_driver = :webkit

      page.driver.block_unknown_urls
      page.driver.allow_url '*.js'
      page.driver.allow_url '*.css'

      DatabaseCleaner.strategy = :truncation
    else
      Capybara.use_default_driver
      DatabaseCleaner.strategy = :transaction
    end
  end

  config.before(:suite) do
    DatabaseCleaner.clean_with(:truncation)
  end

  config.after(:each) do |example|
    if example.metadata[:type] == :feature
      DatabaseCleaner.strategy = :truncation
    end
  end

  config.before(:each) do
    DatabaseCleaner.start
  end

  config.before(:each, :stub_warden) { Warden.test_mode! }
  config.after(:each,  :stub_warden) { Warden.test_reset! }

  config.after(:each) do
    DatabaseCleaner.clean
  end

  config.before(:suite) do
    Percy.config.access_token = ENV['PERCY_TOKEN']
    Percy::Capybara.initialize_build
  end

  config.after(:suite) do
    Percy::Capybara.finalize_build
  end
end

require 'vcr'
VCR.configure do |c|
  c.cassette_library_dir = 'spec/fixtures/vcr_cassettes'
  c.hook_into :webmock
  c.ignore_localhost = true
  c.ignore_hosts 'codeclimate.com', 'percy.io'

  # use the actual test API Key when you want to record new cassettes.
  c.filter_sensitive_data('<FUND_AMERICA_API_KEY>') { FundAmerica::Config.configuration.api_keys[:default] }
  c.filter_sensitive_data('<FUND_AMERICA_SPV_API_KEY>') { FundAmerica::Config.configuration.api_keys[:spv] }
  c.filter_sensitive_data('<FUND_AMERICA_CF_API_KEY>') { FundAmerica::Config.configuration.api_keys[:reg_c] }

  c.filter_sensitive_data('<FINTECH_CLEARING_API_KEY>') { FintechClearing::Config.configuration.api_keys[:default] }
  c.filter_sensitive_data('<FINTECH_CLEARING_USERNAME>') { FintechClearing::Config.configuration.basic_auth[:email] }
  c.filter_sensitive_data('<FINTECH_CLEARING_PASSWORD>') { FintechClearing::Config.configuration.basic_auth[:password] }

  c.filter_sensitive_data('<IDOLOGY_USERNAME>') { ENV['IDOLOGY_USERNAME'] }
  c.filter_sensitive_data('<IDOLOGY_PASSWORD>') { ENV['IDOLOGY_PASSWORD'] }
end

def verify_accreditation_for(entity_id, day=Date.today)
  Accreditor::Span.create!(investor_entity_id: entity_id, status: :approved, start_date: day - 1, end_date: day + 1)
end
