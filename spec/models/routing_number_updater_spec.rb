require 'rails_helper'
require 'artifice'

# TODO: Removed these tests for two reasons:
# 1) Artifice breaks vcr. When/if we revive these tests, we have to remove artifice
# 2) The routing number updater itself is broken anyway, because the url to fetch
# the file changed. It's not just a straight swap to put the new url in though,
# because they now have an "agree to not steal this data" page that you have to
# click through, so we have to solve that problem.
#
# Also, I'm not deleting this code because we are planning on re-implementing ach very soon.

describe RoutingNumberUpdater do
  describe '.run' do

#     let(:db_connection) do
#       db_config = ActiveRecord::Base.configurations[Rails.env]
#
#       Sequel.postgres(
#         host: db_config['host'],
#         database: db_config['database'],
#         user: db_config['username'],
#         password: db_config['password']
#       )
#     end
#
#     after do
#       Artifice.deactivate
#     end
#
#     context 'the server responds with routing numbers' do
#       before do
#         Artifice.activate_with ->(env) {
#           response = <<-RESPONSE
# 011000015O0110000150020802000000000FEDERAL RESERVE BANK                1000 PEACHTREE ST N.E.              ATLANTA             GA303094470866234568111
# 011000028O0110000151072811000000000STATE STREET BANK AND TRUST COMPANY JAB2NW                              N. QUINCY           MA021710000617664240011
# 011000138O0110000151101310000000000BANK OF AMERICA, N.A.               8001 VILLA PARK DRIVE               HENRICO             VA232280000800446013511
#           RESPONSE
#
#           [200, {}, response]
#         }
#       end
#
#       it 'updates our store of routing numbers' do
#         RoutingNumber.destroy_all
#
#         expect(RoutingNumber.count).to eq(0)
#
#         described_class.run(db_connection)
#
#         expect(RoutingNumber.count).to eq(3)
#
#         routing_number_record = RoutingNumber.first
#
#         expect(routing_number_record.routing_number).to eq('011000015')
#       end
#
#       it 'destroys existing routing numbers' do
#         RoutingNumber.create routing_number: '111111111'
#
#         described_class.run(db_connection)
#
#         expect(RoutingNumber.count).to eq(3)
#       end
#     end
#
#     context 'the server responds with a 200 but the response body is empty' do
#       before do
#         Artifice.activate_with ->(env) {
#           response = ''
#
#           [200, {}, response]
#         }
#       end
#
#       it 'does not create or destroy any records' do
#         RoutingNumber.destroy_all
#
#         RoutingNumber.create routing_number: '111111111'
#
#         described_class.run(db_connection)
#
#         expect(RoutingNumber.count).to eq(1)
#
#         routing_number_record = RoutingNumber.first
#         expect(routing_number_record.routing_number).to eq '111111111'
#       end
#     end
#
#     context 'the server responds with a 200 but the response body is malformed' do
#       before do
#         Artifice.activate_with ->(env) {
#           response = 'This page has been returned in an error'
#
#           [200, {}, response]
#         }
#       end
#
#       it 'does not create or destroy any records' do
#         RoutingNumber.destroy_all
#
#         RoutingNumber.create routing_number: '111111111'
#
#         expect {
#           described_class.run(db_connection)
#         }.to raise_exception(RoutingNumberUpdater::InvalidRoutingNumber)
#
#         expect(RoutingNumber.count).to eq(1)
#
#         routing_number_record = RoutingNumber.first
#         expect(routing_number_record.routing_number).to eq '111111111'
#       end
#     end
#
#     context 'the server has an error fetching routing numbers' do
#       before do
#         Artifice.activate_with ->(env) {
#           [404, {}, '']
#         }
#       end
#
#       it 'keeps old routing numbers if the new ones cannot be fetched' do
#         RoutingNumber.destroy_all
#
#         RoutingNumber.create routing_number: '111111111'
#
#         described_class.run(db_connection)
#
#         expect(RoutingNumber.count).to eq(1)
#
#         routing_number_record = RoutingNumber.first
#         expect(routing_number_record.routing_number).to eq '111111111'
#       end
#     end
  end
end
