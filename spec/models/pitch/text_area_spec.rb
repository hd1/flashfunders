require 'rails_helper'

module Pitch
  describe TextArea do

    let(:attributes) { {body: 'Something'} }

    subject { described_class.new(attributes) }

    it { should be_valid }

    context 'when body is not present' do
      before { subject.body = nil }

      it { should_not be_valid }
    end

  end
end

