require 'rails_helper'

module Pitch
  describe Section do

    let(:attributes) { {title: 'Foo'} }

    subject { described_class.new(attributes) }

    it { should be_valid }

    context 'when title is not present' do
      before { subject.title = nil }

      it { should_not be_valid }
    end

    describe '#content' do
      let(:item_a) { double(ContentItem) }
      let(:item_b) { double(ContentItem) }

      before do
        allow(item_a).to receive(:content).and_return(:content_a)
        allow(item_b).to receive(:content).and_return(:content_b)
      end

      before { allow(subject).to receive(:content_items).and_return([item_a, item_b]) }

      it 'returns an array of content' do
        expect(subject.content).to eql([:content_a, :content_b])
      end

    end

  end
end

