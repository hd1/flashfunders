require 'rails_helper'

module Pitch
  describe ContentItem do

    subject { described_class.new }

    it { should be_valid }

    describe '#before_destroy' do
      let(:content) { double }

      before do
        subject.save!
        allow(subject).to receive(:content).and_return(content)
        allow(content).to receive(:destroy)
      end

      it 'destroys the associated content' do
        subject.destroy

        expect(content).to have_received(:destroy)
      end
    end

    describe '#build_content' do
      it 'sets the content attribute' do
        subject.content_type = 'Pitch::TextArea'
        subject.build_content({})
        expect(subject.content).to be_a(Pitch::TextArea)

        subject.content_type = 'Pitch::Image'
        subject.build_content({})
        expect(subject.content).to be_a(Pitch::Image)

        subject.content_type = 'Pitch::Carousel'
        subject.build_content({})
        expect(subject.content).to be_a(Pitch::Carousel)
      end

      context 'when the content_type is not allowed' do
        it 'raises an error' do
          subject.content_type = 'Object'

          expect{ subject.build_content({}) }.to raise_error
        end
      end

    end

  end
end

