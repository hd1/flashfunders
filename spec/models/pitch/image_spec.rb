require 'rails_helper'

module Pitch
  describe Image do

    let(:attributes) { {key: 'offering/pitch/f4834af0-a447-468d-b0a8-c5337e8a48cb/photo.png'} }

    subject { described_class.new(attributes) }

    it { should be_valid }

    context 'when photo is not present' do
      let(:attributes) { {} }

      it { should_not be_valid }
    end

    context 'when photo has an invalid path' do
      let(:attributes) { {key: 'some/invalid/path/photo.png'} }

      it { should_not be_valid }
    end

  end
end
