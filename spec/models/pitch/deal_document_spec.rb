require 'rails_helper'

module Pitch
  describe DealDocument do

    let(:attributes) { {name: "Document Name", key: 'offering/document/f4834af0-a447-468d-b0a8-c5337e8a48cb/photo.png'} }

    subject { described_class.new(attributes) }

    it { should be_valid }

    context 'when document is not present' do
      let(:attributes) { {} }

      it { should_not be_valid }
    end

    context 'when document has a missing name' do
      let(:attributes) { {name: nil, key: 'offering/document/f4834af0-a447-468d-b0a8-c5337e8a48cb/photo.png'} }

      it { should_not be_valid }
    end

    context 'when document has a missing key' do
      let(:attributes) { {name: "Document Name", key: nil} }

      it { should_not be_valid }
    end

    context 'when document has an invalid path' do
      let(:attributes) { {key: 'some/invalid/path/photo.png'} }

      it { should_not be_valid }
    end

  end
end
