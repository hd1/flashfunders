require 'rails_helper'

describe IssuerApplicationFormValidator do
  let(:params) { {
    # Founder Info
    registration_name: 'Joe Founder',
    email: 'joe_founder@company.com',
    phone_number: '1234567890',
    # Company Info
    company_name: 'Big Company Co.',
    website: 'http://bigcompany.com',
    entity_type: IssuerApplication::ENTITY_TYPES.first.second,
    # Campaign Info
    amount_needed: '100000',
    company_criteria: [IssuerApplication::COMPANY_CRITERIA.first.second],
    new_money_committed_range: IssuerApplication::NEW_MONEY_COMMITTED_RANGES.first.second,
    num_users: IssuerApplication::NUM_USERS.first.second,
    promotion_budget: IssuerApplication::PROMOTION_BUDGET.first.second,
    lead_source: IssuerApplication::LEAD_SOURCES.first.second
  } }

  MULTIVALUED_ATTRS = [:company_criteria]

  subject { described_class.new(params) }

  it 'should be_valid' do
    expect(subject.valid?).to be_truthy
  end

  context 'presence should be required' do
    IssuerApplicationForm::ISSUER_ATTRS.each do |field_name|
      before do
        params[field_name] = field_name.in?(MULTIVALUED_ATTRS) ? [] : ''
      end

      it "for #{field_name}" do
        if field_name == :preferred_partner
          params[:lead_source] = 'option8'
        end
        expect(subject.valid?).to be_falsy
        expect(subject.errors[field_name]).to be_present
      end
    end
  end

  context 'when entity type is not in the list' do
    before { params[:entity_type] = 'Fake type of entity' }

    it 'has an error' do
      expect(subject.valid?).to be_falsy
      expect(subject.errors[:entity_type]).to be_present
    end
  end

end
