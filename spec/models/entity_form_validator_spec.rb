require 'rails_helper'

describe EntityFormValidator do
  let(:klass) { EntityFormValidator }
  let(:params) { {
    name:                      name,
    tax_id_number:             '123456789',
    formation_date:            '12/12/2012',
    formation_state_id:        'AL',
    formation_country:         'US',
    address_1:                 '123 Main st.',
    address_2:                 '',
    city:                      'Anytown',
    state_id:                  'AL',
    country:                   'US',
    zip_code:                  '12345',
    phone_number:              '1234567890',
    position:                  position,
    signatory_first_name:      'Ian',
    signatory_middle_initial:  'I',
    signatory_last_name:       'AM',
    signatory_date_of_birth:   '01/01/1980',
    signatory_ssn:             '555-55-5555',
    signatory_address1:        '123 A St.',
    signatory_address2:        '',
    signatory_city:            'Los Angeles',
    signatory_country:         'US',
    signatory_zip_code:        '91919',
    signatory_state_id:        'CA',
    signatory_us_citizen:      true,
    signatory_daytime_phone:   '555-555-5555',
    signatory_id_type:         0,
    signatory_id_number:       'DL9999999',
    signatory_location:        'California',
    signatory_issued_date:     '01/01/1980',
    signatory_expiration_date: '01/01/1980',
    signatory_mobile_phone:    '1234567890',
    reviewed_information:      '1',
    high_risk_agree:           '1',
    experience_agree:          '1',
    loss_risk_agree:           '1',
    time_horizon_agree:        '1'
  } }

  let(:name) { 'trust' }
  let(:position) { 'Chief' }
  let(:investment) { double(Investment, calc_data: nil) }
  before { allow(investment).to receive(:is_reg_c?).and_return(false) }

  subject { klass.new(params, investment) }

  it 'should be valid' do
    expect(subject).to be_valid
    expect(subject.errors).to be_blank
  end

  context 'when name is not present' do
    before { params[:name] = '' }

    it 'has an error on name' do
      expect(subject.valid?).to be_falsy
      expect(subject.errors[:name]).to be_present
    end
  end

  context 'when tax_id_number is not 9 digits' do
    before { params[:tax_id_number] = '123' }

    it 'has an error on tax_id_number' do
      expect(subject.valid?).to be_falsy
      expect(subject.errors[:tax_id_number]).to include('Provide a nine digit tax ID.')
    end
  end

  context 'when address 1 is not present' do
    before { params[:address_1] = '' }

    it 'has an error on address 1' do
      expect(subject.valid?).to be_falsy
      expect(subject.errors[:address_1]).to be_present
    end
  end

  context 'when city is not present' do
    before { params[:city] = '' }

    it 'has an error on city' do
      expect(subject.valid?).to be_falsy
      expect(subject.errors[:city]).to be_present
    end
  end

  context 'when state_id is not present' do
    before { params[:state_id] = '' }

    it 'has an error on state_id' do
      expect(subject.valid?).to be_falsy
      expect(subject.errors[:state_id]).to be_present
    end
  end

  context 'when zip_code is not present' do
    before { params[:zip_code] = '' }

    it 'has an error on zip_code' do
      expect(subject.valid?).to be_falsy
      expect(subject.errors[:zip_code]).to be_present
    end
  end

  context 'when phone number is not valid' do
    before { params[:phone_number] = 'as6d1asd' }

    it 'has an error on phone number' do
      expect(subject.valid?).to be_falsy
      expect(subject.errors[:phone_number]).to be_present
    end
  end

  context 'when position is not present' do
    before { params[:position] = '' }

    it 'has an error on position' do
      expect(subject.valid?).to be_falsy
      expect(subject.errors[:position]).to be_present
    end
  end

  context 'when formation date is invalid' do
    before { params[:formation_date] = 'notadate' }

    it 'has an error on formation date' do
      expect(subject.valid?).to be_falsy
      expect(subject.errors[:formation_date]).to be_present
    end
  end

  context 'when agreement questions are not answered' do
    before { params[:high_risk_agree] = '0' }

    it 'has an error on high_risk_agree' do
      expect(subject.valid?).to be_falsy
      expect(subject.errors[:high_risk_agree]).to be_present
    end
  end

  context 'entity that requires a formation state' do
    subject { klass.new(params, investment) }

    let(:name) { 'entity' }
    let(:position) { 'Chief' }

    describe 'TrustFormValidator' do
      let(:klass) { EntityFormValidator }
      let(:name) { 'trust' }
      let(:position) { 'Trustee' }

      it 'should be valid' do
        expect(subject).to be_valid
        expect(subject.errors).to be_blank
      end
      context 'when formation state id is not present' do
        before { params[:formation_state_id] = '' }

        it 'has an error on formation_state_id' do
          expect(subject.valid?).to be_falsy
          expect(subject.errors[:formation_state_id]).to be_present
        end
      end

      it 'should be valid' do
        expect(subject).to be_valid
        expect(subject.errors).to be_blank
      end

      context 'when formation state id is not present' do
        before { params[:formation_state_id] = '' }

        it 'has an error on formation_state_id' do
          expect(subject.valid?).to be_falsy
          expect(subject.errors[:formation_state_id]).to be_present
        end
      end

      context 'when formation_date is not present' do
        before { params[:formation_date] = '' }

        subject { klass.new(params, investment) }

        it 'does not have an error on formation_date' do
          expect(subject.errors[:formation_date]).to be_empty
        end
      end

      context 'when formation_date is not a date' do
        before { params[:formation_date] = '123' }

        subject { klass.new(params, investment) }

        it 'has an error on formation_date' do
          expect(subject.valid?).to be_falsy
          expect(subject.errors[:formation_date]).to include('Provide date in mm/dd/yyyy format.')
        end
      end

    end

    describe 'LLCFormValidator' do
      let(:klass) { EntityFormValidator }

      it 'should be valid' do
        expect(subject).to be_valid
        expect(subject.errors).to be_blank
      end

      context 'when formation state id is not present' do
        before { params[:formation_state_id] = '' }

        it 'has an error on formation_state_id' do
          expect(subject.valid?).to be_falsy
          expect(subject.errors[:formation_state_id]).to be_present
        end
      end
    end

    describe 'PartnershipFormValidator' do
      let(:klass) { EntityFormValidator }
      let(:name) { 'trust' }
      let(:position) { 'General Partner' }

      it 'should be_valid' do
        expect(subject.errors).to be_blank
      end

      context 'when formation state id is not present' do
        before { params[:formation_state_id] = '' }

        it 'has an error on formation_state_id' do
          expect(subject.valid?).to be_falsy
          expect(subject.errors[:formation_state_id]).to be_present
        end
      end
    end

    describe 'CorporationFormValidator' do
      let(:klass) { EntityFormValidator }

      it 'should be valid' do
        expect(subject).to be_valid
        expect(subject.errors).to be_blank
      end

      context 'when formation state id is not present' do
        before { params[:formation_state_id] = '' }

        it 'has an error on formation_state_id' do
          expect(subject.valid?).to be_falsy
          expect(subject.errors[:formation_state_id]).to be_present
        end
      end
    end
  end
end
