require 'rails_helper'

describe InvestmentPathMapper do

  include Rails.application.routes.url_helpers

  let(:investment) { double(Investment, state: state, offering_id: 123) }
  let(:path_mapper) { described_class.new(investment) }

  before do
    allow(investment).to receive_message_chain(:offering, :ended?).and_return(false)
    allow(investment).to receive(:resumable?).and_return(true)
    allow(investment).to receive(:persisted?).and_return(true)
    allow(investment).to receive(:ach_debit_failed?).and_return(false)
  end

  context 'Old Investment Flow' do
    before do
      allow(investment).to receive_message_chain(:offering, :reg_c_enabled?).and_return(false)
      allow(investment).to receive(:is_reg_d?).and_return(true)
    end

    describe '#target_for' do
      subject { path_mapper.target_for }

      context 'when the offering has ended' do
        let(:state) { 'started' }
        before do
          allow(investment).to receive_message_chain(:offering, :ended?).and_return(true)
        end
        it { expect(subject).to eql offering_path(id: 123) }
      end

      context 'when the investment is in the started state' do
        let(:state) { 'started' }
        it { expect(subject).to eql new_offering_investment_path(offering_id: 123) }
      end

      context 'when the investment is in the intended state' do
        let(:state) { 'intended' }
        it { expect(subject).to eql new_offering_invest_investment_profile_path(offering_id: 123) }
      end

      context 'when the investment is in the entity selected state' do
        let(:state) { 'entity_selected' }
        it { expect(subject).to eql new_offering_invest_accreditation_new_path(offering_id: 123) }
      end

      context 'when the investment is in the accreditation verified state' do
        let(:state) { 'accreditation_verified' }
        it { expect(subject).to eql offering_invest_investment_documents_path(offering_id: 123) }
      end

      context 'when the investment is in the declined documents state' do
        let(:state) { 'declined_documents' }
        it { expect(subject).to eql offering_invest_investment_documents_path(offering_id: 123) }
      end

      context 'when the investment is in the signed documents state' do
        let(:state) { 'signed_documents' }
        it { expect(subject).to eql offering_invest_fund_path(offering_id: 123) }
      end

      context 'when the investment is in the funding method selected state' do
        let(:state) { 'funding_method_selected' }
        it { expect(subject).to eql dashboard_investments_path }
      end

      context 'when the investment is in some other state' do
        let(:state) { Object.new }
        it { expect(subject).to eql dashboard_investments_path }
      end
    end

    describe '#prohibited?' do
      context 'when the investment is in the started state' do
        let(:state) { 'started' }
        it {
          expect(path_mapper.prohibited?(new_offering_investment_path(offering_id: 123), :get)).to eq(false)
          expect(path_mapper.prohibited?(recreate_offering_investment_path(offering_id: 123), :get)).to eq(false)

          expect(path_mapper.prohibited?(new_offering_invest_investment_profile_path(offering_id: 123), :get)).to eq(true)
        }
      end
    end
  end

  context 'New Investment Flow' do
    before do
      allow(investment).to receive_message_chain(:offering, :reg_c_enabled?).and_return(true)
    end

    describe '#target_for' do
      subject { path_mapper.target_for }

      context 'when the offering has ended' do
        let(:state) { 'started' }
        before do
          allow(investment).to receive_message_chain(:offering, :ended?).and_return(true)
        end
        it { expect(subject).to eql offering_path(id: 123) }
      end

      context 'when the investment is in the started state' do
        let(:state) { 'started' }
        it { expect(subject).to eql new_offering_investment_flow_path(offering_id: 123) }
      end

      context 'when the investment is in the intended state' do
        let(:state) { 'intended' }
        it { expect(subject).to eql new_offering_invest_investment_profile_path(offering_id: 123) }
      end

      context 'when the investment is in the entity selected state' do
        let(:state) { 'entity_selected' }
        it { expect(subject).to eql new_offering_invest_accreditation_new_path(offering_id: 123) }
      end

      context 'when the investment is in the accreditation verified state' do
        let(:state) { 'accreditation_verified' }
        it { expect(subject).to eql offering_invest_investment_documents_path(offering_id: 123) }
      end

      context 'when the investment is in the declined documents state' do
        let(:state) { 'declined_documents' }
        it { expect(subject).to eql offering_invest_investment_documents_path(offering_id: 123) }
      end

      context 'when the investment is in the signed documents state' do
        let(:state) { 'signed_documents' }
        it { expect(subject).to eql offering_investment_flow_fund_path(offering_id: 123) }
      end

      context 'when the investment is in the funding method selected state' do
        let(:state) { 'funding_method_selected' }
        it { expect(subject).to eql dashboard_investments_path }
      end

      context 'when the investment is in some other state' do
        let(:state) { Object.new }
        it { expect(subject).to eql dashboard_investments_path }
      end
    end

    describe '#prohibited?' do
      context 'when the investment is in the started state' do
        let(:state) { 'started' }

        it 'should let me go to the investment amount' do
          expect(path_mapper.prohibited?(new_offering_investment_flow_path(offering_id: 123), :get)).to eq(false)
          expect(path_mapper.prohibited?(offering_investment_flow_path(offering_id: 123), :post)).to eq(false)
        end

        it 'should not let me skip ahead' do
          expect(path_mapper.prohibited?(new_offering_invest_investment_profile_path(offering_id: 123), :get)).to eq(true)
          expect(path_mapper.prohibited?(new_offering_invest_accreditation_new_path(offering_id: 123), :get)).to eq(true)
          expect(path_mapper.prohibited?(offering_invest_investment_documents_path(offering_id: 123), :get)).to eq(true)
          expect(path_mapper.prohibited?(new_offering_invest_fund_path(offering_id: 123), :get)).to eq(true)
        end
      end

      context 'when the investment is in the intended state' do
        let(:state) { 'intended' }

        it 'should let me edit the investment amount' do
          expect(path_mapper.prohibited?(new_offering_investment_flow_path(offering_id: 123), :get)).to eq(true)
          expect(path_mapper.prohibited?(edit_offering_investment_flow_path(offering_id: 123), :get)).to eq(false)
          expect(path_mapper.prohibited?(offering_investment_flow_path(offering_id: 123), :post)).to eq(false)
        end

        it 'should let me go to the profile' do
          expect(path_mapper.prohibited?(new_offering_invest_investment_profile_path(offering_id: 123), :get)).to eq(false)
          expect(path_mapper.prohibited?(offering_invest_investment_profile_path(offering_id: 123), :post)).to eq(false)
        end

        it 'should not let me skip ahead' do
          expect(path_mapper.prohibited?(new_offering_invest_accreditation_new_path(offering_id: 123), :get)).to eq(true)
          expect(path_mapper.prohibited?(offering_invest_investment_documents_path(offering_id: 123), :get)).to eq(true)
          expect(path_mapper.prohibited?(new_offering_invest_fund_path(offering_id: 123), :get)).to eq(true)
        end
      end

      context 'when the investment is in the entity selected state' do
        let(:state) { 'entity_selected' }

        it 'should let me edit the investment amount' do
          expect(path_mapper.prohibited?(new_offering_investment_flow_path(offering_id: 123), :get)).to eq(true)
          expect(path_mapper.prohibited?(edit_offering_investment_flow_path(offering_id: 123), :get)).to eq(false)
          expect(path_mapper.prohibited?(offering_investment_flow_path(offering_id: 123), :post)).to eq(false)
        end

        it 'should let me edit the profile' do
          expect(path_mapper.prohibited?(edit_offering_invest_investment_profile_path(offering_id: 123), :get)).to eq(false)
          expect(path_mapper.prohibited?(offering_invest_investment_profile_path(offering_id: 123), :post)).to eq(false)
        end

        it 'should let me go to the accreditation' do
          expect(path_mapper.prohibited?(new_offering_invest_accreditation_new_path(offering_id: 123), :get)).to eq(false)
          expect(path_mapper.prohibited?(edit_offering_invest_accreditation_new_path(offering_id: 123), :get)).to eq(true)
          expect(path_mapper.prohibited?(new_offering_invest_accreditation_new_path(offering_id: 123), :patch)).to eq(false)
        end

        it 'should not let me skip ahead' do
          expect(path_mapper.prohibited?(offering_invest_investment_documents_path(offering_id: 123), :get)).to eq(true)
          expect(path_mapper.prohibited?(new_offering_invest_fund_path(offering_id: 123), :get)).to eq(true)
        end
      end

    end
  end

end


