require 'rails_helper'

describe ThirdPartyQualificationFormValidator do

  let(:attributes) {
    {
      name: 'Verifier Fake Name',
      role: 'Lawyer',
      company_name: 'Test Company Name',
      license_number: '123License',
      license_state: 'AR',
      phone_number: '555-555-5555',
      signature: 'Verifier Fake Name',
      investor_entity: investor_entity
    }
  }

  let(:investor_entity) { InvestorEntity::Individual.new }

  subject { ThirdPartyQualificationFormValidator.new(attributes) }

  it { should be_valid }

  context 'when "name" is not present' do
    before { attributes[:name] = nil }
    it { should_not be_valid }
  end

  context 'when "role" is not present' do
    before { attributes[:role] = nil }
    it { should_not be_valid }
  end

  context 'when "company_name" is not present' do
    before { attributes[:company_name] = nil }
    it { should_not be_valid }
  end

  context 'when "license_state" is not present' do
    before { attributes[:license_state] = nil }
    it { should_not be_valid }
  end

  context 'when "license_state" is not actually a state abbreviation' do
    before { attributes[:license_state] = 'CY' }
    it { should_not be_valid }
  end

  context 'when "license_number" is not present' do
    before { attributes[:license_number] = nil }
    it { should_not be_valid }
  end

  context 'when "phone_number" is not present' do
    before { attributes[:phone_number] = nil }
    it { should_not be_valid }
  end

  context 'when "phone_number" is not formatted properly' do
    before { attributes[:phone_number] = '1-310-555-12120' }
    it { should_not be_valid }
  end

  context 'when "signature" is not present' do
    before { attributes[:signature] = nil }
    it { should_not be_valid }
  end

  context 'when signature does not match name' do
    before { attributes[:signature] = 'Invalid Name'}

    it { should_not be_valid }

    context 'and differs by case only' do
      before { attributes[:signature] = attributes[:name].upcase }
      it { should be_valid }
    end

    context 'and differs by surrounding whitespace only' do
      before { attributes[:signature] = " #{attributes[:name]} " }
      it { should be_valid }
    end
  end

  context 'when investor entity is an llc' do
    let(:investor_entity) { InvestorEntity::LLC.new }

    it { should_not be_valid }

    context 'and a verification criteria is selected' do
      before { attributes[:verified_by_assets] = '1'}
      it { should be_valid }
    end
  end
end

