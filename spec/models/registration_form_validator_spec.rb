require 'rails_helper'

describe RegistrationFormValidator do

  let(:attributes) {
    {
      password: 'secretpw',
      password_confirmation: 'secretpw',
      us_citizen: true,
      country: 'United States',
      registration_name: 'Joe User',
      email: 'joe@user.com',
      agreed_to_tos: '1'
    }
  }

  subject { described_class.new(attributes) }

  context 'with complete data' do
    it { should be_valid }
  end

  context 'when "password" is not present' do
    before { attributes[:password] = nil }
    it { should_not be_valid }
  end

  context 'when "password_confirmation" is not present' do
    before { attributes[:password_confirmation] = nil }
    it { should_not be_valid }
  end

  context 'when "registration_name" is not present' do
    before { attributes[:registration_name] = nil }
    it { should_not be_valid }
  end

  context 'when "email" is not present' do
    before { attributes[:email] = nil }
    it { should_not be_valid }
  end

  context 'when "email" is not valid' do
    before { attributes[:email] = 'foo@bar' }
    it { should_not be_valid }
  end

  context 'when "email" is already taken' do
    before { create_user(email: attributes[:email], registration_name: attributes[:registration_name]) }
    it { should_not be_valid }
  end

  context 'when "registration_name" is too long' do
    before { attributes[:registration_name] = rand(36**100).to_s(36) }
    it { should_not be_valid }
  end

  context 'when "registration_name" contains periods' do
    before { attributes[:registration_name] = 'Donald A. Duck Jr.'}
    it { should be_valid }
  end

  context 'when "registration_name" contains an illegal character' do
    before { attributes[:registration_name] = 'Donald A. Duck Jr>'}
    it { should_not be_valid }
  end

end
