require 'rails_helper'

describe Subscription do

  describe '#save' do

    context 'it creates a subscription when all the fields are valid' do
      let(:params) { { email: 'test@example.com', source_page: '/flashfundersinsider' } }
      let(:subscription) { Subscription.new(params) }

      it 'is valid' do
        expect(-> { subscription.save }).to change(Subscription, :count).from(0).to(1)

        subs = Subscription.first
        expect(subs.email).to eq('test@example.com')
      end
    end

    context 'it does not create a subscription when the email address is invalid' do
      let(:params) { { email: 'bad_email', source_page: '/flashfundersinsider' } }
      let(:subscription) { Subscription.new(params) }

      it 'is invalid' do
        expect(-> { subscription.save }).to_not change(Subscription, :count)
      end
    end

    context 'embedded spaces fail validation' do
      let(:params) { { email: ' test@ example.com ', source_page: '/flashfundersinsider' } }
      let(:subscription) { Subscription.new(params) }

      it 'is invalid' do
        expect(-> { subscription.save }).to_not change(Subscription, :count)
      end
    end

    context 'it creates a subscription when all the fields are valid' do
      let(:params) { { email: '  test@example.com   ', source_page: '/flashfundersinsider' } }
      let(:subscription) { Subscription.new(params) }

      it 'is valid' do
        expect(-> { subscription.save }).to change(Subscription, :count).from(0).to(1)

        subs = Subscription.first
        expect(subs.email).to eq('test@example.com')
      end
    end

    context 'it creates only one subscription for a given email address' do
      let(:params) { { email: ' Test@example.com', source_page: '/flashfundersinsider' } }
      let(:subscription) { Subscription.new(params) }
      let(:duplicate_subscription) { Subscription.new(params) }

      it 'is valid' do
        expect(-> { subscription.save }).to change(Subscription, :count).from(0).to(1)

        subs = Subscription.first
        expect(subs.email).to eq('test@example.com')

        expect(-> { duplicate_subscription.save }).to_not change(Subscription, :count)
      end
    end
  end
end
