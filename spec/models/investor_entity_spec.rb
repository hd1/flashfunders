require 'rails_helper'

describe InvestorEntity do

  let!(:investor_entity) { FactoryGirl.build(:investor_entity) }
  let!(:pfii) {FactoryGirl.build(:personal_federal_identification_information, investor_entity: investor_entity) }
  subject { investor_entity }

  describe '#full_name' do
    it 'delegates it to the PFII' do
      expect(subject.full_name).to eql('Paulo A Delgado')
    end
  end

  describe '#country_name and #formation_country_name' do
    before do
      allow(State).to receive(:country).and_return(double(name: 'United States'))
    end

    it 'returns the full name of the country' do
      expect(subject.country_name).to eql('United States')
    end

    it 'returns the full name of the formation_country' do
      expect(subject.formation_country_name).to eql('United States')
    end
  end

  describe "#state_name and #formation_state_name" do
    let(:country_double) { double() }
    before do
      allow(State).to receive(:country).and_return(country_double)
      allow(country_double).to receive_message_chain("subregions.count").and_return(1)
      allow(country_double).to receive_message_chain("subregions.coded").and_return( double(name: "California")  )
    end

    it "returns the full name of the state" do
      expect(subject.state_name).to eql("California")
    end

    it "returns the full name of the formation_state" do
      expect(subject.formation_state_name).to eql("California")
    end

  end

  describe "#accreditation" do
    let(:accreditation) { double(EntityAccreditation) }

    before { allow(EntityAccreditation).to receive(:new).and_return(accreditation) }

    it 'returns an EntityAccreditation object' do
      expect(subject.accreditation).to eql(accreditation)
    end
  end

  describe "#dependent associations" do
    let!(:user) { FactoryGirl.create(:user) }
    let!(:investor_entity_die) { FactoryGirl.create(:investor_entity_individual, :with_pfii, user_id: user.id) }
    let!(:qualification) { Accreditor::IndividualIncomeQualification.create!(user_id: user.id, investor_entity_id: investor_entity_die.id, data: Hash.new) }
    let!(:span) { Accreditor::Span.create!(investor_entity_id: investor_entity_die.id) }

    it 'deletes dependent rows when it is destroyed' do
      expect(InvestorEntity.count).to eq(1)
      expect(PersonalFederalIdentificationInformation.count).to eq(1)
      expect(Accreditor::Qualification.count).to eq(1)
      expect(Accreditor::Span.count).to eq(1)

      investor_entity_die.destroy

      expect(InvestorEntity.count).to eq(0)
      expect(PersonalFederalIdentificationInformation.count).to eq(0)
      expect(Accreditor::Qualification.count).to eq(0)
      expect(Accreditor::Span.count).to eq(0)
    end
  end
end
