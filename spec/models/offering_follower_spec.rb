require 'rails_helper'

describe OfferingFollower do

  subject { described_class.new(user_id: 1, offering_id: 1) }

  describe '#following?' do
    it { expect(subject.following?).to be_falsy }

    context 'when mode is set' do
      before { subject.mode = :asker }
      it { expect(subject.following?).to be_truthy }
    end
  end

  describe '#follow_as!' do
    it 'sets the provided mode of following' do
      Timecop.freeze do
        subject.follow_as!(:asker)

        expect(subject).to_not be_changed
        expect(subject.mode).to eql("asker")
      end
    end

    context 'when mode is already set' do
      before { allow(subject).to receive(:save!) }

      it 'only updates if a higher priority' do
        subject.mode = :follower
        expect{ subject.follow_as!(:asker) }.to change{ subject.mode }
        expect{ subject.follow_as!(:investor) }.to change{ subject.mode }

        subject.mode = :asker
        expect{ subject.follow_as!(:follower) }.not_to change{ subject.mode }
        expect{ subject.follow_as!(:investor) }.to change{ subject.mode }

        subject.mode = :investor
        expect{ subject.follow_as!(:follower) }.not_to change{ subject.mode }
        expect{ subject.follow_as!(:asker) }.not_to change{ subject.mode }
      end
    end
  end

  describe '#unfollow!' do
    before { subject.mode = :asker }

    it 'clears the mode attribute' do
      subject.unfollow!

      expect(subject).to_not be_changed
      expect(subject.mode).to be_nil
    end
  end

end
