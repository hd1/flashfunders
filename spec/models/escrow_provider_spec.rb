require 'rails_helper'

describe EscrowProvider do

  let(:investment) { double(Investment, id: 34) }

  subject { FactoryGirl.create(:escrow_provider_fundamerica) }

  describe 'with an escrow provider that has a realtime investment api' do
    before do
      allow(subject).to receive(:provides_realtime_investment_api?).and_return(true)
    end

    it 'queues a call to the realtime investment api for fund america' do
      expect{subject.notify_completion!(investment)}.to change(Sidekiq::Extensions::DelayedModel.jobs, :size).by(1)
    end

    it 'queues a call to the realtime investment api for fund america reg C' do
      subject.provider_key = :fund_america_reg_c
      expect{subject.notify_completion!(investment)}.to change(Sidekiq::Extensions::DelayedModel.jobs, :size).by(1)
    end
  end

  describe 'with an escrow provider that does not have a realtime investment api' do
    before do
      allow(subject).to receive(:provides_realtime_investment_api?).and_return(false)
    end

    it 'does not queue a call to the realtime investment api' do
      expect{subject.notify_completion!(investment)}.to change(Sidekiq::Extensions::DelayedModel.jobs, :size).by(0)
    end
  end

end
