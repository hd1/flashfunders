require 'rails_helper'

describe EntityFormAuditor do

  subject(:auditor) { EntityFormAuditor.new }

  describe '#write' do
    it 'creates an AuditLogEntry' do
      expect {
        auditor.write({},nil,nil,nil,nil)
      }.to change(AuditLogEntry, :count).from(0).to(1)
    end

    it 'create an AuditLogEntry with the correct attributes' do
      timestamp = Time.now.utc

      auditor.write({first_name: 'foo'}, 1, '1.2.3.4', '/foo/path', timestamp)

      audit_log_entry = AuditLogEntry.last

      expect(audit_log_entry.acting_user_id).to eq(1)
      expect(audit_log_entry.acting_user_ip_address).to eq('1.2.3.4')
      expect(audit_log_entry.form_path).to eq('/foo/path')
      expect(audit_log_entry.persisted_state).to eq('first_name' => 'foo')
      expect(audit_log_entry.persisted_time.to_i).to eq(timestamp.to_i)
    end

    it 'sanitizes SSN for AuditLogEntry Record' do
      auditor.write({first_name: 'foo', signatory_ssn: '123-45-6789'}, 1, '1.2.3.4', '/foo/path', Time.now)

      expect(AuditLogEntry.last.persisted_state['signatory_ssn']).to eq('***-**-6789')
    end

  end
end
