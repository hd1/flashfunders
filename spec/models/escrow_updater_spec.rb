require 'rails_helper'

describe EscrowUpdater do
  describe '#fetch_and_process_all_escrows' do

    let(:funds_received_mail_double) { double(deliver_now: true) }
    let(:investment) { create(:investment,
      offering_id: 23,
      amount: BigDecimal.new(5000), 
      user_id: 5,
      state: :funding_method_selected,
      escrowed_at: 1.day.ago) }

    subject(:escrow_updater) do
      EscrowUpdater.new
    end

    before do
      allow(Investment).to receive(:where).and_return([investment])
      allow(InvestmentMailer).to receive(:funds_received).and_return(funds_received_mail_double)
      allow_any_instance_of(GoogleAnalytics::Base).to receive_message_chain(:tracker, :event)
    end

    describe '#process_investments!!' do

      it 'updates state of investment to escrowed if it can transition' do
        subject.process_investments!
        expect(investment.escrowed?).to be_truthy
      end

      it 'queues the email to be delivered' do
        subject.process_investments!
        expect(InvestmentMailer).to have_received(:funds_received).with(investment.id)
        expect(funds_received_mail_double).to have_received(:deliver_now)
      end

      it 'does not update the state of the investment' do
        allow(investment).to receive(:can_transfer_funds?).and_return(false)
        subject.process_investments!
        expect(investment.funding_method_selected?).to be_truthy
      end
    end

  end
end
