require 'rails_helper'

describe InvestorInvestmentFormValidator do
  let(:investment_calculator_double) { double(InvestorInvestmentCalculator, projected_shares: 88, projected_cost:33.76, projected_percent_ownership: 27.879) }
  let(:offering_presenter_double) { double(OfferingPresenter, minimum_investment_amount: 10.00, maximum_total_investment: 10000, accredited_balance: 3000.00) }

  def validator_with_doubles_injected(attributes = {})
    InvestorInvestmentFormValidator.new(
        attributes,
        investment_calculator_double,
        offering_presenter_double
    )
  end

  before do
    allow(Honeybadger).to receive(:notify)
    allow(offering_presenter_double).to receive(:offering)
    allow_any_instance_of(InvestorInvestmentFormValidator).to receive(:offering)
  end

  describe '#valid?' do
    let(:valid_attributes) do
      {
          investment_shares: '88',
          investment_amount: '33.76',
          investment_percent_ownership: '27.879',
          unrounded_investment_amount: '$3,200',
      }
    end

    it 'is invalid by default' do
      expect(validator_with_doubles_injected).to_not be_valid
    end

    it 'can be valid' do
      validator = validator_with_doubles_injected(valid_attributes)
      expect(validator).to be_valid
    end

    context 'to be valid' do
      it 'matches the format of $##.##' do
        expect(validator_with_doubles_injected(valid_attributes.merge(unrounded_investment_amount: '12,345.6789'))).to be_valid
        expect(validator_with_doubles_injected(valid_attributes.merge(unrounded_investment_amount: '$123.45'))).to be_valid
        expect(validator_with_doubles_injected(valid_attributes.merge(unrounded_investment_amount: '$$123.45'))).to be_valid
        expect(validator_with_doubles_injected(valid_attributes.merge(unrounded_investment_amount: '$$1 any 2 old 3 crap .4 you 5 like'))).to be_valid
        expect(validator_with_doubles_injected(valid_attributes.merge(unrounded_investment_amount: '$$bills'))).to be_valid
        expect(validator_with_doubles_injected(valid_attributes.merge(unrounded_investment_amount: ''))).to_not be_valid
      end

      it 'the calculated investment must be above the minimum investment amount' do
        allow(investment_calculator_double).to receive(:projected_cost).and_return(offering_presenter_double.minimum_investment_amount - 1)
        validator = validator_with_doubles_injected(valid_attributes)
        expect(validator).to_not be_valid
        expect(validator.errors[:unrounded_investment_amount]).to eq(['Must be larger than $10.00'])
      end

      it 'the calculated investment must be at or below the maximum investment amount' do
        allow(investment_calculator_double).to receive(:projected_cost).and_return(offering_presenter_double.maximum_total_investment + 1)
        validator = validator_with_doubles_injected(valid_attributes)
        expect(validator).to_not be_valid
        expect(validator.errors[:unrounded_investment_amount]).to eq(['Must not be larger than $10000.00'])
      end
    end
  end

  describe '#errors' do
    it 'is empty by default' do
      expect(InvestorInvestmentFormValidator.new.errors).to be_empty
    end

    it 'behaves like active record errors when they exist' do
      validator = validator_with_doubles_injected
      validator.valid?
      expect(validator.errors).to be_a ActiveModel::Errors
    end
  end
end
