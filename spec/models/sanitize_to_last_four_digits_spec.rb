require 'rails_helper'

describe SanitizeToLastFourDigits do
  describe '.sanitize' do
    it 'stars out all but the last 4 alphanumeric characters' do
      expect(SanitizeToLastFourDigits.sanitize('123-45-6789')).to eq('***-**-6789')
      expect(SanitizeToLastFourDigits.sanitize('123d73aa67m9')).to eq('********67m9')
      expect(SanitizeToLastFourDigits.sanitize('7m9')).to eq('***')
      expect(SanitizeToLastFourDigits.sanitize('7m96')).to eq('****')
    end

    context 'when argument is nil' do
      it 'returns an empty string' do
        expect(SanitizeToLastFourDigits.sanitize(nil)).to eq('')
      end
    end

    context 'when argument is an empty string' do
      it 'returns an empty string' do
        expect(SanitizeToLastFourDigits.sanitize('')).to eq('')
      end
    end
  end
end