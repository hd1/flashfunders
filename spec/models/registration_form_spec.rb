require 'rails_helper'

describe RegistrationForm do

  let(:attributes) {
    {
      password: 'secret_pass',
      password_confirmation: 'secret_pass',
      registration_name: 'Joe User',
      email: 'joe@user.com',
      agreed_to_tos_ip: '127.0.0.1',
      agreed_to_tos_datetime: Time.now.utc,
      agreed_to_tos: '1'
    }
  }

  subject { described_class.new(attributes) }

  context 'with complete data' do
    it { should be_valid }
  end

  describe '#save' do
    let(:partially_registered_user) { create_temporary_user(email: 'joe@temp.com', registration_name: 'Joe Temp') }
    let(:unconfirmed_user) { create_unconfirmed_user(email: 'joe@unconfirmed.com', registration_name: 'Joe Unconfirmed') }

    context 'with an unregistered user' do
      it 'should create a new user' do
        expect(User.find_for_authentication(email: attributes[:email])).to be_blank
        expect(subject.save).to be_truthy
        expect(User.find_for_authentication(email: attributes[:email])).to_not be_blank
      end
    end

    context 'with a partially registered user' do
      before {
        attributes[:email] = partially_registered_user.email
        attributes[:registration_name] = partially_registered_user.registration_name
      }
      it 'should set the missing attributes' do
        expect(partially_registered_user.us_citizen).to be_blank
        expect(subject.save).to be_truthy
        partially_registered_user.reload
        expect(partially_registered_user.us_citizen).to be_truthy
        expect(partially_registered_user.has_no_password?).to be_falsy
        expect(partially_registered_user.confirmed?).to be_falsy
      end
    end

    context 'with an unconfirmed user' do
      before {
        attributes[:email] = unconfirmed_user.email
        attributes[:registration_name] = unconfirmed_user.registration_name
      }
      it 'should complain about existing email' do
        expect(subject.save).to be_falsy
        expect(subject.errors.messages[:email]).to include('has already been taken')
      end
    end

    context 'with invalid data' do
      before { attributes[:password] = nil }
      it 'should complain about a missing password' do
        expect(subject.save).to be_falsy
        expect(subject.errors.messages[:password]).to include('Required')
      end
    end
  end

end
