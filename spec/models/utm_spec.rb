require 'rails_helper'

describe Utm do
  subject { described_class.new }
  let(:client_id) { '1234567890.0123456789' }
  let(:utm_params) do
    {
      utm_source: 'dev',
      utm_medium: 'rspec',
      utm_campaign: 'test',
      utm_term: 'test',
      utm_content: 'utmstuff',
      utm_other: 'others',
    }
  end
  let(:page_action) { Subscription.create(email: "foo@bar.com") }

  describe '#track!' do
    context 'with utm params and no page action' do
      it 'creates a utm record' do
        expect {
          Utm.track!(client_id, utm_params.stringify_keys, nil, nil)
        }.to change(Utm, :count).by(1)
      end
    end

    context 'with page action and no utm params' do
      it 'does not create a utm record for new unique visitor that does not have any utm param' do
        expect {
          Utm.track!(client_id, {}, nil, page_action)
        }.to_not change(Utm, :count)
      end

      it 'creates a client tracker record when the visitor used to visit site with utm params' do
        Utm.track!(client_id, utm_params.stringify_keys, nil, nil)

        expect {
          Utm.track!(client_id, {}, nil, page_action)
        }.to change(ClientTracker, :count).by(1)
      end
    end
  end

  describe '#parse_utms' do
    let(:utm_attrs_keys) { Utm.column_names - ['id', 'client_id', 'created_at', 'updated_at'] }

    it 'truncates utm_ prefix from utm params' do
      attrs_keys = Utm.send(:parse_utms, utm_params.stringify_keys).keys
      expect(attrs_keys).to eq(utm_attrs_keys)
    end
  end

  describe '#track_action' do
    let(:subscription) { Subscription.create(email: 'utm@user.com')}
    before { allow(ClientTracker).to receive(:create) }

    it 'does not track any action without action option params' do
      Utm.send(:track_action, client_id, nil)
      expect(ClientTracker).to_not have_received(:create)
    end

    it 'creates a client tracker record' do
      Utm.send(:track_action, client_id, subscription)
      expect(ClientTracker).to have_received(:create).with(client_id: client_id, trackable_id: subscription.id, trackable_type: subscription.class.to_s)
    end

    it 'does not create duplicated client tracker records' do
      allow(ClientTracker).to receive(:create).and_call_original

      expect {
        2.times { Utm.send(:track_action, client_id, subscription) }
      }.to change(ClientTracker, :count).by(1)
    end
  end

  describe '#store_utms' do
    before { allow(Utm).to receive(:create) }

    it 'does not store utm without utms params' do
      Utm.send(:store_utms, client_id, {})
      expect(Utm).to_not have_received(:create)
    end

    it 'creats utm record with correct arguments' do
      Utm.send(:store_utms, client_id, {'utm_source' => 'blah'})
      expect(Utm).to have_received(:create).with('source' => 'blah', client_id: client_id)
    end
  end

  describe '#tie_user' do
    let(:registered_user) { create_user }
    before { allow(ClientTracker).to receive(:create) }

    it 'does not tie user with client id if user has not sign in' do
      Utm.send(:tie_user, client_id, nil)
      expect(ClientTracker).to_not have_received(:create)
    end

    it 'ties signed in user with client id' do
      Utm.send(:tie_user, client_id, registered_user)
      expect(ClientTracker).to have_received(:create).with(
        client_id: client_id, trackable_id: registered_user.id,  trackable_type: registered_user.class.to_s)
    end

    it 'does not create duplicated client tracker records' do
      allow(ClientTracker).to receive(:create).and_call_original

      expect {
        2.times { Utm.send(:tie_user, client_id, registered_user) }
      }.to change(ClientTracker, :count).by(1)
    end
  end

end
