require 'rails_helper'

describe InvestorInvestmentForm do

  let(:user_double) { double(User, id: 88) }
  let(:security_double) { double(Securities::Security) }
  let(:offering_double) { double(Offering, id: 42, reg_d_direct_security: security_double, other_security: nil) }
  let(:investment_double) { double(Investment, id: 5, begin!: nil, update_attributes: nil) }
  let(:validator_double) { double(InvestorInvestmentFormValidator, errors: 'foo') }

  subject(:investor_investment_form) do
    InvestorInvestmentForm.new({offering_id: 42, user: user_double}, validator_double)
  end

  before do
    allow(Investment).to receive(:create).and_return(investment_double)
  end

  describe '#create' do
    before {
      allow(Offering).to receive(:find).and_return(offering_double)
    }

    it 'creates the investment amount and registers the investment state' do
      investor_investment_form.create
      expect(Investment).to have_received(:create).
                                             with(hash_including(:user_id,
                                                                 :offering_id,
                                                                 :security,
                                                                 :amount,
                                                                 :shares,
                                                                 :percent_ownership))
      expect(investment_double).to have_received(:begin!)
    end
  end

  describe '#update' do
    before {
      allow(Offering).to receive(:find).and_return(offering_double)
      allow(Investment).to receive(:find).and_return(investment_double)
      allow(investment_double).to receive(:can_edit_amount?).and_return(false)
    }

    it 'updates the investment amount' do
      investor_investment_form = InvestorInvestmentForm.new(
          {
              offering_id: 42,
              user: user_double,
              investment_id: 27,
              investment_amount: 2435.35,
              investment_shares: 58,
              investment_percent_ownership: 32.645
          },
          validator_double,
      ).update

      expect(investment_double).to have_received(:update_attributes).with(
        security: security_double,
        amount: 2435.35,
        shares: 58,
        percent_ownership: 32.645)
    end
  end

  describe '#investment_id' do
    before {
      allow(Offering).to receive(:find).and_return(offering_double)
    }

    context 'using the form to create an investment, no investment exists' do
      it 'returns the id of the investment if it has been created, and nil if it has not' do
        expect(investor_investment_form.investment_id).to eq(nil)
        investor_investment_form.create
        expect(investor_investment_form.investment_id).to eq(5)
      end
    end

    context 'using the form to update an already existing investment' do
      it 'returns the id of the already existing investment it finds' do
        investor_investment_form = InvestorInvestmentForm.new(
            {offering_id: 42, user: user_double, investment_id: 27},
            validator_double,
        )
        expect(investor_investment_form.investment_id).to eq(27)
      end
    end
  end

  describe '#errors' do
    it 'delegates the errors to the validator' do
      expect(investor_investment_form.errors).to eq(validator_double.errors)
    end
  end
end
