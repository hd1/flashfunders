require 'rails_helper'

describe Stakeholder do

  subject { described_class.new(stakeholder_type_id: 2, full_name: "Some Investor") }
  let!(:offering) { FactoryGirl.create(:offering) }

  describe "notable investor" do
    context "when not saved or not yet having an offering" do

      it "assigns an instance variable" do
        subject.is_notable_investor = "moo"
        expect(subject.instance_variable_get(:@is_notable_investor)).to be_truthy
      end

      it "getter returns false" do
        expect(subject.is_notable_investor?).to be_falsy
      end

    end

    context "when saved" do
      before do
        allow(offering).to receive(:update_attribute).and_call_original
        subject.is_notable_investor = true
        subject.offering = offering
      end
      it "updates the offering if necessary" do
        expect(subject).to receive(:update_notable_investor_if_necessary).and_call_original
        subject.save
        expect(offering).to have_received(:update_attribute).with('notable_investor_id', subject.reload.id)
      end

      it "checks with the offering to see if its a notable investor" do
        subject.save
        expect(subject.is_notable_investor?).to be_truthy

        offering.update_attribute('notable_investor_id', nil)

        expect(subject.reload.is_notable_investor?).to be_falsy

      end
    end

  end
end
