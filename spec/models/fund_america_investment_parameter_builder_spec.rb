require 'rails_helper'

describe FundAmericaInvestmentParameterBuilder do

  context 'with a non RegC offering' do
    let(:offering) { double(Offering, share_price: BigDecimal.new('500.00')) }
    let(:security) { double(Securities::Security, fund_america_id: 'yLzpAj3qTsWGChgvml4uZw') }
    let(:investment) { double(Investment, security: security,
                              amount: BigDecimal.new(5000),
                              agreement_url: 'https://www.example.com/agreement.pdf',
                              shares: 10,
                              investor_entity: investor_entity,
                              user: investor_user,
                              by_individual?: investment_by_individual?,
                              is_spv?: false,
                              is_reg_c?: false,
                              fund_america_id: nil,
                              funding_method: "wire",
                              funding_method_selected?: true) }

    let(:investor_user) { double(User, email: 'john.investor@example.com',
                                 id: 101) }

    let(:personal_federal_identification_information) do
      double(PersonalFederalIdentificationInformation,
             city: 'Las Vegas',
             country: 'US',
             date_of_birth: Date.parse('1980-01-01'),
             full_name: 'John Q Investor',
             phone_number: '17025551212',
             zip_code: '89123',
             state_id: 'NV',
             address1: '555 Some St',
             ssn: '000000000'
      )
    end

    describe '.build' do
      context 'with a individual investor' do
        let(:investor_entity) { double(InvestorEntity,
                                       country: nil, city: nil, full_name: 'John Q Investor',
                                       state_id: nil, address_1: nil, zip_code: nil,
                                       phone_number: nil, tax_id_number: nil,
                                       personal_federal_identification_information: personal_federal_identification_information,
                                       fund_america_id: '',
                                       fund_america_ach_authorization_id: ''
        ) }

        let(:investment_by_individual?) { true }
        it 'should build the correct parameters' do
          expect(FundAmericaInvestmentParameterBuilder.build(offering, investment))
            .to eq(
                  {
                    id: nil,
                    amount: '5000.00',
                    equity_share_count: 10,
                    equity_share_price: '500.00',
                    offering_id: 'yLzpAj3qTsWGChgvml4uZw',
                    payment_method: 'wire',
                    subscription_agreement_url: 'https://www.example.com/agreement.pdf',
                    entity: {
                      id: '',
                      city: 'Las Vegas',
                      country: 'US',
                      date_of_birth: '1980-01-01',
                      email: 'john.investor@example.com',
                      name: 'John Q Investor',
                      phone: '17025551212',
                      postal_code: '89123',
                      region: 'NV',
                      street_address_1: '555 Some St',
                      tax_id_number: '000000000',
                      type: 'person'
                    }
                  }
                )
        end
      end
      context 'with a corporate investor' do
        let(:investor_entity) { double(InvestorEntity, country: investor_entity_country,
                                       city: 'London',
                                       name: 'Biz Baz Incorporated',
                                       full_name: personal_federal_identification_information.full_name,
                                       state_id: 'AA',
                                       formation_state_id: 'BB',
                                       executive_name: 'Bilbo Baggins',
                                       address_1: '123 Fake Street',
                                       zip_code: '12345',
                                       phone_number: '1231231234',
                                       tax_id_number: '123456789') }


        let(:investment_by_individual?) { false }
        let(:investor_entity_country) { 'UK' }
        it 'should build the correct parameters' do
          expect(FundAmericaInvestmentParameterBuilder.build(offering, investment))
            .to eq(
                  {
                    id: nil,
                    amount: '5000.00',
                    equity_share_count: 10,
                    equity_share_price: '500.00',
                    offering_id: 'yLzpAj3qTsWGChgvml4uZw',
                    payment_method: 'wire',
                    subscription_agreement_url: 'https://www.example.com/agreement.pdf',
                    entity: {
                      type: 'company',
                      city: 'London',
                      country: 'UK',
                      email: 'john.investor@example.com',
                      contact_name: 'John Q Investor',
                      name: 'Biz Baz Incorporated',
                      phone: '1231231234',
                      postal_code: '12345',
                      region: 'AA',
                      street_address_1: '123 Fake Street',
                      tax_id_number: '123456789',
                      executive_name: 'Bilbo Baggins',
                      state_formed_in: 'BB'
                    }
                  }
                )
        end
        context 'without specify country' do
          let(:investor_entity_country) { nil }
          it 'should assume US' do
            expect(FundAmericaInvestmentParameterBuilder.build(offering, investment)[:entity][:country]).to eq('US')
          end
        end
      end
    end

    context 'with a Reg C offering' do
      let(:offering) { double(Offering, share_price: BigDecimal.new('500.00'), fund_america_id: 'yLzpAj3qTsWGChgvml4uZw', reg_c_enabled?: true) }
      let(:investment) { double(Investment, amount: BigDecimal.new(5000),
                                agreement_url: 'https://www.example.com/agreement.pdf',
                                shares: 10,
                                investor_entity: investor_entity,
                                user: investor_user,
                                by_individual?: investment_by_individual?,
                                is_spv?: false,
                                is_reg_c?: true,
                                fund_america_id: nil,
                                funding_method: 'wire',
                                funding_method_selected?: true,
                                investor_account_id: 1,
                                calc_data: { reg_s_investor: false }) }

      let(:investor_user) { double(User, email: 'john.investor@example.com',
                                   full_name: 'John User', id: 101) }

      let(:personal_federal_identification_information) do
        double(PersonalFederalIdentificationInformation,
               city: 'Las Vegas',
               country: 'US',
               date_of_birth: Date.parse('1980-01-01'),
               full_name: 'John Q Investor',
               phone_number: '17025551212',
               zip_code: '89123',
               state_id: 'NV',
               address1: '555 Some St',
               ssn: '000000000'
        )
      end

      describe '.ach_authorization_params' do
        before do
          allow(Bank::InvestorAccount).to receive(:find).and_return(bank)

        end
        context 'with a individual investor' do
          let(:investor_entity) { double(InvestorEntity,
                                         country: nil, city: nil, full_name: 'John Q Investor',
                                         state_id: nil, address_1: nil, zip_code: nil,
                                         phone_number: nil, tax_id_number: nil,
                                         personal_federal_identification_information: personal_federal_identification_information,
                                         fund_america_id: '',
                                         fund_america_ach_authorization_id: ''
          ) }

          let(:investment_by_individual?) { true }
          let(:bank) { double(Bank::Account, account_number: '123123123', routing_number: '011000015', account_type: "CHECKING", account_holder: 'Bongo Bob') }
          let(:fund_america_entity) { double(FundAmerica::Entity, id: nil) }
          let(:controller_params) { { ip_address: '192.168.0.1', user_agent: 'ruby' } }
          it 'should build the correct parameters' do
            expect(FundAmericaInvestmentParameterBuilder.ach_authorization_params(
              investment, fund_america_entity, controller_params))
              .to eq({ id: '',
                       account_number: '123123123',
                       account_type: 'checking',
                       name_on_account: 'Bongo Bob',
                       routing_number: '011000015',
                       entity_id: nil,
                       check_type: 'personal',
                       email: 'john.investor@example.com',
                       literal: 'John User',
                       ip_address: '192.168.0.1',
                       user_agent: 'ruby'
                     }
                  )
          end
        end
      end
    end
  end
end
