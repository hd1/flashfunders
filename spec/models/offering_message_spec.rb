require 'rails_helper'

describe OfferingMessage do
  describe '#distinct_offerings_for_user' do
    it 'finds messages only for the given user id' do
      message = OfferingMessage.create user_id: 1, body: 'message'
      OfferingMessage.create user_id: 2, body: 'message'

      expect(
          described_class.distinct_offerings_for_user(1)
      ).to eq([message])
      expect(
          described_class.distinct_offerings_for_user(1)[0]['body']
      ).to eq('message')
    end

    it 'finds the most recent message for each offering' do
      message1 = OfferingMessage.create user_id: 1, offering_id: 1, created_at: Time.current + 1.minute, body: 'message 1'
      OfferingMessage.create user_id: 1, offering_id: 1, created_at: Time.current, body: 'message 1'
      message2 = OfferingMessage.create user_id: 1, offering_id: 2, created_at: Time.current + 1.minute, body: 'message 2'
      OfferingMessage.create user_id: 1, offering_id: 2, created_at: Time.current, body: 'message 2'

      expect(
          described_class.distinct_offerings_for_user(1)
      ).to eq([message2, message1])
      expect(
          described_class.distinct_offerings_for_user(1)[1]['body']
      ).to eq('message 1')
      expect(
          described_class.distinct_offerings_for_user(1)[0]['body']
      ).to eq('message 2')
    end
  end

  describe '#distinct_users_for_offering' do
    it 'finds messages only for the given offering id' do
      message = OfferingMessage.create offering_id: 1, body: 'message'
      OfferingMessage.create offering_id: 2, body: 'message'

      expect(
          described_class.distinct_users_for_offering(1)
      ).to eq([message])
      expect(
          described_class.distinct_users_for_offering(1)[0]['body']
      ).to eq('message')

    end

    it 'finds the most recent message from each user' do
      message1 = OfferingMessage.create user_id: 1, offering_id: 1, created_at: Time.current + 1.minute, body: 'message 1'
      OfferingMessage.create user_id: 1, offering_id: 1, created_at: Time.current, body: 'message 1'
      message2 = OfferingMessage.create user_id: 2, offering_id: 1, created_at: Time.current + 1.minute, body: 'message 2'
      OfferingMessage.create user_id: 2, offering_id: 1, created_at: Time.current, body: 'message 2'

      expect(
          described_class.distinct_users_for_offering(1)
      ).to eq([message2, message1])
      expect(
          described_class.distinct_users_for_offering(1)[1]['body']
      ).to eq('message 1')
      expect(
          described_class.distinct_users_for_offering(1)[0]['body']
      ).to eq('message 2')
    end
  end

  describe 'validation' do
    let(:valid_attributes) do  
      { body: 'message' } 
    end
    let(:validator) { OfferingMessage.new(valid_attributes) }

    it 'can be valid' do
      expect(validator).to be_valid
    end

    it 'can not be valid' do
      valid_attributes[:body] = nil
      expect(validator).to_not be_valid
      expect(validator.errors[:body]).to eq(['Required'])
    end
  end
end
