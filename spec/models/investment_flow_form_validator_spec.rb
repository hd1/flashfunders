require 'rails_helper'

describe InvestmentFlowFormValidator do
  subject { described_class.new(params) }

  context 'reg C investment' do

    describe '#errors' do

      let(:params) { {} }
      subject { described_class.new(params) }

      it 'is invalid when all params are missing' do
        expect(subject).to_not be_valid
        expect(subject.errors.size).to eq(3)
      end

      it 'is invalid when the amount is missing' do
        params[:selector] = 'reg-c'
        expect(subject).to_not be_valid
        expect(subject.errors.size).to eq(1)
        expect(subject.errors[:amount][0]).to eql('Required')
      end

      it 'is invalid when the selector is missing' do
        params[:amount] = '100'
        expect(subject).to_not be_valid
        expect(subject.errors.size).to eq(2)
        expect(subject.errors[:selector][0]).to eql('Required')
        expect(subject.errors[:selector][1]).to eql('is not included in the list')
      end

      it 'is invalid when the selector is incorrect' do
        params[:amount] = '100'
        params[:selector] = 'x'
        expect(subject).to_not be_valid
        expect(subject.errors.size).to eq(1)
        expect(subject.errors[:selector][0]).to eql('is not included in the list')
      end

    end

    describe '#valid?' do
      let(:offering_double) { double(Offering, reg_d_direct_security: double(Securities::Security, maximum_total_investment: 1000000), share_price: 1) }

      let(:params) { {
        selector: 'reg-c',
        income: 1000000,
        net_worth: 5000000,
        external: 98000,
        internal: 200,
        international: 'false',
        reg_c_cutoff: 20000,
        reg_c_min: 100,
        reg_c_output: '{"limit":100000,"available":1800,"over_limit":0}',
        server_internal: 200,
        server_reg_c_cutoff: 20000,
        offering: offering_double
      } }

      it 'is valid when the amount is within bounds' do
        params[:amount] = 1800
        expect(subject).to be_valid
      end

      it 'is invalid when the amount is too high' do
        params[:amount] = 1801
        expect(subject).to_not be_valid
        expect(subject.errors[:amount].size).to eq(1)
        expect(subject.errors[:amount][0]).to eq('Investment is over the amount you have available')
      end

      it 'is invalid when the amount is too low' do
        params[:amount] = 99
        expect(subject).to_not be_valid
        expect(subject.errors[:amount].size).to eq(1)
      end

      it 'is invalid when the provided calc output does not match the values calculated on the server' do
        params[:amount] = 1800
        params[:reg_c_output] = '{"limit":100000,"available":18000,"over_limit":0}'
        expect(subject).to_not be_valid
        expect(subject.errors.size).to eq(1)
        expect(subject.errors[:amount].size).to eq(1)
        expect(subject.errors[:amount][0]).to eq('Reg C Calculations did not match')
      end
    end

  end

  context 'Reg S investment' do

    describe '#valid?' do

      let(:params) { {
        selector: 'reg-c',
        international: 'true',
        reg_c_cutoff: 20000,
        reg_c_min: 100,
        server_reg_c_cutoff: 20000
      } }

      it 'is valid when the amount is within bounds' do
        params[:amount] = 19999
        expect(subject).to be_valid
      end

      it 'is invalid when the amount is too high' do
        params[:amount] = 20000
        expect(subject).to_not be_valid
      end

      it 'is invalid when the amount is too low' do
        params[:amount] = 99
        expect(subject).to_not be_valid
      end

    end

  end

  context 'Reg D investment' do
    describe '#valid?' do

      let(:offering_double) { double(Offering, reg_d_direct_security: double(Securities::Security, maximum_total_investment: 1000000), share_price: 1) }
      let(:params) { {
        selector: 'reg-d',
        reg_c_cutoff: 20000,
        reg_c_min: 100,
        offering: offering_double,
        server_reg_c_cutoff: 20000
      } }

      it 'is valid when the amount is within bounds' do
        params[:amount] = 20000
        expect(subject).to be_valid
      end

      it 'is invalid when the amount is too high' do
        params[:amount] = 1000001
        expect(subject).to_not be_valid
        expect(subject.errors[:amount][0]).to eq('Reg D Investment Amount is out of bounds')
      end

      it 'is invalid when the amount is too low' do
        params[:amount] = 19999
        expect(subject).to_not be_valid
        expect(subject.errors[:amount][0]).to eq('Reg D Investment Amount is out of bounds')
      end

    end

  end

end
