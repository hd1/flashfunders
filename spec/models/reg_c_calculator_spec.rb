require 'rails_helper'

describe RegCCalculator do
  let(:external) { 0 }
  let(:internal) { 0 }
  let(:reg_c_cutoff) { 20000 }
  let(:share_price) { 1 }

  context 'with an income of $100K and a net worth of $100K' do
    let(:income) { 100000 }
    let(:net_worth) { 100000 }

    subject { described_class.new(income, net_worth, external, internal, reg_c_cutoff, share_price) }

    context 'investing for the first time' do

      it 'should limit me to $10K with $10K available' do
        expect(subject.limit).to eq(10000.0)
        expect(subject.available).to eq(10000)
      end
    end

    context 'with external CF investments of $2,500' do
      let(:external) { 2500 }

      it 'should limit me to $10K with $7500 available' do
        expect(subject.limit).to eq(10000.0)
        expect(subject.available).to eq(7500)
      end

      context 'and CF investments on FlashFunders of $1,000' do
        let(:internal) { 1000 }

        it 'should limit me to $10K with $6500 available' do
          expect(subject.limit).to eq(10000.0)
          expect(subject.available).to eq(6500)
        end
      end

      context 'and CF investments on FlashFunders of $8,000' do
        let(:internal) { 8000 }

        it 'should limit me to $10K with $0 available' do
          expect(subject.limit).to eq(10000)
          expect(subject.available).to eq(0)
        end
      end
    end
  end

  context 'with an income of $75K and a net worth of $500K' do
    let(:income) { 75000 }
    let(:net_worth) { 500000 }

    subject { described_class.new(income, net_worth, external, internal, reg_c_cutoff, share_price) }

    context 'investing for the first time' do

      it 'should limit me to $3750 with $3750K available' do
        expect(subject.limit).to eq(3750.0)
        expect(subject.available).to eq(3750)
      end
    end

    context 'with external CF investments of $2,500' do
      let(:external) { 2500 }

      it 'should limit me to $3750K with $1250 available' do
        expect(subject.limit).to eq(3750.0)
        expect(subject.available).to eq(1250)
      end

      context 'and CF investments on FlashFunders of $1,000' do
        let(:internal) { 1000 }

        it 'should limit me to $3750K with $250 available' do
          expect(subject.limit).to eq(3750.0)
          expect(subject.available).to eq(250)
          expect(subject.over_limit).to eq(0)
        end
      end

      context 'and CF investments on FlashFunders of $2,000' do
        let(:internal) { 2000 }

        it 'should limit me to $3750K with $0 available' do
          expect(subject.limit).to eq(3750.0)
          expect(subject.available).to eq(0)
          expect(subject.over_limit).to eq(750)
        end
      end

    end
  end

  context 'with an income of $2M and a net worth of $5M' do
    let(:income) { 2000000 }
    let(:net_worth) { 5000000 }

    subject { described_class.new(income, net_worth, external, internal, reg_c_cutoff, share_price) }

    context 'investing for the first time' do

      it 'should limit me to $100K with $19,999 available' do
        expect(subject.limit).to eq(100000.0)
        expect(subject.available).to eq(reg_c_cutoff-1)
      end
    end

    context 'with external CF investments of $25,000' do
      let(:external) { 25000 }

      it 'should limit me to $100K with $19,999 available' do
        expect(subject.limit).to eq(100000.0)
        expect(subject.available).to eq(reg_c_cutoff-1)
      end

      context 'and CF investments on FlashFunders of $10,000' do
        let(:internal) { 10000 }

        it 'should limit me to $100K with 19,999 available' do
          expect(subject.limit).to eq(100000.0)
          expect(subject.available).to eq(reg_c_cutoff-1)
        end
      end

      context 'and CF investments on FlashFunders of $80K' do
        let(:internal) { 80000 }

        it 'should limit me to $100K with $0 available' do
          expect(subject.limit).to eq(100000)
          expect(subject.available).to eq(0)
          expect(subject.over_limit).to eq(5000)
        end
      end
    end
  end

end
