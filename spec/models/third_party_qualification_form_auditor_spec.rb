require 'rails_helper'

describe AccreditationFormAuditor do
  describe '#process_state' do
    it 'filters the spouse ssn' do
      auditor = AccreditationFormAuditor.new

      expect(auditor.process_state('spouse_ssn' => '123456789')).to eq('spouse_ssn' => '*****6789')
    end
  end
end
