require 'rails_helper'

describe GenericEntityForm do
  let(:attrs) { {} }
  let(:user) { FactoryGirl.build(:user) }
  let(:errors) { ActiveModel::Errors.new(double) }
  let(:validator) { double("validator", valid?: true, errors: errors) }
  let(:entity) { double("entity", id: 1) }
  let(:options) { {validator: validator, entity_class: entity} }
  let(:params) { ActionController::Parameters.new({
      name:               name,
      tax_id_number:      '123456789',
      formation_date:     '12/19/2012',
      formation_state_id: 'AL',
      address_1:          '123 Main st.',
      address_2:          '',
      city:               'Anytown',
      state_id:           'AL',
      zip_code:           '12345',
      phone_number:       '1234567890',
      position:           'Chief'
  })}
  let(:name) { 'trust' }

  subject{ GenericEntityForm.new(attrs, user, options) }

  describe '.key' do
    it 'should equal entity_form' do
      expect(described_class.key).to eq('generic_entity_form')
    end
  end

  describe '.slice_params' do
    let(:munged_params) { params.merge(undesired_key: 'fake') }

    before do
      described_class.form_reader *params.keys
    end

    it 'returns only the desired keys' do
      expect(described_class.slice_params(munged_params)).to eq(params)
    end

    it 'does not return incorrect keys' do
      expect(described_class.slice_params({undesired_key: 'fake'})).to eq({})
    end
  end

end
