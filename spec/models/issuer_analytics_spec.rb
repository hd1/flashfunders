require 'rails_helper'

describe IssuerAnalytics do
  let(:offering) { Offering.new(id: 123, user_id: 321, vanity_path: 'likeminder') }
  let(:start_date) { '2016-01-01' }
  let(:end_date) { '2016-01-15' }
  let(:subject) { IssuerAnalytics.new }

  shared_examples_for 'a Google Analytics API call' do |api|
    before do
      allow(Google::Auth).to receive(:get_application_default).and_return(nil)
      ENV['GOOGLE_PROFILE_ID'] = 'XXXXXXXX' # Use dummy profile id
    end

    context 'with data from Google' do

      it 'should transform the data to the expected format' do
        VCR.use_cassette("google_analytics_#{api}", record: :none, match_requests_on: [:method, :query]) do
          result = subject.send(api, start_date, end_date, offering, options)

          expect(result).to eql(expected)
        end
      end
    end
  end

  context 'with canned data' do
    describe '.referral_info ' do
      let(:expected) { { data:  [['hs_email', 92.0, 0.6301369863013698], ['(direct)', 32.0, 0.2191780821917808], ['linkedin.com', 10.0, 0.0684931506849315], ['Likeminder Updates 2015', 8.0, 0.0547945205479452], ['digital.nyc', 2.0, 0.0136986301369863], ['lqdwifi.com', 2.0, 0.0136986301369863]],
                         total: 146.0 } }
      let(:options) { {} }

      it_behaves_like 'a Google Analytics API call', :referral_info
    end

    describe '.page_visit_info ' do
      context 'when requesting daily data' do
        let(:expected) { { x_series: [['20160101'], ['20160102'], ['20160103'], ['20160104'], ['20160105'], ['20160106'], ['20160107'], ['20160108'], ['20160109'], ['20160110'], ['20160111'], ['20160112'], ['20160113'], ['20160114'], ['20160115']],
                           y_series: { 'ga:pageviews'       => [0.0, 0.0, 6.0, 4.0, 0.0, 4.0, 10.0, 52.0, 4.0, 2.0, 4.0, 8.0, 0.0, 12.0, 40.0],
                                       'ga:uniquePageviews' => [0.0, 0.0, 1.0, 2.0, 0.0, 2.0, 5.0, 18.0, 1.0, 1.0, 2.0, 4.0, 0.0, 5.0, 20.0],
                                       'ga:avgTimeOnPage'   => [0.0, 0.0, 0.8, 0.0, 0.0, 0.0, 0.0, 50.355555555555554, 19.0, 0.0, 42.5, 21.75, 0.0, 65.71428571428571, 30.586206896551722] },
                           totals:   { 'ga:pageviews' => '146', 'ga:uniquePageviews' => '61', 'ga:avgTimeOnPage' => '36.628571428571426' } } }
        let(:options) { { frequency: 'daily' } }

        it_behaves_like 'a Google Analytics API call', :page_visit_info
      end

      context 'when requesting weekly data' do
        let(:expected) { { x_series: [['01', '2016'], ['02', '2016'], ['03', '2016']],
                           y_series: { 'ga:pageviews'       => [0.0, 80.0, 66.0],
                                       'ga:uniquePageviews' => [0.0, 29.0, 32.0],
                                       'ga:avgTimeOnPage'   => [0.0, 37.53225806451613, 35.325581395348834] },
                           totals:   { 'ga:pageviews' => '146', 'ga:uniquePageviews' => '61', 'ga:avgTimeOnPage' => '36.628571428571426' } } }
        let(:options) { { frequency: 'weekly' } }

        it_behaves_like 'a Google Analytics API call', :page_visit_info
      end
    end
  end

  context 'with no data' do
    let(:offering) { Offering.new(id: 123, user_id: 321, vanity_path: 'no_such_company') }
    let(:expected) { {} }
    let(:options) { {} }

    describe '.referral_info' do
      it_behaves_like 'a Google Analytics API call', :referral_info
    end

    describe '.page_visit_info' do
      it_behaves_like 'a Google Analytics API call', :page_visit_info
    end

  end
end
