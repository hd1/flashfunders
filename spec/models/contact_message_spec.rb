require 'rails_helper'

describe ContactMessage, type: :model do

  let (:user) { FactoryGirl.build(:user) }
  let (:params) { { email: 'investor@example.com', name: 'Joe Investor', kind: 'investor', message: 'How do I invest?' } }

  subject { described_class.new(params) }

  it 'should create a valid entry' do
    params.each do |key, val|
      expect(subject.send(key)).to eql(val)
    end
  end

  it '#save' do
    expect(subject.save).to be_truthy
  end

  it 'should have the correct user_id' do
    user.save
    subject.user_id = user.id
    expect(subject.user).to eq(user)
  end

end
