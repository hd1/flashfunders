require 'rails_helper'

describe InvestorInvestmentSummary do
  let(:investment) do
    Investment.new(
        user_id: 5,
        offering_id: 42,
        shares: 1603,
        amount: 1503.00,
        percent_ownership: 0.567,
    )
  end
  let(:offering) { Offering.new(tagline: 'oh yes, we are the best', share_price: 1.45, id: 25) }
  let(:company) { Company.new(name: 'Toasters do not Toast Toast, Toast toasts Toast', offering_id: 25) }

  subject(:summary) do
    InvestorInvestmentSummary.new(
        investment,
        offering
    )
  end

  describe '#company_name' do
    it 'delegates to company' do
      company.save
      expect(summary.company_name).to eq(company.name)
    end
  end

  describe '#campaign_tagline' do
    it 'delegates to offering' do
      expect(summary.campaign_tagline).to eq(offering.tagline)
    end
  end

  describe '#formatted_amount' do
    it 'returns the amount as a currency' do
      expect(summary.formatted_amount).to eq('$1,503.00')
    end
  end

  describe '#number_of_shares' do
    it 'delegates the number of shares' do
      expect(summary.number_of_shares).to eq('1,603')
    end
  end

  describe '#formatted_ownership_percentage' do
    it 'returns the ownership percentage in percent format' do
      expect(summary.formatted_ownership_percentage).to eq('0.567%')
    end
  end

  describe '#formatted_share_price' do
    it 'returns the share price as a currency' do
      expect(summary.formatted_share_price).to eq('$1.450')
    end
  end
end