require 'rails_helper'

describe Company do
  subject { described_class.new(name: 'Iron Bank of Braavos') }

  it 'should validate website_url as invalid urls' do
    invalid_urls = [
      'https://www.aluguest.com/ http://www.alupays.com/',
      'random.com',
      '//www.example.com'
    ]

    invalid_urls.each do |url|
      subject.website_url = url
      subject.save

      expect(subject).to_not be_valid
    end
  end

  it 'should validate website_url as valid urls' do
    valid_urls = [
      'https://www.aluguest.com/',
      'https://random.com',
      'http://www.example.com'
    ]

    valid_urls.each do |url|
      subject.website_url = url
      subject.save

      expect(subject).to be_valid
    end
  end
end
