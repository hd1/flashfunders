require 'rails_helper'

describe AchDebitFormAuditor do

  subject(:auditor) { AchDebitFormAuditor.new }

  describe '#write' do
    it 'creates an AuditLogEntry' do
      expect {
        auditor.write({},nil,nil,nil,nil)
      }.to change(AuditLogEntry, :count).from(0).to(1)
    end

    it 'create an AuditLogEntry with the correct attributes' do
      timestamp = Time.now.utc

      auditor.write({first_name: 'foo'}, 1, '1.2.3.4', '/foo/path', timestamp)

      audit_log_entry = AuditLogEntry.last

      expect(audit_log_entry.acting_user_id).to eq(1)
      expect(audit_log_entry.acting_user_ip_address).to eq('1.2.3.4')
      expect(audit_log_entry.form_path).to eq('/foo/path')
      expect(audit_log_entry.persisted_state).to eq('first_name' => 'foo')
      expect(audit_log_entry.persisted_time.to_i).to eq(timestamp.to_i)
    end

    it 'sanitizes bank account for AuditLogEntry Record' do
      auditor.write({first_name: 'foo', bank_account_number: '123456789'}, 1, '1.2.3.4', '/foo/path', Time.now)

      expect(AuditLogEntry.last.persisted_state['bank_account_number']).to eq('*****6789')
    end

    it 'converts investment amount BigDecimal to 2 decimal point string for AuditLogEntry Record' do
      auditor.write({first_name: 'foo', investment_amount: 100.00.to_d}, 1, '1.2.3.4', '/foo/path', Time.now)

      expect(AuditLogEntry.last.persisted_state['investment_amount']).to eq('100.00')
    end
  end
end