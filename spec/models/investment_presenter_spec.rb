require 'rails_helper'

describe InvestmentPresenter do
  let(:investment) { Investment.new(user_id: 1, offering_id: 2, amount: 2001.43, shares: 4382, percent_ownership: 1.021, updated_at: Time.parse('01/25/1985 12:34pm')) }
  let(:offering) { Offering.new(id: 2)}
  let (:user) { User.new(id: 1, registration_name: 'Joe Investor') }

  subject(:presenter) { InvestmentPresenter.new(investment) }

  describe '#formatted_amount' do
    it 'returns the amount formatted as money' do
      expect(presenter.formatted_amount).to eq('$2,001.43')
    end
  end

  describe '#amount' do
    it 'returns the raw amount' do
      expect(presenter.amount).not_to be(nil)
    end
  end

  describe '#shares_owned' do
    it 'returns the shares owned' do
      expect(presenter.shares_owned).to eq('4,382')
    end
  end

  describe '#formatted_updated_at' do
    it 'returns the formatted last updated month and day' do
      expect(presenter.formatted_updated_at).to eq('01/25')
    end
  end

  describe '#investor_name' do
    before do
      allow(investment).to receive(:user).and_return(user)
    end

    it 'returns the registration name of the investor' do
      expect(presenter.investor_name).to eq('Joe Investor')
    end
  end

  describe '#investor_messages' do
    it 'returns the set of messages for this offering' do
      expect(presenter.investor_messages.count).to eq(0)
    end
  end

  describe '#formatted_percent_ownership' do
    it 'returns the formatted percent ownership' do
      expect(presenter.formatted_percent_ownership).to eq('1.021%')
    end
  end

  shared_examples_for 'a pending investment status' do
    describe '#title' do
      it 'returns the correct title' do
        expect(subject.title).to eq(I18n.t('investment_dashboard.my_investments_tab.statuses.pending.title'))
      end
    end

    describe '#status_type' do
      it 'returns "pending"' do
        expect(subject.status_type).to eq('pending')
      end
    end
  end

  describe '#accreditation_state' do
    context 'the accreditation process is completely verified' do
      before do
        allow(investment).to receive(:accreditation_verified?).and_return(true)
        allow(investment).to receive(:has_verified_accreditation?).and_return(true)
        allow(investment).to receive(:verified?).and_return(true)
      end
      it 'returns complete if the investor is accredited' do
        expect(presenter.accreditation_state).to eq('complete')
      end
    end

    context 'is not completely verified' do
      before do
        allow(investment).to receive(:accreditation_verified?).and_return(false)
        allow(investment).to receive(:has_verified_accreditation?).and_return(true)
        allow(investment).to receive(:verified?).and_return(false)
      end

      it 'returns pending if the investor is past the step but has no qualification yet' do
        expect(presenter.accreditation_state).to eq('pending')
      end
    end

    context 'is not verified at all' do
      before do
        allow(investment).to receive(:accreditation_verified?).and_return(false)
        allow(investment).to receive(:has_verified_accreditation?).and_return(false)
        allow(investment).to receive(:verified?).and_return(false)
      end
      it 'is incomplete when the user has not completed the step yet' do
        expect(presenter.accreditation_state).to eq('incomplete')
      end
    end
  end

  describe '#investment_display_state' do
    context 'returns "complete" when' do
      before do
        allow(investment).to receive(:pending_transfer?).and_return(false)
        allow(investment).to receive(:escrowed?).and_return(true)
        allow(investment).to receive(:complete?).and_return(true)
        allow(investment).to receive(:pending_transfer?).and_return(false)
        allow(investment).to receive(:offering).and_return(offering)
        allow(offering).to receive(:escrow_closed?).and_return(false)
      end
      it 'the investment has been transferred' do
        expect(presenter.investment_display_state).to eq('complete')
      end
    end

    context 'returns "incomplete" when' do
      before do
        allow(investment).to receive(:escrowed?).and_return(false)
        allow(investment).to receive(:complete?).and_return(false)
        allow(investment).to receive(:pending_transfer?).and_return(false)
        allow(investment).to receive(:offering).and_return(offering)
        allow(offering).to receive(:escrow_closed?).and_return(false)
      end
      it 'the investment has not been transferred' do
        expect(presenter.investment_display_state).to eq('incomplete')
      end
    end

    context 'returns "pending" when wire is selected' do
      before do
        allow(investment).to receive(:funding_method_selected?).and_return(true)
        allow(investment).to receive(:pending_transfer?).and_return(true)
        allow(investment).to receive(:offering).and_return(offering)
        allow(offering).to receive(:escrow_closed?).and_return(false)
      end
      it 'the investment has selected wire transfer' do
        expect(presenter.investment_display_state).to eq('pending')
      end
    end
  end

  describe '#accreditor_data' do
    let(:investor_entity) { FactoryGirl.create(:investor_entity_individual) }
    let(:qualification_tp) { Accreditor::ThirdPartyIncomeQualification.new(
      investor_entity_id: investor_entity.id,
      email: 'accountant@example.com',
      role: 'Certified Public Accountant',
      user_id: investment.user_id
    )}

    let(:qualification_jo) { Accreditor::JointNetWorthQualification.new(
      investor_entity_id: investor_entity.id,
      spouse_name: 'Wilma',
      spouse_email: 'wilma@flintstones.net',
      spouse_ssn: '123129999',
      user_id: investment.user_id
    )}

    before do
      allow(investment).to receive(:current_accreditor_qualification).and_return(qualification_tp)
    end

    subject { presenter.accreditor_data }

    context 'Third Party Qualifications' do
      it 'has a method field' do
        expect(subject[:method]).to eq('Third Party Income')
      end

      it 'has a role and email field' do
        expect(subject[:role]).to eq('Certified Public Accountant')
        expect(subject[:email]).to eq('accountant@example.com')
      end

      it 'has a state' do
        expect(subject[:state]).to eq('')
      end

      it 'is set to pending if emailed_at is present' do
        qualification_tp.emailed_at = 3.days.ago
        expect(subject[:state]).to eq('pending')
      end

      it 'can be expired' do
        qualification_tp.expires_at = 1.month.ago
        expect(subject[:state]).to eq('expired')
      end

      it 'can be declined' do
        qualification_tp.declined_at = 1.month.ago
        expect(subject[:state]).to eq('declined')
      end

      it 'can be verified' do
        qualification_tp.verified_at = 1.month.ago
        expect(subject[:state]).to eq('verified')
      end
    end

    context 'Non Third party qualifications' do

      let(:docs) {
        2.times.map do |i|
          OpenStruct.new.tap do |main_doc|
            main_doc.document = OpenStruct.new.tap do |doc|
              doc.filename = "somefile_#{i}.pdf"
              doc.url = "http://domain.com/somefile_#{i}.pdf"
            end
          end
        end
      }

      before do
        allow(investment).to receive(:current_accreditor_qualification).and_return(qualification_jo)
        allow(qualification_jo).to receive(:documents).and_return(docs)
      end

      it 'has a method field' do
        expect(subject[:method]).to eq('Joint Net Worth')
      end

      it 'has a pending state by default' do
        expect(subject[:state]).to eq('pending')
      end

      it 'has a docs field' do
        expect(subject[:docs]).to be_present
      end

      it 'provides name and url for the documents' do
        doc0 = subject[:docs][0]
        doc1 = subject[:docs][1]
        expect(doc0[:name]).to eq('somefile_0.pdf')
        expect(doc0[:url]).to eq('http://domain.com/somefile_0.pdf')
        expect(doc1[:name]).to eq('somefile_1.pdf')
        expect(doc1[:url]).to eq('http://domain.com/somefile_1.pdf')
      end

      it 'can be expired' do
        qualification_jo.expires_at = 1.month.ago
        expect(subject[:state]).to eq('expired')
      end

      it 'can be declined' do
        qualification_jo.declined_at = 1.month.ago
        expect(subject[:state]).to eq('declined')
      end

      it 'can be verified' do
        qualification_jo.verified_at = 1.month.ago
        expect(subject[:state]).to eq('verified')
      end

    end
  end

  describe '#can_cancel_investment' do
    before { offering.update_attribute(:ends_at, 3.days.from_now) }

    context 'Reg C investment' do
      before { allow(investment).to receive(:is_reg_c?).and_return(true) }

      it 'can cancel an investment' do
        investment.update_attribute(:state, 'funding_method_selected')
        expect(presenter.can_cancel_investment?).to be_truthy
      end

      it 'cannot cancel an investment' do
        expect(presenter.can_cancel_investment?).to be_falsy
      end
    end

    context 'Non Reg C investment' do
      before { allow(investment).to receive(:is_reg_c?).and_return(false) }

      it 'cannot cancel an investment' do
        investment.update_attribute(:state, 'funds_sent')
        expect(presenter.can_cancel_investment?).to be_falsy
      end
    end
  end

  describe '#cooling_off_ended' do
    it 'should be in cooling off period' do
      offering.update_attribute(:ends_at, 3.day.from_now)
      expect(presenter.cooling_off_ended?).to be_falsy
    end

    it 'the cooling off period ended' do
      offering.update_attribute(:ends_at, 1.day.from_now)
      expect(presenter.cooling_off_ended?).to be_truthy
    end
  end
end
