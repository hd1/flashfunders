require 'rails_helper'

describe GenericEntityFormValidator do

  def new_generic_entity_form_validator_with_doubles_injected(attributes)
    GenericEntityFormValidator.new(attributes, investment)
  end

  let(:attributes) do
    {
      high_risk_agree: '1',
      experience_agree: '1',
      loss_risk_agree: '1',
      time_horizon_agree: '1',
      cancel_agree: '1',
      resell_agree: '1'
    }
  end

  context 'with a Reg C investment' do
    let(:investment) { double(Investment) }
    before do
      allow(investment).to receive(:is_reg_c?).and_return(true)
    end

    describe '#valid?' do

      it 'is invalid by default' do
        expect(new_generic_entity_form_validator_with_doubles_injected({})).not_to be_valid
      end

      it 'can be valid' do
        expect(new_generic_entity_form_validator_with_doubles_injected(attributes)).to be_valid
      end

    end

    describe 'with invalid attributes' do
      it 'flags the missing field' do
        attributes.each_with_index do |(attr, val), index|
          attributes[attr] = '0'
          expect(new_generic_entity_form_validator_with_doubles_injected(attributes)).not_to be_valid
          attributes[attr] = '1'
        end
      end
    end
  end
end
