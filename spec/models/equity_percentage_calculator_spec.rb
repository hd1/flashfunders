require 'rails_helper'

describe EquityPercentageCalculator do

  describe '#percent_offered' do
    context 'is the shares offered divided by the sum of shares offered and fully diluted shares, represented as a percentage rounded to two decimals' do
      it 'and rounds down correctly' do
        expect(EquityPercentageCalculator.new(50, 100).percent_offered).to eq(33.33)
      end

      it 'and rounds up correctly' do
        expect(EquityPercentageCalculator.new(75, 37.5).percent_offered).to eq(66.67)
      end
    end
  end
end