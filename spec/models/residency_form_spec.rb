require 'rails_helper'

describe ResidencyForm do

  let(:user) { create_user }
  let(:attributes) { {} }

  subject { described_class.new(attributes, user) }

  describe '#save' do

    context 'with an unregistered user' do
      let(:user) { nil }
      it 'should not save' do
        expect(subject.save).to be_falsy
      end
    end

    context 'with a partially registered user' do
      let(:user) { create_temporary_user(email: 'joe@temp.com', registration_name: 'Joe Temp') }

      it 'should set the missing attributes' do
        expect(user.us_citizen).to be_blank
        expect(subject.save).to be_truthy
        user.reload
        expect(user.us_citizen).to be_truthy
        expect(user.country).to eq('US')
      end
    end

  end

end
