require 'rails_helper'

describe InvestorFundFormValidator do

  it 'returns false when no valid_attributes are passed' do
    expect(described_class.new).to_not be_valid
  end

  let(:base_params) do
    {
      bank_account_number: 12345678912345678,
      bank_account_routing: 123456789,
      bank_account_type: 'CHECKING',
      bank_account_holder: 'Fred Smith',
    }
  end

  subject { described_class.new(params) }

  before do
    RoutingNumber.create!(routing_number: 123456789, name: 'Bank of Vinnie')
  end

  context 'ACH Transfer' do
    context 'with valid data' do
      let(:params) { base_params.merge(funding_method: 'ach', authorization: true) }

      it 'is valid' do
        expect(subject).to be_valid
      end
    end

    context 'missing authorization' do
      let(:params) { base_params.merge(funding_method: 'ach') }

      it 'should require authorization' do
        expect(subject).to be_invalid
        expect(subject.errors.count).to eq(1)
        expect(subject.errors[:authorization]).to include(I18n.t('fund_flow.authorization.error_message'))
      end
    end

    context 'invalid routing number' do
      let(:params) { base_params.merge(funding_method: 'ach', authorization: true, bank_account_routing: 'ABCD') }

      it 'should require a known routing number' do
        expect(subject).to be_invalid
        expect(subject.errors.count).to eq(1)
        expect(subject.errors[:bank_account_routing]).to include(I18n.t('fund_flow.ach.link_refund_account.invalid.routing_number'))
      end
    end
  end

  context 'Wire Transfer' do
    let(:params) { base_params.merge(funding_method: 'wire') }

    it 'can be valid' do
      expect(subject).to be_valid
    end
  end

end