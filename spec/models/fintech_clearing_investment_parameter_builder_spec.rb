require 'rails_helper'

describe FintechClearingInvestmentParameterBuilder do

  let(:offering) { double(Offering, share_price: BigDecimal.new('500.00')) }
  let(:security) { double(Securities::Security, fund_america_id: 'd9175bf5-59f8-4016-acbf-94de6a65432f') }
  let(:bank_account) { double(Bank::InvestorAccount, id: 99, routing_number: '111111111',
    account_type: 'CHECKING',
    account_number: '12345678991',
    account_holder: 'Lanister',
  )}
  let(:investment) { double(Investment, security: security,
    amount: BigDecimal.new(5000),
    investor_entity: investor_entity,
    user: investor_user,
    by_individual?: investment_by_individual?,
    is_spv?: false,
    is_reg_c?: false,
    fund_america_id: nil,
    funding_method: "wire",
    ach?: true,
    investor_account_id: 99,
    funding_method_selected?: true) }

  let(:investor_user) { double(User, email: 'john.investor@example.com', id: 101) }

  let(:personal_federal_identification_information) do
    double(PersonalFederalIdentificationInformation,
      full_name: 'Patryk Kopec',
      first_name: 'Patryk',
      last_name: 'Kopec',
      phone_number: '1112223333',
      email: 'email@poland.com',
      address1: 'Thousandstreet',
      address2: '',
      city: 'Santa Monica',
      state_id: 'CA',
      country: 'US',
      zip_code: '12345',
      date_of_birth: Date.new(1975,2,1),
      ssn: '1234567890'
    )
  end

  before do
    allow(Bank::InvestorAccount).to receive(:find).and_return(bank_account)
  end

  describe '.build' do
    context 'with a individual investor' do
      let(:investor_entity) { double(InvestorEntity,
        country: nil, city: nil, full_name: 'John Q Investor',
        state_id: nil, address_1: nil, zip_code: nil,
        phone_number: nil, tax_id_number: nil,
        personal_federal_identification_information: personal_federal_identification_information,
        fund_america_id: '',
        fund_america_ach_authorization_id: ''
      ) }

      let(:investment_by_individual?) { true }

      it 'should build the correct parameters' do
        expect(FintechClearingInvestmentParameterBuilder.build(investment))
          .to eq({
            investment: {
              uuid: nil,
              offering_uuid: 'd9175bf5-59f8-4016-acbf-94de6a65432f',
              amount: '5000.00',
              funding_method: 'funding_method_ach',
              bank_routing_number: '111111111',
              bank_account_type: 'bank_account_type_checking',
              bank_account_number: '12345678991',
              bank_account_holder: 'Lanister',
              investor_first_name: 'Patryk',
              investor_last_name: 'Kopec',
              investor_phone: '1112223333',
              investor_email: 'john.investor@example.com',
              investor_address1: 'Thousandstreet',
              investor_address2: '',
              investor_city: 'Santa Monica',
              investor_state: 'CA',
              investor_country: 'US',
              investor_postal_code: '12345',
              investor_date_of_birth: '1975-02-01',
              investor_ssn: '1234567890'
            }
          })
      end
    end

    context 'with a corporate investor' do
      let(:investor_entity) { double(InvestorEntity,
        name: 'Biz Baz Incorporated',
        full_name: personal_federal_identification_information.full_name,
        display_name: 'Trustee',
        position: 'CTO',
        state_id: 'AA',
        formation_state_id: 'BB',
        formation_date: '',
        executive_name: 'Bilbo Baggins',
        address_1: '123 Fake Street',
        address_2: '',
        city: 'London',
        country: investor_entity_country,
        phone_number: '1231231234',
        tax_id_number: '123456789',
        zip_code: '123145') }

      let(:investment_by_individual?) { false }
      let(:investor_entity_country) { 'UK' }

      it 'should build the correct parameters' do
        expect(FintechClearingInvestmentParameterBuilder.build(investment))
          .to eq({
            investment:{
              uuid: nil,
              offering_uuid: 'd9175bf5-59f8-4016-acbf-94de6a65432f',
              amount: '5000.00',
              funding_method: 'funding_method_ach',
              bank_routing_number: '111111111',
              bank_account_type: 'bank_account_type_checking',
              bank_account_number: '12345678991',
              bank_account_holder: 'Lanister',
              entity_name: 'Patryk Kopec',
              entity_type: 'Trustee',
              entity_signatory_title: 'CTO',
              entity_phone: '1231231234',
              entity_ein: '123456789',
              entity_state_of_domicile: 'BB',
              entity_date_of_formation: '',
              entity_address1: '123 Fake Street',
              entity_address2: '',
              entity_city: 'London',
              entity_state: 'AA',
              entity_country: investor_entity_country,
              entity_postal_code: '123145'
            }
          })
      end

      context 'without specify country' do
        let(:investor_entity_country) { nil }
        it 'should assume US' do
          expect(FintechClearingInvestmentParameterBuilder.build(investment)[:investment][:entity_country]).to eq('US')
        end
      end
    end
  end

end
