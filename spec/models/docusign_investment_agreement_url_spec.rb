require 'rails_helper'

describe DocusignInvestmentAgreementUrl do
  subject(:docusign_investment_agreement_url_fetcher) { DocusignInvestmentAgreementUrl }
  let(:docusign_client_double) { double(DocusignRest::Client, get_recipient_view: {'url' => 'recipient_url'}) }

  before { allow(subject).to receive(:docusign_client).and_return(docusign_client_double) }

  describe '#get_embedded_url' do
    let(:valid_args) { ['envelope_id', 'INVESTOR GUY', 'investor@example.com', 'return_url'] }

    it 'calls docusign to get the recipient view' do
      subject.get_embedded_url(*valid_args)

      expect(docusign_client_double).to have_received(:get_recipient_view).with(
                                            envelope_id: 'envelope_id',
                                            name: 'INVESTOR GUY',
                                            email: 'investor@example.com',
                                            return_url: 'return_url',
                                        )
    end

    it 'returns the url from the recipient view hash' do
      expect(subject.get_embedded_url(*valid_args)).to eq('recipient_url')
    end
  end

end