require 'rails_helper'

describe InvestorFundForm do
  let!(:user) { create_user }

  let(:account_params) {
    {
        bank_account_number:  '12345678912345678',
        bank_account_routing: '123456789',
        bank_account_holder:  'Joe Smith',
        bank_account_type:    'Checking'
    }
  }
  let(:validator_double) { double(InvestorFundFormValidator, errors: ActiveModel::Errors.new(double('RandomClass'))) }
  let(:investment_double) { double(Investment) }
  let(:investor_fund_form_params) { { params: account_params, validator: validator_double } }
  let(:offering_double) { mock_model(Offering, id: 78, ended?: false, convertible?: false) }
  let(:escrow_provider_double) { mock_model(EscrowProvider, id: 78, wire_code: 'XXX')}
  subject(:investor_fund_form) {
    described_class.new(user, investment_double, investor_fund_form_params)
  }

  before do
    allow(Offering).to receive(:find).and_return(offering_double)
    allow(investment_double).to receive(:offering).and_return(offering_double)
    allow(offering_double).to receive(:escrow_provider).and_return(escrow_provider_double)
    allow(EscrowProvider).to receive(:find).and_return(escrow_provider_double)
  end

  describe '#save' do
    before {
      # allow(Bank::InvestorAccount).to receive(:create!).and_return(true)
      allow(investment_double).to receive(:funding_method).and_return('checking')
      allow(investment_double).to receive(:funding_method=).and_return(true)
      allow(investment_double).to receive(:investor_account_id).and_return(true)
      allow(investment_double).to receive(:investor_account_id=).and_return(true)
      allow(investment_double).to receive(:save).and_return(true)
    }
    it 'returns true' do
      expect(investor_fund_form.save).to eq(true)
    end
  end

  describe '#errors' do
    it 'delegates errors to the validator' do
      expect(investor_fund_form.errors).to eq(validator_double.errors)
    end
  end

  describe '#bank_account_type_i18n_label' do
    context 'when "checking" account' do
      let(:account_params) { { bank_account_type: 'CHECKING' } }

      it 'displays the i18n key' do
        expect(subject.bank_account_type_i18n_label).to eq('linked_external_account_type.option_1')
      end
    end

    context 'when "savings" account' do
      let(:account_params) { { bank_account_type: 'SAVINGS' } }

      it 'displays the i18n key' do
        expect(subject.bank_account_type_i18n_label).to eq('linked_external_account_type.option_2')
      end
    end
  end

  describe "#bank_account_routing" do
    let(:investor_fund_form_params) { { params: { bank_account_routing: routing_number } } }
    context 'with a valid routing number' do
      let(:routing_number) { '123+ 456--789 ' }
      it 'should strip non-numeric characters from routing number' do
        expect(subject.bank_account_routing).to eq('123456789')

        expect(subject.send(:validator).send(:bank_account_routing)).to eq('123456789')
      end
    end
  end

  describe '#update' do
    let!(:account1) { Bank::InvestorAccount.create!({ user_id:        user.id,
                                                      account_holder: 'Account Holder',
                                                      account_number: '012345678901',
                                                      routing_number: '011000390',
                                                      account_type:   'CHECKING'
                                                    }) }
    let!(:account2) { Bank::InvestorAccount.create!({ user_id:        user.id,
                                                      account_holder: 'Account Holder',
                                                      account_number: '012345678902',
                                                      routing_number: '011000390',
                                                      account_type:   'CHECKING'
                                                    }) }
    let!(:account3) { Bank::InvestorAccount.create!({ user_id:        user.id,
                                                      account_holder: 'Account Holder',
                                                      account_number: '012345678903',
                                                      routing_number: '011000390',
                                                      account_type:   'CHECKING'
                                                    }) }

    let!(:investment1) { create_investment({ user_id: user.id, investor_account_id: account1.id }) }
    let!(:investment2) { create_investment({ user_id: user.id, investor_account_id: account2.id }) }

    context 'A user with two investments and 3 accounts' do
      let (:form1) { InvestorFundForm.new(user, investment1, { params: { user_id:              user.id,
                                                                         bank_account_id:      account1.id,
                                                                         bank_account_holder:  'Account Holder',
                                                                         bank_account_number:  '012345678901',
                                                                         bank_account_routing: '011000390',
                                                                         bank_account_type:    'CHECKING',
                                                                         funding_method:       'check'
                                              } }) }

      it 'Should remove unassigned account3 after update' do
        expect(Bank::InvestorAccount.where(user_id: user.id).count).to eq(3)
        form1.update account1
        expect(Bank::InvestorAccount.where(user_id: user.id).count).to eq(2)
        expect(Bank::InvestorAccount.where(id: account3.id).count).to eq(0)
      end

      let (:form2) { InvestorFundForm.new(user, investment2, { params: { user_id:              user.id,
                                                                         bank_account_id:      account2.id,
                                                                         bank_account_holder:  'Account Holder',
                                                                         bank_account_number:  '012345678903',
                                                                         bank_account_routing: '011000390',
                                                                         bank_account_type:    'CHECKING',
                                                                         funding_method:       'check'
                                              } }) }

      it 'Should update account3 and remove account2 after update' do
        expect(Bank::InvestorAccount.where(user_id: user.id).count).to eq(3)
        form2.update account2
        expect(Bank::InvestorAccount.where(user_id: user.id).count).to eq(2)
        expect(Bank::InvestorAccount.where(id: account2.id).count).to eq(0)
      end
    end

    context 'A user with two investments sharing the same refund account' do
      let! (:investment3) { create_investment({ user_id: user.id, investor_account_id: account1.id }) }
      let (:form1) { InvestorFundForm.new(user, investment3, { params: { user_id:              user.id,
                                                                         bank_account_id:      account1.id,
                                                                         bank_account_holder:  'Account Holder',
                                                                         bank_account_number:  '012345678904',
                                                                         bank_account_routing: '011000390',
                                                                         bank_account_type:    'CHECKING',
                                                                         funding_method:       'wire'
                                              } }) }

      it 'Should create a new account after update and remove account 3' do
        expect(Bank::InvestorAccount.where(user_id: user.id).count).to eq(3)
        form1.update account1
        expect(Bank::InvestorAccount.where(user_id: user.id).count).to eq(3)
        expect(Bank::InvestorAccount.where(id: account2.id).count).to eq(1)
        expect(Bank::InvestorAccount.where(id: account3.id).count).to eq(0)
      end
    end
  end
end
