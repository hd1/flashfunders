require 'rails_helper'

module Bank
  describe Account do
    let(:bank_name) { nil }
    let(:bank_account) { Bank::Account.new account_number: '123456789', bank_name: bank_name }

    describe '#last_four' do
      it 'should be the last four digits of the account number' do
        expect(bank_account.last_four).to eq('6789')
      end
    end

    describe '#bank_name' do
      subject { bank_account.bank_name }

      context 'the attribute exists' do
        let(:bank_name) { "Bank Name Goes Here" }

        it 'should be the attributes value' do
          expect(subject).to eq(bank_name)
        end
      end

      context 'the routing number exists' do
        before do
          allow(RoutingNumber).to receive(:find_by_routing_number).and_return(double(RoutingNumber, name: 'Some bank name'))
        end

        it 'should use that value' do
          expect(subject).to eq('Some bank name')
        end
      end
    end
  end
end
