require 'rails_helper'

describe IdologyEntityParameterBuilder do

  describe '.build' do
    context 'domestic individual investor entity' do
      let(:individual_investor_entity) { FactoryGirl.create(:investor_entity_individual, :with_pfii) }

      before do
        individual_investor_entity.create_personal_federal_identification_information(
          first_name: 'John',
          last_name: 'Smith',
          address1: '222333 Peachtree Place',
          address2: 'Mars',
          city: 'Atlanta',
          state_id: 'GA',
          zip_code: '30318',
          date_of_birth: Date.new(1975,2,1),
          daytime_phone: '333-333-4444',
          ssn: '112223333')
      end

      it 'should build the correct parameters' do
        pfii = individual_investor_entity.personal_federal_identification_information

        expect(IdologyEntityParameterBuilder.build(individual_investor_entity))
          .to eq(
            {
              firstName: pfii.first_name,
              lastName: pfii.last_name,
              address: pfii.address1 + pfii.address2,
              city: pfii.city,
              state: pfii.state_id,
              zip: pfii.zip_code,
              dobMonth: pfii.date_of_birth.month,
              dobYear: pfii.date_of_birth.year,
              telephone: pfii.phone_number,
              ssn: pfii.ssn
            }
          )
      end
    end
  end
end
