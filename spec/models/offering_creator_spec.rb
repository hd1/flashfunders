require 'rails_helper'

describe OfferingCreator do
  describe 'create' do

    subject(:offering_creator) { OfferingCreator.new }

    it 'creates a Campaign for the user' do
      expect(-> { offering_creator.create(user_id: 1) }).to change(Offering, :count).from(0).to(1)

      offering = Offering.first

      expect(offering.user_id).to eq(1)
      expect(offering.visibility_public?).to eq(false)
    end

    it 'returns the campaign' do
      offering = offering_creator.create(user_id: 1)
      expect(offering.user_id).to eq(1)
    end

    it 'creates a Company associated with the campaign' do
      expect(-> { offering_creator.create(user_id: 1) }).to change(Company, :count).from(0).to(1)

      offering = Offering.first

      expect(Company.first.offering_id).to eq(offering.id)
    end

    it 'create a Corporate Federal Identification Information associated with the Company' do
      expect(-> { offering_creator.create(user_id: 1) }).to change(CorporateFederalIdentificationInformation, :count).from(0).to(1)

      company = Company.first

      expect(CorporateFederalIdentificationInformation.first.company_id).to eq(company.id)
    end

  end
end

