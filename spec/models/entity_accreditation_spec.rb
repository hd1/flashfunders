require 'rails_helper'

describe EntityAccreditation do

  let(:entity) { double(InvestorEntity) }
  let(:history) { double(Accreditor::StatusHistory) }
  let(:current_status) { double }

  before { allow(history).to receive(:get_status).and_return(current_status) }

  subject { described_class.new(entity, status_history: history) }

  describe '#status' do
    it 'returns the status for the current date' do
      Timecop.freeze do
        expect(subject.status).to eql(current_status)

        expect(history).to have_received(:get_status).with(Date.current)
      end
    end
  end

  describe '#previously_approved?' do
    before { allow(history).to receive(:has_occurred?).and_return(:something) }

    it 'returns whether or not the status has been approved' do
      expect(subject.previously_approved?).to eql(:something)

      expect(history).to have_received(:has_occurred?).with(Accreditor::Status.approved)
    end
  end

  describe '#qualification' do
    before { allow(current_status).to receive(:qualification).and_return(:something) }

    it 'returns the status\' qualification' do
      expect(subject.qualification).to eql(:something)
    end
  end

  describe '#expiration_date' do
    before { allow(current_status).to receive(:expiration_date).and_return(:something) }

    it 'returns the status\' expiration' do
      expect(subject.expiration_date).to eql(:something)
    end
  end

  describe '#days_until_expiration' do
    let(:date) { 6.days.from_now.to_date }

    before { allow(current_status).to receive(:expiration_date).and_return(date) }

    it 'returns the days until expiration' do
      expect(subject.days_until_expiration).to eql(6)
    end
  end

end

