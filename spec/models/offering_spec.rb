require 'rails_helper'

describe Offering do

  let(:valid_params) do
    { }
  end

  subject(:offering) { Offering.new }
  # before { offering.escrow_provider = FactoryGirl.build(:escrow_provider_manual) }

  it_behaves_like 'it is uniquely identifiable'

  describe "validations" do
    describe "#vanity_path" do
      before { offering.vanity_path = vanity_path }

      context 'with numbers, letters, and dashes' do
        let(:vanity_path) { 'My-01-vanity-Path9' }
        it { should be_valid }
      end
      context 'blank' do
        let(:vanity_path) { '' }
        it { should be_valid }
      end
      context 'too long' do
        let(:vanity_path) { "a" * 256 }
        it { should_not be_valid }
      end
      context 'with spaces' do
        let(:vanity_path) { "my vanity path" }
        it { should_not be_valid }
      end
    end
  end

  describe '#ended?' do
    context 'when there is no end date' do
      before { offering.ends_at = nil }
      it { should_not be_ended }
    end

    context 'when the end date is in the past' do
      before { offering.ends_at = 3.days.ago }
      it { should be_ended }
    end

    context 'when the end date is today' do
      before { offering.ends_at = Time.current }
      it { should be_ended }
    end

    context 'when the end date is in the future' do
      before { offering.ends_at = 1.day.from_now }
      it { should_not be_ended }
    end
  end

  describe '#escrow_closed?' do
    context 'when there is no escrow end date' do
      before { offering.escrow_end_date = nil}
      it { should_not be_escrow_closed }
    end

    context 'when the escrow end date is in the past' do
      before { offering.escrow_end_date = 1.days.ago }
      it { should be_escrow_closed }
    end

    context 'when the escrow end date is today' do
      before { offering.escrow_end_date = Time.current }
      it { should_not be_escrow_closed }
    end

    context 'when the escrow end date is in the future' do
      before { offering.escrow_end_date = 1.day.from_now }
      it { should_not be_escrow_closed }
    end
  end

  describe '#vanity_path=' do
    it 'should let you set all lowercase' do
      offering.vanity_path = 'my-vanity-url'
      expect(offering.vanity_path).to eq('my-vanity-url')
    end
    it 'should convert to lowercase' do
      offering.vanity_path = 'CAPITAL-LETTERS'
      expect(offering.vanity_path).to eq('capital-letters')
    end
  end

end
