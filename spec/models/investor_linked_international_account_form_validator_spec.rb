require 'rails_helper'

describe InvestorLinkedInternationalAccountFormValidator do
  it 'returns false when no valid_attributes are passed' do
    expect(InvestorLinkedInternationalAccountFormValidator.new).to_not be_valid
  end

  let(:valid_attributes) do
    {
      bank_account_number: 12345678912345678,
      bank_account_routing: 123456789,
      bank_account_holder: 'Fred Smith',
      passport_image: 'Some Image'
    }
  end

  let(:validator) { InvestorLinkedInternationalAccountFormValidator.new(valid_attributes) }

  context 'when valid' do
    it 'can be valid' do
      expect(validator).to be_valid
    end
  end

  context 'when invalid' do
    context 'bank_account_number' do
      it 'is required' do
        valid_attributes[:bank_account_number] = nil

        expect(validator).to_not be_valid
        expect(validator.errors[:bank_account_number]).to eq(['Required'])
      end
    end

    context 'bank_account_holder' do
      it 'is required' do
        valid_attributes[:bank_account_holder] = ''
        expect(validator).to_not be_valid
        expect(validator.errors[:bank_account_holder]).to eq(['Required'])
      end
    end

    context 'bank_account_routing' do
      it 'is required' do
        valid_attributes[:bank_account_routing] = ''
        expect(validator).to_not be_valid
        expect(validator.errors[:bank_account_routing]).to eq(['Required'])
      end
    end

    context 'passport_image' do
      it 'is required' do
        valid_attributes[:passport_image] = ''
        expect(validator).to_not be_valid
        expect(validator.errors[:passport_image]).to eq(['Required'])
      end
    end
  end
end
