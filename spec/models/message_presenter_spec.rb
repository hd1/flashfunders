require 'rails_helper'

describe MessagePresenter do
  let(:issuer_user) { create_user(email: 'issuer@example.com', password: 'password', password_confirmation: 'password') }
  let(:company) { Company.create!(name: "Testco") }
  let(:offering) { Offering.create!(issuer_user_id: issuer_user.id, company: company) }
  let(:message) { OfferingMessage.create user_id: issuer_user.id, offering_id: offering.id, created_at: Time.current + 1.minute }

  let(:offering_message) { described_class.new(message, current_offering: offering) }
  let(:other_message) { described_class.new(message) }

  describe '#is_offering_message?' do
    it 'is offering message' do
      expect(offering_message.is_offering_message?).to be_truthy
    end

    it 'is not offering message' do
      expect(other_message.is_offering_message?).to be_falsy
    end
  end

  describe '#company_name' do
    it 'is from offering message' do
      expect(offering_message.company_name).to be_nil
    end

    it 'is from other message' do
      expect(other_message.company_name).to eq(company.name)
    end
  end

  describe '#user_name' do
    it 'is from offering message' do
      expect(offering_message.user_name).to eq(issuer_user.full_name)
    end

    it 'is from other message' do
      expect(other_message.user_name).to eq(offering.public_contact_name)
    end
  end

  describe '#user_uuid' do
    it 'is from offering message' do
      expect(offering_message.user_uuid).to eq(issuer_user.uuid)
    end

    it 'is from other message' do
      expect(other_message.user_uuid).to be_nil
    end
  end

end
