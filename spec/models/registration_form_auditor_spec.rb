require 'rails_helper'

describe RegistrationFormAuditor do
  subject(:auditor) { RegistrationFormAuditor.new }

  describe '#write' do
    it 'creates an AuditLogEntry' do
      expect {
        auditor.write({},nil,nil,nil,nil)
      }.to change(AuditLogEntry, :count).by(1)
    end

    it 'create an AuditLogEntry with the correct attributes' do
      timestamp = Time.now.utc

      auditor.write({first_name: 'foo'}, 1, '1.2.3.4', '/foo/path', timestamp)

      audit_log_entry = AuditLogEntry.last

      expect(audit_log_entry.acting_user_id).to eq(1)
      expect(audit_log_entry.acting_user_ip_address).to eq('1.2.3.4')
      expect(audit_log_entry.form_path).to eq('/foo/path')
      expect(audit_log_entry.persisted_state).to eq('first_name' => 'foo')
      expect(audit_log_entry.persisted_time.to_i).to eq(timestamp.to_i)
    end

    it 'sanitizes password/password_confirmation' do
      auditor.write({ first_name: 'foo', password: 'bar', password_confirmation: 'baz'}, 1, '1.2.3.4', '/foo/path', Time.now)

      expect(AuditLogEntry.last.persisted_state['password']).to be_nil
      expect(AuditLogEntry.last.persisted_state['password_confirmation']).to be_nil
    end
  end
end