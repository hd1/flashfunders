require 'rails_helper'

describe ThirdPartyQualificationForm do

  let(:errors) { [:foo] }
  let(:params) { {role: 'something'} }
  let(:options) { {validator: validator} }

  let(:qualification) { Accreditor::ThirdPartyQualification.new }
  let(:validator) { double(ThirdPartyQualificationFormValidator, errors: errors) }

  subject { ThirdPartyQualificationForm.new(params, qualification, options) }

  it 'delegates errors to the validator' do
    expect(subject.errors).to eql([:foo])
  end

  describe "#save" do
    before { allow(validator).to receive(:valid?).and_return(true) }
    before { allow(qualification).to receive(:save!).and_return(true) }

    it 'returns true' do
      expect(subject.save).to be_truthy
    end

    it 'updates the qualification record with provided params' do
      subject.save

      expect(qualification).to have_received(:save!)
      expect(qualification.role).to eql('something')
    end

    it 'sets the submission datetime' do
      subject.save

      expect(qualification.signed_by_third_party_at).to be_present
    end

    context 'when validation fails' do
      before { allow(validator).to receive(:valid?).and_return(false) }

      it 'returns false' do
        expect(subject.save).to be_falsy
      end
    end
  end

end

