require 'rails_helper'

describe OfferingComment do
  let(:commenter) { FactoryGirl.create(:user, email: 'commenter@example.com', registration_name: 'commenter') }
  let(:issuer_user) { FactoryGirl.create(:user, email: 'issuer_user@example.com', registration_name: 'issuer') }
  let(:offering) { FactoryGirl.create(:offering, issuer_user: issuer_user) }
  let(:comment_form_attrs) {
    {
      body: "Nullam In Dui Mauris",
      offering_id: offering.id,
      user_id: commenter.id
    }
  }

  before do
    @comment = OfferingCommentForm.new(comment_form_attrs).save
  end

  describe "#username" do
    it "returns correct comment's username" do
      expect(@comment.username).to eq(commenter.registration_name)
    end
  end

  describe "#hide!" do
    it "hides comment by a user" do
      @comment.hide!(issuer_user)
      expect(OfferingComment.display_states[@comment.display_state]).to eq(OfferingComment.display_states[:comment_hidden_by_author])
      expect(@comment.updated_by_user).to eq(issuer_user.id)
    end
  end

  describe "#has_visible_issuer_reply?" do
    let(:reply_form_attrs) {
      {
        body: "Mauris Iaculis Porttitor Posuere. Praesent",
        parent_id: @comment.id,
        user_id: issuer_user.id
      }
    }

    before do
      @reply = OfferingCommentForm.new(reply_form_attrs).save
    end

    it "has visible issuer's reply" do
      expect(@comment.has_visible_issuer_reply?).to be_truthy
    end

    it "has hidden issuer's reply" do
      @reply.hide!(issuer_user)
      expect(@comment.has_visible_issuer_reply?).to be_falsy
    end
  end

  describe "#is_reply?" do
    let(:reply_form_attrs) {
      {
        body: "Mauris Iaculis Porttitor Posuere. Praesent",
        user_id: issuer_user.id
      }
    }

    it "is a reply of a comment" do
      comment = OfferingCommentForm.new(reply_form_attrs.merge(parent_id: @comment.id)).save
      expect(comment.is_reply?).to be_truthy
    end

    it "is not a reply a comment" do
      comment = OfferingCommentForm.new(reply_form_attrs.merge(offering_id: offering.id)).save
      expect(comment.is_reply?).to be_falsy
    end
  end

  describe "#is_owner?" do
    it "is a comment owner" do
      expect(@comment.owner?(commenter)).to be_truthy
    end

    it "is not a comment owner" do
      expect(@comment.owner?(issuer_user)).to be_falsy
    end
  end

  describe "#visible" do
    let(:attrs_for_hidden_comment) {
      {
        body: "Mauris Iaculis Porttitor Posuere. Praesent",
        user_id: issuer_user.id,
        offering_id: offering.id,
        display_state: OfferingComment.display_states[:comment_hidden_by_author]
      }
    }

    before do
      OfferingCommentForm.new(attrs_for_hidden_comment).save
    end

    it "returns visible comments only" do
      expect(OfferingComment.count).to eq(2)
      expect(OfferingComment.visible.count).to eq(1)
    end
  end
end
