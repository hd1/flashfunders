require 'rails_helper'

describe ConfirmationForm do

  let(:attributes) {
    {
      password: 'secret_pass',
      password_confirmation: 'secret_pass',
    }
  }

  subject { described_class.new(attributes) }

  describe '#save' do
    let(:partially_registered_user) { create_temporary_user(email: 'joe@temp.com', registration_name: 'Joe Temp') }

    context 'with an unregistered user' do
      it 'should not create a new user' do
        expect(User.find_for_authentication(email: attributes[:email])).to be_blank
        expect(subject.save).to be_falsy
        expect(User.find_for_authentication(email: attributes[:email])).to be_blank
      end
    end

    context 'with a partially registered user' do
      before {
        attributes[:email] = partially_registered_user.email
        attributes[:registration_name] = partially_registered_user.registration_name
      }
      it 'should set the missing attributes' do
        expect(partially_registered_user.us_citizen).to be_blank
        expect(subject.save).to be_truthy
        partially_registered_user.reload
        expect(partially_registered_user.us_citizen).to be_truthy
        expect(partially_registered_user.has_no_password?).to be_falsy
        expect(partially_registered_user.confirmed?).to be_falsy
      end
    end

    context 'with invalid data' do
      before { attributes[:password] = nil }
      it 'should complain about a missing password' do
        expect(subject.save).to be_falsy
        expect(subject.errors.messages[:password]).to include('Required')
      end
    end
  end

end
