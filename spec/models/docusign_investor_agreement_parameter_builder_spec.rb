require 'rails_helper'

describe DocusignInvestorAgreementParameterBuilder do

  let!(:issuer_user) { FactoryGirl.create(:user, email: 'bob@example.com') }
  let!(:investor_user) { FactoryGirl.create(:user, email: 'bluemangroup@example.com') }
  let(:correct_closing_date) { 60.days.from_now.to_date }
  let!(:offering) { FactoryGirl.create(:offering, user: issuer_user, created_at: Time.now, ends_at: correct_closing_date) }
  let!(:personal_federal_identification_information_issuer) { FactoryGirl.create(:personal_federal_identification_information, first_name: 'Bob', middle_initial: 'K', last_name: 'Loblaw', user: issuer_user) }

  let!(:personal_federal_identification_information_investor) { FactoryGirl.create(:personal_federal_identification_information, investor_entity: investor_entity, first_name: 'Tobias', middle_initial: 'M', last_name: 'Funke', address1: "12345 Test Place 1",
                                                                                   address2: "Suite 300", zip_code: "12345", daytime_phone: "404-123-4321", city: "Townsville", state_id: 'CA', country: 'US') }
  let!(:investor_entity) { FactoryGirl.create(:investor_entity_individual) }

  let(:security) { FactoryGirl.create(:security, docusign_template_id: 'docusign_template', docusign_international_template_id: 'international') }
  let!(:investment) { FactoryGirl.create(:investment, envelope_id: '123', offering: offering, shares: 1000, amount: 4444.32, investor_entity: investor_entity, envelope_email: nil, user: investor_user, security: security) }
  subject(:docusign_investor_agreement_parameter_builder) { DocusignInvestorAgreementParameterBuilder.new(investment.id) }

  describe '#envelope_details' do

    context 'as an individual investor' do

      it 'should return a params hash' do
        expect(subject.envelope_details).to eq(
                                              {
                                                issuer_name: "Bob K Loblaw",
                                                issuer_email: "bob@example.com",
                                                corporate_name: "ACME Corp.",
                                                investor_name: "Tobias M Funke",
                                                investor_email: "bluemangroup@example.com",
                                                closing_date: correct_closing_date,
                                                investor_state: "California",
                                                investor_country: "United States",
                                                share_count: 1000,
                                                investment_amount: '$4,444.32',
                                                investor_phone_number: "4041234321",
                                                investor_address1: "12345 Test Place 1",
                                                investor_address2: "Suite 300",
                                                investor_city: "Townsville",
                                                investor_zip_code: "12345"
                                              }
                                            )

      end
    end

    context 'as an individual investor from a country that does not have subregions' do

      let!(:pfii_no_state) { FactoryGirl.create(:personal_federal_identification_information, investor_entity: investor_entity_no_state, first_name: 'John', middle_initial: 'Q', last_name: 'Stateless', address1: "12345 Test Place 1",
                                                address2: "Suite 300", zip_code: "12345", daytime_phone: "404-123-4321", city: "Townsville", state_id: "", country: 'FK') }
      let!(:investor_entity_no_state) { FactoryGirl.create(:investor_entity_individual) }
      let!(:investment) { FactoryGirl.create(:investment, envelope_id: '123', offering: offering, shares: 1000, amount: 4444.32, investor_entity: investor_entity_no_state, envelope_email: nil, user: investor_user) }


      it 'should return a params hash' do
        expect(subject.envelope_details).to eq(
                                              {
                                                issuer_name: "Bob K Loblaw",
                                                issuer_email: "bob@example.com",
                                                corporate_name: "ACME Corp.",
                                                investor_name: "John Q Stateless",
                                                investor_email: "bluemangroup@example.com",
                                                closing_date: correct_closing_date,
                                                investor_state: "",
                                                investor_country: "Falkland Islands (Malvinas)",
                                                share_count: 1000,
                                                investment_amount: '$4,444.32',
                                                investor_phone_number: "404-123-4321",
                                                investor_address1: "12345 Test Place 1",
                                                investor_address2: "Suite 300",
                                                investor_city: "Townsville",
                                                investor_zip_code: "12345"
                                              }
                                            )
      end
    end

    context 'as an entity investor' do
      let!(:investor_entity_trust) { FactoryGirl.create(:investor_entity_trust, phone_number: '123-456-7890', address_1: '123 Eighth Street', address_2: 'Bld 5', city: 'New York', zip_code: '09312', position: 'Tester', name: 'Testfund LLC', state_id: 'TX', country: 'US') }

      let!(:personal_federal_identification_information_investor) { FactoryGirl.create(:personal_federal_identification_information, investor_entity: investor_entity_trust, first_name: 'Tobias', middle_initial: 'M', last_name: 'Funke', address1: "12345 Test Place 1",
                                                                                       address2: "Suite 300", zip_code: "12345", daytime_phone: "404-123-4321", city: "Townsville", state_id: 'CA', country: 'US') }

      let!(:investment) { FactoryGirl.create(:investment, envelope_id: '123', offering: offering, shares: 1000, amount: 4444.32, investor_entity: investor_entity_trust, envelope_email: nil, user: investor_user) }

      it 'should return a params hash' do
        expect(subject.envelope_details).to eq(
                                              {
                                                issuer_name: "Bob K Loblaw",
                                                issuer_email: "bob@example.com",
                                                corporate_name: "ACME Corp.",
                                                investor_name: "Tobias M Funke",
                                                investor_email: "bluemangroup@example.com",
                                                closing_date: correct_closing_date,
                                                investor_state: "California",
                                                investor_country: "United States",
                                                share_count: 1000,
                                                investment_amount: '$4,444.32',
                                                investor_phone_number: "4041234321",
                                                investor_address1: "12345 Test Place 1",
                                                investor_address2: "Suite 300",
                                                investor_city: "Townsville",
                                                investor_zip_code: "12345",
                                                investor_role: 'Tester',
                                                entity_name: 'Testfund LLC',
                                                entity_state: 'Texas',
                                                entity_city: 'New York',
                                                entity_phone_number: '123-456-7890',
                                                entity_address1: '123 Eighth Street',
                                                entity_address2: 'Bld 5',
                                                entity_zip_code: '09312',
                                                entity_country: 'United States',
                                              }
                                            )

      end
    end

    context 'as an entity investor from a country that does not have subregions' do

      let!(:investor_entity_trust) { FactoryGirl.create(:investor_entity_trust,
                                                        phone_number: '123-456-7890',
                                                        address_1: '123 Eighth Street',
                                                        address_2: 'Bld 5',
                                                        city: 'New York',
                                                        zip_code: '09312',
                                                        position: 'Tester',
                                                        name: 'Testfund LLC',
                                                        state_id: '',
                                                        country: 'FK') }

      let!(:pfii_no_state) { FactoryGirl.create(:personal_federal_identification_information, investor_entity: investor_entity_trust, first_name: 'John', middle_initial: 'Q', last_name: 'Stateless', address1: "12345 Test Place 1",
                                                address2: "Suite 300",
                                                zip_code: "12345", daytime_phone: "404-123-4321", city: "Townsville", state_id: "", country: 'FK') }

      let!(:investment) { FactoryGirl.create(:investment, envelope_id: '123', offering: offering, shares: 1000, amount: 4444.32, investor_entity: investor_entity_trust, envelope_email: nil, user: investor_user) }

      it 'should return a params hash' do

        expect(subject.envelope_details).to eq(
                                              {
                                                issuer_name: "Bob K Loblaw",
                                                issuer_email: "bob@example.com",
                                                corporate_name: "ACME Corp.",
                                                investor_name: "John Q Stateless",
                                                investor_email: "bluemangroup@example.com",
                                                closing_date: correct_closing_date,
                                                investor_state: "",
                                                investor_country: "Falkland Islands (Malvinas)",
                                                share_count: 1000,
                                                investment_amount: '$4,444.32',
                                                investor_phone_number: "404-123-4321",
                                                investor_address1: "12345 Test Place 1",
                                                investor_address2: "Suite 300",
                                                investor_city: "Townsville",
                                                investor_zip_code: "12345",
                                                investor_role: 'Tester',
                                                entity_name: 'Testfund LLC',
                                                entity_state: '',
                                                entity_city: 'New York',
                                                entity_phone_number: '123-456-7890',
                                                entity_address1: '123 Eighth Street',
                                                entity_address2: 'Bld 5',
                                                entity_zip_code: '09312',
                                                entity_country: 'Falkland Islands (Malvinas)',
                                              }
                                            )

      end
    end

    describe '#docusign_template' do
      context 'With an US entity' do
        it 'should use the domestic template' do
          expect(subject.template_id).to eq('docusign_template')
        end
      end

      context 'With an international entity' do
        let(:pfii) { FactoryGirl.create(:personal_federal_identification_information, country: 'UK', us_citizen: false )}
        let!(:investor_entity) { FactoryGirl.create(:investor_entity_individual, country: 'UK', personal_federal_identification_information: pfii) }

        it 'should use the international template' do
          expect(subject.template_id).to eq('international')
        end
      end
    end
  end
end
