require 'rails_helper'

describe OfferingPresenter do
  let(:offering) { FactoryGirl.build(:offering, securities: [security_1, security_2]) }
  let(:security_1) { FactoryGirl.build(:security)}
  let(:security_2) { FactoryGirl.build(:security)}
  let(:company) { Company.new }
  let(:investments) { double('investments') }
  let(:offering_investments) { double(OfferingInvestments) }

  before do
    allow(OfferingInvestments).to receive(:new).and_return(offering_investments)
    allow(offering_investments).to receive(:committed_security).and_return(investments)
  end

  subject { described_class.new(offering) }

  describe '#campaign_flash_video_url' do
    before do
      allow_any_instance_of(CampaignVideoURLBuilder).to receive(:convert_vimeo_page_to_swf_url).and_return('vimeo swf url')
    end

    it 'returns the flash video formatted url' do
      expect(subject.campaign_flash_video_url).to eq('vimeo swf url')
    end
  end

  describe '#video_present?' do
    it { expect(subject.video_present?).to be_falsy }

    context 'when video url is present' do
      before { offering.streaming_url = 'something' }

      it { expect(subject.video_present?).to be_truthy }
    end
  end

  describe '#funded?' do
    context 'when it is over the minimums' do
      before do
        security_1.minimum_total_investment = 6000
        allow(investments).to receive(:sum).and_return(10000)
        security_2.minimum_total_investment = 0
      end

      it { expect(subject.funded?).to be_truthy }
    end

    context 'when it equals one of the minimums' do
      before do
        security_1.minimum_total_investment = 6000
        allow(investments).to receive(:sum).and_return(6000)
        security_2.minimum_total_investment = 0
      end

      it { expect(subject.funded?).to be_truthy }
    end

    context 'when minimum total investment is more than accredited balance' do
      before do
        security_1.minimum_total_investment = 100000
        allow(investments).to receive(:sum).and_return(500000)
      end

      it { expect(subject.funded?).to be_falsy }
    end

    context 'when the minimum is zero' do
      before do
        security_1.minimum_total_investment = 0
        allow(investments).to receive(:sum).and_return(0)
        security_2.minimum_total_investment = 0
      end

      it { expect(subject.funded?).to be_falsy }
    end

  end

  describe '#swatch_photo_url' do
    before { allow(offering).to receive(:photo_swatch_url).and_return('swatch') }

    it 'returns the swatch photo url' do
      expect(subject.photo_swatch_url).to eql('swatch')
    end
  end

  describe '#logo_photo_url' do
    before { allow(offering).to receive(:photo_logo_url).and_return('logo') }

    it 'returns the logo photo url' do
      expect(subject.photo_logo_url).to eql('logo')
    end
  end

  describe '#photo_cover_url' do
    before { allow(offering).to receive(:photo_cover_url).and_return('cover') }

    it 'returns the cover photo url' do
      expect(subject.photo_cover_url).to eql('cover')
    end
  end

  describe '#security_presenters' do
    context 'with two perfectly normal securities' do
      let(:security_1) { FactoryGirl.build(:cf_stock)}
      let(:security_2) { FactoryGirl.build(:fsp_stock)}
      it 'should return the right types of presenter' do
        expect(subject.security_presenters.first.class).to eq(SecurityPresenters::CfStock)
        expect(subject.security_presenters.last.class).to eq(SecurityPresenters::FspStock)
      end
    end

    context 'with an undefined presenter class' do
      before { allow(offering).to receive(:securities).and_return([double(Securities::Security, class: 'FakeClass')]) }

      it 'should return a generic presenter' do
        expect(subject.security_presenters.first.class).to eq(SecurityPresenters::Security)
      end
    end

  end

  describe '#discount_rate' do
    it 'returns nil when there are no convertible securities' do
      expect(subject.discount_rate).to be_nil
    end

    context 'with convertible securities' do
      let(:security_1) { FactoryGirl.build(:convertible, is_spv: true, discount_rate: 'discount rate 1') }
      let(:security_2) { FactoryGirl.build(:fsp_convertible, discount_rate: 'discount rate 2') }

      it 'returns the discount rate of the non-spv convertible' do
        expect(subject.discount_rate).to eq(security_2.discount_rate)
      end
    end
  end

  describe '#valuation_cap' do
    it 'returns nil when there are no convertible securities' do
      expect(subject.valuation_cap).to be_nil
    end

    context 'with convertible securities' do
      let(:security_1) { FactoryGirl.build(:convertible, is_spv: true, valuation_cap: 'valuation cap 1') }
      let(:security_2) { FactoryGirl.build(:fsp_convertible, valuation_cap: 'valuation cap 2') }

      it 'returns the discount rate of the non-spv convertible' do
        expect(subject.valuation_cap).to eq(security_2.valuation_cap)
      end
    end
  end

  describe '#spv_total_investment' do
    it 'returns 0 when there are no spv securities' do
      expect(subject.spv_total_investment).to eq(0)
    end

    context 'with an spv' do
      let(:security_1) { FactoryGirl.build(:security, is_spv: true) }

      before { allow(subject.security_presenters.first).to receive(:accredited_balance).and_return(100) }

      it 'returns the balance of the spv security' do
        expect(subject.spv_total_investment).to eq(100)
      end
    end
  end

  describe '#spv_minimum_goal' do
    it 'returns 0 when there are no spv securities' do
      expect(subject.spv_minimum_goal).to eq(0)
    end

    context 'with an spv' do
      let(:security_2) { FactoryGirl.build(:security, is_spv: true, minimum_total_investment: 5000) }

      it 'returns the discount rate of the non-spv convertible' do
        expect(subject.spv_minimum_goal).to eq(5000)
      end
    end
  end

  describe '#maximum_total_investment' do
    it 'is returns 1 when the offering\'s maximum_total_investment is nil' do
      security_1.maximum_total_investment = nil
      security_2.maximum_total_investment = nil
      expect(subject.maximum_total_investment).to eq 1
    end
    it 'is returns the max of the offering\'s maximum_total_investment' do
      security_1.maximum_total_investment = 5
      security_2.maximum_total_investment = 6
      expect(subject.maximum_total_investment).to eq 6
    end
  end

  describe '#minimum_total_investment' do
    it 'is returns 0 when the offering\'s minimum_total_investment is nil' do
      security_1.minimum_total_investment = 0
      security_2.minimum_total_investment = 0
      expect(subject.minimum_total_investment).to eq 0
    end
    it 'is returns the max of the offering\'s minimum_total_investment' do
      security_1.minimum_total_investment = 5
      security_2.minimum_total_investment = 6
      expect(subject.minimum_total_investment).to eq 6
    end
  end

  describe '#option_pool_popover' do
    it 'is returns copy from the security presenter' do
      security_1.maximum_total_investment_display = 'hello'
      expect(subject.option_pool_popover).to eq(      I18n.t('show_campaign.offering_information.option_pool_popover', maximum_total_investment_display: 'hello'))
    end
  end

  describe '#maximum_total_investment_display' do
    it 'is correctly formatted' do
      security_1.maximum_total_investment = 100
      security_2.maximum_total_investment = 200
      expect(subject.maximum_total_investment_display).to eql('$300')
    end
  end

  describe '#unpresentable_minimum?' do
    it 'is true if no minimum' do
      security_1.minimum_total_investment = nil
      security_2.minimum_total_investment = nil
      expect(subject.unpresentable_minimum?).to eql(true)
    end

    it 'is true if minimum is zero' do
      security_1.minimum_total_investment = 0
      security_2.minimum_total_investment = 0
      expect(subject.unpresentable_minimum?).to eql(true)
    end

    it 'is false if there is a minimum' do
      security_1.minimum_total_investment = 0
      security_2.minimum_total_investment = 100
      expect(subject.unpresentable_minimum?).to eql(false)
    end
  end

  describe '#minimum_investment_amount' do
    it 'shows the minimum' do
      security_1.minimum_investment_amount = 200
      security_2.minimum_investment_amount = 2
      expect(subject.minimum_investment_amount).to eql(2)

      security_1.minimum_investment_amount = 2
      security_2.minimum_investment_amount = 200
      expect(subject.minimum_investment_amount).to eql(2)
    end
  end

  describe '#minimum_investment_amount_display' do
    it 'is correctly formatted' do
      security_1.minimum_investment_amount = 20
      security_1.minimum_investment_amount_display = 'something'
      security_2.minimum_investment_amount = 200
      security_2.minimum_investment_amount_display = 'something else'

      expect(subject.minimum_investment_amount_display).to eql('something')
    end
  end

  describe '#first_team_title' do
    it 'uses the value from the offering' do
      offering.first_team_title = 'The team'

      expect(subject.first_team_title).to eql('The team')
    end

    it 'defaults to "Founding Team"' do
      offering.first_team_title = nil

      expect(subject.first_team_title).to eql('Founding Team')
    end
  end

  describe '#second_team_title' do
    it 'uses the value from the offering' do
      offering.second_team_title = 'The other team'

      expect(subject.second_team_title).to eql('The other team')
    end

    it 'defaults to "Founding Team"' do
      offering.second_team_title = nil

      expect(subject.second_team_title).to eql('Investors & Advisors')
    end
  end

  describe '#convertible_offering_valuation_cap_display' do
    it 'should return the valuation cap for a convertible security' do
      offering.securities = [Securities::FspConvertible.new(valuation_cap_display: 'cap 1')]
      expect(subject.convertible_offering_valuation_cap_display).to eq('cap 1')
    end

    it 'should return nil if not convertible' do
      offering.securities = [Securities::FspStock.new]
      expect(subject.convertible_offering_valuation_cap_display).to be_nil
    end
  end

  describe '#cf_amount_raised' do
    before { allow(investments).to receive(:sum).and_return(555) }
    it 'should return the amount if there is a cf security' do
      security_2.regulation = :reg_cf
      expect(subject.cf_amount_raised).to eq(555)
    end

    it 'should return 0 if there is no cf security' do
      expect(subject.cf_amount_raised).to eq(0)
    end
  end

  describe '#cf_minimum_reached?' do
    before { allow(investments).to receive(:sum).and_return(555) }
    it 'should return the amount if there is a cf security' do
      security_2.regulation = :reg_cf
      security_2.minimum_total_investment = 500
      expect(subject.cf_minimum_reached?).to eq(true)
    end

    it 'should return 0 if there is no cf security' do
      security_2.regulation = :reg_cf
      security_2.minimum_total_investment = 600
      expect(subject.cf_minimum_reached?).to eq(false)
    end
  end

  describe '#minimum_total_investment_display' do
    it 'is correctly formatted' do
      security_1.minimum_total_investment = 20
      security_1.minimum_total_investment_display = 'something'
      security_2.minimum_total_investment = 200
      security_2.minimum_total_investment_display = 'something else'

      expect(subject.minimum_total_investment_display).to eql('something else')
    end
  end

  describe '#pre_money_valuation_display' do
    it 'is correctly formatted' do
      offering.pre_money_valuation_display = 'something'
      expect(subject.pre_money_valuation_display).to eql('something')
    end
  end

  describe '#formatted_end_date_for_popover' do
    context 'with an ends_at date' do
      before { offering.ends_at = Date.parse("2014-12-30 01:00:00 -0800") }
      it 'is correctly formatted' do
        expect(subject.formatted_end_date_for_popover).to eq 'Tue, Dec 30 2014'
      end
    end

    context 'without an ends-at date' do
      before { offering.ends_at = nil }

      it 'is correctly formatted' do
        expect(subject.formatted_end_date_for_popover).to be_nil
      end
    end
  end

  describe '#formatted_end_date_for_banner' do
    context 'with an ends_at date' do
      before { offering.ends_at = Date.parse("2014-12-30 01:00:00 -0800") }
      it 'is correctly formatted' do
        expect(subject.formatted_end_date_for_banner).to eq 'December 30, 2014'
      end
    end

    context 'without an ends-at date' do
      before { offering.ends_at = nil }

      it 'is correctly formatted' do
        expect(subject.formatted_end_date_for_banner).to be_nil
      end
    end
  end

  describe '#investable?' do
    it 'should be_truthy' do
      offering.investable = true
      expect(subject.investable?).to be_truthy
    end

    it 'should be false' do
      offering.investable = false
      expect(subject.investable?).to be_falsy
    end
  end

  describe '#ended?' do
    before { offering.ends_at = 1.day.ago }

    it { expect(subject.ended?).to eql(true) }

    context 'when offering has not ended' do
      before { offering.ends_at = 2.days.from_now }

      it { expect(subject.ended?).to eql(false) }
    end
  end

  describe '#closed_successfully?' do
    before do
      offering.ends_at = 1.day.ago
      offering.closed_status = :successful
    end

    it { expect(subject.closed_successfully?).to eql(true) }

    context 'when offering has not ended' do
      before { offering.ends_at = 2.days.from_now }

      it { expect(subject.closed_successfully?).to eql(false) }
    end

    context 'when offering has not closed successfully' do
      before { offering.closed_status = :unsuccessful }

      it { expect(subject.closed_successfully?).to eql(false) }
    end
  end

  describe '#closed_unsuccessfully?' do
    before do
      offering.ends_at = 1.day.ago
      offering.closed_status = :unsuccessful
    end

    it { expect(subject.closed_unsuccessfully?).to eql(true) }

    context 'when offering has not ended' do
      before { offering.ends_at = 2.days.from_now }

      it { expect(subject.closed_unsuccessfully?).to eql(false) }
    end

    context 'when offering has closed successfully' do
      before { offering.closed_status = :successful }

      it { expect(subject.closed_unsuccessfully?).to eql(false) }
    end
  end

  describe '#closed_pending?' do
    before do
      offering.ends_at = 1.day.ago
      offering.closed_status = :pending
    end

    it { expect(subject.closed_pending?).to eql(true) }

    context 'when offering has not ended' do
      before { offering.ends_at = 2.days.from_now }

      it { expect(subject.closed_pending?).to eql(false) }
    end

    context 'when offering has closed successfully' do
      before { offering.closed_status = :successful }

      it { expect(subject.closed_pending?).to eql(false) }
    end
  end

  describe '#formatted_cancel_date' do
    context 'with nil ends_at' do
      before { offering.ends_at = nil }

      it { expect(subject.formatted_cancel_date).to be_nil }
    end

    context 'with a valid ends_at' do
      before { offering.ends_at = Time.parse('05/24/2016 11:00:00 GMT') }

      it { expect(subject.formatted_cancel_date).to eq('05/22/2016') }
    end
  end

  describe '#formatted_end_date' do
    context 'with nil ends_at' do
      before { offering.ends_at = nil }

      it { expect(subject.formatted_end_date).to be_nil }
    end

    context 'with a valid ends_at' do
      before { offering.ends_at = Time.parse('05/24/2016 11:00:00 GMT') }

      it { expect(subject.formatted_end_date).to eq('05/24/2016') }
    end
  end

  describe '#option_pool' do
    it 'displays correctly if there is an option pool' do
      offering.option_pool = 'Option Pool'
      expect(subject.option_pool).to eq "Option Pool"
    end

    it 'displays correctly if there is no option pool' do
      expect(subject.option_pool).to eq "N/A"
    end
  end

  describe '#formatted_percent_equity' do
    it 'is correctly formatted' do
      security_1.shares_offered = 5000
      security_2.shares_offered = 0
      offering.fully_diluted_shares = 10000
      expect(subject.formatted_percent_equity).to eq "33.33%"
    end
  end

  describe '#formatted_minimum_total' do
    it 'is correctly formatted' do
      security_1.minimum_total_investment = 6000
      security_2.minimum_total_investment = 0
      expect(subject.formatted_minimum_total).to eq "$6,000"
    end
  end

  describe "#formatted_percent_complete" do
    before do
      security_1.minimum_total_investment = 1_000_000
      security_2.minimum_total_investment = 0
      allow(investments).to receive(:sum).and_return(989_999.0/2)
    end

    context "less than 99 percent" do
      it 'should floor it' do
        expect(subject.formatted_percent_complete).to eq "98%"
      end
    end

    context "more than 99 percent" do
      before { allow(investments).to receive(:sum).and_return(990_001.0/2) }

      it 'correctly floors to 99%' do
        expect(subject.formatted_percent_complete).to eq "99%"
      end
    end

    context "exactly 99 percent" do
      before { allow(investments).to receive(:sum).and_return(990_000.0/2) }

      it 'correctly stays at 99%' do
        expect(subject.formatted_percent_complete).to eq "99%"
      end
    end

    context "when minimum_total_investment is nil" do
      before do
        security_1.minimum_total_investment = nil
        security_2.minimum_total_investment = nil
      end

      it 'returns 100%' do
        expect(subject.formatted_percent_complete).to eq "100%"
      end
    end

    context "when minimum_total_investment is zero" do
      before do
        security_1.minimum_total_investment = 0
        security_2.minimum_total_investment = 0
      end

      it 'returns 100%' do
        expect(subject.formatted_percent_complete).to eq "100%"
      end
    end
  end

  describe '#escrow_end_date' do
    context "when escrow end date is not set" do
      before { offering.escrow_end_date = nil }
      it "should have an empty escrow end date" do
        expect(subject.escrow_end_date).to eq ''
      end
    end

    context "when escrow end date is set" do
      before { offering.escrow_end_date = '2014-12-30' }
      it "should have a correctly-formatted escrow end date" do
        expect(subject.escrow_end_date).to eq 'Tue, Dec 30 2014'
      end
    end
  end

  describe '#days_left' do
    it 'returns the days until the offering ends' do
      offering.ends_at = 2.days.from_now

      expect(subject.days_left).to eql(2)
    end

    it 'returns 0 days if the offering has ended' do
      offering.ends_at = 2.days.ago

      expect(subject.days_left).to eql(0)
    end
  end

  describe '#formatted_days_left' do
    it 'returns the days until the offering ends' do
      offering.ends_at = 2.days.from_now

      expect(subject.formatted_days_left).to eql('2 days')
    end

    it 'returns 1 day if the offering will end tomorrow' do
      offering.ends_at = 1.day.from_now

      expect(subject.formatted_days_left).to eql('1 day')
    end

    it 'returns 0 days if the offering is just about to end' do
      offering.ends_at = 1.second.from_now

      expect(subject.formatted_days_left).to eql('0 days')
    end

    it 'returns 0 days if the offering has ended' do
      offering.ends_at = 1.second.ago

      expect(subject.formatted_days_left).to eql('0 days')
    end
  end

  describe '#formatted_accredited_balance' do
    before do
      security_1.external_investment_amount = 15
      allow(investments).to receive(:sum).and_return(10.20)
    end

    it 'returns the accredited balance in currency format (rounded down)' do
      expect(subject.formatted_accredited_balance).to eql('$35')
    end

    context 'handles nil external_investment_amount nicely' do
      before do
        security_1.external_investment_amount = nil
        security_2.external_investment_amount = nil
        allow(investments).to receive(:sum).and_return(10.20)
      end

      it 'returns the sum of investments' do
        expect(subject.formatted_accredited_balance).to eql('$20')
      end
    end

    context 'handles nil investments.sum nicely' do
      before do
        security_1.external_investment_amount = 15
        allow(investments).to receive(:sum).and_return(nil)
      end

      it 'returns the sum of investments' do
        expect(subject.formatted_accredited_balance).to eql('$15')
      end
    end
  end

  describe '#accredited_balance' do
    before do
      security_1.external_investment_amount = 7
      security_2.external_investment_amount = 8
      allow(investments).to receive(:sum).and_return(10)
    end

    it 'returns the sum of investments and external amounts' do
      expect(subject.accredited_balance).to eql(35.0)
    end

    context 'with a round-up external investment amount' do
      before { security_1.external_investment_amount = 981.99 }

      it 'rounds appropriately up' do
        expect(subject.formatted_accredited_balance).to eq '$1,010'
      end
    end

    context 'with a round-down external investment amount' do
      before { security_1.external_investment_amount = 492.49 }

      it 'rounds appropriately down' do
        expect(subject.formatted_accredited_balance).to eq '$520'
      end
    end

  end

  describe '#number_of_investors' do
    let(:investments) { [1,2] }

    before do
      security_1.external_investor_count = 1
      security_2.external_investor_count = 3
    end

    it 'returns the number of investments and external investors' do
      expect(subject.number_of_investors).to eql(8)
    end
  end

  describe '#formatted_direct_investment_threshold' do
    it 'is correctly formatted' do
      offering.reg_d_securities.first.update_attribute(:minimum_investment_amount, 25000)
      expect(subject.formatted_direct_investment_threshold).to eql('$25,000.00')
    end
  end

  describe '#convertible?' do
    it 'should return true for fsp convertible' do
      offering.securities = [Securities::FspConvertible.new]
      expect(subject.convertible?).to eq(true)
    end

    it 'should return true for custom convertible' do
      offering.securities = [Securities::CustomConvertible.new]
      expect(subject.convertible?).to eq(true)
    end

    it 'should return false otherwise' do
      offering.securities = [Securities::FspStock.new]
      expect(subject.convertible?).to eq(false)
    end
  end

  describe '#llcs?' do
    it 'should return true for fsp llc' do
      offering.securities = [Securities::FspLlc.new]
      expect(subject.llcs?).to eq(true)
    end

    it 'should return true for custom llc' do
      offering.securities = [Securities::CustomLlc.new]
      expect(subject.llcs?).to eq(true)
    end

    it 'should return false otherwise' do
      offering.securities = [Securities::FspStock.new]
      expect(subject.llcs?).to eq(false)
    end
  end

  describe '#has_reg_d_and_reg_cf_securities?' do
    it 'should return true when both security types are present' do
      security_1.regulation = :reg_d
      security_2.regulation = :reg_cf
      expect(subject.has_reg_d_and_reg_cf_securities?).to eq(true)
    end
    it 'should return false when either security type is missing' do
      offering.securities = [Securities::FspStock.new]
      offering.securities.first.regulation = :reg_d
      expect(subject.has_reg_d_and_reg_cf_securities?).to eq(false)
      offering.securities.first.regulation = :reg_cf
      expect(subject.has_reg_d_and_reg_cf_securities?).to eq(false)
    end
  end

end
