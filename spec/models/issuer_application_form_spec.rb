require 'rails_helper'

describe IssuerApplicationForm do
  let(:user_double) { double(User, id: 43, registration_name: 'Joe User', email: 'joe@user.com') }
  let(:errors) { ActiveModel::Errors.new(double) }
  let(:validator) { double(IssuerApplicationFormValidator, valid?: true, errors: errors) }
  let(:issuer_application_double) { double(IssuerApplication) }
  let(:attributes) { {
    # Founder Info
    phone_number: '1234567890',
    # Company Info
    company_name: 'Big Company Co.',
    website: 'http://bigcompany.com',
    entity_type: IssuerApplication::ENTITY_TYPES.first.second,
    # Campaign Info
    amount_needed: '100000',
    company_criteria: [IssuerApplication::COMPANY_CRITERIA.first.second],
    new_money_committed_range: IssuerApplication::NEW_MONEY_COMMITTED_RANGES.first.second,
    num_users: IssuerApplication::NUM_USERS.first.second,
    promotion_budget: IssuerApplication::PROMOTION_BUDGET.first.second,
    lead_source: IssuerApplication::LEAD_SOURCES.first.second
  } }

  context 'with an existing user' do
    subject { described_class.new(attributes, user_double, validator: validator) }

    describe '#save' do
      context 'when all is valid' do
        before do
          allow(validator).to receive(:valid?).and_return(true)
          allow(IssuerApplication).to receive_message_chain(:new, :save).and_return(true)
        end

        it 'returns true' do
          expect(subject.save).to be_truthy
        end

        it 'saves the issuer application' do
          subject.save

          expect(IssuerApplication).to have_received(:new).with(
            {
              # Founder Info
              phone_number: '1234567890',
              # Company Info
              company_name: 'Big Company Co.',
              website: 'http://bigcompany.com',
              entity_type: IssuerApplication::ENTITY_TYPES.first.second,
              # Campaign Info
              amount_needed: '100000',
              company_criteria: [IssuerApplication::COMPANY_CRITERIA.first.second],
              new_money_committed_range: IssuerApplication::NEW_MONEY_COMMITTED_RANGES.first.second,
              num_users: IssuerApplication::NUM_USERS.first.second,
              promotion_budget: IssuerApplication::PROMOTION_BUDGET.first.second,
              lead_source: IssuerApplication::LEAD_SOURCES.first.second,
              user_id: 43
            }.stringify_keys)
        end
      end

      context 'when not valid' do
        before do
          allow(validator).to receive(:valid?).and_return(false)
        end

        it 'return errors' do
          expect(subject.save).to eq(false)
          expect(subject.errors).to eq(errors)
        end
      end
    end
  end

  context 'as an unregistered user' do

    subject { described_class.new(registration_info) }

    before do
      allow(IssuerApplication).to receive_message_chain(:new, :save).and_return(true)
    end

    describe '#save' do
      context 'with registration name and email' do
        let(:registration_info) { attributes.merge({ registration_name: 'Joe Normal', email: 'joe@normal.com' }) }

        it 'should create the new user' do
          expect(User.find_by_email(registration_info[:email])).to be(nil)
          expect(subject.save).to be_truthy
          expect(User.find_by_email(registration_info[:email])).to_not be(nil)
        end
      end

      context 'with mixed case email' do
        let(:registration_info) { attributes.merge({ registration_name: 'Joe Mixed', email: 'jOe@NorMal.com' }) }

        it 'should create the new user with correct email' do
          expect(User.find_by_email(registration_info[:email])).to be(nil)
          expect(subject.save).to be_truthy
          # user email is always lowercase so the first search fails
          expect(User.find_by_email(registration_info[:email])).to be(nil)
          expect(User.find_by_email(registration_info[:email].downcase)).to_not be(nil)
        end
      end
    end
  end
end
