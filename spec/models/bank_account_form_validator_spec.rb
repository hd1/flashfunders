require 'rails_helper'

describe BankAccountFormValidator do
  it 'returns false when no valid_attributes are passed' do
    expect(BankAccountFormValidator.new).to_not be_valid
  end

  let(:valid_attributes) do
    {
      bank_account_number: 12345678912345678,
      bank_account_routing: 123456789,
      bank_account_type: 'CHECKING',
      bank_account_holder: 'Fred Smith'
    }
  end

  let(:validator) { BankAccountFormValidator.new(valid_attributes) }

  before { allow(RoutingNumber).to receive_message_chain(:where, :exists?).and_return(true) }

  context 'when valid' do
    it 'can be valid' do
      allow(validator).to receive(:routing_number_is_valid).and_return(true)
      expect(validator).to be_valid
    end
  end

  context 'when invalid' do
    context 'bank_account_number' do
      it 'is required' do
        valid_attributes[:bank_account_number] = nil

        expect(validator).to_not be_valid
        expect(validator.errors[:bank_account_number]).to eq(['Required'])
      end

      it 'is a maximum of 17 characters' do
        valid_attributes[:bank_account_number] = 123456789123456789
        expect(validator).to_not be_valid
        expect(validator.errors[:bank_account_number]).to eq(['Maximum of 17 characters'])
      end

      it 'is a minimum of 4 characters' do
        valid_attributes[:bank_account_number] = 123
        expect(validator).to_not be_valid
        expect(validator.errors[:bank_account_number]).to eq(['Minimum of 4 characters'])
      end

      it 'validates it is integer' do
        valid_attributes[:bank_account_number] = '11b432'
        expect(validator).to_not be_valid
        expect(validator.errors[:bank_account_number]).to eq(['Invalid bank account number'])
      end
    end

    context 'bank_account_holder' do
      it 'is required' do
        valid_attributes[:bank_account_holder] = ''
        expect(validator).to_not be_valid
        expect(validator.errors[:bank_account_holder]).to eq(['Required'])
      end

      it 'is 22 characters or less' do
        valid_attributes[:bank_account_holder] = 'Mr Smith the owner of it'
        expect(validator).to_not be_valid
        expect(validator.errors[:bank_account_holder]).to eq(['Maximum of 22 characters'])
      end

      it 'can have non alphanumeric' do
        valid_attributes[:bank_account_holder] = '11bbb!!re'
        expect(validator).to be_valid
        expect(validator.errors[:bank_account_holder]).to be_empty
      end
    end

    context 'bank_account_routing' do
      it 'is required' do
        valid_attributes[:bank_account_routing] = ''
        expect(validator).to_not be_valid
        expect(validator.errors[:bank_account_routing]).to include('Required')
      end
    end

    context 'account type' do
      it 'is required' do
        valid_attributes[:bank_account_type] = ''
        expect(validator).to_not be_valid
        expect(validator.errors[:bank_account_type]).to include('Required')
      end

      it 'must be either CHECKING or SAVINGS' do
        valid_attributes[:bank_account_type] = 'Monkey'
        expect(validator).to_not be_valid
        expect(validator.errors[:bank_account_type]).to eq(['Invalid bank account type'])
      end
    end
  end
end
