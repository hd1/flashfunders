require 'rails_helper'

describe OfferingsVanityConstraints do
  describe '#matches?' do
    subject { OfferingsVanityConstraints.new.matches?(request) }
    before do
      allow(Offering).to receive(:exists?).with(anything).and_return(false)
      allow(Offering).to receive(:exists?).with(vanity_path: 'this-exists').and_return(true)
    end

    let(:request) { double('request', path: path) }

    context 'when given a good match' do
      let(:path) { '/this-exists' }
      it { should be_truthy }
    end
    context 'when given a bad match' do
      let(:path) { '/no-such-path' }
      it { should be_falsy }
    end
    context 'when given a case-insensitive match' do
      let(:path) { '/This-EXISTS' }
      it { should be_truthy }
    end
  end
end
