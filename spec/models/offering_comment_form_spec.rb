require 'rails_helper'

describe OfferingCommentForm do

  let(:offering) { FactoryGirl.create(:offering) }
  let(:offering_2) { FactoryGirl.create(:offering) }
  let(:user) { FactoryGirl.create(:user) }
  let(:long_body) {'a' * 1501}

  describe 'validations' do
    it 'can create a root comment' do
      comment1 = OfferingCommentForm.new(offering_id: offering.id, user_id: user.id, body: 'body').save
      expect(comment1.valid?).to be_truthy
    end

    it 'validates that body is present' do
      comment1 = OfferingCommentForm.new(offering_id: offering.id, user_id: user.id).save
      expect(comment1.valid?).to be_falsy
    end

    it 'validates that the body is not too long' do
      comment1 = OfferingCommentForm.new(offering_id: offering.id, user_id: user.id, body: long_body).save
      expect(comment1.valid?).to be_falsy
    end

    it 'can create a reply' do
      comment1 = OfferingCommentForm.new(offering_id: offering.id, user_id: user.id, body: 'body').save
      reply1 = OfferingCommentForm.new(user_id: user.id, body: 'body', parent_id: comment1.id).save
      expect(reply1.valid?).to be_truthy
    end

    it 'will not allow a comment to be saved with both parent_id and offering_id' do
      comment1 = OfferingCommentForm.new(offering_id: offering.id, user_id: user.id, body: 'body').save
      reply1 = OfferingCommentForm.new(offering_id: offering.id, user_id: user.id, body: 'body', parent_id: comment1.id).save
      expect(reply1.valid?).to be_falsy
      expect(reply1.errors.size).to eq(2)
    end

  end

end
