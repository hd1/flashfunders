require 'rails_helper'

describe InvestmentProgressBar do

  include Rails.application.routes.url_helpers

  let(:steps) { [:amount, :profile, :sign, :fund] }
  let(:offering) { Offering.new id: 98 }
  let(:investment) { Investment.new id: 99, state: :started, offering_id: 98 }
  let(:amount_path) { offering.reg_c_enabled? ? edit_offering_investment_flow_path(offering) : edit_offering_investment_path(offering) }
  let(:profile_path) { edit_offering_invest_investment_profile_path(offering) }
  let(:verify_path) { edit_offering_invest_accreditation_new_path(offering) }
  let(:sign_path) { offering_invest_investment_documents_path(offering) }
  let(:fund_path) { offering_invest_transfer_investment_path(offering) }

  before do
    allow(investment).to receive(:offering).and_return(offering)
  end

  context 'progress bar has correct step labels' do
    subject { described_class.new(investment, 'investment') }

    it 'should have the right label for step 1' do
      expect(subject.step_name(:amount)).to eq(I18n.t('investment_progress_bar.steps.amount'))
    end
    it 'should have the right label for step 2' do
      expect(subject.step_name(:profile)).to eq(I18n.t('investment_progress_bar.steps.profile'))
    end
    it 'should have the right label for step 3' do
      expect(subject.step_name(:verify)).to eq(I18n.t('investment_progress_bar.steps.verify'))
    end
    it 'should have the right label for step 4' do
      expect(subject.step_name(:sign)).to eq(I18n.t('investment_progress_bar.steps.sign'))
    end
    it 'should have the right label for step 5' do
      expect(subject.step_name(:fund)).to eq(I18n.t('investment_progress_bar.steps.fund'))
    end
  end

  shared_examples 'ProgressBar' do

    context 'investment_state is :started' do
      subject { described_class.new(investment, 'investment') }

      it 'should have the right investment state' do
        expect(investment.state.to_sym).to eq(:started)
      end

      it 'should have the correct step states' do
        validate_step_states(amount: :active, profile: :none, sign: :none, fund: :none)
      end

      it 'should have the correct links' do
        validate_step_links(empty_step_links(steps))
      end
    end

    context 'investment_state is :intended' do
      subject { described_class.new(investment, 'investment_profile') }

      before do
        investment.state = :intended
      end

      it 'should have the right investment state' do
        expect(investment.state.to_sym).to eq(:intended)
      end

      it 'should have the correct step states' do
        validate_step_states(amount: :done, profile: :active, sign: :none, fund: :none)
      end

      it 'should have the correct links' do
        validate_step_links(amount: amount_path, profile: nil, sign: nil, fund: nil)
      end
    end

    context 'investment_state is :entity_selected' do
      subject { described_class.new(investment, investment.is_reg_d? ? 'new' : 'investment_documents') }

      before do
        investment.state = :entity_selected
      end

      it 'should have the right investment state' do
        expect(investment.state.to_sym).to eq(:entity_selected)
      end

      it 'should have the correct step states' do
        if investment.is_reg_d?
          validate_step_states(amount: :done, profile: :done, verify: :active, sign: :none, fund: :none)
        else
          validate_step_states(amount: :done, profile: :done, sign: :active, fund: :none)
        end
      end

      it 'should have the correct links' do
        if investment.is_reg_d?
          validate_step_links(amount: amount_path, profile: profile_path, verify: nil, sign: nil, fund: nil)
        else
          validate_step_links(amount: amount_path, profile: profile_path, sign: nil, fund: nil)
        end
      end
    end

    context 'investment_state is :accreditation_verified' do
      subject { described_class.new(investment, 'investment_documents') }

      before do
        investment.state = :accreditation_verified
      end

      it 'should have the right investment state' do
        expect(investment.state.to_sym).to eq(:accreditation_verified)
      end

      it 'should have the correct step states' do
        if investment.is_reg_d?
          validate_step_states(amount: :done, profile: :done, verify: :done, sign: :active, fund: :none)
        else
          validate_step_states(amount: :done, profile: :done, sign: :active, fund: :none)
        end
      end

      it 'should have the correct links' do
        if investment.is_reg_d?
          validate_step_links(amount: amount_path, profile: profile_path, verify: verify_path, sign: nil, fund: nil)
        else
          validate_step_links(amount: amount_path, profile: profile_path, sign: nil, fund: nil)
        end
      end

    end

    context 'investment_state is :signed_documents' do
      subject { InvestmentProgressBar.new(investment, 'fund') }

      before do
        investment.state = :signed_documents
      end

      it 'should have the right investment state' do
        expect(investment.state.to_sym).to eq(:signed_documents)
      end

      it 'should have the correct step states' do
        if investment.is_reg_d?
          validate_step_states(amount: :done, profile: :done, verify: :done, sign: :done, fund: :active)
        else
          validate_step_states(amount: :done, profile: :done, sign: :done, fund: :active)
        end
      end

      it 'should have the correct links' do
        validate_step_links(empty_step_links(steps))
      end

      it 'should indicate it is on the farthest step' do
        expect(subject.last_step?).to eq(true)
      end
    end

    context 'investment_state is :funding_method_selected' do
      subject { InvestmentProgressBar.new(investment, 'funding_method_selected') }

      before do
        investment.state = :funding_method_selected
      end

      it 'should have the right investment state' do
        expect(investment.state.to_sym).to eq(:funding_method_selected)
      end

      it 'should have the correct step states' do
        if investment.is_reg_d?
          validate_step_states(amount: :done, profile: :done, verify: :done, sign: :done, fund: :done)
        else
          validate_step_states(amount: :done, profile: :done, sign: :done, fund: :done)
        end
      end

      it 'should have the correct links' do
        validate_step_links(empty_step_links(steps))
      end

    end

    context 'investment_state is :intended but user returned to edit their amount' do
      subject { InvestmentProgressBar.new(investment, 'investment') }

      before do
        investment.state = :intended
      end

      it 'should have the right investment state' do
        expect(investment.state.to_sym).to eq(:intended)
      end

      it 'should have the correct step states' do
        if investment.is_reg_d?
          validate_step_states(amount: :active, profile: :visited, verify: :none, sign: :none, fund: :none)
        else
          validate_step_states(amount: :active, profile: :visited, sign: :none, fund: :none)
        end
      end

      it 'should have the correct links' do
        if investment.is_reg_d?
          validate_step_links(amount: nil, profile: profile_path, verify: nil, sign: nil, fund: nil)
        else
          validate_step_links(amount: nil, profile: profile_path, sign: nil, fund: nil)
        end
      end

      it 'should report that it is not on the last step' do
        expect(subject.last_step?).to eq(false)
      end
    end

    context 'investment_state is :entity_selected but user returned to edit their profile' do
      subject { InvestmentProgressBar.new(investment, 'investment_profile') }

      before do
        investment.state = :entity_selected
      end

      it 'should have the right investment state' do
        expect(investment.state.to_sym).to eq(:entity_selected)
      end

      it 'should have the correct step states' do
        if investment.is_reg_d?
          validate_step_states(amount: :done, profile: :active, verify: :visited, sign: :none, fund: :none)
        else
          validate_step_states(amount: :done, profile: :active, sign: :visited, fund: :none)
        end
      end

      it 'should have the correct links' do
        if investment.is_reg_d?
          validate_step_links(amount: amount_path, profile: nil, verify: verify_path, sign: nil, fund: nil)
        else
          validate_step_links(amount: amount_path, profile: nil, sign: sign_path, fund: nil)
        end
      end

      it 'should report that it is not on the last step' do
        expect(subject.last_step?).to eq(false)
      end
    end
  end

  context 'With a non Reg-C offering' do
    before do
      allow(offering).to receive(:reg_c_enabled?).and_return(false)
    end

    context 'With Reg D Investment' do
      before do
        allow(investment).to receive(:is_reg_d?).and_return(true)
        allow(investment).to receive(:is_reg_s?).and_return(false)
        allow(investment).to receive(:is_reg_c?).and_return(false)
      end

      it_should_behave_like 'ProgressBar'
    end

    context 'With Reg S Investment' do
      before do
        allow(investment).to receive(:is_reg_d?).and_return(false)
        allow(investment).to receive(:is_reg_s?).and_return(true)
        allow(investment).to receive(:is_reg_c?).and_return(false)
      end

      it_should_behave_like 'ProgressBar'
    end

  end

  context 'With a Reg-C offering' do
    before do
      allow(offering).to receive(:reg_c_enabled?).and_return(true)
    end

    context 'With Reg C Investment' do
      before do
        allow(investment).to receive(:is_reg_d?).and_return(false)
        allow(investment).to receive(:is_reg_s?).and_return(false)
        allow(investment).to receive(:is_reg_c?).and_return(true)
      end

      it_should_behave_like 'ProgressBar'
    end

    context 'With Reg D Investment' do
      before do
        allow(investment).to receive(:is_reg_d?).and_return(true)
        allow(investment).to receive(:is_reg_s?).and_return(false)
        allow(investment).to receive(:is_reg_c?).and_return(false)
      end

      it_should_behave_like 'ProgressBar'
    end

    context 'With Reg S Investment' do
      before do
        allow(investment).to receive(:is_reg_d?).and_return(false)
        allow(investment).to receive(:is_reg_s?).and_return(true)
        allow(investment).to receive(:is_reg_c?).and_return(false)
      end

      it_should_behave_like 'ProgressBar'
    end
  end

  def validate_step_states(expected)
    expected.each_pair do |step, state|
      expect(subject.step_state(step)).to eq(state)
    end
  end

  def validate_step_links(expected)
    expected.each_pair do |step, link|
      expect(subject.step_link(step)).to eq(link)
    end
  end

  def empty_step_links(steps)
    {}.tap { |h| steps.each { |step| h[step] = nil } }
  end

end
