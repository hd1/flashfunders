require 'rails_helper'

describe CacheManager do
  before do
    Sidekiq::Testing.disable!
    Sidekiq::ScheduledSet.new.clear
  end

  after { Sidekiq::Testing.fake! }

  describe '#schedule_jobs' do
    it 'has no schedule jobs' do
      expect(CacheManager.schedule_jobs('some_page')).to be_empty
    end

    it 'has schedule jobs' do
      CacheManager.delay_for(2.minutes).recursive_recache_page('/path', 'some_page_key', 2.minutes)
      expect(CacheManager.schedule_jobs('some_page_key')).to_not be_empty
    end
  end

  describe '#delete_schedule' do
    it 'deletes all schedule jobs' do
      3.times { CacheManager.delay_for(2.minutes).recursive_recache_page('/path', 'page1_key', 2.minutes) }
      expect(CacheManager.schedule_jobs('page1_key')).to_not be_empty

      CacheManager.delete_schedule('page1_key')
      expect(CacheManager.schedule_jobs('page1_key')).to be_empty
    end
  end

  describe '#recache_page' do
    it 'recaches browse page successfully' do
      allow(Honeybadger).to receive(:notify)
      CacheManager.recache_page('/browse')

      expect(Honeybadger).to_not have_received(:notify)
    end
  end
end
