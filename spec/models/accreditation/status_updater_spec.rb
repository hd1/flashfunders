require 'rails_helper'
require 'accreditation'

module Accreditation
  describe StatusUpdater do

    subject { described_class.new }

    describe '#update!' do
      let(:user) { create_user }
      let(:escrow_end_date) { 10.days.from_now }
      let(:offering) { Offering.create!(ends_at: escrow_end_date - 10.days, escrow_end_date: escrow_end_date) }
      let(:entity) { InvestorEntity.create!(user_id: user.id) }
      let!(:investment) { Investment.create!(user_id: user.id, offering_id: offering.id, investor_entity_id: entity.id) }

      context 'when offering is closed' do
        let(:escrow_end_date) { 5.days.ago }

        context 'and user accreditation status is changed' do
          before do
            Accreditor::Span.create!(investor_entity_id: entity.id, start_date: 2.days.ago, end_date: 2.days.from_now, status: :approved)
          end

          it 'does not update investments' do
            expect { subject.update! }.not_to change {
              investment.reload.verified?
            }
          end
        end
      end

      context 'when investment documents have not been signed' do
        context 'and entity accreditation status is rejected' do
          before do
            Accreditor::ThirdPartyIncomeQualification.create!(user_id: user.id, investor_entity_id: entity.id, declined_at: 2.days.ago, email: 'fake@fake.com')
          end

          it 'sets current offering investments to rejected' do
            expect { subject.update! }.to change {
              investment.reload.rejected?
            }.to(true)
          end
        end
      end

      context 'when investment documents have been signed' do
        before do
          investment.update_attribute :docs_signed_at, Date.today - 15
        end

        context 'and user accreditation is pending' do
          before do
            Accreditor::Span.create!(investor_entity_id: entity.id, start_date: 2.days.ago, end_date: 2.day.from_now, status: :pending)
          end

          it 'does not update investments' do
            expect { subject.update! }.not_to change {
              investment.reload.pending?
            }
          end
        end

        context 'and user accreditation is verified after the sign date' do
          before do
            Accreditor::ThirdPartyIncomeQualification.create!(user_id: user.id, investor_entity_id: entity.id, created_at: 10.days.ago, verified_at: 10.days.ago, expires_at: 9.days.from_now, email: 'fake@fake.com')
            Accreditor::ThirdPartyIncomeQualification.create!(user_id: user.id, investor_entity_id: entity.id, declined_at: 10.days.from_now, email: 'fake@fake.com')
          end

          it 'sets current offering investments to verified' do
            expect { subject.update! }.to change {
              investment.reload.verified?
            }.to(true)
          end
        end
      end

      context 'and user accreditation is verified, but not after the sign date' do
        before do
          Accreditor::Span.create!(investor_entity_id: entity.id, start_date: 20.days.ago, end_date: 17.days.ago, status: :approved)
          Accreditor::Span.create!(investor_entity_id: entity.id, start_date: 16.days.ago, end_date: 10.days.from_now, status: :rejected)
        end

        it 'sets current offering investments to verified' do
          expect { subject.update! }.not_to change {
            investment.reload.verified?
          }
        end
      end

      context 'when accreditation has been rejected' do
        before do
          InvestmentEvent::SignDocumentsEvent.create!(investment_id: investment.id)
          Accreditor::ThirdPartyIncomeQualification.create!(user_id: user.id, investor_entity_id: entity.id, declined_at: 2.days.ago, email: 'fake@fake.com')
        end

        it 'sets current offering investments to rejected' do
          expect { subject.update! }.to change {
            investment.reload.rejected?
          }.to(true)
        end
      end
    end
  end
end

