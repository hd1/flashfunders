require 'rails_helper'

describe ConfirmationFormValidator do

  let(:attributes) {
    {
      email: 'user@example.com',
      registration_name: 'Joe User',
      password: 'secretpw',
      password_confirmation: 'secretpw',
      us_citizen: true,
      country: 'United States'
    }
  }

  subject { described_class.new(attributes) }

  context 'with complete data' do
    it { should be_valid }
  end

  context 'when "password" is not present' do
    before { attributes[:password] = nil }
    it { should_not be_valid }
  end

  context 'when "password_confirmation" is not present' do
    before { attributes[:password_confirmation] = nil }
    it { should_not be_valid }
  end

  context 'when "password" is too short' do
    before { attributes[:password] = 'short' }
    it { should_not be_valid }
  end

end
