require 'rails_helper'

describe Investment do

  let(:valid_params) do
    { }
  end

  let(:investment) { Investment.new }

  it_behaves_like 'it is uniquely identifiable'

  describe '#by_individual?' do
    let(:investment) { Investment.new investor_entity: entity }

    subject { investment.by_individual? }

    context 'when the entity is an InvestorEntity::Individual' do
      let(:entity) { InvestorEntity::Individual.new }

      it { should be_truthy }
    end

    context 'when the entity is not an InvestorEntity::Individual' do
      let(:entity) { InvestorEntity::LLC.new }

      it { should be_falsy }
    end
  end

  describe "#has_verified_accreditation?" do
    it 'returns true when a AccreditationVerified exists' do
      InvestmentEvent::VerifyAccreditationEvent.create(investment_id: investment.id)

      expect(investment.has_verified_accreditation?).to eq(true)
    end

    it 'returns false in the absence of an AccreditationVerified' do
      expect(investment.has_verified_accreditation?).to eq(false)
    end
  end

  describe '#has_signed_documents?' do
    it 'returns true when a SignDocumentsEvent exists' do
      InvestmentEvent::SignDocumentsEvent.create(investment_id: investment.id)

      expect(investment.has_signed_documents?).to eq(true)
    end

    it 'returns false in the absence of an SignDocumentsEvent' do
      expect(investment.has_signed_documents?).to eq(false)
    end
  end

  describe '#complete?' do
    complete_statuses = %w(escrowed counter_signed stock_received)
    complete_statuses.each do |state|
      it "returns true for #{state}" do
        investment.state = state
        expect(investment.complete?).to be_truthy
      end
    end

    all_statuses = Investment.state_machines[:state].states.map(&:name).map(&:to_s)
    (all_statuses - complete_statuses).each do |state|
      it "returns false for #{state}" do
        investment.state = state
        expect(investment.complete?).to be_falsy
      end

    end
  end

  describe '#pending_transfer?' do
    let(:investment) { Investment.new }
    context 'returns true when' do
      %w[funds_sent].each do |state|
        it "the investment state is #{state}" do
          investment.state = state
          expect(investment.pending_transfer?).to eq(true)
        end
      end
    end
    context 'returns false when' do
      it 'the investment state is not wire_selected or check_selected' do
        investment.state = 'bananas'
        expect(investment.pending_transfer?).to eq(false)
      end
    end
  end

  describe '#resumable?' do
    let(:investment) { Investment.new }
    context 'returns false when' do
      %w[escrowed refunded].each do |state|
        it "the investment state is #{state}" do
          investment.state = state
          expect(investment.resumable?).to eq(false)
        end
      end
    end
    context 'returns true when' do
      let(:offering) { Offering.create(escrow_end_date: 1.day.from_now) }
      before do
        investment.offering_id = offering.id
      end
      it 'the investment state is anything but escrowed or refunded' do
        investment.state = 'bananas'
        expect(investment.resumable?).to eq(true)
      end
    end
    context 'investment state is appropriate, but escrow end date is in the past' do
      let(:offering) { Offering.create(escrow_end_date: 1.day.ago) }
      before do
        investment.offering_id = offering.id
      end
      it 'returns false' do
        investment.state = 'accreditation_verified'
        expect(investment.resumable?).to eq(false)
      end
    end
    context 'investment state is appropriate, but escrow end date is today' do
      let(:offering) { Offering.create(escrow_end_date: 0.days.from_now) }
      before do
        investment.offering_id = offering.id
      end
      it 'returns true' do
        investment.state = 'accreditation_verified'
        expect(investment.resumable?).to eq(true)
      end
    end
    context 'investment state is appropriate, but escrow end date is in the future' do
      let(:offering) { Offering.create(escrow_end_date: 1.days.from_now) }
      before do
        investment.offering_id = offering.id
      end
      it 'returns true' do
        investment.state = 'accreditation_verified'
        expect(investment.resumable?).to eq(true)
      end
    end
  end

  describe "#email_third_party" do
    let!(:user) { FactoryGirl.create(:user) }
    let!(:investor_entity) { FactoryGirl.create(:investor_entity_individual, :with_pfii, user_id: user.id) }
    let!(:qualification) { Accreditor::ThirdPartyIncomeQualification.create(investor_entity_id: investor_entity.id, email: "email@example.com", role: "Certified Public Accountant", user_id: user.id) }
    let(:security) { FactoryGirl.create(:fsp_stock) }
    let!(:investment) { FactoryGirl.create(:investment, investor_entity: investor_entity, security_id: security.id) }

    before do
      investment.state = 'accreditation_verified'
      investment.save

      ::Accreditation::StatusUpdater.new.update_investment investment

      allow(AccreditationMailer).to receive(:third_party_person_verification_email).and_return(double(deliver: true))
      allow(AccreditationMailer).to receive(:third_party_entity_verification_email).and_return(double(deliver: true))
    end

    it "is called when investment transitions to signed_documents" do
      expect(investment).to receive(:email_third_party)
      investment.sign_documents!
    end

    it "sends the email for person if the qualification is for ThirdPartyIncome" do
      expect { AccreditationMailer.delay.third_party_person_verification_email(qualification, {investor_name: investor_entity.full_name, investor_email: user.email})}.to change(Sidekiq::Extensions::DelayedMailer.jobs, :size).by(1)
      investment.sign_documents!
    end

    it "sends the email for person if the qualification is for ThirdPartyNetWorth" do
      investor_entity.accreditor_qualifications = [Accreditor::ThirdPartyNetWorthQualification.new(email: "email@example.com", role: "Certified Public Accountant", user_id: user.id)]
      expect { AccreditationMailer.delay.third_party_person_verification_email(qualification, {investor_name: investor_entity.full_name, investor_email: user.email})}.to change(Sidekiq::Extensions::DelayedMailer.jobs, :size).by(1)
      investment.sign_documents!
    end

    it "sends the email for person if the qualification is for ThirdPartyEntity" do
      investor_entity.accreditor_qualifications = [Accreditor::ThirdPartyEntityQualification.new(email: "email@example.com", role: "Certified Public Accountant", user_id: user.id)]
      expect { AccreditationMailer.delay.third_party_entity_verification_email(qualification, {investor_name: investor_entity.full_name, investor_email: user.email, entity_name: investor_entity.name})}.to change(Sidekiq::Extensions::DelayedMailer.jobs, :size).by(1)
      investment.sign_documents!
    end

  end

  describe '#funding_options' do
    let!(:ep) { FactoryGirl.create(:escrow_provider_fundamerica) }
    let!(:offering) { Offering.new }
    let(:security) { FactoryGirl.build(:security, escrow_provider: ep) }
    let(:investment) { Investment.new(offering: offering, security: security) }

    context 'when the investment amount is under the ACH cutoff' do
      before do
        investment.amount = 999
      end
      it 'returns ach only' do
        expect(investment.funding_options).to eq(['ach'])
      end
    end

    context 'when the investment amount is over the ACH cutoff' do
      before do
        investment.amount = 1000
      end
      it 'returns ach and wire' do
        expect(investment.funding_options).to eq(['ach', 'wire'])
      end
    end

    context 'when the investment amount is over the wire cutoff' do
      before do
        investment.amount = 20000
      end
      it 'returns wire only' do
        expect(investment.funding_options).to eq(['wire'])
      end
    end

    context 'when the investor is international' do
      before do
        investment.amount = 2000
        investment.reg_s_investor = true
      end
      it 'returns wire only' do
        expect(investment.funding_options).to eq(['wire'])
      end
    end

    context 'when the offering escrow provider does not provide any funding instructions' do
      before do
        ep.properties = {}
        investment.amount = 2000
        investment.reg_s_investor = true
      end
      it 'returns manual only' do
        expect(investment.funding_options).to eq(['manual'])
      end
    end

  end

end
