require 'rails_helper'

describe TransferFundsPresenter do

  let(:check_address) { Address.new }
  let(:bank_address) { Address.new }
  let(:beneficiary_address) { Address.new }
  let(:funding_detail_direct) { OfferingFundingDetail.new }
  let(:funding_detail_direct_international) { OfferingFundingDetail.new }
  let(:bank_account) { Bank::Account.new }
  let(:investment) { Investment.new }
  let(:investor_entity) { double(InvestorEntity) }
  let(:security) { double(Securities::Security) }
  let(:offering) { double(Offering) }
  let(:location) { 'domestic' }

  before do
    allow(funding_detail_direct).to receive(:check_address).and_return(check_address)
    allow(funding_detail_direct).to receive(:wire_bank_address).and_return(bank_address)
    allow(funding_detail_direct).to receive(:wire_beneficiary_address).and_return(beneficiary_address)
    allow(funding_detail_direct).to receive(:wire_bank_account).and_return(bank_account)
    allow(funding_detail_direct).to receive(:wire_instructions).and_return('wire instructions')
    allow(funding_detail_direct).to receive(:check_memo).and_return('check memo')
    allow(investment).to receive(:investor_entity).and_return(investor_entity)
    allow(investor_entity).to receive(:full_name).and_return('Joe Investor')

    allow(investment).to receive(:security).and_return(security)
    allow(security).to receive(:offering_funding_detail_domestic).and_return(funding_detail_direct)
    allow(security).to receive(:offering_funding_detail_international).and_return(funding_detail_direct_international)

    allow(offering).to receive(:reg_c_enabled?).and_return(false)

    funding_detail_direct.wire_beneficiary_name = 'funding_detail_direct'
    funding_detail_direct_international.wire_beneficiary_name = 'funding_detail_direct_international'
  end

  subject { described_class.new(investment, location) }

  describe '#wire_bank_name' do
    it 'returns the bank name' do
      bank_account.bank_name = 'Bank of the West'
      expect(subject.wire_bank_name).to eql('Bank of the West')
    end

    context 'when bank account doesn\'t exist' do
      let(:bank_account) { nil }
      it { expect(subject.wire_bank_name).to be_nil }
    end
  end

  describe '#check_phone_number' do
    it 'returns the formatted check phone number' do
      check_address.phone_number = '1234567890'
      expect(subject.check_phone_number).to eql('(123) 456-7890')
    end

    context 'when address doesn\'t exist' do
      let(:address) { nil }
      it { expect(subject.check_phone_number).to be_nil }
    end
  end

  describe '#wire_bank_phone_number' do
    it 'returns the formatted bank phone number' do
      bank_address.phone_number = '1234567890'
      expect(subject.wire_bank_phone_number).to eql('(123) 456-7890')
    end

    context 'when address doesn\'t exist' do
      let(:bank_address) { nil }
      it { expect(subject.wire_bank_phone_number).to be_nil }
    end

  end

  describe '#wire_account_number' do
    it 'returns the bank account number' do
      bank_account.account_number = '12341234'
      expect(subject.wire_account_number).to eql('12341234')
    end

    context 'when bank account doesn\'t exist' do
      let(:bank_account) { nil }
      it { expect(subject.wire_account_number).to be_nil }
    end
  end

  describe '#wire_routing_number' do
    it 'returns the bank account number' do
      bank_account.routing_number = '12341234'
      expect(subject.wire_routing_number).to eql('12341234')
    end

    context 'when bank account doesn\'t exist' do
      let(:bank_account) { nil }
      it { expect(subject.wire_routing_number).to be_nil }
    end
  end

  describe '#wire_account_type' do
    it 'returns the bank account type' do
      bank_account.account_type = 'Checking'
      expect(subject.wire_account_type).to eql('Checking')
    end

    context 'when bank account doesn\'t exist' do
      let(:bank_account) { nil }
      it { expect(subject.wire_account_type).to be_nil }
    end
  end

  describe '#formatted_check_address' do
    it 'returns to address in multi-line format' do
      check_address.address1 = '123 Hide St'
      check_address.address2 = '#123'
      check_address.city = 'Venice'
      check_address.state = 'CA'
      check_address.zip_code = '55555'

      expect(subject.formatted_check_address).to eql('123 Hide St<br>#123<br>Venice, CA 55555')
    end

    context 'when address doesn\'t exist' do
      let(:check_address) { nil }
      it { expect(subject.formatted_check_address).to be_nil }
    end
  end

  describe '#formatted_wire_bank_address' do
    it 'returns to address in multi-line format' do
      bank_address.address1 = '123 Hide St'
      bank_address.address2 = '#123'
      bank_address.city = 'Venice'
      bank_address.state = 'CA'
      bank_address.zip_code = '55555'

      expect(subject.formatted_wire_bank_address).to eql('123 Hide St<br>#123<br>Venice, CA 55555')
    end

    context 'when address doesn\'t exist' do
      let(:bank_address) { nil }
      it { expect(subject.formatted_wire_bank_address).to be_nil }
    end
  end

  describe '#formatted_wire_beneficiary_address' do
    it 'returns to address in multi-line format' do
      beneficiary_address.address1 = '123 Hide St'
      beneficiary_address.address2 = '#123'
      beneficiary_address.city = 'Venice'
      beneficiary_address.state = 'CA'
      beneficiary_address.zip_code = '55555'

      expect(subject.formatted_wire_beneficiary_address).to eql('123 Hide St<br>#123<br>Venice, CA 55555')
    end

    context 'when address doesn\'t exist' do
      let(:beneficiary_address) { nil }
      it { expect(subject.formatted_wire_beneficiary_address).to be_nil }
    end
  end

  describe '#formatted_wire_instructions' do
    it 'returns wire instructions with investor name appended' do
      expect(subject.formatted_wire_instructions).to eql('wire instructions - Joe Investor')
    end
  end

  describe '#formatted_check_instructions' do
    it 'returns check memo with investor name appended' do
      expect(subject.formatted_check_instructions).to eql('check memo - Joe Investor')
    end
  end

  describe '#initialize - domestic' do
    before do
      allow(offering).to receive(:reg_c_enabled?).and_return(true)
      allow(investor_entity).to receive(:is_international?).and_return(false)
    end

    it 'picks the correct funding detail record for FlashSeed' do
      investment.amount = 20000
      expect(subject.wire_beneficiary_name).to eql('funding_detail_direct')
    end

  end

  describe '#initialize - international' do
    let(:location) { 'international' }
    before do
      allow(offering).to receive(:reg_c_enabled?).and_return(true)
      allow(investor_entity).to receive(:is_international?).and_return(true)
    end

    it 'picks the correct funding detail record for FlashSeed' do
      investment.amount = 20000
      expect(subject.wire_beneficiary_name).to eql('funding_detail_direct_international')
    end

  end

end
