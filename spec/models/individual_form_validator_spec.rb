require 'rails_helper'

describe IndividualFormValidator do
  describe '#valid?' do
    let(:valid_attributes) do
      {
        user_id: 23,
        signatory_first_name: 'Quigley',
        signatory_last_name: 'Downunder',
        signatory_address1: '123 Street Blvd',
        signatory_city: 'Santa Monica',
        signatory_state_id: 'AL',
        signatory_country: 'US',
        signatory_zip_code: '90401',
        signatory_ssn: '555-66-7777',
        signatory_date_of_birth: '09/17/1965',
        signatory_daytime_phone: '333-333-4444',
        signatory_reviewed_information: '1',
        signatory_id_type: 'Passport',
        signatory_id_number: '123',
        signatory_location: 'New York, NY',
        signatory_issued_date: '1/1/2005',
        signatory_expiration_date: '1/5/2016',
        signatory_us_citizen: true,
        high_risk_agree: '1',
        experience_agree: '1',
        loss_risk_agree: '1',
        time_horizon_agree: '1'
      }
    end
    let(:investment) { double(Investment, calc_data: nil) }
    before { allow(investment).to receive(:is_reg_c?).and_return(false) }

    def new_individual_form_validator_with_doubles_injected(attributes)
      IndividualFormValidator.new(attributes, investment)
    end

    it 'is invalid by default' do
      expect(new_individual_form_validator_with_doubles_injected({})).to_not be_valid
    end

    it 'can be valid' do
      expect(new_individual_form_validator_with_doubles_injected(valid_attributes)).to be_valid
    end

    it 'requires first_name to have allowable characters for a name' do
      validator = new_individual_form_validator_with_doubles_injected(valid_attributes.merge(signatory_first_name: nil))

      expect(validator).to_not be_valid

      validator = new_individual_form_validator_with_doubles_injected(valid_attributes.merge(signatory_first_name: '!!!'))
      expect(validator).to_not be_valid
    end

    it 'requires last_name to have allowable characters for a name' do
      validator = new_individual_form_validator_with_doubles_injected(valid_attributes.merge(signatory_last_name: nil))
      expect(validator).to_not be_valid

      validator = new_individual_form_validator_with_doubles_injected(valid_attributes.merge(signatory_last_name: '!!!'))
      expect(validator).to_not be_valid
    end

    it 'requires first_name to be within 45 characters' do
      validator = new_individual_form_validator_with_doubles_injected(valid_attributes.merge(signatory_first_name: 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaah'))
      expect(validator).to_not be_valid
    end

    it 'requires middle_initial to be <= 1 character' do
      validator = new_individual_form_validator_with_doubles_injected(valid_attributes.merge(signatory_middle_initial: ''))
      expect(validator).to be_valid

      validator = new_individual_form_validator_with_doubles_injected(valid_attributes.merge(signatory_middle_initial: nil))
      expect(validator).to be_valid

      validator = new_individual_form_validator_with_doubles_injected(valid_attributes.merge(signatory_middle_initial: 'a'))
      expect(validator).to be_valid

      validator = new_individual_form_validator_with_doubles_injected(valid_attributes.merge(signatory_middle_initial: 'aah'))
      expect(validator).to_not be_valid
    end

    it 'does not allow middle initial to be a space' do
      validator = new_individual_form_validator_with_doubles_injected(valid_attributes.merge(signatory_middle_initial: ' '))
      expect(validator).to_not be_valid
    end

    it 'requires middle_initial to be alpha' do
      validator = new_individual_form_validator_with_doubles_injected(valid_attributes.merge(signatory_middle_initial: '1'))
      expect(validator).to_not be_valid
    end

    it 'requires last_name to be within 45 characters' do
      validator = new_individual_form_validator_with_doubles_injected(valid_attributes.merge(signatory_last_name: 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaah'))
      expect(validator).to_not be_valid
    end

    it 'requires address' do
      validator = new_individual_form_validator_with_doubles_injected(valid_attributes.merge(signatory_address1: nil))
      expect(validator).to_not be_valid
    end

    it 'requires address1 to be within 128 characters' do
      validator = new_individual_form_validator_with_doubles_injected(valid_attributes.merge(signatory_address1: 'Find an address1 find yourself a address1 to live in  Find a address1 find yourself a address1 to live in' +
        'Find an address1 find yourself a address1 to live in  Find a address1 find yourself a address1 to live in' +
        'Find an address1 find yourself a address1 to live in  Find a address1 find yourself a address1 to live in'))
      expect(validator).to_not be_valid
    end

    it 'requires address2 to be within 128 characters' do
      validator = new_individual_form_validator_with_doubles_injected(valid_attributes.merge(signatory_address2: 'Find an address2 find yourself a address2 to live in  Find a address2 find yourself a address2 to live in' +
        'Find an address2 find yourself a address2 to live in  Find a address2 find yourself a address2 to live in' +
        'Find an address2 find yourself a address2 to live in  Find a address2 find yourself a address2 to live in'))
      expect(validator).to_not be_valid
    end

    it 'requires city' do
      validator = new_individual_form_validator_with_doubles_injected(valid_attributes.merge(signatory_city: nil))
      expect(validator).to_not be_valid
    end

    it 'requires city to be within 45 characters' do
      validator = new_individual_form_validator_with_doubles_injected(valid_attributes.merge(signatory_city: 'Find a city find yourself a city to live in Find a city find yourself a city to live in'))
      expect(validator).to_not be_valid
    end

    it 'requires city to contain only letter and space characters' do
      validator = new_individual_form_validator_with_doubles_injected(valid_attributes.merge(signatory_city: 'Find a city, now!'))
      expect(validator).to_not be_valid
    end

    it 'requires state_id to be a valid state code' do
      validator = new_individual_form_validator_with_doubles_injected(valid_attributes.merge(signatory_state_id: nil))
      expect(validator).to_not be_valid

      validator = new_individual_form_validator_with_doubles_injected(valid_attributes.merge(signatory_state_id: '0'))
      expect(validator).to_not be_valid

      validator = new_individual_form_validator_with_doubles_injected(valid_attributes.merge(signatory_state_id: 'XX'))
      expect(validator).to_not be_valid
    end

    it 'requires zip_code to match the proper format' do
      validator = new_individual_form_validator_with_doubles_injected(valid_attributes.merge(signatory_zip_code: nil))
      expect(validator).to_not be_valid

      validator = new_individual_form_validator_with_doubles_injected(valid_attributes.merge(signatory_zip_code: '123456'))
      expect(validator).to_not be_valid
    end

    it 'requires ssn to match the proper format' do
      validator = new_individual_form_validator_with_doubles_injected(valid_attributes.merge(signatory_ssn: nil))
      expect(validator).to_not be_valid

      validator = new_individual_form_validator_with_doubles_injected(valid_attributes.merge(signatory_ssn: '12345678900'))
      expect(validator).to_not be_valid

      validator = new_individual_form_validator_with_doubles_injected(valid_attributes.merge(signatory_ssn: 'a'*9))
      expect(validator).to_not be_valid
    end

    it 'requires phone_number to match the proper format' do
      validator = new_individual_form_validator_with_doubles_injected(valid_attributes.merge(signatory_daytime_phone: nil))
      expect(validator).to_not be_valid

      validator = new_individual_form_validator_with_doubles_injected(valid_attributes.merge(signatory_daytime_phone: '123456789000'))
      expect(validator).to_not be_valid
    end

    it 'requires either daytime or mobile phone to be valid' do
      validator = new_individual_form_validator_with_doubles_injected(valid_attributes.merge(signatory_daytime_phone: nil, signatory_mobile_phone: nil))
      expect(validator).to_not be_valid

      validator = new_individual_form_validator_with_doubles_injected(valid_attributes.merge(signatory_daytime_phone: '12345678900', signatory_mobile_phone: nil))
      expect(validator).to be_valid

      validator = new_individual_form_validator_with_doubles_injected(valid_attributes.merge(signatory_daytime_phone: nil, signatory_mobile_phone: '12345678900'))
      expect(validator).to be_valid
    end

    it 'requires that date of birth is in the valid format and corresponds to an age >=18' do
      invalid_birthdate = '1927-4-13'

      validator = new_individual_form_validator_with_doubles_injected(valid_attributes.merge(signatory_date_of_birth: invalid_birthdate))
      expect(validator).to_not be_valid

      under_18_birthdate = (Date.today - 15.years).strftime("%m/%d/%Y")

      validator = new_individual_form_validator_with_doubles_injected(valid_attributes.merge(signatory_date_of_birth: under_18_birthdate))
      expect(validator).to_not be_valid
    end

    it 'requires the id_type to be present' do
      validator = new_individual_form_validator_with_doubles_injected(valid_attributes.merge(signatory_id_type: nil))
      expect(validator).to_not be_valid
    end

    it 'requires the issued date to be in the mm/dd/yyyy format' do
      validator = new_individual_form_validator_with_doubles_injected(valid_attributes.merge(signatory_issued_date: 'abc'))
      expect(validator).to_not be_valid

      validator = new_individual_form_validator_with_doubles_injected(valid_attributes.merge(signatory_issued_date: '12-1234'))
      expect(validator).to_not be_valid

      validator = new_individual_form_validator_with_doubles_injected(valid_attributes.merge(signatory_issued_date: '12/31/2234'))
      expect(validator).to be_valid
    end

    it 'requires the expiration date to be in the mm/dd/yyyy format' do
      validator = new_individual_form_validator_with_doubles_injected(valid_attributes.merge(signatory_expiration_date: 'abc'))
      expect(validator).to_not be_valid

      validator = new_individual_form_validator_with_doubles_injected(valid_attributes.merge(signatory_expiration_date: '12-2034'))
      expect(validator).to_not be_valid

      validator = new_individual_form_validator_with_doubles_injected(valid_attributes.merge(signatory_expiration_date: '12/13/2026'))
      expect(validator).to be_valid
    end

    it 'requires the id_number to be present' do
      validator = new_individual_form_validator_with_doubles_injected(valid_attributes.merge(signatory_id_number: nil))
      expect(validator).to_not be_valid
    end

    it 'requires the location to be present' do
      validator = new_individual_form_validator_with_doubles_injected(valid_attributes.merge(signatory_location: nil))
      expect(validator).to_not be_valid
    end

    it 'requires the agreement questions to be answered in the affirmative' do
      validator = new_individual_form_validator_with_doubles_injected(valid_attributes.merge(high_risk_agree: '0'))
      expect(validator).to_not be_valid

      validator = new_individual_form_validator_with_doubles_injected(valid_attributes.merge(experience_agree: '0'))
      expect(validator).to_not be_valid

      validator = new_individual_form_validator_with_doubles_injected(valid_attributes.merge(loss_risk_agree: '0'))
      expect(validator).to_not be_valid

      validator = new_individual_form_validator_with_doubles_injected(valid_attributes.merge(time_horizon_agree: '0'))
      expect(validator).to_not be_valid
    end
  end
end
