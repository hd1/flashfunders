require 'rails_helper'

describe TwitterPresenter do
  subject(:presenter) { TwitterPresenter.new(url, via, company_name, campaign_tagline) }
  let(:url) { '' }
  let(:via) { '' }
  let(:company_name) { '' }
  let(:campaign_tagline) { '' }
  def params
    @params ||= URI.decode_www_form(URI(presenter.offering_url).query).each_with_object({}) { |param, hash| hash[param.first] = param.last }.symbolize_keys
  end

  def final_message
    "#{params[:text]} #{'X' * 22} via @#{params[:via]}"
  end

  describe '#offering_url' do
    let(:url) { 'http://example.com/super_long_url_that_will_get_truncated_by_twitter' }
    let(:via) { 'flashfunders' }
    let(:company_name) { 'Bob Builds Stuff' }

     it 'calls the right twitter url' do
      uri = URI(presenter.offering_url)
      expect(uri.scheme).to eq('https')
      expect(uri.host).to eq('twitter.com')
      expect(uri.path).to eq('/share')
    end

    it 'builds the url and via params correctly' do
      expect(params[:via]).to eq('flashfunders')
      expect(params[:url]).to eq('http://example.com/super_long_url_that_will_get_truncated_by_twitter')
    end

    it 'returns a string' do
      expect(presenter.offering_url).to be_a String
    end

    context 'combined arguments less than 140 characters' do
      let(:campaign_tagline) { 'He is a wizard, with robe and cap.' }
      it 'does not truncate the message' do
        expect(final_message.size).to eq(93)
        expect(params[:text]).to eq('Bob Builds Stuff: ' + campaign_tagline)
      end
    end

    context 'combined arguments are exactly 140 characters' do
      let(:campaign_tagline) { 'a' * 81 }
      it 'does not truncate the message' do
        expect(final_message.size).to eq(140)
        expect(params[:text]).to eq('Bob Builds Stuff: ' + campaign_tagline)
      end
    end

    context 'combined arguments more than 140 characters' do
      let(:campaign_tagline) { 'a' * 140 }
      it 'truncates the tagline with ellipsis' do
        expect(params[:text]).to eq('Bob Builds Stuff: ' + ('a' * 78) + '...')
      end
    end
  end

  describe '#completed_investment_url' do
    let(:url) { 'http://example.com/super_long_url_that_will_get_truncated_by_twitter' }
    let(:via) { 'flashfunders' }
    let(:company_name) { 'Bob Builds Stuff' }

     it 'calls the right twitter url' do
      uri = URI(presenter.completed_investment_url)
      expect(uri.scheme).to eq('https')
      expect(uri.host).to eq('twitter.com')
      expect(uri.path).to eq('/share')
    end

    it 'builds the url and via params correctly' do
      expect(params[:via]).to eq('flashfunders')
      expect(params[:url]).to eq('http://example.com/super_long_url_that_will_get_truncated_by_twitter')
    end

    it 'returns a string' do
      expect(presenter.offering_url).to be_a String
    end

    context 'combined arguments less than 140 characters' do
      let(:campaign_tagline) { 'He is a wizard, with robe and cap.' }
      it 'does not truncate the message' do
        expect(final_message.size).to eq(93)
        expect(params[:text]).to eq('Bob Builds Stuff: ' + campaign_tagline)
      end
    end

    context 'combined arguments are exactly 140 characters' do
      let(:campaign_tagline) { 'a' * 81 }
      it 'does not truncate the message' do
        expect(final_message.size).to eq(140)
        expect(params[:text]).to eq('Bob Builds Stuff: ' + campaign_tagline)
      end
    end

    context 'combined arguments more than 140 characters' do
      let(:campaign_tagline) { 'a' * 140 }
      it 'truncates the tagline with ellipsis' do
        expect(params[:text]).to eq('Bob Builds Stuff: ' + ('a' * 78) + '...')
      end
    end
  end
end
