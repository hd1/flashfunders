require 'rails_helper'

describe DealDocument do
  let(:valid_params) { {name: "Document Name", key: sample_key(DocumentUploader.new)} }
  it_behaves_like 'it is uniquely identifiable'

  subject { described_class.new }

  describe '#public_path' do
    let(:offering_id) { 123 }
    let(:uuid) { '7998a674-7489-4f18-b3f1-2457d06aebda' }

    before do
      subject.uuid = uuid
      subject.documentable_id = offering_id
      subject.documentable_type = 'Offering'

      allow(subject).to receive(:document_url).and_return(:something)
      allow(subject).to receive(:aws_link_offering_campaign_documents_path).and_return(:some_path)
    end

    it 'returns the public path of the document' do
      expect(subject.public_path).to eql(:some_path)
      expect(subject).to have_received(:aws_link_offering_campaign_documents_path).with(offering_id, uuid)
    end

    context 'when document is not set' do
      before { allow(subject).to receive(:document_url) }

      it { expect(subject.public_path).to be_nil }
    end

    context 'when not an offering document' do
      before { subject.documentable_type = "Foo" }

      it { expect(subject.public_path).to be_nil }
    end
  end
end