require 'rails_helper'

describe AccreditationFormValidator do
  subject { described_class.new(params) }

  context 'when no attributes are given' do
    let(:params) { {} }

    it { should_not be_valid }
  end

  describe ThirdPartyIncomeFormValidator do
    subject { ThirdPartyIncomeFormValidator.new(params) }

    let(:params) {
      {
          role: 'some-role',
          email: 'foo@example.com',
          investor_email: 'bar@example.com'
      }
    }

    it 'should be valid' do
      expect(subject).to be_valid
      expect(subject.errors).to be_blank
    end

    context 'when role is missing' do
      before { params[:role] = '' }

      it 'has an error on role' do
        expect(subject.valid?).to be_falsy
        expect(subject.errors[:role]).to include(I18n.t('investor_accreditation.error.role_not_present'))
      end
    end

    context 'when email is missing' do
      before { params[:email] = '' }

      it 'has an error on email' do
        expect(subject.valid?).to be_falsy
        expect(subject.errors[:email]).to include(I18n.t('investor_accreditation.error.verifier_email_not_present'))
      end
    end

    context 'when email is not formatted correctly' do
      before { params[:email] = 'foo@example:com' }

      it 'has an error on email' do
        expect(subject.valid?).to be_falsy
        expect(subject.errors[:email]).to include(I18n.t('investor_accreditation.error.email_not_valid'))
      end
    end

    context 'when email matches the investor email' do
      before { params[:investor_email] = params[:email] }

      it 'has an error on email' do
        expect(subject.valid?).to be_falsy
        expect(subject.errors[:email]).to include(I18n.t('investor_accreditation.error.verifier_email_not_present'))
      end
    end
  end

  describe ThirdPartyEntityFormValidator do
    subject { ThirdPartyEntityFormValidator.new(params) }

    let(:params) {
      {
        role: 'some-role',
        email: 'foo@example.com',
        investor_email: 'bar@example.com'
      }
    }

    it 'should be valid' do
      expect(subject).to be_valid
      expect(subject.errors).to be_blank
    end

    context 'when email matches the investor email' do
      before { params[:investor_email] = params[:email] }

      it 'has an error on email' do
        expect(subject.valid?).to be_falsy
        expect(subject.errors[:email]).to include(I18n.t('investor_accreditation.error.verifier_email_not_present'))
      end
    end
  end

  describe IndividualIncomeFormValidator do
    subject { IndividualIncomeFormValidator.new(params) }

    let(:params) {
      {
          consent_to_income: '1',
          file_keys: ['upload/foo.pdf'],
          investor_email: 'bar@example.com'
      }
    }

    it 'should be valid' do
      expect(subject).to be_valid
      expect(subject.errors).to be_blank
    end

    context 'when the consent_to_income is not made' do
      before { params[:consent_to_income] = '0' }

      it 'has an error on consent_to_income' do
        expect(subject.valid?).to be_falsy
        expect(subject.errors[:consent_to_income]).to be_present
      end
    end

    context 'when no files are uploaded' do
      before { params[:file_keys] = [] }

      it 'has an error on files' do
        expect(subject.valid?).to be_falsy
        expect(subject.errors[:file_keys]).to include(I18n.t('investor_accreditation.error.files_not_present'))
      end
    end
  end

  describe JointIncomeFormValidator do
    subject { JointIncomeFormValidator.new(params) }

    let(:params) {
      {
          consent_to_income: '1',
          file_keys: ['upload/foo.pdf'],
          spouse_name: 'Larry',
          spouse_email: 'boss@larry.com',
          investor_email: 'bar@example.com'
      }
    }

    it 'should be valid' do
      expect(subject).to be_valid
      expect(subject.errors).to be_blank
    end

    context 'when the consent_to_income is not made' do
      before { params[:consent_to_income] = '0' }

      it 'has an error on consent_to_income' do
        expect(subject.valid?).to be_falsy
        expect(subject.errors[:consent_to_income]).to be_present
      end
    end

    context 'when no files are uploaded' do
      before { params[:file_keys] = [] }

      it 'has an error on files' do
        expect(subject.valid?).to be_falsy
        expect(subject.errors[:file_keys]).to include(I18n.t('investor_accreditation.error.files_not_present'))
      end
    end

    context 'when spouse name is missing' do
      before { params[:spouse_name] = '' }

      it 'has an error on spouse name' do
        expect(subject.valid?).to be_falsy
        expect(subject.errors[:spouse_name]).to include(I18n.t('investor_accreditation.error.spouse_name_not_present'))
      end
    end

    context 'when spouse email is missing' do
      before { params[:spouse_email] = '' }

      it 'has an error on spouse email' do
        expect(subject.valid?).to be_falsy
        expect(subject.errors[:spouse_email]).to include(I18n.t('investor_accreditation.error.spouse_email_not_present'))
      end
    end

    context 'when spouse email is not formatted correctly' do
      before { params[:spouse_email] = 'foo@example:com' }

      it 'has an error on spouse email' do
        expect(subject.valid?).to be_falsy
        expect(subject.errors[:spouse_email]).to include(I18n.t('investor_accreditation.error.email_not_valid'))
      end
    end

    context 'when spouse email matches the investor email' do
      before { params[:investor_email] = params[:spouse_email] }

      it 'has an error on spouse email' do
        expect(subject.valid?).to be_falsy
        expect(subject.errors[:spouse_email]).to include(I18n.t('investor_accreditation.error.spouse_email_matches_investors'))
      end
    end
  end

  describe IndividualNetWorthFormValidator do
    subject { IndividualNetWorthFormValidator.new(params) }

    let(:params) {
      {
          credit_report_authorization: '1',
          file_keys: ['upload/foo.pdf'],
          investor_email: 'bar@example.com'
      }
    }

    it 'should be valid' do
      expect(subject).to be_valid
      expect(subject.errors).to be_blank
    end

    context 'when they do not authorize a credit report' do
      before { params[:credit_report_authorization] = '0' }

      it 'has an error on credit report authorization' do
        expect(subject.valid?).to be_falsy
        expect(subject.errors[:credit_report_authorization]).to be_present
      end
    end

    context 'when no files are uploaded' do
      before { params[:file_keys] = [] }

      it 'has an error on files' do
        expect(subject.valid?).to be_falsy
        expect(subject.errors[:file_keys]).to include(I18n.t('investor_accreditation.error.files_not_present'))
      end
    end
  end

  describe JointNetWorthFormValidator do
    subject { JointNetWorthFormValidator.new(params) }

    let(:params) {
      {
          credit_report_authorization: '1',
          file_keys: ['upload/foo.pdf'],
          spouse_name: 'Larry',
          spouse_email: 'boss@larry.com',
          spouse_ssn: '123456789',
          investor_email: 'bar@example.com'
      }
    }

    it 'should be valid' do
      expect(subject).to be_valid
      expect(subject.errors).to be_blank
    end

    context 'when they do not authorize a credit report' do
      before { params[:credit_report_authorization] = '0' }

      it 'has an error on credit report authorization' do
        expect(subject.valid?).to be_falsy
        expect(subject.errors[:credit_report_authorization]).to be_present
      end
    end

    context 'when no files are uploaded' do
      before { params[:file_keys] = [] }

      it 'has an error on files' do
        expect(subject.valid?).to be_falsy
        expect(subject.errors[:file_keys]).to include(I18n.t('investor_accreditation.error.files_not_present'))
      end
    end

    context 'when spouse name is missing' do
      before { params[:spouse_name] = '' }

      it 'has an error on spouse name' do
        expect(subject.valid?).to be_falsy
        expect(subject.errors[:spouse_name]).to include(I18n.t('investor_accreditation.error.spouse_name_not_present'))
      end
    end

    context 'when spouse email is missing' do
      before { params[:spouse_email] = '' }

      it 'has an error on spouse email' do
        expect(subject.valid?).to be_falsy
        expect(subject.errors[:spouse_email]).to include(I18n.t('investor_accreditation.error.spouse_email_not_present'))
      end
    end

    context 'when spouse email is not formatted correctly' do
      before { params[:spouse_email] = 'foo@example:com' }

      it 'has an error on spouse email' do
        expect(subject.valid?).to be_falsy
        expect(subject.errors[:spouse_email]).to include(I18n.t('investor_accreditation.error.email_not_valid'))
      end
    end

    context 'when spouse email matches the investor email' do
      before { params[:investor_email] = params[:spouse_email] }

      it 'has an error on spouse email' do
        expect(subject.valid?).to be_falsy
        expect(subject.errors[:spouse_email]).to include(I18n.t('investor_accreditation.error.spouse_email_matches_investors'))
      end
    end

    context 'when spouse ssn is missing' do
      before { params[:spouse_ssn] = '' }

      it 'has an error on spouse ssn' do
        expect(subject.valid?).to be_falsy
        expect(subject.errors[:spouse_ssn]).to include(I18n.t('investor_accreditation.error.spouse_ssn_not_present'))
      end
    end
  end

  describe OfflineEntityFormValidator do
    subject { OfflineEntityFormValidator.new(params) }

    let(:params) {
      {
        contact_request: '1',
        investor_email: 'bar@example.com'
      }
    }

    it 'should be valid' do
      expect(subject).to be_valid
      expect(subject.errors).to be_blank
    end

    context 'when they do not request contact' do
      before { params[:contact_request] = '0' }

      it 'has an error on credit report authorization' do
        expect(subject.valid?).to be_falsy
        expect(subject.errors[:contact_request]).to be_present
      end
    end
  end

end
