require 'rails_helper'

describe AchDebitForm do

  let(:params) do
    {
      bank_account: '12341234',
      ip_address: '1.1.1.1',
      current_user_id: 123,
      investment_amount: 10000,
      investment_id: 456,
      authorized_transfer: "1"
    }
  end

  subject(:ach_debit_form) { AchDebitForm.new(params) }

  describe '#save' do
    before do
      allow(AchDebit).to receive(:create)
    end

    context 'when transfer is authorized' do
      it 'returns true' do
        expect(ach_debit_form.save).to eql(true)
      end

      it 'creates an ach debit record' do
        ach_debit_form.save
        expect(AchDebit).to have_received(:create)
      end
    end

    context 'when transfer is not authorized' do
      before { params[:authorized_transfer] = "0" }

      it 'returns false' do
        expect(ach_debit_form.save).to eql(false)
      end
    end

  end
end

