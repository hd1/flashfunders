require 'rails_helper'

describe Auditor do

  subject(:auditor) { Auditor.new }

  describe '#write' do
    it 'creates an AuditLogEntry' do
      expect {
        auditor.write({},nil,nil,nil,nil)
      }.to change(AuditLogEntry, :count).from(0).to(1)
    end

    it 'create an AuditLogEntry with the correct attributes' do
      timestamp = Time.now.utc

      auditor.write({foo: 'bar'}, 1, '1.2.3.4', '/foo/path', timestamp)

      audit_log_entry = AuditLogEntry.last

      expect(audit_log_entry.acting_user_id).to eq(1)
      expect(audit_log_entry.acting_user_ip_address).to eq('1.2.3.4')
      expect(audit_log_entry.form_path).to eq('/foo/path')
      expect(audit_log_entry.persisted_state).to eq('foo' => 'bar')
      expect(audit_log_entry.persisted_time.to_i).to eq(timestamp.to_i)
    end
  end

  describe '#process_state' do
    it 'is a hook that allows state to be processed, it returns the parameter it is passed by default' do
      expect(auditor.process_state({foo: 'bar'})).to eq({foo: 'bar'})
    end
  end
end
