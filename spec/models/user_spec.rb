require 'rails_helper'

describe User do

  subject { described_class.new }

  let(:valid_params) do
    { email: 'email@example.com', registration_name: "Test Complete Fullname", password: 'sekretss', agreed_to_tos: true, user_type: :user_type_investor }
  end

  it_behaves_like 'it is uniquely identifiable'

  context '#forename and #surname' do
    context 'with a full registration name' do

      it 'has a correct first name section' do
        subject.registration_name = 'Test Complete Fullname'
        expect(subject.forename).to eq "Test"
        expect(subject.surname).to eq "Fullname"
      end
    end

    context 'with a blank registration name' do

      it 'is named correctly' do
        subject.registration_name = ''
        expect(subject.forename).to eq ""
        expect(subject.surname).to eq ""
      end
    end

    context 'with a single word in the name' do

      it 'is named correctly' do
        subject.registration_name = 'Madonna'
        expect(subject.forename).to eq "Madonna"
        expect(subject.surname).to eq ""
      end
    end

    context 'with many words and spaces in the name' do

      it 'is named correctly' do
        subject.registration_name = '  many    strange  ways to type a    name    '
        expect(subject.forename).to eq "many"
        expect(subject.surname).to eq "name"
      end
    end
  end

  context '#full_name' do
    it 'should use the pfii fields if they are not blank' do
      pfii = PersonalFederalIdentificationInformation.new first_name: 'first', last_name: 'last'
      subject.personal_federal_identification_information = pfii
      subject.registration_name = 'registration name'

      expect(subject.full_name).to eq('first last')
    end

    it 'should use the registration name if the pfii fields are blank' do
      pfii = PersonalFederalIdentificationInformation.new
      subject.personal_federal_identification_information = pfii
      subject.registration_name = 'registration name'

      expect(subject.full_name).to eq('registration name')
    end
  end
end
