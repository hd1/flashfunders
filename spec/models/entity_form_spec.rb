require 'rails_helper'
require 'entity_form'

shared_examples_for 'an entity form' do
  let(:user) { FactoryGirl.create(:user, :with_pfii, registration_name: 'John Doe', id: 43) }
  let(:errors) { ActiveModel::Errors.new(double) }
  let(:validator) { double(EntityFormValidator, valid?: true, errors: errors) }
  let(:entity) { double(entity_class, id: 1) }

  subject { EntityForm.new(
    params,
    user,
    validator: validator,
    entity_class: entity_class) }

  shared_examples_for 'initializes properly' do
    let(:form) { EntityForm.new({}, user, validator: validator, entity_class: entity_class) }

    it 'sets the signatory us_citizen and country values from the user' do
      expect(form.signatory_us_citizen).to eq(user.us_citizen)
      expect(form.signatory_country).to eq(user.country)
    end
  end

  describe '.initialize' do
    context 'as a domestic user' do
      before do
        user.personal_federal_identification_information.us_citizen = true
        user.personal_federal_identification_information.country = 'US'
      end

      it_behaves_like 'initializes properly'
    end

    context 'as an international user' do
      before do
        user.personal_federal_identification_information.us_citizen = false
        user.personal_federal_identification_information.country = 'UK'
      end

      it_behaves_like 'initializes properly'
    end
  end

  describe '#save' do
    context 'when all is valid' do
      before do
        allow(validator).to receive(:valid?).and_return(true)
      end

      it 'returns true and creates entity with correct values' do
        expect(subject.save).to eq(true)
        expect(subject.entity.class).to eq(entity_class)
        expect(subject.entity.address_1).to eql(subject.address_1)
        expect(subject.entity.city).to eql(subject.city)
        expect(subject.entity.state_id).to eql(subject.state_id)
        expect(subject.entity.zip_code).to eql(subject.zip_code)
        expect(subject.entity.phone_number).to eql(subject.phone_number)
        expect(subject.entity.position).to eql(subject.position)
        # Check the dates especially
        expect(subject.entity.formation_date).to eq(Date.parse(subject.formation_date, '%m/%d/%Y'))
        expect(subject.entity.personal_federal_identification_information.date_of_birth).to eq(Date.parse(subject.signatory_date_of_birth, '%m/%d/%Y'))
        expect(subject.entity.personal_federal_identification_information.issued_date).to eq(Date.parse(subject.signatory_issued_date, '%m/%d/%Y'))
        expect(subject.entity.personal_federal_identification_information.expiration_date).to eq(Date.parse(subject.signatory_expiration_date, '%m/%d/%Y'))

        subject.entity.save
        expect(subject.send(:find_existing_entity)).to eq(subject.entity)
      end

      it 'has the correct attributes' do
        expect(subject.name).to eql(name)
        expect(subject.tax_id_number).to eql('123456789')
        expect(subject.formation_date).to eql('12/22/2012')
        expect(subject.address_1).to eql('123 Main st.')
        expect(subject.city).to eql('Anytown')
        expect(subject.state_id).to eql('AL')
        expect(subject.zip_code).to eql('12345')
        expect(subject.phone_number).to eql('1234567890')
        expect(subject.position).to eql('Chief')
      end

      it 'generates the proper PFII params' do
        expect(subject.pfii_params).to eq({
          first_name: 'Paulo',
          middle_initial: 'A',
          last_name: 'InvestorTwo',
          date_of_birth: '01/21/1970',
          address1: '123 Main St',
          address2: '',
          city: 'Cali',
          zip_code: '91919',
          state_id: 'AL',
          country: 'US',
          id_type: 0,
          id_number: 'D98987987987',
          location: 'Cali',
          issued_date: '01/21/2000',
          expiration_date: '01/21/2020',
          us_citizen: true,
          employer_name: 'Moo',
          employment_role: 'Chef',
          employer_industry: 'false',
          employment_status: 'Employed',
          mobile_phone: '555 678 6789',
          daytime_phone: '555 876 9876'
        })

      end
    end

    context 'when not valid' do
      before { allow(validator).to receive(:valid?).and_return(false) }

      it 'return errors' do
        expect(subject.save).to eq(false)
        expect(subject.errors).to eq(errors)
      end
    end
  end
end


describe EntityForm do

  let(:params) { ActionController::Parameters.new({
                  name: name,
                  form_type: name,
                  tax_id_number: '123456789',
                  formation_date: '12/22/2012',
                  formation_state_id: 'AL',
                  address_1: '123 Main st.',
                  address_2: '',
                  city: 'Anytown',
                  state_id: 'AL',
                  zip_code: '12345',
                  phone_number: '1234567890',
                  position: 'Chief',
                  signatory_first_name: 'Paulo',
                  signatory_middle_initial: 'A',
                  signatory_last_name: 'InvestorTwo',
                  signatory_date_of_birth: '01/21/1970',
                  signatory_address1: '123 Main St',
                  signatory_address2: '',
                  signatory_city: 'Cali',
                  signatory_zip_code: '91919',
                  signatory_state_id: 'AL',
                  signatory_country: 'US',
                  signatory_id_type: 0,
                  signatory_id_number: 'D98987987987',
                  signatory_location: 'Cali',
                  signatory_issued_date: '01/21/2000',
                  signatory_expiration_date: '01/21/2020',
                  signatory_employer_name: 'Moo',
                  signatory_employment_role: 'Chef',
                  signatory_employer_industry: 'false',
                  signatory_employment_status: 'Employed',
                  signatory_passport_image: nil,
                  signatory_investor_entity_id: 270,
                  signatory_us_citizen: true,
                  signatory_mobile_phone: '555 678 6789',
                  signatory_daytime_phone: '555 876 9876'
                }) }

  describe '.default_validator_class' do
    it 'should prepend the class name to Validator' do
      expect(described_class.default_validator_class).to eq(EntityFormValidator)
    end
  end

  describe 'TrustForm' do
    let(:entity_class) { InvestorEntity::Trust }
    let(:name) { 'trust' }

    it_behaves_like 'an entity form'

  end

  describe 'LLCForm' do
    let(:entity_class) { InvestorEntity::LLC }
    let(:name) { 'llc' }

    it_behaves_like 'an entity form'

  end

  describe 'PartnershipForm' do
    let(:entity_class) { InvestorEntity::Partnership }
    let(:name) { 'partnership' }

    it_behaves_like 'an entity form'

  end

  describe 'CorporationForm' do
    let(:entity_class) { InvestorEntity::Corporation }
    let(:name) { 'corporation' }

    it_behaves_like 'an entity form'

  end

end
