require 'rails_helper'

shared_examples_for 'HubspotTracker Integration' do
  before do
    Hubspot.configure hapikey: ENV['HUBSPOT_API_KEY'], portal_id: ENV['HUBSPOT_KEY']
  end

  describe 'HubspotTracker Integration' do
    it 'Updates HubspotTracker with the specified properties' do
      resource.save
      resource.reload
      VCR.use_cassette("hubspot_integration_#{vcr_label}") do
        HubspotTracker.send_contact_info(resource.class, resource.id)
      end
      # Since data is cached and unchanged, this should do nothing
      expect(HubspotTracker.send_contact_info(resource.class, resource.id)).to eq(nil)
    end

    it 'Generates the correct HubspotTracker properties' do
      info = HubspotTracker.tracking_info(resource)
      info.delete(:_date_generated)
      expect(info).to eql(hubspot_info)
    end
  end
end

describe Subscription do
  let(:now) { '2015-12-03 16:58:48 -0800' }

  context 'When a visitor subscribes to the newsletter' do
    let(:vcr_label) { :subscription }

    let(:resource) { Subscription.new(email: 'new_subscriber@example.com', created_at: now) }
    let(:hubspot_info) { { email: 'new_subscriber@example.com', lifecyclestage: 'subscriber', first_subscribed: now } }

    it_behaves_like 'HubspotTracker Integration'
  end

  context 'When a registered user subscribes to the newsletter' do
    before do
      existing_user.save
      resource.save
    end

    let(:vcr_label) { :registered_user_subscription }

    let(:resource) { Subscription.new(email: 'existing_user@example.com', created_at: now) }
    let!(:hubspot_info) { { email: 'existing_user@example.com',
                            lifecyclestage: 'customer',
                            firstname: 'Existing',
                            lastname: 'User',
                            user_type: 'user_type_investor',
                            number_of_investments: 0,
                            started_investment: '',
                            signed_docs: '',
                            funded: '',
                            amount_invested: 0,
                            company: '',
                            first_subscribed: now } }

    let!(:existing_user) { create_user(email: 'existing_user@example.com', registration_name: 'Existing User', user_type: :user_type_investor) }

    it_behaves_like 'HubspotTracker Integration'
  end
end

describe RegistrationsController do
  let!(:hubspot_info) { { email: 'new_user@example.com',
                          lifecyclestage: 'customer',
                          firstname: 'New',
                          lastname: 'User',
                          user_type: 'user_type_investor',
                          number_of_investments: 0,
                          started_investment: '',
                          signed_docs: '',
                          funded: '',
                          amount_invested: 0,
                          company: '' } }

  context 'When a visitor registers as an investor' do
    let(:vcr_label) { :registration_investor }

    let(:resource) { create_user(email: 'new_user@example.com', registration_name: 'New User', user_type: :user_type_investor) }

    it_behaves_like 'HubspotTracker Integration'
  end

  context 'When a visitor registers as an issuer' do
    before do
      hubspot_info[:user_type] = 'user_type_entrepreneur'
    end

    let(:vcr_label) { :registration_issuer }

    let(:resource) { create_user(email: 'new_user@example.com', registration_name: 'New User', user_type: :user_type_entrepreneur) }

    it_behaves_like 'HubspotTracker Integration'
  end

  context 'When a user has not specified a user type' do
    before do
      hubspot_info[:user_type] = 'user_type_unknown'
    end

    let(:vcr_label) { :registration_unknown }

    let(:resource) { create_user(email: 'new_user@example.com', registration_name: 'New User', user_type: nil) }

    it_behaves_like 'HubspotTracker Integration'
  end

end

describe ApplicationController do
  let(:hubspot_info) { { email: 'investor_user@email.com',
                         lifecyclestage: 'customer',
                         firstname: 'Joe',
                         lastname: 'Investor',
                         user_type: 'user_type_investor',
                         city: 'Moo',
                         state: 'CA',
                         country: 'US',
                         number_of_investments: 0,
                         started_investment: '',
                         signed_docs: '',
                         funded: '',
                         amount_invested: 0,
                         company: '',
                         followed_offerings: '*37*48*'} }
  let(:issuer_user) { create_user(user_type: :user_type_entrepreneur, personal_federal_identification_information: issuer_pfii) }
  let(:issuer_user_2) { create_user(user_type: :user_type_entrepreneur, personal_federal_identification_information: issuer_pfii, email: 'issuer_2@example.com') }
  let(:issuer_pfii) { FactoryGirl.create(:personal_federal_identification_information, first_name: 'John', last_name: 'Issuer', city: 'Moo', state_id: 'CA', country: 'US') }

  let(:investor_user) { create_user(email: 'investor_user@email.com', registration_name: 'Joe Investor', personal_federal_identification_information: individual_pfii) }
  let(:individual_investor_entity) { FactoryGirl.create(:investor_entity_individual) }
  let(:individual_pfii) { FactoryGirl.create(:personal_federal_identification_information, investor_entity: individual_investor_entity, first_name: 'Joe', last_name: 'Investor') }

  let(:company_1) { FactoryGirl.create(:company) }
  let(:company_2) { FactoryGirl.create(:company) }
  let(:offering_1) { FactoryGirl.create(:offering, user_id: issuer_user.id, company: company_1, id: 37) }
  let(:offering_2) { FactoryGirl.create(:offering, user_id: issuer_user_2.id, company: company_2, id: 48) }

  let!(:investment_1) { Investment.create! id: 1, user: investor_user, offering: offering_1, state: 'intended', amount: 5125.34 }
  let!(:follower_1) { OfferingFollower.create(offering_id: offering_1.id, user_id: investor_user.id, mode: :investor)}
  let!(:follower_2) { OfferingFollower.create(offering_id: offering_2.id, user_id: investor_user.id, mode: :investor)}

  context 'When an investor logs into the site' do
    let(:resource) { investor_user }

    describe 'with a started investment' do
      before do
        hubspot_info[:started_investment] = 'Acme Co. [Inv# 1]'
      end

      let(:vcr_label) { :started_investment }

      it_behaves_like 'HubspotTracker Integration'
    end

    describe 'with a started investment and docs signed' do
      before do
        investment_1.state = :signed_documents
        investment_1.save
        hubspot_info[:signed_docs] = 'Acme Co. [Inv# 1]'
      end

      let(:vcr_label) { :signed_documents }

      it_behaves_like 'HubspotTracker Integration'
    end

    describe 'with a funded investment' do
      before do
        investment_1.state = :escrowed
        investment_1.save
        hubspot_info[:funded] = 'Acme Co. [Inv# 1]'
        hubspot_info[:number_of_investments] = 1
        hubspot_info[:amount_invested] = 5125
      end

      let(:vcr_label) { :escrowed }

      it_behaves_like 'HubspotTracker Integration'
    end
  end

  context 'When an issuer logs into the site' do
    before do
      hubspot_info.merge!({ email: 'user@example.com',
                            lifecyclestage: 'customer',
                            firstname: 'John',
                            lastname: 'Issuer',
                            user_type: 'user_type_entrepreneur',
                            company: 'Acme Co. (37)' })
      hubspot_info.delete(:followed_offerings)
    end
    let(:vcr_label) { :issuer_login }

    let(:resource) { issuer_user }

    it_behaves_like 'HubspotTracker Integration'
  end

  context 'When a user with a subscription logs into the site' do
    before do
      subscription.save
      hubspot_info[:first_subscribed] = subscription.created_at.to_s
      hubspot_info[:started_investment] = 'Acme Co. [Inv# 1]'
    end
    let(:vcr_label) { :subscriber_login }

    let(:resource) { investor_user }

    let(:subscription) { Subscription.new(email: resource.email) }

    it_behaves_like 'HubspotTracker Integration'
  end
end

describe IssuerApplicationsController do
  let(:issuer_user) { create_user(user_type: :user_type_entrepreneur, personal_federal_identification_information: issuer_pfii) }
  let(:issuer_pfii) { FactoryGirl.create(:personal_federal_identification_information, first_name: 'John', last_name: 'Issuer', city: 'Moo', state_id: 'CA', country: 'US') }
  let(:issuer_application) { IssuerApplication.new(# Company Info
                                                   company_name: 'Big Company Co.',
                                                   website: 'http://bigcompany.com',
                                                   entity_type: IssuerApplication::ENTITY_TYPES.first.second,
                                                   phone_number: '1234567890',
                                                   # Campaign Info
                                                   amount_needed: '$100,000',
                                                   company_criteria: IssuerApplication::COMPANY_CRITERIA[1..2].map(&:second),
                                                   new_money_committed_range: IssuerApplication::NEW_MONEY_COMMITTED_RANGES.first.second,
                                                   num_users: IssuerApplication::NUM_USERS.first.second,
                                                   promotion_budget: IssuerApplication::PROMOTION_BUDGET.first.second,
                                                   lead_source: IssuerApplication::LEAD_SOURCES.first.second,
                                                   user_id: issuer_user.id) }
  let(:hubspot_info) { { 'company_name' => 'Big Company Co.',
                         'company_website' => 'http://bigcompany.com',
                         'entity_type' => 'option0',
                         'phone_number' => '1234567890',
                         'money_needed_' => '$100,000',
                         'company_criteria' => ['option1', 'option2'],
                         'amount_of_new_money_lined_up' => 'option0',
                         'num_users' => 'option0',
                         'promotion_budget' => 'option0',
                         'how_did_you_hear_about_flashfunders_' => 'option0',
                         'hubspot_owner_id' => 1 } }

  context 'With a new issuer application' do
    before do
      issuer_user.save
      issuer_application.save
    end

    it 'should create a deal on Hubspot' do
      VCR.use_cassette('hubspot_integration_create_deal') do
        deal = HubspotTracker.create_deal(issuer_application.id)
        expect(deal).to_not be(nil)
        expect(deal.deal_id).to eq(37864160) # Deal Id recorded in VCR cassette
        expect(deal.properties['hubspot_owner_id']).to eq('12619659') # Owner Id recorded in VCR cassette
      end
    end
  end

  context 'The Hubspot deal properties' do
    let(:target_properties) do
      VCR.use_cassette('hubspot_integration_deal_properties') do
        Hubspot::DealProperties.all.map { |p| p['name'] }
      end
    end

    it 'should include our Hubspot deal properties' do
      HubspotTracker::HUBSPOT_DEAL_PROPERTIES_MAP.each do |_, p|
        expect(target_properties.include?(p)).to be_truthy
      end
    end
  end

end

describe '.batch_update_user_info' do
  before do
    %w(a b c d e).each do |i|
      create_user(email: "test_user#{i}@example.com", registration_name: "Test User#{i.upcase}", user_type: :user_type_investor)
    end
  end

  context 'When run with an empty cache' do
    let (:vcr_label) { :batch_test_uncached }

    it 'Sends info to Hubspot' do
      HubspotTracker.clear_cache
      VCR.use_cassette("hubspot_integration_#{vcr_label}") do
        count, errors = HubspotTracker.batch_update_user_info(5)
        expect(count).to eq(5)
        expect(errors).to eq([])
      end
    end
  end

  context 'When run again' do
    let (:vcr_label) { :batch_test_cached }

    it 'Does not send unchanged info to Hubspot' do
      HubspotTracker.clear_cache
      VCR.use_cassette("hubspot_integration_#{vcr_label}") do
        count, errors = HubspotTracker.batch_update_user_info(5)
        expect(count).to eq(5)
        expect(errors).to eq([])
        count, errors = HubspotTracker.batch_update_user_info(5)
        expect(count).to eq(0)
        expect(errors).to eq([])
      end
    end
  end
end

describe '.batch_create_deals' do
  before do
    allow(HubspotTracker).to receive(:send_deal_info).and_return(deal_double)
  end

  let(:deal_double) { double(Hubspot::Deal) }
  let(:issuer_user) { create_user(user_type: :user_type_entrepreneur, personal_federal_identification_information: issuer_pfii) }
  let(:issuer_pfii) { FactoryGirl.create(:personal_federal_identification_information, first_name: 'John', last_name: 'Issuer', city: 'Moo', state_id: 'CA', country: 'US') }
  let(:issuer_application) { IssuerApplication.new(company_name: 'Big Company Co.',
                                                   website: 'http://bigcompany.com',
                                                   entity_type: IssuerApplication::ENTITY_TYPES.first.second,
                                                   state_id: 'AL',
                                                   phone_number: '1234567890',
                                                   amount_needed: '$100,000',
                                                   new_money_committed_range: IssuerApplication::NEW_MONEY_COMMITTED_RANGES.first.second,
                                                   num_users: IssuerApplication::NUM_USERS.first.second,
                                                   promotion_budget: IssuerApplication::PROMOTION_BUDGET.first.second,
                                                   lead_source: IssuerApplication::LEAD_SOURCES.first.second,
                                                   user_id: issuer_user.id) }

  context 'With an application missing the hubspot_deal_id' do
    before do
      issuer_application.save
    end

    it 'should send the info to Hubspot' do
      expect(HubspotTracker.batch_create_deals(0, false)).to eq([1, []])
    end
  end

  context 'With an application previously sent to Hubspot' do
    before do
      issuer_application.hubspot_deal_id = 12345
      issuer_application.save
    end

    it 'should not send the info to Hubspot' do
      expect(HubspotTracker.batch_create_deals(0, false)).to eq([0, []])
    end
  end
end

describe 'Contacts Error Handling' do
  let(:response) { double(body: 'This is the body') }
  let(:hubspot_info) { { email: 'new_user@example.com',
                         lifecyclestage: 'customer',
                         firstname: 'New',
                         lastname: 'User',
                         user_type: 'user_type_investor',
                         number_of_investments: 0,
                         started_investment: '',
                         signed_docs: '',
                         funded: '',
                         amount_invested: 0,
                         company: '' } }
  let(:email) { hubspot_info[:email] }

  before do
    allow(Hubspot::Contact).to receive(:find_by_email).and_return(nil)
  end

  describe 'with Hubspot errors' do
    context 'with a contact created after checking for an existing contact' do
      before do
        allow(Hubspot::Contact).to receive(:create!).and_raise(Hubspot::RequestError.new(response, 'CONTACT_EXISTS'))
      end

      it 'should retry once' do

        expect(HubspotTracker).to receive(:create_or_update).with(email, hubspot_info, nil).and_call_original
        expect(HubspotTracker).to receive(:create_or_update).with(email, hubspot_info, nil, 1)

        HubspotTracker.send_info(hubspot_info)
      end
    end

    context 'with a bad email' do
      before do
        allow(Hubspot::Contact).to receive(:create!).and_raise(Hubspot::RequestError.new(response, 'Email address (foo@example.com) is invalid'))
      end

      it 'should only log and not retry' do

        expect(HubspotTracker).to receive(:create_or_update).with(email, hubspot_info, nil).and_call_original
        expect(HubspotTracker).to_not receive(:create_or_update).with(email, hubspot_info, nil, 1)
        expect(HubspotTracker).to receive(:log_error)

        HubspotTracker.send_info(hubspot_info)
      end
    end
  end

  context 'with any other error' do
    before do
      allow(Hubspot::Contact).to receive(:create!).and_raise('some other error')
    end

    it 'should propagate the error' do
      expect { HubspotTracker.send_info(hubspot_info) }.to raise_error
    end
  end

end

describe 'Deals Error Handling' do
  let (:response) { double(body: 'This is the body') }
  let(:issuer_application) { IssuerApplication.new(company_name: 'Big Company Co.',
                                                   website: 'http://bigcompany.com',
                                                   entity_type: 'C-Corp',
                                                   state_id: 'AL',
                                                   phone_number: '1234567890',
                                                   amount_needed: '$100,000',
                                                   round_status: 'New',
                                                   active_conversations: 'Yes',
                                                   new_money_committed_range: 'Less than $50K',
                                                   investor_type: 'Angel',
                                                   usecf: 'Yes',
                                                   accreditation_proof: 'Yes',
                                                   lead_source: 'Personal Referral',
                                                   user_id: 123) }
  let(:owner) { Hubspot::Owner.new(owner_id: 12345) }

  before do
    allow(IssuerApplication).to receive(:find).and_return(issuer_application)
    allow(HubspotTracker).to receive(:get_vids).and_return([])
    allow(Hubspot::Owner).to receive(:find_by_email).and_return(owner)
  end

  context 'with an error message matching LOG_ONLY_ERRORS' do
    before do
      allow(Hubspot::Deal).to receive(:create!).and_raise(Hubspot::RequestError.new(response, 'Email address (foo@example.com) is invalid'))
    end

    it 'should log the error' do
      expect(HubspotTracker).to receive(:log_error)

      HubspotTracker.create_deal(12)
    end
  end

  context 'with any other error' do
    before do
      allow(Hubspot::Deal).to receive(:create!).and_raise('some other error')
    end

    it 'should propagate the error' do
      expect { HubspotTracker.create_deal(12) }.to raise_error('some other error')
    end
  end

end
