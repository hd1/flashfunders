require 'rails_helper'

describe LinkedInPresenter do
  subject(:presenter) { LinkedInPresenter.new(url, company_name, campaign_tagline) }
  let(:url) { 'http://example.com/some_url' }
  let(:company_name) { 'Interesting Company' }
  let(:campaign_tagline) { 'We make things, and try to sell them to you!' }

  def params
    @params ||= URI.decode_www_form(URI(presenter.offering_url).query).each_with_object({}) { |param, hash| hash[param.first] = param.last }.symbolize_keys
  end

  describe '#offering_url' do
    it 'calls the right facebook url' do
      uri = URI(presenter.offering_url)
      expect(uri.scheme).to eq('https')
      expect(uri.host).to eq('linkedin.com')
      expect(uri.path).to eq('/shareArticle')
    end

    it 'builds the url param correctly' do
      expect(params[:mini]).to eq('true')
      expect(params[:title]).to eq('Interesting Company')
      expect(params[:summary]).to eq('We make things, and try to sell them to you!')
    end
  end
end
