require 'rails_helper'
require 'accreditation_form'

shared_context 'an accreditation form' do
  let(:user_double) { double(User, id: 43) }
  let(:entity_double) { double(InvestorEntity, id: 56) }
  let(:errors) { ActiveModel::Errors.new(double) }
  let(:validator) { double(validator_class, valid?: true, errors: errors) }
  let(:qualification) { double(qualification_class, id: 1)}

  subject {
    described_class.new(
      params,
      user_double,
      entity_double,
      validator: validator,
      qualification_class: qualification_class)
  }

  describe '#save' do
    context 'when all is valid' do
      before do
        allow(validator).to receive(:valid?).and_return(true)
        allow(qualification_class).to receive(:create!).and_return(qualification)
      end

      it 'returns true' do
        expect(subject.save).to eq(true)
      end

      it 'saves the investor verification information' do
        subject.save

        expect(qualification_class).to have_received(:create!).with(
          hash_including(*(attributes - [:file_keys]).map(&:to_s), :user_id, :investor_entity_id))
      end
    end

    context 'when not valid' do
      before { allow(validator).to receive(:valid?).and_return(false) }

      it 'return errors' do
        expect(subject.save).to eq(false)
        expect(subject.errors).to eq(errors)
      end
    end
  end
end

shared_context 'an accreditation form with file uploads' do
  let(:user_double) { double(User, id: 43) }
  let(:entity_double) { double(InvestorEntity, id: 56) }
  let(:errors) { ActiveModel::Errors.new(double) }
  let(:validator) { double(validator_class, valid?: true, errors: errors) }
  let(:qualification) { double(qualification_class, id: 1)}

  before { allow(Accreditation::Document).to receive(:create!) }

  subject {
    described_class.new(
      params,
      user_double,
      entity_double,
      validator: validator,
      qualification_class: qualification_class)
  }

  include_context 'an accreditation form'

  context 'when all is valid' do
    before do
      allow(validator).to receive(:valid?).and_return(true)
      allow(qualification_class).to receive(:create!).and_return(qualification)
    end

    it 'saves the user verification information' do
      subject.save

      expect(Accreditation::Document).to have_received(:create!).with({
        key: file_key,
        accreditation_qualification_id: qualification.id
      })
    end
  end
end

describe ThirdPartyIncomeForm do
  include_context 'an accreditation form' do
    let(:validator_class) { ThirdPartyIncomeFormValidator }
    let(:qualification_class) { Accreditor::ThirdPartyIncomeQualification }

    let(:attributes) { [:role, :email] }
    let(:params) { ActionController::Parameters.new({
        role: 'foo',
        email: 'foo@example.com'
    }) }
    it 'has correct attributes' do
      expect(subject.role).to eql("foo")
      expect(subject.email).to eql("foo@example.com")
    end
  end
end

describe ThirdPartyNetWorthForm do
  include_context 'an accreditation form' do
    let(:validator_class) { ThirdPartyNetWorthFormValidator }
    let(:qualification_class) { Accreditor::ThirdPartyNetWorthQualification }

    let(:attributes) { [:role, :email] }
    let(:params) { ActionController::Parameters.new({
        role: 'foo',
        email: 'foo@example.com'
    }) }

    it 'has correct attributes' do
      expect(subject.role).to eql("foo")
      expect(subject.email).to eql("foo@example.com")
    end
  end
end

describe ThirdPartyEntityForm do
  include_context 'an accreditation form' do
    let(:validator_class) { ThirdPartyEntityFormValidator }
    let(:qualification_class) { Accreditor::ThirdPartyEntityQualification }

    let(:attributes) { [:role, :email] }
    let(:params) { ActionController::Parameters.new({
        role: 'foo',
        email: 'foo@example.com'
    }) }

    it 'has correct attributes' do
      expect(subject.role).to eql("foo")
      expect(subject.email).to eql("foo@example.com")
    end
  end
end

describe IndividualIncomeForm do
  include_context 'an accreditation form with file uploads' do
    let(:validator_class) { IndividualIncomeFormValidator }
    let(:qualification_class) { Accreditor::IndividualIncomeQualification }
    let(:file_key) { 'upload/foo.pdf' }

    let(:attributes) { [:consent_to_income, :file_keys] }
    let(:params) { ActionController::Parameters.new({
        consent_to_income: true,
        file_keys: [file_key]
    }) }

    it 'has correct attributes' do
      expect(subject.consent_to_income).to be_truthy
      expect(subject.file_keys).to eql([file_key])
    end
  end
end

describe IndividualNetWorthForm do
  include_context 'an accreditation form with file uploads' do
    let(:validator_class) { IndividualIncomeFormValidator }
    let(:qualification_class) { Accreditor::IndividualIncomeQualification }
    let(:file_key) { 'upload/foo.pdf' }

    let(:attributes) { [:credit_report_authorization, :file_keys] }
    let(:params) { ActionController::Parameters.new({
        credit_report_authorization: true,
        file_keys: [file_key]
    }) }
    it 'has correct attributes' do
      expect(subject.credit_report_authorization).to be_truthy
      expect(subject.file_keys).to eql([file_key])
    end
  end
end

describe JointIncomeForm do
  include_context 'an accreditation form with file uploads' do
    let(:validator_class) { JointIncomeFormValidator }
    let(:qualification_class) { Accreditor::JointIncomeQualification }
    let(:file_key) { 'upload/foo.pdf' }

    let(:attributes) { [:consent_to_income, :file_keys, :spouse_name, :spouse_email] }
    let(:params) { ActionController::Parameters.new({
        consent_to_income: true,
        file_keys: [file_key],
        spouse_name: 'Spousey',
        spouse_email: 'spouse@spouse.ly'
    }) }

    it 'has correct attributes' do
      expect(subject.consent_to_income).to be_truthy
      expect(subject.file_keys).to eql([file_key])
      expect(subject.spouse_name).to eql('Spousey')
      expect(subject.spouse_email).to eql('spouse@spouse.ly' )
    end

  end
end

describe JointNetWorthForm do
  include_context 'an accreditation form with file uploads' do
    let(:validator_class) { JointNetWorthFormValidator }
    let(:qualification_class) { Accreditor::JointNetWorthQualification }
    let(:file_key) { 'upload/foo.pdf' }

    let(:attributes) { [:credit_report_authorization, :file_keys, :spouse_name, :spouse_email, :spouse_ssn] }
    let(:params) { ActionController::Parameters.new({
        credit_report_authorization: true,
        file_keys: [file_key],
        spouse_name: 'Spousey',
        spouse_email: 'spouse@spouse.ly',
        spouse_ssn: '123456789'
    }) }

    it 'has correct attributes' do
      expect(subject.credit_report_authorization).to be_truthy
      expect(subject.file_keys).to eql([file_key])
      expect(subject.spouse_name).to eql('Spousey')
      expect(subject.spouse_email).to eql('spouse@spouse.ly' )
      expect(subject.spouse_ssn).to eql('123456789' )
    end

  end
end

describe OfflineEntityForm do
  include_context 'an accreditation form' do
    let(:validator_class) { OfflineEntityFormValidator }
    let(:qualification_class) { Accreditor::OfflineEntityQualification }

    let(:attributes) { [:request_contact] }
    let(:params) { ActionController::Parameters.new({
        request_contact: true
      }
    )}

    it 'has correct attributes' do
      expect(subject.request_contact).to be_truthy
    end
  end
end

describe VerifiedEntityForm do
  let(:attrs) { { promise: '1' } }
  let(:errors) { 'some fake error' }
  let(:validity) { true }
  let(:validator) { double(VerifiedEntityFormValidator, valid?: validity, errors: errors) }
  let(:form) { described_class.new(attrs, validator: validator) }

  describe '#save' do
    subject { form.save }

    context 'valid' do
      it 'should be true' do
        expect(subject).to be_truthy
      end
    end

    context 'invalid' do
      let(:validity) { false }
      it 'should be false' do
        expect(subject).to be_falsy
      end
    end
  end

  describe '#errors' do
    subject { form.errors }

    it 'should delegate to the validator' do
      expect(subject).to eq(errors)
    end
  end
end