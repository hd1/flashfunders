require 'rails_helper'

describe AchDebitFormValidator do
  let(:valid_attributes) do
    {
      bank_account_number: 12345678912345678,
      bank_account_routing: 123456789,
      bank_account_type: 'CHECKING',
      bank_account_holder: 'Fred Smith',
      authorized_transfer: '1',
    }
  end
  before { allow(RoutingNumber).to receive_message_chain(:where, :exists?).and_return(true) }

  context 'when valid' do
    it 'can be valid' do
      expect(AchDebitFormValidator.new(valid_attributes)).to be_valid
    end
  end

  context 'to be valid' do
    context 'authorized transfer must be 1' do
      it 'or it fails' do
        valid_attributes[:authorized_transfer] = '0'
        validator = AchDebitFormValidator.new(valid_attributes)
        expect(validator).to_not be_valid
        expect(validator.errors[:authorized_transfer]).to eq(['Required'])
      end
    end
  end
end