require 'rails_helper'

shared_examples_for InvestorInvestmentCalculator do
  let(:calculator) { InvestorInvestmentCalculator.get_calc(params[:amount], offering_presenter) }
  before { offering.reg_d_securities.first.update_attribute(:minimum_investment_amount, 25000) }

  describe '#projected_shares' do
    it 'returns the number of shares' do
      expect(calculator.projected_shares).to eq(params[:projected_shares])
    end
  end

  describe '#projected_cost' do
    it 'returns the projected cost' do
      expect(calculator.projected_cost).to eq(params[:projected_cost])
    end
  end

  describe '#projected_percent_ownership' do
    it 'returns the projected % ownership' do
      expect(calculator.projected_percent_ownership).to eq(params[:projected_ownership])
    end
  end
end

describe InvestorInvestmentCalculator do

  let(:offering_presenter) { OfferingPresenter.new(offering) }

  context 'with a custom convertible, model_direct_spv offering' do
    let(:offering) { FactoryGirl.create(:spv_convertible_offering) }

    it 'ignores fractional cents in the proposed investment amount' do
      calculator = InvestorInvestmentCalculator.get_calc('1317.999', offering_presenter)
      expect(calculator.projected_shares).to eq 0
      expect(calculator.projected_cost).to eq 1318.0
    end

    context 'with an investment ($24999.99) below the direct_threshold' do
      let(:params) { { amount: '24999.99', projected_shares: 0, projected_cost: 24999.99, projected_ownership: 0.357 } }

      it_behaves_like InvestorInvestmentCalculator
    end

    context 'with an investment ($25,000) at the direct_threshold' do
      let(:params) { { amount: '25000', projected_shares: 0, projected_cost: 25000.0, projected_ownership: 0.357 } }

      it_behaves_like InvestorInvestmentCalculator
    end

    context 'with an investment ($50,000) above the direct_threshold' do
      let(:params) { { amount: '50000', projected_shares: 0, projected_cost: 50000.0, projected_ownership: 0.714 } }

      it_behaves_like InvestorInvestmentCalculator
    end

  end

  context 'with a custom convertible, model_direct offering' do
    let(:offering) { FactoryGirl.create(:convertible_offering) }

    it 'ignores fractional cents in the proposed investment amount' do
      calculator = InvestorInvestmentCalculator.get_calc('1317.999', offering_presenter)
      expect(calculator.projected_shares).to eq 0
      expect(calculator.projected_cost).to eq 1318.0
    end

    context 'with an investment of $2,500' do
      let(:params) { { amount: '2500', projected_shares: 0, projected_cost: 2500, projected_ownership: 0.036 } }

      it_behaves_like InvestorInvestmentCalculator
    end
  end

  context 'with a flashseed preferred, model_direct_spv offering' do
    let(:offering) { FactoryGirl.build(:offering) }

    it 'is idempotent' do
      first_calculation = InvestorInvestmentCalculator.get_calc('29.37', offering_presenter)
      calculated_cost   = first_calculation.projected_cost
      calculated_shares = first_calculation.projected_shares

      second_calculation       = InvestorInvestmentCalculator.get_calc(calculated_cost, offering_presenter)
      second_calculated_cost   = second_calculation.projected_cost
      second_calculated_shares = second_calculation.projected_shares

      third_calculation       = InvestorInvestmentCalculator.get_calc(second_calculated_cost, offering_presenter)
      third_calculated_cost   = third_calculation.projected_cost
      third_calculated_shares = third_calculation.projected_shares

      expect(calculated_cost).to eq(29.37)
      expect(second_calculated_cost).to eq(calculated_cost)
      expect(third_calculated_cost).to eq(second_calculated_cost)

      expect(calculated_shares).to eq(23)
      expect(second_calculated_shares).to eq(calculated_shares)
      expect(third_calculated_shares).to eq(calculated_shares)
    end

    it 'ignores fractional cents in the proposed investment amount' do
      calculator = InvestorInvestmentCalculator.get_calc('1317.999', offering_presenter)
      expect(calculator.projected_shares).to eq 1000
      expect(calculator.projected_cost).to eq 1318.0
    end

    context 'with an investment ($24999.99) below the direct_threshold' do
      let(:params) { { amount: '24999.99', projected_shares: 18969, projected_cost: 24999.99, projected_ownership: 0.455 } }

      it_behaves_like InvestorInvestmentCalculator
    end

    context 'with an investment ($25,000) at the direct_threshold' do
      let(:params) { { amount: '25000', projected_shares: 18969, projected_cost: 25001.14, projected_ownership: 0.455 } }

      it_behaves_like InvestorInvestmentCalculator
    end

    context 'with an investment ($50,000) above the direct_threshold' do
      let(:params) { { amount: '50000', projected_shares: 37937, projected_cost: 50000.96, projected_ownership: 0.909 } }

      it_behaves_like InvestorInvestmentCalculator
    end
  end

  context 'with a flashseed preferred, model_direct_spv, spv_fee > 0, spv goal = 0 offering' do
    let(:offering) { FactoryGirl.create(:offering, setup_fee_dollar: 5000) }

    context 'with an investment ($24999.99) below the direct_threshold (spv dilution kicks in)' do
      let(:params) { { amount: '24999.99', projected_shares: 18969, projected_cost: 24999.99, projected_ownership: 0.364 } }

      it_behaves_like InvestorInvestmentCalculator
    end

    context 'with an investment ($50,000) above the direct_threshold' do
      let(:params) { { amount: '50000', projected_shares: 37937, projected_cost: 50000.96, projected_ownership: 0.909 } }

      it_behaves_like InvestorInvestmentCalculator
    end
  end

  context 'with a flashseed preferred, model_direct_spv, spv_fee > 0, spv goal = $30,000 offering' do
    let(:offering) do
      offering = FactoryGirl.create(:offering, setup_fee_dollar: 5000)
      security = offering.securities.detect { |s| s.is_spv? }
      security.minimum_total_investment = 90000
      offering
    end

    # let(:offering) do
    #   FactoryGirl.create(:offering, setup_fee_dollar: 5000, spv_minimum_goal: 90000) do |offering|
    #     security = offering.securities.detect { |s| s.is_spv? }
    #     security.
    #         end
    #   end

    context 'with an investment ($24999.99) below the direct_threshold (spv dilution and spv goal kick in)' do
      let(:params) { { amount: '24999.99', projected_shares: 18969, projected_cost: 24999.99, projected_ownership: 0.429 } }

      it_behaves_like InvestorInvestmentCalculator
    end

    context 'with an investment ($50,000) above the direct_threshold' do
      let(:params) { { amount: '50000', projected_shares: 37937, projected_cost: 50000.96, projected_ownership: 0.909 } }

      it_behaves_like InvestorInvestmentCalculator
    end
  end

  context 'with a flashseed preferred, model_direct_spv, high precision share price offering' do
    let(:offering) do
      FactoryGirl.build(:offering, share_price: 2.0353984539, pre_money_valuation: (2.0353984539 * 10000),
                        securities: [
                            FactoryGirl.build(:fsp_stock, maximum_total_investment: 5000 * 2.0353984539),
                            FactoryGirl.build(:fsp_stock, is_spv: true)
                        ])
    end

    context 'with an investment ($25,000) at the direct_threshold' do
      let(:params) { { amount: '25000', projected_shares: 12283, projected_cost: 25000.79, projected_ownership: 81.887 } }

      it_behaves_like InvestorInvestmentCalculator
    end
  end

  context 'with a flashseed preferred, model_direct offering' do
    let(:offering) { FactoryGirl.create(:fsp_only_offering) }

    it 'is idempotent' do
      first_calculation = InvestorInvestmentCalculator.get_calc('29.37', offering_presenter)
      calculated_cost   = first_calculation.projected_cost
      calculated_shares = first_calculation.projected_shares

      second_calculation       = InvestorInvestmentCalculator.get_calc(calculated_cost, offering_presenter)
      second_calculated_cost   = second_calculation.projected_cost
      second_calculated_shares = second_calculation.projected_shares

      third_calculation       = InvestorInvestmentCalculator.get_calc(second_calculated_cost, offering_presenter)
      third_calculated_cost   = third_calculation.projected_cost
      third_calculated_shares = third_calculation.projected_shares

      expect(calculated_cost).to eq(30.31)
      expect(second_calculated_cost).to eq(calculated_cost)
      expect(third_calculated_cost).to eq(second_calculated_cost)

      expect(calculated_shares).to eq(23)
      expect(second_calculated_shares).to eq(calculated_shares)
      expect(third_calculated_shares).to eq(calculated_shares)
    end

    it 'ignores fractional cents in the proposed investment amount' do
      calculator = InvestorInvestmentCalculator.get_calc('1317.999', offering_presenter)
      expect(calculator.projected_shares).to eq 1000
      expect(calculator.projected_cost).to eq 1318.0
    end

    context 'with an investment of $2,500' do
      let(:params) { { amount: '2500', projected_shares: 1897, projected_cost: 2500.24, projected_ownership: 0.045 } }

      it_behaves_like InvestorInvestmentCalculator
    end
  end

  context 'with any offering' do
    let(:offering) { FactoryGirl.create(:offering) }

    context 'with dollar bills and commas in the input' do
      let(:params) { { amount: '$2,500', projected_shares: 1897, projected_cost: 2500.24, projected_ownership: 0.045 } }
    end

    context 'with an investment amount of zero' do
      let(:params) { { amount: 0, projected_shares: 0, projected_cost: 0, projected_ownership: 0 } }
    end

  end

end
