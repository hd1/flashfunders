require 'rails_helper'

describe PersonalFederalIdentificationInformation do

  subject { described_class.new }

  context 'before saving' do
    before { allow(State).to receive(:default_country).and_return('US') }

    it 'sets the country' do
      subject.save
      expect(subject.country).to eql('US')
    end

    context 'when country is set' do
      before { subject.country = 'AU' }

      it 'does not set the country' do
        subject.save
        expect(subject.country).to eql('AU')
      end
    end

    it 'sanitizes the ssn' do
      subject.update_attributes(ssn: '123456789')
      expect(subject.ssn).to eql('123456789')

      subject.update_attributes(ssn: '1.2.3.4.5.6.7.8.9')
      expect(subject.ssn).to eql('123456789')

      subject.update_attributes(ssn: '.,123.a45.6789')
      expect(subject.ssn).to eql('123456789')
    end

    it 'sanitizes the daytime phone' do
      subject.update_attributes(daytime_phone: '1.(123)+-456-7890')
      expect(subject.daytime_phone).to eql('1234567890')

      subject.update_attributes(daytime_phone: '(123) 456-7890')
      expect(subject.daytime_phone).to eql('1234567890')
    end

    it 'santizies the mobile phone' do
      subject.update_attributes(mobile_phone: '1.(123)+-456-7890')
      expect(subject.mobile_phone).to eql('1234567890')

      subject.update_attributes(mobile_phone: '(123) 456-7890')
      expect(subject.mobile_phone).to eql('1234567890')
    end
  end

  describe '#state' do
    let(:state) { double(State) }

    before do
      subject.state_id = 'CA'
      subject.country = 'US'

      allow(State).to receive(:find).and_return(double(name: 'California'))
    end

    it 'returns the state' do
      expect(subject.state_name).to eql('California')
      expect(State).to have_received(:find).with('CA', 'US')
    end
  end

  describe '#country_name' do
    before do
      allow(State).to receive(:country).and_return(double(name: 'United States'))
    end

    it 'returns the full name of the country' do
      expect(subject.country_name).to eql('United States')
    end
  end

  describe '#international?' do
    before { allow(State).to receive(:default_country).and_return('US') }

    context 'US Citizen' do
      before { subject.us_citizen = true }

      context 'when country is the default country' do
        before { subject.country = 'US' }
        it { should_not be_international }
      end
      context 'when country is not the default country' do
        before { subject.country = 'GB' }
        it { should_not be_international }
      end
    end

    context 'Non-US Citizen' do
      before { subject.us_citizen = false }

      context 'when country is the default country' do
        before { subject.country = 'US' }
        it { should_not be_international }
      end

      context 'when country is not the default country' do
        before { subject.country = 'AU' }
        it { should be_international }
      end
    end
  end

  describe '#passport_number' do
    before do
      subject.id_type = 0
      subject.id_number = '123'
    end

    it { expect(subject.passport_number).to be_nil }

    context 'when id type is a passport' do
      before { subject.id_type = 1 }

      it 'returns the id number' do
        expect(subject.passport_number).to eql('123')
      end
    end
  end

  describe '#phone_number' do
    before do
      subject.daytime_phone = '123'
      subject.mobile_phone= '456'
    end

    it 'returns the daytime phone' do
      expect(subject.phone_number).to eql('123')
    end

    context 'when daytime phone is nil' do
      before { subject.daytime_phone = nil }

      it 'returns the mobile phone' do
        expect(subject.phone_number).to eql('456')
      end
    end
  end

  describe '#ssn_last_four' do
    it 'converts a full ssn to just the last four digits' do
      personal_fed_info = PersonalFederalIdentificationInformation.new({ ssn: '122-34-5678' })

      expect(personal_fed_info.ssn_last_four).to eq('***-**-5678')
    end
  end

  describe '#full_name' do
    before do
      subject.first_name = 'Joe'
      subject.middle_initial = 'C'
      subject.last_name = 'Smith'
    end

    it 'return the first, middle and last name string' do
      expect(subject.full_name).to eql('Joe C Smith')
    end

    context 'when middle initial is empty' do
      before { subject.middle_initial = '' }

      it 'returns the first, last name string' do
        expect(subject.full_name).to eql('Joe Smith')
      end
    end
  end

  context "equal_to" do
    describe "equal_to" do
      let(:pfii) { FactoryGirl.build(:personal_federal_identification_information, daytime_phone: "555-555-5555 321") }
      let(:equal_pfii_params) { {:first_name=>"Paulo",
        :last_name=>"Delgado",
        :middle_initial=>"A",
        :date_of_birth=>"10/31/1976",
        :ssn=>"654-65-6544",
        :address1=>"123 Main St",
        :address2=>"",
        :city=>"Moo",
        :zip_code=>"91919",
        :daytime_phone=>"555-555-5555 321",
        :mobile_phone=>"",
        :state_id=>"CA",
        :country=>'US',
        :id_type=>"Driver's License",
        :id_number=>"D123456789",
        :location=>"Cali",
        :issued_date=>"01/01/1980",
        :expiration_date=>"01/01/2080",
        :employment_status=>"",
        :employment_role=>"",
        :employer_name=>"",
        :employer_industry=>"true",
        :us_citizen=>"true"} }

      let(:international_pfii) { FactoryGirl.build(:personal_federal_identification_information, :international) }
      let(:equal_international_pfii_params) { {:first_name=>"Jean Paul",
        :last_name=>"Delgado",
        :middle_initial=>"A",
        :date_of_birth=>"10/31/1976",
        :ssn=>"654-65-6544",
        :address1=>"1 Rue Le Main",
        :address2=>"",
        :city=>"Paris",
        :zip_code=>"FR12345",
        :daytime_phone=>"+43 2 555-555-5555",
        :mobile_phone=>"",
        :state_id=>"F",
        :country=>'FR',
        :id_type=>"Driver's License",
        :id_number=>"D123456789",
        :location=>"Cali",
        :issued_date=>"01/01/1980",
        :expiration_date=>"01/01/2080",
        :employment_status=>"",
        :employment_role=>"",
        :employer_name=>"",
        :employer_industry=>"true",
        :us_citizen=>"false"}
      }



      it "returns true when equal" do
        expect(pfii.equals_to(equal_pfii_params)).to be_truthy
      end

      it "returns true when equal even with different phone formatting for US entities" do
        pfii.daytime_phone = '555-555-5555'
        pfii.save
        equal_pfii_params[:daytime_phone] = '5-55-55-55-555'
        expect(pfii.equals_to(equal_pfii_params)).to be_truthy
      end

      it "returns true even for international pfii's" do
        expect(international_pfii.equals_to(equal_international_pfii_params)).to be_truthy
      end
    end

  end

end
