require 'rails_helper'

describe Address do

  subject { described_class.new }

  it { should be_valid }

  describe '#phone_nubmer=' do
    it 'strips all non-numeric values in phone number' do
      subject.phone_number = '1 (123) 456-7890'
      expect(subject.phone_number).to eql('11234567890')
    end
  end

end

