require 'rails_helper'

describe ResidencyFormValidator do

  let(:user) { create_user }
  let(:attributes) {
    {
      us_citizen: true,
      country: 'United States'
    }
  }

  subject { described_class.new(attributes, user) }

  context 'with complete data' do
    it { should be_valid }
  end

end
