require 'rails_helper'

describe IndividualForm do
  let!(:user) { FactoryGirl.create(:user, :with_pfii) }
  let!(:empty_user) { FactoryGirl.create(:user) }

  let!(:individual_investor_entity_second) { FactoryGirl.create(:investor_entity_individual, :with_pfii) }
  let!(:pfii_second) { individual_investor_entity_second.personal_federal_identification_information }

  let!(:individual_investor_entity) { FactoryGirl.create(:investor_entity_individual, :with_pfii) }
  let!(:pfii) { individual_investor_entity.personal_federal_identification_information }
  let!(:security) { FactoryGirl.create(:fsp_stock) }
  let!(:investment) { FactoryGirl.create(:investment, user: user, investor_entity: individual_investor_entity, security_id: security.id) }

  before do
    pfii_second.first_name = 'Good Paulo'
    pfii_second.save
    user.investor_entities = [individual_investor_entity, individual_investor_entity_second]
  end

  describe 'initialize' do
    it 'sets its instance variables to match the provided user' do
      form = described_class.new({}, user)
      GenericEntityForm::SIGNATORY_ATTRS.each do |attr|
        attr_name = form.class.remove_signatory(attr)
        expect(form.send(attr)).to eq(mask_value(form, attr, pfii.send(attr_name)))
      end
    end

    it 'overrides its instance variables if attributes are provided' do
      attributes = { signatory_first_name: 'Evil Paulo' }
      form       = described_class.new(attributes, user)
      expect(form.signatory_first_name).to eq('Evil Paulo')
    end
  end

  describe '#save' do

    let(:attributes) { {
      signatory_first_name:            pfii.first_name,
      signatory_middle_initial:        pfii.middle_initial,
      signatory_last_name:             pfii.last_name,
      signatory_date_of_birth:         pfii.date_of_birth.strftime(Date::DATE_FORMATS[:short_date]),
      signatory_ssn:                   pfii.ssn,
      signatory_address1:              pfii.address1.to_s,
      signatory_address2:              pfii.address2.to_s,
      signatory_city:                  pfii.city.to_s,
      signatory_zip_code:              pfii.zip_code.to_s,
      signatory_daytime_phone:         pfii.daytime_phone.to_s,
      signatory_mobile_phone:          pfii.mobile_phone.to_s,
      signatory_state_id:              pfii.state_id.to_s,
      signatory_country:               pfii.country.to_s,
      signatory_reviewed_information:  '1',
      signatory_id_type:               pfii.id_type.to_s,
      signatory_id_number:             pfii.id_number.to_s,
      signatory_location:              pfii.location.to_s,
      signatory_issued_date:           pfii.issued_date.strftime(Date::DATE_FORMATS[:short_date]),
      signatory_expiration_date:       pfii.expiration_date.strftime(Date::DATE_FORMATS[:short_date]),
      signatory_us_citizen:            true,
      signatory_employment_status:     pfii.employment_status.to_s,
      signatory_employment_role:       pfii.employment_role.to_s,
      signatory_employer_name:         pfii.employer_name.to_s,
      signatory_employer_industry:     pfii.employer_industry.to_s,
      high_risk_agree:                 '1',
      experience_agree:                '1',
      loss_risk_agree:                 '1',
      time_horizon_agree:              '1'
    } }
    context 'when the user already has an individual investor entity' do

      before do
        individual_investor_entity.save
      end

      context 'and the provided attributes are equal' do
        it 'uses the existing investor entity and pfii' do
          form = described_class.new(attributes, user, { investment: investment })
          expect { form.save }.to change { InvestorEntity::Individual.count }.by(0)
          expect(form.save).to be_truthy
          expect(form.entity).to eq(individual_investor_entity)
          expect(form.pfii).to eq(pfii)
        end

        it 'even when the phone formatting is not the same' do
          attributes[:signatory_daytime_phone] = '55-55-55-55-55'
          form                                 = described_class.new(attributes, user, { investment: investment })
          expect { form.save }.to change { InvestorEntity::Individual.count }.by(0)
          expect(form.save).to be_truthy
          expect(form.entity).to eq(individual_investor_entity)
          expect(form.pfii).to eq(pfii)
        end
      end

      context 'and the provided attributes are different' do
        it 'creates a new individual investor entity and its corresponding PFII' do
          attributes[:signatory_first_name] = 'Evil Paulo'
          form                              = described_class.new(attributes, user, { investment: investment })
          expect { form.save }.to change { InvestorEntity::Individual.count }.by(1)
          expect(form.entity).to_not eq(individual_investor_entity)
          expect(form.pfii).to_not eq(pfii)
          expect(form.pfii.first_name).to eq('Evil Paulo')
        end
      end

      context 'and the provided attributes match one of the existing PFIIs' do
        it 'uses the matching individual investor entity and its corresponding PFII' do
          attributes[:signatory_first_name] = 'Good Paulo'
          form                              = described_class.new(attributes, user, { investment: investment })
          expect { form.save }.to change { InvestorEntity::Individual.count }.by(0)
          expect(form.entity).to_not eq(individual_investor_entity)
          expect(form.entity).to eq(individual_investor_entity_second)
          expect(form.pfii).to_not eq(pfii)
          expect(form.pfii).to eq(pfii_second)
          expect(form.pfii.first_name).to eq('Good Paulo')
        end
      end
    end

    context 'when the user does not have an existing individual investor entity' do
      it 'creates a new individual investor entity and its corresponding PFII' do
        attributes[:signatory_first_name] = 'Empty Paulo'
        form                              = described_class.new(attributes, empty_user, { investment: investment })
        pfii_count                        = PersonalFederalIdentificationInformation.count
        expect { form.save }.to change { InvestorEntity::Individual.count }.by(1)
        expect(form.pfii.first_name).to eq('Empty Paulo')
        expect(PersonalFederalIdentificationInformation.count).to eq(pfii_count+1)
      end
    end
  end

  def mask_value(form, attr, val)
    case attr.to_sym
      when :signatory_date_of_birth
        form.mask_date(val)
      when :signatory_id_number
        form.mask_number(val, 2)
      when :signatory_ssn
        form.mask_number(val, 4)
      else
        val
    end
  end

end
