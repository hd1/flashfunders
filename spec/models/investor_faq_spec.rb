require 'rails_helper'

describe InvestorFaq do

  let(:attributes) { {
      question: 'Why is the sky blue?',
      answer: 'Just because.'
  } }

  subject { described_class.new(attributes) }

  it { should be_valid }

  context 'when question is not present' do
    before { subject.question = nil }

    it { should_not be_valid }
  end

  context 'when answer is not present' do
    before { subject.answer = nil }

    it { should_not be_valid }
  end

end

