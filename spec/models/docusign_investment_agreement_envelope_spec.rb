require 'rails_helper'

describe DocusignInvestmentAgreementEnvelope do
  subject(:docusign_envelope) { DocusignInvestmentAgreementEnvelope.new(1) }
  let(:docusign_client_double) { double(DocusignRest::Client, create_envelope_from_template: { 'envelopeId' => 'ENVID' }, get_envelope_recipients: { 'signers' => [
                                        {
    'roleName' => 'Issuer',
    'status' => 'sent',
  },
  {
    'roleName' => 'Investor',
    'status' => investor_status,
    'signedDateTime' => "2015-01-14T20:48:49.0130000Z"
  }
  ] })
  }
  let(:envelope_id) { 1 }
  let(:investor_status) { 'completed' }
  before { allow(DocusignInvestmentAgreementEnvelope).to receive(:docusign_client).and_return(docusign_client_double) }

  describe '#investor_status' do
    ['created', 'sent', 'delivered', 'faxpending', 'autoresponded', nil].each do |status|
      context "docusign investor status is #{status}" do
        let(:investor_status) { status }

        it 'returns incomplete' do
          expect(subject.investor_status).to eq(DocusignInvestmentAgreementEnvelope::INCOMPLETE)
        end
      end
    end

    ['signed', 'completed'].each do |status|
      context "docusign investor status is #{status}" do
        let(:investor_status) { status }

        it 'returns completed' do
          expect(subject.investor_status).to eq(DocusignInvestmentAgreementEnvelope::COMPLETED)
        end
      end
    end

    context 'docusign investor status is declined' do
      let(:investor_status) { 'declined' }

      it 'returns declined' do
        expect(subject.investor_status).to eq(DocusignInvestmentAgreementEnvelope::DECLINED)
      end
    end
  end

  describe '#investor_declined?' do
    context 'when the status is declined' do
      let(:investor_status) { 'declined' }

      it 'returns true' do
        expect(subject.investor_declined?).to eq(true)
      end
    end

    context 'when the status is not declined' do
      let(:investor_status) { 'completed' }

      it 'returns false' do
        expect(subject.investor_declined?).to eq(false)
      end
    end
  end

  describe '#investor_signed?' do
    context 'when the status is declined' do
      let(:investor_status) { 'declined' }

      it 'returns false' do
        expect(subject.investor_signed?).to eq(false)
      end
    end

    context 'when the status is not declined' do
      let(:investor_status) { 'completed' }

      it 'returns true' do
        expect(subject.investor_signed?).to eq(true)
      end
    end
  end

  describe '#investor_signed_at' do
    it 'returns a Time with zone' do
      expect(subject.investor_signed_at).to eq(Time.zone.parse("2015-01-14T20:48:49.0130000Z"))
    end
  end

  describe '#investor_signed_at' do
    it 'returns a Time with zone' do
      expect(subject.investor_signed_at).to eq(Time.zone.parse("2015-01-14T20:48:49.0130000Z"))
    end
  end

  describe '.create' do
    let(:date) {(Date.today + 2.weeks).to_s}
    let(:valid_args) { ['template_id', information] }
    let(:information) { {
        share_count: 1234,
        investment_amount: 22.27,
        closing_date: date,
        issuer_name: 'Issuer Guy',
        issuer_email: 'issuer@example.com',
        investor_name: 'fred',
        investor_email: 'fred@example.com',
        investor_phone_number: '555-555-5555',
        investor_address1: '123 Easy Street',
        investor_address2: 'Apt 1',
        investor_zip_code: '12354',
        investor_city: 'Santa Monica',
        investor_state: 'Colorado',
        investor_country: 'United States',
        corporate_name: 'The Company, Inc.',
    } }

    context 'Invalid response from Docusign' do
      before { allow(docusign_client_double).to receive(:create_envelope_from_template).and_return({}) }

      it 'raises a DocusignInvestmentAgreementEnvelope::EnvelopeNotCreatedError' do
        expect { DocusignInvestmentAgreementEnvelope.create(*valid_args) }.to raise_error(DocusignInvestmentAgreementEnvelope::EnvelopeNotCreatedError)
      end
    end

    context 'for an individual investor' do 
      it 'creates a new Docusign envelope when initialized' do
        DocusignInvestmentAgreementEnvelope.create(*valid_args)

        expect(docusign_client_double).to have_received(:create_envelope_from_template).with(
          template_id: 'template_id',
          email: {
          subject: 'The Company, Inc. Investment Agreement',
          body: '',
        },
        signers: [
          {
          embedded: true,
          name: 'fred',
          email: 'fred@example.com',
          role_name: 'Investor',
          :text_tabs=> [
            {:label=>'\\*bEntityName', :value=>''},
            {:label=>'\\*bInvestorName', :value=>''}, 
            {:label=>'\\*bInvestorRole', :value=> '' }, 
            {:label=>'\\*ieAddress1', :value=>'123 Easy Street'},
            {:label=>'\\*ieAddress2', :value=>'Apt 1'},
            {:label=>'\\*ieCity', :value=>'Santa Monica'},
            {:label=>'\\*ieName', :value=>'fred'},
            {:label=>'\\*ieState', :value=>'Colorado'},
            {:label=>'\\*ieCountry', :value=>'United States'},
            {:label=>'\\*ieTelephone', :value=>'555-555-5555'},
            {:label=>'\\*ieZip', :value=>'12354'},
            {:label=>'\\*CloseDate', :value=> date},
            {:label=>'\\*iInvestorEmail', :value=>'fred@example.com'},
            {:label=>'\\*iInvestorShares', :value=>1234},
            {:label=>'\\*iInvestorInvestmentAmount', :value=>22.27},
            {:label=>'\\*iInvestorName', :value=>'fred'},
        ]
        },
          {
          embedded: true,
          name: 'Issuer Guy',
          email: 'issuer@example.com',
          role_name: 'Issuer'
        },
        ],
        status: 'sent',
        )
      end

      it 'returns the envelopeId from the envelope hash' do
        expect(DocusignInvestmentAgreementEnvelope.create(*valid_args).envelope_id).to eq('ENVID')
      end
    end

    context 'for an entity investor' do 
      let(:information) { {
        share_count: 1234,
        investment_amount: 22.27,
        closing_date: date,
        issuer_name: 'Issuer Guy', 
        issuer_email: 'issuer@example.com',
        investor_role: 'Partner',
        investor_name: 'fred', 
        investor_email: 'fred@example.com', 
        investor_phone_number: '555-555-5555', 
        investor_address1: '123 Easy Street',
        investor_address2: 'Apt 1', 
        investor_zip_code: '12354',
        investor_city: 'Santa Monica',
        investor_state: 'Colorado',
        investor_country: 'United States',
        corporate_name: 'The Company, Inc.',
        entity_name: 'Private LLC', 
        entity_state: 'Alaska',
        entity_phone_number: '123-555-5555', 
        entity_address1: '777 Easy Street',
        entity_address2: '', 
        entity_zip_code: '55555',
        entity_city: 'Monica Santa',
        entity_country: 'United States',
      } }


      it 'creates a new Docusign envelope when initialized' do
        DocusignInvestmentAgreementEnvelope.create(*valid_args)

        expect(docusign_client_double).to have_received(:create_envelope_from_template).with(
          template_id: 'template_id',
          email: {
          subject: 'The Company, Inc. Investment Agreement',
          body: '',
        },
        signers: [
          {
          embedded: true,
          name: 'fred',
          email: 'fred@example.com',
          role_name: 'Investor',
          :text_tabs=> [
            {:label=>'\\*bEntityName', :value=>'Private LLC'},
            {:label=>'\\*bInvestorName', :value=>'fred'}, 
            {:label=>'\\*bInvestorRole', :value=> 'Partner' }, 
            {:label=>'\\*ieAddress1', :value=>'777 Easy Street'},
            {:label=>'\\*ieAddress2', :value=>''},
            {:label=>'\\*ieCity', :value=>'Monica Santa'},
            {:label=>'\\*ieName', :value=>'Private LLC'},
            {:label=>'\\*ieState', :value=>'Alaska'},
            {:label=>'\\*ieCountry', :value=>'United States'},
            {:label=>'\\*ieTelephone', :value=>'123-555-5555'},
            {:label=>'\\*ieZip', :value=>'55555'},
            {:label=>'\\*CloseDate', :value=> date},
            {:label=>'\\*iInvestorEmail', :value=>'fred@example.com'},
            {:label=>'\\*iInvestorShares', :value=>1234},
            {:label=>'\\*iInvestorInvestmentAmount', :value=>22.27},
            {:label=>'\\*iInvestorName', :value=>'fred'},
        ]
        },
          {
          embedded: true,
          name: 'Issuer Guy',
          email: 'issuer@example.com',
          role_name: 'Issuer'
        },
        ],
        status: 'sent',
        )
      end
    end
  end
end
