require 'rails_helper'

describe FacebookPresenter do
  subject(:presenter) { FacebookPresenter.new(url) }
  let(:url) { 'http://example.com/some_url' }

  def params
    @params ||= URI.decode_www_form(URI(presenter.share_url).query).each_with_object({}) { |param, hash| hash[param.first] = param.last }.symbolize_keys
  end

  describe '#share_url' do
    it 'calls the right facebook url' do
      uri = URI(presenter.share_url)
      expect(uri.scheme).to eq('https')
      expect(uri.host).to eq('www.facebook.com')
      expect(uri.path).to eq('/dialog/share')
    end

    it 'builds the url param correctly' do
      expect(params[:display]).to eq('popup')
      expect(params[:app_id]).to eq(ENV['FB_APP_ID'])
      expect(params[:redirect_uri]).to eq(ENV['FB_REDIRECT_URL'])
    end
  end
end